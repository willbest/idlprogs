; list of commands to carry out when first start IDL
; M. Liu


set_plot,'x'
;!edit_input = 512		; increase command history buffer


; set up paths properly
; with just the "+" symbol, IDL only finds directories with .pro or .sav files
;!path = !path+':'+expand_path('+/home/group3/tdupuy/idl', /all_dirs)

;defsysv, '!ROOT_DIR', '~/idl/masking/'
;defsysv, '!ROOT_DIR', '~/idl/masking-old/'


;if getyn('setup for normal 8-color display (24-color otherwise)?') then begin & $
;    device,pseudo_color=8 & $
;    ;device,decomposed=0 & $
;endif else begin & $
;    device,true_color=24 & $
;    device,decomposed=0 & $  ; added 5/00 - make ATV.PRO work right
;endelse
	


; don't need this for 24-bit color machine
; unless you want to use the LINCOLR.PRO routine
;device,pseudo_color=8 
device,decomposed=0


; to have proper backing store
device, retain=2


; set plotting symbol 8 to circle
a = findgen(16)*(!pi*2/16.)
usersym,cos(a),sin(a),/fill


; bad pixel convention for image processing routines
BADVAL = -1e6


; open window and display a pattern
;if getyn('open a window?') then begin &$
;	win, 0,sz=400  &$
;	loadct,	15 &$
;	display,/aspect,byte((findgen(401)-200) # (findgen(401)-200)) &$
;endif


; initialize for IDL Astro (Goddard) stuff
astrolib


; remap the ctrl-D key to regular Unix usage
define_key, /CONTROL, '^D', /DELETE_CURRENT


; Postscript labels for SED: 
;	lambda, micron, lambda * F(lambda)
;
lambda = '!9'+STRING(108B)+'!X'
um='!9'+STRING(109B)+'!Xm'
lamflam = lambda+'F!D'+lambda+'!N'
alpha = '!9'+STRING(97B)+'!X'
delta = '!9'+STRING(68B)+'!X'
angstrom = '!9'+STRING(197B)+'!X'

;; angstrom symbol
;angsym = STRING("305B)


; The Sun symbol ("\odot" in IDL) for vector fonts
; for Postscript: use SUNSYMBOL() function in IDL astro lib
Msun = "M!D!9n!X!N" 

; don't need this for 24-bit color machine
; unless you want to use the LINCOLR.PRO routine
;device,pseudo_color=8 
;device,decomposed=0
