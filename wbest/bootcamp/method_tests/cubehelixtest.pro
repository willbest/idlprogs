;PRO CUBEHELIXTEST

device, decomposed=0
window, 0

;; cubehelix, /plot, rots=0.9, start=2.5, hue=1.5, gamma=0.8
;; earlyM = 200
;; lateM = 180
;; earlyL = 150
;; lateL = 125
;; earlyT = 80
;; lateT = 50

cubehelix, /plot, rots=0.8, start=0.5, hue=1.5, gamma=0.8
earlyM = 200
lateM = 180
earlyL = 150
lateL = 125
earlyT = 80
lateT = 50

cset = [earlyM, lateM, earlyL, lateL, earlyT, lateT]
black = 0
white = 255

c = findgen(6)*10
y = intarr(100,6)
for i=0, 5 do y[*,i] = findgen(100) + c[i]

window, 1

plot, y[*,0], background=white, color=black, charsize=1.5, /nodata
for i=0, 5 do oplot, y[*,i], color=cset[i], thick=4

END

