FUNCTION GAUSSTWO, x, y
s = 14 / (2*sqrt(2*alog(2)))
a = 1; / 12.567569
return, a * exp( -(x^2/(2*s^2)) -(y^2/(2*s^2)) )
END

FUNCTION PQ_Limits, x
;return, [0., sqrt(100^2 - x^2)]
return, [0., sqrt(7^2 - x^2)]
END

PRO FWHM
;AB_Limits = [0., 100.]
AB_Limits = [0., 7]
print, 4 * int_2D('gausstwo', AB_Limits, 'PQ_Limits', 96, /DOUBLE)
END


