FUNCTION DIFFER, x, y
return, [ -0.5*y[0], 4.0 - 0.3*y[1] -0.1*y[0], 2.*y[1] - 0.3*y[2]^2 ]
END

PRO RKTEST
y = [4., 6., 1.5]
x = 0.
h = 0.1
for i=0, 9 do begin
    dydx = differ(x,y)
    y = rk4(y, dydx, x, h, 'differ', /DOUBLE)
    print, y
;    x = x + h
endfor
END
