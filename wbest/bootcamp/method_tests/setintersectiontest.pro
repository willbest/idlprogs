;PRO SETINTERSECTIONTEST

flux = 0.6
uncert = 0.3

for i=0, 3 do begin
    print
    lineflux = randomu(seed, 15)
    print, lineflux
    matched = where((lineflux ge flux-uncert) and (lineflux le flux+uncert))
    print, matched
    if i eq 0 then begin
        inters = matched
    endif else begin
        inters = setintersection(inters, matched)
    endelse
    print
    print, inters
endfor

END
