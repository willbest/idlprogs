pro switchtest, member

switch member of
	'mama' : print, 'mama'
	'papa' : print, 'papa'
	else : print, 'no one'
endswitch

end
