;PRO ARRANGING

ints = indgen(5,4,3)

; Create an array of structures
test = {a:0, b:0, c:0., f:0}
x = [2,3,4,5,6]
y = [7,9,3,11]
z = [.1,.2,.3]

for i=0, 4 do begin
    for j=0, 3 do begin
        for k=0, 2 do begin
            test = [test, {a:x[i], b:y[j], c:z[k], f:ints[i,j,k]}]
        endfor
    endfor
endfor

test = test[1:*]

; Arrange the array in a random order
seq = randomu(seed, 60)
order = sort(seq)
test = test[order]

print
;print, test
print

; Re-sort the array by the a, b, c tags -- DOESN'T WORK
;; ordera = sort(test.a)
;; test = test[ordera]
;; orderb = sort(test.b)
;; test = test[orderb]
;; orderc = sort(test.c)
;; test = test[orderc]

; Build an array -- THIS WORKS
array = intarr(5,4,3, /nozero)

for i=0, 59 do begin
    array[where(x eq test[i].a), where(y eq test[i].b), where(z eq test[i].c)] = test[i].f
endfor

print, array
print

END
