;  flambda=[0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5]
;  lambda=[0.6,0.73,0.82,0.89,1.0,1.11,1.21,1.28,1.38,1.53,1.62,1.7]
;  lind=lambda-min(lambda)
;  maxlind=max(lind)
;  lind=lind/max(lind)
;  lnum=n_elements(lambda)
;  lind=lind*(lnum-1)
;  print, lind

flambda=[0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5]
lambda=[0.6,0.73,0.82,0.89,1.0,1.05,1.11,1.21,1.28,1.32,1.38,1.53,1.62,1.7]
;lambda=[0.6,0.7,0.8,0.85,0.9,0.95,1.0,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4,1.45,1.5,1.6,1.7]
fitind=where((lambda ge min(flambda)) and (lambda le max(flambda)))
; print, fitind
;           2           3           4           5           6           7
;           8
fitlambda=lambda[fitind]
; print, fitlambda
;     0.820000     0.890000      1.00000      1.11000      1.21000      1.28000
;      1.38000
lind=fitlambda-min(flambda)
maxlind=max(flambda)-min(flambda)
lind=lind/maxlind
lnum=n_elements(flambda)
lind=lind*(lnum-1)
print, lind
