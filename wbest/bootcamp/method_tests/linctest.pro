;PRO LINCTEST

lincolr, /silent
device, decomposed=0
x=indgen(20)
plot, x, x, /nodata, color=0, background=1
fl = [ 'Black', 'White', 'Green', 'Red', 'Blue', 'Magenta', 'Cyan', 'Brown', 'Orange', $
          'Pink', 'Aquamarine', 'Orchid', 'Gray', 'Dark Green', 'Gold', 'Navy' ]
cset=indgen(16)
legend, fl, chars=2, linestyle=0, outline=1, colors=cset, textcolors=cset

END
