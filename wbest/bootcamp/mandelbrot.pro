FUNCTION COMPQUAD, VALUE, CONSTANT
;Computes the value of the complex quadratic function y = z^2 + c
;VALUE = [real value, imaginary value]
;CONSTANT = [real constant, imaginary constant]

real = 0.0D
imag = 0.0D
real = value[0]^2 - value[1]^2 + constant[0]
imag = 2*value[0]*value[1] + constant[1]
return, [real,imag]

END


PRO MANDELBROT, MAXITER=MAXITER, FRAME=FRAME

;Check arguments
if (n_elements(maxiter) ne 1) then maxiter=100
if (n_elements(frame) ne 4) then frame=double([-2,1,-1,1])

;Display blank axes using FRAME to set bounds
window, 0, xsize=540, ysize=378
plot, [0,0], [0,0], title='!3Mandelbrot Set', ticklen=0, $
xstyle=1, xrange=[frame[0],frame[1]], $
ystyle=1, yrange=[frame[2],frame[3]], /nodata
wide = double(convert_coord(!x.crange, !y.crange, /to_device))

;Begin the loop for each pixel
for h=frame[0], frame[1], (frame[1]-frame[0])/(wide[0,1]-wide[0,0]) do begin
   for v=frame[2], frame[3], (frame[3]-frame[2])/(wide[1,1]-wide[1,0]) do begin

;iterate the complex quadratic function, using the given constant,
;a maximum of maxiter times.
;Adapted from http://en.wikipedia.org/wiki/Mandelbrot_set
        constant=[h,v]
        value = dblarr(2)
        count=0
        while (total(value^2) le 4.0D) and count lt maxiter do begin
            value = compquad(value,constant)
            count = count + 1
        endwhile

;If the value is unbounded, plot the point.
        if (count ne maxiter) then oplot, [h], [v], psym=3

    endfor
endfor

END
