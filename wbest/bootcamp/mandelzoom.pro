PRO MANDELZOOM, MAXITER=MAXITER, FRAME=FRAME

;Check arguments
if (n_elements(maxiter) ne 1) then maxiter=100
if (n_elements(frame) ne 4) then frame=double([-2,1,-1,1])

;Call program MANDELBROT to draw the initial Mandelbrot Set
mandelbrot, maxiter=maxiter, frame=frame

;Copy image into pixmap window
wset, 0
xsize=!D.x_vsize
ysize=!D.y_vsize
window, 1, /pixmap, xsize=xsize, ysize=ysize

;Wait for input 'q' to quit the program.
print, "Press q to quit the program"
while get_kbrd(0) ne 'q' do begin

    ;Allow user to click-and-drag a box.
    ;This "rubberbox" procedure written by David Fanning.
    wset, 1
    device, copy=[0,0,xsize,ysize,0,0,0]
    wset, 0
    cursor, x1, y1, /down, /device
    repeat begin
        cursor, x2, y2, /change, /device
        device, copy=[0,0,xsize,ysize,0,0,1]
        plots, [x1,x1,x2,x2,x1], [y1,y2,y2,y1,y1], linestyle=3, /device
    endrep until !Mouse.Button eq 0
    
    ;Redraw the graph, framed by the user-defined box.
    xvals=double([x1,x2])
    yvals=double([y1,y2])
    datac=convert_coord(xvals, yvals, /device, /to_data)
    xmin = min(datac[0,*], max=xmax)
    ymin = min(datac[1,*], max=ymax)
    frame=[xmin,xmax,ymin,ymax]
    mandelbrot, maxiter=maxiter, frame=frame

endwhile

END
