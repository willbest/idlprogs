; Procedure to determine all the prime factors of an input number

PRO PRIMEFAC, NUM

if (NUM lt 1) or (ulong(NUM) ne float(NUM)) then begin
    print, 'You must enter a positive integer less than 4,294,967,296'
    return
endif

if NUM eq 1 then begin
    print, '1 is a unit, so it has no prime factors.'
    return
endif

print, 'Prime Factors:'
x = NUM
fac=[0]
while x mod 2UL eq 0 do begin
    fac=[fac,2]
    x = x/2UL
endwhile

for i = 3UL, ulong(sqrt(x)), 2UL do begin
    while x mod i eq 0 do begin
        fac=[fac,i]
        x = x/i
    endwhile
endfor

if x gt 1UL then fac=[fac,x]
fac=fac[1:*]
if fac[0] eq NUM then begin
    print, fac, format='(i10, " is prime.")'
endif else begin
    print, fac, format='(i10)'
endelse

END
