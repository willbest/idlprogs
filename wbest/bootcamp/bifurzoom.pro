PRO BIFURZOOM, seed=seed, niter=niter, frame=frame

;Check inputs for seed and niter, needed for zoom-ins
;Get and check arguments
if (n_elements(seed) ne 1) then read, seed, prompt=$
  'Enter a seed value between 0 and 1: '
if (seed le 0) or (seed ge 1) then begin
    print, 'Seed value defaulted to 0.5.'
    seed=0.5
endif

if (n_elements(niter) ne 1) then read, niter, prompt=$
  'Enter the number of iterations (minimum 20): '
if (niter lt 20) then begin
    print, 'Number of iterations defaulted to 500.'
    niter=500.0
endif

;Call program Bifur to draw the initial bifurcation diagram
bifur, seed=seed, niter=niter, frame=frame

;Copy image into pixmap window
wset, 0
xsize=!D.x_vsize
ysize=!D.y_vsize
window, 1, /pixmap, xsize=xsize, ysize=ysize

;Wait for input 'q' to quit the program.
print, "Press q to quit the program"
while get_kbrd(0) ne 'q' do begin

    ;Allow user to click-and-drag a box.
    ;This "rubberbox" procedure written by David Fanning.
    wset, 1
    device, copy=[0,0,xsize,ysize,0,0,0]
    wset, 0
    cursor, x1, y1, /down, /device
    repeat begin
        cursor, x2, y2, /change, /device
        device, copy=[0,0,xsize,ysize,0,0,1]
        plots, [x1,x1,x2,x2,x1], [y1,y2,y2,y1,y1], linestyle=3, /device
    endrep until !Mouse.Button eq 0
    
    ;Redraw the graph, framed by the user-defined box.
    xvals=[x1,x2]
    yvals=[y1,y2]
    datac=convert_coord(xvals, yvals, /device, /to_data)
    xmin = min(datac[0,*], max=xmax)
    ymin = min(datac[1,*], max=ymax)
    frame=[xmin,xmax,ymin,ymax]
    bifur, seed=seed, niter=niter, frame=frame

endwhile

END
