FUNCTION LOGISTIC_INDEP, INVALUE, COEFFICIENT=COEFFICIENT

;Computes the value of the logistic function f(x)=nx(1-x)
;for a given coefficient 1<n<4 and input value 0<x<1.

;Default value for the coefficient is 1
if (n_elements(coefficient) eq 0) then coefficient=1

;Check arguments
if (n_params() ne 1) then message, "No input value given."
if (invalue ge 1) or (invalue le 0) then message, $
  "The input value must be between 0 and 1."
if (coefficient gt 4) or (coefficient lt 1) then message, $
  "The coefficient must be between 1 and 4, inclusive."

return, coefficient*invalue*(1 - invalue)

END
