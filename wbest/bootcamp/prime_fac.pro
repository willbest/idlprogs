; Procedure to determine all the prime factors of an input number

PRO PRIMEFAC, NUM

case 1 of

(NUM lt 1) or (ulong(NUM) ne float(NUM)) : begin
    print, 'You must enter a positive integer less than 4,294,967,296'
    end

NUM eq 1 : print, '1 is a unit, so it has no prime factors.'

else: begin
    print, 'Prime Factors:'
    x = NUM
    fac=[0]
    while x mod 2UL eq 0 do begin
        fac=[fac,2]
        x = x/2UL
    endwhile
    for i = 3UL, ulong(sqrt(x)), 2UL do begin
        while x mod i eq 0 do begin
            fac=[fac,i]
            x = x/i
        endwhile
    endfor
    if x gt 1UL then fac=[fac,x]
    fac=fac[1:*]
    print, fac
    end

endcase

END
