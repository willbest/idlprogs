FUNCTION LOGISTIC, INVALUE, COEFFICIENT
;Computes the value of the logistic function f(x)=nx(1-x).

return, coefficient*invalue*(1 - invalue)

END


PRO ITERATE, SEED=SEED, COEFFICIENT=COEFFICIENT, NITER=NITER

;Get and check arguments
if (n_elements(seed) ne 1) then read, seed, prompt=$
  'Enter a seed value between 0 and 1: '
if (seed le 0) or (seed ge 1) then begin
    print, 'Seed value defaulted to 0.5.'
    seed=0.5
endif

if (n_elements(coefficient) ne 1) then read, coefficient, prompt=$
  'Enter a coefficient between 1 and 4, inclusive: '
if (coefficient lt 1) or (coefficient gt 4) then begin
    print, 'Coefficient defaulted to 1.'
    coefficient=1.0
endif

if (n_elements(niter) ne 1) then read, niter, prompt=$
  'Enter the number of iterations (minimum 20): '
if (niter lt 20) then begin
    print, 'Number of iterations defaulted to 500.'
    niter=500.0
endif

;iterate the logistic function, using given values, niter times,
;storing the function value of each iteration in array y. 
f=seed & y=seed
for i=1.0, niter do begin
    f=logistic(f,coefficient)
    y=[y,f]
endfor

;Plot the results. x-values default to [0,1,2,3,...].
plot, y, psym=3, title='### iterations of f(x)=nx(1-x) with n=###'

;help, seed, coefficient, niter

END
