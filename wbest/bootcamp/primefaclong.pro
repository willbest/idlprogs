PRO PRIMEFACLONG, NUM

; Procedure to determine all the prime factors of an input number

case 1 of
    (NUM lt 1) or (long64(NUM) ne float(NUM)) : begin
        print, 'You must enter a positive integer!'
    end

    NUM eq 1 : print, '1 is a unit, so it has no prime factors.'

    else: begin
        print, 'Prime Factors:'
        x = NUM
        fac=[0]
        while x mod 2LL eq 0 do begin
            fac=[fac,2LL]
            x = x/2LL
        endwhile
        for i = 3LL, long64(sqrt(x)), 2LL do begin
            while x mod i eq 0 do begin
                fac=[fac,i]
                x = x/i
            endwhile
        endfor
        if x gt 1LL then fac=[fac,x]
        fac=fac[1:*]
        print, fac
    end

endcase

END
