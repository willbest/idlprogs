FUNCTION LOGISTIC, INVALUE, COEFFICIENT
;Computes the value of the logistic function f(x)=nx(1-x).

return, coefficient*invalue*(1 - invalue)

END


FUNCTION ITER, SEED, COEFFICIENT, NITER
;iterate the logistic function, using given values, niter times.

f=seed
for i=1.0, niter do f=logistic(f,coefficient)
return, f

END


PRO BIFUR, SEED=SEED, NITER=NITER, FRAME=FRAME

;Get and check arguments
if (n_elements(seed) ne 1) then read, seed, prompt=$
  'Enter a seed value between 0 and 1: '
if (seed le 0) or (seed ge 1) then begin
    print, 'Seed value defaulted to 0.5.'
    seed=0.5
endif

if (n_elements(niter) ne 1) then read, niter, prompt=$
  'Enter the number of iterations (minimum 20): '
if (niter lt 20) then begin
    print, 'Number of iterations defaulted to 500.'
    niter=500.0
endif

if (n_elements(frame) ne 4) then begin
    print, 'Enter values for min_coeff, max_coeff, ymin, ymax.'
    print, 'Coeff values must be between 1 and 4 inclusive.'
    read, frame, prompt=$
      'y values must be between 0 and 1: '
endif
switch 1 of
    n_elements(frame) ne 4 :
    frame[0] lt 1.0 :
    frame[1] gt 4.0 :
    frame[2] lt 0.0 :
    frame[3] gt 1.0 : begin
        print, 'Coefficient min and max defaulted to 1 and 4.'
        print, 'y min and max defaulted to 0 and 1.'
        frame=[1.0,4.0,0.0,1.0]
    end
    else : if size(frame, /type) ne 4 then frame=float(frame)
endswitch

;blank axes using FRAME to set bounds
window, 0
plot, [0,0], [0,0], title='!3Bifurcation Diagram for f(x)=nx(1-x)', $
charsize=2.0, xtitle='coefficient n', xcharsize=0.7, ycharsize=0.7, $
xstyle=1, xrange=[frame[0],frame[1]], $
ystyle=1, yrange=[frame[2],frame[3]], /nodata

;Obtain the long-term iteration values and plot them.
wide=convert_coord(!x.crange, !y.crange, /to_device)
for i=frame[0], frame[1], (frame[1]-frame[0])/(wide[0,1]-wide[0,0]) do begin
    newseed=iter(seed,i,niter)
    newf=newseed & y=0.0
    for j=1, 100 do begin
        newf=logistic(newf,i)
        y=[y,newf]
    endfor
    x=replicate(i,100)
    y=y[1:*]
    oplot, x, y, psym=3, symsize=2.0
endfor

END
