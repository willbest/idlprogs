;PRO SUBARUSCRIPT

;  Creates an observing script for Subaru LGS-AO188 imaging targets.
;
;  HISTORY
;  Written by Will Best (IfA), 06/07/2012
;
;  INPUTS: 
;      ARRAY - Vector to be searched.
;      VALUE - The target scalar value.
;
;  OUTPUT:
;      INDEX - The index of the element in ARRAY whose value is closest to VALUE.
;
;  OPTIONAL KEYWORD OUTPUTS:
;      ACTUAL - The actual value of the element in ARRAY whose index is INDEX.
;      DELTA - The difference between the closest element and the target value.
;

; Primary target list
targs = '~/Astro/observing/Subaru/jun12.lgs19.subaru.txt'
readcol, targs, rar, decr, namer, format='a,a,a', delim=string(9b), comment='#', /silent
n = n_elements(rar)

name = strmid(namer, 3, 1#strpos(namer,' ')-3)
; this 1# extracts only one substring from each string in the array

alias = strarr(n)
ps1 = where(strmid(name,0,3) eq 'PS1', complement=nops1)
alias[ps1] = strmid(name[ps1],0,3) + '_' + strmid(name[ps1], 1#strpos(name[ps1],'-',/reverse_search)+1)
alias[nops1] = strmid(name[nops1], 0, 1#strlen(name[nops1])-5)
; this 1# extracts only one substring from each string in the array

ra = rar
remchar, ra , ':'
ra = ra + '0'

dec = decr
remchar, dec , ':'
dec = dec + '0'

equinox = replicate('2000.0', n)
pa = replicate('0.0', n)

textout='~/Astro/observing/Subaru/script.2012.06.txt'
forprint, alias, name, ra, dec, equinox, pa, textout=textout, $
  format='(a,9H=OBJECT=",a,5H" RA=,a10," DEC=",a10," EQUINOX=",a6," FIELD_PA=",a3,13H GSMODE="LGS")', /silent


; Tip-tilt stars
targst = '~/Astro/observing/Subaru/lgs19.jun12-subaru.txt'
readcol, targst, namert, rah, ram, ras, decd, decm, decs, format='a,a,a,a,a,a,a', comment='#', /silent
nt = n_elements(rah)

aliast = strarr(nt)
namet = strarr(nt)
obj = where(strmid(namert,0,1) eq '+', complement=tt)
aliast[obj] = alias
namet[obj] = name
for i=0, n_elements(tt)-1 do begin
    d = where((tt[i]-obj) gt 0)
    aliast[tt[i]] = alias[max(d)]+'_TT'+trim(min(tt[i] - obj[d]))
    namet[tt[i]] = name[max(d)]+'_TT'+trim(min(tt[i] - obj[d]))
endfor

rat = rah+ram+ras
dect = decd+decm+decs

equinoxt = replicate('2000.0', nt)
pat = replicate('0.0', nt)

textout='~/Astro/observing/Subaru/tscript.2012.06.txt'
forprint, aliast, namet, rat, dect, equinoxt, pat, textout=textout, $
  format='(a,9H=OBJECT=",a,5H" RA=,a10," DEC=",a10," EQUINOX=",a6," FIELD_PA=",a3,13H GSMODE="LGS")', /silent

outfile = '~/Astro/observing/Subaru/imag.2012.06.txt'
openw, 1, outfile

for i=0, n-1 do begin
    printf, 1, '# ',strmid(namer[i],0,strpos(namer[i],' ')),'  ',rar[i],' ',decr[i]
    printf, 1, '#when TT guide star != target center'
    printf, 1, 'SetupField $DEF_IMSTA $DEF_IM20CH4S $',alias[i],'_TT1 TMODE=SID'
    printf, 1, 'AO188_OFFSET_RADEC $DEF_AOLN $',alias[i] 
    printf, 1, 'CheckField $DEF_IMSTA $DEF_IM20CH4S EXPTIME=90'
    printf, 1, 'GetObject $DEF_IMS5A EXPTIME=90 DITH=5.0 COADDS=1 PIXSCALE=20MAS NDUMMYREAD=0 MODE=AOP'
    printf, 1, 'CheckField $DEF_IMSTA $DEF_IM20H EXPTIME=90'
    printf, 1, 'GetObject $DEF_IMS5A EXPTIME=90 DITH=5.0 COADDS=1 PIXSCALE=20MAS NDUMMYREAD=0 MODE=AOP'
    printf, 1, 'CheckField $DEF_IMSTA $DEF_IM20J EXPTIME=90'
    printf, 1, 'GetObject $DEF_IMS5A EXPTIME=90 DITH=5.0 COADDS=1 PIXSCALE=20MAS NDUMMYREAD=0 MODE=AOP'
    printf, 1, 'CheckField $DEF_IMSTA $DEF_IM20K EXPTIME=90'
    printf, 1, 'GetObject $DEF_IMS5A EXPTIME=90 DITH=5.0 COADDS=1 PIXSCALE=20MAS NDUMMYREAD=0 MODE=AOP'
    printf, 1, 'CheckField $DEF_IMSTA $DEF_IM20Z EXPTIME=90'
    printf, 1, 'GetObject $DEF_IMS5A EXPTIME=90 DITH=5.0 COADDS=1 PIXSCALE=20MAS NDUMMYREAD=0 MODE=AOP'
    printf, 1
endfor

close, 1

;; ;Bad weather backup list
;; targs = '~/Astro/observing/Subaru/subaru_backups.txt'
;; readcol, targs, name, ra, dec, format='a,a,a', /silent
;; n = n_elements(ra)

;; alias = strupcase(name)

;; remchar, ra , ':'
;; ra = ra + '0'

;; remchar, dec , ':'
;; dec = dec + '0'

;; equinox = replicate('2000.0', n)
;; pa = replicate('0.0', n)

;; textout='~/Astro/observing/Subaru/backups.2012.06.txt'
;; forprint, alias, name, ra, dec, equinox, pa, textout=textout, $
;;   format='(a,9H=OBJECT=",a,5H" RA=,a10," DEC=",a10," EQUINOX=",a6," FIELD_PA=",a3,13H GSMODE="LGS")', /silent


END
