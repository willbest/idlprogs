; compute needed integration time for each object in JHK
; to reach a desired S/N 
; 07/26/00 M. Liu
;
; added SpeX numbers, changed input file
; 11/04/01 MCL
;
; renamed from CALC-TINT.IDL (from IC 348 work) to SPECTRA-CALC-TINT.IDL
; added calculation of total observing time needed,
;   given the observing efficiency and per-target overhead
; 03/31/06 MCL
;
; slight tweaks to input/output
; 06/02/07 MCL
;
; added min integration time for any one target ('tobs_min')
; 04/01/10 MCL


targetlist = '~/Astro/observing/proposals/IRTF/2013B.dat'
outlist = '~/Astro/observing/proposals/IRTF/2013B_time.txt'
;targetlist = 'lodieu-ukidss-GCS-table4-faint.dat'


; observing inputs
snr = 40.          ; desired SNR per res element
obs_eff = 0.75     ; observing efficiency when on-target
t_setup =  15.     ; fixed overhead (acquisition & std stars) per target, in minutes
tobs_max = 60.     ; max integration time for any one target, in min
tobs_min = 3.      ; min integration time for any one target, in min


;----------------------------------------------------------------------
; limiting JHK mags which give S/N=40 per res element in 1 hr
;----------------------------------------------------------------------
tint0 = 60.  ; nominal int time, in minutes
snr0 = 40    ; nominal SNR=40
; SpeX (S/N is per pixel I think)
;sens_mags = [14.2, 13.7, 13.3] &  ss = 'SpeX: 0.5" slit (R=1200), 0.8" seeing'
;sens_mags = [14.0, 13.5, 13.1] &  ss = 'SpeX: 0.5" slit (R=1200), 1.1" seeing'
;sens_mags = [15.1, 14.7, 14.2] &  ss = 'SpeX: 0.8" slit (R=750), 0.8" seeing'
;sens_mags = [16.4, 16.0, 15.5] &  ss =  'SpeX prism: 0.5" slit (R~150), 0.8" seeing'
;sens_mags = [15.4, 14.9, 14.5] &  ss =  'SpeX prism: 0.3" slit (R~250), 0.8" seeing'
sens_mags = [17.4, 16.9, 16.4]  &  ss = 'Spex prism: 0.8" slit (R~100), 0.8" seeing'

;; IRCS
;sens_mags = [17.6, 16.4, 15.9]  & ss = 'R~1400, 0.15" slit'
;sens_mags = [18.4, 17.2, 16.7] & ss = 'R~700, 0.30" slit'
;sens_mags = [18.9, 17.7, 17.2] & ss = 'R~350, 0.60" slit'

;; NIRSPEC
;sens_mags = [18.6, 17.9, 17.6] &  $
; ss = 'NIRSPEC, in OH regions, R~2000'
;sens_mags = [19.45, 18.65, 18.35] &  $
; ss = 'NIRSPEC, in OH regions, binned to R~400'


;----------------------------------------------------------------------
; load target list info
;----------------------------------------------------------------------
readcol2, targetlist, $
          id, jmag, hmag, kmag, form = 'a,f,f,f'
nobj = n_elements(id)
;readcol2, targetlist, $
;          id, rastr, decstr, hmag, kmag, $
;          form = 'a,a,a,f,f,f'
;jmag = hmag+1.0   ; assume (J-H) = 1.0
;nobj = n_elements(id)
;
;readcol2, 'swift-pmtargets.lst', $
;          id, jmag, hmag, kmag, form = 'a,f,f,f'
;nobj = n_elements(ra)
;id = strc(ra)+'+'+strc(dec)
;spt = intarr(nobj)
;hmag = (jmag+kmag)/2.  ; no H-band phot for these objects, so use an estimate
;
;readcol2, '../ic348_targets.lst', $
;  id, spt, espt, ra1, ra2, ra3, dec1, dec2, dec3,  $
;  kmag, Ak, imag, jmag, hmag,  $
;  form = 'a,f,f,f,f,f,f,f,f,f,f,f,f,f', /silent
;nobj = n_elements(id)


jtint = fltarr(nobj)
htint = fltarr(nobj)
ktint = fltarr(nobj)

print
print, '#--------------------------------------------------#'
print, '# ', strupcase(getproname()), +':'+userid()
print, '#   ', ss
print, '#--------------------------------------------------#'
print, '# baseline {J,H,K} sensitivites = ', commalist(sens_mags)
print, '#   reference int. time (min) = ', strc(tint0)
print, '#   reference S/N per pixel = ', strc(snr0)
print, '# '
print, '# desired S/N per pixel = '+strc(snr)
print, '# computed integration time is in minutes'
print, '#--------------------------------------------------#'
print, '# id               Jmag    tint(J)   '+ $
  'Hmag    tint(H)   Kmag    tint(K)'
;print, '# id     SpT(M)    A(K)     Kmag    tint(K)   '+ $
;  'Jmag    tint(J)   Hmag    tint(H)'
print
for i = 0, nobj-1 do begin

    ; calc int times assuming background limited,
    ; i.e. for fixed S/N, tint scales as (object)^2.
    jtint(i) = 10^(0.8*(jmag(i)-sens_mags(0))) * tint0 * (snr/snr0)^2
    htint(i) = 10^(0.8*(hmag(i)-sens_mags(1))) * tint0 * (snr/snr0)^2
    ktint(i) = 10^(0.8*(kmag[i]-sens_mags(2))) * tint0 * (snr/snr0)^2

    print, $
      form = '(A15,3(F9.2,I6,"   "))', $
      id(i), jmag(i), jtint(i), hmag(i), htint(i), kmag[i], ktint(i)
      ;id(i), kmag[i], ktint(i), jmag(i), jtint(i), hmag(i), htint(i)
      ;form = '(A6,F7.1,F10.2,3(F10.2,I8))', $
      ;id(i), spt(i), Ak(i),  $
      ;kmag[i], ktint(i), jmag(i), jtint(i), hmag(i), htint(i)

endfor
print
print, '#--------------------------------------------------#'
print, '# ', strupcase(getproname()), +':'+userid()
print, '# input file = "', targetlist, '"'
print, '# ', ss
print, '# total observing time required'
print, '#--------------------------------------------------#'
print, '# desired S/N per pixel = '+strc(snr)
print, '# on-target observing efficiency = ', strc(obs_eff)
print, '# fixed overhead per target (min) = ', strc(t_setup)
print, '# max allowed integration per target (min) ', strc(tobs_max)
print, '# min allowed integration per target (min) ', strc(tobs_min)
print, '#'
jhk_tint = [total(jtint < tobs_max > tobs_min), $
            total(htint < tobs_max > tobs_min), $
            total(ktint < tobs_max > tobs_min)]
jhk_over = [total(jtint gt tobs_max), $
            total(htint gt tobs_max), $
            total(ktint gt tobs_max)]
jhk_str = ['J-band', 'H-band', 'K-band']
print, '# total number of targets = ', strc(nobj)
for i=0, 2 do begin
  print, '# ', jhk_str(i), ':'
  print, '#   total hours of observing = ', $
         strc((jhk_tint(i)/obs_eff + nobj*t_setup)/60.)
  print, '#   number of targets not reaching desired S/N = ', strc(fix(jhk_over(i))), ' out of ', strc(nobj)
endfor
print, '#'
print, '#--------------------------------------------------#'

if n_elements(outlist) gt 0 then begin
    get_lun, lun
    openw, lun, outlist
    printf, lun, '#--------------------------------------------------#'
    printf, lun, '# ', strupcase(getproname()), +':'+userid()
    printf, lun, '#   ', ss
    printf, lun, '#--------------------------------------------------#'
    printf, lun, '# baseline {J,H,K} sensitivites = ', commalist(sens_mags)
    printf, lun, '#   reference int. time (min) = ', strc(tint0)
    printf, lun, '#   reference S/N per pixel = ', strc(snr0)
    printf, lun, '# '
    printf, lun, '# desired S/N per pixel = '+strc(snr)
    printf, lun, '# computed integration time is in minutes'
    printf, lun, '#--------------------------------------------------#'
    printf, lun, '# id               Jmag    tint(J)   '+ $
           'Hmag    tint(H)   Kmag    tint(K)'
    for i = 0, nobj-1 do begin
        jtint(i) = 10^(0.8*(jmag(i)-sens_mags(0))) * tint0 * (snr/snr0)^2
        htint(i) = 10^(0.8*(hmag(i)-sens_mags(1))) * tint0 * (snr/snr0)^2
        ktint(i) = 10^(0.8*(kmag[i]-sens_mags(2))) * tint0 * (snr/snr0)^2

        printf, lun, $
          form = '(A15,3(F9.2,I6,"   "))', $
          id(i), jmag(i), jtint(i), hmag(i), htint(i), kmag[i], ktint(i)

    endfor
    printf, lun, '#--------------------------------------------------#'
    printf, lun, '# ', strupcase(getproname()), +':'+userid()
    printf, lun, '# input file = "', targetlist, '"'
    printf, lun, '# ', ss
    printf, lun, '# total observing time required'
    printf, lun, '#--------------------------------------------------#'
    printf, lun, '# desired S/N per pixel = '+strc(snr)
    printf, lun, '# on-target observing efficiency = ', strc(obs_eff)
    printf, lun, '# fixed overhead per target (min) = ', strc(t_setup)
    printf, lun, '# max allowed integration per target (min) ', strc(tobs_max)
    printf, lun, '# min allowed integration per target (min) ', strc(tobs_min)
    printf, lun, '#'
    printf, lun, '# total number of targets = ', strc(nobj)
    for i=0, 2 do begin
        printf, lun, '# ', jhk_str(i), ':'
        printf, lun, '#   total hours of observing = ', $
               strc((jhk_tint(i)/obs_eff + nobj*t_setup)/60.)
        printf, lun, '#   number of targets not reaching desired S/N = ', strc(fix(jhk_over(i))), ' out of ', strc(nobj)
    endfor

    free_lun, lun
endif

end
