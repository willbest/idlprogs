PRO MAKESTARCAT, DATE, NUM, BASEPATH

;  Creates a starcat file for observing on IRTF.
;
;  Target list must have:
;     # at the beginning of any comment lines
;     Targets as the first NUM non-comment lines
;     name, ra, dec, mag   as the first four columns of the target lines
;                                 (columns are space-separated)
;              after the first four columns, you can put whatever you want.

;  If you include a line whose first characters are "DONE DONE DONE DONE", the
;     program will ignore all lines after this.
;     In this case, you can omit the NUM parameter.

;  example of target list:
;     Dropbox/panstarrs-BD/will/observing/jun26_plan.txt
;     Dropbox/GROUP/idl/share_wbest/jun26_plan.txt
;
;  HISTORY
;  Written by Will Best (IfA), 06/25/2012
;  06/27/12 (WB):  Added BASEPATH parameter.
;
;  INPUTS: 
;      DATE - String, to be used for file names.  Use e.g. 'jun26'
;             Observing plan/target list should be DATE_plan.txt
;      NUM - Number of targets to read into starcat file.
;      BASEPATH - Directory containing the input file
;
;  DEFAULT OUTPUT:
;      ~/Dropbox/panstarrs-BD/will/observing/DATE.starcat
;
;  EXAMPLE:
;      makestarcat, 'jun26', 15

if n_elements(basepath) eq 0 then basepath = '~/Dropbox/panstarrs-BD/will/observing/'

if n_params() lt 1 then begin
    print
    print, "   Use:  makestarcat, 'DATE' [, NUM, 'basepath']"
    print, "   DATE is a string used as a prefix for file names, e.g. 'jun26'"
    print, "   NUM is the number of lines to be read from the target list"
    print, "   BASEPATH is the directory containing the target list"
    print, "      Default for BASEPATH: "+basepath
    print, "          input file: BASEPATH/DATE_plan.txt"
    print, "          output file: BASEPATH/DATE.starcat"
    print
    print, "   Target list must have:"
    print, "      '#' at the beginning of any comment lines"
    print, "      Targets as the first NUM non-comment lines"
    print, "            OR"
    print, "         A line beginning with 'DONE DONE DONE DONE' after all the targets"
    print, "              (subsequent lines will be ignored)"
    print, "      name, ra, dec, mag   as the first four columns of the target lines"
    print, "                                  (columns are space-separated)"
    print
    print, "   example of target list:"
    print, "      Dropbox/GROUP/idl/share_wbest/jun26_plan.txt"
    print
    return
endif

; Primary target list
infile = basepath+date+'_plan.txt'
readcol, infile, name, ra, dec, mag, format='a,a,a,a', $
         comment='#', numline=num, /silent

; Find the end of the target list, if entered
done = where(name eq 'DONE')
if done ge 0 then begin
    name = name[0:done-1]
    ra = ra[0:done-1]
    dec = dec[0:done-1]
    mag = mag[0:done-1]
endif

; Make lines for the starcat file
stars = name+' '+ra+' '+dec+' 0.0 0.0 '+mag

; Write it
outfile = basepath+date+'.starcat'
forprint, stars, textout=outfile, format='(a)', /nocomment, /silent

END
