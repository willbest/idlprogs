PRO MAKEFETCHFINDERS, PROJECT, SAMPLE, FILEPREFIX=fileprefix, IDNO=idno, LIST=list, $
                 NAMEPREFIX=nameprefix, OUTPATH=outpath, RADIUS=radius, SILENT=silent

;  Create finder charts for a list of objects, using images from the FETCH and
;  PS1 image directories.
;
;  Currently includes these images:
;  PS1 y, 2MASS J, 2MASS H, 2MASS K.
; 
;  HISTORY
;  Written by Niall Deacon sometime around October, 2010.
;  Revised by Will Best (IfA), 07/09/2012
;  07/13/2013 (WB): Added red circle at center of images.
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      FILEPREFIX - Prefix for postscript finder chart file names.
;                   Default: 'wbest'
;      IDNO - If set, use the object's i.d. number (from the input list),
;             instead of coordinates, for the finder chart titles and file names.
;      LIST - ASCII file containing list of objects to make finder charts for.
;             Format: id, ra (degrees), dec (degrees)
;;;             Default: ~/Astro/PS1stamps/<project>/<sample>/<sample>.good
;             Default: /Volumes/Hickory/wbest/<project>/<sample>/<sample>.good
;      NAMEPREFIX - prefix for postscript finder chart file names.
;                   Default: 'wbest'
;      OUTPATH - where to put the postscript finder charts.
;                Default: ~/Astro/PS1stamps/finders/<project>/<sample>/
;      RADIUS - Maximum separation for FETCH and PS1 objects to match (arcsec).
;               Default = 3.0 arcsec
;      SILENT - Suppress screen outputs  (*** not yet functional ***)
;
;  e.g.
;  IDL> makefetchfinders, list='~/lttrans/goodlist.ascii', nameprefix='PS1+WISE ', /idno
;    Input list of objects is ~/lttrans/goodlist.ascii
;    Makes finder charts with filenames like 'wbest442.ps', 'wbest1000765.ps'
;    The charts have titles like 'PS1+WISE 442', 'PS1+WISE 1000765'
;    The files are placed in ~/Astro/PS1stamps/finders/getstamp/getstamp/
;
;  IDL> makefetchfinders, 'lttrans', 'big', outpath='~/finders/ltbig/', nameprefix='PS1+WISE ', fileprefix='PW'
;    Input list of objects is ~/Astro/PS1stamps/project/sample/sample.good
;    Makes finder charts with filenames like 'PW2314+5239.ps', 'PW0387-2256.ps'
;    The charts have titles like 'PS1+WISE 2314+5239', 'PS1+WISE 0387-2256'
;    The files are placed in ~/finders/ltbig/
;
;  CALLS
;      asinh_stretch
;      cutoutlite
;      fits_read (astrolib)
;      gcirc (astrolib)
;      invct (fenlib)
;      iterstat
;      ps_close (fenlib)
;      ps_open (fenlib)
;      pscutout2
;      readcol (astrolib)
;      tvscale (coyote)
;

; Prepare chart titles and file names
if n_elements(fileprefix) eq 0 then fileprefix = 'wbest'
if n_elements(nameprefix) eq 0 then nameprefix = 'wbest'

; Matching radius for FETCH and PS1 objects
if n_elements(radius) eq 0 then radius = 3.0     ; arcsec

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

; Prepare the image parameters
pos1 = [0.00, 0.58, 0.46, 0.90]
pos2 = [0.54, 0.58, 1.00, 0.90]
pos3 = [0.00, 0.19, 0.46, 0.51]
pos4 = [0.54, 0.19, 1.00, 0.51]
size=60.0    ;arcseconds
;SET_PLOT, 'PS'

; Set up the file paths
;; if n_elements(list) eq 0 then list = '~/Astro/PS1stamps/'+project+'/'+sample+'/'+sample+'.good'
;; if n_elements(outpath) eq 0 then outpath = '~/Astro/PS1stamps/finders/'+project+'/'+sample+'/'
;; if n_elements(list) eq 0 then list = '/Volumes/Hickory/wbest/'+project+'/'+sample+'/'+sample+'.good'
;; if n_elements(outpath) eq 0 then outpath = '/Volumes/Hickory/wbest/finders/'+project+'/'+sample+'/'
if n_elements(list) eq 0 then list = '/Volumes/Hanalei/wbest/'+project+'/'+sample+'/'+sample+'.good'
if n_elements(outpath) eq 0 then outpath = '/Volumes/Hanalei/wbest/finders/'+project+'/'+sample+'/'
spawn, 'mkdir -p '+outpath
;; fetchpath='~/Astro/PS1stamps/FETCH_files/'
;; pspath='~/Astro/PS1stamps/PS1_files/getstamp.'
;; fetchpath='~/Astro/699-1/'+project+'/'+sample+'id.ascii_FETCH/'
;; pspath='~/Astro/699-1/'+project+'/'+sample+'.'
;; fetchpath='/Volumes/Hickory/wbest/fetch/'+project+'/'+sample+'id.ascii_FETCH/'
;; pspath='/Volumes/Hickory/wbest/'+project+'/'+sample+'/'+sample+'.'
fetchpath='/Volumes/Hanalei/wbest/fetch/'+project+'/'+sample+'id.ascii_FETCH/'
pspath='/Volumes/Hanalei/wbest/'+project+'/'+sample+'/'+sample+'.'

; Get lists of FETCH image directories
spawn, 'ls -d '+fetchpath+'name*', fetchlist

; Get the target coordinates from the FETCH directory names
if not keyword_set(silent) then print, 'Obtaining FETCH object coordinates'
rafrgx = 'coord=[^ ]+ [^ ]+ [^ ]+ '        ; regular expression to look for
fpos = stregex(fetchlist, rafrgx, length=flen)
rafpos = fpos + 6
decfpos = fpos + flen
decfstop = strpos(fetchlist, ':arcsec')
rafstr = strmid(fetchlist, 1#rafpos, 1#decfpos-1#rafpos-1)
decfstr = strmid(fetchlist, 1#decfpos, 1#decfstop-1#decfpos)
raf = tenv(rafstr) * 15.
decf = tenv(decfstr)

; Read in the object i.d. and coordinates
readcol, list, F='l,d,d', id, ra2010, dec2010
n=n_elements(ra2010)

for i=0, n-1 do begin

; Get name and coordinates for the object
    sexo = adstring(ra2010[i], dec2010[i], 1)
    coordo = '!9a!X = '+strmid(sexo,1,11)+'   !9d!X = '+strmid(sexo,14,11)
    if keyword_set(idno) then nameo = trim(id[i]) else $
      nameo = strcompress(strmid(sexo,1,5)+strmid(sexo,14,6), /remove)    ;;;;;;;;;;;;;;;;;;
    print, 'Matching '+nameprefix+nameo+' to FETCH coordinates'

; Look for a match
    gcirc, 2, ra2010[i], dec2010[i], raf, decf, sep
    m = where(sep lt radius)

; What if there is more than one match?
    if n_elements(m) gt 1 then begin
        if not keyword_set(silent) then begin
            print, nameprefix+nameo+' has '+n_elements(m)+' matches!'
            print, fetchlist[m]
        endif
        m = where(sep eq min(sep))
    endif

; If match exists, make a finder chart!
    if m ge 0 then begin
        if not keyword_set(silent) then print, 'Found 1 match'
; Find the PS1 name for the object
        nstrt=strpos(fetchlist[m],'coord=')
        nstrt1=strpos(fetchlist[m],'name=id')
        name=strmid(fetchlist[m],(nstrt1+7),(nstrt-1-(nstrt1+7)))
; Find the images for the object
        spawn, 'ls ' + pspath + 'y/' + name + '_*' , psylist
        spawn, 'ls "' + fetchlist[m] + '/"*-j*.fits' , jlist
        spawn, 'ls "' + fetchlist[m] + '/"*-h*.fits' , hlist
        spawn, 'ls "' + fetchlist[m] + '/"*-k*.fits' , klist
; Get y image   
        print, 'y band'
        ydummyflag=0
        if ((rstrpos(psylist[0],'/') lt 0) or (n_elements(psylist) lt 1)) then begin
            ydummyflag=1
        endif else begin
            yflag1=0
            yindex=0
            while ((yflag1 lt 1) and (yindex lt n_elements(psylist))) do begin
                fits_read, psylist[yindex], mosaic, header
                mosaicsize=size(mosaic)
                print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
                centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
                iterstat,centre_mosaic,centre_statvec, /silent
                if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                    pscutout2, mosaic, header, cutouty, size, ra2010[i], dec2010[i]
                    yflag1 = 1
                    print, psylist[yindex]
                endif else yindex = yindex + 1
            endwhile
            if(yflag1 lt 1) then begin
                fits_read, psylist[0], mosaic, header
                pscutout2, mosaic, header, cutouty, size, ra2010[i], dec2010[i]
            endif
        endelse
; Get J image
        jdummyflag=0
        print, 'J band'
        if (rstrpos(jlist[0],'/') lt 0) then begin
            jdummyflag=1
        endif else begin
            cutoutlite, jlist[0], ra2010[i], dec2010[i], size, cutoutJ, cutoutheadJ, '2MASS'
        endelse
; Get H image
        hdummyflag=0
        print, 'H band'
        if (rstrpos(hlist[0],'/') lt 0) then begin
            hdummyflag=1
        endif else begin
            cutoutlite, hlist[0], ra2010[i], dec2010[i], size, cutoutH, cutoutheadH, '2MASS'
        endelse
; Get K image
        kdummyflag=0
        print, 'K band'
        if (rstrpos(klist[0],'/') lt 0) then begin
            kdummyflag=1
        endif else begin
            cutoutlite, klist[0], ra2010[i], dec2010[i], size, cutoutK, cutoutheadK, '2MASS'
        endelse
; Create the finder chart
        loadct, 0
        invct              ; Inverts the color table (black-on-white to white-on-black)
        ps_open, outpath+fileprefix+nameo, /color, /portrait, /ps_fonts
; Plot the image titles
        xyouts, (pos1[0]+pos1[2])/2., pos1[3]+0.01, 'PS1 y', color=255, align=0.5, chars=1.25, /normal
        xyouts, (pos2[0]+pos2[2])/2., pos2[3]+0.01, '2MASS J', color=255, align=0.5, chars=1.25, /normal
        xyouts, (pos3[0]+pos3[2])/2., pos3[3]+0.01, '2MASS H', color=255, align=0.5, chars=1.25, /normal
        xyouts, (pos4[0]+pos4[2])/2., pos4[3]+0.01, '2MASS K', color=255, align=0.5, chars=1.25, /normal
; Plot the scale bars
        xyouts, (pos1[0]+pos1[2])/2., pos1[1]-0.02, '<-------------------  1''  ------------------>', $
                color=255, align=0.5, chars=0.74, /normal
        xyouts, (pos2[0]+pos2[2])/2., pos2[1]-0.02, '<-------------------  1''  ------------------>', $
               color=255,  align=0.5, chars=0.74, /normal
; Plot the finder chart title and coordinates
        xyouts, 0.5, 1.0, nameprefix+nameo, color=255, align=0.5, charsize=2.25, /normal
        xyouts, 0.5, 0.96, coordo, color=255, align=0.55, charsize=1.75, /normal
; Plot the compass rose: N up, E left
        xyouts, 1.04, 1.04, 'N', color=255, charsize=0.9, /normal
        xyouts, 0.92, 0.955, 'E', color=255, charsize=0.8, /normal
        arrow, 1.05, 0.96, 0.95, 0.96, color=255, /normal, hsize=-0.2, thick=6
        arrow, 1.05, 0.96, 1.05, 1.03, color=255, /normal, hsize=-0.2, thick=6
; Plot the images
        if (ydummyflag ne 1) then begin
            tvimage, asinh_stretch(cutouty), position=pos1
        endif
        if (jdummyflag ne 1) then begin
            tvimage, asinh_stretch(cutoutJ), position=pos2
        endif
        if (hdummyflag ne 1) then begin
            tvimage, asinh_stretch(cutoutH), position=pos3
        endif
        if (kdummyflag ne 1) then begin
            tvimage, asinh_stretch(cutoutK), position=pos4
        endif
; Plot the circles
        lincolr_wb, /silent
        if (ydummyflag ne 1) then begin
            tvcircle, 0.03, 0.23, 0.74, /data, thick=8, color=3
        endif
        if (jdummyflag ne 1) then begin
            tvcircle, 0.03, 0.77, 0.74, /data, thick=8, color=3
        endif
        if (hdummyflag ne 1) then begin
            tvcircle, 0.03, 0.23, 0.35, /data, thick=8, color=3
        endif
        if (kdummyflag ne 1) then begin
            tvcircle, 0.03, 0.77, 0.35, /data, thick=8, color=3
        endif
        ps_close
    endif else begin
        if not keyword_set(silent) then print, 'No matches found!'
    endelse

endfor

END
