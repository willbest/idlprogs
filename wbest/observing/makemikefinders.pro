PRO MAKEMIKEFINDERS, PROJECT, SAMPLE, FILEPREFIX=fileprefix, IDNO=idno, LIST=list, $
                 NAMEPREFIX=nameprefix, OUTPATH=outpath, RADIUS=radius, SILENT=silent

;  Create finder charts for a list of objects, using images from the FETCH and
;  PS1 image directories.
;
;  Currently includes these images:
;  PS1 y, 2MASS J, 2MASS H, 2MASS K.
; 
;  HISTORY
;  Written by Niall Deacon sometime around October, 2010.
;  Revised by Will Best (IfA), 07/09/2012
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      FILEPREFIX - Prefix for postscript finder chart file names.
;                   Default: 'wbest'
;      IDNO - If set, use the object's i.d. number (from the input list),
;             instead of coordinates, for the finder chart titles and file names.
;      LIST - ASCII file containing list of objects to make finder charts for.
;             Format: id, ra (degrees), dec (degrees)
;             Default: ~/Astro/PS1stamps/<project>/<sample>/<sample>.good
;      NAMEPREFIX - prefix for postscript finder chart file names.
;                   Default: 'wbest'
;      OUTPATH - where to put the postscript finder charts.
;                Default: ~/Astro/PS1stamps/<project>/<sample>/
;      RADIUS - Maximum separation for FETCH and WISE objects to match (arcsec).
;               Default = 3.0 arcsec
;      SILENT - Suppress screen outputs  (*** not yet functional ***)
;
;  e.g.
;  IDL> makefinders, list='~/lttrans/goodlist.ascii', nameprefix='PS1+WISE ', /idno
;    Input list of objects is ~/lttrans/goodlist.ascii
;    Makes finder charts with filenames like 'wbest442.ps', 'wbest1000765.ps'
;    The charts have titles like 'PS1+WISE 442', 'PS1+WISE 1000765'
;    The files are placed in ~/Astro/PS1stamps/finders/getstamp/getstamp/
;
;  IDL> makefinders, lttrans, big, outpath='~/finders/ltbig/', nameprefix='PS1+WISE ', fileprefix='PW'
;    Input list of objects is ~/Astro/PS1stamps/project/sample/sample.good
;    Makes finder charts with filenames like 'PW2314+5239.ps', 'PW0387-2256.ps'
;    The charts have titles like 'PS1+WISE 2314+5239', 'PS1+WISE 0387-2256'
;    The files are placed in ~/Astro/PS1stamps/finders/ltbig/
;
;  CALLS
;      asinh_stretch
;      cutoutlite
;      fits_read (astrolib)
;      gcirc (astrolib)
;      invct (fenlib)
;      iterstat
;      ps_close (fenlib)
;      ps_open (fenlib)
;      pscutout2
;      readcol (astrolib)
;      tvscale (coyote)
;

; Prepare the image parameters
pos1 = [0.00, 0.58, 0.46, 0.90]
pos2 = [0.54, 0.58, 1.00, 0.90]
pos3 = [0.00, 0.19, 0.46, 0.51]
pos4 = [0.54, 0.19, 1.00, 0.51]
size=60.0    ;arcseconds
SET_PLOT, 'PS'
loadct, 0
invct          ; Inverts the color table (black-on-white to white-on-black)

; Set up the file paths
outpath = '~/Desktop/copy/'
wpath = '~/Desktop/copy/'
pspath = '~/Desktop/copy/'
namep = '27477'
namew = '2974'
ra2010 = 297.88759
dec2010 = 27.048298

; Find the images for the object
spawn, 'ls ' + pspath + namep + '_*' , psylist
spawn, 'ls ' + wpath + namew +'*'+'w1*.fits' , w1list
spawn, 'ls ' + wpath + namew +'*'+'w2*.fits' , w2list
spawn, 'ls ' + wpath + namew +'*'+'w3*.fits' , w3list

; Get y image   
print, 'y band'
fits_read, psylist[0], mosaic, header
pscutout2, mosaic, header, cutouty, size, ra2010, dec2010

; Get W1 image
print, 'W1 band'
cutoutlite, w1list, ra2010, dec2010, size, cutoutW1, cutoutheadW1, 'WISE'

; Get W2 image
print, 'W2 band'
cutoutlite, w2list, ra2010, dec2010, size, cutoutW2, cutoutheadW2, 'WISE'

; Get W3 image
print, 'W3 band'
cutoutlite, w3list, ra2010, dec2010, size, cutoutW3, cutoutheadW3, 'WISE'

; Create the finder chart
ps_open, outpath+namep+'stamps', /color, /portrait, /ps_fonts
; Plot the image titles
xyouts, (pos1[0]+pos1[2])/2., pos1[3]+0.01, 'PS1 y', color=255, align=0.5, chars=1.25, /normal
xyouts, (pos2[0]+pos2[2])/2., pos2[3]+0.01, 'WISE W1', color=255, align=0.5, chars=1.25, /normal
xyouts, (pos3[0]+pos3[2])/2., pos3[3]+0.01, 'WISE W2', color=255, align=0.5, chars=1.25, /normal
xyouts, (pos4[0]+pos4[2])/2., pos4[3]+0.01, 'WISE W3', color=255, align=0.5, chars=1.25, /normal
; Plot the scale bars
xyouts, (pos1[0]+pos1[2])/2., pos1[1]-0.02, '<-------------------  1''  ------------------>', $
        color=255, align=0.5, chars=0.74, /normal
xyouts, (pos2[0]+pos2[2])/2., pos2[1]-0.02, '<-------------------  1''  ------------------>', $
        color=255,  align=0.5, chars=0.74, /normal
; Plot the finder chart title and coordinates
xyouts, 0.5, 1.0, 'id'+namep, color=255, align=0.5, charsize=2.25, /normal
; Plot the compass rose: N up, E left
xyouts, 1.04, 1.04, 'N', color=255, charsize=0.9, /normal
xyouts, 0.92, 0.955, 'E', color=255, charsize=0.8, /normal
arrow, 1.05, 0.96, 0.95, 0.96, color=255, /normal, hsize=-0.2, thick=6
arrow, 1.05, 0.96, 1.05, 1.03, color=255, /normal, hsize=-0.2, thick=6
; Plot the images
tvimage, asinh_stretch(cutouty), position=pos1
tvimage, asinh_stretch(cutoutW1), position=pos2
tvimage, asinh_stretch(cutoutW2), position=pos3
tvimage, asinh_stretch(cutoutW3), position=pos4

ps_close

END
