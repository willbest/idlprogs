PRO IRTF_ENDNIGHT, GNUM, DATE, ENGINEERING=engineering

;+
;  Does end-of-night procedures for IRTF data:
;     - Copy Bigdog and Guidedog image files from stefan (IRTF server) 
;       to bigmac:~mliu/data-irtf/
;     - Create folders on bigmac for the above data, as needed 
;     - Set permissions for these folders to world-readable and writeable.
;
;  USE
;      The program is designed only to run on bigmac.
;        Repeat: RUN THIS ON BIGMAC.
;      But it can be run from any directory on BIGMAC, 
;        doesn't need to be in the target data directory.
;
;      irtf_endnight, GNUM [, 'DATE', /ENGINEERING]
;
;      e.g. irtf_endnight, 8, /engineering
;      e.g. irtf_endnight, 13, '03oct12'
;
;      The program will read the date and time from the system.  It will convert
;      HST to UT, and handle last day of the month, etc.  
;
;  INPUT 
;      GNUM - Guest account number on IRTF.  e.g. 8
;
;  OPTIONAL INPUT
;      DATE - Optional string, to be used for file names.  Use e.g. '26jun12'
;             If not entered, will use the current date on the machine.
;
;  KEYWORDS 
;      ENGINEERING - Set this for engineering time, so resulting directory name contains "eng".
;
;  FUTURE ENHANCEMENTS
;      Automatically change 'filename' prefix to 'spc' and 'img' if needed.
;
;  CALLS
;      trim.pro
;
;  HISTORY
;  Written by Will Best (IfA), 09/24/2012
;  09/26/2012 (WB): Asks "Press any key" after copying Bigdog files, before
;                     copying Guidedog files.  Without this, the program will
;                     automatically prompt for the guest account password, and
;                     will time out after a minute if the user doesn't
;                     notice, breaking the routine.
;  10/02/2012 (WB): Fixed issues with getting the date from systime().
;                   GNUM is now entered as a number, not a string.
;  10/07/2012 (ML): Checks that program is running on BIGMAC.
;                   Minor code & comment cleanup - uses 'DATADIR' variable to store path, etc.
;-


; data directory on BIGMAC
DATADIR = '~mliu/data-irtf/'
;DATADIR = '/Volumes/nene/data-irtf/'


if n_params() lt 1 then begin
    print
    print, "   Use:  irtf_endnight, GNUM [, 'DATE', /ENGINEERING]"
    print
    print, "         GNUM is your guest account number on IRTF, e.g. 8"
    print, "         DATE is a string used for file names, e.g. '26jun12'"
    print, "            Be sure to use the UT date!"
    print
    return
endif


; check if running on BIGMAC
spawn, 'echo $HOST', out
if strpos(out, 'bigmac') eq -1 then begin
   message, 'You are currently running on "'+out+'"', /info
   message, 'This program can only be run on BIGMAC.', /info
   return
endif


; Turn the guest account number into a string
gnum = trim(gnum)
if strlen(gnum) eq 1 then gnum = '0'+gnum

; Get the date
if n_elements(date) gt 0 then begin
    day = strmid(date, 0, 2)
    month = strlowcase(strmid(date, 2, 3))
    year = strmid(date, 5, 2)

endif else begin
    ; Prep to get date info from the system time
    mlist = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']
    dlist = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    ; Get the information from systime()
    date = systime()
    hour = fix(strmid(date, 11, 2))
    day = fix(strmid(date, 8, 2))
    month = strlowcase(strmid(date, 4, 3))
    year = fix(strmid(date, 22, 2))
    if (year mod 4 eq 0) and (year ne 0) then dlist[1] = 29   ; Leap years!

    ; Establish the correct day of the month (UT)
    if hour ge 14 then day = day + 1         ; set for HST

    ; Establish the correct month and year (UT)
    m = where(mlist eq month)
    if day gt dlist[m] then begin
        day = 1
        m = (m+1) mod 12
        month = mlist[m]
        month = month[0]
        if m eq 0 then year = (year+1) mod 100
    endif

    ; Make the day & year strings
    day = trim(day)
    if strlen(day) eq 1 then day = '0'+day
    year = trim(year)
    if strlen(year) eq 1 then year = '0'+year

endelse

; Make directories
root = 'spex.'+year+month
if keyword_set(engineering) then root = root+'-eng'
big = month+day+'.spc'
guide = month+day+'.img'

cd, DATADIR
;cd, '~mliu/data-irtf/'
spawn, 'mkdir -p '+root+'/'+big
spawn, 'mkdir '+root+'/'+guide

; Copy files
stef = 'guest'+gnum+'@stefan.ifa.hawaii.edu:'
bpath = '/scrs1/bigdog/guest'+gnum+'/'+day+month
gpath = '/scrs1/guidedog/guest'+gnum+'/'+day+month

cd, root
print, 'Copying bigdog files.'
spawn, 'scp -r '+stef+bpath+'/\* '+big

cd, DATADIR+root
;cd, '~mliu/data-irtf/'+root
print, 'Press any key to copy guidedog files (will have to type password).'
dummy=get_kbrd()
spawn, 'scp -r '+stef+gpath+'/\* '+guide

; Set permissions
;; cd, '~mliu/data-irtf/'+root
;; print, 'Setting permissions'
;; spawn, 'chmod -R a+rw '+big
;; spawn, 'chmod -R a+rw '+guide

cd, DATADIR
;cd, '~mliu/data-irtf/'
print
print, 'Setting permissions on new directories:'
print, '  ', root+'/'+big
print, '  ', root+'/'+guide
spawn, 'chmod -R a+rw '+root

print
print, 'Done!'
print

END
