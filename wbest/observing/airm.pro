PRO airm, infile, month, day, year, $
          amrange=amrange,  $
          nolabel=nolabel, color=color, $
          noerase=noerase, $
          _extra = _extra

;+
; Program to calculate airmass at MKO
;
; Requires file named "source_list" for input data.
; Takes a maximum of 10 objects for each plot.
; Example format: 
;    NGC_1333   03 29 00.0  +31 17 00
;    IC_348     03 44 00.0  +32 10 00
;    Taurus     04 19 00.0  +28 20 00
; Tokunaga, 25 Nov. 1999
;
; hacked:
; - use LINCOLR for color plotting
; - date & file name are passed as parameters
; - makes color PS file --> doesn't work
; 12/02/03 M. Liu
;
; added 'amrange' and 'timerange' params - controls plotted range
; added 'color' - to plot in only one color (useful for multiple calls)
; added /noerase (as a side effect, this will turn off making of Postscript file)
; 09/26/05 MCL
;-

if n_params() ne 4 then begin
    print, 'pro airm, infile, month, day, year, amrange=[3.0,1.0]'
    return
endif
if not(keyword_set(amrange)) then amrange = [3.0, 0.95]

date = [day, month, year]
;date = fltarr(3)
;;read, date, prompt='input HST date [day, month, year]'
;print='MANUAL INPUT FOR HST DATE'
;;------------------------------------------------------------
;date=[02, 12, 2003]    ; Manual input for HST date [dd,mm,yyyy]
;;------------------------------------------------------------

da = date[0]
mo = date[1]
yr = date[2]
date = strcompress(string(fix(da)), /remove_all) + '-' + $
   strcompress(string(fix(mo)), /remove_all) + '-' + $
   strcompress(string(fix(yr)), /remove_all)
hr = 18.     ; start at 6PM HST 

; Read in objects.

fmt = 'A,F,F,F,F,F,F'
; 'readCol' is a Goddard library procedure.
;------------------------------------------------------------
readCol2,infile,F=fmt, name,hour,min,sec,deg,dmin,dsec
;readCol,'sources_list',F=fmt, name,hour,min,sec,deg,dmin,dsec
;------------------------------------------------------------
nitems = n_elements(name)

lat = 0.349066  ; = 19d 49.6m in radians, MKO latitude
lng = 2.713493  ; = 155.4717 deg in radians, MKO longitude
pi = !pi

; 'ten' is a Goddard library procedure.s
ra = ten(hour[0],min[0],sec[0])*15.*pi/180.
dec = ten(deg[0],dmin[0],dsec[0])*pi/180.

;print, name[0], hour[0],min[0],sec[0], deg[0],dmin[0],dsec[0]

; Compute 4 points per hour for 12 hours, for up to 10 objects.
am = fltarr(48,nitems)
;am = fltarr(48,10)   ; removed 09/26/05
time = fltarr(48)
jul1 = dblarr(48)
count = -0.25

jdcnv, yr, mo, da, hr, jul
;jdcnv, 1999, mo, da, hr, jul
; convert to julian date for Universal Time
jul = jul + 0.416667
jul0 = jul

  FOR j=0,47 DO BEGIN
    ; 'airmass' is a Buie library procedure.
    am(j,0) = airmass(jul,ra,dec,lat,lng)
    jul1(j) = jul
    ; Advance 0.25 hour
    jul = jul + 0.25/24.  
    hr = hr + 0.25
    count = count + 0.25
    time(j) = count
  ENDFOR

; Define time to start at 18 hours HST
time = time + 18.0

; Plot the first one.
; Load color table.
  lincolr, /silent
; loadCT, 12     ; 16-level color table.


;window, 0

label = ['18', '20', '22', '24', '2', '4', '6']


; make the box
plot, time, am(*,0), xr = xr, yRange=amrange, /yStyle, xTickLen=1, yTickLen=1, $
      xGridStyle=1, yGridStyle=1, xTickName=label, charSize=1.3, $
      xTitle='HST,'+'  '+date, yTitle='airmass', /nodata, $
      noerase=noerase, _extra = _extra

if keyword_set(color) then pcolor = color $
  else pcolor = !p.color
oplot, time, am(*,0), col=pcolor  ; white color   
;oplot, time, am(*,0), color=255  ; white color   
z = where(am(*,0) LT amrange(0), nz)
if (nz gt 0) and not(keyword_set(nolabel)) then $
  xyOutS, time[z[0]]+0.1, am[z[0],0]-0.05, name[0], color=pcolor, _extra = _extra
;xyOutS, time[z[0]]+0.1, am[z[0],0]-0.05, name[0], color=255

; Plot the others.

  FOR j=1,nitems-1  DO BEGIN

  ra = ten(hour[j],min[j],sec[j])*15.*pi/180.
  dec = ten(deg[j],dmin[j],dsec[j])*pi/180.
  
;  print, name[j], hour[j],min[j],sec[j], deg[j],dmin[j],dsec[j]

    FOR k=0,47 DO BEGIN
      am(k,j) = airmass(jul1(k),ra,dec,lat,lng)
    ENDFOR
    
    m = j mod 4    ; Multiplying factor to space out names.
    
      CASE m OF
          0 : c = !p.color      ; white
          1 : c = 7             ; blue
          2 : c = 3             ; red
          3 : c = 2             ; green
          ;0 : c = 255  ; white
          ;1 : c = 79   ; blue
          ;2 : c = 165  ; red
          ;3 : c = 15   ; green
          ELSE :
      ENDCASE
      if keyword_set(color) then c = color

  oplot, time, am(*,j), lineStyle=j*(not(keyword_set(color))), color=c
  ;oplot, time, am(*,j), lineStyle=j, color=c
  z = where(am(*,j) LT amrange(0), nz)
  if (nz gt 0) and not(keyword_set(nolabel)) then $
    xyOutS, time[z[0]]+m*0.2, am[z[0],j]-m*0.1, name[j], color=c, _extra = _extra

  ENDFOR


;------------------------------------------------------------
; if running in /noerase mode, then don't make PS file
if keyword_set(noerase) then $
    return

; ---------------------------------------------------------------------
; Make postscript file.
;
thisDevice = !D.Name
Set_Plot, 'PS'
Device, Filename='airmass_plot.ps', /Landscape, XSize=8, Ysize=6, /Inches, /color
      
loadct, 12
;lincolr

; Plot first one.     
plot, time, am(*,0), yRange=[3,0.95], /yStyle, xTickLen=1, yTickLen=1, $
      xGridStyle=1, yGridStyle=1, xTickName=label, charSize=1.3, $
      xTitle='HST,'+'  '+date, yTitle='airmass', /nodata

if keyword_set(color) then pcolor = color $
  else pcolor = !p.color
oplot, time, am(*,0), color=!p.color ;, color=255  ; white color   
z = where(am(*,0) LT amrange(0))
if not(keyword_set(nolabel)) then $
  xyOutS, time[z[0]]+0.1, am[z[0],0]-0.05, name[0], charSize=0.8, _extra = _extra  ;, color=255

; Plot the others.

  FOR j=1,nitems-1  DO BEGIN

  ra = ten(hour[j],min[j],sec[j])*15.*pi/180.
  dec = ten(deg[j],dmin[j],dsec[j])*pi/180.
  
;  print, name[j], hour[j],min[j],sec[j], deg[j],dmin[j],dsec[j]

    FOR k=0,47 DO BEGIN
      am(k,j) = airmass(jul1(k),ra,dec,lat,lng)
    ENDFOR
    
    m = j mod 4    ; Multiplying factor to space out names.
  
    CASE m OF
        0 : c = 255             ; white
        1 : c = 79              ; blue
        2 : c = 165             ; red
        3 : c = 15              ; green
        ELSE :
    ENDCASE
    if keyword_set(color) then c = color

  oplot, time, am(*,j), lineStyle=j*(not(keyword_set(color)))    ; , color=c
  ;oplot, time, am(*,j), lineStyle=j    ; , color=c
  z = where(am(*,j) LT amrange(0), nz)
  if (nz gt 0) and not(keyword_set(nolabel)) then $
    xyOutS, time[z[0]]+m*0.2, am[z[0],j]-m*0.1, name[j], charSize=0.8, _extra = _extra ; , color=c

  ENDFOR

Device, /Close_File
Set_Plot, thisDevice


END
