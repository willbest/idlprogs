FUNCTION LTBUILD_MASS, NSTARS, MMIN, MMAX, NSIMS, $
                       LOGNORMAL=lognormal, INDEX=index, MCRIT=mcrit, SILENT=silent

;  Generates masses for stellar objects, distributed according to one of
;  three distributions: simple power low, two-part power law, or lognormal (not
;  yet implemented).
;
;  HISTORY
;  Written by Will Best (IfA), 05/2012
;
;  INPUTS
;      NSTARS - Number of stars per simulation to return a mass for.
;      MMIN - Minimum mass of stars to be generated.
;      MMAX - Maximum mass of stars to be generated.
;      NSIMS - The number of simulations being run before aggregating results.
;
;  KEYWORDS:
;      INDEX - If set, the simulation will use a power-law IMF of the form
;                  dN/dm ~ m^(-a)
;              If INDEX is a single value, this value is used for a.
;              If INDEX is a two-element vector, then a two-part power law will
;              be used, with a=INDEX[0] below a critical mass, and a=INDEX[1]
;              above the critical mass.  The critical mass is set by the MCRIT
;              keyword.  If MCRIT is not set, 0.05 Msun will be used.
;           ---> Allen et al (2005) use 0.09 Msun
;              If INDEX is not used, the distribution will be lognormal.
;              If /LOGNORMAL is set, it wins and the value of INDEX is ignored.
;      LOGNORMAL - If set, the simulation will use a log-normal IMF of the form
;                      phi(log m) ~ exp( -(log m - log mcrit)^2 / (2sigma^2) )
;                  The critical mass is set by the MCRIT keyword.  If MCRIT is not
;                  set, 0.25 Msun will be used.
;                  The spread (sigma) is set by the MSIGMA keyword.  If MSIGMA is not
;                  set, 0.5 dex will be used.
;      MCRIT - Critical mass to be used with the model IMF.
;              If INDEX is a two-element vector (two-part power law), MCRIT has
;              a default value of 0.05 Msun.
;              If LOGNORMAL is set, MCRIT has a default value of 0.25 Msun.
;      SILENT - Suppress outputs to screen.
;

if n_params() lt 4 then begin
    message, 'You must provide values for the minimum and maximum masses, and nsims.'
endif

nind = n_elements(index)

ntot = nstars*nsims

; Mass from log-normal IMF
if keyword_set(lognormal) or (nind eq 0) then begin
    if not keyword_set(silent) then print, 'A log-normal IMF will be used.'
    if not keyword_set(mcrit) then mcrit = 0.25                        ; Msun
    if not keyword_set(msigma) then msigma = 0.5                       ; Msun
;log-normal stuff goes here
;    x = 
;    nind = 3
endif else begin

; Mass from power law IMF
    case nind of
        1 : begin
            if not keyword_set(silent) then print, $
              'A simple power law IMF with index '+trim(index)+' will be used.'
            randomp, x, -index, ntot, range_x=[mmin, mmax]
;            x = alog10(reform[temporary(x), nstars, nsims])
            x = alog10(temporary(x))
        end

        2 : begin
            if not keyword_set(silent) then print, 'A two-part power law IMF will be used.'
          ; This assumes the power law is continuous from one part to the other.
            if not keyword_set(mcrit) then mcrit = 0.05
            i01 = -index[0] + 1.
            i11 = -index[1] + 1.
          ; arat is ratio of areas
            arat = i01/i11 * (mmax^i11 - mcrit^i11) / mcrit^(i11-i01) / (mcrit^i01 - mmin^i01)
;            ncrit = long(nstars/(arat + 1))
            ncrit = long(ntot/(arat + 1))
            randomp, x, -index[0], ncrit, range_x=[mmin, mcrit]
            randomp, x1, -index[1], ntot-ncrit, range_x=[mcrit, mmax]
;            x = [reform(alog10(temporary(x)), ncrit, nsims), $
;                         reform(alog10(temporary(x1)), ntot-ncrit, nsims)]
;            x = reform([reform(temporary(x), ncrit, nsims), $
;                                reform(temporary(x1), ntot-ncrit, nsims)], ntot)
            x = reform([reform(alog10(temporary(x)), ncrit, nsims), $
                        reform(alog10(temporary(x1)), ntot-ncrit, nsims)], ntot)
        end
        else : message, 'INDEX must be either a single value or a two-element vector.'
    endcase
endelse

return, x

END
