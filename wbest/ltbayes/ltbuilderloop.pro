;ps=1
points=1
index=.25
nsims=1
density=0.011
radius=275.4
area=2026/41253.
silent=1
;binary=0.
;PRO LTBUILDER, NSIMS=nsims, DENSITY=denisty, RADIUS=radius, MMIN=mmin, MMAX=mmax, $
;               INDEX=index, LOGNORMAL=lognormal, MCRIT=mcrit, MSIGMA=msigma, $
;               BINARY=binary, SFH=sfh, AREA=area, _EXTRA=extrakey, $
;               SILENT=silent

;  Runs Monte Carlo simulations of L-T transition dwarfs in the Solar
;  neighborhood.
;
;  HISTORY
;  Begun by Will Best (IfA), 05/24/2012
;
;  CALLING SEQUENCE: 
;     
;
;  OPTIONAL KEYWORD INPUTS:
;      AREA - Fraction of the full sky to simulate.
;             Default: 1 (whole sky)
;      BINARY - Fraction of stars that are binaries.
;               Default: 0.15
;      DENSITY - The number density of brown dwarfs in the Solar
;                neighborhood, in pc^-3.  Default value is 0.01 pc^-3.
;      INDEX - If set, the simulation will use a power-law IMF of the form
;                  phi(m) ~ m^(-a)
;              If INDEX is a single value, this value is used for a.
;              If INDEX is a two-element vector, then a two-part power law will
;              be used, with a=INDEX[0] below a critical mass, and a=INDEX[1]
;              above the critical mass.  The critical mass is set by the MCRIT
;              keyword.  If MCRIT is not set, 0.05 Msun will be used.
;           ---> Allen et al (2005) use 0.09 Msun
;              If /LOGNORMAL is set, the value of INDEX is ignored.
;      LOGNORMAL - If set, the simulation will use a log-normal IMF of the form
;                      phi(log m) ~ exp( -(log m - log mcrit)^2 / (2sigma^2) )
;                  The critical mass is set by the MCRIT keyword.  If MCRIT is not
;                  set, 0.25 Msun will be used.
;                  The spread (sigma) is set by the MSIGMA keyword.  If MSIGMA is not
;                  set, 0.5 dex will be used.
;      MCRIT - Critical mass to be used with the model IMF.
;              If INDEX is a two-element vector (two-part power law), MCRIT has
;              a default value of 0.05 Msun.
;              If LOGNORMAL is set, MCRIT has a default value of 0.25 Msun.
;      MMAX - Maximum mass of stars to be generated.  Default is 0.1 Msun.
;      MMIN - Minimum mass of stars to be generated.  Default is 0.01 Msun.
;      MSIGMA - Spread for a log-normal IMF.  If LOGNORMAL is set, MSIGMA has a
;               default value of 0.5 dex.
;      NSIMS - The number of simulations to run before aggregating results.
;              Default is 100.
;      RADIUS - The radius of the simulated solar neighborhood, in parsecs.
;               The program rounds RADIUS down to the nearest (long) integer.
;               Default is 100 pc.
;      SFH - Slope to be used for the star formation history function, which has
;            the form  P(age) = slope*age + P(0)
;            Default value is 0 Gyr^-1 (i.e. constant star formation rate over time).
;      SILENT - Suppress outputs to screen.
;

if not keyword_set(silent) then print, systime()

minspt = 6
maxspt = 28

; Model for interpolating luminosities and temperatures
; Saumon & Marley (2008) hybrid
modelpath = '~/idlprogs/wbest/ltbayes/hybrid_isochrones.dat'
modeltags = 'age:f, mass:f, Teff:f, lum:f'

; Set up parameters, use default values as needed
if not keyword_set(density) then density = 0.01                        ; pc^-3
if not keyword_set(radius) then radius = 100                           ; pc
if not keyword_set(area) then area = 1.                                ; fraction of full sky
nstars = long(4./3*!pi*long(radius)^3)
    ; Is long big enough? Max value is 2,147,483,648.  Could use long64.
nstars = nstars * area
if not keyword_set(nsims) then nsims = 100
nsims = long(nsims)
ntot = nstars*nsims

if not keyword_set(mmax) then mmax = 0.1                               ; Msun
if not keyword_set(mmin) then mmin = 0.01                              ; Msun
if mmax lt mmin then swap, mmax, mmin

; Make structure to contain all the simulation data
    ; binaries: in sims.bin, the value is the index in a second structure that
    ;    contains the data on binaries.  Default value for sims.binary is -1.
if not keyword_set(silent) then print, 'Creating the data structure'
; This line sets up an array of #nsims structures.  Each tag contains #nstars values.
;sims = replicate({dist:fltarr(nstars), mass:fltarr(nstars), age:fltarr(nstars), bin:lonarr(nstars), $
;                  lum:fltarr(nstars), Teff:fltarr(nstars), SpT:fltarr(nstars), y:replicate(-999.,nstars), $
;;                  i:replicate(-999.,nstars), z:replicate(-999.,nstars), $
;                  W1:replicate(-999.,nstars), W2:replicate(-999.,nstars), $
;                  W3:replicate(-999.,nstars)}, nsims)
; This line sets up single structure.  Each tag contains #ntot values.
;; sims = {dist:fltarr(ntot), mass:fltarr(ntot), age:fltarr(ntot), bin:lonarr(ntot), $
;;         lum:fltarr(ntot), Teff:fltarr(ntot), SpT:fltarr(ntot), M_y:replicate(-999.,ntot), $
;; ;        M_i:replicate(-999.,ntot), M_z:replicate(-999.,ntot), $
;;         M_W1:replicate(-999.,ntot), M_W2:replicate(-999.,ntot), M_W3:replicate(-999.,ntot), $
;;         y:replicate(-999.,ntot), $;i:replicate(-999.,ntot), z:replicate(-999.,ntot), $
;;         W1:replicate(-999.,ntot), W2:replicate(-999.,ntot), W3:replicate(-999.,ntot) }

sims = {dist:fltarr(ntot), mass:fltarr(ntot), age:fltarr(ntot), bin:lonarr(ntot), $
        lum:fltarr(ntot), Teff:fltarr(ntot), SpT:fltarr(ntot), M_y:replicate(!VALUES.F_NAN,ntot), $
;        M_i:replicate(!VALUES.F_NAN,ntot), M_z:replicate(!VALUES.F_NAN,ntot), $
        M_J2:replicate(!VALUES.F_NAN,ntot), M_H2:replicate(!VALUES.F_NAN,ntot), M_K2:replicate(!VALUES.F_NAN,ntot), $
        M_W1:replicate(!VALUES.F_NAN,ntot), M_W2:replicate(!VALUES.F_NAN,ntot), $
        M_W3:replicate(!VALUES.F_NAN,ntot), y:replicate(!VALUES.F_NAN,ntot), $
;        i:replicate(!VALUES.F_NAN,ntot), z:replicate(!VALUES.F_NAN,ntot), $
        J2:replicate(!VALUES.F_NAN,ntot), H2:replicate(!VALUES.F_NAN,ntot), K2:replicate(!VALUES.F_NAN,ntot), $
        W1:replicate(!VALUES.F_NAN,ntot), W2:replicate(!VALUES.F_NAN,ntot), $
        W3:replicate(!VALUES.F_NAN,ntot) }



runs=1000
ltnums = intarr(runs,16)
for jj=0, runs-1 do begin
print, 'Run '+trim(jj+1)

; Generate the distance for each star -- for uniform distribution in a
;     volume, use a P(r) ~ r^2 probability distribution.
if not keyword_set(silent) then print, 'Generating random distances'
randomp, x, 2, ntot, range_x=[0,radius]
;sims.dist = reform[temporary(x), nstars, nsims]
sims.dist = temporary(x)      ; temporary() saves memory!


; Generate masses
if not keyword_set(silent) then print, 'Generating random masses'
ii = n_elements(index)
; Mass from log-normal IMF
if keyword_set(lognormal) then begin
    if not keyword_set(silent) then print, 'A log-normal IMF will be used.'
    if not keyword_set(mcrit) then mcrit = 0.25                        ; Msun
    if not keyword_set(msigma) then msigma = 0.5                       ; Msun
;log-normal stuff goes here
;    sims.mass = 
;    ii = 3
endif else begin
; Mass from power law IMF
    case ii of
        0 : begin
            print, 'You must either set a value or two-element vector for INDEX,'
            message, 'or set the LOGNORMAL keyword.'
        end
        1 : begin
            if not keyword_set(silent) then print, 'A simple power law IMF will be used.'
            randomp, x, -index, ntot, range_x=[mmin, mmax]
;            sims.mass = reform[temporary(x), nstars, nsims]
;            sims.mass = (temporary(x))
            sims.mass = alog10(temporary(x))
        end
        2 : begin
            if not keyword_set(silent) then print, 'A two-part power law IMF will be used.'
          ; This assumes the power law is continuous from one part to the other.
            if not keyword_set(mcrit) then mcrit = 0.25
            i01 = -index[0] + 1.
            i11 = -index[1] + 1.
          ; arat is ratio of areas
            arat = i01/i11 * (mmax^i11 - mcrit^i11) / mcrit^(i11-i01) / (mcrit^i01 - mmin^i01)
;            ncrit = fix(nstars/(arat + 1))
            ncrit = long(ntot/(arat + 1))
            randomp, x, -index[0], ncrit, range_x=[mmin, mcrit]
            randomp, x1, -index[1], ntot-ncrit, range_x=[mcrit, mmax]
;            sims.mass = [reform(alog10(temporary(x)), ncrit, nsims), $
;                         reform(alog10(temporary(x1)), ntot-ncrit, nsims)]
;            sims.mass = reform([reform(temporary(x), ncrit, nsims), $
;                                reform(temporary(x1), ntot-ncrit, nsims)], ntot)
            sims.mass = reform([reform(alog10(temporary(x)), ncrit, nsims), $
                                reform(alog10(temporary(x1)), ntot-ncrit, nsims)], ntot)
        end
        else : message, 'INDEX must be either a single value or a two-element vector.'
    endcase
endelse


; Generate the age of each star
if not keyword_set(silent) then print, 'Generating random ages'
if not keyword_set(sfh) then sfh = 0
if sfh eq 0 then begin
; Constant star formation rate
;    sims.age = alog10(10*randomu(seed, nstars, nsims))
;    sims.age = 10*randomu(seed, ntot)                          ; log Gyr
    sims.age = alog10(10*randomu(seed, ntot))                          ; log Gyr
endif else begin
; Linear star formation rate function
    if sfh lt 0 then p0 = sfh * (-10.) else p0 = 0.
;    sims.age = alog10((-p0 + sqrt(p0^2 + 2.*sfh*randomu(seed, nstars, nsims))) / sfh)
;    sims.age = (-p0 + sqrt(p0^2 + 2.*sfh*randomu(seed, ntot))) / sfh   ; log Gyr
    sims.age = alog10((-p0 + sqrt(p0^2 + 2.*sfh*randomu(seed, ntot))) / sfh)   ; log Gyr
endelse


; Binarity
if not keyword_set(silent) then print, 'Generating binary parameters, masses'
if n_elements(binary) eq 0 then binary = 0.15
if binary lt 0 then binary = 0.
if binary gt 0 then begin
    nbinary = long(nstars * binary)
;    sims.bin = lindgen(nstars, nsims)
;    sims.bin[nbinary:*] = -1        ; -1 for objects that are NOT binaries
    sims.bin = lindgen(ntot)
    sims.bin[where((sims.bin mod nstars) ge nbinary)] = -1   ; -1 for objects that are NOT binaries
    binlist = where(sims.bin gt -1)                          ; list of sims indices of binary objects
; Create the structure with information about binary companions
; Distance, age will be the same as the primary
;    binars = replicate({mass:fltarr(nbinary)}, nsims)
    nbintot = nbinary * nsims
    binars = { mass:fltarr(nbintot), lum:fltarr(nbintot), Teff:fltarr(nbintot), SpT:fltarr(nbintot), $
               M_y:replicate(!VALUES.F_NAN,nbintot), $
               M_J2:replicate(!VALUES.F_NAN,nbintot), M_H2:replicate(!VALUES.F_NAN,nbintot), M_K2:replicate(!VALUES.F_NAN,nbintot), $
               M_W1:replicate(!VALUES.F_NAN,nbintot), M_W2:replicate(!VALUES.F_NAN,nbintot), M_W3:replicate(!VALUES.F_NAN,nbintot) }
;;     ; Half-Gaussian distribution for mass ratio of binaries
;;     sigmaq = 0.2
;;     binars.mass = sims.mass[binlist] + alog10(1.-abs(sigmaq*randomn(seed, nbintot)))  ; mass in log form
    ; Power law distribution for mass ratio of binaries, as per Allen (2007)
    gamma = 1.8
    randomp, x, gamma, ntot, range_x=[0.1, 1]
    binars.mass = sims.mass[binlist] + alog10(temporary(x))  ; mass in log form
endif


; Luminosity and Temperature
; Read in model for interpolating luminosities and temperatures
model = read_struct2(modelpath, modeltags, comment='#', /nan, _EXTRA=ex)
model.age = alog10(model.age)
model.mass = alog10(model.mass)
model.Teff = alog10(model.Teff)
triangulate, model.age, model.mass, tr
; mass and age ---> [model] ---> luminosity
if not keyword_set(silent) then print, 'Interpolating luminosities'
sims.lum = griddata(model.age, model.mass, model.lum, xout=sims.age, yout=sims.mass, $
                    /linear, triangles=tr, missing=Nan)                ; log(Lbol/Lsun)
if binary gt 0 then binars.lum = griddata(model.age, model.mass, model.lum, xout=sims.age[binlist], $
                                          yout=binars.mass, /linear, triangles=tr, missing=Nan)
; mass and age ---> [model] ---> temperature
if not keyword_set(silent) then print, 'Interpolating effective temperatures'
sims.Teff = griddata(model.age, model.mass, model.Teff, xout=sims.age, yout=sims.mass, $
                     /linear, triangles=tr, missing=Nan)               ; K
sims.Teff = 10^temporary(sims.Teff)    ; convert back to regular units
; Same steps for binaries
if binary gt 0 then begin
    binars.Teff = griddata(model.age, model.mass, model.Teff, xout=sims.age[binlist], $
                           yout=binars.mass, /linear, triangles=tr, missing=Nan)
    binars.Teff = 10^temporary(binars.Teff)     ; convert back to regular units
endif


; Spectral Type, using Table 6 from Golimowski et al (2004).
;; ; I fit a polynomial to SpT as a function of Teff.  The coefficients are:
;; cSpT = [ 40.346539,  -0.014774, -4.86242e-07, 5.13256e-10]                               ; 3rd degree
;; cSpT = [-14.036113,   0.163334, -2.20703e-04,  1.29705e-07, -3.61549e-11, 3.88183e-15]   ; 5th degree
;; sims.SpT = poly(sims.Teff, cSpT)
; I fit a logistic function, using logistic.pro, to SpT as a function of Teff.
; The coefficients for this function are:
cSpT = [0.006856, 1.001071, 0.019012]          ; logistic
sims.SpT = logistic(sims.Teff, cSpT)           ; get SpT from fit
scat = 2.;0.                                   ; scatter in Spectal Type
if scat ne 0 then begin
    scatSpT = scat * randomn(seed, ntot)
    sims.SpT = temporary(sims.SpT) + scatSpT   ; add Gaussian scatter to SpT
endif
sims.SpT = round(2*temporary(sims.SpT))/2.     ; round SpT to nearest half-integer

; Same steps for binaries
if binary gt 0 then begin
    binars.SpT = logistic(binars.Teff, cSpT)
    if scat ne 0 then begin
        ; Use same Spt scatter value for each companion as for its primary
        binars.SpT = temporary(binars.SpT) + scatSpT[binlist]
    endif
    binars.SpT = round(2*temporary(binars.SpT))/2. ; round SpT to nearest half-integer
endif

; Flag data with unrealistic or unneeded spectral types
badspt = where((sims.SpT gt maxspt) or (sims.SpT lt minspt), comp=goodspt)
                                        ; goodspt is the list of SIMS indices for good objects
sims.Teff[badspt] = !VALUES.F_NAN;-999
sims.lum[badspt] = !VALUES.F_NAN;-999
sims.SpT[badspt] = !VALUES.F_NAN;-999
if binary gt 0 then begin
    ; Flag the binary companions whose primaries have already been flagged
    sims.bin[badspt] = !VALUES.F_NAN;-999
    badbin = where((binars.SpT gt maxspt) or (binars.SpT lt minspt) or (sims.bin[binlist] lt 0), $
                       comp=goodbin)    ; goodbin is the list of BINARS indices for good companions
    ; Flag the binary companions with unrealistic or unneeded spectral types
    binars.Teff[badbin] = !VALUES.F_NAN;-999
    binars.lum[badbin] = !VALUES.F_NAN;-999
    binars.SpT[badbin] = !VALUES.F_NAN;-999
    remove, badbin, binlist             ; binlist is the list of SIMS indices for good companions
                                        ; binlist and goodbin are of equal length
                                        ; **binlist is a subset of goodspt**
endif


; Absolute magnitudes, using Kimberly's megatable
; order of filters: z, y, 2MASS J, H, K, UKIDSS Y, J, H, K, W1, W2, W3
;; ; Load a structure called 'filter', with tags:  name, poly
;; restore, file='~/idlprogs/wbest/ltbayes/Mpoly.sav'
;; sims.y[goodspt] = poly(sims.SpT[goodspt], filter[1].poly)
;; etc.
; Load a structure called 'fits', with tags:  name, spt, M, Merr
restore, file='~/idlprogs/wbest/ltbayes/Mfits.sav'
;flist = [1,9,10,11]       ; y, W1, W2, W3
flist = [1,2,3,4,9,10,11]       ; y, 2MASS J, H, K, W1, W2, W3
nfilts = n_elements(flist)

match2, sims.SpT[goodspt], fits[0].spt, sptmatch
nmatch = long(n_elements(sptmatch))
for i=0, nfilts-1 do begin
;    scatM = fits[flist[i]].Merr[sptmatch]*randomn(seed, nmatch)
    scatM = fits[flist[i]].Merr[sptmatch] * 1.0*randomn(seed, nmatch)
    sims.(i+7)[goodspt] = fits[flist[i]].M[sptmatch] + scatM
endfor

; Get the absolute magnitudes of the binary companions using the same steps
; At this point, keeping the primary and companion abs mags separate.
if binary gt 0 then begin
    match2, binars.SpT[goodbin], fits[0].spt, sptbinmatch
    nbinmatch = long(n_elements(sptbinmatch))
    for i=0, nfilts-1 do begin
        ; Use same Spt scatter value for each companion as for its primary
        binars.(i+4)[goodbin] = fits[flist[i]].M[sptbinmatch] + scatM[binlist]
    endfor
endif


; Apparent magnitudes
for i=0, nfilts-1 do begin
    ; Formula: m = M + 5.*alog10(distance) - 5.
    sims.(i+7+nfilts)[goodspt] = sims.(i+7)[goodspt] + 5.*alog10(sims.dist[goodspt]) - 5.
    ; If the object is a binary, convolve the two apparent magnitudes
    if binary gt 0 then begin
        sims.(i+7+nfilts)[binlist] = -2.5*alog10(10^(temporary(sims.(i+7+nfilts)[binlist])/(-2.5)) + $
                         10^((binars.(i+4)[goodbin] + 5.*alog10(sims.dist[binlist]) - 5.)/(-2.5)))
    endif
endfor


; Knock out the objects beyond the detection limits

; Limiting magnitudes for the surveys.
; Strong version:  something that incorporates the empirical mean and
; distribution of the magnitudes of each spectral type at the S/N limit for the
; sample.
; [strong version here]
; Simple version:  use the published/estimated limits
;limmag = [20.3, 18.1, 16.8, 12.7]   ; {y, W1, W2, W3} at S/N={5, 2, 2, 2}
limmag = [20.3, 17.06, 16.17, 15.56, 18.1, 16.8, 12.7]   ; {y, J2, H2, K2, W1, W2, W3} at S/N={5, 5, 5, 5, 2, 2, 2}

; Set NaN for apparent magnitudes beyond the limits
detstr = 'detect = where((sims.(7+nfilts) le limmag[0])'
for i=1, nfilts-2 do begin       ; -2 means to allow W3 non-detections
    detstr = detstr + ' and (sims.('+trim(i)+'+7+nfilts) le limmag['+trim(i)+'])'
endfor
detstr = detstr + ', comp=nodetect)'
dummy = execute(detstr)
for i=0, nfilts-2 do begin       ; -2 means to allow W3 non-detections
    sims.(i+7+nfilts)[nodetect] = !VALUES.F_NAN
endfor
sims.SpT[nodetect] = !VALUES.F_NAN


; Print interesting values to the screen
print, trim(n_elements(detect))+' brown dwarfs are visible in this simulation.'
print, 'The farthest one lies at '+trim(max(sims.dist[detect]))+' pc.'
lttrans = where((sims.spt ge 16) and (sims.spt lt 24))
print, trim(n_elements(lttrans))+' L/T transition dwarfs are visible in this simulation.'
print, 'The farthest one lies at '+trim(max(sims.dist[lttrans]))+' pc.'

for kk=0, 15 do ltnums[jj,kk] = n_elements(where(sims.spt eq 16+.5*kk))
endfor

lttots = total(ltnums, 2)
print, 'Mean number of visible L/T transition dwarfs = '+trim(mean(lttots))
print, '                          Standard deviation = '+trim(stddev(lttots))
print
lthist = total(ltnums, 1)/float(runs)

; Set up plotting window
lincolr_wb, /silent
device, decomposed=0

if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/simhistlt';specplot1'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 28, retain=2, xsize=800, ysize=600
endelse
sptaxis = ['L6','L8','T0','T2','T4']
sptval = [16,18,20,22,24]
plot, findgen(18)*.5+15.75, [lthist[0], lthist, lthist[15]], xtitle='Spectral Type', ytitle='Count', $
      charsize=1.6, /nodata, color=0, backg=1, xtickname=sptaxis, xtickv=spt, xrange=[16,24], xstyle=1
oplot, findgen(18)*.5+15.75, [lthist[0], lthist, lthist[15]], psym=10, color=4

if keyword_set(ps) then ps_close

    

STOP

; Histogram of spectral types
;restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
;list = temporary(wise_new)
;list = list[where(list.spt gt 0 and list.spt lt 30)]
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/simhist';specplot1'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 27, retain=2, xsize=800, ysize=600
endelse
sptaxis = ['L0','L5','T0','T5']
sptval = [10,15,20,25]
plothist, sims.spt[detect], xtitle='Spectral Type', ytitle='Count', charsize=1.6, color=4, $
          backg=1, bin=1, xrange=[10,29], xstyle=1, xtickname=sptaxis, xtickv=spt
vline, 16, color=0, lines=2
vline, 24, color=0, lines=2
;plothist, list.spt, color=0, /overplot
;plothist, sims.spt[detect], color=4, /overplot
; Add vertical dashed lines

if keyword_set(ps) then ps_close

STOP

cuts = [0, 10, 16, 18, 20, 24, 30]
ncats = n_elements(cuts) - 1
cset = [15, 12, 2, 3, 4, 6]       ; new color scheme

symbol = replicate({full:0, open:0, size:0.}, ncats)
; diamond, diamond, square, circle, triangle, triangle
symbol.full = [14, 14,  15,  16,  17,  17]
symbol.open = [ 4,  4,   6,   9,   5,   5]
symbol.size = [.8, .8, 1.5, 1.5, 1.5, 1.5]


;; for i=0, nfilts-1 do begin
;;     plotmag = sims.(i+7)[goodspt]
;;     if binary gt 0 then plotmag[binlist] = $
;;       -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;;     window, i, retain=2, xsize=800, ysize=600
;;     plot, sims.SpT[goodspt], plotmag, color=0, background=1, psym=4, yrange=[24,8], $
;;       title=fits[flist[i]].name, xtitle='Spectral Type', ytitle='Absolute Magnitude', charsize=1.5
;;     oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
;; endfor

;; for i=0, nfilts-1 do begin
;;     window, i+nfilts, retain=2, xsize=800, ysize=600
;;     plot, sims.SpT[goodspt], sims.(i+7+nfilts)[goodspt], color=0, background=1, psym=4, yrange=[24,8], $
;;       title=fits[flist[i]].name, xtitle='Spectral Type', ytitle='Apparent Magnitude', charsize=1.5
;; endfor

if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/mags';specplot1'
    ps_open, outfile, /color, /en, /portrait, thick=4
endif else begin
    window, 0, retain=2, xsize=600, ysize=800
endelse
!p.multi = [0, 1, 3, 0, 0]
xmarg = [12,3]

i=0
plotmag = sims.(i+7)
if binary gt 0 then plotmag[binlist] = $
  -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[24,8], xrange=[5,29], $
      xtickname=replicate(' ',6), xmargin=xmarg, ytickname=' ', ymargin=[2,1]
oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
hline, limmag[i], /data, /noerase, lines=2, color=0
legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
        psym=[4,symcat(14),0], line=[0,0,2], charsize=1, symsize=[1.5,1.5,0], /right

i=4
plotmag = sims.(i+7)
if binary gt 0 then plotmag[binlist] = $
  -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[20,0], xrange=[5,29], $
      xtickname=replicate(' ',6), xmargin=xmarg, ytickname=' ', ymargin=[5,-2]
oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
hline, limmag[i], /data, /noerase, lines=2, color=0
legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
        psym=[4,symcat(14),0], line=[0,0,2], charsize=1, symsize=[1.5,1.5,0], /right

i=5
plotmag = sims.(i+7)
if binary gt 0 then plotmag[binlist] = $
  -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[20,0], $
;      xtitle='Spectral Type', xmargin=xmarg, xchars=2.5, ymargin=[8,-5]
sptaxis = ['M5','L0','L5','T0','T5']
sptval = [5,10,15,20,25]
plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[20,0], ymargin=[8,-5], $
      xtitle='Spectral Type', xmargin=xmarg, xchars=2.5, xstyle=1, xrange=[5,29], $
      xtickname=sptaxis, xtickv=spt
oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
hline, limmag[i], /data, /noerase, lines=2, color=0
legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
        psym=[4,symcat(14),0], line=[0,0,2], charsize=1, symsize=[1.5,1.5,0], /right

;plot, spt, dmod[*,0], color=0, /nodata, background=1, xstyle=1, ystyle=5, $
;      xmargin=[8,8], ymargin=[4,2.5], yrange=[-2,12], $
;      xtickname=sptaxis, xrange=[0,19], xtickv=spt, xtitle='Spectral Type', $
;      title='Survey Depth by Spectral Type', charsize=2
;axis, yaxis=0, color=0, ytitle='Distance (pc)', charsize=2, /ylog, ystyle=1, $
;      yrange=10.^((!Y.CRANGE+5.)/5.)  
;axis, yaxis=1, color=0, ytitle='Distance Modulus, m - M', charsize=2

xyouts, .03, .5, 'Magnitude', chars=1.5, align=0.5, orient=90, /normal
xyouts, .12, .95, 'PS1', chars=2, align=0, /normal
xyouts, .12, .64, 'WISE W1', chars=2, align=0, /normal
xyouts, .12, .33, 'WISE W2', chars=2, align=0, /normal

;; for i=0, nfilts-1 do begin
;;     if (i eq 1) or (i eq 2) or (i eq 3) or (i eq 6) then continue
;;     plotmag = sims.(i+7)
;;     if binary gt 0 then plotmag[binlist] = $
;;       -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;;     window, i, retain=2, xsize=800, ysize=600
;;     plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[24,8], $
;;       title=fits[flist[i]].name, xtitle='Spectral Type', ytitle='Magnitude', charsize=1.5
;;     oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
;;     oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
;;     hline, limmag[i], /data, /noerase, lines=2, color=0
;;     legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
;;             psym=[4,symcat(14),0], line=[0,0,2], charsize=1.5, symsize=[1.5,1.5,0], /bottom
;; endfor

if keyword_set(ps) then ps_close
!p.multi = [0, 0, 1, 0, 0]

; W1 vs. W1-W2
outfile='~/Astro/699-1/paper/modelplots/w1w1w2'
xmin = min(sims.w1-sims.w2, /nan) - 0.2
xmax = max(sims.w1-sims.w2, /nan) + 0.2
ymin = min(sims.M_w1, /nan) - 0.5
ymax = max(sims.M_w1, /nan) + 0.5
ltsimplot, sims.w1-sims.w2, sims.M_w1, sims.spt, ps=ps, outfile=outfile, $
           xmin=xmin, xmax=xmax, ymin=ymax, ymax=ymin, $;segment=segment, $
           xtitle='W1-W2', ytitle='M!DW1!N', $
           win=nfilts, points=points, /bottom

; W1-W2 vs. y-W1
outfile='~/Astro/699-1/paper/modelplots/w1w2yw1'
xmin = min(sims.y-sims.w1, /nan) - 0.2
xmax = max(sims.y-sims.w1, /nan) + 0.2
ymin = min(sims.w1-sims.w2, /nan) - 0.2
ymax = max(sims.w1-sims.w2, /nan) + 0.2
ltsimplot, sims.y-sims.w1, sims.w1-sims.w2, sims.spt, ps=ps, outfile=outfile, $
           xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, $;segment=segment, $
           xtitle='y-W1', ytitle='W1-W2', $
           win=nfilts+1, points=points, /right

; W2-W3 vs. W1-W2
outfile='~/Astro/699-1/paper/modelplots/w2w3w1w2'
xmin = min(sims.w1-sims.w2, /nan) - 0.2
xmax = max(sims.w1-sims.w2, /nan) + 0.2
ymin = min(sims.w2-sims.w3, /nan) - 0.2
ymax = max(sims.w2-sims.w3, /nan) + 0.2
ltsimplot, sims.w1-sims.w2, sims.w2-sims.w3, sims.spt, ps=ps, outfile=outfile, $
           xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, $;segment=segment, $
           xtitle='W1-W2', ytitle='W2-W3', $
           win=nfilts+2, points=points, /right, /bottom

; J vs. J-H
outfile='~/Astro/699-1/paper/modelplots/JJH'
xmin = min(sims.J2-sims.H2, /nan) - 0.2
xmax = max(sims.J2-sims.H2, /nan) + 0.2
ymin = min(sims.M_J2, /nan) - 0.5
ymax = max(sims.M_J2, /nan) + 0.5
ltsimplot, sims.J2-sims.H2, sims.M_J2, sims.spt, ps=ps, outfile=outfile, $
           xmin=xmin, xmax=xmax, ymin=ymax, ymax=ymin, $;segment=segment, $
           xtitle='J!D2MASS!N-H!D2MASS!N', ytitle='M!DJ_2MASS!N', $
           win=nfilts+3, points=points, /right

; K vs. J-K
outfile = '~/Astro/699-1/paper/modelplots/KJK'
xmin = min(sims.J2-sims.K2, /nan) - 0.2
xmax = max(sims.J2-sims.K2, /nan) + 0.2
ymin = min(sims.M_K2, /nan) - 0.5
ymax = max(sims.M_K2, /nan) + 0.5
ltsimplot, sims.J2-sims.K2, sims.M_K2, sims.spt, ps=ps, outfile=outfile, $
           xmin=xmin, xmax=xmax, ymin=ymax, ymax=ymin, $;segment=segment, $
           xtitle='J!D2MASS!N-K!D2MASS!N', ytitle='M!DK_2MASS!N', $
           win=nfilts+4, points=points, /right, /bottom

;; ; J-H vs. SpT
;; window, 20, retain=2, xsize=800, ysize=600
;; plot, sims.SpT, sims.J2-sims.H2, color=0, background=1, psym=4, symsize=0.5, $;yrange=[24,8], $
;;       xtitle='Spectral Type', ytitle='J!D2MASS!N-H!D2MASS!N', charsize=1.5

; J-K vs. SpT
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/jkspt';specplot1'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 21, retain=2, xsize=800, ysize=600
endelse

plot, sims.SpT, sims.J2-sims.K2, /nodata, color=0, background=1, $
      xtitle='Spectral Type', ytitle='J!D2MASS!N-K!D2MASS!N', charsize=1.5

gcols = where(finite(sims.J2-sims.K2))
JK = sims.J2[gcols]-sims.K2[gcols]
JKspt = sims.SpT[gcols]
minqspt = 6
maxqspt = 25

sptlist = findgen(2*(maxqspt-minqspt)+1)/2. + minqspt
JKlq = fltarr(2*(maxqspt-minqspt)+1)
JKuq = JKlq
for i=0, 2*(maxqspt-minqspt) do begin
    isp = where(JKspt eq sptlist[i])
    JKi = JK[isp]
    JKmed = median(JKi)
    JKdev = stddev(JKi)
    plots, [sptlist[i], sptlist[i]], [JKmed-JKdev, JKmed+JKdev], color=3, thick=24
endfor

;; for i=0, 2*(maxqspt-minqspt) do begin
;;     isp = where(JKspt eq sptlist[i])
;;     JKi = JK[isp]
;;     JKlq[i] = JKi[n_elements(isp)/4 - 1]
;;     JKuq[i] = JKi[3*n_elements(isp)/4 - 1]
;;     plots, [sptlist[i], sptlist[i]], [JKlq[i], JKuq[i]], color=3, thick=12
;; endfor

;; sptlist = findgen((maxqspt-minqspt)+1) + minqspt
;; JKlq = fltarr((maxqspt-minqspt)+1)
;; JKuq = JKlq
;; for i=0, (maxqspt-minqspt) do begin
;;     isp = where((JKspt eq sptlist[i]) or (JKspt eq sptlist[i]+.5))
;;     JKi = JK[isp]
;;     JKmed = median(JKi)
;;     JKdev = stddev(JKi)
;;     plots, [sptlist[i], sptlist[i]], [JKmed-JKdev, JKmed+JKdev], color=3, thick=12
;; endfor

;; for i=0, (maxqspt-minqspt) do begin
;;     isp = where((JKspt eq sptlist[i]) or (JKspt eq sptlist[i]+.5))
;;     JKi = JK[isp]
;;     JKlq[i] = JKi[n_elements(isp)/4 - 1]
;;     JKuq[i] = JKi[3*n_elements(isp)/4 - 1]
;;     plots, [sptlist[i], sptlist[i]], [JKlq[i], JKuq[i]], color=3, thick=12
;; endfor

oplot, sims.SpT, sims.J2-sims.K2, color=0, psym=4, symsize=0.5
if keyword_set(ps) then ps_close

if not keyword_set(silent) then print, systime()

END
