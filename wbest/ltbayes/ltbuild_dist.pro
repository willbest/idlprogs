FUNCTION LTBUILD_DIST, NOBJ, RADIUS

;  Randomly generate distances for objects distributed randomly in a spherical
;  volume around a star.
;
;  HISTORY
;  Written by Will Best (IfA), 05/2012
;
;  INPUTS
;      NOBJ - Number of objects to return a distance for
;      RADIUS - Radius of the volume in which to generate distances
;
;  OPTIONAL KEYWORDS:
;

; Generate the distance for each object -- for uniform distribution in a
;     volume, use a P(r) ~ r^2 probability distribution.
randomp, x, 2, nobj, range_x=[0,radius]
;x = reform[temporary(x), nstars, nsims]     ; temporary() saves memory!

return, x

END
