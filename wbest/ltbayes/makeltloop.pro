;PRO MAKELT, _EXTRA=extrakey

;  Wrapper to simulations of L-T transition dwarfs in the Solar neighborhood.
;  Calls ltbuilder.pro
;
;  HISTORY
;  Written by Will Best (IfA), 08/31/2012
;
;  OPTIONAL KEYWORD INPUTS for LTBUILDER.PRO:
;      NSIMS - The number of simulations to run before aggregating results.
;              Default is 100.
;      AREA - Fraction of the full sky to simulate.
;             Default: 1 (whole sky)
;      BINARY - Fraction of stars that are binaries.
;               Default: 0.15
;      DENSITY - The number density of brown dwarfs in the Solar
;                neighborhood, in pc^-3.  Default value is 0.01 pc^-3.
;      FLIST - Vector containing the indices of filters, according to the
;              sequence from the megatable:
;                   PS1 z, y; 2MASS J, H, K; UKIDSS Y, J, H, K; WISE W1, W2, W3
;              Default: flist = [1,2,3,4,9,10,11] for: y, 2MASS J, H, K, W1, W2, W3
;      INDEX - If set, the simulation will use a power-law IMF of the form
;                  phi(m) ~ m^(-a)
;              If INDEX is a single value, this value is used for a.
;              If INDEX is a two-element vector, then a two-part power law will
;              be used, with a=INDEX[0] below a critical mass, and a=INDEX[1]
;              above the critical mass.  The critical mass is set by the MCRIT
;              keyword.  If MCRIT is not set, 0.05 Msun will be used.
;           ---> Allen et al (2005) use 0.09 Msun
;              If /LOGNORMAL is set, the value of INDEX is ignored.
;      LOGNORMAL - If set, the simulation will use a log-normal IMF of the form
;                      phi(log m) ~ exp( -(log m - log mcrit)^2 / (2sigma^2) )
;                  The critical mass is set by the MCRIT keyword.  If MCRIT is not
;                  set, 0.25 Msun will be used.
;                  The spread (sigma) is set by the MSIGMA keyword.  If MSIGMA is not
;                  set, 0.5 dex will be used.
;      MAGFIX - Band to which to fix generated absolute magnitudes.
;               If set, absolute magnitudes for each spectral type will be
;               assigned from the values and errors of the model in this band.
;               Use /NOERR to assign these without error.
;               Absolute magnitudes for other bands will be determined by colors
;               and color errors with the other bands.
;               Use /NOCOLERR to assign these without error.
;               Options: z, y, J2, H2, K2, YM, JM, HM, KM, W1, W2, W3
;      MCRIT - Critical mass to be used with the model IMF.
;              If INDEX is a two-element vector (two-part power law), MCRIT has
;              a default value of 0.05 Msun.
;              If LOGNORMAL is set, MCRIT has a default value of 0.25 Msun.
;      MAXSPT - Maximum spectral type to model.  Default is 28 (T8).
;               Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;      MINSPT - Minimum spectral type to model.  Default is 6 (M6).
;      MMAX - Maximum mass of stars to be generated.  Default is 0.1 Msun.
;      MMIN - Minimum mass of stars to be generated.  Default is 0.01 Msun.
;      MODELFIX - Generated magnitudes from the fixed band (MAGFIX) to the
;                 evolutionary model.
;      MSIGMA - Spread for a log-normal IMF.  If LOGNORMAL is set, MSIGMA has a
;               default value of 0.5 dex.
;      RADIUS - The radius of the simulated solar neighborhood, in parsecs.
;               The program rounds RADIUS down to the nearest (long) integer.
;               Default is 100 pc.
;      SFH - Slope to be used for the star formation history function, which has
;            the form  P(age) = slope*age + P(0)
;            Default value is 0 Gyr^-1 (i.e. constant star formation rate over time).
;      SILENT - Suppress outputs to screen.
;

runs=100
nsims=1
;area=2026/41253.
;binary=0.
density=0.0249
;graph=1
index=.25
magfix='J2'
;modelfix=1
;noerr=1
;nocolerr=1
ps=1
points=1
radius=275.4
silent=1

; Common block for data to be plotted later
common outputs, sims, goodspt, detect, nodetect, nfilts

ltnums = intarr(runs,16)
ltlocs = intarr(runs,16)
for jj=0, runs-1 do begin
    print, 'Run '+trim(jj+1)

    ltbuilder, nsims=nsims, area=area, binary=binary, density=density, graph=graph, index=index, $
               magfix=magfix, modelfix=modelfix, nocolerr=nocolerr, noerr=noerr, radius=radius, $
               silent=silent


; Print interesting values to the screen
    landt = where((sims.spt ge 10) and (sims.spt lt 30))
    lttrans = where((sims.spt ge 16) and (sims.spt lt 24))
    local = where(sims.dist le 20)
    sims.SpT[nodetect] = !VALUES.F_NAN
    landtdet = where((sims.spt ge 10) and (sims.spt lt 30))
    lttransdet = where((sims.spt ge 16) and (sims.spt lt 24))
    locallt = where((sims.spt ge 16) and (sims.spt lt 24) and (sims.dist le 20))
    volume = 4./3*!pi*long(radius)^3 * area
    print
    print, 'This simulation contains a total of '+trim(n_elements(goodspt))+' ultracool dwarfs.'
    print, 'Space density of L and T dwarfs:  '+trim(n_elements(landt)/volume)+' pc^-3'
    print
    print, 'This simulation contains a total of '+trim(n_elements(lttrans))+' L/T transition dwarfs.'
    print, trim(n_elements(lttransdet))+' L/T transition dwarfs are detectable in y, W1, and W2.'
    print
    print, trim(n_elements(local))+' objects lie within 20 pc of the Sun.'
    print, 'Of these, '+trim(n_elements(locallt))+' are L/T transition dwarfs.'
    print

; Record numbers for each L/T transition spectal type
    ;; for kk=0, 15 do ltnums[jj,kk] = n_elements(where(sims.spt eq 16+.5*kk))
    ;; for kk=0, 15 do ltlocs[jj,kk] = n_elements(where(sims.spt[locallt] eq 16+.5*kk))
    for kk=0, 7 do ltnums[jj,kk] = n_elements(where((sims.spt ge 16+kk) and (sims.spt lt 17+kk)))
    for kk=0, 7 do ltlocs[jj,kk] = $
      n_elements(where((sims.spt[locallt] ge 16+kk) and (sims.spt[locallt] lt 17+kk)))

endfor

lttots = total(ltnums, 2)
print, 'Mean number of visible L/T transition dwarfs = '+trim(mean(lttots))
print, '                          Standard deviation = '+trim(stddev(lttots))
lthist = total(ltnums, 1)/float(runs)

ltloctots = total(ltlocs, 2)
print, 'Mean number of visible L/T transition dwarfs within 20 pc of the Sun = '+trim(mean(ltloctots))
print, '                                                  Standard deviation = '+trim(stddev(ltloctots))
ltlochist = total(ltlocs, 1)/float(runs)

; Set up plotting window
lincolr_wb, /silent
device, decomposed=0

; Histogram of L/T trans SpT visible
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/simhistltvis'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 28, retain=2, xsize=800, ysize=600
endelse
sptaxis = ['L6','L8','T0','T2','T4']
sptval = [16,18,20,22,24]
;; plot, findgen(18)*.5+15.75, [lthist[0], lthist, lthist[15]], xtitle='Spectral Type', ytitle='Number', $
;;       charsize=1.6, /nodata, color=0, backg=1, xtickname=sptaxis, xtickv=spt, xrange=[16,24], xstyle=1, $
;;       yrange=[0,65], ystyle=1
plot, findgen(10)+15.5, [lthist[0], lthist, lthist[7]], xtitle='Spectral Type', ytitle='Number', $
      charsize=1.6, /nodata, color=0, backg=1, xtickname=sptaxis, xtickv=spt, xrange=[16,24], xstyle=1
restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
list = temporary(wise_new)
list = list[where((list.spt ge 16) and (list.spt lt 24))]
plothist, list.spt, color=0, bin=1, /overplot
;; oplot, findgen(18)*.5+15.75, [lthist[0], lthist, lthist[15]], psym=10, color=4
oplot, findgen(10)+15.5, [lthist[0], lthist, lthist[7]], psym=10, color=4
legend, ['Known (152)', 'Simulated 10!7r!X ('+trim(round(mean(lttots)))+')'], textc=[0,4], $
        colors=[0,4], outline=0, line=[0,0], charsize=1.3, /right
if keyword_set(ps) then ps_close


; Histogram of L/T trans visible within 20 pc, overplotted with known
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/simhist20pc'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 29, retain=2, xsize=800, ysize=600
endelse
;; plot, findgen(18)*.5+15.75, [ltlochist[0], ltlochist, ltlochist[15]], xtitle='Spectral Type', $
;;       ytitle='Number', charsize=1.6, /nodata, color=0, backg=1, xtickname=sptaxis, xtickv=spt, $
;;       xrange=[16,24], xstyle=1, yrange=[0,18], ystyle=1
plot, findgen(10)+15.5, [ltlochist[0], ltlochist, ltlochist[7]], xtitle='Spectral Type', $
      ytitle='Number', charsize=1.6, /nodata, color=0, backg=1, xtickname=sptaxis, xtickv=spt, $
      xrange=[16,24], xstyle=1;, yrange=[0,18], ystyle=1
list = list[where((list.dis gt 0) and (list.dis le 20))]
plothist, list.spt, color=0, bin=1, /overplot
;; oplot, findgen(18)*.5+15.75, [ltlochist[0], ltlochist, ltlochist[15]], psym=10, color=4
oplot, findgen(10)+15.5, [ltlochist[0], ltlochist, ltlochist[7]], psym=10, color=4
legend, ['Known (48)', 'Simulated ('+trim(round(mean(ltloctots)))+')'], textc=[0,4], colors=[0,4], $
        outline=0, line=[0,0], charsize=1.3
if keyword_set(ps) then ps_close


END
