pro burrows_lbol_age, logLbol0, logage0, $
                      logLbolerr = err_logLbol, logageerr = err_logage, $
                      mass = mass, teff = teff, logg = logg_out, rad = rad, $
                      interpolate_age = interpolate_age0, $
                      uniform_ageerr = uniform_ageerr, $
                      debug = debug, reset = reset, $
                      printstats = printstats, $
                      silent = silent

;+
; given the (Lbol, age) for an object, return all other quantites based on the Burrows models
;
; if input age is a scalar and matches an an existing isochrone file, then that is used.
;   to do calculations at arbitrary ages, set /interpolate_age
;   this is also the default for the case of 2 or more input ages
;     even if both of them correspond to existing isochrone
;   (this feature may not behave properly if you are at the edges of the model grid!)
; 
; INPUTS
;   logLbol    log(Lbol/Lsun) (scalar or vector)
;   logage     log(age), in years (scalar or vector)
;
; KEYWORD INPUTS
;   logLbolerr   uncertainty in log(Lbol) (scalar)
;   logageerr    uncetainty in log(age) (scalar)
;
; 12/22/08 M. Liu (IfA/Hawaii)
; 10/24/09 MCL Major update to allow for arbitrary choice of age, if /interpolate_age is set
;                does this using GRIDDATA routine in IDL with a finely interpolated set of isochrones
;                the program is now structured as basically 2 different methods
;                  one for the simple case (desired age matches existing isochrone)
;                  one for the fully age-interpolated case
;              'logage' can now be a vector, not just a scalar
;              Changed name from BURROWS_OBSCALC.PRO to BURROWS_LBOL_AGE.PRO
;              Switched to double-precision calculations (minor effect)
; 11/16/09 MCL Robust handling of allowing Lbol and/or age to be either scalar or vector (or both)
;              Added easy incorporation of measurement errors with 'err_lbol' and 'err_logage' 
;                though only works for scalar inputs
;              Added /printstats
; 11/27/09 MCL Change input for Lbol to log(Lbol)
;              Text output now includes the 1-sigma confidence limits
; 02/03/12 MCL Added /uniform_ageerr, to assume age uncertainty is uniformly distribution
;-


; location of Burrows models
MODELDIR = '~/work/brown-models/burrows/'
ALL_ISOCHRONES = 'tucson_isochrones.SMALL.dat'


; define common block to sort model info for a single isochrone, 
; so can be efficient if calling same isochrone many times
common cb_burrowsmodel, $
   cb_isolist, cb_isolist_logage, $   ; properties of complete list of isochrones
   cb_isofile, cb_isofile_logage, $   ; name & age of isochrone file currently loaded
   cb_mass, cb_age, cb_teff, cb_Lbol, cb_rad, $  ; data in isochrone file
   cb_log_mass, cb_log_age, cb_log_teff, cb_log_lbol, cb_log_rad   ; compute from data, to make faster


; define common block for full set of interpolated isochrones
; note: this file give log(Lbol), while single-age isochrones give Lbol
common cb_all_burrowsmodel, $
   cb_all_logage, cb_all_mass, cb_all_teff, cb_all_logLbol, cb_all_logg, cb_all_rad, $
   cb_all_log_mass, cb_all_log_teff, cb_all_log_rad, $
   cb_all_triangles


; load list of isochrone files for Burrows models, if not done before
if n_elements(cb_isolist) eq 0 then begin
    message, 'loading list of isochrone files', /info, noprint = silent
    readcol2, MODELDIR+'isochrones.index', cb_isolist, cb_isolist_logage, form = 'a,f', /silent
endif



;--------------------------------------------------------------------------------
; print help info & setup
;--------------------------------------------------------------------------------
if n_params() ne 2 then begin
    print, 'pro burrows_obscalc, log(Lbol/Lsun), log(age/yr),'
    print, '                     [logLbolerr=], [logageerr=],'
    print, '                     [mass(Mjup)=], [teff=], [logg=], [rad(Rsun)=]'
    print, '                     [interpolate_age], [uniform_ageerr],'
    print, '                     [debug], [reset], [printstats], [silent]'
    print, '  list of log(age) = '+commalist(cb_isolist_logage, 'f4.1')
    return
endif
;if n_elements(logage) ne 1 then begin
;    message, '**input log(age) must be a scalar**', /info
;    return
;endif
if keyword_set(debug) then $   
   noprint = (debug eq 1)


; value of keyword may be altered by program so handle this w/a a copy of the variable
if keyword_set(interpolate_age0) then $
   interpolate_age = interpolate_age0 $
else $
   interpolate_age = 0


; clean up 1-elements vectors, which will mess up some calcs
if (n_elements(logLbol0) eq 1) then logLbol = logLbol0(0)
if (n_elements(logage0) eq 1) then logage = logage0(0)
if (n_elements(err_lbol) eq 1) then err_lbol = err_lbol(0)
if (n_elements(err_logage) eq 1) then err_logage = err_logage(0)


; sanity check on # of values of {Lbol, age} passed
if (n_elements(logLbol0) ne n_elements(logage0)) and $
   ((n_elements(logLbol0) ne 1) and (n_elements(logage0) ne 1)) then begin
   message, '** number of {Lbol,age} values passed are not compatible **', /info
   message, '     number of Lbol values = '+strc(n_elements(logLbol0)), /info
   message, '     number of age values = '+strc(n_elements(logage0)), /info
   return
; for case where Lbol/age is a scalar but the other is a vector, make sizes equal
endif else if (n_elements(logLbol0) eq 1) and (n_elements(logage0) ne 1) then begin
   logLbol = logLbol0 + fltarr(n_elements(logage0))   
   logage = logage0
endif else if (n_elements(logLbol0) ne 1) and (n_elements(logage0) eq 1) then begin
   logLbol = logLbol0
   logage = logage0 + fltarr(n_elements(logLbol0)) 
endif else if (n_elements(logLbol0) eq n_elements(logage0)) then begin
   logLbol = logLbol0
   logage = logage0
endif else $
   message, '**Problem!!**'



; if errors in Lbol or age are passed, then generate the array of values
; note that this only works if inputted Lbol, age, and errors all are scalars
NTRIALS = 10000
if keyword_set(err_logLbol) or keyword_set(err_logage) then begin

   if (n_elements(logLbol) eq 1) and (n_elements(logage) eq 1) and $
      (n_elements(err_logLbol) le 1) and (n_elements(err_logage) le 1) then begin
      ; apply Lbol errors
      if keyword_set(err_logLbol) then begin
         logLbol = logLbol + err_logLbol*randomn(seed, NTRIALS)
         if not(keyword_set(err_logage)) then $
            logage = logage + fltarr(NTRIALS)
      endif
      ; apply log(age) errors
      if keyword_set(err_logage) then begin
         if keyword_set(uniform_ageerr) then $
            logage = logage + err_logage*(2*randomu(seed, NTRIALS)-1.0) $
         else $
            logage = logage + err_logage*randomn(seed, NTRIALS) 
         interpolate_age = 1  ; activate /interpolate_age
         if not(keyword_set(err_logLbol)) then $
            logLbol = logLbol + fltarr(NTRIALS)
      endif

   endif else begin
      message, '** applying errors in log(Lbol) and/or log(age) only works for scalars **', /info
      message, '     # of elements for log(Lbol) = '+strc(n_elements(logLbol)), /info
      message, '     # of elements for log(age) = '+strc(n_elements(logage)), /info
      message, '     # of elements for err_logLbol = '+strc(n_elements(err_logLbol)), /info
      message, '     # of elements for err_logage = '+strc(n_elements(err_logage)), /info
   endelse
endif
         


;--------------------------------------------------------------------------------
; load appropriate model isochrone file, if not previously loaded
;--------------------------------------------------------------------------------
if (n_elements(cb_isofile_logage) eq 0) or (keyword_set(reset)) then $
   cb_isofile_logage = -999    ; kludge for handling the decision tree



; load appropriate isochrone file, if not previously loaded 
if (total(logage-cb_isofile_logage) ne 0) then begin

    match, cb_isolist_logage, logage(uniq(logage, sort(logage))), wa, wb, count = nmatch

    ; (1) if desired age(s) is a unique value, identify the model isochrone file and load it
    ;     note this only happens if *one* age is passed, even if the multiple ages all have isochrone files
    if (nmatch eq 1) then begin
    ;if (nmatch eq 1) and (total(logage-logage(0)) eq 0) then begin

        ; load file
        w = (where(cb_isolist_logage eq logage(0)))(0)   ; important that this is a scalar, not a vector!
        message, 'loading new isochrone file = "'+cb_isolist(w)+'"', /info, noprint = silent
        readcol2, /silent, MODELDIR+cb_isolist(w), $
                  cb_mass, cb_age, cb_teff, cb_lbol, cb_rad, $
                  form = 'd,d,d,d,d'
        cb_rad = cb_rad * 1e9 / 6.96e10   ; models are tabulated in Gcm, convert to Rsun
        cb_isofile = cb_isolist(w)
        cb_isofile_logage = cb_isolist_logage(w)     

        ; do some calcs for computational efficiency
        cb_log_mass = alog10(cb_mass)
        cb_log_lbol = alog10(cb_lbol)
        cb_log_teff = alog10(cb_teff)
        cb_log_rad = alog10(cb_rad)

        ; de-activate /interpolate_age, if it is set
        if (keyword_set(interpolate_age)) then begin
           message, 'deactivating /interpolate_age, since there is an existing isochrone file', /info
           interpolate_age = 0
        endif

    ; (2) if desired age does not match an isochrone file and /interpolate_age is set, 
    ;       then need to load fully interpolated isochrone file w/all ages
    ;     or if list of desired ages corresponds to more than 1 isochrone file
    endif else if keyword_set(interpolate_age) or (nmatch ge 2) then begin
       message, '** input age does not uniquely match list of isochrones. using full interpolation **', /info

       ; active /interpolate_age, if needed be
       if (keyword_set(interpolate_age) eq 0) and (nmatch ge 2) then $
          interpolate_age = 1
       
       ; load file with all isochrones
       if (n_elements(cb_all_logage) eq 0) or keyword_set(reset) then begin
          message, '   loading full isochrone file (be patient) = "'+ALL_ISOCHRONES+'"', /info
          readcol2, /silent, MODELDIR + ALL_ISOCHRONES, $
                    cb_all_logage, cb_all_mass, cb_all_teff, cb_all_logLbol, cb_all_logg, cb_all_rad, $
                    form = 'd,d,d,d,d,d'
          cb_all_mass = cb_all_mass * 1048.  ; Mjup

          ; do some calcs for computational efficiency
          cb_all_log_mass = alog10(cb_all_mass)
          cb_all_log_teff = alog10(cb_all_teff)
          cb_all_log_rad = alog10(cb_all_rad)
       endif

    ; (3) if no matching age and not /interpolate_age, then quit
    endif else begin
        message, '** isochrone file not found for desired log(age) = '+strc(logage), /info
        message, '    possible choices = '+commalist(cb_isolist_logage), /info
        message, '** aborting **', /info
        return
    endelse

endif else begin
   if not(keyword_set(silent)) then $
      message, 're-using existing isochrone file, log(age) = '+strc(cb_isofile_logage), /info, noprint = silent
   ; de-activate /interpolate_age, if it is set
   if (keyword_set(interpolate_age)) then begin
      message, 'deactivating /interpolate_age, since there is an existing isochrone file', /info
      interpolate_age = 0
   endif
endelse


;--------------------------------------------------------------------------------
; compute desired quantities, using interpolation in log space
; choosing the appropriate method given the input age(s)
;--------------------------------------------------------------------------------
; (1) single age, using existing isochrone file
;     this works fine if Lbol is scalar or vector
if not(keyword_set(interpolate_age)) then begin
   message, 'using existing isochrone file', /info
   mass = 10.^interpol(cb_log_mass, cb_log_lbol, loglbol)
   teff = 10.^interpol(cb_log_teff, cb_log_lbol, loglbol)
   rad = 10.^interpol(cb_log_rad, cb_log_lbol, loglbol)
   logg_out = logg(mass/1048., rad)

   max_logLbol = alog10(max(cb_lbol))
   min_logLbol = alog10(min(cb_lbol))


; (2) multiple ages, or an age that does not match the existing isochrone files
endif else begin
   message, 'using interpolation', /info

   if (n_elements(cb_all_triangles) eq 0) then begin
      message, 'computing triangles for interpolation', /info
      triangulate, cb_all_logage, cb_all_logLbol, cb_all_triangles
   endif

   ; input age needs to be a vector with the same length as Lbol
   if (n_elements(logage) eq 1) and (n_elements(logLbol) ne 1) then $
      logage_1 = fltarr(n_elements(logLbol)) + logage $ 
   else $
      logage_1 = logage

   ; plot locus of models & input data as a check
   if keyword_set(debug) then begin
      lincolr, /silent
      win, 0, 500, /check, /wshow
      plot, cb_all_logage, cb_all_logLbol, ps = 7, sym = 0.5, $
            xtit = 'log(age)', ytit = 'log(Lbol)', chars = 2
      oplot, logage_1, logLbol, ps = 1, sym = 0.5, col = 3
      legend, /bottom, chars = 2, psym= 1, col = 3, 'input values'
      checkcontinue
   endif

   ; interpolate
   mass = 10.^griddata(cb_all_logage, cb_all_logLbol, alog10(cb_all_mass), $      
                       /linear, triangles = cb_all_triangles, $
                       xout = logage_1, yout = logLbol)
   teff = 10.^griddata(cb_all_logage, cb_all_logLbol, alog10(cb_all_teff), $      
                       /linear, triangles = cb_all_triangles, $
                       xout = logage_1, yout = logLbol)
   rad = 10.^griddata(cb_all_logage, cb_all_logLbol, alog10(cb_all_rad), $      
                      /linear, triangles = cb_all_triangles, $
                      xout = logage_1, yout = logLbol)
   logg_out = logg(mass/1048., rad)

   ; flag data that exceeds age range
   w = where(logage_1 gt max(cb_all_logage) or logage_1 lt min(cb_all_logage), nb)
   if (nb gt 0) then begin
      mass(wb) = -999
      teff(wb) = -999
      rad(wb) = -999
      logg_out(wb) = -999
   endif

   ; store Lbol limits
   max_logLbol = max(cb_all_logLbol)
   min_logLbol = min(cb_all_logLbol)

endelse


; truncate results outside the model range
wb = where(logLbol gt max_logLbol or logLbol lt min_logLbol, nb)
if (nb gt 0) then begin
    mass(wb) = -999
    teff(wb) = -999
    rad(wb) = -999
    logg_out(wb) = -999
endif


;--------------------------------------------------------------------------------
; print results
;--------------------------------------------------------------------------------


; table
if not(keyword_set(silent)) and (n_elements(logLbol) le 30) then begin
    print, ' log(Lbol/Lsun)  log(age)', 'Mass[Mjup]', 'Teff[K]', 'Rad[Rsun]', 'log(g)[cgs]', $
           form = '(a-27,4(a12))'
    forprint, sigfig(logLbol, 3), sigfig(logage, 3), sigfig(mass, 3), $
              sigfig(teff, 4), sigfig(rad, 3), sigfig(logg_out, 3), $
              form = '(2(a11),"   ",4(a12))'
endif 
if n_elements(logLbol) gt 30 then $
   message, 'more than 30 elements in input Lbol, so suppressing printout', /info


; or statistics, if generated errors for Lbol and age
if keyword_set(err_logLbol) or keyword_set(err_logage) or keyword_set(printstats) then begin
   mass0 = conflimits(mass, 1, mass_cf, /relative)
   teff0 = conflimits(teff, 1, teff_cf, /relative)
   rad0 = conflimits(rad, 1, rad_cf, /relative)
   logg0 = conflimits(logg_out, 1, logg_cf, /relative)
   print, '  ---- inputs ----'
   print, '  log(Lbol) = ', string(median(logLbol), '(f6.3)'), ' +/- ', string(stdev(logLbol), '(f6.3)')
   print, '  log(age) = ', strc(median(logage)), ' +/- ', strc(stdev(logage))
   if keyword_set(uniform_ageerr) then $
      print, '     uniform age uncertainties, log(age) = ', strc(min(logage)), ' to ', strc(max(logage))
   print, '  ---- outputs ----'
   print, '  Mass [Mjup] = ', string(median(mass), '(f6.2)'), ' +/- ', string(stdev(mass), '(f5.2)'), $
          '    {', string(mass_cf(0), '(f6.2)'), ', +', string(mass_cf(1), '(f5.2)'), '}'
   print, '  Teff [K] = ', string(median(teff), '(f6.1)'), ' +/- ', string(stdev(teff), '(f5.1)'), $
          '    {', string(teff_cf(0), '(f6.1)'), ', +', string(teff_cf(1), '(f6.1)'), '}'
   print, '  Radius [Rsun] = ', string(median(rad),'(f6.4)'), ' +/- ', string(stdev(rad), '(f6.4)'), $
          '    {', string(rad_cf(0), '(f7.4)'), ', +', string(rad_cf(1), '(f6.4)'), '}'
   print, '  log(g) [cgs] = ', string(median(logg_out), '(f5.3)'), ' +/- ', string(stdev(logg_out), '(f5.3)'), $
          '    {', string(logg_cf(0), '(f6.3)'), ', +', string(logg_cf(1), '(f6.3)'), '}'
   print
endif


end
