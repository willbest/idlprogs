PRO VALUEOFPI, NITER=niter, NSIMS=nsims

;  Runs a Monte Carlo simulation to estimate the value of pi.
;  Written as a first dabble into Monte Carlo simulation.
;  Idea from:  http://en.wikipedia.org/wiki/Monte_Carlo_method
;
;  HISTORY
;  Written by Will Best (IfA), 05/22/2012
;
;  CALLING SEQUENCE: 
;     valueofpi [, NITER=niter, NSIMS=nsims]
;
;  OPTIONAL KEYWORD INPUTS:
;      NITER - The number of iterations to run for each simulation.
;              Default is 10,000.
;      NSIMS - The number of simulations to run before aggregating results.
;              Default is 100.

If not keyword_set(niter) then niter = 10000
niter = long(niter)
If not keyword_set(nsims) then nsims = 100
nsims = long(nsims)

; Generate random points on a 1x1 grid.
x = randomu(seed, niter, nsims, /double)
y = randomu(seed, niter, nsims, /double)

; Determine if the numbers are within 1 unit of the origin.
d = sqrt(x^2 + y^2)
g = where(d le 1)
h = intarr(niter, nsims)
h[g] = 1
count = total(h, 1)    ; sum along the 1st dimension of the array

; Divide number of points within 1 unit of the origin by NITER, and multiply by 4.
val = 4 * double(count)/niter

; Determine the mean, median, and sigma for the calculated values of pi.
print, mean(val), format='("mean value = ",d8.6)'
print, median(val), format='("median value = ",d8.6)'
print, stddev(val), format='("standard deviation = ",d8.6)'

; For nsmis>10, plot a histogram of pi values.
if nsims gt 10 then begin
    window, retain=2, xsize=800, ysize=600
    lincolr, /silent
    pichar='!7p!X'
    plothist, val, bin=.5/sqrt(niter), color=0, background=1, charsize=1.5, $
          xtitle='Value of '+pichar, title='Value of '+pichar+' from Monte Carlo Simulations', $
          ytitle='Frequency'
endif

END
