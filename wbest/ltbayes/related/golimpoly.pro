;ps=1
;PRO GOLIMPOLY

; Do awesome stuff with Table 6 from Golimowski et al (2004).

; This table has columns:
; Name, SpectralType, SpT, binary, Mbol, Mbolerr, BC_K, BC_Kerr, log Lbol/Lsun,
;          log Lbol/Lsunerr, Teff range, Teff 3 Gyr
;    SpectralType is text (e.g. M5, L7.5)
;    SpT is a number (M0=0, L0=10, T0=20)
;    binary: 0=no, 1=yes

;  HISTORY
;  Begun by Will Best (IfA), 07/28/2012
;

infile = '~/Astro/699-1/Golimowski04.tb6.csv'
tagnames = 'name:a, spectraltype:a, spt:f, bin:b, mbol:f, mbolerr:f, bck:f, bckerr:f, lum:F, lumerr:f, teffrange:a, teff3:i'
;readcol, infile, name, spt, bin, mbol, mbolerr, lum, lumerr, teffrange, teff3, $
;         comment='#', delim=',', format='a,x,f,b,f,f,x,x,f,f,a,i', /silent
g = read_struct2(infile, tagnames, comment='#', delim=',');, /silent)

; Remove known binaries
nobin = where(g.bin eq 0)
g = g[nobin]

; Remove SpT<M6
later = where(g.spt ge 6)
g = g[later]

; Plot the data
device, decomposed=0
lincolr, /silent

if keyword_set(ps) then begin
    fpath = '~/Astro/699-1/paper/modelplots/golimlogis'
    ps_open, fpath, /en, thick=4
endif else window, 0, retain=2, xsize=800, ysize=600

;; plot, g.spt, g.lum, color=0, background=1, charsize=1.5, psym=2, $
;;       xtitle='Spectral Type', ytitle='log (Lbol/Lsun)';, xrange=[5,30]
;; ;; cL3 = poly_fit(g.spt, g.lum, 3)
;; ;; Lfit3 = poly(g.spt, cL3)
;; ;; oplot, g.spt, Lfit3, color=2
;; ;; cL4 = poly_fit(g.spt, g.lum, 4)
;; ;; Lfit4 = poly(g.spt, cL4)
;; ;; oplot, g.spt, Lfit4, color=0
;; cL5 = poly_fit(g.spt, g.lum, 5)
;; Lfit5 = poly(g.spt, cL5)
;; oplot, g.spt, Lfit5, color=3
;; cL6 = poly_fit(g.spt, g.lum, 6)
;; Lfit6 = poly(g.spt, cL6)
;; oplot, g.spt, Lfit6, color=4

; Fit a polynomial to SpT vs. Teff
g = g[sort(g.Teff3)]       ; order by Teff
;; window, 0
plot, g.teff3, g.spt, color=0, background=1, charsize=1.5, psym=2, yrange=[3,32], ystyle=1, $
      ytitle='Spectral Type', xtitle='T!Deff!N', xrange=[3000,500]
;; cT3 = poly_fit(g.Teff3, g.spt, 3, chisq=chiT3, sigma=sigT3, yband=ybandT3, yerror=yerrT3, yfit=yfitT3)
;; Tfit3 = poly(g.Teff3, cT3)
;; oplot, g.Teff3, Tfit3, color=2
;; print, cT3, format='("[", 2(f10.6,", "), e12.5,", ", e11.5,"]")'
;; cT4 = poly_fit(g.Teff3, g.spt, 4, chisq=chiT4, sigma=sigT4, yband=ybandT4, yerror=yerrT4, yfit=yfitT4)
;; Tfit4 = poly(g.Teff3, cT4)
;; oplot, g.Teff3, Tfit4, color=0
;; print, cT4, format='("[", 2(f10.6,", "), 2(e12.5,", "), e12.5,"]")'
;; cT5 = poly_fit(g.Teff3, g.spt, 5, chisq=chiT5, sigma=sigT5, yband=ybandT5, yerror=yerrT5, yfit=yfitT5)
;; Tfit5 = poly(g.Teff3, cT5)
;; oplot, g.Teff3, Tfit5, color=3
;print, cT5, format='("[", 2(f10.6,", "), 3(e12.5,", "), e11.5,"]")'
;; cT6 = poly_fit(g.Teff3, g.spt, 6, chisq=chiT6, sigma=sigT6, yband=ybandT6, yerror=yerrT6, yfit=yfitT6)
;; Tfit6 = poly(g.Teff3, cT6)
;; oplot, g.Teff3, Tfit6, color=4

; TRYING OTHER FITS
err = dblarr(n_elements(g.spt))+1
Teff = indgen(461)*5 + 600
; Logistic
startlgsT = double([01., 1.001, .01])
csptlgsT = mpfitfun('logistic', g.Teff3, g.spt, err, startlgsT, weights=1D, /quiet)
sptfitlgsT = logistic(Teff, csptlgsT)
oplot, Teff, sptfitlgsT, color=0
if keyword_set(ps) then ps_close
print, csptlgsT, format='("[", 2(f8.6,", "), f8.6,"]")'

; Get error with 4 bins: 10 points with T>2000, 11 with 1600<T<2000, 12 with 1250<T<1600, 12 with T<1250
resid = g.spt - logistic(g.Teff3, csptlgsT)
spterr1 = stddev(resid[where(g.Teff3 gt 2000)])
spterr2 = stddev(resid[where((g.Teff3 gt 1600) and (g.Teff3 lt 2000))])
spterr3 = stddev(resid[where((g.Teff3 gt 1250) and (g.Teff3 lt 1600))])
spterr4 = stddev(resid[where(g.Teff3 lt 1250)])
spterr = [spterr1, spterr2, spterr3, spterr4]
print, spterr, format='("[", 3(f5.3,", "), f5.3,"]")'



; Fit a polynomial to SpT vs. Luminosity

;; window, 1, retain=2, xsize=800, ysize=600

;; g = g[sort(g.lum)]       ; order by Luminosity
;; plot, g.lum, g.spt, color=0, background=1, charsize=1.5, psym=2, yrange=[3,32], ystyle=1, $
;;       ytitle='Spectral Type', xtitle='log (L!Dbol!N/L'+sunsymbol()+')'
;; cspt3 = poly_fit(g.lum, g.spt, 3, chisq=chiL3, sigma=sigL3, yband=ybandL3, yerror=yerrL3, yfit=yfitL3)
;; sptfit3 = poly(g.lum, cspt3)
;; oplot, g.lum, sptfit3, color=2
;; cspt4 = poly_fit(g.lum, g.spt, 4, chisq=chiL4, sigma=sigL4, yband=ybandL4, yerror=yerrL4, yfit=yfitL4)
;; sptfit4 = poly(g.lum, cspt4)
;; oplot, g.lum, sptfit4, color=3
;; cspt5 = poly_fit(g.lum, g.spt, 5, chisq=chiL5, sigma=sigL5, yband=ybandL5, yerror=yerrL5, yfit=yfitL5)
;; sptfit5 = poly(g.lum, cspt5)
;; oplot, g.lum, sptfit5, color=0
;; cspt6 = poly_fit(g.lum, g.spt, 6, chisq=chiL6, sigma=sigL6, yband=ybandL6, yerror=yerrL6, yfit=yfitL6)
;; sptfit6 = poly(g.lum, cspt6)
;; oplot, g.lum, sptfit6, color=4

; TRYING OTHER FITS
;; err = dblarr(n_elements(g.spt))+1
;; lum = findgen(330)/100 - 5.9
;; ; Cube root
;; ;startcube = double([-10., -4.3, 18.])
;; ;cspt3rd = mpfitfun('cuberoot', g.lum, g.spt, err, startcube, weights=1D, /quiet)
;; ;sptfit3rd = cuberoot(lum, cspt3rd)
;; ; Logistic
;; startlgsL = double([6., 3., .01])
;; csptlgsL = mpfitfun('logistic', g.lum, g.spt, err, startlgsL, weights=1D, /quiet)
;; sptfitlgsL = logistic(lum, csptlgsL)
;; oplot, lum, sptfitlgsL, color=0

END
