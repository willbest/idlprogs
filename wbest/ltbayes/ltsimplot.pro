PRO LTSIMPLOT, X, Y, SPT, CATS=cats, NOLEG=noleg, OUTFILE=outfile, POINTS=points, PS=ps, $
               SEGMENT=segment, WIN=win, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax, $
               _EXTRA=extrakey

;  Routine to plot color-color and color-magnitude diagrams for the solor
;  neighborhood simulator ltbuilder.pro.
;
;  HISTORY
;  Written by Will Best (IfA), 08/14/2012
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      X - Vector of values to plot on the horizontal axis.
;      Y - Vector of values to plot on the horizontal axis.
;          Must be the same length as X.
;      SPT - Vector of spectral types (numbers, not text, e.g. 14, not L4).
;            Must be supplied, even if it's the same as X, and must be
;            the same length as X and Y.
;
;  OPTIONAL KEYWORDS
;      CATS - Vector containing spectral types for the boundaries of the plotted
;             colors.
;             Default: [0, 10, 16, 18, 20, 24, 30]
;             The first element cannot be less than 0.
;      NOLEG - Don't draw a legend.
;      OUTFILE - Path for postscript output
;                Default: ~/Astro/colorplot1.ps
;      POINTS - Plot with tiny points (psym=3) instead of symbols.
;      PS - Output to postscript.
;      SEGMENT - Array of structures, containing information on line segments to
;                draw on the plot.
;                segment.x = 2-element vectors containing x-coords of segment
;                endpoints.
;                segment.y = 2-element vectors containing y-coords of segment
;                endpoints.
;                segment.color = color for segment
;                segment.style = linestyle for segment (keyword for PLOTS)
;                segment.thick = thickness for segment (keyword for PLOTS)
;      WIN - Window number to use when calling the IDL window routine.
;            Usefull when a program calls colorplot multiple times.
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;

;=======================

n = n_elements(x)

;=======================

;; ; Create vector for filled and open symbols
;; opensym = bytarr(n)

; Default cats vector
if n_elements(cats) eq 0 then cats = [6, 10, 16, 18, 20, 24, 28.5]
ncats = n_elements(cats) - 1

; Default error bar vector
if n_elements(errorv) eq 0 then errorv = intarr(ncats)

; Define structure with plotting symbols
symbol = replicate({full:0, open:0, size:0.}, ncats)
if keyword_set(points) then begin
    ; only small points
    ;; symbol.full = intarr(ncats) + 3
    ;; symbol.open = intarr(ncats) + 3
    ;; symbol.size = fltarr(ncats) + 1
    symbol.full = intarr(ncats) + 16
    symbol.open = intarr(ncats) + 16
    symbol.size = fltarr(ncats) + .2
endif else begin
    ; diamond, diamond, square, circle, triangle, triangle
    symbol.full = [14, 14,  15,  16,  17,  17]
    symbol.open = [ 4,  4,   6,   9,   5,   5]
    symbol.size = [.8, .8, 1.5, 1.5, 1.5, 2]
endelse

; Load a color table
device, decomposed=0
lincolr_wb, /silent

; Build the legend structure
leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
sptl = [replicate('M',10), replicate('L',10), replicate('T',10)]
for i=0, ncats-1 do leg[i].text = sptl[fix(cats[i])]+trim(cats[i] mod 10) + '-' $
  + sptl[fix(cats[i+1]-.5)]+trim((cats[i+1]-.5) mod 10)
leg.textc = intarr(ncats)
legchars = 1.5
if keyword_set(points) then begin
    leg.sym = intarr(ncats) + 16
    leg.syms = intarr(ncats) + 1
endif else begin
    leg.sym = symbol.full
    leg.syms = symbol.size
endelse
if n_elements(cset) eq ncats then leg.symc = cset


; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/colorplot1.ps'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=600
endelse

if not keyword_set(xmin) then xmin = min(x, /nan) - .5
if not keyword_set(xmax) then xmax = max(x, /nan) + .5
if not keyword_set(ymin) then ymin = min(y, /nan) - .5
if not keyword_set(ymax) then ymax = max(y, /nan) + .5

; Plot the graph
if keyword_set(noleg) then begin
    colorplot, x, y, spt, cats=cats, cset=cset, _extra=extrakey, $
           xrange=[xmin,xmax], xerr=xerr, errorvec=errorv, $
           yrange=[ymin,ymax], yerr=yerr, symbol=symbol
endif else begin
    colorplot, x, y, spt, leg=leg, chars=legchars, cats=cats, cset=cset, _extra=extrakey, $
           xrange=[xmin,xmax], xerr=xerr, errorvec=errorv, $
           yrange=[ymin,ymax], yerr=yerr, symbol=symbol
endelse

; Plot any segments called
for i=0, n_elements(segment)-1 do plots, segment[i].x, segment[i].y, $
           color=segment[i].color, linestyle=segment[i].style, thick=segment[i].thick

if keyword_set(ps) then ps_close


END
