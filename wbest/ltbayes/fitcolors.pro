FUNCTION FITCOLORS, BAND1, BAND2, OBJSPT, FITSPT, COLNAME=colname, GRAPH=graph, $
                    FITERR=fiterr, PFIT=pfit, SILENT=silent

;  Using Kimberley Aller's table of known ultracool dwarfs, plots a color
;  vs. spectral type graph for two given bands.  Fits a 5th degree polynomial
;  fit to the colors, graphs the polynomial, and prints out the polynomial
;  coefficients.  Returns the best-fit color values at each SpT, and standard
;  error (optional).  Currently handles only M, L and T dwarfs.
;
;  HISTORY
;  Written by Will Best (IfA), 08/25/2011
;
;  INPUTS
;      BAND1 - Array of magnitudes for band 1.
;      BAND2 - Array of magnitudes for band 2.
;         Order matters!  The BAND1 - BAND2 colors will be fit.
;      OBJSPT - Array of spectral types for the objects.
;         These three arrays must have the same dimensions, and should match up!
;
;      FITSPT - Vector of SpT values, for which colors are calculated using the
;               best fit model.
;
;  KEYWORDS
;      COLNAME - Title for the y-axis of the graph
;      GRAPH - Display fitting graph, and pause the program; any key to continue.
;      FITERR - Array of color errors for each SpT in the fit.
;      PFIT - Coefficients of the polynomial fit
;      SILENT - Suppress screen outputs
;
;  FUTURE IMPROVEMENTS
;

colors = band1 - band2

real = where(finite(colors))
colors = colors[real]
spt = objspt[real]

; Preliminary fit
pfit = poly_fit(spt, colors, 5, yerror=cerr)     ; get the coefficients of a 5th order polynomial
fitcolor = poly(fitspt, pfit)                    ; plug the SpT vector into the polynomial

; Get magnitude errors from residuals, binned by SpT intervals
chunk = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 24, 26]
nchunk = n_elements(chunk)
colerr = fltarr(nchunk)           ; vector for magnitude errors
errind = lookup(spt, chunk)
sigout = -1                       ; reset the outlier tracker to null
for j=0, nchunk-1 do begin
    spt2 = where(errind eq j)
    if spt2[0] eq -1 then continue
    resid = colors[spt2] - poly(spt[spt2], pfit)
    colerr[j] = stddev(resid)

; Check for 5 sigma outliers
    sigout2 = where((abs(resid) - 5*colerr[j]) ge 0)
    if sigout2[0] gt -1 then sigout = [sigout, sigout2]
endfor

; If 5sigma outliers exist, remove them and re-do the polynomial fit
;while n_elements(sigout) gt 1 do begin
if n_elements(sigout) gt 1 then begin
    sigout = sigout[1:*]
    nooutl = indgen(n_elements(spt))
    remove, sigout, nooutl
    spt = spt[nooutl]
    colors = colors[nooutl]
; Re-do the poynomial fit
    pfit = poly_fit(spt, colors, 5, yerror=cerr) ; get the coefficients of a 5th order polynomial
    fitcolor = poly(fitspt, pfit)                ; plug the SpT vector into the polynomial
; Re-calculate the magnitude errors
    errind = lookup(spt, chunk)
    sigout = -1                 ; reset the outlier tracker to null
    for j=0, nchunk-1 do begin
        spt2 = where(errind eq j)
        if spt2[0] eq -1 then continue
        resid = colors[spt2] - poly(spt[spt2], pfit)
        colerr[j] = stddev(resid)
; Check for 5 sigma outliers
        sigout2 = where((abs(resid) - 5*colerr[j]) ge 0)
        if sigout2[0] gt -1 then sigout = [sigout, sigout2]
    endfor
endif
;endwhile

; Establish the color errors
dummy = lookup(fitspt, chunk, colerr, fiterr)

; Print the polynomial coefficients
if not keyword_set(silent) then print, pfit, format='("[",5(f11.7,","),f11.7,"]")'

; Set up plotting window
if keyword_set(graph) then begin
    lincolr, /silent
    device, decomposed=0
    window, retain=2, xsize=800, ysize=600

; Plot the absolute magnitudes and the polynomial fit
    if n_elements(colname) eq 0 then colname = 'Color'
    plot, spt, colors, color=0, background=1, psym=4, xrange=[min(fitspt)-2,max(fitspt)+2], xstyle=1, $
          xtitle='Spectral Type', ytitle=colname, charsize=1.5
    oplot, fitspt, fitcolor, color=0
    wait = get_kbrd()
endif

return, fitcolor

END
