FUNCTION LTBUILD_AGE, NOBJ, SFH

;  Randomly generate distances for objects distributed randomly in a spherical
;  volume around a star.
;
;  HISTORY
;  Written by Will Best (IfA), 05/2012
;
;  INPUTS
;      NOBJ - Number of objects to return a distance for
;      SFH - Slope to be used for the star formation history function, which has
;            the form  P(age) = slope*age + P(0)
;
;  OPTIONAL KEYWORDS:
;

; Generate the age of each star
if sfh eq 0 then begin
; Constant star formation rate
;    x = alog10(10*randomu(seed, nstars, nsims))
    x = alog10(10*randomu(seed, nobj))                          ; log Gyr
endif else begin
; Linear star formation rate function
    if sfh lt 0 then p0 = sfh * (-10.) else p0 = 0.
;    x = alog10((-p0 + sqrt(p0^2 + 2.*sfh*randomu(seed, nstars, nsims))) / sfh)
    x = alog10((-p0 + sqrt(p0^2 + 2.*sfh*randomu(seed, nobj))) / sfh)   ; log Gyr
endelse

return, x

END
