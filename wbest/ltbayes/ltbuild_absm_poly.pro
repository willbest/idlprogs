FUNCTION LTBUILD_ABSM_POLY, MODELMAG, MODELERR, MODELSPT, SPTLIST, $
                            ERRIN=errin, ERROUT=errout, NOERR=noerr

;  Generates absolute magnitudes for objects with known spectral type.
;
;  Assigns magnitudes according to a model (e.g. a polynomial fit to previous
;  data).  Distibutes magnitudes using user-supplied model spectral types,
;  magnitudes, and errors.
;   
;  HISTORY
;  Written by Will Best (IfA), 08/25/2012
;
;  INPUTS
;      MODELMAG - Magnitudes for the model.
;      MODELERR - Magnitude errors for the model.
;      MODELSPT - Spectral types for the model.
;         These three vectors must have the same length, and should match up!
;      SPTLIST - List of spectral types to return an absolute magnitude for.
;
;  OPTIONAL INPUTS:
;      ERRIN - Vector of deviations from the model absolute magnitudes that are
;              assigned to each object.  If supplied, they are used to assign the
;              absolute magnitudes.  If not supplied, deviations are generated according
;              to the model.  Suppyling pre-determined deviations can be useful
;              if you want a binary companion to have the same deviation as its
;              primary.
;      ERROUT - Variable for returning a vector of deviations from the model
;               absolute magnitudes that are generated in this routine.
;      NOERR - Do not use deviations in determining the absolute magnitudes.
;

; Determine absolute magnitudes, using Kimberly's megatable
; order of filters: PS1 z, y; 2MASS J, H, K; UKIDSS Y, J, H, K; WISE W1, W2, W3

match2, sptlist, modelspt, sptmatch
nmatch = long(n_elements(sptmatch))
if keyword_set(noerr) then begin
    x = modelmag[sptmatch]
endif else begin
    if n_elements(errin) eq 0 then begin
        errout = modelerr[sptmatch]*randomn(seed, nmatch)
    endif else errout = errin
    x = modelmag[sptmatch] + errout 
endelse

return, x

END
