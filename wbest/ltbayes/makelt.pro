;PRO MAKELT, _EXTRA=extrakey

;  Wrapper to simulations of L-T transition dwarfs in the Solar neighborhood.
;  Calls ltbuilder.pro
;
;  HISTORY
;  Written by Will Best (IfA), 08/31/2012
;
;  OPTIONAL KEYWORD INPUTS for LTBUILDER.PRO:
;      NSIMS - The number of simulations to run before aggregating results.
;              Default is 100.
;      AREA - Fraction of the full sky to simulate.
;             Default: 1 (whole sky)
;      BINARY - Fraction of stars that are binaries.
;               Default: 0.15
;      DENSITY - The number density of brown dwarfs in the Solar
;                neighborhood, in pc^-3.  Default value is 0.01 pc^-3.
;      FLIST - Vector containing the indices of filters, according to the
;              sequence from the megatable:
;                   PS1 z, y; 2MASS J, H, K; UKIDSS Y, J, H, K; WISE W1, W2, W3
;              Default: flist = [1,2,3,4,9,10,11] for: y, 2MASS J, H, K, W1, W2, W3
;      INDEX - If set, the simulation will use a power-law IMF of the form
;                  phi(m) ~ m^(-a)
;              If INDEX is a single value, this value is used for a.
;              If INDEX is a two-element vector, then a two-part power law will
;              be used, with a=INDEX[0] below a critical mass, and a=INDEX[1]
;              above the critical mass.  The critical mass is set by the MCRIT
;              keyword.  If MCRIT is not set, 0.05 Msun will be used.
;           ---> Allen et al (2005) use 0.09 Msun
;              If /LOGNORMAL is set, the value of INDEX is ignored.
;      LOGNORMAL - If set, the simulation will use a log-normal IMF of the form
;                      phi(log m) ~ exp( -(log m - log mcrit)^2 / (2sigma^2) )
;                  The critical mass is set by the MCRIT keyword.  If MCRIT is not
;                  set, 0.25 Msun will be used.
;                  The spread (sigma) is set by the MSIGMA keyword.  If MSIGMA is not
;                  set, 0.5 dex will be used.
;      MAGFIX - Band to which to fix generated absolute magnitudes.
;               If set, absolute magnitudes for each spectral type will be
;               assigned from the values and errors of the model in this band.
;               Use /NOERR to assign these without error.
;               Absolute magnitudes for other bands will be determined by colors
;               and color errors with the other bands.
;               Use /NOCOLERR to assign these without error.
;               Options: z, y, J2, H2, K2, YM, JM, HM, KM, W1, W2, W3
;      MCRIT - Critical mass to be used with the model IMF.
;              If INDEX is a two-element vector (two-part power law), MCRIT has
;              a default value of 0.05 Msun.
;              If LOGNORMAL is set, MCRIT has a default value of 0.25 Msun.
;      MAXSPT - Maximum spectral type to model.  Default is 28 (T8).
;               Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;      MINSPT - Minimum spectral type to model.  Default is 6 (M6).
;      MMAX - Maximum mass of stars to be generated.  Default is 0.1 Msun.
;      MMIN - Minimum mass of stars to be generated.  Default is 0.01 Msun.
;      MODELFIX - Generated magnitudes from the fixed band (MAGFIX) to the
;                 evolutionary model.
;      MSIGMA - Spread for a log-normal IMF.  If LOGNORMAL is set, MSIGMA has a
;               default value of 0.5 dex.
;      RADIUS - The radius of the simulated solar neighborhood, in parsecs.
;               The program rounds RADIUS down to the nearest (long) integer.
;               Default is 100 pc.
;      SFH - Slope to be used for the star formation history function, which has
;            the form  P(age) = slope*age + P(0)
;            Default value is 0 Gyr^-1 (i.e. constant star formation rate over time).
;      SILENT - Suppress outputs to screen.
;

nsims=1
area=2026/41253.
binary=0.
density=0.0249
;graph=1
index=.25
magfix='J2'
;modelfix=1
;noerr=1
;nocolerr=1
ps=1
points=1
radius=275.4
;silent=1

ltbuilder, nsims=nsims, area=area, binary=binary, density=density, graph=graph, index=index, $
           magfix=magfix, modelfix=modelfix, nocolerr=nocolerr, noerr=noerr, radius=radius, $
           silent=silent

; Common block for data to be plotted later
common outputs, sims, goodspt, detect, nodetect, nfilts


; Print interesting values to the screen
landt = where((sims.spt ge 10) and (sims.spt lt 30))
lttrans = where((sims.spt ge 16) and (sims.spt lt 24))
local = where(sims.dist le 20)
sims.SpT[nodetect] = !VALUES.F_NAN
landtdet = where((sims.spt ge 10) and (sims.spt lt 30))
lttransdet = where((sims.spt ge 16) and (sims.spt lt 24))
locallt = where((sims.spt ge 16) and (sims.spt lt 24) and (sims.dist le 20))
volume = 4./3*!pi*long(radius)^3 * area
print
print, 'This simulation contains a total of '+trim(n_elements(goodspt))+' ultracool dwarfs.'
print, trim(n_elements(detect))+' ultracool dwarfs are detectable in y, W1, and W2.'
print, 'The farthest one lies at '+trim(max(sims.dist[detect]))+' pc.'
print
print, 'This simulation contains a total of '+trim(n_elements(landt))+' L and T dwarfs.'
print, '   Space density of L and T dwarfs:  '+trim(n_elements(landt)/volume)+' pc^-3'
print, trim(n_elements(landtdet))+' L and T dwarfs are detectable in y, W1, and W2.'
print, 'The farthest one lies at '+trim(max(sims.dist[landtdet]))+' pc.'
print
print, 'This simulation contains a total of '+trim(n_elements(lttrans))+' L/T transition dwarfs.'
print, trim(n_elements(lttransdet))+' L/T transition dwarfs are detectable in y, W1, and W2.'
print, 'The farthest one lies at '+trim(max(sims.dist[lttransdet]))+' pc.'
print
print, trim(n_elements(local))+' objects lie within 20 pc of the Sun.'
print, 'Of these, '+trim(n_elements(locallt))+' are L/T transition dwarfs.'


; Set up plotting window
lincolr_wb, /silent
device, decomposed=0

; Histogram of all spectral types
;restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
;list = temporary(wise_new)
;list = list[where(list.spt gt 0 and list.spt lt 30)]
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/simhist'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 27, retain=2, xsize=800, ysize=600
endelse
sptaxis = ['L0','L5','T0','T5']
sptval = [10,15,20,25]
plothist, sims.spt[detect], xtitle='Spectral Type', ytitle='Number', charsize=1.6, color=4, $
          backg=1, bin=1, xrange=[10,29], xstyle=1, xtickname=sptaxis, xtickv=sptval
vline, 16, color=0, lines=2
vline, 24, color=0, lines=2
;plothist, list.spt, color=0, /overplot
;plothist, sims.spt[detect], color=4, /overplot
; Add vertical dashed lines
if keyword_set(ps) then ps_close


; Histogram of L/T trans SpT visible
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/simhistltvis'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 28, retain=2, xsize=800, ysize=600
endelse
sptaxis = ['L6','L8','T0','T2','T4']
sptval = [16,18,20,22,24]
plothist, sims.spt[detect], xtitle='Spectral Type', ytitle='Number', charsize=1.6, color=4, $
          backg=1, bin=1, xrange=[16,24], xstyle=1, xtickname=sptaxis, xtickv=sptval;, yrange=[0,100]
restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
list = temporary(wise_new)
list = list[where((list.spt ge 16) and (list.spt lt 24))]
plothist, list.spt, color=0, bin=1, /overplot
plothist, sims.spt[detect], color=4, bin=1, /overplot
legend, ['Known (152)', 'Simulated 10!7r!X (729)'], textc=[0,4], colors=[0,4], outline=0, $
        line=[0,0], charsize=1.3, /right
if keyword_set(ps) then ps_close


; Histogram of L/T trans visible within 20 pc, overplotted with known
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/simhist20pc'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 29, retain=2, xsize=800, ysize=600
endelse
sptaxis = ['L6','L8','T0','T2','T4']
sptval = [16,18,20,22,24]
plothist, sims.spt[locallt], xtitle='Spectral Type', ytitle='Number', charsize=1.6, color=4, $
          backg=1, bin=1, xrange=[16,24], xstyle=1, xtickname=sptaxis, xtickv=sptval
list = list[where((list.dis gt 0) and (list.dis le 20))]
plothist, list.spt, color=0, bin=1, /overplot
plothist, sims.spt[locallt], color=4, bin=1, /overplot
legend, ['Known (48)', 'Simulated (156)'], textc=[0,4], colors=[0,4], outline=0, $
        line=[0,0], charsize=1.3, /right
if keyword_set(ps) then ps_close

;STOP

cuts = [0, 10, 16, 18, 20, 24, 30]
ncats = n_elements(cuts) - 1
cset = [15, 12, 2, 3, 4, 6]       ; new color scheme

symbol = replicate({full:0, open:0, size:0.}, ncats)
; diamond, diamond, square, circle, triangle, triangle
symbol.full = [14, 14,  15,  16,  17,  17]
symbol.open = [ 4,  4,   6,   9,   5,   5]
symbol.size = [.8, .8, 1.5, 1.5, 1.5, 1.5]


;; for i=0, nfilts-1 do begin
;;     plotmag = sims.(i+7)[goodspt]
;;     if binary gt 0 then plotmag[binlist] = $
;;       -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;;     window, i, retain=2, xsize=800, ysize=600
;;     plot, sims.SpT[goodspt], plotmag, color=0, background=1, psym=4, yrange=[24,8], $
;;       title=fits[flist[i]].name, xtitle='Spectral Type', ytitle='Absolute Magnitude', charsize=1.5
;;     oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
;; endfor

;; for i=0, nfilts-1 do begin
;;     window, i+nfilts, retain=2, xsize=800, ysize=600
;;     plot, sims.SpT[goodspt], sims.(i+7+nfilts)[goodspt], color=0, background=1, psym=4, yrange=[24,8], $
;;       title=fits[flist[i]].name, xtitle='Spectral Type', ytitle='Apparent Magnitude', charsize=1.5
;; endfor

;; if keyword_set(ps) then begin
;;     outfile = '~/Astro/699-1/paper/modelplots/mags';specplot1'
;;     ps_open, outfile, /color, /en, /portrait, thick=4
;; endif else begin
;;     window, 0, retain=2, xsize=600, ysize=800
;; endelse
;; !p.multi = [0, 1, 3, 0, 0]
;; xmarg = [12,3]

;; i=0
;; plotmag = sims.(i+7)
;; if binary gt 0 then plotmag[binlist] = $
;;   -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;; plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[24,8], xrange=[5,29], $
;;       xtickname=replicate(' ',6), xmargin=xmarg, ytickname=' ', ymargin=[2,1]
;; oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
;; oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
;; hline, limmag[i], /data, /noerase, lines=2, color=0
;; legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
;;         psym=[4,symcat(14),0], line=[0,0,2], charsize=1, symsize=[1.5,1.5,0], /right

;; i=4
;; plotmag = sims.(i+7)
;; if binary gt 0 then plotmag[binlist] = $
;;   -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;; plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[20,0], xrange=[5,29], $
;;       xtickname=replicate(' ',6), xmargin=xmarg, ytickname=' ', ymargin=[5,-2]
;; oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
;; oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
;; hline, limmag[i], /data, /noerase, lines=2, color=0
;; legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
;;         psym=[4,symcat(14),0], line=[0,0,2], charsize=1, symsize=[1.5,1.5,0], /right

;; i=5
;; plotmag = sims.(i+7)
;; if binary gt 0 then plotmag[binlist] = $
;;   -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;; ;plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[20,0], $
;; ;      xtitle='Spectral Type', xmargin=xmarg, xchars=2.5, ymargin=[8,-5]
;; sptaxis = ['M5','L0','L5','T0','T5']
;; sptval = [5,10,15,20,25]
;; plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[20,0], ymargin=[8,-5], $
;;       xtitle='Spectral Type', xmargin=xmarg, xchars=2.5, xstyle=1, xrange=[5,29], $
;;       xtickname=sptaxis, xtickv=spt
;; oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
;; oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
;; hline, limmag[i], /data, /noerase, lines=2, color=0
;; legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
;;         psym=[4,symcat(14),0], line=[0,0,2], charsize=1, symsize=[1.5,1.5,0], /right

;; xyouts, .03, .5, 'Magnitude', chars=1.5, align=0.5, orient=90, /normal
;; xyouts, .12, .95, 'PS1', chars=2, align=0, /normal
;; xyouts, .12, .64, 'WISE W1', chars=2, align=0, /normal
;; xyouts, .12, .33, 'WISE W2', chars=2, align=0, /normal

;; for i=0, nfilts-1 do begin
;;     if (i eq 1) or (i eq 2) or (i eq 3) or (i eq 6) then continue
;;     plotmag = sims.(i+7)
;;     if binary gt 0 then plotmag[binlist] = $
;;       -2.5*alog10(10^(temporary(plotmag[binlist])/(-2.5)) + 10^(binars.(i+4)[goodbin]/(-2.5)))
;;     window, i, retain=2, xsize=800, ysize=600
;;     plot, sims.SpT, plotmag, color=0, background=1, psym=4, yrange=[24,8], $
;;       title=fits[flist[i]].name, xtitle='Spectral Type', ytitle='Magnitude', charsize=1.5
;;     oplot, fits[flist[i]].spt, fits[flist[i]].M, color=0
;;     oplot, sims.SpT[detect], sims.(i+7+nfilts)[detect], color=3, psym=symcat(14)
;;     hline, limmag[i], /data, /noerase, lines=2, color=0
;;     legend, ['Absolute Mag', 'Apparent Mag', 'Limiting Mag'], textc=[0,3,0], colors=[0,3,0], outline=0, $
;;             psym=[4,symcat(14),0], line=[0,0,2], charsize=1.5, symsize=[1.5,1.5,0], /bottom
;; endfor
;; 
;; if keyword_set(ps) then ps_close
;; !p.multi = [0, 0, 1, 0, 0]

; W1 vs. W1-W2
outfile='~/Astro/699-1/paper/modelplots/w1w1w2'
xmin = min(sims.w1-sims.w2, /nan) - 0.2
xmax = max(sims.w1-sims.w2, /nan) + 0.2
ymin = min(sims.M_w1, /nan) - 0.5
ymax = max(sims.M_w1, /nan) + 0.5
ltsimplot, sims.w1[detect]-sims.w2[detect], sims.M_w1[detect], sims.spt[detect], $
           xmin=xmin, xmax=xmax, ymin=ymax, ymax=ymin, $
           xtitle='W1-W2', ytitle='M!DW1!N', ps=ps, outfile=outfile, $
           win=nfilts, points=points, /bottom

; W1-W2 vs. y-W1
outfile='~/Astro/699-1/paper/modelplots/w1w2yw1'
;; xmin = min(sims.y-sims.w1, /nan) - 0.3
;; xmax = max(sims.y-sims.w1, /nan) + 0.2
;; ymin = min(sims.w1-sims.w2, /nan) - 0.2
;; ymax = max(sims.w1-sims.w2, /nan) + 0.2
xmin=1.8
xmax=6.5
ymin=-0.4
ymax=3.2
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 3)
segment.color = [0, 0, 4]
segment.style = [2, 2, 3]
segment.thick = [5, 5, 8]
segment.x = [ [3.0, 3.0],  [3.0, xmax], [xmin,      (6-3.*ymin) < xmax] ]
segment.y = [ [0.4, ymax], [0.4, 0.4],  [2-xmin/3., ymin > (2-xmax/3.)] ]
ltsimplot, sims.y[detect]-sims.w1[detect], sims.w1[detect]-sims.w2[detect], sims.spt[detect], $
           xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
           xtitle='y!DPS1!N-W1', ytitle='W1-W2', ps=ps, outfile=outfile, $
           win=nfilts+1, /right

;for m=0, (xmax-xmin+.1)*20+1 do begin

STOP

; W2-W3 vs. W1-W2
;; outfile='~/Astro/699-1/paper/modelplots/w2w3w1w2'
;; xmin = min(sims.w1-sims.w2, /nan) - 0.2
;; xmax = max(sims.w1-sims.w2, /nan) + 0.2
;; ymin = min(sims.w2-sims.w3, /nan) - 0.2
;; ymax = max(sims.w2-sims.w3, /nan) + 0.2
;; ltsimplot, sims.w1[detect]-sims.w2[detect], sims.w2[detect]-sims.w3[detect], sims.spt[detect], $
;;            xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, $;segment=segment, $
;;            xtitle='W1-W2', ytitle='W2-W3', ps=ps, outfile=outfile, $
;;            win=nfilts+2, points=points, /right, /bottom

; J vs. J-H
outfile='~/Astro/699-1/paper/modelplots/JJH'
xmin = min(sims.J2-sims.H2, /nan) - 0.2
xmax = max(sims.J2-sims.H2, /nan) + 0.2
ymin = min(sims.M_J2, /nan) - 0.5
ymax = max(sims.M_J2, /nan) + 0.5
ltsimplot, sims.J2[detect]-sims.H2[detect], sims.M_J2[detect], sims.spt[detect], $
           xmin=xmin, xmax=xmax, ymin=ymax, ymax=ymin, $;segment=segment, $
           xtitle='J!D2MASS!N-H!D2MASS!N', ytitle='M!DJ,2MASS!N', ps=ps, outfile=outfile, $
           win=nfilts+3, points=points

; K vs. J-K
outfile = '~/Astro/699-1/paper/modelplots/KJK'
xmin = min(sims.J2-sims.K2, /nan) - 0.2
xmax = max(sims.J2-sims.K2, /nan) + 0.2
ymin = min(sims.M_K2, /nan) - 0.5
ymax = max(sims.M_K2, /nan) + 0.5
ltsimplot, sims.J2[detect]-sims.K2[detect], sims.M_K2[detect], sims.spt[detect], $
           xmin=xmin, xmax=xmax, ymin=ymax, ymax=ymin, $;segment=segment, $
           xtitle='J!D2MASS!N-K!D2MASS!N', ytitle='M!DK,2MASS!N', ps=ps, outfile=outfile, $
           win=nfilts+4, points=points, /right, /bottom

;; ; J-H vs. SpT
;; window, 20, retain=2, xsize=800, ysize=600
;; plot, sims.SpT, sims.J2-sims.H2, color=0, background=1, psym=4, symsize=0.5, $;yrange=[24,8], $
;;       xtitle='Spectral Type', ytitle='J!D2MASS!N-H!D2MASS!N', charsize=1.5

; J-K vs. SpT
if keyword_set(ps) then begin
    outfile = '~/Astro/699-1/paper/modelplots/jkspt';specplot1'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    window, 21, retain=2, xsize=800, ysize=600
endelse

;; sptaxis = ['M5','L0','L5','T0','T5']
;; sptval = [5,10,15,20,25]
;; plot, sims.SpT[detect], sims.J2[detect]-sims.K2[detect], /nodata, color=0, background=1, $
;;       xtitle='Spectral Type', ytitle='J!D2MASS!N-K!D2MASS!N', charsize=1.5, $
;;       xrange=[5,29], xstyle=1, xtickname=sptaxis, xtickv=spt
sptaxis = ['L0','L1','L2','L3','L4','L5','L6','L7','L8']
sptval = [10,11,12,13,14,15,16,17,18]
plot, sims.SpT[detect], sims.J2[detect]-sims.K2[detect], /nodata, color=0, background=1, $
      xtitle='Spectral Type', ytitle='J!D2MASS!N-K!D2MASS!N', charsize=1.5, xstyle=1, $
      xrange=[9.6,18.4], xticks=8, xtickname=sptaxis, xtickv=sptval, yrange=[0.7,2.6], ystyle=1

gcols = where(finite(sims.J2-sims.K2))
JK = sims.J2[gcols]-sims.K2[gcols]
JKspt = sims.SpT[gcols]
minqspt = 10
maxqspt = 18

; Standard deviation bars, every .5 SpT
sptlist = findgen(2*(maxqspt-minqspt)+1)/2. + minqspt
JKlq = fltarr(2*(maxqspt-minqspt)+1)
JKuq = JKlq
for i=0, 2*(maxqspt-minqspt) do begin
    isp = where(JKspt eq sptlist[i])
    JKi = JK[isp]
    JKmed = median(JKi)
    JKdev = stddev(JKi)
    plots, [sptlist[i], sptlist[i]], [JKmed-JKdev, JKmed+JKdev], color=15, thick=30
endfor

;; ; Quartile bars, every .5 SpT
;; for i=0, 2*(maxqspt-minqspt) do begin
;;     isp = where(JKspt eq sptlist[i])
;;     JKi = JK[isp]
;;     JKlq[i] = JKi[n_elements(isp)/4 - 1]
;;     JKuq[i] = JKi[3*n_elements(isp)/4 - 1]
;;     plots, [sptlist[i], sptlist[i]], [JKlq[i], JKuq[i]], color=3, thick=12
;; endfor

;; ; Standard deviation bars, every 1 SpT
;; sptlist = findgen((maxqspt-minqspt)+1) + minqspt
;; JKlq = fltarr((maxqspt-minqspt)+1)
;; JKuq = JKlq
;; for i=0, (maxqspt-minqspt) do begin
;;     isp = where((JKspt eq sptlist[i]) or (JKspt eq sptlist[i]+.5))
;;     JKi = JK[isp]
;;     JKmed = median(JKi)
;;     JKdev = stddev(JKi)
;;     plots, [sptlist[i], sptlist[i]], [JKmed-JKdev, JKmed+JKdev], color=3, thick=12
;; endfor

;; ; Quartile bars, every 1 SpT
;; for i=0, (maxqspt-minqspt) do begin
;;     isp = where((JKspt eq sptlist[i]) or (JKspt eq sptlist[i]+.5))
;;     JKi = JK[isp]
;;     JKlq[i] = JKi[n_elements(isp)/4 - 1]
;;     JKuq[i] = JKi[3*n_elements(isp)/4 - 1]
;;     plots, [sptlist[i], sptlist[i]], [JKlq[i], JKuq[i]], color=3, thick=12
;; endfor

oplot, sims.SpT[detect], sims.J2[detect]-sims.K2[detect], color=0, psym=4, symsize=0.5
if keyword_set(ps) then ps_close

END
