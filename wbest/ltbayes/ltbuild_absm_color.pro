FUNCTION LTBUILD_ABSM_COLOR, FIXMAGS, SPTLIST, FIXINDEX, INDEX2, ERRIN=errin, ERROUT=errout, $
                             LIST=list, NOCOLERR=nocolerr, SILENT=silent, _EXTRA=extrakey

;  Using  magnitudes in a "fixed" band for objects with known spectral type,
;  generates magnitudes in a second band, using a polynomial fit to colors
;  determined from objects in Kimberly Aller's megatable.  Distibutes
;  magnitudes using errors from the polynomial fit.
;   
;  HISTORY
;  Written by Will Best (IfA), 08/25/2012
;
;  INPUTS
;      FIXMAGS - Vector of known magnitudes in the "fixed" band.
;      SPTLIST - Vector of spectral types of the objects.  Must be the same
;                length as FIXMAGS.
;      FIXINDEX - Index of the band with known magnitudes.
;      INDEX2 - Index of the band for which magnitudes are to be generated.
;
;  OPTIONAL INPUTS:
;      ERRIN - Array of deviations from the model colors that are
;              assigned to each object.  If supplied, they are used to assign the
;               magnitudes.  If not supplied, deviations are generated according
;              to the model.  Suppyling pre-determined deviations can be useful
;              if you want a binary companion to have the same deviation as its
;              primary.
;      ERROUT - Variable for returning an array of deviations from the model
;               colors that are generated in this routine.
;      LIST - If set, the program prints a list of the filters currently
;             available for fitting, then returns.
;      NOCOLERR - Do not use deviations in determining the magnitudes.
;      SILENT - Suppress screen outputs
;

; Filter names
shorts = ['z', 'y', $
         'J2', 'H2', 'K2', $
         'YM', 'JM', 'HM', 'KM', $
         'W1', 'W2', 'W3' ];, 'W4']

; If requested, print the filter names and quit
if keyword_set(list) then begin
    print, 'Currently fitting colors for the following filters:'
    print, shorts, format='(a)'
    return, fixmags
endif

; Choose the right order for BAND1 - BAND2!
b1 = fixindex < index2
b2 = fixindex > index2
if ((b1 eq 2) and (b2 eq 5)) or ((b1 eq 3) and (b2 ge 5) and (b2 le 6)) or $
  ((b1 eq 4) and (b2 ge 5) and (b2 le 7)) then swap, b1, b2
colname = shorts[b1]+'-'+shorts[b2]
if not keyword_set(silent) then print, 'Fitting colors for '+colname

;Which SpT values will be fit by this program?
minspt = min(sptlist, /nan)
maxspt = max(sptlist, /nan)
nfits = (maxspt-minspt)*2+1
sptvec = findgen(nfits)/2. + minspt         ; vector of spectral type values

; Load Kimberly's megatable, to use in determining colors
restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'

; Create arrays for object magnitudes, in each filter
phot = [ [wise_new.z], [wise_new.y], $
         [wise_new.jmag], [wise_new.hmag], [wise_new.kmag], $
         [wise_new.y_2], [wise_new.j], [wise_new.h], [wise_new.k], $
         [wise_new.w1], [wise_new.w2], [wise_new.w3] ];, [wise_new.w4] ]
phot[where(phot le 0)] = !VALUES.F_NAN     ; Turn non-detections into NaN
spt = wise_new.spt                        ; Spectral Type vector
;n = n_elements(shorts)

; Re-format the notes field to remove "        " type notes
quotes = where(strpos(wise_new.note, '"') eq 0)
wise_new[quotes].note = strmid(wise_new[quotes].note, 1, 1#strlen(wise_new[quotes].note)-2)
wise_new.note = trim(wise_new.note)

; Remove binaries
nobin = where(strlen(wise_new.note) eq 0)    ; these objects are not binaries or unusual
phot = phot[nobin,*]
spt = spt[nobin]

; remove spectral types outside the range of the simulated objects
spt = abs(spt)        ; include subdwarfs in the color fitting
goodspt = where((spt ge minspt) and (spt le maxspt))
phot = phot[goodspt,*]
spt = spt[goodspt]

;; ; remove non-detections
;; detect = where((phot[*,b1] gt 0) and (phot[*,b2] gt 0))
;; spt = spt[detect]
;; phot = phot[detect]

modelcolors = fitcolors(phot[*,b1], phot[*,b2], spt, sptvec, fiterr=modelcolerr, $
                        colname=colname, _extra=extrakey, /silent)

match2, sptlist, sptvec, sptmatch
nmatch = long(n_elements(sptmatch))
if keyword_set(nocolerr) then begin
    colors = modelcolors[sptmatch]
endif else begin
    if n_elements(errin) eq 0 then errout = modelcolerr[sptmatch]*randomn(seed, nmatch) $
         else errout = errin
    colors = modelcolors[sptmatch] + errout 
endelse

if fixindex eq b1 then mags2 = fixmags - colors else mags2 = fixmags + colors

return, mags2

END
