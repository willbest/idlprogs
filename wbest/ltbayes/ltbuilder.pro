;; nsims=1
;; area=2026/41253.
;; ;binary=0.
;; density=0.011
;; graph=1
;; index=.25
;; magfix='J2'
;; ;modelfix=1
;; ;noerr=1
;; ;nocolerr=1
;; ;ps=1
;; ;points=1
;; radius=27;5.4
;; ;silent=1
PRO LTBUILDER, NSIMS=nsims, AREA=area, BINARY=binary, DENSITY=density, FLIST=flist, $
               INDEX=index, LOGNORMAL=lognormal, MAGFIX=magfix, MCRIT=mcrit, $
               MAXSPT=maxspt, MINSPT=minspt, MMIN=mmin, MMAX=mmax, MODELFIX=modelfix, $
               MSIGMA=msigma, RADIUS=radius, SFH=sfh, SILENT=silent, _EXTRA=extrakey

;  Builds simulations of L-T transition dwarfs in the Solar neighborhood.
;
;  HISTORY
;  Witten by Will Best (IfA), 08/30/2012
;
;  CALLING SEQUENCE: ltbuilder [, nsims=nsims, area=area, binary=binary, density=density, $
;                         flist=flist, index=index, lognormal=lognormal, magfix=magfix, $
;                         mcrit=mcrit, maxspt=maxspt, minspt=minspt, mmin=mmin, mmax=mmax, $
;                         modelfix=modelfix, msigma=msigma, radius=radius, sfh=sfh, $
;                         silent=silent, _extra=extrakey ]
;
;  OPTIONAL KEYWORD INPUTS:
;      NSIMS - The number of simulations to run before aggregating results.
;              Default is 100.
;      AREA - Fraction of the full sky to simulate.
;             Default: 1 (whole sky)
;      BINARY - Fraction of stars that are binaries.
;               Default: 0.15
;      DENSITY - The number density of brown dwarfs in the Solar
;                neighborhood, in pc^-3.  Default value is 0.01 pc^-3.
;      FLIST - Vector containing the indices of filters, according to the
;              sequence from the megatable:
;                   PS1 z, y; 2MASS J, H, K; UKIDSS Y, J, H, K; WISE W1, W2, W3
;              Default: flist = [1,2,3,4,9,10,11] for: y, 2MASS J, H, K, W1, W2, W3
;      INDEX - If set, the simulation will use a power-law IMF of the form
;                  phi(m) ~ m^(-a)
;              If INDEX is a single value, this value is used for a.
;              If INDEX is a two-element vector, then a two-part power law will
;              be used, with a=INDEX[0] below a critical mass, and a=INDEX[1]
;              above the critical mass.  The critical mass is set by the MCRIT
;              keyword.  If MCRIT is not set, 0.05 Msun will be used.
;           ---> Allen et al (2005) use 0.09 Msun
;              If /LOGNORMAL is set, the value of INDEX is ignored.
;      LOGNORMAL - If set, the simulation will use a log-normal IMF of the form
;                      phi(log m) ~ exp( -(log m - log mcrit)^2 / (2sigma^2) )
;                  The critical mass is set by the MCRIT keyword.  If MCRIT is not
;                  set, 0.25 Msun will be used.
;                  The spread (sigma) is set by the MSIGMA keyword.  If MSIGMA is not
;                  set, 0.5 dex will be used.
;      MAGFIX - Band to which to fix generated absolute magnitudes.
;               If set, absolute magnitudes for each spectral type will be
;               assigned from the values and errors of the model in this band.
;               Use /NOERR to assign these without error.
;               Absolute magnitudes for other bands will be determined by colors
;               and color errors with the other bands.
;               Use /NOCOLERR to assign these without error.
;               Options: z, y, J2, H2, K2, YM, JM, HM, KM, W1, W2, W3
;      MCRIT - Critical mass to be used with the model IMF.
;              If INDEX is a two-element vector (two-part power law), MCRIT has
;              a default value of 0.05 Msun.
;              If LOGNORMAL is set, MCRIT has a default value of 0.25 Msun.
;      MAXSPT - Maximum spectral type to model.  Default is 28 (T8).
;               Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;      MINSPT - Minimum spectral type to model.  Default is 6 (M6).
;      MMAX - Maximum mass of stars to be generated.  Default is 0.1 Msun.
;      MMIN - Minimum mass of stars to be generated.  Default is 0.01 Msun.
;      MODELFIX - Generated magnitudes from the fixed band (MAGFIX) to the
;                 evolutionary model.
;      MSIGMA - Spread for a log-normal IMF.  If LOGNORMAL is set, MSIGMA has a
;               default value of 0.5 dex.
;      RADIUS - The radius of the simulated solar neighborhood, in parsecs.
;               The program rounds RADIUS down to the nearest (long) integer.
;               Default is 100 pc.
;      SFH - Slope to be used for the star formation history function, which has
;            the form  P(age) = slope*age + P(0)
;            Default value is 0 Gyr^-1 (i.e. constant star formation rate over time).
;      SILENT - Suppress outputs to screen.
;
;  FUTURE IMPROVEMENTS
;    Pass seed for generating random numbers to the called routines, or
;they'll generate the same sequences of random numbers as each other!
;    Move Spectral Type into its own module 
;    Move binarity into its own module
;    Move Luminosity and Temperature into its own module
;    Combine all the M filter tags into a single array within the sims and
;      binars structures.  Same for the m filter tags.  This will allow for the
;      removal of some FOR loops, and a flexible number of filters to model.
;

if not keyword_set(silent) then print, systime()

; Set up parameters, use default values as needed
if not keyword_set(density) then density = 0.01                        ; pc^-3
if not keyword_set(radius) then radius = 100                           ; pc
if not keyword_set(area) then area = 1.                                ; fraction of full sky
if not keyword_set(maxspt) then maxspt = 28
if not keyword_set(minspt) then minspt = 6

nstars = long(4./3*!pi*long(radius)^3 * density)
    ; Is long big enough? Max value is 2,147,483,648.  Could use long64.
nstars = nstars * area
if not keyword_set(nsims) then nsims = 100
nsims = long(nsims)
ntot = nstars*nsims

if not keyword_set(mmax) then mmax = 0.1                               ; Msun
if not keyword_set(mmin) then mmin = 0.01                              ; Msun
if mmax lt mmin then swap, mmax, mmin

; Common block for data to be plotted later
common outputs, sims, goodspt, detect, nodetect, nfilts

; Make structure to contain all the simulation data
    ; binaries: in sims.bin, the value is the index in a second structure that
    ;    contains the data on binaries.  Default value for sims.binary is -1.
if not keyword_set(silent) then print, 'Creating the data structure'
; This line sets up an array of #nsims structures.  Each tag contains #nstars values.
;sims = replicate({dist:fltarr(nstars), mass:fltarr(nstars), age:fltarr(nstars), bin:lonarr(nstars), $
;                  lum:fltarr(nstars), Teff:fltarr(nstars), SpT:fltarr(nstars), y:replicate(-999.,nstars), $
;;                  i:replicate(-999.,nstars), z:replicate(-999.,nstars), $
;                  W1:replicate(-999.,nstars), W2:replicate(-999.,nstars), $
;                  W3:replicate(-999.,nstars)}, nsims)

; This line sets up a single structure.  Each tag contains #ntot values.
sims = {dist:fltarr(ntot), mass:fltarr(ntot), age:fltarr(ntot), bin:lonarr(ntot), $
        lum:fltarr(ntot), Teff:fltarr(ntot), SpT:fltarr(ntot), M_y:replicate(!VALUES.F_NAN,ntot), $
;        M_i:replicate(!VALUES.F_NAN,ntot), M_z:replicate(!VALUES.F_NAN,ntot), $
        M_J2:replicate(!VALUES.F_NAN,ntot), M_H2:replicate(!VALUES.F_NAN,ntot), M_K2:replicate(!VALUES.F_NAN,ntot), $
        M_W1:replicate(!VALUES.F_NAN,ntot), M_W2:replicate(!VALUES.F_NAN,ntot), $
        M_W3:replicate(!VALUES.F_NAN,ntot), y:replicate(!VALUES.F_NAN,ntot), $
;        i:replicate(!VALUES.F_NAN,ntot), z:replicate(!VALUES.F_NAN,ntot), $
        J2:replicate(!VALUES.F_NAN,ntot), H2:replicate(!VALUES.F_NAN,ntot), K2:replicate(!VALUES.F_NAN,ntot), $
        W1:replicate(!VALUES.F_NAN,ntot), W2:replicate(!VALUES.F_NAN,ntot), $
        W3:replicate(!VALUES.F_NAN,ntot) }


; Generate the distance for each star
if not keyword_set(silent) then print, 'Generating random distances'
sims.dist = ltbuild_dist(ntot, radius)


; Generate the mass of each star -- returned as logarithmic values
if not keyword_set(silent) then print, 'Generating random masses'
sims.mass = ltbuild_mass(nstars, mmin, mmax, nsims, $
                         index=index, lognormal=lognormal, mcrit=mcrit, silent=silent)


; Generate the age of each star -- returned as logarithmic values
if not keyword_set(silent) then print, 'Generating random ages'
if not keyword_set(sfh) then sfh = 0
sims.age = ltbuild_age(ntot, sfh)


; Binarity
if not keyword_set(silent) then print, 'Generating binary parameters, masses'
if n_elements(binary) eq 0 then binary = 0.15
if binary lt 0 then binary = 0.
if binary gt 0 then begin
    nbinary = long(nstars * binary)
;    sims.bin = lindgen(nstars, nsims)
;    sims.bin[nbinary:*] = -1        ; -1 for objects that are NOT binaries
    sims.bin = lindgen(ntot)
    sims.bin[where((sims.bin mod nstars) ge nbinary)] = -1   ; -1 for objects that are NOT binaries
    binlist = where(sims.bin gt -1)                          ; list of sims indices of binary objects
; Create the structure with information about binary companions
; Distance, age will be the same as the primary
;    binars = replicate({mass:fltarr(nbinary)}, nsims)
    nbintot = nbinary * nsims
    binars = { mass:fltarr(nbintot), lum:fltarr(nbintot), Teff:fltarr(nbintot), SpT:fltarr(nbintot), $
               M_y:replicate(!VALUES.F_NAN,nbintot), $
               M_J2:replicate(!VALUES.F_NAN,nbintot), M_H2:replicate(!VALUES.F_NAN,nbintot), M_K2:replicate(!VALUES.F_NAN,nbintot), $
               M_W1:replicate(!VALUES.F_NAN,nbintot), M_W2:replicate(!VALUES.F_NAN,nbintot), M_W3:replicate(!VALUES.F_NAN,nbintot) }
;;     ; Half-Gaussian distribution for mass ratio of binaries
;;     sigmaq = 0.2
;;     binars.mass = sims.mass[binlist] + alog10(1.-abs(sigmaq*randomn(seed, nbintot)))  ; mass in log form
    ; Power law distribution for mass ratio of binaries, as per Allen (2007)
    gamma = 1.8
    randomp, x, gamma, ntot, range_x=[0.1, 1]
    binars.mass = sims.mass[binlist] + alog10(temporary(x))  ; mass in log form
endif


; Model for interpolating luminosities and temperatures
; Saumon & Marley (2008) hybrid
modelpath = '~/idlprogs/wbest/ltbayes/hybrid_isochrones.dat'
modeltags = 'age:f, mass:f, Teff:f, lum:f, g:f, r:f, mkoY:f, mkoZ:f, mkoJ:f, mkoH:f, mkoK:f, '+$
            'mkoLp:f, mkoMp:f, J2:f'

; Luminosity and Temperature
; Read in model for interpolating luminosities and temperatures
model = read_struct2(modelpath, modeltags, comment='#', /nan, /silent, _EXTRA=ex)
model.age = alog10(model.age)
model.mass = alog10(model.mass)
model.Teff = alog10(model.Teff)
model.J2 = alog10(model.J2)
triangulate, model.age, model.mass, tr
; mass and age ---> [model] ---> luminosity
if not keyword_set(silent) then print, 'Interpolating luminosities'
sims.lum = griddata(model.age, model.mass, model.lum, xout=sims.age, yout=sims.mass, $
                    /linear, triangles=tr, missing=Nan)                ; log(Lbol/Lsun)
if binary gt 0 then binars.lum = griddata(model.age, model.mass, model.lum, xout=sims.age[binlist], $
                                          yout=binars.mass, /linear, triangles=tr, missing=Nan)
; mass and age ---> [model] ---> temperature
if not keyword_set(silent) then print, 'Interpolating effective temperatures'
sims.Teff = griddata(model.age, model.mass, model.Teff, xout=sims.age, yout=sims.mass, $
                     /linear, triangles=tr, missing=Nan)               ; K
sims.Teff = 10^temporary(sims.Teff)    ; convert back to regular units
; Same steps for binaries
if binary gt 0 then begin
    binars.Teff = griddata(model.age, model.mass, model.Teff, xout=sims.age[binlist], $
                           yout=binars.mass, /linear, triangles=tr, missing=Nan)
    binars.Teff = 10^temporary(binars.Teff)     ; convert back to regular units
endif


if not keyword_set(silent) then print, 'Assigning Spectral Types from effective temperatures'
; Spectral Type, using Table 6 from Golimowski et al (2004).
;; ; I fit a polynomial to SpT as a function of Teff.  The coefficients are:
;; cSpT = [ 40.346539,  -0.014774, -4.86242e-07, 5.13256e-10]                               ; 3rd degree
;; cSpT = [-14.036113,   0.163334, -2.20703e-04,  1.29705e-07, -3.61549e-11, 3.88183e-15]   ; 5th degree
;; sims.SpT = poly(sims.Teff, cSpT)
; I fit a logistic function, using logistic.pro, to SpT as a function of Teff.
; The coefficients for this function are:
cSpT = [0.006856, 1.001071, 0.019012]          ; logistic
sims.SpT = logistic(sims.Teff, cSpT)           ; get SpT from fit

; Get error with 4 bins: 10 points with T>2000, 11 with 1600<T<2000, 12 with 1250<T<1600, 12 with T<1250
chunk = [0, 1250, 1600, 2000]
chunkerr = [1.222, 1.931, 3.194, 0.964]
dummy = lookup(sims.Teff, chunk, chunkerr, SpTerrvec)
scatSpT = temporary(SpTerrvec) * randomn(seed, ntot)
sims.SpT = temporary(sims.SpT) + scatSpT   ; add Gaussian scatter to SpT
;; scat = 2.;0.                                   ; scatter in Spectal Type
;; if scat ne 0 then begin
;;     scatSpT = scat * randomn(seed, ntot)
;;     sims.SpT = temporary(sims.SpT) + scatSpT   ; add Gaussian scatter to SpT
;; endif
sims.SpT = round(2*temporary(sims.SpT))/2.     ; round SpT to nearest half-integer

; Same steps for binaries
if binary gt 0 then begin
    binars.SpT = logistic(binars.Teff, cSpT)
;;     if scat ne 0 then begin
        ; Use same Spt scatter value for each companion as for its primary
        binars.SpT = temporary(binars.SpT) + scatSpT[binlist]
;;     endif
    binars.SpT = round(2*temporary(binars.SpT))/2. ; round SpT to nearest half-integer
endif

; Flag data with unrealistic or unneeded spectral types
if not keyword_set(silent) then print, 'Rejecting objects with unneeded Spectral Types'
badspt = where((sims.SpT gt maxspt) or (sims.SpT lt minspt), comp=goodspt)
                                        ; goodspt is the list of SIMS indices for good objects
sims.Teff[badspt] = !VALUES.F_NAN;-999
sims.lum[badspt] = !VALUES.F_NAN;-999
sims.SpT[badspt] = !VALUES.F_NAN;-999
if binary gt 0 then begin
    ; Flag the binary companions whose primaries have already been flagged
    sims.bin[badspt] = !VALUES.F_NAN;-999
    badbin = where((binars.SpT gt maxspt) or (binars.SpT lt minspt) or (sims.bin[binlist] lt 0), $
                       comp=goodbin)    ; goodbin is the list of BINARS indices for good companions
    ; Flag the binary companions with unrealistic or unneeded spectral types
    binars.Teff[badbin] = !VALUES.F_NAN;-999
    binars.lum[badbin] = !VALUES.F_NAN;-999
    binars.SpT[badbin] = !VALUES.F_NAN;-999
    remove, badbin, binlist             ; binlist is the list of SIMS indices for good companions
                                        ; binlist and goodbin are of equal length
                                        ; **binlist is a subset of goodspt**
endif


; Absolute magnitudes, using Kimberly's megatable
if not keyword_set(silent) then print, 'Generating absolute magnitudes'

; List of filters for which to obtain absolute magnitudes
if n_elements(flist) eq 0 then flist = [1,2,3,4,9,10,11]       ; y, J2, H2, K2, W1, W2, W3
nfilts = n_elements(flist)

; Load a structure called 'fits', with tags:  name, short, spt, M, Merr
restore, file='~/idlprogs/wbest/ltbayes/Mfits.sav'

if n_elements(magfix) gt 0 then begin
; Get the absolute magnitudes in one 'fixed' band
    fixindex = where(magfix eq fits.short)
;    fixindex = fixindex[0]

    if keyword_set(modelfix) then begin
    ; mass and age ---> [model] ---> absolute magnitude
        if not keyword_set(silent) then print, $
          'Interpolating '+magfix+' magnitudes from the evolutionary model'
        fixmags = griddata(model.age, model.mass, model.J2, xout=sims.age[goodspt], $
                           yout=sims.mass[goodspt], /linear, triangles=tr, missing=Nan)
        fixmags = 10^temporary(fixmags)            ; convert back to linear units
        ; Same steps for binaries
        if binary gt 0 then begin
            fixbinmags = griddata(model.age, model.mass, model.J2, xout=sims.age[binlist], $
                                  yout=binars.mass[goodbin], /linear, triangles=tr, missing=Nan)
            fixbinmags = 10^temporary(fixbinmags) ; convert back to linear units
        endif

    endif else begin
    ; Get the empirical absolute magnitudes in one 'fixed' band
        fixmags = ltbuild_absm_poly(fits[fixindex].M, fits[fixindex].Merr, $
                    fits[fixindex].spt, sims.SpT[goodspt], errout=scatm, _extra=extrakey)
        ; Use same Spt scatter value for each companion as for its primary
        if binary gt 0 then $
          fixbinmags = ltbuild_absm_poly(fits[fixindex].M, fits[fixindex].Merr, $
                         fits[fixindex].spt, binars.SpT[goodbin], errin=scatm, _extra=extrakey)
    endelse

    ; Get the absolute magnitudes for the other bands via colors
    for i=0, nfilts-1 do begin
        if flist[i] eq fixindex then begin
            sims.(i+7)[goodspt] = fixmags
            if binary gt 0 then binars.(i+4)[goodbin] = fixbinmags
            continue
        endif else begin
            sims.(i+7)[goodspt] = ltbuild_absm_color(fixmags, sims.SpT[goodspt], fixindex, $
                                    flist[i], errout=scatm, graph=graph, /silent, _extra=extrakey)
            ; Use same Spt scatter value for each companion as for its primary
            ; Get the absolute magnitudes of the binary companions using the same steps
            if binary gt 0 then $
              binars.(i+4)[goodbin] = ltbuild_absm_color(fixbinmags, binars.SpT[goodbin], fixindex, $
                                        flist[i], errin=scatm, /silent, _extra=extrakey)
            ; At this point, keeping the primary and companion abs mags separate.
        endelse
    endfor

endif else begin
; Get the absolute magnitudes for all bands based on megatable mags and errors
    for i=0, nfilts-1 do begin
        sims.(i+7)[goodspt] = ltbuild_absm_poly(fits[flist[i]].M, fits[flist[i]].Merr, $
                    fits[flist[i]].spt, sims.SpT[goodspt], errout=scatm, _extra=extrakey)
        ; Use same Spt scatter value for each companion as for its primary
        ; Get the absolute magnitudes of the binary companions using the same steps
        if binary gt 0 then begin
            binars.(i+4)[goodbin] = ltbuild_absm_poly(fits[flist[i]].M, fits[flist[i]].Merr, $
                          fits[flist[i]].spt, binars.SpT[goodbin], errin=scatm, _extra=extrakey)
        ; At this point, keeping the primary and companion abs mags separate.
        endif
    endfor
endelse


; Apparent magnitudes
for i=0, nfilts-1 do begin
    ; Formula: m = M + 5.*alog10(distance) - 5.
    sims.(i+7+nfilts)[goodspt] = sims.(i+7)[goodspt] + 5.*alog10(sims.dist[goodspt]) - 5.
    ; If the object is a binary, convolve the two apparent magnitudes
    if binary gt 0 then begin
        sims.(i+7+nfilts)[binlist] = -2.5*alog10(10^(temporary(sims.(i+7+nfilts)[binlist])/(-2.5)) + $
                         10^((binars.(i+4)[goodbin] + 5.*alog10(sims.dist[binlist]) - 5.)/(-2.5)))
    endif
endfor


; Knock out the objects beyond the detection limits

; Limiting magnitudes for the surveys.
; Strong version:  something that incorporates the empirical mean and
; distribution of the magnitudes of each spectral type at the S/N limit for the
; sample.
; [strong version here]
; Simple version:  use the published/estimated limits
;limmag = [20.3, 18.1, 16.8, 12.7]   ; {y, W1, W2, W3} at S/N={5, 2, 2, 2}
;limmag = [20.3, 17.06, 16.17, 15.56, 18.1, 16.8, 12.7]   ; {y, J2, H2, K2, W1, W2, W3} at S/N={5, 5, 5, 5, 2, 2, 2}
limmag = [19.5, 17.06, 16.17, 15.56, 16.35, 15.3, 12.7]   ; {y, J2, H2, K2, W1, W2, W3} at S/N={10, 5, 5, 5, 10, 8, 2}

; Set NaN for apparent magnitudes beyond the limits
;; detstr = 'detect = where((sims.(7+nfilts) le limmag[0])'
;; for i=1, nfilts-2 do begin       ; -2 means to allow W3 non-detections
;;     detstr = detstr + ' and (sims.('+trim(i)+'+7+nfilts) le limmag['+trim(i)+'])'
;; endfor
;; detstr = detstr + ', comp=nodetect)'
;; dummy = execute(detstr)
detect = where((sims.(7+nfilts) le limmag[0]) and (sims.(11+nfilts) le limmag[4]) and $
               (sims.(12+nfilts) le limmag[5]), comp=nodetect)

for i=0, nfilts-2 do begin       ; -2 means to allow W3 non-detections
    sims.(i+7+nfilts)[nodetect] = !VALUES.F_NAN
endfor


if not keyword_set(silent) then print, systime()

END
