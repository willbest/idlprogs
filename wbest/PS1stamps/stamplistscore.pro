PRO STAMPLISTSCORE, PROJECT, SAMPLE, BASEPATH, STAMPPATH, $
                   FILT=filt, LIST=list, MJDMAX=mjdmax, MJDMIN=mjdmin

;  Called by stamplists.pro.  Loads a data structure returned by the Vizier
;  database, and converts coordinates into lists of various formats, for
;  obtaining object images from multiple surveys.
;  Format #1: <id#>, <ra2010>, <dec2010>   [no header]
;     Used by stampmaker, to create Pan-STARRS postage stamp request forms.
;     Output file #1:  <sample>.ascii
;  Format #2: id<id#>, <ra2010>, <dec2010>   [no header]
;     Used by fetch.py on Westfield, to get 2MASS, DSS and SDSS images.
;     Output file #2:  <sample>id.ascii
;  Format #3: <id#>, <ra2010>, <dec2010>  [with ipac header]
;     Used to manually download WISE images from the ipac website.
;     Output file #3:  <sample>.ipac
;  Format #4: <id#>, <racenter>, <decenter>, <ra1999>, <dec1999>, <ra2010>,
;                <dec2010>   [text header]
;     Used by fileripper3.pro, to create a list that listingsrev.pro can use.
;     Output file #4:  <sample>move.ascii
;
;  Copies file #1 to westfield:~/<project>/
;  Copies file #2 to westfield:~/fetch/<project>/
;  Copies file #4 to westfield:~/<project>/<sample>/
;
;  Runs stampmaker on this list, and copies the .tab files over to
;  ipp022:~/tabs/<project>/<sample>/
;
;  HISTORY
;  Written by Will Best (IfA), 07/24/2012
;
;  INPUTS
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;       FILT - Bit-wise code for which filters to include:
;                1 - g    2 - r    4 - i    8 - z    16 - y
;              e.g. 2 = r only
;                   5 = g and i only
;                   29 = g, i, z, y
;                   31 = all five filters (DEFAULT)
;      LIST - ascii file in which the first two columns are RA and DEC at an
;             earlier position (e.g. 1999), and the next two columns are RA and
;             DEC at a later position (e.g. 2010), in decimal degrees (unless
;             SIX keyword is set).
;             If the IDN keyword is set, the first column must be the
;             i.d. numbers, followed by the RA and DEC at two positions.
;      MJDMAX - Latest date for image search
;               Default is 0, which means no upper limit
;      MJDMIN - Earliest date for image search
;               Default is 55700 == May 19, 2011
;               Earliest date = 54983 == June 1, 2009
;

; Check for parameters
if n_params() lt 4 then message, $
  "Use:  stamplistscore, '<project>', '<sample>', '<basepath>', '<stamppath>' $"+$
  "      [, FILT=filt, LIST=list, MJDMAX=mjdmax, MJDMIN=mjdmin]"

; Check for input list
if n_elements(list) eq 0 then list = '~/Astro/PS1stamps/getstamp.sav'
; Load data into named ("merge") structure array called goodlist.
restore, list

; Retrieve unique elements from goodlist
uind = rem_dup(goodlist.id)
ulist = goodlist[uind]

; Set the cutoff dates for PS1 images
if n_elements(mjdmin) eq 0 then mjdmin = 55700
mjdmin = trim(mjdmin)
if n_elements(mjdmax) eq 0 then mjdmax = 0
mjdmax = trim(mjdmax)

; Create output files
print, 'Printing first list to '+textout
textout = basepath+project+'/'+sample+'.ascii'
forprint, ulist.id, ulist.rap, ulist.dep, textout=textout, $
  format='(3x,i7,4x,d10.6,4x,d10.6)', /nocomment, /silent

print, 'Printing second list to '+textoutid
textoutid = basepath+project+'/'+sample+'id.ascii'
idcol = strcompress('id'+string(ulist.id),/remove_all)
forprint, idcol, ulist.rap, ulist.dep, textout=textoutid, $
  format='(1x,a9,4x,d10.6,4x,d10.6)', /nocomment, /silent

print, 'Printing third list to '+textoutip
textoutip = basepath+project+'/'+sample+'.ipac'
header = '|        ra|       dec|'+string(13b)+string(10b)+'|    double|    double|'+string(13b)+string(10b)+'|       deg|       deg|'
forprint, ulist.rap, ulist.dep, textout=textoutip, format='(1x,d10.6,1x,d10.6)', comment=header, /silent

print, 'Printing fourth list to '+textoutmove
textoutmove = basepath+project+'/'+sample+'move.ascii'
forprint, ulist.id, ulist.racenter, ulist.decenter, ulist.ra2, ulist.de2, $
  ulist.rap, ulist.dep, textout=textoutmove, /silent, $
  format='(3x,i7,4x,d10.6,4x,d10.6,4x,d10.6,4x,d10.6,4x,d10.6,4x,d10.6)'

spawn, 'scp '+textout+' wbest@westfield.ifa.hawaii.edu:~/'+project+'/.'
spawn, 'scp '+textoutid+' wbest@westfield.ifa.hawaii.edu:~/fetch/'+project+'/.'
spawn, 'scp '+textoutmove+' wbest@westfield.ifa.hawaii.edu:~/'+project+'/'+sample+'/.'

; Run stampmaker
if not keyword_set(filt) then filt = 31

if (filt and 1) ne 0 then spawn, stamppath+' '+textout+' '+sample+' g '+mjdmin+' '+mjdmax
if (filt and 2) ne 0 then spawn, stamppath+' '+textout+' '+sample+' r '+mjdmin+' '+mjdmax
if (filt and 4) ne 0 then spawn, stamppath+' '+textout+' '+sample+' i '+mjdmin+' '+mjdmax
if (filt and 8) ne 0 then spawn, stamppath+' '+textout+' '+sample+' z '+mjdmin+' '+mjdmax
if (filt and 16) ne 0 then spawn, stamppath+' '+textout+' '+sample+' y '+mjdmin+' '+mjdmax

; Copy tab files to ipp022
spawn, 'mv *.tab '+basepath+project+'/'+sample+'/'
spawn, 'scp -r '+basepath+project+'/'+sample+'/ wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/'

END
