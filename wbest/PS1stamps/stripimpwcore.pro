PRO STRIPIMPWCORE, PROJECT, SAMPLE, LISTPATH, MASTERPATH, PSPATH

;  Remove all the unused PS1 images from the Westfield directories.
;
;  Configured for the PS1+WISE search
; 
;  HISTORY
;  Written by Will Best, 09/21/2012
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans).
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo).
;      LISTPATH - Location of ascii file with target coordinates.
;      MASTERPATH - Location of FETCH image files.
;      PSPATH - Location of Pan-STARRS image files.
;

; Check for parameters
if n_params() lt 4 then message, $
  "Use:  revimpwcore, '<project>', '<sample>', '<listpath>', '<pspath>'"

; Read in the object coordinates
readcol, listpath, idnum, ra2010, dec2010, format='a,d,d', comment='#', /silent
n=n_elements(idnum)

; Read in FETCH folder names
size=20.0
spawn, 'ls -d ' + masterpath  + '/name=id*', folderlist

; Get the target names (which are id numbers) and coordinates from the FETCH directory names
if not keyword_set(silent) then print, 'Obtaining FETCH object coordinates'
npos = strpos(folderlist, 'name=id') + 7
rafrgx = 'coord=[^ ]+ [^ ]+ [^ ]+ '        ; regular expression to look for
fpos = stregex(folderlist, rafrgx, length=flen)
name = strmid(folderlist, 1#npos, 1#fpos-1#npos-1)
rafpos = fpos + 6
decfpos = fpos + flen
decfstop = strpos(folderlist, ':arcsec')
rafstr = strmid(folderlist, 1#rafpos, 1#decfpos-1#rafpos-1)
decfstr = strmid(folderlist, 1#decfpos, 1#decfstop-1#decfpos)
raf = tenv(rafstr) * 15.
decf = tenv(decfstr)

for i=0, n-1 do begin

; Match the name and idnum for the object
    l = where(idnum[i] eq name)
    print, folderlist[l]
; Check that the coordinates also match
    gcirc, 2, ra2010[i], dec2010[i], raf[l], decf[l], sep
    if sep gt 3.0 then begin
        print, 'FETCH and List coordinates do not match!'
        stop
    endif

; Has this object already been reviewed?  If so, go to the next.
    print, 'name = ', idnum[i]
    print, ra2010[i], dec2010[i]

; Find the images for an object
    spawn, 'ls ' + pspath + 'g/' + name[l] + '_*' , psglist
    spawn, 'ls ' + pspath + 'r/' + name[l] + '_*' , psrlist
    spawn, 'ls ' + pspath + 'i/' + name[l] + '_*' , psilist
    spawn, 'ls ' + pspath + 'z/' + name[l] + '_*' , pszlist
    spawn, 'ls ' + pspath + 'y/' + name[l] + '_*' , psylist

; Get g image   
    print, 'g band'
    gdummyflag=0
    if ((rstrpos(psglist[0],'/') lt 0) or (n_elements(psglist) lt 1)) then begin
        gdummyflag=1
    endif else begin
        for gindex=0, n_elements(psglist)-1 do begin
            fits_read, psglist[gindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                print, psglist[gindex]
                break
            endif
        endfor
        if n_elements(psglist) gt 1 then begin
            if gindex eq n_elements(psglist) then gindex = 0
            remove, gindex, psglist
            for jj=0, n_elements(psglist)-1 do spawn, 'rm '+psglist[jj]
        endif
    endelse
    
; Get r image   
    print, 'r band'
    rdummyflag=0
    if ((rstrpos(psrlist[0],'/') lt 0) or (n_elements(psrlist) lt 1)) then begin
        rdummyflag=1
    endif else begin
        for rindex=0, n_elements(psrlist)-1 do begin
            fits_read, psrlist[rindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                print, psrlist[rindex]
                break
            endif
        endfor
        if n_elements(psrlist) gt 1 then begin
            if rindex eq n_elements(psrlist) then rindex = 0
            remove, rindex, psrlist
            for jj=0, n_elements(psrlist)-1 do spawn, 'rm '+psrlist[jj]
        endif
    endelse
    
; Get i image   
    print, 'i band'
    idummyflag=0
    if ((rstrpos(psilist[0],'/') lt 0) or (n_elements(psilist) lt 1)) then begin
        idummyflag=1
    endif else begin
        for iindex=0, n_elements(psilist)-1 do begin
            fits_read, psilist[iindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                print, psilist[iindex]
                break
            endif
        endfor
        if n_elements(psilist) gt 1 then begin
            if iindex eq n_elements(psilist) then iindex = 0
            remove, iindex, psilist
            for jj=0, n_elements(psilist)-1 do spawn, 'rm '+psilist[jj]
        endif
    endelse
    
; Get z image   
    print, 'z band'
    zdummyflag=0
    if ((rstrpos(pszlist[0],'/') lt 0) or (n_elements(pszlist) lt 1)) then begin
        zdummyflag=1
    endif else begin
        for zindex=0, n_elements(pszlist)-1 do begin
            fits_read, pszlist[zindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                print, pszlist[zindex]
                break
            endif
        endfor
        if n_elements(pszlist) gt 1 then begin
            if zindex eq n_elements(pszlist) then zindex = 0
            remove, zindex, pszlist
            for jj=0, n_elements(pszlist)-1 do spawn, 'rm '+pszlist[jj]
        endif
    endelse
    
; Get y image   
    print, 'y band'
    ydummyflag=0
    if ((rstrpos(psylist[0],'/') lt 0) or (n_elements(psylist) lt 1)) then begin
        ydummyflag=1
    endif else begin
        for yindex=0, n_elements(psylist)-1 do begin
            fits_read, psylist[yindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                print, psylist[yindex]
                break
            endif
        endfor
    endelse
    
endfor


END
