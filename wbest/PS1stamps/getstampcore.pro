PRO GETSTAMPCORE, PROJECT, SAMPLE, BASEPATH, STAMPPATH, $
                   FILT=filt, IDN=idn, LIST=list, MJDMAX=mjdmax, MJDMIN=mjdmin, SIX=six

;  Called by getstamps.pro.  This is the core routine that converts input
;  coordinates into lists of various formats, for obtaining object images from
;  multiple surveys.
;  Format #1: <id#>, <ra2010>, <dec2010>   [no header]
;     Used by stampmaker, to create Pan-STARRS postage stamp request forms.
;     Output file #1:  <sample>.ascii
;  Format #2: id<id#>, <ra2010>, <dec2010>   [no header]
;     Used by fetch.py on Westfield, to get 2MASS, DSS and SDSS images.
;     Output file #2:  <sample>id.ascii
;  Format #3: <id#>, <ra2010>, <dec2010>  [with ipac header]
;     Used to manually download WISE images from the ipac website.
;     Output file #3:  <sample>.ipac
;
;  Copies file #1 to westfield:~/<project>/
;  Copies file #2 to westfield:~/fetch/<project>/
;
;  Runs stampmaker on this list, and copies the .tab files over to
;  ipp022:~/tabs/<project>/<sample>/
;  
;  HISTORY
;  Written by Will Best (IfA), 07/24/2012
;
;  INPUTS
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;       FILT - Bit-wise code for which filters to include
;                1 - g    2 - r    4 - i    8 - z    16 - y
;              e.g. 2 = r only
;                   5 = g and i only
;                   29 = g, i, z, y
;                   31 = all five filters (DEFAULT)
;      IDN - Input target list already has i.d. numbers for the targets.  The
;            i.d. numbers must be the first column.
;      LIST - ascii file in which the first two columns are RA and DEC,
;             in decimal degrees (unless SIX keyword is set).
;             If the IDN keyword is set, the first column must be the
;             i.d. numbers, followed by the RA and DEC.
;      MJDMAX - Latest date for image search
;               Default is 0, which means no upper limit
;      MJDMIN - Earliest date for image search
;               Default is 55700 == May 19, 2011
;               Earliest date = 54983 == June 1, 2009
;      SIX - If set, LIST must be an ascii file in which the first six
;               columns are RA and DEC in sexigesimal
;

; Check for parameters
if n_params() lt 4 then message, $
  "Use:  getstampcore, '<project>', '<sample>', '<basepath>', '<stamppath>' "+$
  "[, FILT=filt, IDN=idn, LIST=list, MJDMAX=mjdmax, MJDMIN=mjdmin, SIX=six]"

; Check for input list
if n_elements(list) eq 0 then list = basepath+'targlist.ascii'

; Read in and check the ra and dec values
if keyword_set(six) then begin
    if keyword_set(idn) then readcol, list, n, a, b, c, d, e, f, format='l,d,d,d,d,d,d', comment='#', /silent else readcol, list, a, b, c, d, e, f, format='d,d,d,d,d,d', comment='#', /silent
    a = tenv(a,b,c)
    if (min(a) lt 0) or (max(a) ge 360) then $
      message, 'RA must be values between 0 and 359.99999'
    b = tenv(d,e,f)
    if (min(b) lt -90) or (max(b) gt 90) then $
      message, 'Dec must be values between -90 and 90'
endif else begin
    if keyword_set(idn) then readcol, list, n, a, b, format='l,d,d', comment='#', /silent $
      else readcol, list, a, b, format='d,d', comment='#', /silent
    if (min(a) lt 0) or (max(a) ge 360) then $
      message, 'RA must be values between 0 and 359.99999'
    if (min(b) lt -90) or (max(b) gt 90) then $
      message, 'Dec must be values between -90 and 90'
endelse

; Remove objects with duplicate id numbers
if keyword_set(idn) then begin
    uind = rem_dup(n)
    n = n[uind]
    a = a[uind]
    b = b[uind]
endif

if n_elements(mjdmin) eq 0 then mjdmin = 55700
mjdmin = trim(mjdmin)
if n_elements(mjdmax) eq 0 then mjdmax = 0
mjdmax = trim(mjdmax)

; Create output files
if not keyword_set(idn) then n = indgen(n_elements(a))+1
textout = basepath+project+'/'+sample+'.ascii'
forprint, n, a, b, textout=textout, format='(1x,i7,4x,d10.6,4x,d10.6)', /nocomment, /silent

n = 'id'+trim(n)
textoutid = basepath+project+'/'+sample+'id.ascii'
forprint, n, a, b, textout=textoutid, format='(1x,a9,4x,d10.6,4x,d10.6)', /nocomment, /silent

textoutip = basepath+project+'/'+sample+'.ipac'
header = '|        ra|       dec|'+string(13b)+string(10b)+'|    double|    double|'+string(13b)+string(10b)+'|       deg|       deg|'
forprint, a, b, textout=textoutip, format='(1x,d10.6,1x,d10.6)', comment=header, /silent

spawn, 'scp '+textout+' wbest@westfield.ifa.hawaii.edu:~/'+project+'/.'
spawn, 'scp '+textoutid+' wbest@westfield.ifa.hawaii.edu:~/fetch/'+project+'/.'

; Run stampmaker
if not keyword_set(filt) then filt = 31

if (filt and 1) ne 0 then spawn, stamppath+' '+textout+' '+sample+' g '+mjdmin+' '+mjdmax
if (filt and 2) ne 0 then spawn, stamppath+' '+textout+' '+sample+' r '+mjdmin+' '+mjdmax
if (filt and 4) ne 0 then spawn, stamppath+' '+textout+' '+sample+' i '+mjdmin+' '+mjdmax
if (filt and 8) ne 0 then spawn, stamppath+' '+textout+' '+sample+' z '+mjdmin+' '+mjdmax
if (filt and 16) ne 0 then spawn, stamppath+' '+textout+' '+sample+' y '+mjdmin+' '+mjdmax

;; if (filt and 1) ne 0 then spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' g '+mjdmin+' '+mjdmax;+' > messageg.txt'
;; if (filt and 2) ne 0 then spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' r '+mjdmin+' '+mjdmax;+' > messager.txt'
;; if (filt and 4) ne 0 then spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' i '+mjdmin+' '+mjdmax;+' > messagei.txt'
;; if (filt and 8) ne 0 then spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' z '+mjdmin+' '+mjdmax;+' > messagez.txt'
;; if (filt and 16) ne 0 then spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' y '+mjdmin+' '+mjdmax;+' > messagey.txt'

;; if (filt and 1) ne 0 then begin
;;     spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' g '+mjdmin+' '+mjdmax+' > ;messageg.txt'
;;     readcol, 'messageg.txt', p, format='a', delim=':', /silent
;;     tabname = strmid(p[0], 14)
;;     spawn, 'scp '+tabname+' wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/.'
;; endif

;; if (filt and 2) ne 0 then begin
;;     spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' r '+mjdmin+' '+mjdmax+' > messager.txt'
;;     readcol, 'messager.txt', p, format='a', delim=':', /silent
;;     tabname = strmid(p[0], 14)
;;     spawn, 'scp '+tabname+' wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/.'
;; endif

;; if (filt and 4) ne 0 then begin
;;     spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' i '+mjdmin+' '+mjdmax+' > messagei.txt'
;;     readcol, 'messagei.txt', p, format='a', delim=':', /silent
;;     tabname = strmid(p[0], 14)
;;     spawn, 'scp '+tabname+' wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/.'
;; endif

;; if (filt and 8) ne 0 then begin
;;     spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' z '+mjdmin+' '+mjdmax+' > messagez.txt'
;;     readcol, 'messagez.txt', p, format='a', delim=':', /silent
;;     tabname = strmid(p[0], 14)
;;     spawn, 'scp '+tabname+' wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/.'
;; endif

;; if (filt and 16) ne 0 then begin
;;     spawn, '~/idlprogs/wbest/PS1stamps/stampmaker '+sample+'.ascii '+sample+' y '+mjdmin+' '+mjdmax+' > messagey.txt'
;;     readcol, 'messagey.txt', p, format='a', delim=':', /silent
;;     tabname = strmid(p[0], 15)
;;     spawn, 'scp '+tabname+' wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/.'
;; endif

; Copy tab files to ipp022
spawn, 'mv *.tab '+basepath+project+'/'+sample+'/'
spawn, 'scp -r '+basepath+project+'/'+sample+'/ wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/'

END
