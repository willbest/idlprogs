#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/*gcc stampmaker.c -o stampmaker*/

/*Takes a list of decimal degree positions and produces output tables to be turned into FITS tables for the Pan-STARRS postage stamp server - Niall Deacon*/

/*The required input filter format is inputfile name filtername MJD_min MJD_max. The output files will be the name you give it + unix timestamp + filter + number of the output*/

/*The required input file format is "idno ra dec"*/

int main(int argc, char *argv[])
{
  int count,idno,filecount,mjdmin,mjdmax;
  double ra,dec;
  long t1;
  FILE *input,*output;
  char scanner[500],inname[50],outname[50],outname1[53];
  count=1;
  if(argc!=6)
    {
      printf("Poorly formatted input\n");
      exit(0);
    }
  sscanf(argv[4],"%d",&mjdmin);
  sscanf(argv[5],"%d",&mjdmax);
  if((input = fopen(argv[1],"r"))!=NULL)
    {
      time(&t1);
      filecount=1;
      sprintf(outname,"%s%ld%s%d",argv[2],t1,argv[3],filecount);
      sprintf(outname1,"%s%ld%s%d.tab",argv[2],t1,argv[3],filecount);
      printf("Output file is %s\n",outname1);
      printf("Job name is %s\n",outname);
      output=fopen(outname1,"w");
      fprintf(output,"#%s\n# REQ_NAME           EXTVER\n%s     1\n# ID    |     ROI Specification                   |  JOB Specification | Images of interest specification\n# ROWNUM CENTER_X    CENTER_Y  WIDTH HEIGHT COORD_MASK JOB_TYPE OPTION_MASK PROJECT REQ_TYPE IMG_TYPE    ID   TESS_ID COMPONENT    DATA_GROUP  REQFILT MJD_MIN MJD_MAX | COMMENT\n",outname1,outname); 
      while(fgets(scanner,500,input)!=NULL)
	{
	  sscanf(scanner,"%d %lf %lf",&idno,&ra,&dec);
	  fprintf(output,"%5.5d       %lf     %lf 1000    1000      2        stamp     65537      gpc1    bycoord   chip       null    null   null       ThreePi%%       %s     %d     %d     |\n",idno,ra,dec,argv[3],mjdmin,mjdmax);
	      /*sscanf(scanner,"%lf %lf",&ra,&dec);                        
	    fprintf(output,"%5.5d       %lf     %lf 1000    1000      2        stamp       1        gpc1    bycoord   chip       null    null   null           null       %s  54952      55255      |search by coords (make the server work)\n",count,ra,dec,argv[3]);*/
	  count++;
	  if(((count%50)==0)&&(count>1))
	    {
	      /*Start new output file*/
	      filecount++;
	      fclose(output);
	      sprintf(outname,"%s%ld%s%d",argv[2],t1,argv[3],filecount);
	      sprintf(outname1,"%s%ld%s%d.tab",argv[2],t1,argv[3],filecount);
	      printf("New output file is %s\n",outname1);
	      printf("New job name is %s\n",outname);
	      output=fopen(outname1,"w");
	      fprintf(output,"#%s\n# REQ_NAME           EXTVER\n%s     1\n# ID    |     ROI Specification                   |  JOB Specification | Images of interest specification\n# ROWNUM CENTER_X    CENTER_Y  WIDTH HEIGHT COORD_MASK JOB_TYPE OPTION_MASK PROJECT REQ_TYPE IMG_TYPE    ID   TESS_ID COMPONENT    DATA_GROUP  REQFILT MJD_MIN MJD_MAX | COMMENT\n",outname1,outname);

	    }
	}
      fclose(output);
    }
  else
    {
      printf("Input file not found\n"); 
      exit(0); 
    }
}
