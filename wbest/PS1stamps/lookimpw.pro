PRO LOOKIMPW, PROJECT, SAMPLE, LAPTOP=laptop, _EXTRA=ex

;  Configured to run on Westfield.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  lookimpwcore.pro.
;
;  Displays a set of images of a list of objects, for quick comparison and
;  evaluation.
;
;  Accepts an ascii list of targets.  If the NOLIST keyword is set, the program will
;  query the user for a target id number (0 to quit).
;
;  Configured for the PS1+WISE search
;  Currently displays:  PS1 grizy, 2MASS JHK, WISE W123.
; 
;  HISTORY
;  Adapted by Will Best from revimpw.pro on 09/21/2012.
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      LAPTOP - Configure spacing of image windows for my laptop.
;      LISTPATH - Location of ascii file with target coordinates
;                 Default: '~/<project>/<sample>look.ascii'
;      NOLIST - query the user for id numbers for images to look at.
;        ***DOES NOT WORK YET***
;      WINNUM - Vector containing the window id numbers to use in opening the
;               image windows.  Works with win.pro.
;               Default is a vector that works on Will's Sunray terminal.
;      WINSIZE - Size of image windows.  Default: 200.
;

; Establish path to base folder for all this stuff
;; basepath = '~/'
basepath = '/Volumes/Hanalei/wbest/'

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

; Set up the file paths
if n_elements(listpath) eq 0 then listpath = basepath+project+'/'+sample+'look.ascii'
masterpath = basepath+'fetch/'+project+'/'+sample+'id.ascii_FETCH/'
pspath = basepath+project+'/'+sample+'/'+sample+'.'

; Prepare the image windows
if keyword_set(laptop) then begin
    winnum = [0,1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
    wsize = 200
endif else begin
    winnum = [0,1,2,3,4,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
    wsize = 200
endelse

; Run through all the objects
if keyword_set(nolist) then begin
    lookimpwcore, project, sample, masterpath, pspath, $
                  winnum=winnum, winsize=wsize
endif else begin
    lookimpwcore, project, sample, masterpath, pspath, listpath=listpath, $
                  winnum=winnum, winsize=wsize
endelse

; Close all the open image windows
wdeleteall

END
