PRO REVIMPW2, PROJECT, SAMPLE, _EXTRA=ex

;  Configured to run on Will's laptop.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  revimpwcore.pro.
;
;  Displays a set of images of a list of objects, for quick comparison and
;  evaluation.  Two runs:
;  1st time through:  See all objects, one y image per object.
;  2nd time through:  See the rejected objects, all y images for each object.
;
;  Configured for the PS1+WISE search
;  Currently displays:  PS1 grizy, 2MASS JHK, WISE W123.
; 
;  HISTORY
;  Written by Niall Deacon as listingsrev.pro, sometime before August, 2011.
;  06/29/12 (WB): Adapted for my L-T transition dwarf search.
;                 Added PROGRAM and SAMPLE inputs
;  07/22/12 (WB): Configured to use win.pro properly.
;  07/23/12 (WB): Configured to run off a list of targets, matching id numbers
;                 checking that coordinates also match.
;                 No more need for fileripper.
;  07/24/12 (WB): Made this program a wrapper for revimpwcore.pro
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      ALLY - see all the y images
;      LISTPATH - Location of ascii file with target coordinates
;                 Default: '~/Astro/PS1stamps/<project>/<sample>.ascii'
;      WINNUM - Vector containing the window id numbers to use in opening the
;               image windows.  Works with win.pro.
;               Default is a vector that works on Will's laptop.
;      WINSIZE - Size of image windows.  Default: 200.
;

; Establish path to base folder for all this stuff
basepath = '~/Astro/PS1stamps/'

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

; Set up the file paths
results = basepath+project+'/'+sample+'.samp'
if n_elements(listpath) eq 0 then listpath = basepath+project+'/'+sample+'.ascii'
masterpath = basepath+'FETCH_files/'
pspath = basepath+project+'/'+sample+'.'
; File path for the results of the first run
outfile = results+'1'
; File path for 1st-run-rejected objects list
listpath2 = listpath+'.rej'
; File path for the results of the second run
outfile2 = results+'2'

; Prepare the image windows
winnum = [0,1,2,3,4,7,8,9,10,11,12]
wsize = 200

; Run through all the objects, looking at only one PS1 y image per object
revimpwcore2, project, sample, listpath, masterpath, pspath, outfile, $
             winnum=winnum, winsize=wsize


; Read in the 1st run results
readcol, outfile, id1, flag1, sfound1, dafound1, format='l,i,i,i', comment='#', /silent

; Create a new target list, with the objects rejected in the first run
readcol, listpath, idnum, ra, dec, format='l,d,d', comment='#', /silent
rejid = id1[where(flag1 eq 0, complement=passid1)]
match, idnum, rejid, rej
forprint, rejid, ra[rej], dec[rej], format='(1x,i7,4x,d10.6,4x,d10.6)', /nocomment, $
          textout=listpath2, /silent

; Prepare the image windows
spawn, 'echo $HOST', host
if (host[0] eq 'Hancock.local') then begin
    winnum2 = [0,1,2,3,4,7,8,9,10,11,12,14,15,16,17,18,19,21,22,23,24,25,26]
    wsize2 = 200
endif else begin
    winnum2 = [0,1,2,3,4,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
    wsize2 = 200
endelse

; Run through all the rejected objects, this time looking at all of the PS1 y
; images for each object
revimpwcore2, project, sample, listpath2, masterpath, pspath, outfile2, $
             winnum=winnum2, winsize=wsize2, /ally

; Close all the open image windows
wdeleteall

; Combine the results into one file
readcol, outfile2, id2, flag2, sfound2, dafound2, format='l,i,i,i', comment='#', /silent
id = [id1[passid1], id2]
flag = [flag1[passid1], flag2]
sfound = [sfound1[passid1], sfound2]
dafound = [dafound1[passid1], dafound2]
forprint, id, flag, sfound, dafound, format='(i7,3x,i1,7x,i1,7x,i1)', $
          textout=results, comment='#ID      Good    SIMBAD   DA', /silent

; Delete the 1st-run-rejected objects list
;spawn, 'rm '+listpath2

END
