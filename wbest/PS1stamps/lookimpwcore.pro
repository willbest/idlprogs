PRO LOOKIMPWCORE, PROJECT, SAMPLE, MASTERPATH, PSPATH, $
                  LISTPATH=listpath, WINNUM=winnum, WINSIZE=winsize

;  Called by revimpw.pro.  Displays a set of images of a list of objects, for
;  quick comparison and evaluation.
;
;  Configured for the PS1+WISE search
;  Currently displays:  PS1 grizy, 2MASS JHK, WISE W123.
; 
;  HISTORY
;  Adapted by Will Best from revimpwcore.pro on 09/21/2012.
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;      MASTERPATH - Location of FETCH image files.
;      PSPATH - Location of Pan-STARRS image files.
;
;  KEYWORDS
;      LISTPATH - Location of ascii file with target coordinates.
;      WINNUM - Vector containing the window id numbers to use in opening the
;               image windows.  Works with win.pro.
;               Default is a vector that works on Will's laptop.
;      WINSIZE - Size of image windows.  Default: 200.
;

; Check for parameters
if n_params() lt 4 then message, $
  "Use:  revimpwcore, '<project>', '<sample>', '<masterpath>', '<pspath>' "+$
  "                   [, LISTPATH=listpath, WINNUM=winnum, WINSIZE=winsize]"

; Is there an input list?
list_flag=0
if n_elements(listpath) ne 0 then list_flag=1

; Read in the object coordinates
if list_flag then readcol, listpath, idnum, ra2010, dec2010, format='a,d,d', comment='#', /silent
n=n_elements(idnum)

; Read in FETCH folder names
size=20.0
spawn, 'ls -d ' + masterpath  + '/name=id*', folderlist

; Get the target names (which are id numbers) and coordinates from the FETCH directory names
if not keyword_set(silent) then print, 'Obtaining FETCH object coordinates'
npos = strpos(folderlist, 'name=id') + 7
rafrgx = 'coord=[^ ]+ [^ ]+ [^ ]+ '        ; regular expression to look for
fpos = stregex(folderlist, rafrgx, length=flen)
name = strmid(folderlist, 1#npos, 1#fpos-1#npos-1)
rafpos = fpos + 6
decfpos = fpos + flen
decfstop = strpos(folderlist, ':arcsec')
rafstr = strmid(folderlist, 1#rafpos, 1#decfpos-1#rafpos-1)
decfstr = strmid(folderlist, 1#decfpos, 1#decfstop-1#decfpos)
raf = tenv(rafstr) * 15.
decf = tenv(decfstr)

; Prepare the image windows
device, decomposed=0
dummy = fltarr(500,500)
SET_PLOT, 'X'
loadct, 0
if n_elements(winnum) eq 0 then winnum = [0,1,2,3,4,7,8,9,10,11,12,14,15,16,17,18,19,21,22,23,24,25,26]
if n_elements(wsize) eq 0 then wsize = 200
win, winnum[0], wsize, tit = 'PS g'
win, winnum[1], wsize, tit = 'PS r'
win, winnum[2], wsize, tit = 'PS i'
win, winnum[3], wsize, tit = 'PS z'
win, winnum[4], wsize, tit = 'PS y'
win, winnum[5], wsize, tit = '2MASS J'
win, winnum[6], wsize, tit = '2MASS H'
win, winnum[7], wsize, tit = '2MASS K'
win, winnum[8], wsize, tit = 'WISE W1'
win, winnum[9], wsize, tit = 'WISE W2'
win, winnum[10], wsize, tit = 'WISE W3'
for i=11, n_elements(winnum)-1 do win, winnum[i], wsize, tit = 'PS y'

for i=0, n-1 do begin

; Match the name and idnum for the object
    l = where(idnum[i] eq name)
    print, folderlist[l]
; Check that the coordinates also match
    gcirc, 2, ra2010[i], dec2010[i], raf[l], decf[l], sep
    if sep gt 3.0 then begin
        print, 'FETCH and List coordinates do not match!'
        stop
    endif

    print, 'name = ', idnum[i]
    print, ra2010[i], dec2010[i]

; Find the images for an object
    spawn, 'ls "' + folderlist[l] + '/"*-j*.fits' , jlist
    spawn, 'ls "' + folderlist[l] + '/"*-h*.fits' , hlist
    spawn, 'ls "' + folderlist[l] + '/"*-k*.fits' , klist
    spawn, 'ls "' + folderlist[l] + '/"*-w1-*.fits' , w1list
    spawn, 'ls "' + folderlist[l] + '/"*-w2-*.fits' , w2list
    spawn, 'ls "' + folderlist[l] + '/"*-w3-*.fits' , w3list
    spawn, 'ls ' + pspath + 'g/' + name[l] + '_*' , psglist
    spawn, 'ls ' + pspath + 'r/' + name[l] + '_*' , psrlist
    spawn, 'ls ' + pspath + 'i/' + name[l] + '_*' , psilist
    spawn, 'ls ' + pspath + 'z/' + name[l] + '_*' , pszlist
    spawn, 'ls ' + pspath + 'y/' + name[l] + '_*' , psylist

; Get g image   
    print, 'g band'
    gdummyflag=0
    if ((rstrpos(psglist[0],'/') lt 0) or (n_elements(psglist) lt 1)) then begin
        gdummyflag=1
    endif else begin
        gflag1=0
        gindex=0
        while ((gflag1 lt 1) and (gindex lt n_elements(psglist))) do begin
            fits_read, psglist[gindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutoutg, size, ra2010[i], dec2010[i]
                gflag1 = 1
                print, psglist[gindex]
            endif else gindex = gindex + 1
        endwhile
        if(gflag1 lt 1) then begin
            fits_read, psglist[0], mosaic, header
            pscutout2, mosaic, header, cutoutg, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get r image   
    print, 'r band'
    rdummyflag=0
    if ((rstrpos(psrlist[0],'/') lt 0) or (n_elements(psrlist) lt 1)) then begin
        rdummyflag=1
    endif else begin
        rflag1=0
        rindex=0
        while ((rflag1 lt 1) and (rindex lt n_elements(psrlist))) do begin
            fits_read, psrlist[rindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutoutr, size, ra2010[i], dec2010[i]
                rflag1 = 1
                print, psrlist[rindex]
            endif else rindex = rindex + 1
        endwhile
        if(rflag1 lt 1) then begin
            fits_read, psrlist[0], mosaic, header
            pscutout2, mosaic, header, cutoutr, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get i image   
    print, 'i band'
    idummyflag=0
    if ((rstrpos(psilist[0],'/') lt 0) or (n_elements(psilist) lt 1)) then begin
        idummyflag=1
    endif else begin
        iflag1=0
        iindex=0
        while ((iflag1 lt 1) and (iindex lt n_elements(psilist))) do begin
            fits_read, psilist[iindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutouti, size, ra2010[i], dec2010[i]
                iflag1 = 1
                print, psilist[iindex]
            endif else iindex = iindex + 1
        endwhile
        if(iflag1 lt 1) then begin
            fits_read, psilist[0], mosaic, header
            pscutout2, mosaic, header, cutouti, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get z image   
    print, 'z band'
    zdummyflag=0
    if ((rstrpos(pszlist[0],'/') lt 0) or (n_elements(pszlist) lt 1)) then begin
        zdummyflag=1
    endif else begin
        zflag1=0
        zindex=0
        while ((zflag1 lt 1) and (zindex lt n_elements(pszlist))) do begin
            fits_read, pszlist[zindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutoutz, size, ra2010[i], dec2010[i]
                zflag1 = 1
                print, pszlist[zindex]
            endif else zindex = zindex + 1
        endwhile
        if(zflag1 lt 1) then begin
            fits_read, pszlist[0], mosaic, header
            pscutout2, mosaic, header, cutoutz, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get y image   
    print, 'y band'
    ydummyflag=0
    if ((rstrpos(psylist[0],'/') lt 0) or (n_elements(psylist) lt 1)) then begin
        ydummyflag=1
    endif else begin
        yflag1=0
        yindex=0
        while ((yflag1 lt 1) and (yindex lt n_elements(psylist))) do begin
            fits_read, psylist[yindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutouty, size, ra2010[i], dec2010[i]
                yflag1 = 1
                print, psylist[yindex]
            endif else yindex = yindex + 1
        endwhile
        if(yflag1 lt 1) then begin
            yindex = yindex - 1
            fits_read, psylist[0], mosaic, header
            pscutout2, mosaic, header, cutouty, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get J image
    jdummyflag=0
    print, 'J band'
    if (rstrpos(jlist[0],'/') lt 0) then begin
        jdummyflag=1
    endif else begin
        cutoutlite, jlist[0], ra2010[i], dec2010[i], size, cutoutJ, cutoutheadJ, '2MASS'
    endelse
    
; Get H image
    hdummyflag=0
    print, 'H band'
    if (rstrpos(hlist[0],'/') lt 0) then begin
        hdummyflag=1
    endif else begin
        cutoutlite, hlist[0], ra2010[i], dec2010[i], size, cutoutH, cutoutheadH, '2MASS'
    endelse
    
; Get K image
    kdummyflag=0
    print, 'K band'
    if (rstrpos(klist[0],'/') lt 0) then begin
        kdummyflag=1
    endif else begin
        cutoutlite, klist[0], ra2010[i], dec2010[i], size, cutoutK, cutoutheadK, '2MASS'
    endelse
    
; Get W1 image
    w1dummyflag=0
    print, 'W1 band'
    if (rstrpos(w1list[0],'/') lt 0) then begin
        w1dummyflag=1
    endif else begin
        cutoutlite, w1list[0], ra2010[i], dec2010[i], size, cutoutW1, cutoutheadW1, 'WISE'
    endelse
    
; Get W2 image
    w2dummyflag=0
    print, 'W2 band'
    if (rstrpos(w2list[0],'/') lt 0) then begin
        w2dummyflag=1
    endif else begin
        cutoutlite, w2list[0], ra2010[i], dec2010[i], size, cutoutW2, cutoutheadW2, 'WISE'
    endelse
    
; Get W3 image
    w3dummyflag=0
    print, 'W3 band'
    if (rstrpos(w3list[0],'/') lt 0) then begin
        w3dummyflag=1
    endif else begin
        cutoutlite, w3list[0], ra2010[i], dec2010[i], size, cutoutW3, cutoutheadW3, 'WISE'
    endelse
    
; Display the images
    if (gdummyflag eq 1) then begin
        wset, winnum[0]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[0]  &  loadct, 1, /silent;  &  invct, /quiet
        display2,  smooth(cutoutg,5.0), xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse
    
    if (rdummyflag eq 1) then begin
        wset, winnum[1]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[1]  &  loadct, 1, /silent;  &  invct, /quiet
        display2,  smooth(cutoutr,5.0), xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse
    
    if (idummyflag eq 1) then begin
        wset, winnum[2]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[2]  &  loadct, 8, /silent;  &  invct, /quiet
        display2,  smooth(cutouti,5.0), xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse
    
    if (zdummyflag eq 1) then begin
        wset, winnum[3]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[3]  &  loadct, 8, /silent;  &  invct, /quiet
        display2,  smooth(cutoutz,5.0), xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse
    
    if (ydummyflag eq 1) then begin
        wset, winnum[4]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[4]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  smooth(cutouty,5.0), xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse
    
    if (jdummyflag eq 1) then begin
        wset, winnum[5]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[5]  &  loadct, 3, /silent;  &  invct, /quiet
        display2,  cutoutJ, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse
    
    if (hdummyflag eq 1) then begin
        wset, winnum[6]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[6]  &  loadct, 3, /silent;  &  invct, /quiet
        display2,  cutoutH, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse
    
    if (kdummyflag eq 1) then begin
        wset, winnum[7]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[7]  &  loadct, 3, /silent;  &  invct, /quiet
        display2,  cutoutK, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse

    if (w1dummyflag eq 1) then begin
        wset, winnum[8]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[8]  &  loadct, 7, /silent;  &  invct, /quiet
        display2,  cutoutW1, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse

    if (w2dummyflag eq 1) then begin
        wset, winnum[9]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[9]  &  loadct, 7, /silent;  &  invct, /quiet
        display2,  cutoutW2, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse

    if (w3dummyflag eq 1) then begin
        wset, winnum[10]  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endif else begin
        wset, winnum[10]  &  loadct, 7, /silent;  &  invct, /quiet
        display2,  cutoutW3, xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
    endelse


; Ask to approve or reject the object, or see the rest of the y images
    print, 'object ' + string(i+1) + ' out of ' + string(n)
    print, 'See all the y images? (y for yes, q for quit, any other to continue)'
    yim = get_kbrd()
    if yim eq 'q' then break

    if yim eq 'y' then begin
  ; Display the rest of the y images
        nnn = n_elements(psylist) < (n_elements(winnum)-10)
        wink = 0
        for j=0, nnn-1 do begin
            if j eq yindex then begin
                wink = 1
                continue
            endif else begin
                fits_read, psylist[j], mosaic, header
                pscutout2, mosaic, header, cutouty2, size, ra2010[i], dec2010[i]
                wset, winnum[11+j-wink]
                loadct, 0, /silent ;  &  invct, /quiet
                display2, smooth(cutouty2,5.0), xtit = 'pix', ytit = 'pix', tit = name[l], chars = 1.5, /silent
            endelse
        endfor

; Ask to approve or reject the object
        print, 'Press any key for next images, or q to quit.'
        quit=get_kbrd()
        if quit eq 'q' then break

; Clear the extra y windows
        if (ydummyflag eq 0) then for k=0, nnn-2 do begin
            wset, winnum[11+k]
            erase
        endfor
    endif

endfor


END
