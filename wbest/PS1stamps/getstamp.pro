PRO GETSTAMP, A, B, C, D, E, F, _EXTRA=ex

;  Configured to run on Will's laptop.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  getstampcore.pro.
;  Basically, this wrapper allows you to enter coordinates for a single object
;  (or place on the sky), and saves you the trouble of creating a one-line ascii
;  file.
;  Coordinates can be decimal (two inputs) or sexigesimal (six inputs).
;
;  Converts input coordinates into lists of various formats, for obtaining
;  object images from multiple surveys.
;  Format #1: 1, <ra2010>, <dec2010>   [no header]
;     Used by stampmaker.pro, to create Pan-STARRS postage stamp request forms.
;     Output file #1:  ~/Astro/PS1stamps/getstamp/getstamp.ascii
;  Format #2: id1, <ra2010>, <dec2010>   [no header]
;     Used by fetch.py on Westfield, to get 2MASS, DSS and SDSS images.
;     Output file #2:  ~/Astro/PS1stamps/getstamp/getstampid.ascii
;  Format #3: 1, <ra2010>, <dec2010>  [with ipac header]
;     Used to manually download WISE images from the ipac website.
;     Output file #3:  ~/Astro/PS1stamps/getstamp/getstamp.ipac
;
;  Copies file #1 to westfield:~wbest/getstamp/
;  Copies file #2 to westfield:~wbest/tabs/getstamp/
; 
;  Runs stampmaker on this list, and copies the .tab files over to
;  ipp022:~wbest/tabs/getstamp/getstamp/
;  
;  HISTORY
;  Written by Will Best (IfA), 07/04/2012
;  Adapted from getstamps.pro, to be used for a single object.
;  07/24/12 (WB):  Made this program a wrapper for getstampcore.pro
;
;  INPUTS
;       Two numbers - interpreted as RA and DEC in decimal degrees
;       Six numbers - inteprested as RA and DEC in sexigesimal
;
;  KEYWORDS
;       FILT - Bit-wise code for which filters to include
;                1 - g    2 - r    4 - i    8 - z    16 - y
;              e.g. 2 = r only
;                   5 = g and i only
;                   29 = g, i, z, y
;                   31 = all five filters (DEFAULT)
;      MJDMAX - Latest date for image search
;               Default is 0, which means no upper limit
;      MJDMIN - Earliest date for image search
;               Default is 55700 == May 19, 2011
;               Earliest date = 54983 == June 1, 2009
;

case n_params() of
    2 : begin
        if (a lt 0) or (a ge 360) then $
          message, 'RA must be a value between 0 and 359.99999'
        if (b lt -90) or (b gt 90) then $
          message, 'Dec must be a value between -90 and 90'
    end
    6 : begin
        a = ten(a,b,c)*15
        print, 'RA = ', a
        if (a lt 0) or (a ge 360) then $
          message, 'RA must be a value between 0 and 359.99999'
        b = ten(d,e,f)
        print, 'Dec = ', b
        if (b lt -90) or (b gt 90) then $
          message, 'Dec must be a value between -90 and 90'
    end
    else : message, 'USE:  getstamp, ra, dec'
endcase

; Establish path to base folder for all this stuff
basepath = '~/Astro/PS1stamps/'

; Establish path to stampmaker
stamppath = '~/idlprogs/wbest/PS1stamps/stampmaker'

; Create a temporary one-item target list for getstampscore to use
list = basepath+'templist.ascii'
forprint, a, b, textout=list, format='(d10.6,4x,d10.6)', /nocomment, /silent

; Check and echo the project and sample
project='getstamp'
sample='getstamp'

; Create output directory
cd, basepath
spawn, 'mkdir -pv '+project+'/'+sample

getstampcore, project, sample, basepath, stamppath, list=list, _extra=ex

; Remove the temporary one-item target list
spawn, 'rm '+list

END
