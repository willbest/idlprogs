PRO LISTINGSREV, PROJECT, SAMPLE, LAPTOP=laptop

; NEEDS UPDATING -- compare with listingsrevpw.pro

;  Displays a set of images of a list of objects, for quick comparison and evaluation.
;
;  Configured for the 2MASS-SDSS-PS1 search.
;  Currently displays:  DSS R, DSS I, SDSS z, PS1 y, 2MASS J, 2MASS H.
;                       Displays at ~1999 and ~2010 positions.
; 
;  HISTORY
;  Written by Niall Deacon, sometime before August, 2011.
;  06/29/12 (WB):  Added PROJECT and SAMPLE inputs
;                  Adapted window layout for my Sunray station
;  07/07/12 (WB):  Added LAPTOP keyword
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      LAPTOP - Configure spacing of image windows for my laptop.
;

; Check and echo the project and sample
if n_elements(project) eq 0 then project='lttrans'
if n_elements(sample) eq 0 then sample='demo'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

masterpath='/Users/wbest/fetch/'+project+'/'+sample+'_posid.ascii_FETCH/'
pspath='/Users/wbest/'+project+'/'+sample+'/'+sample+'.y/'
outfile='/Users/wbest/'+project+'/'+sample+'/'+sample+'.samp/'
file_flag=0
if (FILE_TEST(outfile) eq 0) then begin
    openw, 33, outfile
    printf, 33, '#ID        Good    SIMBAD    DA'
endif else begin
    readcol, outfile, F='D,D,D,D', prev_id, prev_yn, prev_sim, prev_DA
    openw, 33, outfile, /APPEND
    file_flag=1
endelse

datapath='~/'+project+'/'+sample+'/'+sample+'_objdata/'
size=20.0
spawn, 'ls -d ' + masterpath  + '/name=id*', folderlist
n=n_elements(folderlist)

dummy = fltarr(500,500)
SET_PLOT, 'X'
loadct, 0
invct
if keyword_set(laptop) then begin
    col0 = 20
    col1 = 222
    col2 = 424
    col3 = 626
    col4 = 828
    col5 = 1030
    row0 = 650
    row1 = 426
endif else begin
    col0 = 20
    col1 = 232
    col2 = 444
    col3 = 656
    col4 = 868
    col5 = 1080
    row0 = 976
    row1 = 744
endelse
win, 0, xs = 200, ys = 200, xpos = col0, ypos = row0, tit = 'DSS R PSpos'
win, 1, xs = 200, ys = 200, xpos = col1, ypos = row0, tit = 'DSS I PSpos'
;win, 2, xs = 200, ys = 200, xpos = col2, ypos = row0, tit = 'sdss i PSpos'
win, 2, xs = 200, ys = 200, xpos = col2, ypos = row0, tit = 'sdss z PSpos'
win, 3, xs = 200, ys = 200, xpos = col3, ypos = row0, tit = 'PS y PSpos'
win, 4, xs = 200, ys = 200, xpos = col4, ypos = row0, tit = '2MASS J PSpos'
win, 5, xs = 200, ys = 200, xpos = col5, ypos = row0, tit = '2MASS H PSpos'
win, 6, xs = 200, ys = 200, xpos = col0, ypos = row1, tit = 'DSS R 2Mpos'
win, 7, xs = 200, ys = 200, xpos = col1, ypos = row1, tit = 'DSS I 2Mposs'
;win, 8, xs = 200, ys = 200, xpos = col2, ypos = row1, tit = 'sdss i 2Mpos'
win, 8, xs = 200, ys = 200, xpos = col2, ypos = row1, tit = 'sdss z 2Mpos'
win, 9, xs = 200, ys = 200, xpos = col3, ypos = row1, tit = 'PS y 2Mpos'
win, 10, xs = 200, ys = 200, xpos = col4, ypos = row1, tit = '2MASS J 2Mpos'
win, 11, xs = 200, ys = 200, xpos = col5, ypos = row1, tit = '2MASS H 2Mpos'

for i=0, n-1 do begin
    print, folderlist[i]
    spawn, 'ls "' + folderlist[i] + '/"*_ir*.fits' , ilist
    spawn, 'ls "' + folderlist[i] + '/"*-j*.fits' , jlist
    spawn, 'ls "' + folderlist[i] + '/"*-h*.fits' , hlist
    spawn, 'ls "' + folderlist[i] + '/"*-k*.fits' , klist
    spawn, 'ls "' + folderlist[i] + '/"poss2*red*.fits' , redlist
;    spawn, 'ls "' + folderlist[i] + '/"fpC*-i*.fits' , sdsslist
    spawn, 'ls "' + folderlist[i] + '/"fpC*-z*.fits' , sdsslist
    ni=n_elements(ilist)
    nj=n_elements(jlist)
    nh=n_elements(hlist)
    nk=n_elements(klist)
    nred=n_elements(redlist)
    nsdss=n_elements(sdsslist)

    nstrt=strpos(folderlist[i],'coord=')
    nstrt1=strpos(folderlist[i],'name=id')
    print, 'thing', nstrt, nstrt1
    name=strmid(folderlist[i],(nstrt1+7),(nstrt-1-(nstrt1+7)))
    print, 'name = ', name
    if (file_flag gt 0) then begin
        same_thing=where(prev_id eq long(name))
        if (same_thing[0] gt -1) then continue
    endif
    spawn, 'ls ' + pspath + '/' + name + '_*' , pslist, COUNT=ny
    rah=strmid(folderlist[i],(nstrt+6),2)
    rah1=fix(rah)
    if rah1 lt 10 then begin
        ram=strmid(folderlist[i],(nstrt+8),2)
        ram1=fix(ram)
        if ram1 lt 10 then begin
            ras=strmid(folderlist[i],(nstrt+10),6)
            ras1=float(ras)
        endif else begin
            ras=strmid(folderlist[i],(nstrt+11),6)
            ras1=float(ras)
        endelse
    endif else begin
        ram=strmid(folderlist[i],(nstrt+9),2)
        ram1=fix(ram)
        if ram1 lt 10 then begin
            ras=strmid(folderlist[i],(nstrt+11),6)
            ras1=float(ras)
        endif else begin
            ras=strmid(folderlist[i],(nstrt+12),6)
            ras1=float(ras)
        endelse
    endelse

    nstrt=strpos(folderlist[i],'+')
    if nstrt lt 0 then begin
        nstrt=rstrpos(folderlist[i],'-')
    endif
    decstr=strmid(folderlist[i],nstrt,12)
    nstrt1=strpos(decstr,' ')
    nstrt2=rstrpos(decstr,' ')
    dech=strmid(decstr,0,nstrt1)
    decm=strmid(decstr,(nstrt1+1),(nstrt2-nstrt1-1))
    decs2=strmid(decstr,(nstrt2+1),4)
    decs=strtrim(decs2,2)
    dech1=fix(dech)
    decm1=fix(decm)
    decs1=float(decs)

    print, name
    spawn, 'more "' + folderlist[i] + '/"' + 'extra-data.txt', extra
    print,extra
    nstrt=strpos(extra[0],'simbad_found:')
    sfound1=strmid(extra[0],(nstrt+13),1)
    sfound=fix(sfound1)
    nstrt=strpos(extra[0],'dwarf_found:')
    dafound1=strmid(extra[0],(nstrt+12),1)
    dafound=fix(dafound1)
    print, 'OBJECTS IN DATA BASE',sfound,dafound

    readcol,datapath+name,F='F,F,F,F',ra2010,dec2010,ra1999,dec1999
    d_rad=sphdist(ra2010,dec2010,ra1999,dec1999,/DEGREES)
    d_rad=d_rad*3600.0

; Get PS1 image
    print, 'PS1 y band'
    ydummyflag=0
    if ((rstrpos(pslist[0],'/') lt 0) or (n_elements(pslist) lt 1)) then begin
        ydummyflag=1
    endif else begin
        yflag1=0
        yindex=0
        while ((yflag1 lt 1) and (yindex lt n_elements(pslist))) do begin
            fits_read, pslist[yindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2,mosaic, header, cutouty_1999,size, ra1999[0], dec1999[0]
                pscutout2,mosaic, header, cutouty_2010,size, ra2010[0], dec2010[0]
                yflag1 = 1
                print, pslist[yindex]
            endif else yindex = yindex + 1
        endwhile
        if(yflag1 lt 1) then begin
            fits_read, pslist[0], mosaic, header
            pscutout2,mosaic, header, cutouty_1999,size, ra1999[0], dec1999[0]
            pscutout2,mosaic, header, cutouty_2010,size, ra2010[0], dec2010[0]
        endif
    endelse

; Get DSS I image
    idummyflag=0
    print, 'I band'
    if (rstrpos(ilist[0],'/') lt 0) then begin
        idummyflag=1
    endif else begin
        cutoutlite, ilist[0], ra1999[0], dec1999[0],size, cutoutI_1999, cutoutheadI_1999, 'DSS'
        cutoutlite, ilist[0], ra2010[0], dec2010[0],size, cutoutI_2010, cutoutheadI_2010, 'DSS'
    endelse

; Get J image
    jdummyflag=0
    print, 'J band'
    if (rstrpos(jlist[0],'/') lt 0) then begin
        jdummyflag=1
    endif else begin
        cutoutlite, jlist[0], ra1999[0], dec1999[0], size, cutoutJ_1999, cutoutheadJ_1999, '2MASS'
        cutoutlite, jlist[0], ra2010[0], dec2010[0], size, cutoutJ_2010, cutoutheadJ_2010, '2MASS'
    endelse

; Get H image
    hdummyflag=0
    print, 'H band'
    if (rstrpos(hlist[0],'/') lt 0) then begin
        hdummyflag=1
    endif else begin
        cutoutlite, hlist[0], ra1999[0], dec1999[0], size, cutoutH_1999, cutoutheadH_1999, '2MASS'
        cutoutlite, hlist[0], ra2010[0], dec2010[0], size, cutoutH_2010, cutoutheadH_2010, '2MASS'
    endelse

; Get K image
    kdummyflag=0
        print, 'K band'
    if (rstrpos(klist[0],'/') lt 0) then begin
        kdummyflag=1
    endif else begin
        cutoutlite, klist[0], ra1999[0], dec1999[0], size, cutoutK_1999, cutoutheadK_1999, '2MASS'
        cutoutlite, klist[0], ra2010[0], dec2010[0], size, cutoutK_2010, cutoutheadK_2010, '2MASS'
    endelse

; Get DSS R image
    print, 'R band'
    reddummyflag=0
    if (rstrpos(redlist[0],'/') lt 0) then begin
        reddummyflag=1
    endif else begin
        cutoutlite, redlist[0], ra1999[0], dec1999[0], size, cutoutred_1999, cutoutheadred_1999, 'DSS'
        cutoutlite, redlist[0], ra2010[0], dec2010[0], size, cutoutred_2010, cutoutheadred_2010, 'DSS'
    endelse

; Get SDSS z image
    print, 'SDSS z band'
    sdssdummyflag=0
    if (rstrpos(sdsslist[0],'/') lt 0) then begin
        sdssdummyflag=1
    endif else begin
        cutoutlite, sdsslist[0], ra1999[0], dec1999[0], size, cutoutSDSS_1999, cutoutheadSDSS_1999, 'SDSS'
        cutoutlite, sdsslist[0], ra2010[0], dec2010[0], size, cutoutSDSS_2010, cutoutheadSDSS_2010, 'SDSS'
    endelse

; Display the images
    if (reddummyflag eq 1) then begin
        wset, 0  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 6  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 0  &  loadct, 0, /silent  &  invct, /quiet
        if (d_rad lt 100.0) then  display2,  cutoutred_2010, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 6  &  loadct, 0, /silent  &  invct, /quiet
        display2,  cutoutred_1999, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (idummyflag eq 1) then begin
        wset, 1  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 7  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 1  &  loadct, 0, /silent  &  invct, /quiet
        if (d_rad lt 100.0) then display2,  cutoutI_2010, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 7  &  loadct, 0, /silent  &  invct, /quiet
        display2,  cutoutI_1999, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse

    if (sdssdummyflag eq 1) then begin
        wset, 2  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 8  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 2  &  loadct, 0, /silent  &  invct, /quiet
        if (d_rad lt 100.0) then display2,  cutoutsdss_2010, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 8  &  loadct, 0, /silent  &  invct, /quiet
        display2,  cutoutsdss_1999, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse

    if (ydummyflag eq 1) then begin
        wset, 3  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 9  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 3  &  loadct, 0, /silent  &  invct, /quiet
        display2,  smooth(cutouty_2010,5.0), xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 9  &  loadct, 0, /silent  &  invct, /quiet
        if (d_rad lt 100.0) then display2,  smooth(cutouty_1999,5.0), xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse

    if (jdummyflag eq 1) then begin
        wset, 4  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 10  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 4  &  loadct, 0, /silent  &  invct, /quiet
        if (d_rad lt 100.0) then display2,  cutoutJ_2010, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 10  &  loadct, 0, /silent  &  invct, /quiet
        display2,  cutoutJ_1999, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse

    if (hdummyflag eq 1) then begin
        wset, 5  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 11  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 5  &  loadct, 0, /silent  &  invct, /quiet
        if (d_rad lt 100.0) then display2,  cutoutH_2010, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
        wset, 11  &  loadct, 0, /silent  &  invct, /quiet
        display2,  cutoutH_1999, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse


    print, 'object ' + string(i) + ' out of ' + string(n_elements(folderlist))
    flag = getyn('Is this a good image?')
;flag=1
    printf,33, name, flag, sfound, dafound
endfor

close, 33

END
