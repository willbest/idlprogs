PRO WISEINGEST, PROJECT, SAMPLE, _EXTRA=ex

;  Distribute WISE images downloaded from IPAC into folders already created and
;  filled by FETCH.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  wiseingestcore.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 07/04/2012
;  07/24/12 WB:  Made this program a wrapper for wiseingestcore.pro
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;                Default = getstamp
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;                Default = getstamp
;
;  KEYWORDS (optional)
;      NFILTS - Number of WISE filters for which images have been obtained.
;               Default = 3
;      RADIUS - Maximum separation for FETCH and WISE objects to match (arcsec).
;               Default = 3.0 arcsec
;      SILENT - Suppress screen outputs
;
;  CALLS
;      WISEINGESTCORE
;

; Check for parameters
;if n_params() lt 2 then message, $
;  "Use:  wiseingest, '<project>', '<sample>' [, nfilts=nfilts, radius=radius, silent=silent]"

; Set path for FETCH and WISE files
if n_elements(project) eq 0 then project = 'getstamp'
if n_elements(sample) eq 0 then sample = 'getstamp'
fetchpath = '~/Astro/PS1stamps/FETCH_files/'
wisepath = '~/Astro/PS1stamps/'+project+'/WISE_files/'

wiseingestcore, project, sample, fetchpath, wisepath, _extra=ex

END
