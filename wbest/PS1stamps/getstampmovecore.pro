PRO GETSTAMPMOVECORE, PROJECT, SAMPLE, BASEPATH, STAMPPATH, $
                   FILT=filt, IDN=idn, LIST=list, MJDMAX=mjdmax, MJDMIN=mjdmin, SIX=six

;  Called by getstamps.pro.  This is the core routine that converts input
;  coordinates, for objects at two different positions, into lists
;  of various formats, for obtaining object images from multiple surveys.
;  Format #1: <id#>, <ra2010>, <dec2010>   [no header]
;     Used by stampmaker, to create Pan-STARRS postage stamp request forms.
;     Output file #1:  <sample>.ascii
;  Format #2: id<id#>, <ra2010>, <dec2010>   [no header]
;     Used by fetch.py on Westfield, to get 2MASS, DSS and SDSS images.
;     Output file #2:  <sample>id.ascii
;  Format #3: <id#>, <ra2010>, <dec2010>  [with ipac header]
;     Used to manually download WISE images from the ipac website.
;     Output file #3:  <sample>.ipac
;  Format #4: <id#>, <racenter>, <decenter>, <ra1999>, <dec1999>, <ra2010>,
;                <dec2010>   [text header]
;     Used by fileripper3.pro, to create a list that listingsrev.pro can use.
;     Output file #4:  <sample>move.ascii
;
;  Copies file #1 to westfield:~/<project>/
;  Copies file #2 to westfield:~/fetch/<project>/
;  Copies file #4 to westfield:~/<project>/<sample>/
;
;  Runs stampmaker on this list, and copies the .tab files over to
;  ipp022:~/tabs/<project>/<sample>/
;  
;  HISTORY
;  Written by Will Best (IfA), 07/24/2012
;
;  INPUTS
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;       FILT - Bit-wise code for which filters to include:
;                1 - g    2 - r    4 - i    8 - z    16 - y
;              e.g. 2 = r only
;                   5 = g and i only
;                   29 = g, i, z, y
;                   31 = all five filters (DEFAULT)
;      IDN - Input target list already has i.d. numbers for the targets.  The
;            i.d. numbers must be the first column.
;      LIST - ascii file in which the first two columns are RA and DEC at an
;             earlier position (e.g. 1999), and the next two columns are RA and
;             DEC at a later position (e.g. 2010), in decimal degrees (unless
;             SIX keyword is set).
;             If the IDN keyword is set, the first column must be the
;             i.d. numbers, followed by the RA and DEC at two positions.
;      MJDMAX - Latest date for image search
;               Default is 0, which means no upper limit
;      MJDMIN - Earliest date for image search
;               Default is 55700 == May 19, 2011
;               Earliest date = 54983 == June 1, 2009
;      SIX - If set, LIST must be an ascii file in which the first six
;               columns are RA and DEC in sexigesimal
;

; Check for parameters
if n_params() lt 4 then message, $
  "Use:  getstampmovecore, '<project>', '<sample>', '<basepath>', '<stamppath>' $"+$
  "      [, FILT=filt, IDN=idn, LIST=list, MJDMAX=mjdmax, MJDMIN=mjdmin, SIX=six]"

; Check for input list
if n_elements(list) eq 0 then list = basepath+'targlist.ascii'

; Read in and check the ra and dec values
if keyword_set(six) then begin
    if keyword_set(idn) then readcol, list, n, a, b, c, d, e, f, g, h, j, k, l, m, format='l,d,d,d,d,d,d,d,d,d,d,d,d', comment='#', /silent else readcol, list, a, b, c, d, e, f, g, h, j, k, l, m, format='d,d,d,d,d,d,d,d,d,d,d,d', comment='#', /silent
    a1 = tenv(a,b,c)
    a2 = tenv(g,h,j)
    if (min(a1) lt 0) or (max(a1) ge 360) or (min(a2) lt 0) or (max(a2) ge 360) then $
      message, 'RA must be values between 0 and 359.99999'
    b1 = tenv(d,e,f)
    b2 = tenv(k,l,m)
    if (min(b1) lt -90) or (max(b1) gt 90) or (min(b2) lt -90) or (max(b2) gt 90) then $
      message, 'Dec must be values between -90 and 90'
endif else begin
    if keyword_set(idn) then readcol, list, n, a, b, c, d, format='l,d,d,d,d', comment='#', /silent $
      else readcol, list, a1, b1, a2, b2, format='d,d,d,d', comment='#', /silent
    if (min(a1) lt 0) or (max(a1) ge 360) or (min(a2) lt 0) or (max(a2) ge 360) then $
      message, 'RA must be values between 0 and 359.99999'
    if (min(b1) lt -90) or (max(b1) gt 90) or (min(b2) lt -90) or (max(b2) gt 90) then $
      message, 'Dec must be values between -90 and 90'
endelse

; Remove objects with duplicate id numbers
if keyword_set(idn) then begin
    uind = rem_dup(n)
    n = n[uind]
    a1 = a1[uind]
    b1 = b1[uind]
    a2 = a2[uind]
    b2 = b2[uind]
endif

; Set the cutoff dates for PS1 images
if n_elements(mjdmin) eq 0 then mjdmin = 55700
mjdmin = trim(mjdmin)
if n_elements(mjdmax) eq 0 then mjdmax = 0
mjdmax = trim(mjdmax)

; Create output files
if not keyword_set(idn) then n = indgen(n_elements(a))+1
textout = basepath+project+'/'+sample+'.ascii'
forprint, n, a2, b2, textout=textout, format='(1x,i7,4x,d10.6,4x,d10.6)', /nocomment, /silent

n = 'id'+trim(n)
textoutid = basepath+project+'/'+sample+'id.ascii'
forprint, n, a2, b2, textout=textoutid, format='(1x,a9,4x,d10.6,4x,d10.6)', /nocomment, /silent

textoutip = basepath+project+'/'+sample+'.ipac'
header = '|        ra|       dec|'+string(13b)+string(10b)+'|    double|    double|'+string(13b)+string(10b)+'|       deg|       deg|'
forprint, a2, b2, textout=textoutip, format='(1x,d10.6,1x,d10.6)', comment=header, /silent

textoutmove = basepath+project+'/'+sample+'move.ascii'
header = '    id           racenter     decenter     ra1999       dec1999      ra2010       dec2010'
ac = (a1+a2)/2.
bc = (b1+b2)/2.
forprint, n, ac, bc, a1, b1, a2, b2, textout=textoutmove, format='(1x,i7,4x,d10.6,4x,d10.6,4x,d10.6,4x,d10.6,4x,d10.6,4x,d10.6)', /nocomment, /silent

spawn, 'scp '+textout+' wbest@westfield.ifa.hawaii.edu:~/'+project+'/.'
spawn, 'scp '+textoutid+' wbest@westfield.ifa.hawaii.edu:~/fetch/'+project+'/.'
spawn, 'scp '+textoutmove+' wbest@westfield.ifa.hawaii.edu:~/'+project+'/'+sample+'/.'

; Run stampmaker
if not keyword_set(filt) then filt = 31

if (filt and 1) ne 0 then spawn, stamppath+' '+textout+' '+sample+' g '+mjdmin+' '+mjdmax
if (filt and 2) ne 0 then spawn, stamppath+' '+textout+' '+sample+' r '+mjdmin+' '+mjdmax
if (filt and 4) ne 0 then spawn, stamppath+' '+textout+' '+sample+' i '+mjdmin+' '+mjdmax
if (filt and 8) ne 0 then spawn, stamppath+' '+textout+' '+sample+' z '+mjdmin+' '+mjdmax
if (filt and 16) ne 0 then spawn, stamppath+' '+textout+' '+sample+' y '+mjdmin+' '+mjdmax

; Copy tab files to ipp022
spawn, 'mv *.tab '+basepath+project+'/'+sample+'/'
spawn, 'scp -r '+basepath+project+'/'+sample+'/ wbest@ipp0222.ifa.hawaii.edu:~/tabs/'+project+'/'

END
