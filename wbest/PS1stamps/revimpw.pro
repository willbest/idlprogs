PRO REVIMPW, PROJECT, SAMPLE, HICKORY=hickory, _EXTRA=ex

;  Configured to run on Will's laptop.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  revimpwcore.pro.
;
;  Displays a set of images of a list of objects, for quick comparison and
;  evaluation.  Two runs:
;  1st time through:  See all objects, one y image per object.
;  2nd time through:  See the rejected objects, all y images for each object.
;
;  Configured for the PS1+WISE search
;  Currently displays:  PS1 grizy, 2MASS JHK, WISE W123.
; 
;  HISTORY
;  Written by Niall Deacon as listingsrev.pro, sometime before August, 2011.
;  06/29/12 (WB): Adapted for my L-T transition dwarf search.
;                 Added PROGRAM and SAMPLE inputs
;  07/22/12 (WB): Configured to use win.pro properly.
;  07/23/12 (WB): Configured to run off a list of targets, matching id numbers
;                 checking that coordinates also match.
;                 No more need for fileripper.
;  07/24/12 (WB): Made this program a wrapper for revimpwcore.pro
;  07/27/12 (WB): Three options: yes, no, see all the y images
;  12/07/12 (WB): Added Hickory keyword (external hard drive)
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      LISTPATH - Location of ascii file with target coordinates
;                 Default: '~/Astro/PS1stamps/<project>/<sample>.ascii'
;      WINNUM - Vector containing the window id numbers to use in opening the
;               image windows.  Works with win.pro.
;               Default is a vector that works on Will's laptop.
;      WINSIZE - Size of image windows.  Default: 200.
;

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

if keyword_set(hickory) then begin
; Establish path to base folder for all this stuff
    basepath = '/Volumes/Hickory/wbest/'
; Set up the file paths
    results = basepath+project+'/'+sample+'/'+sample+'.samp'
    if n_elements(listpath) eq 0 then listpath = basepath+project+'/'+sample+'.ascii'
    masterpath = basepath+'fetch/'+project+'/'+sample+'id.ascii_FETCH/'
    pspath = basepath+project+'/'+sample+'/'+sample+'.'
endif else begin
; Establish path to base folder for all this stuff
    basepath = '~/Astro/PS1stamps/'
; Set up the file paths
    results = basepath+project+'/'+sample+'.samp'
    if n_elements(listpath) eq 0 then listpath = basepath+project+'/'+sample+'.ascii'
    masterpath = basepath+'FETCH_files/'
    pspath = basepath+project+'/'+sample+'.'
endelse

; Prepare the image windows
spawn, 'echo $HOST', host
if (host[0] eq 'Hancock.local') then begin
    winnum = [0,1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23]
    wsize = 200
endif else begin
    winnum = [0,1,2,3,4,7,8,9,10,11,12,14,15,16,17,18,19,20,21,22,23,24,25,26,27]
    wsize = 200
endelse

; Run through all the objects
revimpwcore, project, sample, listpath, masterpath, pspath, results, $
             winnum=winnum, winsize=wsize

; Close all the open image windows
wdeleteall

END
