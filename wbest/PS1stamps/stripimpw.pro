PRO STRIPIMPW, PROJECT, SAMPLE, _EXTRA=ex

;  Configured to run on Westfield.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  stripimpwcore.pro.
;
;  Remove all the unused PS1 images from the Westfield directories.
;
;  Configured for the PS1+WISE search
; 
;  HISTORY
;  Written by Will Best, 09/21/2012.
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      LISTPATH - Location of ascii file with target coordinates
;                 Default: '~/<project>/<sample>.ascii'
;

; Establish path to base folder for all this stuff
basepath = '~/'

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

; Set up the file paths
results = basepath+project+'/'+sample+'/'+sample+'.samp'
if n_elements(listpath) eq 0 then listpath = basepath+project+'/'+sample+'.ascii'
masterpath = basepath+'fetch/'+project+'/'+sample+'id.ascii_FETCH/'
pspath = basepath+project+'/'+sample+'/'+sample+'.'

; Run through all the objects
stripimpwcore, project, sample, listpath, masterpath, pspath


END
