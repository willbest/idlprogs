PRO LISTINGSREVPW, PROJECT, SAMPLE, LAPTOP=laptop

;  Displays a set of images of a list of objects, for quick comparison and evaluation.
;
;  Configured for the PS1+WISE search
;  Currently displays:  PS1 grizy, 2MASS JHK, WISE W123.
; 
;  HISTORY
;  Written by Niall Deacon, sometime before August, 2011.
;  06/29/12 (WB): Adapted for my L-T transition dwarf search.
;                 Added PROGRAM and SAMPLE inputs
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;      LAPTOP - Configure spacing of image windows for my laptop.
;

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

; Set up the file paths
masterpath='~/Astro/PS1stamps/FETCH_files/'
pspath='~/Astro/PS1stamps/getstamp/getstamp.'
listpath='~/Astro/PS1stamps/getstamp/getstamp.ascii'
outfile='~/Astro/PS1stamps/getstamp/getstamp.samp'

; Has part of this sample previously been reviewed?
file_flag=0
if (FILE_TEST(outfile) eq 0) then begin
    openw, 33, outfile
    printf, 33, '#ID        Good    SIMBAD    DA'
endif else begin
    readcol, outfile, F='D,D,D,D', prev_id, prev_yn, prev_sim, prev_DA
    openw, 33, outfile, /APPEND
    file_flag=1
endelse 

; Read in the object coordinates
readcol, listpath, F='x,f,f', ra2010, dec2010

; Read in FETCH folder names
size=20.0
spawn, 'ls -d ' + masterpath  + '/name=id*', folderlist
n=n_elements(folderlist)

; Prepare the image windows
dummy = fltarr(500,500)
SET_PLOT, 'X'
loadct, 0
if keyword_set(laptop) then begin
    col0 = 20
    col1 = 222
    col2 = 424
    col3 = 626
    col4 = 828
    col5 = 1030
    row0 = 650
    row1 = 426
endif else begin
    col0 = 20
    col1 = 232
    col2 = 444
    col3 = 656
    col4 = 868
    col5 = 1080
    row0 = 976
    row1 = 744
endelse
;; win, 0, xs = 200, ys = 200, xpos = col0, ypos = row0, tit = 'PS g'
;; win, 1, xs = 200, ys = 200, xpos = col1, ypos = row0, tit = 'PS r'
;; win, 2, xs = 200, ys = 200, xpos = col2, ypos = row0, tit = 'PS i'
;; win, 3, xs = 200, ys = 200, xpos = col3, ypos = row0, tit = 'PS z'
;; win, 4, xs = 200, ys = 200, xpos = col4, ypos = row0, tit = 'PS y'
;; win, 5, xs = 200, ys = 200, xpos = col0, ypos = row1, tit = '2MASS J'
;; win, 7, xs = 200, ys = 200, xpos = col1, ypos = row1, tit = '2MASS H'
;; win, 8, xs = 200, ys = 200, xpos = col2, ypos = row1, tit = '2MASS K'
;; win, 9, xs = 200, ys = 200, xpos = col3, ypos = row1, tit = 'WISE W1'
;; win, 10, xs = 200, ys = 200, xpos = col4, ypos = row1, tit = 'WISE W2'
;; win, 11, xs = 200, ys = 200, xpos = col5, ypos = row1, tit = 'WISE W3'
wsize = 200
win, 0, wsize, tit = 'PS g'
win, 1, wsize, tit = 'PS r'
win, 2, wsize, tit = 'PS i'
win, 3, wsize, tit = 'PS z'
win, 4, wsize, tit = 'PS y'
win, 7, wsize, tit = '2MASS J'
win, 8, wsize, tit = '2MASS H'
win, 9, wsize, tit = '2MASS K'
win, 10, wsize, tit = 'WISE W1'
win, 11, wsize, tit = 'WISE W2'
win, 12, wsize, tit = 'WISE W3'

for i=0, n-1 do begin

; Find a name for the object
    print, folderlist[i]
    nstrt=strpos(folderlist[i],'coord=')
    nstrt1=strpos(folderlist[i],'name=id')
    print, 'thing', nstrt, nstrt1
    name=strmid(folderlist[i],(nstrt1+7),(nstrt-1-(nstrt1+7)))

; Has this object already been reviewed?  If so, go to the next.
    if (file_flag gt 0) then begin
        same_thing=where(prev_id eq long(name))
        if (same_thing[0] gt -1) then continue
    endif
    print, 'name = ', name

; Find the images for an object
; *** MAY NOT NEED the COUNT variables below
    spawn, 'ls "' + folderlist[i] + '/"*-j*.fits' , jlist;, COUNT=nj
    spawn, 'ls "' + folderlist[i] + '/"*-h*.fits' , hlist;, COUNT=nh
    spawn, 'ls "' + folderlist[i] + '/"*-k*.fits' , klist;, COUNT=nk
    spawn, 'ls "' + folderlist[i] + '/"*-w1-*.fits' , w1list;, COUNT=nw1
    spawn, 'ls "' + folderlist[i] + '/"*-w2-*.fits' , w2list;, COUNT=nw2
    spawn, 'ls "' + folderlist[i] + '/"*-w3-*.fits' , w3list;, COUNT=nw3
;    nj=n_elements(jlist)
;    nh=n_elements(hlist)
;    nk=n_elements(klist)
;    ni=n_elements(w1list)
;    ni=n_elements(w2list)
;    ni=n_elements(w3list)
    spawn, 'ls ' + pspath + 'g/' + name + '_*' , psglist;, COUNT=ng
    spawn, 'ls ' + pspath + 'r/' + name + '_*' , psrlist;, COUNT=nr
    spawn, 'ls ' + pspath + 'i/' + name + '_*' , psilist;, COUNT=ni
    spawn, 'ls ' + pspath + 'z/' + name + '_*' , pszlist;, COUNT=nz
    spawn, 'ls ' + pspath + 'y/' + name + '_*' , psylist;, COUNT=ny

; Get right ascension for the object
    rah=strmid(folderlist[i],(nstrt+6),2)
    rah1=fix(rah)
    if rah1 lt 10 then begin
        ram=strmid(folderlist[i],(nstrt+8),2)
        ram1=fix(ram)
        if ram1 lt 10 then begin
            ras=strmid(folderlist[i],(nstrt+10),6)
            ras1=float(ras)
        endif else begin
            ras=strmid(folderlist[i],(nstrt+11),6)
            ras1=float(ras)
        endelse
    endif else begin
        ram=strmid(folderlist[i],(nstrt+9),2)
        ram1=fix(ram)
        if ram1 lt 10 then begin
            ras=strmid(folderlist[i],(nstrt+11),6)
            ras1=float(ras)
        endif else begin
            ras=strmid(folderlist[i],(nstrt+12),6)
            ras1=float(ras)
        endelse
    endelse

; Get declination for the object   
    nstrt=strpos(folderlist[i],'+')
    if nstrt lt 0 then begin
        nstrt=rstrpos(folderlist[i],'-')
    endif
    decstr=strmid(folderlist[i],nstrt,12)
    nstrt1=strpos(decstr,' ')
    nstrt2=rstrpos(decstr,' ')
    dech=strmid(decstr,0,nstrt1)
    decm=strmid(decstr,(nstrt1+1),(nstrt2-nstrt1-1))
    decs2=strmid(decstr,(nstrt2+1),4)
    decs=strtrim(decs2,2)
    dech1=fix(dech)
    decm1=fix(decm)
    decs1=float(decs)
    
; Is the object in SIMBAD or DwarfArchives?
    spawn, 'more "' + folderlist[i] + '/"' + 'extra-data.txt', extra
    print,extra
    nstrt=strpos(extra[0],'simbad_found:')
    sfound1=strmid(extra[0],(nstrt+13),1)
    sfound=fix(sfound1)
    nstrt=strpos(extra[0],'dwarf_found:')
    dafound1=strmid(extra[0],(nstrt+12),1)
    dafound=fix(dafound1)
    print, 'OBJECTS IN DATA BASE',sfound,dafound
    
; Get g image   
    print, 'g band'
    gdummyflag=0
    if ((rstrpos(psglist[0],'/') lt 0) or (n_elements(psglist) lt 1)) then begin
        gdummyflag=1
    endif else begin
        gflag1=0
        gindex=0
        while ((gflag1 lt 1) and (gindex lt n_elements(psglist))) do begin
            fits_read, psglist[gindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutoutg, size, ra2010[i], dec2010[i]
                gflag1 = 1
                print, psglist[gindex]
            endif else gindex = gindex + 1
        endwhile
        if(gflag1 lt 1) then begin
            fits_read, psglist[0], mosaic, header
            pscutout2, mosaic, header, cutoutg, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get r image   
    print, 'r band'
    rdummyflag=0
    if ((rstrpos(psrlist[0],'/') lt 0) or (n_elements(psrlist) lt 1)) then begin
        rdummyflag=1
    endif else begin
        rflag1=0
        rindex=0
        while ((rflag1 lt 1) and (rindex lt n_elements(psrlist))) do begin
            fits_read, psrlist[rindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutoutr, size, ra2010[i], dec2010[i]
                rflag1 = 1
                print, psrlist[rindex]
            endif else rindex = rindex + 1
        endwhile
        if(rflag1 lt 1) then begin
            fits_read, psrlist[0], mosaic, header
            pscutout2, mosaic, header, cutoutr, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get i image   
    print, 'i band'
    idummyflag=0
    if ((rstrpos(psilist[0],'/') lt 0) or (n_elements(psilist) lt 1)) then begin
        idummyflag=1
    endif else begin
        iflag1=0
        iindex=0
        while ((iflag1 lt 1) and (iindex lt n_elements(psilist))) do begin
            fits_read, psilist[iindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutouti, size, ra2010[i], dec2010[i]
                iflag1 = 1
                print, psilist[iindex]
            endif else iindex = iindex + 1
        endwhile
        if(iflag1 lt 1) then begin
            fits_read, psilist[0], mosaic, header
            pscutout2, mosaic, header, cutouti, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get z image   
    print, 'z band'
    zdummyflag=0
    if ((rstrpos(pszlist[0],'/') lt 0) or (n_elements(pszlist) lt 1)) then begin
        zdummyflag=1
    endif else begin
        zflag1=0
        zindex=0
        while ((zflag1 lt 1) and (zindex lt n_elements(pszlist))) do begin
            fits_read, pszlist[zindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutoutz, size, ra2010[i], dec2010[i]
                zflag1 = 1
                print, pszlist[zindex]
            endif else zindex = zindex + 1
        endwhile
        if(zflag1 lt 1) then begin
            fits_read, pszlist[0], mosaic, header
            pscutout2, mosaic, header, cutoutz, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get y image   
    print, 'y band'
    ydummyflag=0
    if ((rstrpos(psylist[0],'/') lt 0) or (n_elements(psylist) lt 1)) then begin
        ydummyflag=1
    endif else begin
        yflag1=0
        yindex=0
        while ((yflag1 lt 1) and (yindex lt n_elements(psylist))) do begin
            fits_read, psylist[yindex], mosaic, header
            mosaicsize=size(mosaic)
            print, 'MOSAIC CENTRE',mosaic[mosaicsize[1]/2,mosaicsize[2]/2], finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2])
;Searching for unmasked chip gaps which have very low vaiance
            centre_mosaic=mosaic[((mosaicsize[1]/2)-2):((mosaicsize[1]/2)+2),((mosaicsize[2]/2)-2):((mosaicsize[2]/2)+2)]
            iterstat,centre_mosaic,centre_statvec, /silent
            if ((finite(mosaic[mosaicsize[1]/2,mosaicsize[2]/2]) gt 0) and (centre_statvec[3] gt 3.0)) then begin
                pscutout2, mosaic, header, cutouty, size, ra2010[i], dec2010[i]
                yflag1 = 1
                print, psylist[yindex]
            endif else yindex = yindex + 1
        endwhile
        if(yflag1 lt 1) then begin
            fits_read, psylist[0], mosaic, header
            pscutout2, mosaic, header, cutouty, size, ra2010[i], dec2010[i]
        endif
    endelse
    
; Get J image
    jdummyflag=0
    print, 'J band'
    if (rstrpos(jlist[0],'/') lt 0) then begin
        jdummyflag=1
    endif else begin
        cutoutlite, jlist[0], ra2010[i], dec2010[i], size, cutoutJ, cutoutheadJ, '2MASS'
    endelse
    
; Get H image
    hdummyflag=0
    print, 'H band'
    if (rstrpos(hlist[0],'/') lt 0) then begin
        hdummyflag=1
    endif else begin
        cutoutlite, hlist[0], ra2010[i], dec2010[i], size, cutoutH, cutoutheadH, '2MASS'
    endelse
    
; Get K image
    kdummyflag=0
    print, 'K band'
    if (rstrpos(klist[0],'/') lt 0) then begin
        kdummyflag=1
    endif else begin
        cutoutlite, klist[0], ra2010[i], dec2010[i], size, cutoutK, cutoutheadK, '2MASS'
    endelse
    
; Get W1 image
    w1dummyflag=0
    print, 'W1 band'
    if (rstrpos(w1list[0],'/') lt 0) then begin
        w1dummyflag=1
    endif else begin
        cutoutlite, w1list[0], ra2010[i], dec2010[i], size*2, cutoutW1, cutoutheadW1, 'WISE'
    endelse
    
; Get W2 image
    w2dummyflag=0
    print, 'W2 band'
    if (rstrpos(w2list[0],'/') lt 0) then begin
        w2dummyflag=1
    endif else begin
        cutoutlite, w2list[0], ra2010[i], dec2010[i], size*2, cutoutW2, cutoutheadW2, 'WISE'
    endelse
    
; Get W3 image
    w3dummyflag=0
    print, 'W3 band'
    if (rstrpos(w3list[0],'/') lt 0) then begin
        w3dummyflag=1
    endif else begin
        cutoutlite, w3list[0], ra2010[i], dec2010[i], size*2, cutoutW3, cutoutheadW3, 'WISE'
    endelse
    
; Display the images
    if (gdummyflag eq 1) then begin
        wset, 0  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 0  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  smooth(cutoutg,5.0), xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (rdummyflag eq 1) then begin
        wset, 1  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 1  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  smooth(cutoutr,5.0), xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (idummyflag eq 1) then begin
        wset, 2  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 2  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  smooth(cutouti,5.0), xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (zdummyflag eq 1) then begin
        wset, 3  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 3  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  smooth(cutoutz,5.0), xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (ydummyflag eq 1) then begin
        wset, 4  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 4  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  smooth(cutouty,5.0), xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (jdummyflag eq 1) then begin
        wset, 7  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 7  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  cutoutJ, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (hdummyflag eq 1) then begin
        wset, 8  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 8  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  cutoutH, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse
    
    if (kdummyflag eq 1) then begin
        wset, 9  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 9  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  cutoutK, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse

    if (w1dummyflag eq 1) then begin
        wset, 10  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 10  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  cutoutW1, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse

    if (w2dummyflag eq 1) then begin
        wset, 11  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 11  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  cutoutW2, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse

    if (w3dummyflag eq 1) then begin
        wset, 12  &  loadct, 0, /silent  &  invct, /quiet
        display2,  dummy, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endif else begin
        wset, 12  &  loadct, 0, /silent;  &  invct, /quiet
        display2,  cutoutW3, xtit = 'pix', ytit = 'pix', tit = name, chars = 1.5, /silent
    endelse


    print, 'object ' + string(i+1) + ' out of ' + string(n)
    flag = getyn('Is this a good image?')
;flag=1
    printf,33, name, flag, sfound, dafound
endfor

close, 33

END
