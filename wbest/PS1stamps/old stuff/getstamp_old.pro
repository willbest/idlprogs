PRO GETSTAMP, A, B, C, D, E, F, FILT=filt
;
;  Converts input coordinates into a format for obtaining PS1 images.
;  Format: 1, <ra2010>, <dec2010>
;  Output file:  ~/Astro/PS1stamps/getstamp.ascii
;  
;  HISTORY
;  Written by Will Best (IfA), 05/08/2012
;  06/13/12:  Modified for Macs.
;             Added FILT keyword
;
;  INPUTS
;       Two numbers - interpreted as RA and DEC in decimal degrees
;       Six numbers - inteprested as RA and DEC in sexigesimal
;
;  KEYWORDS
;       FILT - Bit-wise code for which filters to include
;                1 - g    2 - r    4 - i    8 - z    16 - y
;              e.g. 2 = r only
;                   3 = g and r only
;                   29 = g, i, z, y
;                   31 = all five filters
;              Default is 31, all five filters
;

case n_params() of
    2 : begin
        if (a lt 0) or (a ge 360) then $
          message, 'RA must be a value between 0 and 359.99999'
        if (b lt -90) or (b gt 90) then $
          message, 'Dec must be a value between -90 and 90'
    end
    6 : begin
        a = ten(a,b,c)
        print, 'RA = ', a
        if (a lt 0) or (a ge 360) then $
          message, 'RA must be a value between 0 and 359.99999'
        b = ten(d,e,f)
        print, 'Dec = ', b
        if (b lt -90) or (b gt 90) then $
          message, 'Dec must be a value between -90 and 90'
    end
    else : message, 'USE:  getstamp, ra, dec'
endcase

; Create output file
textout = '~/Astro/PS1stamps/getstamp.ascii'
head = '  id     ra2010        dec2010'
forprint, 1, a, b, textout=textout, $
  format='(3x,i1,4x,f10.6,4x,f10.6)', comment='#'+head, /silent

; Run stampmaker
if not keyword_set(filt) then filt = 31
cd, '~/Astro/PS1stamps'

if (filt and 1) ne 0 then begin
    spawn, '~/idlprogs/wbest/PS1stamps/stampmaker getstamp.ascii getstamp g 54983 null > messageg.txt'
    readcol, 'messageg.txt', p, format='a', delim=':'
    tabname = strmid(p[0], 14)
;    spawn, 'scp '+tabname+' wbest@ipp022.ifa.hawaii.edu:~/tabs/getstamp/.'
endif

if (filt and 2) ne 0 then begin
    spawn, '~/idlprogs/wbest/PS1stamps/stampmaker getstamp.ascii getstamp r 54983 null > messager.txt'
    readcol, 'messager.txt', p, format='a', delim=':'
    tabname = strmid(p[0], 14)
;    spawn, 'scp '+tabname+' wbest@ipp022.ifa.hawaii.edu:~/tabs/getstamp/.'
endif

if (filt and 4) ne 0 then begin
    spawn, '~/idlprogs/wbest/PS1stamps/stampmaker getstamp.ascii getstamp i 54983 null > messagei.txt'
    readcol, 'messagei.txt', p, format='a', delim=':'
    tabname = strmid(p[0], 14)
;    spawn, 'scp '+tabname+' wbest@ipp022.ifa.hawaii.edu:~/tabs/getstamp/.'
endif

if (filt and 8) ne 0 then begin
    spawn, '~/idlprogs/wbest/PS1stamps/stampmaker getstamp.ascii getstamp z 54983 null > messagez.txt'
    readcol, 'messagez.txt', p, format='a', delim=':'
    tabname = strmid(p[0], 14)
;    spawn, 'scp '+tabname+' wbest@ipp022.ifa.hawaii.edu:~/tabs/getstamp/.'
endif

if (filt and 16) ne 0 then begin
    spawn, '~/idlprogs/wbest/PS1stamps/stampmaker getstamp.ascii getstamp y 54983 null > messagey.txt'
    readcol, 'messagey.txt', p, format='a', delim=':
    tabname = strmid(p[0], 15)
;    spawn, 'scp '+tabname+' wbest@ipp022.ifa.hawaii.edu:~/tabs/getstamp/.'
endif

END
