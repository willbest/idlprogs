matchlist = '~/ps1/hispeed/goodlistt_2.sav'
outname = '~/ps1/hispeed/hispeedt_2'

;PRO STAMPLISTS, MATCHLIST, OUTNAME=outname
;
;  Loads a data structure returned by the Vizier database, and creates
;  three target lists. The first two are for obtaining PS1 images
;  using stampmaker, and the third is for use by fileripper3.
;  List 1: <id#>, <ra2010>, <dec2010>
;          <OUTNAME>_pos.ascii
;  List 2: id<id#>, <ra2010>, <dec2010>
;          <OUTNAME>_posid.ascii
;  List 3: <id#>, <racenter>, <decenter>, <ra1999>, <dec1999>, <ra2010>, <dec2010>
;          <OUTNAME>_good.ascii
;  
;  HISTORY
;  Written by Will Best (IfA), 10/19/2011
;  10/20/2011:  Eliminate duplicates in the list of targets
;
;  INPUTS
;       MATCHLIST - .sav file with structure containing matches
;
;  KEYWORDS
;       OUTNAME - name to use in output files (see above)
;                 Default is ~/ps1/hispeed/goodlist_pos[id].ascii
;
;  CALLS
;       FORPRINT
;

; Load data into named ("merge") structure array called goodlist.
if n_elements(matchlist) eq 0 then matchlist = '~/ps1/hispeed/goodlistt_2.sav'
restore, matchlist

;Retrieve unique elements from goodlist
uind = rem_dup(goodlist.id)
ulist = goodlist[uind]

; Write the first list to an ascii file
if size(outname,/type) eq 7 then textout = outname+'_pos.ascii' else $
  textout = '~/ps1/hispeed/goodlist_pos.ascii'
print, 'Printing first list to '+textout
head = '    id           ra2010       dec2010'
forprint, ulist.id, ulist.rap, ulist.dep, textout=textout, $
  format='(5x,i7,4x,f9.5,4x,f9.5)', comment='#'+head, /silent

; Write the second list to an ascii file
textoutid = strmid(textout,0,strpos(textout,'_pos')+4) + 'id' + $
  strmid(textout,strpos(textout,'_pos')+4)
print, 'Printing second list to '+textoutid
head = '    id           ra2010       dec2010'
idcol = strcompress('id'+string(ulist.id),/remove_all)
forprint, idcol, ulist.rap, ulist.dep, textout=textoutid, $
  format='(3x,a9,4x,f9.5,4x,f9.5)', comment='#'+head, /silent

; Write the third list to an ascii file
textoutgood = strmid(textout,0,strpos(textout,'_pos')) + '_good' + $
  strmid(textout,strpos(textout,'_pos')+4)
print, 'Printing third list to '+textoutgood
head = '    id           racenter     decenter     ra1999       dec1999      ra2010       dec2010'
forprint, ulist.id, ulist.racenter, ulist.decenter, ulist.ra2, ulist.de2, $
  ulist.rap, ulist.dep, textout=textoutgood, comment='#'+head, /silent, $
  format='(5x,i7,4x,f9.5,4x,f9.5,4x,f9.5,4x,f9.5,4x,f9.5,4x,f9.5)'

END
