PRO WISEINGESTCORE, PROJECT, SAMPLE, FETCHPATH, WISEPATH, RADIUS=radius, SILENT=silent

;  Called by wiseingest.pro.  This is the core routine that distributes WISE
;  images, downloaded from IPAC, into folders already created and filled by
;  FETCH.  Does this by reading the coordinates from the FETCH directory names
;  and the WISE image file names, and matching up the coordinates.
;  
;  HISTORY
;  Written by Will Best (IfA), 07/24/2012
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;                Default = getstamp
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;                Default = getstamp
;
;  KEYWORDS (optional)
;      RADIUS - Maximum separation for FETCH and WISE objects to match (arcsec).
;               Default = 3.0 arcsec
;      SILENT - Suppress screen outputs
;

; Check for parameters
if n_params() lt 4 then message, $
  "Use:  wiseingestcore, '<project>', '<sample>', '<fetchpath>', '<wisepath>' $"+$
  "      [, radius=radius, silent=silent]"

; Matching radius for FETCH and WISE objects
if not keyword_set(radius) then radius = 3.0     ; arcsec

; Get lists of image directories
spawn, 'ls -d '+wisepath+'*', wisedlist
spawn, 'ls -d '+fetchpath+'name*', fetchlist

; Get the target coordinates from the FETCH directory names
if not keyword_set(silent) then print, 'Obtaining FETCH object coordinates'
rafrgx = 'coord=[^ ]+ [^ ]+ [^ ]+ '        ; regular expression to look for
fpos = stregex(fetchlist, rafrgx, length=flen)
rafpos = fpos + 6
decfpos = fpos + flen
decfstop = strpos(fetchlist, ':arcsec')
rafstr = strmid(fetchlist, 1#rafpos, 1#decfpos-1#rafpos-1)
decfstr = strmid(fetchlist, 1#decfpos, 1#decfstop-1#decfpos)
raf = tenv(rafstr) * 15.
decf = tenv(decfstr)

; Attempt to match each WISE object to a FETCH directory
nw = n_elements(wisedlist)
if not keyword_set(silent) then print, 'Attempting to match WISE objects to FETCH coordinates'
for i=0, nw-1 do begin

; Get the WISE file names in the WISE directory
    spawn, 'ls '+wisedlist[i]+'/', wisename

; Get the target coordinates from the WISE file names
    rawpos = strpos(wisename[0], '_ra') + 3
    raw = double(strmid(wisename[0], rawpos, 10))
    decwpos = strpos(wisename[0], '_dec') + 4
    decw = double(strmid(wisename[0], decwpos, 9))

; Print the short WISE name to screen
    if not keyword_set(silent) then begin
        sexw = adstring(w[i].ra, w[i].dec, 1)
        namew = 'WISE '+strcompress(strmid(sexw,1,5)+strmid(sexw,14,6), /remove)
        print, namew
    endif

; Look for a match
    gcirc, 2, raw, decw, raf, decf, sep
    m = where(sep lt radius)
; What if there is more than one match?
    if n_elements(m) gt 1 then begin
        if not keyword_set(silent) then begin
            print, namew+' has '+n_elements(m)+' matches!'
            print, fetchlist[m]
            print, 'Using closest match.'
        endif
        m = where(sep eq min(sep))
    endif
; If match exists, copy the WISE images into the FETCH directory
    if m ge 0 then begin
        if (not keyword_set(silent)) and (m eq 1) then print, 'Found 1 match'
        spawn, 'cp ' + wisedlist[i] + '/* "' + fetchlist[m] + '"'
    endif else begin
        if not keyword_set(silent) then print, 'No matches found!'
    endelse

endfor

END
