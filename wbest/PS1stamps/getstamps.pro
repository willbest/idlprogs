PRO GETSTAMPS, PROJECT, SAMPLE, _EXTRA=ex

;  Configured to run on Will's laptop.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  getstampcore.pro.
;
;  Converts input coordinates into lists of various formats, for obtaining
;  object images from multiple surveys.
;  Format #1: <id#>, <ra2010>, <dec2010>   [no header]
;     Used by stampmaker.pro, to create Pan-STARRS postage stamp request forms.
;     Output file #1:  ~/Astro/PS1stamps/<project>/<sample>.ascii
;  Format #2: id<id#>, <ra2010>, <dec2010>   [no header]
;     Used by fetch.py on Westfield, to get 2MASS, DSS and SDSS images.
;     Output file #2:  ~/Astro/PS1stamps/<project>/<sample>id.ascii
;  Format #3: <id#>, <ra2010>, <dec2010>  [with ipac header]
;     Used to manually download WISE images from the ipac website.
;     Output file #3:  ~/Astro/PS1stamps/<project>/<sample>.ipac
;
;  Copies file #1 to westfield:~wbest/<project>/
;  Copies file #2 to westfield:~wbest/fetch/<project>/
;
;  Runs stampmaker on this list, and copies the .tab files over to
;  ipp022:~wbest/tabs/<project>/<sample>/
;  
;  HISTORY
;  Written by Will Best (IfA), 05/08/2012
;  06/13/12 (WB):  Modified for Macs.
;                  Added FILT keyword
;  06/28/12 (WB):  Added MJDMX, MJDMIN, SIX keywords
;  07/11/12 (WB):  Generalized; added PROJECT and SAMPLE inputs
;                  Added IDN keyword
;                  Changed LIST from an input to a keyword
;  07/18/12 (WB):  Added code to remove objects with duplicate id numbers.
;  07/20/12 (WB):  Can now handle multiple tab files for each filter.
;  07/22/12 (WB):  Create new directories for the input project and sample, as needed.
;                  Put all the created files in these directories.
;                  No longer using the message?.txt files.
;  07/24/12 (WB):  Made this program a wrapper for getstampcore.pro
;
;  INPUTS
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;                Default:  getstamp
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;                Default:  getstamp
;
;  KEYWORDS
;       FILT - Bit-wise code for which filters to include
;                1 - g    2 - r    4 - i    8 - z    16 - y
;              e.g. 2 = r only
;                   5 = g and i only
;                   29 = g, i, z, y
;                   31 = all five filters (DEFAULT)
;      IDN - Input target list already has i.d. numbers for the targets.  The
;            i.d. numbers must be the first column.
;      LIST - ascii file in which the first two columns are RA and DEC,
;             in decimal degrees (unless SIX keyword is set).
;             If the IDN keyword is set, the first column must be the
;             i.d. numbers, followed by the RA and DEC.
;             Default: ~/Astro/PS1stamps/targlist.ascii
;      MJDMAX - Latest date for image search
;               Default is 0, which means no upper limit
;      MJDMIN - Earliest date for image search
;               Default is 55700 == May 19, 2011
;               Earliest date = 54983 == June 1, 2009
;      SIX - If set, LIST must be an ascii file in which the first six
;               columns are RA and DEC in sexigesimal
;

; Establish path to base folder for all this stuff
basepath = '~/Astro/PS1stamps/'

; Establish path to stampmaker
stamppath = '~/idlprogs/wbest/PS1stamps/stampmaker'

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

; Create output directory
cd, basepath
spawn, 'mkdir -pv '+project+'/'+sample

getstampcore, project, sample, basepath, stamppath, _extra=ex

END
