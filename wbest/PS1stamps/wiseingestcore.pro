PRO WISEINGESTCORE, PROJECT, SAMPLE, FETCHPATH, WISEPATH, NFILTS=nfilts, RADIUS=radius, SILENT=silent

;  Called by wiseingest.pro.  This is the core routine that distributes WISE
;  images, downloaded from IPAC, into folders already created and filled by
;  FETCH.  Does this by reading the coordinates from the FETCH directory names
;  and the WISE image file names, and matching up the coordinates.
;  
;  HISTORY
;  Written by Will Best (IfA), 07/24/2012
;  08/01/2012:  Revised to work with flat WISE directory (no L3a or coadd directories.)
;
;  INPUTS (optional)
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;                Default = getstamp
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;                Default = getstamp
;
;  KEYWORDS (optional)
;      NFILTS - Number of WISE filters for which images have been obtained.
;               Default = 3
;      RADIUS - Maximum separation for FETCH and WISE objects to match (arcsec).
;               Default = 3.0 arcsec
;      SILENT - Suppress screen outputs
;

; Check for parameters
if n_params() lt 4 then message, $
  "Use:  wiseingestcore, '<project>', '<sample>', '<fetchpath>', '<wisepath>' $"+$
  "      [, radius=radius, silent=silent]"

; Matching radius for FETCH and WISE objects
if not keyword_set(radius) then radius = 3.0     ; arcsec

; Number of filters
if not keyword_set(nfilts) then nfilts = 3

; Get lists of image directories
spawn, 'ls '+wisepath, wiselist
spawn, 'ls -d '+fetchpath+'name*', fetchlist

; Get the target coordinates from the FETCH directory names
if not keyword_set(silent) then print, 'Obtaining FETCH object coordinates'
rafrgx = 'coord=[^ ]+ [^ ]+ [^ ]+ '        ; regular expression to look for
fpos = stregex(fetchlist, rafrgx, length=flen)
rafpos = fpos + 6
decfpos = fpos + flen
decfstop = strpos(fetchlist, ':arcsec')
rafstr = strmid(fetchlist, 1#rafpos, 1#decfpos-1#rafpos-1)
decfstr = strmid(fetchlist, 1#decfpos, 1#decfstop-1#decfpos)
raf = tenv(rafstr) * 15.
decf = tenv(decfstr)

; Build structure for WISE information
nw = n_elements(wiselist)
w = replicate({list:'', rapos:0, decpos:0, endpos:0, rastr:'', decstr:'', $
                  ra:0D, dec:0D, sex:'', name:''}, nw)

; Get the target coordinates from the WISE file names
w.list = wiselist
w.rapos = strpos(w.list, '_ra') + 3
w.decpos = strpos(w.list, '_dec') + 4
w.endpos = strpos(w.list, '_asec')
w.rastr = strmid(w.list, 1#w.rapos, 1#w.decpos-1#w.rapos-4)
w.ra = double(w.rastr)
w.decstr = strmid(w.list, 1#w.decpos, 1#w.endpos-1#w.decpos)
w.dec = double(w.decstr)
w.sex = adstring(w.ra, w.dec, 1)
w.name = 'WISE '+strcompress(strmid(w.sex,1,5)+strmid(w.sex,14,6), /remove)

; Sort the WISE files by ra, then dec
w = w[sort(w.dec)]
w = w[sort(w.ra)]

; Attempt to match each WISE object to a FETCH directory
if not keyword_set(silent) then print, 'Attempting to match WISE objects to FETCH coordinates'
nomatch = 0
for i=0, nw-1, nfilts do begin

; Print the short WISE name to screen
    if not keyword_set(silent) then print, w[i].name

; Look for a match
    gcirc, 2, w[i].ra, w[i].dec, raf, decf, sep
    m = where(sep lt radius)
; What if there is more than one match?
    if n_elements(m) gt 1 then begin
        if not keyword_set(silent) then begin
            print, w[i].name+' has '+trim(n_elements(m))+' matches!'
            print, fetchlist[m]
            print, 'Using closest match.'
        endif
        m = where(sep eq min(sep))
        m = m[0]
    endif
; If match exists, copy the WISE images into the FETCH directory
    if m ge 0 then begin
        if (not keyword_set(silent)) and (n_elements(m) eq 1) then print, 'Found 1 match'
        spawn, 'cp '+wisepath+'*_ra'+w[i].rastr+'_dec'+w[i].decstr+'* "'+fetchlist[m]+'"'
    endif else begin
        if not keyword_set(silent) then print, 'No matches found!'
        nomatch = nomatch + 1
    endelse

endfor

print, trim(nomatch)+' objects were not matched.'

;; ls /Users/wbest/fetch/getstamp/getstampsid.ascii_FETCH/
;; index.html
;; name=id1:coord=15 25 2.11296 +8 33 43.866:arcsec=20.000000:finder_arcmin=2.000000

;; ls /Users/wbest/fetch/getstamp/wise/
;; 2313p090_ab41-w1-int-3_ra231.258804_dec8.562185_asec120.000.fits
;; 2313p090_ab41-w2-int-3_ra231.258804_dec8.562185_asec120.000.fits
;; 2313p090_ab41-w3-int-3_ra231.258804_dec8.562185_asec120.000.fits

END
