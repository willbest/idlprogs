PRO STAMPLISTS, PROJECT, SAMPLE, FILT=filt, LIST=list, MJDMAX=mjdmax, MJDMIN=mjdmin

;  Configured to run on Will's laptop.
;  This is a wrapper routine, which supplies data paths and optional keywords to
;  stamplistscore.pro.
;
;  Loads a data structure returned by the Vizier database, and converts
;  coordinates into lists of various formats, for obtaining object images from
;  multiple surveys.
;  Format #1: <id#>, <ra2010>, <dec2010>   [no header]
;     Used by stampmaker.pro, to create Pan-STARRS postage stamp request forms.
;     Output file #1:  ~/Astro/PS1stamps/<sample>.ascii
;  Format #2: id<id#>, <ra2010>, <dec2010>   [no header]
;     Used by fetch.py on Westfield, to get 2MASS, DSS and SDSS images.
;     Output file #2:  ~/Astro/PS1stamps/<sample>id.ascii
;  Format #3: <id#>, <ra2010>, <dec2010>  [with ipac header]
;     Used to manually download WISE images from the ipac website.
;     Output file #3:  ~/Astro/PS1stamps/<sample>.ipac
;  Format #4: <id#>, <racenter>, <decenter>, <ra1999>, <dec1999>, <ra2010>,
;                <dec2010>   [text header]
;     Used by fileripper3.pro, to create a list that listingsrev.pro can use.
;     Output file #4:  ~/Astro/PS1stamps/<sample>move.ascii
;
;  Copies file #1 to westfield:~wbest/<project>/
;  Copies file #2 to westfield:~wbest/fetch/<project>/
;  Copies file #4 to westfield:~wbest/<project>/<sample>/
;
;  Runs stampmaker on this list, and copies the .tab files over to
;  ipp022:~wbest/tabs/<project>/
;
;  HISTORY
;  Written by Will Best (IfA), 10/19/2011
;  10/20/11 (WB):  Eliminate duplicates in the list of targets
;  07/17/12 (WB):  Updated to imitate getstampsmove.pro
;  07/24/12 (WB):  Made this program a wrapper for getstampcore.pro
;
;  INPUTS
;      PROJECT - Science program under which you are looking at these images
;                (e.g. Tmove, redred, LTtrans)
;      SAMPLE - Specific set of images you are looking at (e.g. apr11, demo)
;
;  KEYWORDS
;       FILT - Bit-wise code for which filters to include:
;                1 - g    2 - r    4 - i    8 - z    16 - y
;              e.g. 2 = r only
;                   5 = g and i only
;                   29 = g, i, z, y
;                   31 = all five filters (DEFAULT)
;      LIST - ascii file in which the first two columns are RA and DEC at an
;             earlier position (e.g. 1999), and the next two columns are RA and
;             DEC at a later position (e.g. 2010), in decimal degrees (unless
;             SIX keyword is set).
;             If the IDN keyword is set, the first column must be the
;             i.d. numbers, followed by the RA and DEC at two positions.
;             Default: ~/Astro/PS1stamps/getstamp.sav
;      MJDMAX - Latest date for image search
;               Default is 0, which means no upper limit
;      MJDMIN - Earliest date for image search
;               Default is 55700 == May 19, 2011
;               Earliest date = 54983 == June 1, 2009
;

; Establish path to base folder for all this stuff
basepath = '~/Astro/PS1stamps/'

; Establish path to stampmaker
stamppath = '~/idlprogs/wbest/PS1stamps/stampmaker'

; Check and echo the project and sample
if n_elements(project) eq 0 then project='getstamp'
if n_elements(sample) eq 0 then sample='getstamp'
print, 'PROJECT: ', project
print, 'SAMPLE: ', sample

; Create output directory
cd, basepath
spawn, 'mkdir -pv '+project+'/'+sample

stamplistscore, project, sample, basepath, stamppath, _extra=ex

END
