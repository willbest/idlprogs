PRO QSDSSL, TARGLIST, ASCII=ascii, MERGE=merge, NOSAV=nosav, YEARS=years, $
           _EXTRA=ex

;  Wrapper program, queries the Vizier database for SDSS matches of
;  high proper motion brown dwarf candidates from PS1/2MASS.
;  By default, writes the match list to a .sav file,
;     ~/ps1/hispeed/rawlist.sav
;  
;  HISTORY
;  Written by Will Best (IfA), 10/05/2011
;  10/12/2011:  Added MERGE keyword
;
;  INPUTS
;       TARGLIST - List of target files (overrides default)
;
;  KEYWORDS
;       ASCII - Write the final match list to an ascii file,
;                  ~/ps1/hispeed/rawlist.ascii
;               (Match list will also be written to a .sav file,
;               unless the NOSAV keyword is also set.)
;       MERGE - Merge the matches from Vizier with the target list and
;               the calculated search coordinates.  Returns these in a
;               single named structure array (name="merge").
;       NOSAV - Do not write the match list to a .sav file.
;               (WARNING: If NOSAV is set and ASCII is not set, no
;               output file will be generated!)
;       YEARS - Fixed time radius to search around the midpoint of the
;               2MASS and PS1 positions.  Default is to calculate a
;               different time radius for each target.
;

; Identify file with list of targets to match in SDSS database
if n_elements(targlist) eq 0 then $
  targlist='~/ps1/hispeed/highpm_narrow.ascii'

; Read in the target list
; id = id number from table, for later matching
; pm = proper motion
; Tmean = mean date of 2MASS and PS1 observations (zero date 2001/1/29)
; ra2, de2 = 2MASS coordinates
; rap, dep = PS1 coordinates
targets = read_struct(targlist, 'id, pm, Tmean, ra2, de2, rap, dep', $
                      comment='#', tagformat='l,f,f,f,f,f,f', _EXTRA=ex)
lent = n_elements(targets)

; Calculate midpoint of 2MASS and PS1 locations
; Everything is in epoch 2000.0.
ramid = (targets.ra2 + targets.rap)/2
demid = (targets.de2 + targets.dep)/2

; Determine years for search radius
; T(2000/1/1) = -394 and T(2008/12/31) = 2893
if n_elements(years) eq 0 then begin
    years = (targets.Tmean + 394.) > (2893. - targets.Tmean)
    years = years / 365.24
endif

; dis is the search radius in ARCMINUTES
dis = years * targets.pm / 60.

; Query the Vizier database
;info = QueryVizier('II/294', [RA, DEC], dis, $
;                    [/ALLCOLUMNS, /CANADA, CONSTRAINT=, /SILENT, /VERBOSE ])
;QueryVizier needs a 2-element vector [RA, DEC], both in DEGREES.
;mode, cl(3=galaxy, 6=star), SDSS(coords), M_SDSS(multiple objects same name),
;zsp(redshift), umag, e_umag, gmag, e_gmag, rmag, e_rmag, imag,
;e_imag, zmag, e_zmag, raJ2000(decimal degrees), deJ2000(decimal degrees), 
;obsdate (decimal years), q(quality, 2=acceptable, 3=good)

if keyword_set(merge) then begin
   rawlist = qv_list('II/294', [[ramid], [demid]], dis, mergedata=targets)
endif else rawlist = qv_list('II/294', [[ramid], [demid]], dis)
match = n_elements(rawlist)

;Write the match list to an ascii file
if keyword_set(ascii) then begin
    textout = '~/ps1/hispeed/rawlist.ascii'
    head = strjoin(string(tag_names(rawlist)),'    ')
    forprint, rawlist, textout=textout, comment='#'+head, _EXTRA=ex
endif

;.sav file
if not keyword_set(nosav) then begin
    filename = '~/ps1/hispeed/rawlist.sav'
    save, rawlist, filename=filename, _EXTRA=ex
endif

END
