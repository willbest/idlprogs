PRO FILERIPPER3, PROJECT, SAMPLE

;  Generate files for use by listingsrev.pro
;
;  HISTORY
;  Written by Will Best (IfA), 11/2/11
;
;  USE
;  fileripper3, '[project]', '[sample]'
;
;  Input file: ~/[project]/[sample]/[sample]_good.ascii
;  Output directory: ~/[project]/[sample]/[sample]_objdata/
;

infile = '~/'+project+'/'+sample+'/'+sample+'_good.ascii'
readcol, infile, id, ra1999, dec1999, ra2010, dec2010, $
   format='l,x,x,d,d,d,d', comment='#', /silent
idst = strcompress(string(id),/remove_all)
for i=0, n_elements(id)-1 do begin
   outfile = '~/'+project+'/'+sample+'/'+sample+'_objdata/'+idst[i]
   print, outfile
;   forprint, ra2010[i], dec2010[i], ra1999[i], dec1999[i], textout=outfile, $
;             format='(d10.6,1x,d10.6,1x,d10.6,1x,d10.6)', /silent
   openw, lun, outfile, /get_lun
   printf, lun, ra2010[i], dec2010[i], ra1999[i], dec1999[i], $
           format='(d10.6,1x,d10.6,1x,d10.6,1x,d10.6)'
   free_lun, lun
endfor

end
