PRO SLOANMAG, TABLE, CSV=csv, NAMES=names, NOSAV=nosav

;  Get SDSS magnitudes for a structure of object data.
;  Currently using Kimberly's mega-table from summer 2011.
;  
;  HISTORY
;  Started by Will Best (IfA), 12/15/2011
;
;  INPUTS
;       TABLE - path to input structure
;
;  KEYWORDS
;       CSV - Output the structure, with SDSS mags, to a .csv file.
;       NAMES - Search by object name, rather than position
;       NOSAV - Do not write the structure to a .sav file.
;

; Get the structure
;if n_elements(table) eq 0 then table = '~/ps1/wise_da_legg_complete_duplicate_3pi.sav'
if n_elements(table) eq 0 then table = 'Desktop/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
restore, table
list = wise_new

; Remove duplicate entries from the table
nodup = rem_dup(list.name)
list = list[nodup]
lenlist = n_elements(list)
ra = dblarr(lenlist, /nozero)
dec = dblarr(lenlist, /nozero)

; Find the entries that do and don't have an RA
ra_yes = where(list.ra2010 gt 0, complement=ra_no)
ra[ra_yes] = list[ra_yes].ra2010
dec[ra_yes] = list[ra_yes].dec2010

if ra_no[0] ne -1 then begin
stop
    list_rano = list[ra_no].name
    lenrano = n_elements(list_rano)

; Extract coordinates from object name
; Not completed.  Hopefully, never needed!
    qset = where(strpos(list_rano, '"') eq 0)
    list_rano[qset] = strtrim(strmid(list_rano[qset], 1, 29), 2)
    rafound = dblarr(lenrano)
    decfound = dblarr(lenrano)
    for i=1, lenrano-1 do begin
        case strpos(list_rano[i], 6) of
            'SDSS J' : hascoord = 6
            '2MASS ' : hascoord = 7
            'ULAS J' : hascoord = 6
            'USco J' : hascoord = 6
            '2MASSI' : hascoord = 8
            '2MASSW' : hascoord = 8
            'DENIS ' : hascoord = 7
            'DENIS-' : hascoord = 9
            'EROS-M' : hascoord = 9
            'SDSSp ' : hascoord = 7
            'SSSPM ' : hascoord = 7
            '1RXS J' : hascoord = 6
            'CFBDS ' : hascoord = 7
            'CFBDSI' : hascoord = 9
            'Cha 13' : hascoord = 4
            'IRAC J' : hascoord = 6
            'LSR 16' : hascoord = 4
            'Lup 15' : hascoord = 4
            'NTTDF ' : hascoord = 6
            'SCR 18' : hascoord = 4
            'SIPS J' : hascoord = 6
            'UGCS J' : hascoord = 6
            'UGPS J' : hascoord = 6
            else : hascoord = -1
        endcase
        if i eq 0 then hascoord = 6     ;special case
        if hascoord ge 0 then begin
            coord = strmid(list_rano[i], hascoord)
            pm = strpos(coord, '+') > strpos(cood, '-')
            rastr = strmid(coord,0,pm)
            
            decstr = strmid(coord,pm)

        endif 
    endfor 
; Get RA and DEC values from SIMBAD for any entries that need them
;     rasim = dblarr(lenrano, /nozero)
;     decsim = dblarr(lenrano, /nozero)
;     f = dblarr(lenrano, /nozero)
;     s = strarr(lenrano)
;     for i=0, lenrano-1 do querysimbad, list_rano[i], rasim[i], decsim[i], $
;       found=f[i], server=s[i]
;     rafound = rasim
;     decfound = decsim
endif

if keyword_set(names) then begin
    newlist = QV_list('II/294', list.name, .15, mergedata=list)
    outfile = 'Desktop/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi_sdss_names'
endif else begin
    newlist = QV_list('II/294', [[ra], [dec]], .15, mergedata=list)
    outfile = 'Desktop/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi_sdss'
endelse 

;Write the match list to a csv file
if keyword_set(csv) then begin
    head = 'Name,Class,uMag_S,e_uMag_S,gMag_S,e_gMag_S,rMag_S,e_rMag_S,'$
           +'iMag_S,e_iMag_S,zMag_S,e_zMag_S,ra_S,dec_S,obsdate_S,Q'
    textout = outfile+'.csv'
    forprint, newlist.name, newlist.cl, newlist.umag, newlist.e_umag, newlist.gmag, $
              newlist.e_gmag, newlist.rmag, newlist.e_rmag, newlist.imag, newlist.e_imag, $
              newlist.zmag, newlist.e_zmag, newlist.raj2000, $
              newlist.dej2000, newlist.obsdate, newlist.q, newlist.ra2010, newlist.dec2010, $
              textout=textout, comment='#'+head, _EXTRA=ex, $
              format='(a31,",",i1,",",f7.4,",",f5.3,",",f7.4,",",f5.3,",",f7.4,",",f5.3,",",f7.4,",",f5.3,",",f7.4,",",f5.3,",",f10.6,",",f10.6,",",f9.4,",",i1)'
endif

;.sav file
if not keyword_set(nosav) then begin
    filename = outfile+'.sav'
    save, newlist, filename=filename, _EXTRA=ex
endif


END

; Search by coordinates:  911 matches for 484 out of 1072 targets.
; Search by names:  878 matches for 475 out of 1072 targets.
