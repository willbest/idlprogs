ascii=1
radius=2
;star=1
;PRO SPOTTEST, MATCHLIST, ASCII=ascii, NOSAV=nosav, RADIUS=radius, $
;              STAR=star, _EXTRA=ex

;  Determines whether SDSS matches to candidates from PS1/2MASS are
;  within a given radius of their expected position.
;  Writes the list of good matches to a .sav file,
;     ~/ps1/hispeed/goodlist.sav
;  
;  HISTORY
;  Written by Will Best (IfA), 10/14/2011
;
;  INPUTS
;       MATCHLIST - .sav file with structure containing matches
;
;  KEYWORDS
;       ASCII - Write the final good match list to an ascii file,
;                  ~/ps1/hispeed/goodlist.ascii
;               (Match list will also be written to a .sav file,
;               unless the NOSAV keyword is also set.)
;       NOSAV - Do not write the match list to a .sav file.
;               (WARNING: If NOSAV is set and ASCII is not set, no
;               output file will be generated!)
;       RADIUS - Fixed radius to search around the predicted SDSS
;                position.  Default is 1".
;       STAR - Use SDSS flags to choose only stars (class 6) and
;              objects of unknown type (class 0).
;
;  CALLS
;       GCIRC
;

; Load data into named ("merge") structure array called rawlist.
if n_elements(matchlist) eq 0 then matchlist = '~/ps1/hispeed/rawlist.sav'
restore, matchlist

; TEMPORARY: smaller test list
;rawlist = rawlist[0:49]
lenr = n_elements(rawlist)

if keyword_set(star) then begin
    stars = where((rawlist.cl eq 0) or (rawlist.cl eq 6))
    rawlist = rawlist[stars]
endif

; Calculate predicted coordinates for SDSS match based on epochs
delra = double(rawlist.rap - rawlist.ra2)   ;degrees
delde = double(rawlist.dep - rawlist.de2)   ;degrees
theta = atan(delde/delra)           ;radians

; Convert PS1 time to years, PS1 zero date 2001/1/29
tp = (rawlist.tmean+28D)/365.26 + 2001
deltime = rawlist.obsdate - tp
;ts = (rawlist.obsdate - 2001)*365.26 - 28
;deltime = ts - rawlist.tmean
pm = rawlist.pm/3600D
rasc = pm*deltime*cos(theta) + rawlist.racenter   ;calculated SDSS location
desc = pm*deltime*sin(theta) + rawlist.decenter

; Calculate the separation
ras = rawlist.raj2000    ;actual SDSS location
des = rawlist.dej2000
gcirc, 2, rasc, desc, ras, des, sep

; Find matches within search radius
if not keyword_set(radius) then radius = 1.
radius = double(radius)
good = where(sep lt radius)
goodlist = rawlist[good]

; Write the match list to an ascii file
if keyword_set(ascii) then begin
    textout = '~/ps1/hispeed/goodlist.ascii'
    head = strjoin(string(tag_names(goodlist)),'   ')
    forprint, goodlist, textout=textout, comment='#'+head, _EXTRA=ex
endif

;.sav file
if not keyword_set(nosav) then begin
    filename = '~/Astro/PS1stamps/getstamp.sav'
    save, goodlist, filename=filename, _EXTRA=ex
endif

; Write a subset of the tags to an ascii file for quick graphing.
textout = '~/ps1/hispeed/checklist.ascii'
head2 = 'ID   Class   uMag_S   e_uMag_S   gMag_S   e_gMag_S   rMag_S   e_rMag_S   '$
  +'iMag_S   e_iMag_S   zMag_S   e_zMag_S   ra_S   dec_S   obsdate_S   Q'
forprint, goodlist.id, goodlist.cl, goodlist.umag, goodlist.e_umag, goodlist.gmag, $
  goodlist.e_gmag, goodlist.rmag, goodlist.e_rmag, goodlist.imag, goodlist.e_imag, $
  goodlist.zmag, goodlist.e_zmag, goodlist.raj2000, $
  goodlist.dej2000, goodlist.obsdate, goodlist.q, $
  textout=textout, comment='#'+head2, $
  format='(i7,3x,i1,3x,f7.4,3x,f5.3,3x,f7.4,3x,f5.3,3x,f7.4,3x,f5.3,3x,f7.4,3x,f5.3,3x,f7.4,3x,f5.3,3x,f10.6,3x,f10.6,3x,f9.4,3x,i1)'

;summarize results
print, string(n_elements(good), radius, $
    format='("Found a total of ",i4," matches within ",f4.2," arcseconds of their")')
print, string(lenr, $
    format='("predicted locations, out of ",i4," total matches.")')

; id = id number from table, for later matching
; pm = proper motion
; Tmean = mean date of 2MASS and PS1 observations (zero date 2001/1/29)
; ra2, de2 = 2MASS coordinates
; rap, dep = PS1 coordinates
;QueryVizier needs a 2-element vector [RA, DEC], both in DEGREES.
;mode, cl(3=galaxy, 6=star), SDSS(coords), M_SDSS(multiple objects same name),
;zsp(redshift), umag, e_umag, gmag, e_gmag, rmag, e_rmag, imag,
;e_imag, zmag, e_zmag, raJ2000(decimal degrees), deJ2000(decimal degrees), 
;obsdate (decimal years), q(quality, 2=acceptable, 3=good)
;racenter, decenter

END
