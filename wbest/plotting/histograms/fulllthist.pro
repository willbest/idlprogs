ps=1

device, decomposed=0
lincolr_wb, /silent

; New megatable
megafile = '~/Dropbox/panstarrs-BD/Known_Objects/MegaTable.csv'
readcol, megafile, k_spt, $
         format='x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,f,x', $
         delim=',', comment='#', /preserve_null, /silent

if keyword_set(ps) then begin
    fpath = '~/Astro/699-1/observations/spt.hist.known'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 1, retain=2, xsize=800, ysize=600

sptaxis = ['L0','L5','T0','T5','Y0']
sptval = [10,15,20,25,30]
plothist, k_spt, xtitle='Spectral Type', ytitle='Number Known', charsize=2.0, color=0, backg=1, bin=1, $
          xrange=[10, 31], yrange=[0, 265], ystyle=1, xtickname=sptaxis, xtickv=spt, /nan, thick=6

; Add vertical dashed lines
vline, 16, color=0, lines=2, thick=6
vline, 25, color=0, lines=2, thick=6

if keyword_set(ps) then ps_close

END
