boundary=1
ps=1
;PRO MAKEBAR

;  Program frame for reading in a table and making a barplot.
;
;  USES
;  Built-in IDL procedure bar_plot.pro
;
;  HISTORY
;  Written by Will Best (IfA), 05/23/2012
;
;  OPTIONAL KEYWORD INPUTS:
;      BOUNDARY - If set, label the bars at their boundaries instead of their
;                 middles.
;      PS - Output to postscript file.
;

; Read in the table
infile = '~/Astro/699-1/mergeradius.ascii'
; a=name, b=rad, c=obj, d=qual, format='a,f,ul,u'
readcol, infile, a, b, c, d, comment='#', format='a,f,ul,u', /silent
len = n_elements(a)

; Arrays for non-culmulative bars
c2 = [2*c[len-1], c] - c
d2 = d - [0, d]

; Basic plot set-up
lincolr, /silent
black = 0
white = 1
gray = 12
;pichar='!7p!X'

; Details for this bar plot
p = d2                          ; array to plot
;yra = [0,10000]                 ; y-range
yra = [0,1900]                 ; y-range
xtit = 'Matching Radius (arcsec)'
ytit = 'Number of PS1 + WISE Matches'
barn = trim(b)                  ; bar names
axis = black
back = white
col = replicate(black, len)     ; bar outline colors
barc = replicate(white, len)    ; bar fill colors
chs = 1.5                       ; character size

if keyword_set(boundary) then begin
    bars = 0                    ; space between bars
    cgbarplot, p, barspace=bars, /nodata, background=back, colors=back, barcoords=g
    wdelete
    xtiv = g + (g[2]-g[1])/2.
    xtil = 0.00001
endif

if keyword_set(ps) then begin
    fpath = '~/Astro/699-1/paper/quality_bar'
    ps_open, fpath, /color, /en, thick=4
endif else window, retain=2, xsize=800, ysize=600

if keyword_set(boundary) then begin
    cgbarplot, p, xtitle=xtit, ytitle=ytit, charsize=chs, barnames=barn, $
               yrange=yra, $                 ; if you want this
               xticklen=xtil, xtickv=xtiv, barspace=bars, $
               xchars=1.0, ychars=1.0, $     ; if you want this
               axiscolor=axis, background=back, colors=barc, /outline, oplotcolors=col
endif else begin
    cgbarplot, p, xtitle=xtit, ytitle=ytit, charsize=chs, barnames=barn, $
               yrange=yra, $                 ; if you want this
               axiscolor=axis, background=back, colors=barc, /outline, oplotcolors=col
endelse

; Add a vertical dashed line
vline, g[4]+(g[2]-g[1])/2., color=axis, lines=2

if keyword_set(ps) then ps_close

END


;           barcoords=g, $
;; IDL> print, g                                                            
;;       0.47499995       1.4250001       2.3750000       3.3250001       4.2750003       5.2250004
;;        6.1750001       7.1249993       8.0749994       9.0249996       9.9749997       10.925000
;;        11.874999       12.824999
;; IDL> h=[0,g]                                                             
;; IDL> print, (g-h)/2                                                      
;;       0.23749998      0.47500007      0.47499995      0.47500007      0.47500007      0.47500007
;;       0.47499983      0.47499960      0.47500007      0.47500007      0.47500007      0.47500007
;;       0.47499960      0.47500007
;; IDL> print, g+.475, format='("[", f4.2, 9(",",f4.2), 4(",",f5.2), "]")'
;; [0.95,1.90,2.85,3.80,4.75,5.70,6.65,7.60,8.55,9.50,10.45,11.40,12.35,13.30]
