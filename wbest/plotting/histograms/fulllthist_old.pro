;ps=1

device, decomposed=0
lincolr_wb, /silent

; Kimberly's megatable
restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
fullhist = wise_new.spt
fullhist = fullhist[where(fullhist gt 0 and fullhist lt 30)]

; Kirkpatrick et al 2011
kirk11 = '~/Dropbox/panstarrs-BD/Known_Objects/pre_DA_papers/Kirkpatrick11t4.csv'
readcol, kirk11, kspt, format='x,x,x,x,x,x,x,f', comment='#', delim=','
fullhist = [fullhist, kspt]

; Schmidt et al 2010
sch10 = '~/Dropbox/panstarrs-BD/Known_Objects/pre_DA_papers/Schmidt10disc.csv'
readcol, sch10, sspt, format='x,x,x,x,f', comment='_', delim=','
fullhist = [fullhist, sspt]

if keyword_set(ps) then begin
    fpath = '~/Astro/699-1/paper/spthist'
    ps_open, fpath, /color, /en, thick=4
endif else window, 1, retain=2, xsize=800, ysize=600

sptaxis = ['L0','L5','T0','T5','Y0']
sptval = [10,15,20,25,30]
plothist, fullhist, xtitle='Spectral Type', ytitle='Number Known', charsize=2.0, color=0, backg=1, bin=1, $
          xrange=[10, 31], xtickname=sptaxis, xtickv=spt

; Add vertical dashed lines
vline, 16, color=0, lines=2
vline, 24, color=0, lines=2

if keyword_set(ps) then ps_close

END
