flipleg=1
;labels=1
ps=1
;PRO SPEXSIX, sname1, smame2, objname, OUTFILE=outfile, ps=ps, WIN=win, _extra=extrakey

;  overlay one SpeX spectrum onto another
;
;  HISTORY
;  Written by Will Best (IfA), 07/18/2013
; 

spexpath = '~/Dropbox/panstarrs-BD/SPECTRA/'
objname = strarr(6)

sname0 = spexpath+'PW-11-022304_0.8SXD_2012sep26_merged.fits'
objname[0] = 'PSO J307.6+07 SXD  2012 Sep 26 UT'
sname1 = spexpath+'PW-11-022304_0.8prism_2012sep20.fits'
objname[1] = 'PSO J307.6+07 prism  2012 Sep 20 UT'
sname2 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
objname[2] = 'PSO J307.6+07 prism  2013 Apr 03 UT'
sname3 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
objname[3] = 'PSO J307.6+07_corrected  2013 Apr 03 UT'
sname4 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
objname[4] = 'PSO J307.6+07 prism  2013 Apr 04 UT'
sname5 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
objname[5] = 'PSO J307.6+07 prism  2013 Apr 05 UT'

col = [0, 3, 4, 4, 2, 12]

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname2 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h12 2013 Apr 05 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h12 2013 Apr 05 UT'

; wl region for normalization
;wlnorm = [1.2, 1.3]
;wlnorm = [1.15, 1.35]
;wlnorm = [1.5, 1.6]
;wlnorm = [1.26, 1.27]   ; J-band peak
;wlnorm = [1.27, 1.28]   ; J-band peak
;wlnorm = [1.575, 1.585] ; H-band peak
wlnorm = [1.575, 1.695] ; H-band
;wlnorm = [2.07, 2.09]   ; K-band peak

scl_spex = 1.

; get IRTF/Spex data
spex0 = readfits(sname0, head0)
wl_spex0 = spex0[*, 0]
f_spex0 = spex0[*, 1]
ytit0 = sxpar(head0, 'YTITLE')

spex1 = readfits(sname1, head1)
wl_spex1 = spex1[*, 0]
f_spex1 = spex1[*, 1]
ytit1 = sxpar(head1, 'YTITLE')

spex2 = readfits(sname2, head2)
wl_spex2 = spex2[*, 0]
f_spex2 = spex2[*, 1]
ytit2 = sxpar(head2, 'YTITLE')

spex3 = readfits(sname3, head3)
wl_spex3 = spex3[*, 0]
f_spex3 = spex3[*, 1]
ytit3 = sxpar(head3, 'YTITLE')

spex4 = readfits(sname4, head4)
wl_spex4 = spex4[*, 0]
f_spex4 = spex4[*, 1]
ytit4 = sxpar(head4, 'YTITLE')

spex5 = readfits(sname5, head5)
wl_spex5 = spex5[*, 0]
f_spex5 = spex5[*, 1]
ytit5 = sxpar(head5, 'YTITLE')

;----------------------------------------------------------------------
; choose plotting range and scaling
;SCALING_FACTOR = 1./max(spex1[between(spex1[*, 0], 1.0, 2.4), 1])
SCALING_FACTOR = 1./max(spex0[between(spex0[*, 0], wlnorm[0], wlnorm[1]), 1])
;SCALING_FACTOR = 10.34    ; from Brendan, for absolute flux calibration based on J+H 2MASS photometry

f_spex0 = SCALING_FACTOR * f_spex0
f_spex1 = SCALING_FACTOR * f_spex1
f_spex2 = SCALING_FACTOR * f_spex2
f_spex3 = SCALING_FACTOR * f_spex3
f_spex4 = SCALING_FACTOR * f_spex4
f_spex5 = SCALING_FACTOR * f_spex5

xr = [0.8, 2.5]
;if keyword_set(legend) then yr = [0, 1.3*max(f_spex1[between(wl_spex1, 1.0, 2.3)])] else $
  yr = [0, 1.2*max(f_spex0[between(wl_spex0, 1.0, 2.3)])]
yr = yr/scl_spex

;!x.margin = [10, 30]

;um='!9'+STRING(109B)+'!Xm'
um = textoidl('\mu')+'m'

device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/specplot1'
    ps_open, outfile, /color, /en, thick=4;, /portrait
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=600
;    window, win, retain=2, xsize=600, ysize=800
endelse

;----------------------------------------------------------------------
; (1) plot first object
;----------------------------------------------------------------------
; smooth the data
f_spex0 = smooth(f_spex0, 8, /nan)

; make box
plot, [0], /nodata, col=0, backg=1, $
      xr=xr, /xs, yr=yr, /ys, $
      xtitle='wavelength ('+um+')', xchars=1.5, xmar=[11,3], $
      ;ytitle=textoidl('F_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
      ;; xtitle=textoidl('\lambda ('+um+')'), $
      ytitle=textoidl('!8f!3_\lambda (normalized)'), ychars=1.5, ymar=[5.5,2], $
      ;; ;ytitle=textoidl('!8f!3_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
      ;; chars=1.5, $
;      pos = [0.1, 0.3, 0.9, 0.7]
      _extra=extrakey
;xyouts, /norm, 0.0, 0.5, align=0.5, $
;        textoidl('F_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
;        orient = 90, chars=2.3
;xyouts, 0.94, 0.92, /norm, align = 1, objname, chars = 1.5, col = 12, chart = 1.25*!p.thick

f_spex0 = f_spex0/scl_spex
thick0 = 6
; remove high-noise sections
if n_elements(wl_spex0) gt 600 then begin
    rem_ind = where(((wl_spex0 gt 1.35) and (wl_spex0 lt 1.41)) or ((wl_spex0 gt 1.85) and (wl_spex0 lt 1.91)))
    remove, rem_ind, wl_spex0, f_spex0
    f_spex0[max(where(wl_spex0 le 1.35))] = !Values.F_NAN
    f_spex0[max(where(wl_spex0 le 1.85))] = !Values.F_NAN
    thick0 = 4
endif
; prevent IDL from connecting points across the removed section
; plot object 1 data
oplot, wl_spex0, f_spex0, col=col[0], thick=thick0

;----------------------------------------------------------------------
; (2) overplot second object
;----------------------------------------------------------------------
; rescale to match science object
w0 = between(wl_spex0, wlnorm[0], wlnorm[1], nw)
scl0 = avg(f_spex0[w0])
w1 = between(wl_spex1, wlnorm[0], wlnorm[1], nw)
scl1 = avg(f_spex1[w1])
f_spex1 = f_spex1/scl1*scl0
 
; plot object 2 data
oplot, wl_spex1, f_spex1, col=col[1], thick=6 ;, ps = 10

;----------------------------------------------------------------------
; (3) overplot third object
;----------------------------------------------------------------------
; rescale to match science object
w2 = between(wl_spex2, wlnorm[0], wlnorm[1], nw)
scl2 = avg(f_spex2[w2])
f_spex2 = f_spex2/scl2*scl0
 
; plot object 2 data
oplot, wl_spex2, f_spex2, col=col[2], thick=6, lines=1

;----------------------------------------------------------------------
; (4) overplot fourth object
;----------------------------------------------------------------------
; rescale to match science object
w3 = between(wl_spex3, wlnorm[0], wlnorm[1], nw)
scl3 = avg(f_spex3[w3])
f_spex3 = f_spex3/scl3*scl0
 
; plot object 4 data
oplot, wl_spex3, f_spex3 / fratio_smooth, col=col[3], thick=6 ;, ps = 10

;----------------------------------------------------------------------
; (5) overplot second object
;----------------------------------------------------------------------
; rescale to match science object
w4 = between(wl_spex4, wlnorm[0], wlnorm[1], nw)
scl4 = avg(f_spex4[w4])
f_spex4 = f_spex4/scl4*scl0
 
; plot object 5 data
oplot, wl_spex4, f_spex4, col=col[4], thick=6 ;, ps = 10

;----------------------------------------------------------------------
; (6) overplot sixth object
;----------------------------------------------------------------------
; rescale to match science object
w5 = between(wl_spex5, wlnorm[0], wlnorm[1], nw)
scl5 = avg(f_spex5[w5])
f_spex5 = f_spex5/scl5*scl0
 
; plot object 6 data
oplot, wl_spex5, f_spex5, col=col[5], thick=6 ;, ps = 10


;----------------------------------------
; label spectral features
;----------------------------------------
if keyword_set(labels) then begin
ct = !p.thick < 3
cs = 1.1;0.8
cc = 0
htwoo = 'H!D2!NO'
meth = ' CH!D4!N'
dy = 0.025*(!y.crange(1)-!y.crange(0))
wlmin = !x.crange(0)+0.03
wlmax = !x.crange(1)-0.03

;;; ORDER
; PSO J339.0+51
; PSO J307.6+07
; PSO J140.2+45 (if no third set, use second set)

; J-band labels
yp_j = max(f_spex1(between(wl_spex1, 1.2, 1.3)))+0.1*(!y.crange(1)-!y.crange(0))
ybar_j = [yp_j-dy, yp_j, yp_j, yp_j-dy]

plots, [0.9, 0.9, 1.6, 1.6] > wlmin, ybar_j+.02, thick=ct, color = cc
xyouts, 1.25, yp_j+1.2*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [0.9, 0.9, 1.6, 1.6] > wlmin, ybar_j+.04, thick=ct, color = cc
;xyouts, 1.25, yp_j+1.8*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [0.9, 0.9, 1.6, 1.6] > wlmin, ybar_j+.00, thick=ct, color = cc
;xyouts, 1.25, yp_j+1.0*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc

plots, [1.12, 1.12, 1.2, 1.2], ybar_j-2.0*dy , thick=ct, color = cc
xyouts, 1.157, yp_j-1.4*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [1.12, 1.12, 1.2, 1.2], ybar_j-1.0*dy , thick=ct, color = cc
;xyouts, 1.157, yp_j-0.6*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [1.12, 1.12, 1.2, 1.2], ybar_j-2.2*dy , thick=ct, color = cc
;xyouts, 1.157, yp_j-1.4*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc

plots, [1.3, 1.3, 1.33, 1.33], ybar_j-2.0*dy, thick=ct, color = cc
xyouts, 1.311, yp_j-1.4*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [1.3, 1.3, 1.33, 1.33], ybar_j-1.0*dy, thick=ct, color = cc
;xyouts, 1.311, yp_j-0.6*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc

; H-band labels
yp_h = max(f_spex1(between(wl_spex1, 1.4, 1.6)))+0.1
ybar_h = [yp_h-dy, yp_h, yp_h, yp_h-dy]

plots, [1.6, 1.6, 1.8, 1.8], ybar_h+1.4*dy, thick=ct, color = cc
xyouts, 1.7, yp_h+2.*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [1.6, 1.6, 1.8, 1.8], ybar_h, thick=ct, color = cc
;xyouts, 1.7, yp_h+dy, meth, align=0.5, chars=cs, charthick=ct, color = cc

; K-band labels
yp_k = max(f_spex1(between(wl_spex1, 1.9, 2.3)))+0.2
ybar_k = [yp_k-dy, yp_k, yp_k, yp_k-dy]
yp_hk = avg([yp_h, yp_k])
ybar_hk = [yp_hk-dy, yp_hk, yp_hk, yp_hk-dy]

plots, [1.7, 1.7, 2.1, 2.1], ybar_hk+2.*dy, thick=ct, color = cc
xyouts, 1.9, yp_hk+2.6*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [1.7, 1.7, 2.1, 2.1], ybar_hk, thick=ct, color = cc
;xyouts, 1.9, yp_hk+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [1.7, 1.7, 2.1, 2.1], ybar_hk+1.6*dy, thick=ct, color = cc
;xyouts, 1.9, yp_hk+2.4*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc

plots, [2.1, 2.1, 2.4, 2.4], ybar_k+.4*dy, thick=ct, color = cc
xyouts, 2.25, yp_k+dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [2.1, 2.1, 2.4, 2.4], ybar_k-.1, thick=ct, color = cc
;xyouts, 2.25, yp_k+dy-.1, meth, align=0.5, chars=cs, charthick=ct, color = cc

plots, [2.3, 2.3, 2.5, 2.5] < wlmax, ybar_k/2+.4*dy, thick=ct, color = cc
xyouts, 2.39, yp_k/2.+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [2.3, 2.3, 2.5, 2.5] < wlmax, ybar_k/3., thick=ct, color = cc
;xyouts, 2.39, yp_k/3.+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;plots, [2.3, 2.3, 2.5, 2.5] < wlmax, ybar_k/3.+4*dy, thick=ct, color = cc
;xyouts, 2.39, yp_k/3.+5*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc

plots, [2, 2, 2.5, 2.5] < wlmax, yp_h+1.4*dy, thick=ct, color = cc
xyouts, 2.25, yp_h+2.*dy, 'H!D2!N', align=0.5, chars=cs, charthick=ct, color = cc
;plots, [2, 2, 2.5, 2.5] < wlmax, yp_h, thick=ct, color = cc
;xyouts, 2.25, yp_h+dy, 'H!D2!N', align=0.5, chars=cs, charthick=ct, color = cc

; Y-band labels
yp_y =  max(f_spex1(between(wl_spex1, 0.7, 1.1)));+0.2
ybar_y = [yp_y-dy, yp_y, yp_y, yp_y-dy]

plots, [0.7, 0.7, 1.05, 1.05] > wlmin, ybar_y+1.*dy, thick=ct, color = cc
xyouts, (1.08+!x.crange(0))/2, yp_y+1.6*dy, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
;plots, [0.7, 0.7, 1.05, 1.05] > wlmin, ybar_y, thick=ct, color = cc
;xyouts, (1.08+!x.crange(0))/2, yp_y+dy-.01, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
;xyouts, 0.98, yp_y+dy, 'K I', align=0.5, chars=cs, charthick=ct
endif

; LEGEND
if keyword_set(flipleg) then begin
    leg = [objname[1], objname[0], objname[2:*]]
    col = [col[1], col[0], col[2:*]]
    tcol = col
endif else begin
    leg = objname
    tcol = col
endelse
legend, leg, $
;legend, [objname+' ('+sptypenum(spt,/rev)+')', specname+' ('+specspt+')'], $
        color=col, $
        ;line = intarr(2), $
        /right, box=0, $
;        pos = [1.5, 1.09], box=0, $
        textcol=tcol, $
        chars=1.2

;plots, [1.575, 1.695], [1.0, 0.85], color=4, thick=6

if keyword_set(ps) then ps_close

end
