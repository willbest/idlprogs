;ps=1
win=1
spt=25
;PRO SPEXMATCH, SPT, DATA, NAME=name, OUTFILE=outfile, PS=ps, WIN=win

;+
;  compare a SpeX data spectrum with a SpeX T-dwarf standard spectrum
;  wrapper for spectraplot.pro
;
;  HISTORY
;  Developed by Mike Liu; hacked several times for different projects
;  Hacked into this form by Will Best (IfA), 11/30/2012
;
;  INPUTS
;      SPT - Spectral type of standard.  L8=18, T0=20, etc.
;      DATA - SpeX data fits file
;
;  KEYWORDS
;      NAME - Name for the data spectrum, to be displayed on the graph
;      OUTFILE - path+name for postcript output
;      PS - postscript output
;      WIN - specify window number for screen display
;
; The reduced SpeX fits files are 3-row arrays.
; row 0 is the wavelength solution (microns)
; row 1 is the reduced flux (ergs s^-1 cm^-2 ang^-1)
; row 2 is the error on the flux
;-

; Where is the data spectrum?
if n_elements(data) eq 0 then begin
    datadir = '~/Dropbox/panstarrs-BD/SPECTRA/'
    datafile = 'PW-12-014644_0.8prism_2012oct07.fits'
    ;datafile = 'PW-11-022304_0.8prism_2012sep20.fits'
    data = datadir+datafile
endif

; What's the data spectrum called?
if n_elements(name) eq 0 then begin
    name = 'PSO J339.0734+51.0978'
;    name = 'PSO J307.6784+07.8263'
endif

; Check here for correct number of inputs (at least one = SpT)

; Establish common block, which spectraplot.pro will use
common spldata, spllist, spl_filename, spl_nirspt, spl_name, spl_numnirspt

; Read in the list of T dwarf standards
spllist = '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-prism/spectra.index.Tdwarf-standards.prism'
readcol2, spllist, spl_filename, spl_nirspt, spl_name, spl_numnirspt, $
          format='a,a,x,x,x,a,x,x,x,x,x,x,f', /silent

; Load a color table
device, decomposed=0
lincolr_wb, /silent
!p.color = 0
um = textoidl('\mu')+'m'

; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/specplot1'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=600
endelse
;!p.multi = [0, 1, 7, 0, 0]
;xmarg = [12,3]

; Plot the spectra
spex = readfits(data, h)
spectraplot, spex, h, name, spt, $
             xtitle=textoidl('\lambda ('+um+')'), xmarg=[8,3], ytickname=' '

xyouts, .03, .5, textoidl('!8f!3_\lambda (normalized)'), chars=1.5, align=0.5, orient=90, /normal

if keyword_set(ps) then ps_close
;!p.multi = [0, 0, 1, 0, 0]

END
