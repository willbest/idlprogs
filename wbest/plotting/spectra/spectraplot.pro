PRO SPECTRAPLOT, spex, header, objname, spt, ADDLEG=addleg, COLORS=colors, $
                 compnum=compnum, OFFSET=offset, pos=pos, STRETCH=stretch, _extra=extrakey

; plot SpeX spectrum, by itself and compared to other SpeX Prism Library spectra
;
;  HISTORY
;  Developed by Mike Liu; hacked several times for different projects
;  Hacked into this form by Will Best (IfA), 08/15/2012
;
;  KEYWORDS
;      COLORS - scalar (vector) of colors to be used for comparison plot(s)
;      OFFSET - Vertical shift for object and standard on plot
;      STRETCH - Vertical stretch factor applied to plots -- useful for stacking
;              several offset plots on one set of axes.

; list of camparison objects: spl_filename, spl_nirspt, spl_name, spl_numnirspt
common spldata

; wl region for normalization
;wlnorm = [1.2, 1.3]
;wlnorm = [1.15, 1.35]
;wlnorm = [1.5, 1.6]
;wlnorm = [1.26, 1.27]   ; J-band peak
wlnorm = [1.575, 1.585] ; H-band peak
;wlnorm = [2.07, 2.09]   ; K-band peak

SCALING_FACTOR = 1./max(spex[between(spex[*, 0], 1.0, 2.4), 1])
;SCALING_FACTOR = 10.34    ; from Brendan, for absolute flux calibration based on J+H 2MASS photometry

if n_elements(offset) eq 0 then offset = 0.
if n_elements(stretch) eq 0 then stretch = 1. 

; get IRTF/Spex data
wl_spex = spex[*, 0]
f_spex = spex[*, 1]
ytit = sxpar(header, 'YTITLE')
;;w = where(finite(f_spex) eq 0)
;;remove, w, wl_spex, f_spex
f_spex = SCALING_FACTOR * f_spex

; choose camparison object
sptm = where(spl_numnirspt eq fix(spt))
if n_elements(compnum) eq 0 then compnum = 0
comp = sptm[compnum]
specdir = '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-prism/all/'
speclist = specdir + [spl_filename[comp]]
snlong = spl_name[comp]
specspt = spl_nirspt[comp]
spec_smooth = [1,1,1]

; Format the standard spectra names nicely
pp = strpos(snlong, '_J')
pm = replicate('-', n_elements(snlong))
pd = strarr(n_elements(snlong))

pl = where(strpos(snlong, '+') ge 0, comp=mi)
if pl[0] ge 0 then begin
    pm[pl] = '+'
    pd[pl] = strpos(snlong, '+')
endif
if mi[0] ge 0 then pd[mi] = strpos(snlong[mi], '-', /reverse_search) 
specname = strmid(snlong, 0, 1#pp)+' '+strmid(snlong, 1#pp+2, 4)+pm+strmid(snlong, 1#pd+1, 4)

if n_elements(colors) eq 0 then colors = [3, 6, 13]

;----------------------------------------------------------------------
; choose plotting range and scaling
xr = [0.8, 2.5]
yr = [0, 1.4*max(f_spex[between(wl_spex, 1.0, 2.3)])]

;w = between(wl_spex, wlnorm[0], wlnorm[1], nw)
;scl_spex = 10.^floor(alog10(avg(f_spex(w))))   ; remove exponent from data
scl_spex = 1

yr = yr/scl_spex

;!x.margin = [10, 30]

;um='!9'+STRING(109B)+'!Xm'
um = textoidl('\mu')+'m'

;----------------------------------------------------------------------
; (1) plot of science target only
;----------------------------------------------------------------------
; make box
plot, [0], /nodata, col=0, backg=1, $
      xr=xr, /xs, yr=yr, /ys, $
      ;xtitle='wavelength ('+um+')', $
      ;ytitle=textoidl('F_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
      ;; xtitle=textoidl('\lambda ('+um+')'), $
      ;; ytitle=textoidl('!8f!3_\lambda (normalized)'), $
      ;; ;ytitle=textoidl('!8f!3_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
      chars=1.5, $
;      pos = [0.1, 0.3, 0.9, 0.7]
      _extra=extrakey
;xyouts, /norm, 0.0, 0.5, align=0.5, $
;        textoidl('F_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
;        orient = 90, chars=2.3
;xyouts, 0.94, 0.92, /norm, align = 1, objname, chars = 1.5, col = 12, chart = 1.25*!p.thick


; plot IRTF data
;f_spex = f_spex/scl_spex + offset
oplot, wl_spex, f_spex*stretch+offset, col=0, thick=6


;----------------------------------------------------------------------
; (2) Overplot other objects
;----------------------------------------------------------------------
nspec = n_elements(speclist)
for i=0, nspec-1 do begin

    filebreak, speclist(i), name = name, ext = ext
    if (ext eq 'fits') then begin
       sp = readfits(speclist(i), /silent)
       wl2 = sp(*, 0)
       f2 = sp(*, 1)
    endif else $
       readcol2, speclist[i], wl2, f2

    ; smooth the data
    f2 = smooth(f2, spec_smooth(i), /nan)

    ; rescale to match science object
    w = between(wl_spex, wlnorm(0), wlnorm(1), nw)
    scl = avg(f_spex(w))
    w2 = between(wl2, wlnorm(0), wlnorm(1), nw)
    scl2 = avg(f2(w2))
    f2 = f2/scl2*scl

    ;mplot, [0], /nodata, xr=xr, yr=yr, /xs, /ys, $
    ;       xtitle='wavelength ('+um+')', $
    ;       ;ytitle=textoidl('F_\lambda (W/m^2/')+um+')', $
    ;       /showtoptick, $
    ;       /showbottomtick, $
    ;       chars=3

 
   ; overplot other T dwarf
    oplot, wl2, f2*stretch+offset, col=colors(i), thick = 4;, ps = 10

endfor      



;----------------------------------------
; label spectral features
;----------------------------------------
;; ct = !p.thick < 3
;; cs = 0.8
;; cc = 0
;; htwoo = 'H!D2!NO'
;; meth = ' CH!D4!N'
;; dy = 0.025*(!y.crange(1)-!y.crange(0))
;; wlmin = !x.crange(0)+0.03
;; wlmax = !x.crange(1)-0.03
;; ; J-band labels
;; yp_j = max(f_spex(between(wl_spex, 1.2, 1.3)))+0.1*(!y.crange(1)-!y.crange(0))
;; ybar_j = [yp_j-dy, yp_j, yp_j, yp_j-dy]
;; plots, [0.9, 0.9, 1.6, 1.6] > wlmin, ybar_j, thick=ct, color = cc
;; xyouts, 1.25, yp_j+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;; plots, [1.12, 1.12, 1.2, 1.2], ybar_j-11*dy , thick=ct, color = cc
;; xyouts, 1.16, yp_j-10*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;; plots, [1.3, 1.3, 1.33, 1.33], ybar_j-11*dy, thick=ct, color = cc
;; xyouts, 1.32, yp_j-10*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;; ; H-band labels
;; yp_h = max(f_spex(between(wl_spex, 1.4, 1.6)))+0.2
;; ybar_h = [yp_h-dy, yp_h, yp_h, yp_h-dy]
;; plots, [1.6, 1.6, 1.8, 1.8], ybar_h, thick=ct, color = cc
;; xyouts, 1.7, yp_h+dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;; ; K-band labels
;; yp_k = max(f_spex(between(wl_spex, 1.9, 2.3)))+0.2
;; ybar_k = [yp_k-dy, yp_k, yp_k, yp_k-dy]
;; yp_hk = avg([yp_h, yp_k])
;; ybar_hk = [yp_hk-dy, yp_hk, yp_hk, yp_hk-dy]
;; plots, [1.7, 1.7, 2.1, 2.1], ybar_hk, thick=ct, color = cc
;; xyouts, 1.9, yp_hk+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;; plots, [2.1, 2.1, 2.4, 2.4], ybar_k, thick=ct, color = cc
;; xyouts, 2.25, yp_k+dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
;; plots, [2.3, 2.3, 2.5, 2.5] < wlmax, ybar_k/3., thick=ct, color = cc
;; xyouts, 2.4, yp_k/3.+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
;; plots, [2, 2, 2.5, 2.5] < wlmax, yp_h, thick=ct, color = cc
;; xyouts, 2.25, yp_h+dy, 'H!D2!N', align=0.5, chars=cs, charthick=ct, color = cc
;; ; Y-band labels
;; yp_y =  max(f_spex(between(wl_spex, 0.7, 1.1)))+0.2
;; ybar_y = [yp_y-dy, yp_y, yp_y, yp_y-dy]
;; plots, [0.7, 0.7, 1.05, 1.05] > wlmin, ybar_y, thick=ct, color = cc
;; xyouts, 1.02, yp_y+dy, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
;; ;xyouts, 0.98, yp_y+dy, 'K I', align=0.5, chars=cs, charthick=ct


if keyword_set(addleg) then begin
    legend, [strarr(addleg), specname+' ('+specspt+')'], $
            color=colors[0:nspec-1], $
;            line = 0, $
            /right, box=0, $
            textcol=colors[0:nspec-1], $
            chars=1.3
endif else begin
    legend, [objname, specname+' ('+specspt+')'], $
;legend, [objname+' ('+sptypenum(spt,/rev)+')', specname+' ('+specspt+')'], $
            color=[!p.color, colors(0:nspec-1)], $
;            line = intarr(nspec+1), $
            /right, box=0, $
            textcol=[!p.color, colors(0:nspec-1)], $
            chars=1.3
endelse

end
