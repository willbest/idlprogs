;ps=1
win=2
;PRO PLOT_SPECTRA_LTTRANS, PS=ps, WIN=win

; plot SpeX spectrum, by itself and compared to other SpeX Prism Library spectra
; wrapper for spectraplot.pro
;
;  HISTORY
;  Developed by Mike Liu; hacked several times for different projects
;  Hacked into this form by Will Best (IfA), 08/15/2012
;
; The reduced fits files are 3-row arrays.
; row 0 is the wavelength solution (microns)
; row 1 is the reduced flux (ergs s^-1 cm^-2 ang^-1)
; row 2 is the error on the flux


; list of camparison objects -- structure called "spexprism"
  ; NOT WORKING -- somehow can't match the filenames to the file
  ; restore, '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-prism/spexprism.sav'
common spldata, spllist, spl_filename, spl_nirspt, spl_name, spl_numnirspt
;spllist = '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-prism/spectra.index.All.prism'
;readcol2, spllist, spl_filename, spl_nirspt, spl_name, spl_numnirspt, $
;          format='a,a,x,x,x,a,x,x,x,x,x,x,f', /silent
spllist = '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-prism/spectra.index.Tdwarf-standards.prism'
readcol2, spllist, spl_filename, spl_nirspt, spl_name, spl_numnirspt, $
          format='a,a,x,x,x,a,x,x,x,x,x,x,f', /silent


; Load a color table
device, decomposed=0
lincolr_wb, /silent
!p.color = 0
um = textoidl('\mu')+'m'

; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/specplot1'
    ps_open, outfile, /color, /en, /portrait, thick=4
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=600, ysize=800
endelse
!p.multi = [0, 1, 7, 0, 0]
xmarg = [12,3]

;multiplot, [1,7], gap=0, mxtitle=textoidl('\lambda ('+um+')'), mxtitsize=1.5, $
;           mytitle=textoidl('!8f!3_\lambda (normalized)'), mytitsize=1.5

spexdir = '~/Dropbox/panstarrs-BD/SPECTRA/'

; Object #1 -- 0 is good
objname = 'PW-8-002052'
spt = 21.5
spex = readfits(spexdir+'PW-8-002052_0.8prism_2012jul07.fits', h)
spectraplot, spex, h, objname, spt, compnum=0, $
             xtickname=replicate(' ',4), xmargin=xmarg, ytickname=' ', ymargin=[0,1]

; Object #2 -- 0 is good
objname = 'PW-9-003247'
spt = 21.5
spex = readfits(spexdir+'PW-9-003247_0.8prism_2012aug10.fits', h)
spectraplot, spex, h, objname, spt, compnum=0, $
             xtickname=replicate(' ',4), xmargin=xmarg, ytickname=' ', ymargin=[1,0]

; Object #3 -- 1 is good
objname = 'PW-9-001886'
spt = 21.0
spex = readfits(spexdir+'PW-9-001886_0.8prism_2012jul08.fits', h)
spectraplot, spex, h, objname, spt, compnum=1, $
             xtickname=replicate(' ',4), xmargin=xmarg, ytickname=' ', ymargin=[2,-1]

; Object #4 -- 1 is good
objname = 'PW-9-002253'
spt = 18;17.5
spex = readfits(spexdir+'PW-9-002253_0.8prism_2012jul08.fits', h)
spectraplot, spex, h, objname, spt, compnum=0, $
             xtickname=replicate(' ',4), xmargin=xmarg, ytickname=' ', ymargin=[3,-2]

; Object #5 -- 0 is good
objname = 'PW-9-022359'
spt = 24.5
spex = readfits(spexdir+'PW-9-022359_0.8prism_2012jul07.fits', h)
spectraplot, spex, h, objname, spt, compnum=0, $
             xtickname=replicate(' ',4), xmargin=xmarg, ytickname=' ', ymargin=[4,-3]

; Object #6 -- 1 is good
objname = 'PW-9-023601'
spt = 21.0
spex = readfits(spexdir+'PW-9-023601_0.8prism_2012jul06.fits', h)
spectraplot, spex, h, objname, spt, compnum=1, $
             xtickname=replicate(' ',4), xmargin=xmarg, ytickname=' ', ymargin=[5,-4]

; Object #7 -- 0 is good
objname = 'PW-10-038447'
spt = 23.0
spex = readfits(spexdir+'PW-10-038447_0.8prism_2012jul07.fits', h)
spectraplot, spex, h, objname, spt, compnum=0, $
             xtitle=textoidl('\lambda ('+um+')'), xchars=1.9, xmargin=xmarg, ymargin=[6,-5]

xyouts, .03, .5, textoidl('!8f!3_\lambda (normalized)'), chars=1.2, align=0.5, orient=90, /normal

if keyword_set(ps) then ps_close
;multiplot, /reset
!p.multi = [0, 0, 1, 0, 0]

END
