;labels=1
ps=1
;flipleg=1
;PRO SPEXRATIO, sname1, smame2, objname, OUTFILE=outfile, ps=ps, WIN=win, _extra=extrakey

;  overlay one SpeX spectrum onto another, and calculate the flux difference in
;  a given wavelength interval.
;
;  HISTORY
;  Attached to spextwo.pro by Will Best (IfA), 06/27/2013
; 

spexpath = '~/Dropbox/panstarrs-BD/SPECTRA/'

;; sname1 = spexpath+'PW-12-014644_0.8SXD_2012oct17_merged.fits'
;; objname1 = 'PSO J339.0+51 SXD  2012 Oct 17 UT'
;; sname2 = spexpath+'PW-12-014644_0.8prism_2012oct07.fits'
;; objname2 = 'PSO J339.0+51 prism  2012 Oct 07 UT'

;; sname1 = spexpath+'PW-11-022304_0.8SXD_2012sep26_merged.fits'
;; objname1 = 'PSO J307.6+07 SXD  2012 Sep 26 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2012sep20.fits'
;; objname2 = 'PSO J307.6+07 prism  2012 Sep 20 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname2 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr03_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h21 2013 Apr 03 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr04_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h33 2013 Apr 04 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr04_CorrectShape.fits'
;; objname2 = 'PSO J307.6+07 prism_CS  14h47 2013 Apr 04 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr05_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h43 2013 Apr 05 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h12 2013 Apr 05 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname2 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h12 2013 Apr 05 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname1 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h12 2013 Apr 05 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2012sep20.fits'
;; objname1 = 'PSO J307.6+07 prism  08h44 2012 Sep 20 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname2 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2012sep20.fits'
;; objname1 = 'PSO J307.6+07 prism  08h44 2012 Sep 20 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname2 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'

;; sname1 = spexpath+'PW-11-022304_0.8prism_2012sep20.fits'
;; objname1 = 'PSO J307.6+07 prism  08h44 2012 Sep 20 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h12 2013 Apr 05 UT'

;; sname1 = spexpath+'PW-11-022304_0.8SXD_2012sep26_merged.fits'
;; objname1 = 'PSO J307.6+07 SXD  05h42 2012 Sep 26 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
;; objname2 = 'PSO J307.6+07 prism  14h57 2013 Apr 03 UT'

;; sname1 = spexpath+'PW-11-022304_0.8SXD_2012sep26_merged.fits'
;; objname1 = 'PSO J307.6+07 SXD  05h42 2012 Sep 26 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
;; objname2 = 'PSO J307.6+07 prism  14h47 2013 Apr 04 UT'

;; sname1 = spexpath+'PW-11-022304_0.8SXD_2012sep26_merged.fits'
;; objname1 = 'PSO J307.6+07 SXD  05h42 2012 Sep 26 UT'
;; sname2 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
;; objname2 = 'PSO J307.6+07 prism  15h12 2013 Apr 05 UT'

;; sname1 = spexpath+'2MASS2041+0014_0.8prism_2013apr03_1.fits'
;; objname1 = '2MASS J2041+0014 prism  15h13 2013 Apr 03 UT'
;; sname2 = spexpath+'2MASS2041+0014_0.8prism_2013apr03_2.fits'
;; objname2 = '2MASS J2041+0014 prism  15h28 2013 Apr 03 UT'

;; sname1 = spexpath+'2MASS2041+0014_0.8prism_2013apr04_1.fits'
;; objname1 = '2MASS J2041+0014 prism  15h09 2013 Apr 04 UT'
;; sname2 = spexpath+'2MASS2041+0014_0.8prism_2013apr04_2.fits'
;; objname2 = '2MASS J2041+0014 prism  15h45 2013 Apr 04 UT'

;; sname1 = spexpath+'2MASS2041+0014_0.8prism_2013apr05_1.fits'
;; objname1 = '2MASS J2041+0014 prism  14h54 2013 Apr 05 UT'
;; sname2 = spexpath+'2MASS2041+0014_0.8prism_2013apr05_2.fits'
;; objname2 = '2MASS J2041+0014 prism  15h22 2013 Apr 05 UT'

;; sname1 = spexpath+'2MASS2041+0014_0.8prism_2013apr04_1.fits'
;; objname1 = '2MASS J2041+0014 prism  15h09 2013 Apr 04 UT'
;; sname2 = spexpath+'2MASS2041+0014_0.8prism_2013apr04_CorrectShape.fits'
;; objname2 = '2MASS J2041+0014 prism  15h09 2013 Apr 04 UT'

sname1 = spexpath+'2MASS2041+0014_0.8prism_2013apr03_1.fits'
objname1 = '2MASS J2041+0014 prism  15h13 2013 Apr 03 UT'
sname2 = spexpath+'2MASS2041+0014_0.8prism_2013apr04_1.fits'
objname2 = '2MASS J2041+0014 prism  15h09 2013 Apr 04 UT'

;; sname1 = spexpath+'2MASS2041+0014_0.8prism_2013apr03_1.fits'
;; objname1 = '2MASS J2041+0014 prism  15h13 2013 Apr 03 UT'
;; sname2 = spexpath+'2MASS2041+0014_0.8prism_2013apr05_1.fits'
;; objname2 = '2MASS J2041+0014 prism  14h54 2013 Apr 05 UT'

;; sname1 = spexpath+'2MASS2041+0014_0.8prism_2013apr04_1.fits'
;; objname1 = '2MASS J2041+0014 prism  15h09 2013 Apr 04 UT'
;; sname2 = spexpath+'2MASS2041+0014_0.8prism_2013apr05_1.fits'
;; objname2 = '2MASS J2041+0014 prism  14h54 2013 Apr 05 UT'

;; sname1 = spexpath+'PW-5-011389_0.8prism_2012nov08.fits'
;; objname1 = 'PSO J140.2+45 prism  2012 Nov 08 UT'
;; sname2 = spexpath+'ps1par-1212-1193_0.8prism_2013jan25.fits'
;; objname2 = 'ps1par-1212-1193 prism  2013 Jan 25 UT'


; wl region for normalization
;wlnorm = [1.2, 1.3]
;wlnorm = [1.15, 1.35]
;wlnorm = [1.5, 1.6]
;wlnorm = [1.26, 1.27]   ; J-band peak
;wlnorm = [1.27, 1.28]   ; J-band peak
;wlnorm = [1.575, 1.585] ; H-band peak
wlnorm = [1.575, 1.695] ; H-band
;wlnorm = [2.07, 2.09]   ; K-band peak

scl_spex = 1.

; get IRTF/Spex data
spex1 = readfits(sname1, head1)
wl_spex1 = spex1[*, 0]
f_spex1 = spex1[*, 1]
e_spex1 = spex1[*, 2]
ytit1 = sxpar(head1, 'YTITLE')

filebreak, sname2, name = name, ext = ext
if (ext eq 'fits') then begin
    spex2 = readfits(sname2, head2)
    wl_spex2 = spex2[*, 0]
    f_spex2 = spex2[*, 1]
    e_spex2 = spex2[*, 2]
    ytit2 = sxpar(head2, 'YTITLE')
endif else $
  readcol2, sname2, wl_spex2, f_spex2

;----------------------------------------------------------------------
; scaling
;SCALING_FACTOR = 1./max(spex1[between(spex1[*, 0], 1.0, 2.4), 1])
SCALING_FACTOR = 1./max(spex1[between(spex1[*, 0], wlnorm[0], wlnorm[1]), 1])
;SCALING_FACTOR = 10.34    ; from Brendan, for absolute flux calibration based on J+H 2MASS photometry

f_spex1 = SCALING_FACTOR * f_spex1
e_spex1 = SCALING_FACTOR * e_spex1
f_spex2 = SCALING_FACTOR * f_spex2
e_spex2 = SCALING_FACTOR * e_spex2

;----------------------------------------------------------------------
; choose plotting range
xr = [0.8, 2.5]
if keyword_set(legend) then yr = [0, 1.3*max(f_spex1[between(wl_spex1, 1.0, 2.3)])] $
   else yr = [0, 1.2*max(f_spex1[between(wl_spex1, 1.0, 2.3)])]

yr = yr/scl_spex

;!x.margin = [10, 30]

um = textoidl('\mu')+'m'

device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/specplot1'
    ps_open, outfile, /color, /en, thick=4;, /portrait
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=600
;    window, win, retain=2, xsize=600, ysize=800
endelse

;----------------------------------------------------------------------
; (1) plot first object
;----------------------------------------------------------------------
; smooth the data
;f_spex1_smooth = smooth(f_spex1, 8, /nan)

; make box
plot, [0], /nodata, col=0, backg=1, $
      xr=xr, /xs, yr=yr, /ys, $
      xtitle='wavelength ('+um+')', xchars=1.5, xmar=[11,3], $
      ;ytitle=textoidl('F_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
      ytitle=textoidl('!8f!3_\lambda (normalized)'), ychars=1.5, ymar=[5.5,2], $
      ;; ;ytitle=textoidl('!8f!3_\lambda (10!U'+strc(fix(alog10(scl_spex)))+'!N W/m^2/')+um+')', $
      ;; chars=1.5, $
;      pos = [0.1, 0.3, 0.9, 0.7]
      _extra=extrakey

f_spex1 = f_spex1/scl_spex
e_spex1 = e_spex1/scl_spex
thick1 = 6
; remove high-noise sections
if n_elements(wl_spex1) gt 600 then begin
    thick1 = 3
    rem_ind = where(((wl_spex1 gt 1.35) and (wl_spex1 lt 1.41)) or ((wl_spex1 gt 1.85) and (wl_spex1 lt 1.91)))
    if n_elements(f_spex1_smooth) gt 0 then begin
        remove, rem_ind, wl_spex1, f_spex1, f_spex1_smooth, e_spex1
        f_spex1_smooth[max(where(wl_spex1 le 1.35))] = !Values.F_NAN
        f_spex1_smooth[max(where(wl_spex1 le 1.85))] = !Values.F_NAN
    endif else begin
        remove, rem_ind, wl_spex1, f_spex1
    ; prevent IDL from connecting points across the removed section
        f_spex1[max(where(wl_spex1 le 1.35))] = !Values.F_NAN
        f_spex1[max(where(wl_spex1 le 1.85))] = !Values.F_NAN
    endelse
endif
; plot object 1 data
if n_elements(f_spex1_smooth) gt 0 then begin
    f_spex1_smooth = f_spex1_smooth/scl_spex
    oplot, wl_spex1, f_spex1_smooth, col=0, thick=thick1
    ;oploterror, wl_spex1, f_spex1_smooth, e_spex1, col=0, thick=thick1, errcol=12, errthick=1,/nohat
endif else oplot, wl_spex1, f_spex1, col=0, thick=thick1


;----------------------------------------------------------------------
; (2) overplot second object
;----------------------------------------------------------------------
; smooth the data
;f_spex2 = smooth(f_spex2, 1, /nan)

; rescale to match science object
w1 = between(wl_spex1, wlnorm[0], wlnorm[1], nw)
scl1 = avg(f_spex1[w1])
w2 = between(wl_spex2, wlnorm[0], wlnorm[1], nw)
scl2 = avg(f_spex2[w2])
f_spex2 = f_spex2/scl2*scl1
e_spex2 = e_spex2/scl2*scl1
 
; plot object 2 data
if n_elements(f_spex2_smooth) gt 0 then begin
    f_spex2_smooth = f_spex2_smooth/scl2*scl1
    oplot, wl_spex2, f_spex2_smooth, col=3, thick=6
    ;oploterror, wl_spex2, f_spex2_smooth, e_spex2, col=3, thick=6, errcol=3, errthick=1, /nohat
endif else oplot, wl_spex2, f_spex2, col=3, thick=6
;endif else oploterror, wl_spex2, f_spex2, e_spex2, col=3, thick=6, errcol=3, errthick=1, /nohat


;----------------------------------------
; label spectral features
;----------------------------------------
if keyword_set(labels) then begin
ct = !p.thick < 3
cs = 0.8
cc = 0
htwoo = 'H!D2!NO'
meth = ' CH!D4!N'
dy = 0.025*(!y.crange(1)-!y.crange(0))
wlmin = !x.crange(0)+0.03
wlmax = !x.crange(1)-0.03
; J-band labels
yp_j = max(f_spex1(between(wl_spex1, 1.2, 1.3)))+0.1*(!y.crange(1)-!y.crange(0))
ybar_j = [yp_j-dy, yp_j, yp_j, yp_j-dy]
plots, [0.9, 0.9, 1.6, 1.6] > wlmin, ybar_j-.01, thick=ct, color = cc
xyouts, 1.25, yp_j+dy-.01, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
plots, [1.12, 1.12, 1.2, 1.2], ybar_j-2.5*dy , thick=ct, color = cc
xyouts, 1.157, yp_j-1.8*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
plots, [1.3, 1.3, 1.33, 1.33], ybar_j-2.5*dy, thick=ct, color = cc
xyouts, 1.311, yp_j-1.8*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
; H-band labels
yp_h = max(f_spex1(between(wl_spex1, 1.4, 1.6)))+0.1
ybar_h = [yp_h-dy, yp_h, yp_h, yp_h-dy]
plots, [1.6, 1.6, 1.8, 1.8], ybar_h, thick=ct, color = cc
xyouts, 1.7, yp_h+dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
; K-band labels
yp_k = max(f_spex1(between(wl_spex1, 1.9, 2.3)))+0.2
ybar_k = [yp_k-dy, yp_k, yp_k, yp_k-dy]
yp_hk = avg([yp_h, yp_k])
ybar_hk = [yp_hk-dy, yp_hk, yp_hk, yp_hk-dy]
plots, [1.7, 1.7, 2.1, 2.1], ybar_hk, thick=ct, color = cc
xyouts, 1.9, yp_hk+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
plots, [2.1, 2.1, 2.4, 2.4], ybar_k-.1, thick=ct, color = cc
xyouts, 2.25, yp_k+dy-.1, meth, align=0.5, chars=cs, charthick=ct, color = cc
plots, [2.3, 2.3, 2.5, 2.5] < wlmax, ybar_k/3., thick=ct, color = cc
xyouts, 2.39, yp_k/3.+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
plots, [2, 2, 2.5, 2.5] < wlmax, yp_h, thick=ct, color = cc
xyouts, 2.25, yp_h+dy, 'H!D2!N', align=0.5, chars=cs, charthick=ct, color = cc
; Y-band labels
yp_y =  max(f_spex1(between(wl_spex1, 0.7, 1.1)));+0.2
ybar_y = [yp_y-dy, yp_y, yp_y, yp_y-dy]
plots, [0.7, 0.7, 1.05, 1.05] > wlmin, ybar_y, thick=ct, color = cc
xyouts, (1.08+!x.crange(0))/2, yp_y+dy-.01, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
;xyouts, 0.98, yp_y+dy, 'K I', align=0.5, chars=cs, charthick=ct
endif


; CALCULATE RATIO

fratio = f_spex1 / f_spex2
oplot, wl_spex1, fratio, color=4, thick=3

;; good = where(finite(fratio))
;; c = poly_fit(wl_spex1[good], fratio[good], 3)     ; get the coefficients of a 3rd order polynomial
;; ratfit = poly(wl_spex1[good], c)                        ; plug the SpT vector into the polynomial
;; oplot, wl_spex1[good], ratfit, color=2, thick=2
fratio_smooth = smooth(fratio, 50, /nan)
oplot, wl_spex1, fratio_smooth, color=2, thick=6

; LEGEND
if keyword_set(flipleg) then begin
    leg = [objname2, objname1]
    col = [3, 0]
    tcol = [3, 0]
endif else begin
    leg = [objname1, objname2]
    col = [0, 3]
    tcol = [0, 3]
endelse
legend, leg, $
;legend, [objname+' ('+sptypenum(spt,/rev)+')', specname+' ('+specspt+')'], $
        color=col, $
        ;line = intarr(2), $
        /right, box=0, $
;        pos = [1.5, 1.09], box=0, $
        textcol=tcol, $
        chars=1.2


if keyword_set(ps) then ps_close

END
