ps=1
win=1
names=['PSO J007.7+57 (L9)', 'PSO J282.7+59 (L9)', 'PSO J140.2+45 (L9.5)', 'PSO J103.0+41 (T0)', 'PSO J272.4-04 (T1)', 'PSO J307.6+07 (T1.5)', 'PSO J339.0+51 (T5)']
;data = ['PW-1-013722_0.8prism_2012sep24.fits', 'PW-10-002760_0.8prism_2012sep26.fits', 'PW-5-011389_0.8prism_2012nov08.fits', 'PW-4-005467_0.8prism_2012sep26.fits']
;names = ['PSO J007.7+57 (L9)', 'PSO J282.7+59 (L9)', 'PSO J140.2+45 (L9.5)', 'PSO J103.0+41 (T0)']

;data = ['PW-10-004338_0.8prism_2012oct14.fits', 'PW-11-022304_0.8prism_2012sep20.fits', 'PW-12-014644_0.8prism_2012oct07.fits']
;names = ['PSO J272.4-04 (T1)', 'PSO J307.6+07 (T1.5)', 'PSO J339.0+51 (T5)']

;PRO SPEXSTACK, DATA, DATADIR, NAMES=names, OUTFILE=outfile, PS=ps, WIN=win

;+
;  Plot a vertical stack of SpeX spectra.
;
;  HISTORY
;  Developed by Mike Liu; hacked several times for different projects
;  Hacked into this form by Will Best (IfA), 02/10/2013
;
;  INPUTS
;      DATA - Array of SpeX data fits files
;      DATADIR - Path where SpeX data files are located
;
;  KEYWORDS
;      NAMES - Names for the data spectra, to be displayed on the graph
;      OUTFILE - path+name for postcript output
;      PS - postscript output
;      WIN - specify window number for screen display
;
; The reduced SpeX fits files are 3-row arrays.
; row 0 is the wavelength solution (microns)
; row 1 is the reduced flux (ergs s^-1 cm^-2 ang^-1)
; row 2 is the error on the flux
;-

; Where are the data spectra?
if n_elements(datadir) eq 0 then datadir = '~/Dropbox/panstarrs-BD/SPECTRA/'
if n_elements(data) eq 0 then begin
    data = ['PW-1-013722_0.8prism_2012sep24.fits', 'PW-10-002760_0.8prism_2012sep26.fits', 'PW-5-011389_0.8prism_2012nov08.fits', 'PW-4-005467_0.8prism_2012sep26.fits', 'PW-10-004338_0.8prism_2012oct14.fits', 'PW-11-022304_0.8prism_2012sep20.fits', 'PW-12-014644_0.8prism_2012oct07.fits']
endif
data = datadir + data
nspec = n_elements(data)

; What are the data spectra called?
if n_elements(names) eq 0 then names=['PSO J007.7+57 (L9)', 'PSO J282.7+59 (L9)', 'WISE 0920+4538 (L9.5)', 'PSO J103.0+41 (T0)', 'PSO J272.4-04 (T1)', 'PSO J307.6+07 (T1.5)', 'PSO J339.0+51 (T5)']


; Load a color table
device, decomposed=0
lincolr_wb, /silent
!p.color = 0
um='!9'+STRING(109B)+'!Xm'
;um = textoidl('\mu')+'m'

; Plotting variables
scl_spex = 1.
wlnorm = [1.27, 1.28]   ; J-band peak
;wlnorm = [1.575, 1.585] ; H-band peak
;wlnorm = [2.07, 2.09]   ; K-band peak
xr = [0.8, 2.5]
yr = [0, 1.4+nspec-1]
pcolor = [0, 3, 4, 2]

; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/specplot1'
    ps_open, outfile, /color, /en, thick=4, /portrait, /ps
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=600, ysize=800
endelse

; Set up the axes
plot, [0], /nodata, col=0, backg=1, xrange=xr, /xstyle, yrange=yr, /ystyle, $
      xtitle='wavelength ('+um+')', xchars=1.5, xmar=[9,3], $
      ytitle=textoidl('!8f!3_\lambda (normalized) + constant'), ychars=1.5, ymar=[5.5,2], $
      _extra=extrakey


;;; PLOT THE SPECTRA
for i=0, nspec-1 do begin

; get IRTF/Spex data
    spex = readfits(data[i], head)
    wl_spex = spex[*, 0]
    f_spex = spex[*, 1]
;    ytit = sxpar(head, 'YTITLE')

; choose scaling
    SCALING_FACTOR = 1./max(spex[between(spex[*,0], 1.0, 2.4), 1])
    f_spex = SCALING_FACTOR * f_spex
    f_spex = f_spex/scl_spex

; rescale subsequent objects to match first object
    if i eq 0 then begin
        w1 = between(wl_spex, wlnorm[0], wlnorm[1], nw)
        scl1 = avg(f_spex[w1])
    endif else begin
        w2 = between(wl_spex, wlnorm[0], wlnorm[1], nw)
        scl2 = avg(f_spex[w2])
        f_spex = f_spex/scl2*scl1
    endelse

; Plot the data
    oplot, wl_spex, f_spex+(nspec-i-1), col=0, thick=4

; Label the spectrum
    ;laby = 0.7+(nspeci-i-1)
    laby = max(f_spex[between(wl_spex, 2.0, 2.3, mf)]) + 0.15 + (nspec-i-1)
;    xyouts, 1.96, laby, names[i], chars=1, /data
    xyouts, 1.92, laby, names[i], chars=1, /data

endfor

;xyouts, .03, .5, textoidl('!8f!3_\lambda (normalized) + constant'), chars=1.5, align=0.5, orient=90, /normal

if keyword_set(ps) then ps_close

END
