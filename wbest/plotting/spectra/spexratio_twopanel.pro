;flipleg=1
;labels=1
ps=1
smooth=1
;uncertainties=1

;PRO SPEXRATIO_TWOPANEL, sname1, smame2, objname, OUTFILE=outfile, ps=ps, WIN=win, _extra=extrakey

;  overlay SpeX spectra.
;
;  HISTORY
;  Spliced together by Will Best (IfA), 07/21/2013
; 

yrange1 = [0.5, 1.5]
yrange2 = [0.5, 1.5]

spexpath = '~/Dropbox/panstarrs-BD/SPECTRA/'
obj1name = strarr(3)
obj2name = strarr(5)

s1name0 = spexpath+'2MASS2041+0014_0.8prism_2013apr03_1.fits'
obj1name[0] = '2MASS J2041+0014 (corrected) prism  2013 Apr 03 UT'
s1name1 = spexpath+'2MASS2041+0014_0.8prism_2013apr04_1.fits'
obj1name[1] = '2MASS J2041+0014 prism  2013 Apr 04 UT'
s1name2 = spexpath+'2MASS2041+0014_0.8prism_2013apr05_2.fits'
obj1name[2] = '2MASS J2041+0014 prism  2013 Apr 05 UT'

;; s2name0 = spexpath+'PW-11-022304_0.8SXD_2012sep26_merged.fits'
;; obj2name[0] = 'PSO J307.6+07 SXD  2012 Sep 26 UT'
s2name1 = spexpath+'PW-11-022304_0.8prism_2012sep20.fits'
obj2name[1] = 'PSO J307.6+07 prism  2012 Sep 20 UT'
s2name2 = spexpath+'PW-11-022304_0.8prism_2013apr03_1.fits'
obj2name[2] = 'PSO J307.6+07 (corrected) prism  2013 Apr 03 UT'
s2name3 = spexpath+'PW-11-022304_0.8prism_2013apr04_1.fits'
obj2name[3] = 'PSO J307.6+07 prism  2013 Apr 04 UT'
s2name4 = spexpath+'PW-11-022304_0.8prism_2013apr05_2.fits'
obj2name[4] = 'PSO J307.6+07 prism  2013 Apr 05 UT'

col = [0, 3, 4, 2, 12]
;col = [3, 4, 2, 12]

; wl region for normalization
;wlnorm = [1.2, 1.3]
;wlnorm = [1.15, 1.35]
;wlnorm = [1.5, 1.6]
;wlnorm = [1.26, 1.27]   ; J-band peak
;wlnorm = [1.27, 1.28]   ; J-band peak
;wlnorm = [1.575, 1.585] ; H-band peak
wlnorm = [1.575, 1.695] ; H-band
;wlnorm = [2.07, 2.09]   ; K-band peak


; get IRTF/Spex data
spex10 = readfits(s1name0, head10)
wl_spex10 = spex10[*, 0]
f_spex10 = spex10[*, 1]
e_spex10 = spex10[*, 2]
ytit10 = sxpar(head10, 'YTITLE')

spex11 = readfits(s1name1, head11)
wl_spex11 = spex11[*, 0]
f_spex11 = spex11[*, 1]
e_spex11 = spex11[*, 2]
ytit11 = sxpar(head11, 'YTITLE')

spex12 = readfits(s1name2, head12)
wl_spex12 = spex12[*, 0]
f_spex12 = spex12[*, 1]
e_spex12 = spex12[*, 2]
ytit12 = sxpar(head12, 'YTITLE')


;; spex20 = readfits(s2name0, head20)
;; wl_spex20 = spex20[*, 0]
;; f_spex20 = spex20[*, 1]
;; e_spex20 = spex20[*, 2]
;; ytit20 = sxpar(head20, 'YTITLE')

spex21 = readfits(s2name1, head21)
wl_spex21 = spex21[*, 0]
f_spex21 = spex21[*, 1]
e_spex21 = spex21[*, 2]
ytit21 = sxpar(head21, 'YTITLE')

spex22 = readfits(s2name2, head22)
wl_spex22 = spex22[*, 0]
f_spex22 = spex22[*, 1]
e_spex22 = spex22[*, 2]
ytit22 = sxpar(head22, 'YTITLE')

spex23 = readfits(s2name3, head23)
wl_spex23 = spex23[*, 0]
f_spex23 = spex23[*, 1]
e_spex23 = spex23[*, 2]
ytit23 = sxpar(head23, 'YTITLE')

spex24 = readfits(s2name4, head24)
wl_spex24 = spex24[*, 0]
f_spex24 = spex24[*, 1]
e_spex24 = spex24[*, 2]
ytit24 = sxpar(head24, 'YTITLE')


;----------------------------------------------------------------------
; scaling
;SCALING_FACTOR = 1./max(spex1[between(spex1[*, 0], 1.0, 2.4), 1])
SCALING_FACTOR = 1./max(spex21[between(spex21[*, 0], wlnorm[0], wlnorm[1]), 1])
;SCALING_FACTOR = 10.34    ; from Brendan, for absolute flux calibration based on J+H 2MASS photometry

f_spex10 = SCALING_FACTOR * f_spex10
e_spex10 = SCALING_FACTOR * e_spex10
f_spex11 = SCALING_FACTOR * f_spex11
e_spex11 = SCALING_FACTOR * e_spex11
f_spex12 = SCALING_FACTOR * f_spex12
e_spex12 = SCALING_FACTOR * e_spex12
;; f_spex20 = SCALING_FACTOR * f_spex20
;; e_spex20 = SCALING_FACTOR * e_spex20
f_spex21 = SCALING_FACTOR * f_spex21
e_spex21 = SCALING_FACTOR * e_spex21
f_spex22 = SCALING_FACTOR * f_spex22
e_spex22 = SCALING_FACTOR * e_spex22
f_spex23 = SCALING_FACTOR * f_spex23
e_spex23 = SCALING_FACTOR * e_spex23
f_spex24 = SCALING_FACTOR * f_spex24
e_spex24 = SCALING_FACTOR * e_spex24

w10 = between(wl_spex10, wlnorm[0], wlnorm[1], nw)
scl10 = avg(f_spex10[w10])
w11 = between(wl_spex11, wlnorm[0], wlnorm[1], nw)
scl11 = avg(f_spex11[w11])
w12 = between(wl_spex12, wlnorm[0], wlnorm[1], nw)
scl12 = avg(f_spex12[w12])
;; w20 = between(wl_spex20, wlnorm[0], wlnorm[1], nw)
;; scl20 = avg(f_spex20[w20])
w21 = between(wl_spex21, wlnorm[0], wlnorm[1], nw)
scl21 = avg(f_spex21[w21])
w22 = between(wl_spex22, wlnorm[0], wlnorm[1], nw)
scl22 = avg(f_spex22[w22])
w23 = between(wl_spex23, wlnorm[0], wlnorm[1], nw)
scl23 = avg(f_spex23[w23])
w24 = between(wl_spex24, wlnorm[0], wlnorm[1], nw)
scl24 = avg(f_spex24[w24])

f_spex10 = f_spex10/scl10*scl21
e_spex10 = e_spex10/scl10*scl21
f_spex11 = f_spex11/scl11*scl21
e_spex11 = e_spex11/scl11*scl21
f_spex12 = f_spex12/scl12*scl21
e_spex12 = e_spex12/scl12*scl21
;; f_spex20 = f_spex20/scl20*scl21
;; e_spex20 = e_spex20/scl20*scl21
f_spex22 = f_spex22/scl22*scl21
e_spex22 = e_spex22/scl22*scl21
f_spex23 = f_spex23/scl23*scl21
e_spex23 = e_spex23/scl23*scl21
f_spex24 = f_spex24/scl24*scl21
e_spex24 = e_spex24/scl24*scl21


; CALCULATE CORRECTION RATIO
fcorr = f_spex10 / f_spex11
fcorr_smooth = smooth(fcorr, 50, /nan)


;----------------------------------------------------------------------
; choose plotting range
xr = [0.8, 2.5]
if keyword_set(legend) then yr = [0, 1.3*max(f_spex21[between(wl_spex21, 1.0, 2.3)])] $
   else yr = [0, 1.2*max(f_spex21[between(wl_spex21, 1.0, 2.3)])]

um='!9'+STRING(109B)+'!Xm'
;um = textoidl('\mu')+'m'

device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/specplot1'
    ps_open_size, outfile, xfix=10., yfix=11., /color, /en, thick=4, /ps
    ;ps_open, outfile, /color, /en, thick=4, /ps;, /portrait
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=800
endelse

; plot the wavelength solutions
;; plot, [0], color=0, backg=1, xr=[180,200], yr=[1.2,1.33], /ys, xtit='Pixel', xchars=1.5, $
;;       ytit='wavelength ('+um+')', ychars=1.5, xmar=[12,3], ymar=[5,2]
;; ;oplot, wl_spex20, color=col[0]
;; oplot, wl_spex21, color=col[1]
;; oplot, wl_spex22, color=col[2]
;; oplot, wl_spex23, color=col[3]
;; oplot, wl_spex24, color=col[4]

;; legend, obj2name, color=col, box=0, textcol=col, chars=1.2

;; if keyword_set(ps) then ps_close
;; STOP

!p.multi = [0, 1, 2, 0, 0]

;;; PLOT #1
;----------------------------------------------------------------------
; (1) plot first object
;----------------------------------------------------------------------
; make box
plot, [0], /nodata, col=0, backg=1, $
      xr=xr, /xs, yr=yrange1, /ys, $
      xmar=[11,3], xtickn=replicate(' ',8), $;xtitle='wavelength ('+um+')', xchars=1.5, $
      ytitle=textoidl('!8f!3_\lambda (normalized)'), ychars=1.5, ymar=[2,2], $
      _extra=extrakey
thick1 = 6

; grey regions indicating low telluric atmosphere transmission
polyfill, [0.92, 0.95, 0.95, 0.92], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15
polyfill, [1.11, 1.16, 1.16, 1.11], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15
polyfill, [1.35, 1.48, 1.48, 1.35], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15
polyfill, [1.80, 2.05, 2.05, 1.80], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15

stand1 = f_spex11

; plot object 1 data
f_spex10 = f_spex10 / stand1 / fcorr_smooth
if keyword_set(smooth) then f_spex10 = smooth(f_spex10, 7, /nan)
if keyword_set(uncertainties) then $
  oploterror, wl_spex10, f_spex10, e_spex10, col=col[2], thick=thick1, errcol=col[2], errthick=1, /nohat $
    else oplot, wl_spex10, f_spex10, col=col[2], thick=thick1

;----------------------------------------------------------------------
; (2) overplot second object
;----------------------------------------------------------------------
; plot object 2 data
f_spex11 = f_spex11 / stand1
;if keyword_set(smooth) then f_spex11 = smooth(f_spex11, 7, /nan)
if keyword_set(uncertainties) then $
  oploterror, wl_spex11, f_spex11, e_spex11, col=col[3], thick=thick1, errcol=col[3], errthick=1, /nohat $
    else oplot, wl_spex11, f_spex11, col=col[3], thick=thick1

;----------------------------------------------------------------------
; (3) overplot third object
;----------------------------------------------------------------------
; plot object 3 data
f_spex12 = f_spex12 / stand1
if keyword_set(smooth) then f_spex12 = smooth(f_spex12, 7, /nan)
if keyword_set(uncertainties) then $
  oploterror, wl_spex12, f_spex12, e_spex12, col=col[4], thick=thick1, errcol=col[4], errthick=1, /nohat $
    else oplot, wl_spex12, f_spex12, col=col[4], thick=thick1

; re-draw the axes so the tick marks are visible over the plots
axis, col=0, xr=xr, /xs, xtickn=replicate(' ',8)
axis, /xaxis, col=0, xr=xr, /xs, xtickn=replicate(' ',8)
axis, yaxis=0, col=0, yr=yrange1, /ys, ytickn=replicate(' ',8)
axis, /yaxis, col=0, yr=yrange1, /ys, ytickn=replicate(' ',8)


;;; LEGEND #1
leg1 = obj1name
col1 = col[2:*]
tcol1 = col1
legend, leg1, color=col1, /right, box=0, textcol=tcol1, chars=1.2


;;; PLOT #2
;----------------------------------------------------------------------
; (1) plot first object
;----------------------------------------------------------------------
; make box
plot, [0], /nodata, col=0, backg=1, $
      xr=xr, /xs, yr=yrange2, /ys, $
      xtitle='wavelength ('+um+')', xchars=1.5, xmar=[11,3], $
      ytitle=textoidl('!8f!3_\lambda (normalized)'), ychars=1.5, ymar=[6,-2], $
      _extra=extrakey
thick2 = 6

; grey regions indicating low telluric atmosphere transmission
polyfill, [0.92, 0.95, 0.95, 0.92], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15
polyfill, [1.11, 1.16, 1.16, 1.11], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15
polyfill, [1.35, 1.48, 1.48, 1.35], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15
polyfill, [1.80, 2.05, 2.05, 1.80], [yrange2[0], yrange2[0], yrange2[1], yrange2[1]], /data, color=15

stand2 = f_spex23

;; ; smooth the data
;; f_spex20_smooth = smooth(f_spex20, 8, /nan)

;; ; remove high-noise sections
;; if n_elements(wl_spex20) gt 600 then begin
;;     thick1 = 3
;;     rem_ind = where(((wl_spex20 gt 1.35) and (wl_spex20 lt 1.41)) or ((wl_spex20 gt 1.85) and (wl_spex20 lt 1.91)))
;;     if n_elements(f_spex20_smooth) gt 0 then begin
;;         remove, rem_ind, wl_spex20, f_spex20, f_spex20_smooth, e_spex20
;;         ; prevent IDL from connecting points across the removed section
;;         f_spex20_smooth[max(where(wl_spex20 le 1.35))] = !Values.F_NAN
;;         f_spex20_smooth[max(where(wl_spex20 le 1.85))] = !Values.F_NAN
;;     endif else begin
;;         remove, rem_ind, wl_spex20, f_spex20, e_spex20
;;         ; prevent IDL from connecting points across the removed section
;;         f_spex20[max(where(wl_spex20 le 1.35))] = !Values.F_NAN
;;         f_spex20[max(where(wl_spex20 le 1.85))] = !Values.F_NAN
;;     endelse
;; endif

;; ; plot object 1 data
;; f_spex20 = f_spex20 / f_spex23
;; if keyword_set(smooth) then f_spex20 = smooth(f_spex20, 7, /nan)
;; if keyword_set(uncertainties) then begin
;;     if n_elements(f_spex1_smooth) gt 0 then $
;;       oploterror, wl_spex20, f_spex20_smooth, e_spex20, col=col[0], thick=thick2, errcol=col[0], errthick=thick2, /nohat $
;;         else oploterror, wl_spex20, f_spex20, e_spex20, col=col[0], thick=thick2, errcol=col[0], errthick=thick2, /nohat
;; endif else begin
;;     if n_elements(f_spex1_smooth) gt 0 then $
;;       oplot, wl_spex20, f_spex20_smooth, col=col[0], thick=thick2 $
;;         else oplot, wl_spex20, f_spex20, col=col[0], thick=thick2

;----------------------------------------------------------------------
; (2) overplot second object
;----------------------------------------------------------------------
; plot object 2 data
f_spex21 = shift(f_spex21, 1)
f_spex21 = f_spex21 / stand2
if keyword_set(smooth) then f_spex21 = smooth(f_spex21, 7, /nan)
if keyword_set(uncertainties) then $
  oploterror, wl_spex21, f_spex21, e_spex21, col=col[1], thick=thick2, errcol=col[1], errthick=1, /nohat $
    else oplot, wl_spex21, f_spex21, col=col[1], thick=thick2

;----------------------------------------------------------------------
; (3) overplot third object
;----------------------------------------------------------------------
; plot object 3 data
f_spex22 = shift(f_spex22, 1)
f_spex22 = f_spex22 / stand2 / fcorr_smooth
if keyword_set(smooth) then f_spex22 = smooth(f_spex22, 7, /nan)
if keyword_set(uncertainties) then $
  oploterror, wl_spex22, f_spex22, e_spex22, col=col[2], thick=thick2, errcol=col[2], errthick=1, /nohat $
    else oplot, wl_spex22, f_spex22, col=col[2], thick=thick2

;----------------------------------------------------------------------
; (4) overplot fourth object
;----------------------------------------------------------------------
; plot object 4 data
f_spex23 = f_spex23 / stand2
;if keyword_set(smooth) then f_spex23 = smooth(f_spex23, 7, /nan)
if keyword_set(uncertainties) then $
  oploterror, wl_spex23, f_spex23, e_spex23, col=col[3], thick=thick2, errcol=col[3], errthick=1, /nohat $
    else oplot, wl_spex23, f_spex23, col=col[3], thick=thick2

;----------------------------------------------------------------------
; (5) overplot fifth object
;----------------------------------------------------------------------
; plot object 5 data
f_spex24 = f_spex24 / stand2
if keyword_set(smooth) then f_spex24 = smooth(f_spex24, 7, /nan)
if keyword_set(uncertainties) then $
  oploterror, wl_spex24, f_spex24, e_spex24, col=col[4], thick=thick2, errcol=col[4], errthick=1, /nohat $
    else oplot, wl_spex24, f_spex24, col=col[4], thick=thick2

; re-draw the axes so the tick marks are visible over the plots
axis, col=0, xr=xr, /xs, xtickn=replicate(' ',8)
axis, /xaxis, col=0, xr=xr, /xs, xtickn=replicate(' ',8)
axis, yaxis=0, col=0, yr=yrange2, /ys, ytickn=replicate(' ',8)
axis, /yaxis, col=0, yr=yrange2, /ys, ytickn=replicate(' ',8)


;----------------------------------------
; label spectral features
;----------------------------------------
if keyword_set(labels) then begin
ct = !p.thick < 3
cs = 0.8
cc = 0
htwoo = 'H!D2!NO'
meth = ' CH!D4!N'
dy = 0.025*(!y.crange(1)-!y.crange(0))
wlmin = !x.crange(0)+0.03
wlmax = !x.crange(1)-0.03
; J-band labels
yp_j = max(f_spex1(between(wl_spex1, 1.2, 1.3)))+0.1*(!y.crange(1)-!y.crange(0))
ybar_j = [yp_j-dy, yp_j, yp_j, yp_j-dy]
plots, [0.9, 0.9, 1.6, 1.6] > wlmin, ybar_j-.01, thick=ct, color = cc
xyouts, 1.25, yp_j+dy-.01, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
plots, [1.12, 1.12, 1.2, 1.2], ybar_j-2.5*dy , thick=ct, color = cc
xyouts, 1.157, yp_j-1.8*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
plots, [1.3, 1.3, 1.33, 1.33], ybar_j-2.5*dy, thick=ct, color = cc
xyouts, 1.311, yp_j-1.8*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
; H-band labels
yp_h = max(f_spex1(between(wl_spex1, 1.4, 1.6)))+0.1
ybar_h = [yp_h-dy, yp_h, yp_h, yp_h-dy]
plots, [1.6, 1.6, 1.8, 1.8], ybar_h, thick=ct, color = cc
xyouts, 1.7, yp_h+dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
; K-band labels
yp_k = max(f_spex1(between(wl_spex1, 1.9, 2.3)))+0.2
ybar_k = [yp_k-dy, yp_k, yp_k, yp_k-dy]
yp_hk = avg([yp_h, yp_k])
ybar_hk = [yp_hk-dy, yp_hk, yp_hk, yp_hk-dy]
plots, [1.7, 1.7, 2.1, 2.1], ybar_hk, thick=ct, color = cc
xyouts, 1.9, yp_hk+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
plots, [2.1, 2.1, 2.4, 2.4], ybar_k-.1, thick=ct, color = cc
xyouts, 2.25, yp_k+dy-.1, meth, align=0.5, chars=cs, charthick=ct, color = cc
plots, [2.3, 2.3, 2.5, 2.5] < wlmax, ybar_k/3., thick=ct, color = cc
xyouts, 2.39, yp_k/3.+dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
plots, [2, 2, 2.5, 2.5] < wlmax, yp_h, thick=ct, color = cc
xyouts, 2.25, yp_h+dy, 'H!D2!N', align=0.5, chars=cs, charthick=ct, color = cc
; Y-band labels
yp_y =  max(f_spex1(between(wl_spex1, 0.7, 1.1)));+0.2
ybar_y = [yp_y-dy, yp_y, yp_y, yp_y-dy]
plots, [0.7, 0.7, 1.05, 1.05] > wlmin, ybar_y, thick=ct, color = cc
xyouts, (1.08+!x.crange(0))/2, yp_y+dy-.01, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
;xyouts, 0.98, yp_y+dy, 'K I', align=0.5, chars=cs, charthick=ct
endif


; LEGEND
if keyword_set(flipleg) then begin
    leg2 = [obj2name[1], obj2name[0], obj2name[2:*]]
    col2 = [col[1], col[0], col[2:*]]
    tcol2 = col2
endif else begin
    ;; leg2 = obj2name
    ;; col2 = col
    leg2 = obj2name[1:*]
    col2 = col[1:*]
    tcol2 = col2
endelse
legend, leg2, color=col2, /right, box=0, textcol=tcol2, chars=1.2


if keyword_set(ps) then ps_close

END
