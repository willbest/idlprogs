pro compare_spectra, spec1, spec2, $
                     wlnorm=wlnorm0, jhk=jhk, $
                     tit=tit, $
                     label1=label1, label2=label2, $
                     nohighlight=nohighlight, $
                     _extra=_extra

;+
; given two spectra, overplot them to compare.
; spectra can be passed in two ways through 'spec1' and 'spec2'
;   - 2-d arrays of {wl,f}
;   - names of spectra files, either FITS or text
;
; 10/19/06
;-


if not(keyword_set(wlnorm0)) then wlnorm = [0, 0] $
  else wlnorm = wlnorm0
if keyword_set(jhk) then !p.multi = [0, 1, 3] $
  else !p.multi = [0, 1, 1]
if not(keyword_set(tit)) then tit = ''

if n_params() ne 2 then begin
   print, 'pro compare_spectra, spec1, spec2,'
   print, '                     [wlnorm=], [jhk], [tit=], '+ $
          '[label1=], [label2=], [nohighlight], [_extra=]'
   return
endif

; load the spectra
t1 = size(spec1, /tname)
t2 = size(spec2, /tname)
for i=0, 1 do begin
    if (i eq 0) then begin
        tt = t1 
        spec = spec1
    endif else begin
        tt = t2
        spec = spec2
    endelse

    ; input is an array
    if (tt eq 'FLOAT') or (tt eq 'DOUBLE') then begin
        wl = spec(*, 0)
        flux = spec(*, 1)
        lab = 'spec'+strc(i+1)

    ; input is a file name, either FITS or text
    endif else if (tt eq 'STRING') then begin
        spawn, 'file '+spec, out
        if strpos(out, 'FITS') ne -1 then begin
            print, 'loading FITS file "', spec, '"'
            im = readfits(spec, /silent)
            wl = im(*, 0)
            flux = im(*, 1)
        endif else begin
            print, 'loading text file "', spec, '"'
            readcol2, spec, wl, flux
        endelse
        filebreak, spec, name=name
        lab = name
    endif else $
      message, 'invalid input type for spectrum!'

    if (i eq 0) then begin
        wl1 = wl
        flux1 = flux
        if keyword_set(label1) then lab1 = label1 $
        else lab1 = lab
    endif else begin
        wl2 = wl
        flux2 = flux
        if keyword_set(label2) then lab2 = label2 $
        else lab2 = lab
    endelse

endfor


if keyword_set(jhk) then begin
    nspec = 3 
    wlnorm = [[1.15, 1.35], [1.45, 1.8], [1.95, 2.4]]
    ;wlnorm = [[1.17, 1.33], [1.49, 1.78], [2.03, 2.37]]
endif else $
  nspec = 1
!p.multi = [0, 1, nspec]
lincolr, /silent
for i=0, nspec-1 do begin

    ; compute normalization factors
    if (wlnorm(1, i) eq 0) then begin
        scl1 = max(flux1(where(finite(flux1) eq 1)))
        scl2 = max(flux2(where(finite(flux2) eq 1)))
    endif else begin
        scl1 = max(flux1(between(wl1, wlnorm(0, i), wlnorm(1, i))))
        scl2 = max(flux2(between(wl2, wlnorm(0, i), wlnorm(1, i))))
    endelse

    ; plot spectrum 1
    if (nspec eq 1) then $
      plot, wl1, flux1 > 0, /xs, $
            chars=2, xtit='wl', ytit='flux', tit=tit, ps=10, $
            _extra=_extra $
    else $
      mplot, wl1, flux1 > 0, /xs, chars=2, $
             xtitle='wl', ytitle='flux', mtitle=tit, ps=10, $
             _extra=_extra

    ; plot spectrum 2


    ; highlight overlap region where spectra are normalized
    if not(keyword_set(nohighlight)) then begin
        plots, wlnorm(0, i)+[0, 0], !y.crange, line=1
        plots, wlnorm(1, i)+[0, 0], !y.crange, line=1
    endif
    oplot, wl2, flux2/scl2*scl1, col=2, ps=10

    ;w = between(wl2, wlnorm(0, i), wlnorm(1, i), nw)
    ;;oplot, wl1, flux1, col=2, ps=10
    ;
    ;w = between(wl1, wlnorm(0, i), wlnorm(1, i), nw)
    ;if (nw gt 0) then $
    ;  ;oplot, wl1(w), (flux1)(w), col=7, ps=10
    ;  oplot, wl2(w), (flux2/scl2*scl1)(w), col=2, ps=10

    ; label
    legend, [lab1, lab2], $
            color=[!p.color, 2], $
            textcolor=[!p.color, 2], $            
            line=[0, 0], $
            /right, box=0, $
            chars=1.5-0.5*(!d.name eq 'PS')

endfor


datestamp, 'COMPARE_SPECTRA.PRO'
!p.multi = [0, 1, 1]

end
