spectrum = 'PW-11-022304_0.8SXD_2012sep26_merged.fits'
objname = 'PSO J307.6+07.8 SXD 26-Sep-12 (UT)'
compspec = 'T2_SDSSJ1254-0122.fits'

compare=1
labels=1
noi=1
ps=1
win=2
;PRO SXDPLOT, SPECTRUM, NOI=noi

;+
;  plot SpeX SXD spectrum, in five stacked windows on a single page
;  wrapper for spectraplot.pro
;
;  HISTORY
;  Developed by Mike Liu; hacked several times for different projects
;  Hacked into this form by Will Best (IfA), 01/21/2013
;
;  INPUTS
;      SPECTRUM - SXD spectrum to be plotted
;
;  KEYWORDS
;      NOI - Don't include the I-band (0.81 - 0.95 um)
;-

; The reduced fits files are 3-row arrays.
; row 0 is the wavelength solution (microns)
; row 1 is the reduced flux (ergs s^-1 cm^-2 ang^-1)
; row 2 is the error on the flux

; read in object

; Load a color table
device, decomposed=0
lincolr_wb, /silent
!p.color = 0
um='!9'+STRING(109B)+'!Xm'
;um = textoidl('\mu')+'m'

; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/SXDplot1'
    ps_open, outfile, /color, /en, /portrait, thick=3, /ps
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=600, ysize=800
endelse
if keyword_set(noi) then $
  !p.multi = [0, 1, 4, 0, 0] $
else $
  !p.multi = [0, 1, 5, 0, 0]

; get IRTF/Spex data
spexdir = '~/Dropbox/panstarrs-BD/SPECTRA/'
spex = readfits(spexdir+spectrum, header)
wl_spex = spex[*, 0]
f_spex = spex[*, 1]

; get comparison spectrum data
if keyword_set(compare) then begin
    compdir = '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-library/'
    comp = readfits(compdir+compspec, hc)
    wl_comp = comp[*, 0]
    f_comp = comp[*, 1]
endif

; I-band  0.81 - 0.95 (may not need this)
if not keyword_set(noi) then begin
; normalization  wlnorm = [0.94, 0.95]
SCALING_FACTOR = 1./max(spex[between(spex[*, 0], 0.94, 0.95), 1])
f_plot = SCALING_FACTOR * f_spex
; choose plotting range and scaling
xr = [0.81, 0.95]
yr = [0, 1.2*max(f_plot[between(wl_spex, xr[0], xr[1])], /nan)]
; plot
plot, wl_spex, f_plot, col=0, backg=1, xr=xr, /xs, yr=yr, /ys, $
      chars=1.8, yticki=0.5, yminor=5, _extra=extrakey, ymargin=[2,1];, xmargin=
legend, 'i band', textcol=0, box=0, pos=[0.924, yr[1]], chars=1.4
;legend, objname, col=0, textcol=0, box=0, chars=1.3, pos=[0.875, 1.4]
endif

; Y-band  0.91 - 1.11
; normalization
SCALING_FACTOR = 1./max(spex[between(spex[*, 0], 1.08, 1.09), 1])
f_plot = SCALING_FACTOR * f_spex
; choose plotting range and scaling
xr = [0.91, 1.11]
yr = [0, 1.2*max(f_plot[between(wl_spex, xr[0], xr[1])], /nan)]
; plot
;plot, wl_spex, f_plot, col=0, backg=1, xr=xr, /xs, yr=yr, /ys, $
;      chars=1.8, yticki=0.5, yminor=5, ymargin=[2.5, 0.5]
plot, [0], /nodata, col=0, backg=1, xr=xr, /xs, yr=yr, /ys, $
      chars=1.8, yticki=0.5, yminor=5, ymargin=[2.5, 0.5]
if keyword_set(compare) then begin
    comp_scale = 1./max(comp[between(comp[*, 0], 1.08, 1.09), 1])
    c_plot = comp_scale * f_comp
    oplot, wl_comp, c_plot, col=3
endif
oplot, wl_spex, f_plot, col=0
legend, 'Y band', textcol=0, box=0, pos=[1.071, yr[1]], chars=1.4
; labels
if keyword_set(labels) then begin
    ct = !p.thick < 3
    cs = 0.8
    cc = 0
    htwoo = 'H!D2!NO'
    meth = 'CH!D4!N'
    dy = 0.025*(!y.crange(1)-!y.crange(0))
    wlmin = !x.crange(0)+0.03
    wlmax = !x.crange(1)-0.03
    yp_z = yr[1]-0.1*(!y.crange(1)-!y.crange(0))
    ybar_z = [yp_z-dy, yp_z, yp_z, yp_z]
    plots, [0.91, 0.91, 1.07, 1.07], yp_z-1*dy , thick=ct, color = cc
    xyouts, 0.99, yp_z+0.3*dy, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
    plots, [0.99, 0.99, 1., 1.], ybar_z-8*dy, thick=ct, color = cc
    xyouts, 0.995, yp_z-6.7*dy, 'FeH', align=0.5, chars=cs, charthick=ct, color = cc
endif

; J-band  1.10 - 1.33
; normalization
SCALING_FACTOR = 1./max(spex[between(spex[*, 0], 1.26, 1.27), 1])
f_plot = SCALING_FACTOR * f_spex
; choose plotting range and scaling
xr = [1.10, 1.33]
yr = [0, 1.2*max(f_plot[between(wl_spex, xr[0], xr[1])], /nan)]
; plot
plot, [0], /nodata, backg=1, xr=xr, /xs, yr=yr, /ys, $
      chars=1.8, yticki=0.5, yminor=5, ymargin=[3,0]
if keyword_set(compare) then begin
    comp_scale = 1./max(comp[between(comp[*, 0], 1.26, 1.27), 1])
    c_plot = comp_scale * f_comp
    oplot, wl_comp, c_plot, col=3
endif
oplot, wl_spex, f_plot, col=0
legend, 'J band', textcol=0, box=0, pos=[1.285, yr[1]], chars=1.4
; labels
if keyword_set(labels) then begin
    yp_j = yr[1]-0.1*(!y.crange(1)-!y.crange(0))
    ybar_j = [yp_j-dy, yp_j, yp_j, yp_j-dy]
    plots, [1.105, 1.105, 1.23, 1.23], yp_j-1*dy , thick=ct, color = cc
    xyouts, 1.17, yp_j+0.3*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
    plots, [1.169, 1.169, 1.178, 1.178], ybar_j-9*dy , thick=ct, color = cc
    xyouts, 1.1735, yp_j-7.7*dy, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
    plots, [1.243, 1.243, 1.252, 1.252], ybar_j-1*dy , thick=ct, color = cc
    xyouts, 1.2475, yp_j+0.3*dy, 'K I', align=0.5, chars=cs, charthick=ct, color = cc
    plots, [1.305, 1.305, 1.33, 1.33], yp_j-20*dy, thick=ct, color = cc
    xyouts, 1.3175, yp_j-23*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
endif

; H-band  1.48 - 1.79
; normalization
SCALING_FACTOR = 1./max(spex[between(spex[*, 0], 1.575, 1.585), 1])
f_plot = SCALING_FACTOR * f_spex
; choose plotting range and scaling
xr = [1.48, 1.79]
yr = [0, 1.2*max(f_plot[between(wl_spex, xr[0], xr[1])], /nan)]
; plot
plot, [0], /nodata, backg=1, xr=xr, /xs, yr=yr, /ys, $
      chars=1.8, yticki=0.5, yminor=5, ymargin=[3.5,-0.5]
if keyword_set(compare) then begin
    comp_scale = 1./max(comp[between(comp[*, 0], 1.575, 1.585), 1])
    c_plot = comp_scale * f_comp
    oplot, wl_comp, c_plot, col=3
endif
oplot, wl_spex, f_plot, col=0
legend, 'H band', textcol=0, box=0, pos=[1.727, yr[1]], chars=1.4
; labels
if keyword_set(labels) then begin
    yp_h = yr[1]-0.1*(!y.crange(1)-!y.crange(0))
    ybar_h = [yp_h-dy, yp_h, yp_h, yp_h-dy]
    plots, [1.6, 1.69], yp_h-2*dy, thick=ct, color = cc
    plots, [1.69, 1.78], [yp_h-2*dy, yp_h-18*dy], thick=ct, color = cc
    xyouts, 1.69, yp_h-0.5*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
endif

; K-band  1.91 - 2.45
; normalization
SCALING_FACTOR = 1./max(spex[between(spex[*, 0], 2.07, 2.09), 1])
f_plot = SCALING_FACTOR * f_spex
; choose plotting range and scaling
xr = [1.91, 2.43]
yr = [0, 1.2*max(f_plot[between(wl_spex, xr[0], xr[1])], /nan)]
; plot
plot, [0], /nodata, col=0, backg=1, xr=xr, /xs, yr=yr, /ys, $
      chars=1.8, yticki=0.5, yminor=5, ymargin=[4, -1]
if keyword_set(compare) then begin
    comp_scale = 1./max(comp[between(comp[*, 0], 2.07, 2.09), 1])
    c_plot = comp_scale * f_comp
    oplot, wl_comp, c_plot, col=3
endif
oplot, wl_spex, f_plot, col=0
legend, 'K band', textcol=0, box=0, pos=[2.326, yr[1]], chars=1.4
; labels
if keyword_set(labels) then begin
    yp_k = yr[1]-0.1*(!y.crange(1)-!y.crange(0))
    ybar_k = [yp_k-dy, yp_k, yp_k, yp_k]
    plots, [xr[0], xr[0], 2.05, 2.05], yp_k-2*dy, thick=ct, color = cc
    xyouts, 1.97, yp_k-0.7*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
    plots, [2.16, 2.32], [yp_k-4*dy, yp_k-21*dy], thick=ct, color = cc
    plots, [2.32, xr[1]], yp_k-21*dy, thick=ct, color = cc
    xyouts, 2.32, yp_k-19*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
    plots, [2.20, 2.20, 2.22, 2.22], ybar_k-4*dy, thick=ct, color = cc
    xyouts, 2.21, yp_k-2.7*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
    plots, [2.3, 2.3, 2.5, 2.5] < xr[1], yp_k-7*dy, thick=ct, color = cc
    xyouts, 2.365, yp_k-5.7*dy, htwoo, align=0.5, chars=cs, charthick=ct, color = cc
    plots, [2.315, 2.315, 2.345, 2.345], ybar_k-13*dy, thick=ct, color = cc
    xyouts, 2.33, yp_k-11.6*dy, meth, align=0.5, chars=cs, charthick=ct, color = cc
endif

xyouts, .04, .5, textoidl('!8f!3_\lambda (normalized)'), chars=1.5, align=0.5, orient=90, /normal
xyouts, .5, .01, 'wavelength ('+um+')', chars=1.5, align=0.5, /normal

if keyword_set(ps) then ps_close
!p.multi = [0, 0, 1, 0, 0]

END
