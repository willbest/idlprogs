f1='z'
f2='W1'
f3='W1'
f4='W2'
nfilt=4
;minspt=7
;maxspt=22.5
;PRO COLORCHART2, F1, F2, F3, F4, MINSPT=minspt, MAXSPT=maxspt, OUTFILE=outfile, PS=ps, $
;                XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax

;  Same as colorchart.pro, but with plot colors trimmed to highlight L6-T3
;  dwarfs, for my first 699 project.
;  Using Kimberley Aller's table of known cool dwarfs, plots color-color
;  and color-magnitude diagrams of requested filter combinations.
;  Currently ignores subdwarfs.
;
;  HISTORY
;  Written by Will Best (IfA), 03/05/2012
;
;  INPUTS
;      F1, F2, [F3, F4] - Filters whose colors or magnitudes are to be plotted.
;         If four filters are entered, a plot of F3-F4 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If three filters are entered, a plot of F3 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If two filters are entered, a plot of F1-F2 (y-axis) vs. SpT (x-axis)
;         will be created.
;         If fewer than two filters are entered, the syntax and a list of
;         available filters are printed.
;
;  OPTIONAL KEYWORDS
;      MINSPT - Minimum spectral type to fit.
;          Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;                 (no data exists for types later than T9.)
;          Default: 0 (M0)
;      MAXSPT - Maximum spectral type to fit (same scale as MINSPT).
;          Default: 29 (T9).
;      OUTFILE - Path for encapsulated postscript output
;      PS - Output to postscript
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;
;  FUTURE IMPROVEMENTS
;      Default is to only fit dwarfs.
;          Add keyword to include subdwarfs.
;          Add keyword to only fit subdwarfs.
;      Add a /silent keyword.
;
;  Display colors:
;      Old scheme:
;      M0-M4.5: BRIGHT GREEN
;      M5-M9.5: ORANGE
;      L0-L5.5: RED
;      L6-L9.5: MAGENTA
;      T0-T3.5: BLUE
;      T4-T9.5: CYAN
;
;      New scheme:
;      M0-M4.5: MAGENTA
;      M5-M9.5: BLUE/CYAN
;      L0-L5.5: CYAN/DARK GREEN
;      L6-L9.5: BRIGHT GREEN
;      T0-T3.5: ORANGE
;      T4-T9.5: RED
cuts = [0, 5, 10, 16, 20, 24, 30]

; Create structure for filter names and polynomial coefficients
names = ['PS1 g', 'PS1 r', 'PS1 i', 'PS1 z', 'PS1 y', $
         '2MASS J', '2MASS H', '2MASS K', $
         'UKIDSS Y', 'UKIDSS J', 'UKIDSS H', 'UKIDSS K', $
         'WISE W1', 'WISE W2', 'WISE W3', 'WISE W4']
codes = ['g', 'r', 'i', 'z', 'y', $
         'J', 'H', 'K', $
         'UY', 'UJ', 'UH', 'UK', $
         'W1', 'W2', 'W3', 'W4']
; 37 G  38 GERR  41 R  42 RERR  45 I  46 IERR  49 Z  50 ZERR  53 Y  54 YERR 
; 1 JMAG  2 E_JMAG  3 HMAG  4 E_HMAG  5 KMAG  6 E_KMAG
; 26 Y_2  27 J  28 H  29 K  30 YERR_2  31 JERR  32 HERR  34 KERR
; 121 W1  122 W1ERR  125 W2  126 W2ERR  129 W3  130 W3ERR  133 W4  134 W4ERR
refs = [[37,38], [41,42], [45,46], [49,50], [53,54], $
        [1,2], [3,4], [5,6], $
        [26,30], [27,31], [28,32], [29,34], $
        [121,122], [125,126], [129,130], [133,134] ]
labels = ['g!IPS1!N', 'r!IPS1!N', 'i!IPS1!N', 'z!IPS1!N', 'y!IPS1!N', $
         'J!I2MASS!N', 'H!I2MASS!N', 'K!I2MASS!N', $
         'Y!IUKIDSS!N', 'J!IUKIDSS!N', 'H!IUKIDSS!N', 'K!IUKIDSS!N', $
         'W1', 'W2', 'W3', 'W4']

;; nfilt = n_params()
;; if nfilt lt 2 or nfilt gt 4 then begin
;;     print, 'SYNTAX: colorchart, f1, f2 [, f3, f4, MINSPT=minspt, MAXSPT=maxspt,'
;;     print, '                    XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]'
;;     print, 'Plots (1) f1-f2 vs. SpT  or  (2) f3 vs. f1-f2  or  (3) f3-f4 vs. f1-f2'
;;     print
;;     print, 'Filters available:'
;;     for i=0, n_elements(names)-1 do print, codes[i], ' = ', names[i]
;;     return
;; endif

; Display the earliest and latest spectral types to be plotted
if ((size(minspt, /type) ne 2) and (size(minspt, /type) ne 4)) then minspt = 0
if (minspt lt 0) then minspt = 0
if (minspt gt 29.5) then minspt = 29.5
if ((size(maxspt, /type) ne 2) and (size(maxspt, /type) ne 4)) then maxspt = 29.5
if (maxspt gt 29.5) then maxspt = 29.5
if (maxspt lt minspt) then maxspt = minspt

sptl = [replicate('M',10), replicate('L',10), replicate('T',10)]
if (maxspt eq minspt) then begin
    print, 'Only spectral type plotted will be '+sptl[fix(minspt)]+trim(minspt mod 10)
endif else begin
    print, 'Earliest spectral type plotted will be '+sptl[fix(minspt)]+trim(minspt mod 10)
    print, 'Latest spectral type plotted will be '+sptl[fix(maxspt)]+trim(maxspt mod 10)
endelse

; Load the data table into the structure "wise_new". 
; More info in Dropbox > panstarrs-BD > Known Objects > mlt_dwarfs > README
restore, '~/Desktop/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'

; Create array for photometry, colors, and errors
p = fltarr(13, n_elements(wise_new))
p[0,*] = wise_new.spt                         ; Spectral Type
switch nfilt of
    4 : begin
        n4 = where(f4 eq codes)
        p[9,*] = wise_new.(refs[0,n4])        ; f4
        p[10,*] = wise_new.(refs[1,n4])       ; f4err
    end
    3 : begin
        n3 = where(f3 eq codes)
        p[7,*] = wise_new.(refs[0,n3])        ; f3
        p[8,*] = wise_new.(refs[1,n3])        ; f3err
        if nfilt eq 4 then begin
            p[11,*] = p[7,*] - p[9,*]            ; f3-f4
            p[12,*] = sqrt(p[8,*]^2 + p[10,*]^2) ; f3-f4 err
        endif
    end
    else : begin
        n1 = where(f1 eq codes)
        p[1,*] = wise_new.(refs[0,n1])        ; f1
        p[2,*] = wise_new.(refs[1,n1])        ; f1err
        n2 = where(f2 eq codes)
        p[3,*] = wise_new.(refs[0,n2])        ; f2
        p[4,*] = wise_new.(refs[1,n2])        ; f2err
        p[5,*] = p[1,*] - p[3,*]              ; f1-f2
        p[6,*] = sqrt(p[2,*]^2 + p[4,*]^2)    ; f1-f2 err
    end
endswitch

; restrict spectral types as defined my minspt and maxspt
noneg = where((p[0,*] ge minspt) and (p[0,*] le maxspt))
p = p[*,noneg]

; remove unreasonable magnitudes and colors
switch nfilt of
    4 : begin
        sane4 = where(p[9,*] gt 0)
        p = p[*,sane4]
    end
    3 : begin
        sane3 = where(p[7,*] gt 0)
        p = p[*,sane3]
    end
    else : begin
        sane12 = where((p[1,*] gt 0) and (p[3,*] gt 0))
        p = p[*,sane12]
   end
endswitch

; Output this bad boy.
device, decomposed=0
lincolr, /silent
;  Display colors:
;      Old scheme:
;      M0-M4.5: BRIGHT GREEN
;      M5-M9.5: ORANGE
;      L0-L5.5: RED
;      L6-L9.5: MAGENTA
;      T0-T3.5: BLUE
;      T4-T9.5: CYAN
cset = [2, 8, 3, 5, 4, 6]
;      New scheme:
;      M0-M4.5: MAGENTA
;      M5-M9.5: BLUE/CYAN
;      L0-L5.5: CYAN/DARK GREEN
;      L6-L9.5: BRIGHT GREEN
;      T0-T3.5: ORANGE
;      T4-T9.5: RED
;cset = [5, 4, 6, 2, 8, 3]
usersym, [ 0, 1, 0, -1, 0 ], [ 1, 0, -1, 0, 1 ], /fill   ; filled diamond

if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/colorchart1'
    ps_open, fpath, /color, /en
endif else window, retain=2, xsize=800, ysize=600

case nfilt of
    2 : begin
        if not keyword_set(ymin) then ymin = min(p[5,*])-.5
        if not keyword_set(ymax) then ymax = max(p[5,*])+.5
        xmin = minspt-3
        xmax = maxspt+1
        plot, p[0,*], p[5,*], color=0, background=1, charsize=1.5, /nodata, $
              xrange=[xmin,xmax], xtitle='Spectral Type', xstyle=1, $
              yrange=[ymin,ymax], ytitle=labels[n1]+'-'+labels[n2], ystyle=1
        for i=0, 5 do begin
            type = where((p[0,*] ge cuts[i]) and (p[0,*] lt cuts[i+1]) $
                         and (p[0,*] ge minspt) and (p[0,*] le maxspt))
            if n_elements(type) gt 1 then $
              oploterror, p[0,type], p[5,type], p[6,type], color=cset[i], psym=8, symsize=1.2
        endfor
    end
    3 : begin
        if not keyword_set(xmin) then xmin = min(p[5,*])-2
        if not keyword_set(xmax) then xmax = max(p[5,*])+1
        if not keyword_set(ymin) then ymin = min(p[7,*])-.5
        if not keyword_set(ymax) then ymax = max(p[7,*])+.5
        plot, p[5,*], p[7,*], color=0, background=1, charsize=1.5, /nodata, $
              xrange=[xmin,xmax], xtitle=labels[n1]+'-'+labels[n2], xstyle=1, $
              yrange=[ymin,ymax], ytitle=labels[n3], ystyle=1
        for i=0, 5 do begin
            type = where((p[0,*] ge cuts[i]) and (p[0,*] lt cuts[i+1]) $
                         and (p[0,*] ge minspt) and (p[0,*] le maxspt))
            if n_elements(type) gt 1 then oploterror, $
              p[5,type], p[7,type], p[6,type], p[8,type], color=cset[i], psym=8, symsize=1.2
        endfor
    end
    else : begin
        if not keyword_set(xmin) then xmin = min(p[5,*])-2
        if not keyword_set(xmax) then xmax = max(p[5,*])+1
        if not keyword_set(ymin) then ymin = min(p[11,*])-.5
        if not keyword_set(ymax) then ymax = max(p[11,*])+.5
        plot, p[5,*], p[11,*], color=0, background=1, charsize=1.5, /nodata, $
              xrange=[xmin,xmax], xtitle=labels[n1]+'-'+labels[n2], xstyle=1, $
              yrange=[ymin,ymax], ytitle=labels[n3]+'-'+labels[n4], ystyle=1
        for i=0, 5 do begin
            type = where((p[0,*] ge cuts[i]) and (p[0,*] lt cuts[i+1]) $
                         and (p[0,*] ge minspt) and (p[0,*] le maxspt))
            if n_elements(type) gt 1 then oploterror, $
              p[5,type], p[11,type], p[6,type], p[12,type], color=cset[i], psym=8, symsize=1.2
        endfor
    end
endcase

fl = strarr(6)
for i=0, 5 do fl[i] = sptl[fix(cuts[i])]+trim(cuts[i] mod 10) + '-' $
  + sptl[fix(cuts[i+1]-.5)]+trim((cuts[i+1]-.5) mod 10)
legend, fl, chars=1.5, outline=0, colors=cset, psym=8, textc=0

if keyword_set(ps) then ps_close

END
