PRO MAKE_LTEYE_SAV, FILE

;  Quickie to read in csv data into a structure, and save it as a .sav file for
;  fast future use.
;
;  HISTORY
;  Written by Will Best (IfA), 08/07/2012
;

if n_elements(file) eq 0 then file = '~/Astro/699-1/lttrans/plotting/eyeballed.csv'
eyetags = 'ra:f, dec:f, iz:f, izerr:f, iy:f, iyerr:f, zy:f, zyerr:f, yw1:f, yw1err:f, w1w2:f, w1w2err:f, w2w3:f, w2w3err:f, type:b'
eye = read_struct2(file, eyetags, comment='#', delim=',', /nan, _EXTRA=ex)

; -100 is the marker for no data -- replace this with NaN to make plotting simple
for i=0, n_tags(eye)-1 do begin
    noval = where(eye.(i) eq -100.)
    if noval[0] ge 0 then eye[noval].(i) = !VALUES.F_NAN
endfor

save, eye, filename='~/Astro/699-1/lttrans/plotting/eye.sav', description='eye'

END
