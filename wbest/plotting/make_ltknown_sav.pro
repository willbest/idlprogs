PRO MAKE_LTKNOWN_SAV, FILE

;  Quickie to read in csv data into a structure, and save it as a .sav file for
;  fast future use.
;
;  HISTORY
;  Written by Will Best (IfA), 08/07/2012
;

if n_elements(file) eq 0 then file = 'known'
path = '~/Astro/699-1/lttrans/plotting/' + file + '.csv'
knowntags = 'ra:f, dec:f, iz:f, izerr:f, iy:f, iyerr:f, zy:f, zyerr:f, yw1:f, yw1err:f, w1w2:f, w1w2err:f, w2w3:f, w2w3err:f, type:b'
known = read_struct2(path, knowntags, comment='#', delim=',', /nan, _EXTRA=ex)

; -100 is the marker for no data -- replace this with NaN to make plotting simple
for i=0, n_tags(known)-1 do begin
    noval = where(known.(i) eq -100.)
    if noval[0] ge 0 then known[noval].(i) = !VALUES.F_NAN
endfor

save, known, filename='~/Astro/699-1/lttrans/plotting/'+file+'.sav', description='known'

END
