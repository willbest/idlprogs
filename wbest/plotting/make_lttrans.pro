PRO MAKE_LTTRANS, FILE

;  Quickie to read in csv data into a structure, and save it as a .sav file for
;  fast future use.
;
;  HISTORY
;  Written by Will Best (IfA), 10/09/2012
;

if n_elements(file) eq 0 then file = '~/Astro/699-1/lttrans/plotting/PS1+WISE_results.csv'
lttags = 'id:l, name:a, category:a, spt_ind:f, spt:f, spt_str:a, glon:d, plane:b, glat:d, ra:d, dec:d, rahms:a, decdms:a, area:b, name_wise:a, name_other:a, z:f, z_err:f, y:f, y_err:f, w1:f, w1_err:f, w2:f, w2_err:f, w3:f, w3_err:f, J:f, J_err:f, H:f, H_err:f, K:f, K_err:f, YM:f, YM_err:f, JM:f, JM_err:f, HM:f, HM_err:f, KM:f, KM_err:f, dist:f, disterr:f, ura:i, uraerr:i, udec:i, udecerr:i, vtan:i, vtanerr:i, Teff:i, L:f, H2OJ:f, CH4J:f, H2OH:f, CH4H:f, CH4K:f, H2OK:f, KJrat:f, Hdip:f, H2OJt:a, CH4Jt:a, H2OHt:a, CH4Ht:a, CH4Kt:a, H2OKt:a'
lttrans = read_struct2(file, lttags, comment='#', delim=',', /nan);, /verbose)

; -100 is the marker for no data -- replace this with NaN to make plotting simple
noval = where(lttrans.spt_ind eq -100.)
  if noval[0] ge 0 then lttrans[noval].spt_ind = !VALUES.F_NAN
noval = where(lttrans.spt eq -100.)
  if noval[0] ge 0 then lttrans[noval].spt = !VALUES.F_NAN

for i=16, n_tags(lttrans)-7 do begin
    noval = where(lttrans.(i) eq -100.)
    if noval[0] ge 0 then lttrans[noval].(i) = !VALUES.F_NAN
endfor

save, lttrans, filename='~/Astro/699-1/lttrans/plotting/lttrans.sav', description='lttrans'

END
