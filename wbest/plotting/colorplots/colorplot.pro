PRO COLORPLOT, X, Y, SPT, CHARS=chars, CSET=cset, CATS=cats, DOWN=down, ERRORVEC=errorvec, LEG=leg, $
               OPENSYM=opensym, POSLEG=posleg, SYMBOL=symbol, XERR=xerr, YERR=yerr, $
               _EXTRA=extrakey

;  Creates a scatter plot, with optional errors, color coded according to
;  spectral type, using categories established by CATS.
;  The number of categories, NCATS, is determined by the number of elements in
;  CATS.  NCATS is set to one fewer than the number of elements in CATS.
;  If CATS is not entered, it is set to [0,1,2,3,4,5,6], and NCATS = 6.
;
;  Uses filled and open diamonds for symbols, unless other user-defined symbols
;  are provided.
;  Currently ignores subdwarfs.
;
;  HISTORY
;  Written by Will Best (IfA), 03/09/2012
;  04/23/2012: Flipped axes for color-magnitude diagrams
;              Filters out WISE "magnitudes" for SNR<2
;  04/24/2012: Allowed plotting of singleton objects within a color cut
;              Default plotting of NIR spectral types with open symbol
;  08/06/2012: Changed name from colorchart to colorplot
;              Made this a core routine, to be called by others
;  08/07/2012: Added ERRORVEC keyword
;              Added SYMBOL keyword
;              Added CSET keyword
;              Added WIN keyword
;  08/08/2012: Added LEG keyword for legend drawing
;  08/12/2012: Added SEGMENT keyword
;  08/13/2012: Shifted the calls to window and postcript to above this level.
;              Removed OUTFILE, PS, and WIN keywords.
;              Removed SEGMENT keyword.
;
;  USE
;      colorplot, x, y, spt [, CHARS=chars, CSET=cset, CATS=cats, ERRORVEC=errorvec, LEG=leg, $
;                  OPENSYM=opensym, SYMBOL=symbol, XERR=xerr, YERR=yerr, $
;                  <plotting keywords> ]
;
;  INPUTS
;      X - Vector of values to plot on the horizontal axis.
;      Y - Vector of values to plot on the horizontal axis.
;          Must be the same length as X.
;      SPT - Vector of spectral types (numbers, not text, e.g. 14, not L4).
;            Must be supplied, even if it's the same as X, and must be
;            the same length as X and Y.
;
;  OPTIONAL KEYWORDS
;      CSET - NCATS-element vector containing the colors to be plotted.
;             Color table is set by lincolr_wb
;             Default: [2, 8, 3, 5, 4, 6]
;      CHARS - Text size, will be passed through to legend.pro if the LEG
;              keyword is set.
;      CATS - NCATS+1-element vector of numbers defining the boundaries
;             of the six categories used for plotting colors.
;             The first element cannot be less than 0.
;             The last element cannot be greater than 30.
;             Default: [0, 5, 10, 15, 20, 25, 30]
;      DOWN - plots down arrows, hacked in right now, needs to be generalized.
;      ERRORVEC - NCATS-element vector containing 0's and 1's.
;                 Categories corresponding to 0's are plotted without error bars;
;                 categories corresponding to 1's are plotted with error bars.
;                 Default: all categories plotted with error bars.
;      LEG - Array of structures, containing information needed to draw a legend
;            using the Astrolib legend program.
;            Must contain at least NCATS structures.
;            leg.text = Text to be printed on each line of the legend.
;            leg.textc = Colors for the legend text.
;            leg.sym = Symbols to be printed on each line of the legend.
;            leg.symc = Symbol colors for the legend.  Any colors set to -1 will
;                       be replaced by the corresponding colors in CSET. 
;            leg.syms = Symbol sizes for the legend.  Any sizes set to -1 will
;                       be replaced by the corresponding sizes in SYMBOL.size.
;            Legend text size can be set with the CHARS keyword.
;            If LEG is not set, no legend will be draw.
;      OPENSYM - Vector of same length as SPT, containing 0's and 1's.
;                Points corresponding to 0's are plotted with full symbols;
;                points corresponding to 1's are plotted with open symbols.
;                Default: all points plotted with full symbols.
;      POSLEG = Position vector for LEGEND
;      SYMBOL - Array of structures, containing information on symbols to plot.
;               Must contain NCATS structures.
;               The symbol numbers are used to call SYMCAT in the Coyote
;               Graphics system.  http://www.idlcoyote.com/programs/cgsymcat.pro
;               symbol.full = Full symbols to be plotted (used for default).
;               symbol.open = Open symbols to be plotted.  Ignored if OPENSYM is
;                             not set or has only 0's.
;               symbol.size = Symbol sizes to be plotted.
;               Default:  all points will be standard-sized diamonds.
;      XERR - Vector of X errors.  Must be the same length as X.
;      YERR - Vector of Y errors.  Must be the same length as Y.
;
;  Accepts keywords for plot, oplot, and oploterror.
;
;  FUTURE IMPROVEMENTS
;      Default is to only fit dwarfs.
;          Add keyword to include subdwarfs.
;          Add keyword to only fit subdwarfs.
;      Add a /silent keyword.
;
;=======================
;
;  Default colors:
;      M0-M4: BRIGHT GREEN
;      M5-M9: ORANGE
;      L0-L4: RED
;      L5-L9: MAGENTA
;      T0-T4: BLUE
;      T5-T9: CYAN
;
;=======================


if n_params() lt 3  then begin
    print, 'SYNTAX: colorplot, x, y, spt [, CHARS=chars, CSET=cset, CATS=cats, ERRORVEC=errorvec, '
    print, '          LEG=leg, OPENSYM=opensym, SYMBOL=symbol, XERR=xerr, YERR=yerr, '
    print, '          <plotting keywords> ]'
    return
endif

; Make sure the cats vector is reasonable
cs = size(cats)
if (cs[0] eq 1) and (cs[1] ge 2) and (cs[2] gt 0) and (cs[2] lt 6) then begin
    cats = cats[sort(cats)]
    if cats[0] lt 0 then cats[0] = 0
endif else begin
    print, 'CATS will be set to [0, 1, 2, 3, 4, 5, 6]'
    cats = findgen(7)
endelse

; Number of categories
ncats = n_elements(cats)-1

; Choose a color scheme
;; device, decomposed=0
;; lincolr_wb, /silent
if n_elements(cset) eq 0 then begin
    cdef = [15, 12, 2, 3, 4, 6, 8, 5, 0, 7, 9, 10, 11, 13, 14]
    cset = cdef[0:ncats-1]
endif

; Check for an error bar vector
if n_elements(errorvec) eq 0 then errorvec = intarr(ncats)+1
if n_elements(errorvec) ne ncats then begin
    print, 'ERRORVEC must have the same length as CSET.'
    return
endif

; Define structure with plotting symbols
;; diam = [ [0, 1], [1, 0], [0, -1], [-1, 0], [0, 1] ]
if n_elements(symbol) eq 0 then begin
    symbol = replicate({full:14, open:4, size:1.5}, ncats)          ; default symbol is a diamond
endif
if n_elements(symbol) lt ncats then begin
    print, 'SYMBOL must have at least as many elements as CSET.'
    return
endif
if n_tags(symbol) lt 3 then begin
    print, 'SYMBOL must have at least two tags per structure element.'
    return
endif

; Array for filled/open symbols
n = n_elements(spt)
if n_elements(opensym) eq 0 then opensym = intarr(n)
if n_elements(opensym) ne n then begin
    print, 'OPENSYM vector must have the same length as the other plotting vectors'
    return
endif

; Define the plotting space
if n_elements(xrange) eq 0 then xrange = [min(x)-2, max(x)+1]
if n_elements(yrange) eq 0 then yrange = [min(y)-.5, max(y)+.5]
plot, x, y, color=0, background=1, charsize=1.6, /nodata, _extra=extrakey, $
      xrange=xrange, xstyle=1, yrange=yrange, ystyle=1
;; plot, x, y, color=0, background=1, charsize=2, /nodata, _extra=extrakey, $
;;       xrange=xrange, xstyle=1, yrange=yrange, ystyle=1

;; west11 = 'Dropbox/panstarrs-BD/Known\ Objects/Mdwarfs_DR7_PS1WISE.csv'
;; readcol, west11, wy, ww1, ww2, skipline=1, delim=',', $
;;          format='x,x,x,x,x,x,x,x,x,x, x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,f,x,x,x,x,x,x,x,x,x,x,x,x,f,x,x,f'
;; oplot, wy-ww1, ww1-ww2, psym=4, color=0, symsize=0.1

; Change NaN errors to 0.
if (n_elements(xerr) gt 0) then begin
    badx = where(finite(xerr) eq 0)
    if badx[0] ge 0 then xerr[badx] = 0.
endif
if (n_elements(yerr) gt 0) then begin
    bady = where(finite(yerr) eq 0)
    if bady[0] ge 0 then yerr[bady] = 0.
endif

; Plot each color-coded slice of the data, one at a time
for i=0, ncats-1 do begin

    ; Plot the points with filled symbols
    typef = where((spt ge cats[i]) and (spt lt cats[i+1]) and (opensym eq 0))
    if typef[0] ne -1 then begin
        if (n_elements(yerr) eq 0) or (errorvec[i] eq 0) then begin
            ; If no y-error vector is present, plot without error bars
            oplot, x[typef], y[typef], color=cset[i], psym=symcat(symbol[i].full), symsize=symbol[i].size
        endif else begin
            if (n_elements(xerr) eq 0) then begin
                ; If only a y-error vector is present, plot only those error bars
                nantestye = where(finite(yerr[typef]))
                if nantestye[0] ne -1 then $
                  oploterror, x[typef], y[typef], yerr[typef], /nohat, errthick=2, $
                              color=cset[i], psym=symcat(symbol[i].full), symsize=symbol[i].size
            endif else begin
                ; Otherwise, plot error bars for both x and y
;                nantestxe = where(finite(xerr[typef]))
;                nantestye = where(finite(yerr[typef]))
;                if (nantestye[0] ne -1) and (nantestxe[0] ne -1) then $
                  oploterror, x[typef], y[typef], xerr[typef], yerr[typef], /nohat, errthick=2, $
                              color=cset[i], psym=symcat(symbol[i].full), symsize=symbol[i].size
            endelse
        endelse
    endif

    ; Plot the points with open symbols
    typeo = where((spt ge cats[i]) and (spt lt cats[i+1]) and (opensym eq 1))
    if typeo[0] ne -1 then begin
        if (n_elements(yerr) eq 0) or (errorvec[i] eq 0) then begin
            ; If no y-error vector is present, plot without error bars
            oplot, x[typeo], y[typeo], color=cset[i], psym=symcat(symbol[i].open, thick=5), symsize=symbol[i].size
        endif else begin
            if (n_elements(xerr) eq 0) then begin
                ; If only a y-error vector is present, plot only those error bars
                nantestye = where(finite(yerr[typeo]))
                if nantestye[0] ne -1 then $
                  oploterror, x[typeo], y[typeo], yerr[typeo], /nohat, errthick=2, $
                              color=cset[i], psym=symcat(symbol[i].open, thick=5), symsize=symbol[i].size
            endif else begin
                ; Otherwise, plot error bars for both x and y
                nantestxe = where(finite(xerr[typeo]))
                nantestye = where(finite(yerr[typeo]))
                if (nantestye[0] ne -1) and (nantestxe[0] ne -1) then $
                  oploterror, x[typeo], y[typeo], xerr[typeo], yerr[typeo], /nohat, errthick=2, $
                              color=cset[i], psym=symcat(symbol[i].open, thick=5), symsize=symbol[i].size
            endelse
            ; Fill in open symbols with white
;            oplot, x[typeo], y[typeo], color=1, psym=symcat(symbol[i].full), symsize=.9*symbol[i].size
        endelse
    endif

    ; Down arrows
    if keyword_set(down) and (n_elements(yerr) ne 0) and (errorvec[i] ne 0) then begin
        typex = where((spt ge cats[i]) and (spt lt cats[i+1]) and (yerr eq 0))
        plotsym, 1, 3.0, thick=2, color=cset[i]
        oplot, x[typex], y[typex], psym=8
    endif

endfor

; Draw the legend
if n_elements(leg) ne 0 then begin
; Make sure symbols colors and sizes are good for the legend.
    nocolor = where(leg[0:ncats-1].symc eq -1)
    if nocolor[0] ne -1 then leg[0:ncats-1].symc = cset[nocolor]
    nosize = where(leg[0:ncats-1].syms eq -1)
    if nosize[0] ne -1 then leg[0:ncats-1].syms = symbol[nosize].size
; Draw it
    legend, leg.text, textc=leg.textc, outline=0, colors=leg.symc, $
            psym=leg.sym, symsize=leg.syms, chars=chars, pos=posleg, _extra=extrakey
endif


END
