PRO LTEYEPLOT, X, Y, TYPE, XERR, YERR, CATS=cats, DOWN=DOWN, ERRORV=errorv, ERRSPOT=errspot, $
               NOERROR=noerror, NOLEG=noleg, OUTFILE=outfile, PS=ps, SEGMENT=segment, $
               WIN=win, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax, $
               _EXTRA=extrakey

;  Routine to plot color-color and color-magnitude diagrams for my first 699
;  project, on L-T transition dwarfs.
;  Plots showing the eyeballed objects:
;       all 0, candidates 1, eyeballed not passed 2, eyeballed passed 3, discoveries 5
;
;  HISTORY
;  Written by Will Best (IfA), 08/13/2012
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      X - Vector of values to plot on the horizontal axis.
;      Y - Vector of values to plot on the horizontal axis.
;          Must be the same length as X.
;      FILE - .csv file containing the data to be plotted
;      YERR - Vector of Y errors.  Must be the same length as Y.
;      XERR - Vector of X errors.  Must be the same length as X.
;
;  OPTIONAL KEYWORDS
;      CATS - Vector containing spectral types for the boundaries of the plotted
;             colors.
;             Default: [0, 1, 2, 3, 4, 5, 6]
;             The first element cannot be less than 0.
;      DOWN - Plot down arrows for Type 5 objects
;      ERRORV - Vector containing 0's and 1's.  Must have one fewer element
;               thatn CATS.
;               Categories corresponding to 0's are plotted without error bars;
;               categories corresponding to 1's are plotted with error bars.
;               Default: [0,0,1,1,1,1]
;               If NOERROR is set, ERRORV is ignored.
;      ERRSPOT - Location to draw "typical" error bars for type 1 objects.
;      NOERROR - Don't plot the error bars.  Overrides ERRORVEC.
;      NOLEG - Don't draw a legend.
;      OUTFILE - Path for postscript output
;                Default: ~/Astro/colorplot1.ps
;      PS - Output to postscript.
;      SEGMENT - Array of structures, containing information on line segments to
;                draw on the plot.
;                segment.x = 2-element vectors containing x-coords of segment
;                endpoints.
;                segment.y = 2-element vectors containing y-coords of segment
;                endpoints.
;                segment.color = color for segment
;                segment.style = linestyle for segment (keyword for PLOTS)
;                segment.thick = thickness for segment (keyword for PLOTS)
;      WIN - Window number to use when calling the IDL window routine.
;            Usefull when a program calls colorplot multiple times.
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;

;=======================

n = n_elements(x)
type5 = where(type eq 5)

;=======================

; Create vector for filled and open symbols
opensym = bytarr(n)

; Default cats vector
if n_elements(cats) eq 0 then cats = [0, 2, 3, 5, 6]
ncats = n_elements(cats) - 1

; Default error bar vector
if keyword_set(noerror) then errorv = intarr(ncats)
if n_elements(errorv) eq 0 then begin
    if ncats eq 3 then errorv = [1,1,1] else errorv = [0,1,1,1]
endif

; Default color set
if ncats eq 3 then cset = [12, 4, 3] else cset = [15, 12, 4, 3]

; Define structure with plotting symbols
symbol = replicate({full:0, open:0, size:0.}, 4)
; diamond, square, circle, triangle
symbol.full = [14,  15,  16,  17]
symbol.open = [ 4,   6,   9,   5]
symbol.size = [.8, 1.5, 1.5, 3.0]

; Define structure with only small points for plotting symbols
pts = replicate({full:0, open:0, size:0.}, ncats)
; default symbol is a small square
pts.full = intarr(ncats) + 15
pts.open = intarr(ncats) + 15
pts.size = intarr(ncats) + .5

; Load a color table
device, decomposed=0
lincolr_wb, /silent

; Build the legend structure
leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
ltext = ['All', 'Failed inspection', 'Passed inspection', 'Discoveries']
leg.text = ltext[(4-ncats):3]
leg.textc = intarr(ncats)
legchars = 1.5
leg.sym = symbol[(4-ncats):3].full
leg.syms = symbol[(4-ncats):3].size


; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/colorplot1.ps'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=600
endelse

if n_elements(xmin) eq 0 then xmin = min(x, /nan) - .5
if n_elements(xmax) eq 0 then xmax = max(x, /nan) + .5
if n_elements(ymin) eq 0 then ymin = min(y, /nan) - .5
if n_elements(ymax) eq 0 then ymax = max(y, /nan) + .5

; Plot the graph
if keyword_set(noleg) then begin
    colorplot, x, y, type, cats=cats, cset=cset, _extra=extrakey, $
           xrange=[xmin,xmax], xerr=xerr, errorvec=errorv, $
           yrange=[ymin,ymax], yerr=yerr, symbol=symbol[(4-ncats):3]
endif else begin
    colorplot, x, y, type, leg=leg, chars=legchars, cats=cats, cset=cset, _extra=extrakey, $
           xrange=[xmin,xmax], xerr=xerr, errorvec=errorv, $
           yrange=[ymin,ymax], yerr=yerr, symbol=symbol[(4-ncats):3]
endelse

; Plot any segments called
for i=0, n_elements(segment)-1 do plots, segment[i].x, segment[i].y, $
           color=segment[i].color, linestyle=segment[i].style, thick=segment[i].thick

; Plot a typical error, if called
if n_elements(errspot) gt 0 then begin
    mxerr = median(xerr[type1])
    myerr = median(yerr[type1])
    plots, [errspot[0], errspot[0]], [errspot[1]-myerr, errspot[1]+myerr], $
           color=cset[(ncats-4)], linestyle=0, thick=3
    plots, [errspot[0]-mxerr, errspot[0]+mxerr], [errspot[1], errspot[1]], $
           color=cset[(ncats-4)], linestyle=0, thick=3
endif

; Down arrows
if keyword_set(down) then begin
    plotsym, 1, 3.0, thick=2, color=3
    oplot, x[type5], y[type5], psym=8
endif

if keyword_set(ps) then ps_close


END
