PRO MEGADISCPLOT, F1, F2, F3, F4, CATS=cats, CATTOTS=cattots, ERRORV=errorv, FULLSYM=fullsym, $
              HORLINE=horline, LEGSIZE=legsize, MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, $
              OUTFILE=outfile, PS=ps, SEGMENT=segment, VERLINE=verline, WIN=win, $
              XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax, $
              _EXTRA=extrakey

;  Using Kimberley Aller's table of known ultracool dwarfs, and objects from
;  Will Best's L/T transition dwarf search, plots color-color and
;  color-magnitude diagrams of requested filter combinations.  By default, plots
;  objects with optical spectral type as open diamonds, and objects with
;  near-infrared spectral type as filled diamonds.  Currently ignores subdwarfs.
;
;  HISTORY
;  Written by Will Best (IfA), 10/09/2012
;  Adapted from megaplot.pro
;
;  USE
;      megadiscplot, f1, f2 [, f3, f4, CATS=cats, CATTOTS=cattots, ERRORV=errorv, FULLYM=fullsym, $
;                HORLINE=horline, MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, PS=ps, $
;                SEGMENT=segment, VERLINE=verline, WIN=win, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      F1, F2, [F3, F4] - Filters whose colors or magnitudes are to be plotted.
;         If four filters are entered, a plot of F3-F4 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If three filters are entered, a plot of F3 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If two filters are entered, a plot of F1-F2 (y-axis) vs. SpT (x-axis)
;         will be created.
;         If fewer than two filters are entered, the syntax and a list of
;         available filters are printed, and the program quits.
;
;  OPTIONAL KEYWORDS
;      CSET - Six-element vector containing the colors to be plotted.
;             Color table is set by lincolr_wb
;             Default: [2, 8, 3, 5, 4, 6]
;      CATS - Seven-element vector containing spectral types for the boundaries
;             of the plotted colors.  Default: [0, 5, 10, 15, 20, 25, 30]
;             The first element cannot be less than 0.
;             The last element cannot be greater than 30.
;      CATTOTS - If a legend is being drawn, display the number of objects in
;                each category next to the name for each category in the legend.
;      ERRORV - Six-element vector containing 0's and 1's.
;               Categories corresponding to 0's are plotted without error bars;
;               categories corresponding to 1's are plotted with error bars.
;               Default: all categories plotted with error bars.
;               If NOERROR is set, ERRORV is ignored.
;      FULLSYM - Force all symbols to be filled, irrespective of spectral type.
;      HORLINE - Array of structures, containing information on horizontal lines to
;                draw on the plot.
;                horline.val = y-value of vertical line to draw.
;                horline.color = color for line
;                horline.style = linestyle for segment (keyword for OPLOT)
;                horline.thick = thickness for segment (keyword for OPLOT)
;      LEGSIZE - Multiplier for the size of the legend characters.
;                Default = 1
;      MINSPT - Minimum spectral type to fit.
;          Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;                 (no data exists for types later than T9.)
;          Default: 0 (M0)
;      MAXSPT - Maximum spectral type to fit (same scale as MINSPT).
;          Default: 29 (T9).
;      NOERROR - Don't plot the error bars.  Overrides ERRORVEC.
;      OUTFILE - Path for postscript output
;                Default: ~/Astro/colorplot1.ps
;      PS - Output to postscript.
;      SEGMENT - Array of structures, containing information on line segments to
;                draw on the plot.
;                segment.x = 2-element vectors containing x-coords of segment
;                endpoints.
;                segment.y = 2-element vectors containing y-coords of segment
;                endpoints.
;                segment.color = color for segment
;                segment.style = linestyle for segment (keyword for PLOTS)
;                segment.thick = thickness for segment (keyword for PLOTS)
;      VERLINE - Array of structures, containing information on vertical lines to
;                draw on the plot.
;                verline.val = x-value of vertical line to draw.
;                verline.color = color for line
;                verline.style = linestyle for segment (keyword for OPLOT)
;                verline.thick = thickness for segment (keyword for OPLOT)
;      WIN - Window number to use when calling the IDL window routine.
;            Usefull when a program calls colorplot multiple times.
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;
;  FUTURE IMPROVEMENTS
;      Default is to only fit dwarfs.
;          Add keyword to include subdwarfs.
;          Add keyword to only fit subdwarfs.
;      Add a /silent keyword.
;

;=======================

; Load Kimberly's mega-table. 
; More info in Dropbox > panstarrs-BD > Known Objects > mlt_dwarfs > README
infile1 = '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
;infile1 = '~/Astro/PS1BD/mlt_2uwp_duplicate_3pi_new.sav'
restore, infile1
mega = wise_new
;mega = mlt

; Load L/T search objects. 
infile2 = '~/Astro/699-1/lttrans/plotting/lttrans.sav'
restore, infile2

;=======================

; Create structure for filter names and polynomial coefficients
names = ['PS1 z', 'PS1 y', $
         '2MASS J', '2MASS H', '2MASS K', $
         'MKO Y', 'MOK J', 'MKO H', 'MKO K', $
         'WISE W1', 'WISE W2', 'WISE W3']
codes = ['z', 'y', $
         'J', 'H', 'K', $
         'YM', 'JM', 'HM', 'KM', $
         'W1', 'W2', 'W3']
; 49 Z  50 ZERR  53 Y  54 YERR 
; 1 JMAG  2 E_JMAG  3 HMAG  4 E_HMAG  5 KMAG  6 E_KMAG
; 26 Y_2  27 J  28 H  29 K  30 YERR_2  31 JERR  32 HERR  34 KERR
; 121 W1  122 W1ERR  125 W2  126 W2ERR  129 W3  130 W3ERR  133 W4  134 W4ERR
refsmega = [[49,50], [53,54], $
        [1,2], [3,4], [5,6], $
        [26,30], [27,31], [28,32], [29,34], $
        [121,122], [125,126], [129,130] ]
refslt = [[13,14], [15,16], $
        [23,24], [25,26], [27,28], $
        [29,30], [31,32], [33,34], [35,36], $
        [17,18], [19,20], [21,22] ]
labels = ['z!DPS1!N', 'y!DPS1!N', $
         'J!D2MASS!N', 'H!D2MASS!N', 'K!D2MASS!N', $
         'Y!DMKO!N', 'J!DMKO!N', 'H!DMKO!N', 'K!DMKO!N', $
         'W1', 'W2', 'W3']

; Check for correct number of input parameters
nfilt = n_params()
if nfilt lt 2 or nfilt gt 4 then begin
    print, 'SYNTAX: megaplot, f1, f2 [, f3, f4, CATS=cats, CATTOTS=catttos, ERRORV=errorv, '
    print, '          FULLSYM=fullsym, HORLINE=horline, MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, '
    print, '          OUTFILE=outfile, PS=ps, SEGMENT=segment, VERLINE=verline, WIN=win, '
    print, '          XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax ]'
    print, 'Plots (1) f1-f2 vs. SpT  or  (2) f3 vs. f1-f2  or  (3) f3-f4 vs. f1-f2'
    print
    print, 'Filters available:'
    for i=0, n_elements(names)-1 do print, codes[i], ' = ', names[i]
    return
endif

; Check for sensisble minimum and maximum spectral types
if ((size(minspt, /type) ne 2) and (size(minspt, /type) ne 4)) then minspt = 0
if (minspt lt 0) then minspt = 0
if (minspt gt 29) then minspt = 29
if ((size(maxspt, /type) ne 2) and (size(maxspt, /type) ne 4)) then maxspt = 29
if (maxspt gt 29) then maxspt = 29
if (maxspt lt minspt) then maxspt = minspt

; Display the earliest and latest spectral types to be plotted
sptl = [replicate('M',10), replicate('L',10), replicate('T',10)]
if (maxspt eq minspt) then begin
    print, 'Only spectral type plotted will be '+sptl[fix(minspt)]+trim(minspt mod 10)
endif else begin
    print, 'Earliest spectral type plotted will be '+sptl[fix(minspt)]+trim(minspt mod 10)
    print, 'Latest spectral type plotted will be '+sptl[fix(maxspt)]+trim(maxspt mod 10)
endelse

; restrict spectral types as defined by minspt and maxspt
noneg = where((mega.spt ge minspt) and (mega.spt le maxspt))
mega = mega[noneg]
noneg = where((lttrans.spt ge minspt) and (lttrans.spt le maxspt))
lttrans = lttrans[noneg]

; Default cats vector
if n_elements(cats) eq 0 then cats = [0, 5, 10, 15, 20, 25, 30]
ncats = n_elements(cats) - 1

; Create arrays for photometry, colors, and errors
p = fltarr(14, n_elements(mega))
l = fltarr(14, n_elements(lttrans))
p[0,*] = mega.spt                                       ; Spectral Type
l[0,*] = lttrans.spt                                    ; Spectral Type

; Create vector for filled and open symbols
if not keyword_set(fullsym) then begin
    mega.ospt = trim(strmid(mega.ospt,1,4)) ; Trim the optical SpT string
;mega.nirspt = trim(strmid(mega.nirspt,1,4)) ; Trim the near-IR SpT string
    ospt = sptypenum(mega.ospt, /silent)    ; Convert optical SpT to number
;;       ; Always prefer Optical Spectral Type
;;       hasospt = where((ospt ge 0) and (ospt le 29))
;;       p[0,hasospt] = ospt[hasospt]
    p[13,*] = (ospt eq p[0,*])     ; Open symbol for optical SpT
endif

switch nfilt of
    4 : begin
        n4 = where(f4 eq codes)
        p[9,*] = mega.(refsmega[0,n4])                 ; f4
        p[10,*] = mega.(refsmega[1,n4])                ; f4err
        l[9,*] = lttrans.(refslt[0,n4])                ; f4
        l[10,*] = lttrans.(refslt[1,n4])               ; f4err
    end
    3 : begin
        n3 = where(f3 eq codes)
        p[7,*] = mega.(refsmega[0,n3])                 ; f3
        p[8,*] = mega.(refsmega[1,n3])                 ; f3err
        l[7,*] = lttrans.(refslt[0,n3])                ; f3
        l[8,*] = lttrans.(refslt[1,n3])                ; f3
        if nfilt eq 4 then begin
            p[11,*] = p[7,*] - p[9,*]                  ; f3-f4
            p[12,*] = sqrt(p[8,*]^2 + p[10,*]^2)       ; f3-f4 err
            l[11,*] = l[7,*] - l[9,*]                  ; f3-f4
            l[12,*] = sqrt(l[8,*]^2 + l[10,*]^2)       ; f3-f4 err
        endif
    end
    else : begin
        n1 = where(f1 eq codes)
        p[1,*] = mega.(refsmega[0,n1])                 ; f1
        p[2,*] = mega.(refsmega[1,n1])                 ; f1err
        l[1,*] = lttrans.(refslt[0,n1])                ; f1
        l[2,*] = lttrans.(refslt[1,n1])                ; f1err
        n2 = where(f2 eq codes)
        p[3,*] = mega.(refsmega[0,n2])                 ; f2
        p[4,*] = mega.(refsmega[1,n2])                 ; f2err
        l[3,*] = lttrans.(refslt[0,n2])                ; f2
        l[4,*] = lttrans.(refslt[1,n2])                ; f2err
        p[5,*] = p[1,*] - p[3,*]                       ; f1-f2
        p[6,*] = sqrt(p[2,*]^2 + p[4,*]^2)             ; f1-f2 err
        l[5,*] = l[1,*] - l[3,*]                       ; f1-f2
        l[6,*] = sqrt(l[2,*]^2 + l[4,*]^2)             ; f1-f2 err
    end
endswitch

; remove unreasonable magnitudes and colors
switch nfilt of
    4 : begin
        sane4 = where(p[9,*] gt 0)
        p = p[*,sane4]
    end
    3 : begin
        sane3 = where(p[7,*] gt 0)
        p = p[*,sane3]
    end
    else : begin
        sane12 = where((p[1,*] gt 0) and (p[3,*] gt 0))
        p = p[*,sane12]
   end
endswitch

; remove WISE W1 and W2 upper limit "magnitudes" with no error
switch nfilt of
    4 : begin
        if (f4 eq 'W1') or (f4 eq 'W2') then begin
            wsane4 = where(p[10,*] ge 0)
            p = p[*,wsane4]
        endif
    end
    3 : begin
        if (f3 eq 'W1') or (f3 eq 'W2') then begin
            wsane3 = where(p[8,*] ge 0)
            p = p[*,wsane3]
        endif
    end
    else : begin
        if (f2 eq 'W1') or (f2 eq 'W2') then begin
            wsane2 = where(p[4,*] ge 0)
            p = p[*,wsane2]
        endif
        if (f1 eq 'W1') or (f1 eq 'W2') then begin
            wsane1 = where(p[2,*] ge 0)
            p = p[*,wsane1]
        endif
   end
endswitch

; Build a structure with plotting symbols
;; symbol = {s0:fltarr(2,5), s1:fltarr(2,5), s2:fltarr(2,5), s3:fltarr(2,13), $
;;           s4:fltarr(2,4), s5:fltarr(2,4)}
;; ss = 1.5         ; defines symbol size
;; sm = .7          ; multiplier for smaller symbols
;; symbol.(0) = [ [0, sm*ss], [sm*ss, 0], [0, -sm*ss], [-sm*ss, 0], [0, sm*ss] ]   ; small diamond
;; symbol.(1) = [ [0, sm*ss], [sm*ss, 0], [0, -sm*ss], [-sm*ss, 0], [0, sm*ss] ]   ; small diamond
;; sq = ss*.707     ; multiplier for squares
;; symbol.(2) = [ [sq, sq], [sq, -sq], [-sq, -sq], [-sq, sq], [sq, sq] ]           ; square
;; circ = findgen(13) * (!pi*2/16.)
;; symbol.(3) = transpose([ [ss*cos(circ)], [ss*sin(circ)] ])                      ; circle
;; symbol.(4) = [ [0, ss], [ss*.866, -ss/2.], [-ss*.866, -ss/2.], [0, ss] ]        ; triangle
;; symbol.(5) = [ [0, ss], [ss*.866, -ss/2.], [-ss*.866, -ss/2.], [0, ss]
;; ]        ; triangle

symbolp = replicate({full:0, open:0, size:0.}, ncats)
; diamond, diamond, square, circle, triangle, triangle
symbolp.full = [14, 14,  15,  16,  17,  17]
symbolp.open = [ 4,  4,   6,   9,   5,   5]
symbolp.size = [.8, .8, 1.5, 1.5, 1.5, 1.5]

; Check for error bar vector
if keyword_set(noerror) then errorv = intarr(ncats)
if n_elements(errorv) eq 0 then errorv = intarr(ncats)+1
if n_elements(errorv) ne ncats then begin
    print, 'ERRORV must have as many elements as CSET, all 0''s or 1''s'
    return
endif

; Build the legend structure
fl = strarr(ncats)
sptl = [replicate('M',10), replicate('L',10), replicate('T',10)]
if keyword_set(cattots) then begin
    ctot = intarr(ncats)
    for i=0, ncats-1 do begin
        cm = where((p[0,*] ge cats[i]) and (p[0,*] lt cats[i+1]))
        if cm[0] ne -1 then ctot[i] = n_elements(cm) 
        fl[i] = sptl[fix(cats[i])]+trim(cats[i] mod 10) + '-' $
                + sptl[fix(cats[i+1]-.5)]+trim((cats[i+1]-.5) mod 10) + ' : ' + trim(ctot[i])
    endfor
endif else begin
    for i=0, ncats-1 do fl[i] = sptl[fix(cats[i])]+trim(cats[i] mod 10) + '-' $
      + sptl[fix(cats[i+1]-.5)]+trim((cats[i+1]-.5) mod 10)
endelse

if n_elements(legsize) eq 0 then legsize = 1.
if max(p[13,*]) eq 0 then begin      ; No open symbols plotted
    leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
    leg.text = fl
    leg.textc = intarr(ncats)
    legchars = 1.5*legsize
    leg.sym = symbol.full
    if n_elements(cset) eq ncats then leg.symc = cset
    leg.syms = symbol.size
endif else begin
    leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats+3)
    leg.text = [fl, '-------', 'Optical SpT', 'Near-IR SpT']
    leg.textc = intarr(ncats+3)
    legchars = 1.3*legsize
    leg.sym = [symbol.full, symbol[0].full, symbol[0].full, symbol[0].open]
    if n_elements(cset) eq ncats then leg.symc[0:ncats-1] = cset
    leg[ncats:ncats+2].symc = [1, 0, 0]
    leg.syms = [symbol.size, 1, 1.5, 1.5]
endelse


; Load a color table
device, decomposed=0
lincolr_wb, /silent

; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/colorchart1'
    ps_open, outfile, /color, /en, thick=4
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=600
endelse


case nfilt of

; 2 filters: F1-F2 vs. SpT
    2 : begin
        if not keyword_set(ymin) then ymin = min(p[5,*])-.5
        if not keyword_set(ymax) then ymax = max(p[5,*])+.5
        xmin = minspt-1
        xmax = maxspt+1
        colorplot, p[0,*], p[5,*], p[0,*], opensym=p[13,*], _extra=extrakey, $
                   xrange=[xmin,xmax], xtitle='Spectral Type', $
                   yrange=[ymin,ymax], ytitle=labels[n1]+'-'+labels[n2], yerr=p[6,*], $
                   cats=cats, errorvec=errorv, symbol=symbolp, leg=leg, chars=legchars
        colorplot, l[0,*], l[5,*], l[0,*], _extra=extrakey, $
                   xrange=[xmin,xmax], xtitle='Spectral Type', $
                   yrange=[ymin,ymax], ytitle=labels[n1]+'-'+labels[n2], yerr=p[6,*], $
                   cats=cats, errorvec=errorv, symbol=symbolp, leg=leg, chars=legchars
    end

; 3 filters: F3 vs. F1-F2
    3 : begin
        if not keyword_set(xmin) then xmin = min(p[5,*])-2
        if not keyword_set(xmax) then xmax = max(p[5,*])+1
        if not keyword_set(ymin) then ymin = min(p[7,*])-.5
        if not keyword_set(ymax) then ymax = max(p[7,*])+.5
        colorplot, p[5,*], p[7,*], p[0,*], opensym=p[13,*], _extra=extrakey, $
                   xrange=[xmin,xmax], xtitle=labels[n1]+'-'+labels[n2], xerr=p[6,*], $
                   yrange=[ymax,ymin], ytitle=labels[n3], yerr=p[8,*], $
                   cats=cats, errorvec=errorv, symbol=symbolp, leg=leg, chars=legchars
    end

; 4 filters: F3-F4 vs. F1-F2
    else : begin
        if not keyword_set(xmin) then xmin = min(p[5,*])-2
        if not keyword_set(xmax) then xmax = max(p[5,*])+1
        if not keyword_set(ymin) then ymin = min(p[11,*])-.5
        if not keyword_set(ymax) then ymax = max(p[11,*])+.5
        colorplot, p[5,*], p[11,*], p[0,*], opensym=p[13,*], _extra=extrakey, $
                   xrange=[xmin,xmax], xtitle=labels[n1]+'-'+labels[n2], xerr=p[6,*], $
                   yrange=[ymin,ymax], ytitle=labels[n3]+'-'+labels[n4], yerr=p[12,*], $
                   cats=cats, errorvec=errorv, symbol=symbolp, leg=leg, chars=legchars
    end

endcase

;; west11 = 'Dropbox/panstarrs-BD/Known\ Objects/Mdwarfs_DR7_PS1WISE.csv'
;; readcol, west11, wy, ww1, ww2, skipline=1, delim=',', $
;;          format='x,x,x,x,x,x,x,x,x,x, x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,f,x,x,x,x,x,x,x,x,x,x,x,x,f,x,x,f'
;; oplot, wy-ww1, ww1-ww2, psym=3, color=0

; Plot any segments called
for i=0, n_elements(segment)-1 do plots, segment[i].x, segment[i].y, $
           color=segment[i].color, linestyle=segment[i].style, thick=segment[i].thick

; Plot any horizontal lines called
for i=0, n_elements(horline)-1 do hline, horline[i].val, $
           color=horline[i].color, linestyle=horline[i].style, thick=horline[i].thick

; Plot any vertical lines called
for i=0, n_elements(verline)-1 do vline, verline[i].val, $
           color=verline[i].color, linestyle=verline[i].style, thick=verline[i].thick


if keyword_set(ps) then ps_close

END
