ps=1
;PRO MAKELTPLOT, PS=ps

;  Wrapper for ltknownplot.pro and lteyeplot.pro
;
;  HISTORY
;  Written by Will Best (IfA), 08/13/2012
;
;  USE
;      megaplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]
;  KEYWORDS
;      PS - Output to postscript.
;

cats = [1, 2, 3, 4, 5]
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]

; Load the data table known.sav.
restore, '~/Astro/699-1/lttrans/plotting/known.sav', description=sname
rename = 'list = temporary('+sname+')'
dummy = execute(rename)

; Output #1a: known, dec v. ra
outfile = '~/Astro/699-1/paper/objplots/radec'
col = [15, 2, 3, 4]
xmin = min(list.ra) - 2
xmax = max(list.ra) + 2
ymin = min(list.dec) - 2
ymax = max(list.dec) + 2
ltknownplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
             xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
             xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
             win=0, cset=col, cats=cats, /noerror, posleg=[276, 12]

; Output #1b: known, dec v. ra, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/radec_all'
;; ltknownplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
;;              xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
;;              xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
;;              win=10, /noleg, /noerror

;; ; Output #2a: known, i-y v. i-z
;; outfile = '~/Astro/699-1/paper/objplots/iyiz_obj'
;; xmin = -1.5
;; xmax = 3.6
;; ymin = -1.5
;; ymax = 4.5
;; segment.x = [ [1.8, 1.8], [1.8, 3.6] ]
;; segment.y = [ [2.8, 4.5], [2.8, 2.8] ]
;; ltknownplot, list.iz, list.iy, list.type, list.izerr, list.iyerr, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='i!DPS1!N-z!DPS1!N', ytitle='i!DPS1!N-y!DPS1!N', $
;;              win=1, cats=cats, errspot=[-1.0, 2.9]

;; ; Output #2b: known, i-y v. i-z, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/iyiz_obj_all'
;; xmin = -2.0
;; ymin = -2.0
;; ltknownplot, list.iz, list.iy, list.type, list.izerr, list.iyerr, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='i!DPS1!N-z!DPS1!N', ytitle='i!DPS1!N-y!DPS1!N', $
;;              win=11, errspot=[-1.5, 2.5]

; Output #3a: known, W1-W2 v. y-W1
;; outfile = '~/Astro/699-1/paper/objplots/w1w2yw1_obj'
xmin = 2.2
xmax = 6.3
ymin = 0
ymax = 2.5
segment.x = [ [3.0, 3.0], [3.0, 6.3] ]
segment.y = [ [0.4, 2.5], [0.4, 0.4] ]
;; ltknownplot, list.yw1, list.w1w2, list.type, list.yw1err, list.w1w2err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='y!DPS1!N-W1', ytitle='W1-W2', $
;;              win=2, cats=cats, errspot=[5.8, 1.8], /right

; Output #3b: known, W1-W2 v. y-W1, incl. all objects
outfile = '~/Astro/699-1/paper/objplots/w1w2yw1_obj_all'
ltknownplot, list.yw1, list.w1w2, list.type, list.yw1err, list.w1w2err, ps=ps, outfile=outfile, $
             xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
             xtitle='y!DPS1!N-W1', ytitle='W1-W2', $
             win=12, /right;, errspot=[6.0, 1.7]

; Output #4a: known, W2-W3 v. W1-W2
;; outfile = '~/Astro/699-1/paper/objplots/w2w3w1w2_obj'
;; xmin = 0
;; xmax = 2.5
;; ymin = -0.5
;; ymax = 4
;; segment.x = [ [0.4, 0.4], [0.4, 2.5] ]
;; segment.y = [ [-0.5, 2.5], [2.5, 2.5] ]
;; ltknownplot, list.w1w2, list.w2w3, list.type, list.w1w2err, list.w2w3err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='W1-W2', ytitle='W2-W3', $
;;              win=3, cats=cats, errspot=[2.3, 3.3], posleg=[0.9, 0.5]

;; ; Output #4b: known, W2-W3 v. W1-W2, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/w2w3w1w2_obj_all'
;; ymax = 4.5
;; ltknownplot, list.w1w2, list.w2w3, list.type, list.w1w2err, list.w2w3err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='W1-W2', ytitle='W2-W3', $
;;              win=13, errspot=[2.3, 3.6], posleg=[0.9, 0.7]

;-----------------------------------------------------------

; Load the data table eye.sav.
restore, '~/Astro/699-1/lttrans/plotting/eye.sav', description=sname
rename = 'list = temporary('+sname+')'
dummy = execute(rename)

cats = [2, 3, 5, 6]

; Output #5a: eye, dec v. ra
;; outfile = '~/Astro/699-1/paper/objplots/radec_eye'
;; col = [2, 4, 3]
;; xmin = min(list.ra) - 2
;; xmax = max(list.ra) + 2
;; ymin = min(list.dec) - 2
;; ymax = max(list.dec) + 2
;; lteyeplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
;;            xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
;;            xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
;;            win=4, cset=col, cats=cats, /noerror, /noleg

;; ; Output #5b: eye, dec v. ra, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/radec_eye_all'
;; lteyeplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
;;            xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
;;            xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
;;            win=14, /noleg, /noerror

;; ; Output #6a: eye, i-y v. i-z
;; outfile = '~/Astro/699-1/paper/objplots/iyiz_eye'
;; xmin = -1.5
;; xmax = 3.6
;; ymin = -1.5
;; ymax = 4.5
;; segment.x = [ [1.8, 1.8], [1.8, 3.6] ]
;; segment.y = [ [2.8, 4.5], [2.8, 2.8] ]
;; lteyeplot, list.iz, list.iy, list.type, list.izerr, list.iyerr, ps=ps, outfile=outfile, $
;;            xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;            xtitle='i!DPS1!N-z!DPS1!N', ytitle='i!DPS1!N-y!DPS1!N', $
;;            win=5, cats=cats

; Output #6b: eye, i-y v. i-z, incl. all objects
;; xmin = -2.0
;; ymin = -2.0
;; outfile = '~/Astro/699-1/paper/objplots/iyiz_eye_all'
;; lteyeplot, list.iz, list.iy, list.type, list.izerr, list.iyerr, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='i!DPS1!N-z!DPS1!N', ytitle='i!DPS1!N-y!DPS1!N', $
;;              win=15

; Output #7a: eye, W1-W2 v. y-W1
;; outfile = '~/Astro/699-1/paper/objplots/w1w2yw1_eye'
;; xmin = 2.8
;; xmax = 6.3
;; ymin = 0.2
;; ymax = 2.5
;; segment.x = [ [3.0, 3.0], [3.0, 6.3] ]
;; segment.y = [ [0.4, 2.5], [0.4, 0.4] ]
;; lteyeplot, list.yw1, list.w1w2, list.type, list.yw1err, list.w1w2err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='y!DPS1!N-W1', ytitle='W1-W2', $
;;              win=6, cats=cats, /right

; Output #7b: eye, W1-W2 v. y-W1, incl. all objects
;; ymin = 0.0
;; outfile = '~/Astro/699-1/paper/objplots/w1w2yw1_eye_all'
;; lteyeplot, list.yw1, list.w1w2, list.type, list.yw1err, list.w1w2err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='y!DPS1!N-W1', ytitle='W1-W2', $
;;              win=16, /right

; Output #8a: eye, W2-W3 v. W1-W2
;; outfile = '~/Astro/699-1/paper/objplots/w2w3w1w2_eye'
;; xmin = 0.2
;; xmax = 2.5
;; ymin = 0
;; ymax = 3.2
;; segment.x = [ [0.4, 0.4], [0.4, 2.5] ]
;; segment.y = [ [0.0, 2.5], [2.5, 2.5] ]
;; type5 = where(list.type eq 5)
;; list[type5].w2w3err = 0.
;; lteyeplot, list.w1w2, list.w2w3, list.type, list.w1w2err, list.w2w3err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='W1-W2', ytitle='W2-W3', $
;;              win=7, cats=cats, /down, /right, /bottom

;; ; Output #8b: eye, W2-W3 v. W1-W2, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/w2w3w1w2_eye_all'
;; xmin = 0.1
;; ymin = -0.5
;; ymax = 4.5
;; segment.y = [ [-0.5, 2.5], [2.5, 2.5] ]
;; lteyeplot, list.w1w2, list.w2w3, list.type, list.w1w2err, list.w2w3err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='W1-W2', ytitle='W2-W3', $
;;              win=17, /down, /right, /bottom

; Output #9a: Histogram W1-W2
if keyword_set(ps) then begin
    fpath = '~/Astro/699-1/paper/objplots/W1W2hist'
    ps_open, fpath, /color, /en, thick=4
endif else window, 8, retain=2, xsize=800, ysize=600

plothist, list.w1w2, xtitle='W1-W2', ytitle='Count', charsize=1.6, color=0, backg=1, bin=0.1, $
          xrange=[0.2, 2.0]

; Add a vertical dashed line
vline, 0.4, color=0, lines=2

if keyword_set(ps) then ps_close

; Output #9b: Histogram of spectral types
restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'
list = temporary(wise_new)
list = list[where(list.spt gt 0 and list.spt lt 30)]

if keyword_set(ps) then begin
    fpath = '~/Astro/699-1/paper/spthist'
    ps_open, fpath, /color, /en, thick=4
endif else window, 8, retain=2, xsize=800, ysize=600

sptaxis = ['L0','L5','T0','T5',' ']
sptval = [10,15,20,25,30]
plothist, list.spt, xtitle='Spectral Type', ytitle='Number', charsize=1.6, color=0, backg=1, bin=1, $
          xrange=[10, 30], xtickname=sptaxis, xtickv=spt

; Add vertical dashed lines
vline, 16, color=0, lines=2
vline, 24, color=0, lines=2

if keyword_set(ps) then ps_close

;-----------------------------------------------------------

; Load the data table known_eye_disc.sav.
restore, '~/Astro/699-1/lttrans/plotting/known_eye_disc.sav', description=sname
rename = 'list = temporary('+sname+')'
dummy = execute(rename)

cats = [3, 4, 5, 6]

; Output #10a: known+eyeballed+discoveries, dec v. ra
outfile = '~/Astro/699-1/paper/objplots/radec_ked'
xmin = min(list.ra) - 2
xmax = max(list.ra) + 2
ymin = min(list.dec) - 2
ymax = max(list.dec) + 2
ltkedplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
           xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
           xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
           win=20, cats=cats, /noerror, /noleg

; Output #10b: known+eyeballed+discoveries, dec v. ra, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/radec_ked_all'
;; ltkedplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
;;            xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
;;            xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
;;            win=24, /noleg, /noerror

; Output #11a: known+eyeballed+discoveries, i-y v. i-z
;; outfile = '~/Astro/699-1/paper/objplots/iyiz_ked_bad'
;; xmin = -1.5
;; xmax = 3.6
;; ymin = -1.5
;; ymax = 4.5
;; segment.x = [ [1.8, 1.8], [1.8, 3.6] ]
;; segment.y = [ [2.8, 4.5], [2.8, 2.8] ]
;; ltkedplot, list.iz, list.iy, list.type, list.izerr, list.iyerr, ps=ps, outfile=outfile, $
;;            xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;            xtitle='i!DPS1!N-z!DPS1!N', ytitle='i!DPS1!N-y!DPS1!N', $
;;            win=21, cats=cats

;; ; Output #11b: known+eyeballed+discoveries, i-y v. i-z, incl. all objects
;; xmin = -2.0
;; ymin = -2.0
;; outfile = '~/Astro/699-1/paper/objplots/iyiz_ked_all'
;; ltkedplot, list.iz, list.iy, list.type, list.izerr, list.iyerr, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='i!DPS1!N-z!DPS1!N', ytitle='i!DPS1!N-y!DPS1!N', $
;;              win=25

; Output #11.5a: known, W1-W2 v. z-y
outfile = '~/Astro/699-1/paper/objplots/w1w2zy_ked'
xmin = -1.2
xmax = 3.5
ymin = -0.1
ymax = 2.0
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]
segment.x = [ [0.6, 0.6],  [0.6, xmax] ]
segment.y = [ [0.4, ymax], [0.4, 0.4]  ]
ltkedplot, list.zy, list.w1w2, list.type, list.zyerr, list.w1w2err, ps=ps, outfile=outfile, $
             xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
             xtitle='z!DPS1!N-y!DPS1!N', ytitle='W1-W2', $
             win=29, cats=cats, /right

; Output #12a: known+eyeballed+discoveries, W1-W2 v. y-W1
outfile = '~/Astro/699-1/paper/objplots/w1w2yw1_ked'
xmin = 2.85
xmax = 6.3
ymin = 0
ymax = 2.5
segment.x = [ [3.0, 3.0], [3.0, 6.3] ]
segment.y = [ [0.4, 2.5], [0.4, 0.4] ]
ltkedplot, list.yw1, list.w1w2, list.type, list.yw1err, list.w1w2err, ps=ps, outfile=outfile, $
             xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
             xtitle='y!DPS1!N-W1', ytitle='W1-W2', $
             win=22, cats=cats, /right, /noerror

; Output #12b: known+eyeballed+discoveries, W1-W2 v. y-W1, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/w1w2yw1_ked_all'
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 3)
;; segment.color = [0, 0, 4]
;; segment.style = [2, 2, 3]
;; segment.thick = [5, 5, 8]
;; segment.x = [ [3.0, 3.0],  [3.0, xmax], [xmin,      (6-3.*ymin) < xmax] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4],  [2-xmin/3., ymin > (2-xmax/3.)] ]
;; ltkedplot, list.yw1, list.w1w2, list.type, list.yw1err, list.w1w2err, ps=ps, outfile=outfile, $
;;            xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;; ;           cats=[0,3,4,5], errorv=[0,1,1], $
;;            xtitle='y!DPS1!N-W1', ytitle='W1-W2', $
;;            win=26, /right

; Output #13a: known+eyeballed+discoveries, W2-W3 v. W1-W2
;; outfile = '~/Astro/699-1/paper/objplots/w2w3w1w2_ked'
xmin = 0
xmax = 2.5
ymin = -0.5
ymax = 4
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]
segment.x = [ [0.4, 0.4], [0.4, 2.5] ]
segment.y = [ [-0.5, 2.5], [2.5, 2.5] ]
noerr = where(finite(list.w2w3err) eq 0)
list[noerr].w2w3err = 0.
;; ltkedplot, list.w1w2, list.w2w3, list.type, list.w1w2err, list.w2w3err, ps=ps, outfile=outfile, $
;;              xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
;;              xtitle='W1-W2', ytitle='W2-W3', $
;;              win=23, cats=cats, /down, /right, /bottom

; Output #13b: known+eyeballed+discoveries, W2-W3 v. W1-W2, incl. all objects
outfile = '~/Astro/699-1/paper/objplots/w2w3w1w2_ked_all'
ymax = 4.5
ltkedplot, list.w1w2, list.w2w3, list.type, list.w1w2err, list.w2w3err, ps=ps, outfile=outfile, $
             xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
             xtitle='W1-W2', ytitle='W2-W3', $
             win=27, /down, /right, /bottom


END
