;PRO MAKEMEGADISC

;  Wrapper for megadiscplot.pro
;
;  HISTORY
;  Written by Will Best (IfA), 10/08/2012
;
;  USE
;      megadiscplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]

; L-T Transition: M0-M9.5, L0-L5.5, L6-L7.5, L8-L9.5, T0-T3.5, T4-T9.5
cats = [0, 10, 16, 18, 20, 24, 30]
;cats = [cats, cats[1:n_elements(cats)-1]+30]
cset = [15, 12, 2, 3, 4, 6]       ; new color scheme
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 1)
;; segment.color = [4]
;; segment.style = [3]
;; segment.thick = [8]
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]
verline = replicate({val:0., color:0, style:0, thick:0.}, 1)
verline.color = 0
verline.style = 2
verline.thick = 5
horline = verline

;; ; i-y vs. i-z
;; segment.x = [ [1.8, 1.8], [1.8, 3.2] ]
;; segment.y = [ [2.8, 4.5], [2.8, 2.8] ]
;; megadiscplot, 'i', 'z', 'i', 'y', win=0, cset=cset, cats=cats, xmin=0.6, xmax=3.2, ymin=1.5, ymax=4.5, /fullsym, segment=segment, /ps, outfile='~/Astro/699-1/paper/iyiz'
;; ; W1-W2 vs. z-y
;; segment.x = [ [0.6, 0.6], [0.6, 2.5] ]
;; segment.y = [ [0.4, 3.5], [0.4, 0.4] ]
;; megadiscplot, 'z', 'y', 'W1', 'W2', win=1, cset=cset, cats=cats, xmin=-0.5, xmax=2.5, segment=segment, /ps, outfile='~/Astro/699-1/paper/w1w2zy'
;; ; W1 vs. W1-W2
;; verline.val = 0.4
;; megadiscplot, 'W1', 'W2', 'W1', win=2, cset=cset, cats=cats, xmin=-1, xmax=3.5, verline=verline, /right, /ps, outfile='~/Astro/699-1/paper/w1w1w2'

; W1-W2 vs. y-W1
;; xmin=1.8
;; xmax=6.5
;; ymin=-0.4
;; ymax=3.2
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 3)
;; segment.color = [0, 0, 4]
;; segment.style = [2, 2, 3]
;; segment.thick = [5, 5, 8]
;; segment.x = [ [3.0, 3.0],  [3.0, xmax], [xmin,      (6-3.*ymin) < xmax] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4],  [2-xmin/3., ymin > (2-xmax/3.)] ]
;; segment.x = [ [3.0, 3.0],  [3.0, xmax] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4]  ]
;; ;; segment.x = [xmin,      (6-3.*ymin) < xmax]
;; ;; segment.y = [2-xmin/3., ymin > (2-xmax/3.)]
;; megadiscplot, 'y', 'W1', 'W1', 'W2', win=3, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /fullsym, /right, /ps, outfile='~/Astro/699-1/paper/w1w2yw1'
; W1-W2 vs. i-z
;; segment.x = [ [1.8, 1.8], [1.8, 3.2] ]
;; segment.y = [ [0.4, 0.7], [0.4, 0.4] ]
;; megadiscplot, 'i', 'z', 'W1', 'W2', win=4, cset=cset, cats=cats, xmin=-0.2, xmax=3.2, ymin=0.01, ymax=0.7, segment=segment;, /ps, outfile='~/Astro/699-1/paper/w1w2iz'
;; ; W1-W2 vs. z-W1
;; horline.val = 0.4
;; megadiscplot, 'z', 'W1', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=1, xmax=8, horline=horline;, /ps, outfile='~/Astro/699-1/paper/w1w2zw1'

; i-y vs. i-z
;megadiscplot, 'i', 'z', 'i', 'y', win=0, cset=cset, cats=cats, xmin=0.1, xmax=3.2, ymin=0.4, ymax=4.5, /fullsym;, /ps
; i-y vs. z-y
megadiscplot, 'z', 'y', 'i', 'y', cats=cats, xmin=0.5, xmax=1.5, ymin=1, /noerror;, /ps
; z-y vs. SpT
;megadiscplot, 'z', 'y', cats=cats, minspt=3, /fullsym;, /ps
; y-W1 vs. SpT
;megadiscplot, 'y', 'W1', cats=cats, errorv=[0,0,1,1,1,0], /noerror;, /ps
; y-W1 vs. z-y
;megadiscplot, 'z', 'y', 'y', 'W1', cats=cats, xmin=-1, ymax=7;, /ps
; J-H vs. SpT
;megadiscplot, 'J', 'H', cats=cats, /fullsym, minspt=6, maxspt=28, /right, xtickname=['M5','L0','L5','T0','T5','Y0'], xtickv=[5,10,15,20,25,30], legsize=0.9, /ps, outfile='~/Astro/699-1/paper/jhspt'
          
; J-H vs. y-J
;megadiscplot, 'y', 'J', 'J', 'H', cats=cats, xmin=1, xmax=3.5;, /fullsym, /ps
; J-H vs. J-K
;megadiscplot, 'J', 'K', 'J', 'H', cats=cats, xmin=-1, xmax=3, win=0;, /noerror;, /fullsym, /ps
; J-K vs. Spt
;megadiscplot, 'J', 'K', cats=cats, win=2;, /fullsym, /ps
; H-K vs. J-H
;megadiscplot, 'J', 'H', 'H', 'K', cats=cats, xmin=-0.7, xmax=1.8, ymin=-1, /noerror;, /fullsym, /ps
; W1 vs. W1-W2
;megadiscplot, 'W1', 'W2', 'W1', win=2, cset=cset, cats=cats, xmin=-1, xmax=3.5, /right;, /ps
; W1-W2 vs. i-z
;megadiscplot, 'i', 'z', 'W1', 'W2', win=4, cset=cset, cats=cats, xmin=-0.2, xmax=3.2, ymin=0.01, ymax=0.7;, /ps
; W1-W2 vs. z-y
;megadiscplot, 'z', 'y', 'W1', 'W2', win=1, cset=cset, cats=cats, xmin=-0.5, xmax=2.5;, /ps
; W1-W2 vs. z-W1
;megadiscplot, 'z', 'W1', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=1, xmax=8;, /ps
; W1-W2 vs. y-J
;megadiscplot, 'y', 'J', 'W1', 'W2', win=2, cset=cset, cats=cats, xmin=1, xmax=3.5;, /ps
; W1-W2 vs. y-W1
;megadiscplot, 'y', 'W1', 'W1', 'W2', win=3, cset=cset, cats=cats, xmin=1, xmax=7;, /ps
; W1-W2 vs. J-K
;megadiscplot, 'J', 'K', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-1, xmax=3;, /ps
; W1-W2 vs. H-K
;megadiscplot, 'H', 'K', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-1, xmax=2, /right;, /ps
; W2-W3 vs. SpT
;megadiscplot, 'W2', 'W3', cats=cats, minspt=3;, /ps
; W2-W3 vs. z-y
;megadiscplot, 'z', 'y', 'W2', 'W3', cats=cats, xmin=-0.3, xmax=2;, /noerror;, /ps
; W2-W3 vs. y-W1
;megadiscplot, 'y', 'W1', 'W2', 'W3', cats=cats, xmin=1.5, xmax=9.5, /right, /noerror;, /ps
; W2-W3 vs. W1-W2
;megadiscplot, 'W1', 'W2', 'W2', 'W3', cats=cats, xmin=-.5, xmax=3.2;, /ps


;  USE
;      megadiscplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      F1, F2, [F3, F4] - Filters whose colors or magnitudes are to be plotted.
;         If four filters are entered, a plot of F3-F4 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If three filters are entered, a plot of F3 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If two filters are entered, a plot of F1-F2 (y-axis) vs. SpT (x-axis)
;         will be created.
;         If fewer than two filters are entered, the syntax and a list of
;         available filters are printed, and the program quits.
;
;  OPTIONAL KEYWORDS
;      CSET - Six-element vector containing the colors to be plotted.
;             Color table is set by lincolr_wb
;             Default: [2, 8, 3, 5, 4, 6]
;      CATS - Seven-element vector containing spectral types for the boundaries
;             of the plotted colors.  Default: [0, 5, 10, 15, 20, 25, 30]
;             The first element cannot be less than 0.
;             The last element cannot be greater than 30.
;      ERRORV - Six-element vector containing 0's and 1's.
;               Categories corresponding to 0's are plotted without error bars;
;               categories corresponding to 1's are plotted with error bars.
;               Default: all categories plotted with error bars.
;               If NOERROR is set, ERRORV is ignored.
;      FULLSYM - Force all symbols to be filled, irrespective of spectral type.
;      MINSPT - Minimum spectral type to fit.
;          Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;                 (no data exists for types later than T9.)
;          Default: 0 (M0)
;      MAXSPT - Maximum spectral type to fit (same scale as MINSPT).
;          Default: 29 (T9).
;      NOERROR - Don't plot the error bars.  Overrides ERRORVEC.
;      OUTFILE - Path for postscript output
;                Default: ~/Astro/colorplot1.ps
;      PS - Output to postscript
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;
;  Display colors:
;      Old scheme:
;      M0-M4: BRIGHT GREEN
;      M5-M9: ORANGE
;      L0-L4: RED
;      L5-L9: MAGENTA
;      T0-T4: BLUE
;      T5-T9: CYAN
;
;      New scheme:
;      M0-M4: LIGHT GREY diamond
;      M5-M9: GREY diamond
;      L0-L4: RED circle
;      L5-L9: BRIGHT GREEN square
;      T0-T4: BLUE triangle
;      T5-T9: CYAN triangle
;

END
