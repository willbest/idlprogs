;PRO MAKEMEGA

;  Wrapper for megaplot.pro
;
;  HISTORY
;  Written by Will Best (IfA), 04/04/2012
;
;  USE
;      megaplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]

; L-T Transition: M0-M9.5, L0-L5.5, L6-L7.5, L8-L9.5, T0-T3.5, T4-T9.5
cats = [7, 10, 16, 18, 20, 24, 30]
;cset = [15, 12, 2, 3, 4, 6]       ; new color scheme
cset = [15, 12, 11, 2, 4, 6]       ; new color scheme
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 1)
;; segment.color = [4]
;; segment.style = [3]
;; segment.thick = [8]
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]
verline = replicate({val:0., color:0, style:0, thick:0.}, 1)
verline.color = 0
verline.style = 2
verline.thick = 5
horline = verline

;; ; i-y vs. i-z
;; segment.x = [ [1.8, 1.8], [1.8, 3.2] ]
;; segment.y = [ [2.8, 4.5], [2.8, 2.8] ]
;; megaplot, 'i', 'z', 'i', 'y', win=0, cset=cset, cats=cats, xmin=0.6, xmax=3.2, ymin=1.5, ymax=4.5, /fullsym, segment=segment, /ps, outfile='~/Astro/699-1/paper/iyiz'
;; ; W1 vs. W1-W2
;; verline.val = 0.4
;; megaplot, 'W1', 'W2', 'W1', win=2, cset=cset, cats=cats, xmin=-1, xmax=3.5, verline=verline, /right, /ps, outfile='~/Astro/699-1/paper/w1w1w2'

; W1-W2 vs. y-W1
;; xmin=1.8
;; xmax=6.5
;; ymin=-0.4
;; ymax=3.2
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 3)
;; segment.color = [0, 0, 4]
;; segment.style = [2, 2, 3]
;; segment.thick = [5, 5, 8]
;; segment.x = [ [3.0, 3.0],  [3.0, xmax], [xmin,      (6-3.*ymin) < xmax] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4],  [2-xmin/3., ymin > (2-xmax/3.)] ]
;; ;; segment.x = [ [3.0, 3.0],  [3.0, xmax] ]
;; ;; segment.y = [ [0.4, ymax], [0.4, 0.4]  ]
;; ;; segment.x = [xmin,      (6-3.*ymin) < xmax]
;; ;; segment.y = [2-xmin/3., ymin > (2-xmax/3.)]
;; megaplot, 'y', 'W1', 'W1', 'W2', win=2, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /fullsym, /right;, /ps, outfile='~/Astro/699-1/paper/w1w2yw1'
; W1-W2 vs. i-z
;; segment.x = [ [1.8, 1.8], [1.8, 3.2] ]
;; segment.y = [ [0.4, 0.7], [0.4, 0.4] ]
;; megaplot, 'i', 'z', 'W1', 'W2', win=4, cset=cset, cats=cats, xmin=-0.2, xmax=3.2, ymin=0.01, ymax=0.7, segment=segment;, /ps, outfile='~/Astro/699-1/paper/w1w2iz'
;; ; W1-W2 vs. z-W1
;; horline.val = 0.4
;; megaplot, 'z', 'W1', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=1, xmax=8, horline=horline;, /ps, outfile='~/Astro/699-1/paper/w1w2zw1'

; i-y vs. i-z
;megaplot, 'i', 'z', 'i', 'y', win=0, cset=cset, cats=cats, xmin=0.1, xmax=3.2, ymin=0.4, ymax=4.5, /fullsym;, /ps
; i-y vs. z-y
;megaplot, 'z', 'y', 'i', 'y', cats=cats, xmin=0.5, xmax=1.5, ymin=1, /noerror;, /ps
; z-y vs. SpT
;megaplot, 'z', 'y', cats=cats, minspt=3, /fullsym;, /ps
; z-J vs. SpT
;megaplot, 'z', 'J', cats=cats, minspt=3, ymin=2, ymax=5, /fullsym;, /ps
; y-W1 vs. SpT
;megaplot, 'y', 'W1', cats=cats, errorv=[0,0,1,1,1,0], /noerror;, /ps
; y-W1 vs. z-y
;megaplot, 'z', 'y', 'y', 'W1', cats=cats, xmin=-1, ymax=7;, /ps
; y-W1 vs. y-H
;megaplot, 'y', 'H', 'y', 'W1', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=2, xmax=4, ymin=2.5, ymax=6;, /ps
; J vs. J-W1
;megaplot, 'JM', 'W1', 'JM', win=1, cset=cset, cats=cats, xmin=-0.5, xmax=3.7, ymin=11, ymax=18;, /ps, outfile='~/Desktop/J.Jw1'
; J-W1 vs. SpT
;megaplot, 'JM', 'W1', cats=cats, /fullsym, minspt=5, ymin=-0.5, ymax=3.7, /ps, outfile='~/Desktop/Jw1.spt_known'
; J-H vs. SpT
;megaplot, 'J', 'H', cats=cats, /fullsym, minspt=6, maxspt=28, /right, xtickname=['M5','L0','L5','T0','T5','Y0'], xtickv=[5,10,15,20,25,30], legsize=0.9, /ps, outfile='~/Astro/699-1/paper/jhspt'
          
; i-y vs. i-z
;; cats = [7, 10, 16, 18, 20]     ; only four categories in this plot (no T dwarfs with i photometry)
;; cset = [15, 12, 11, 2]         ; color scheme
;; nsob = 5
;; sob = replicate({x:0., y:0., xerr:0., yerr:0., color:0, symbol:[0,0], size:0., nir:0, $
;;                 xlab:0., ylab:0., label:'', labsize:0., labcol:0}, nsob)
;; sob.x = [3.11, 2.48, 3.19, 2.04, 3.66]
;; sob.y = [4.30, 3.69, 4.43, 3.24, 5.19]
;; sob.xerr = [0.12, 0.12, 0.08, 0.04, 0.14]
;; sob.yerr = [0.12, 0.12, 0.08, 0.04, 0.14]
;; sob.color = intarr(nsob)+3
;; sob.symbol = cmreplicate([45,46], nsob)
;; sob.size = fltarr(nsob)+3
;; sob.nir = intarr(nsob)+1
;; sob.xlab = [0., 0., 2.86, 0., 2.59]
;; sob.ylab = [0., 0., 3.79, 0., 4.90]
;; sob.label = ['', '', 'PSO J140.2+45 (L9.5)', '', 'PSO J307.6+07 (T1.5)']
;; sob.labsize = fltarr(nsob)+1;[1., 1.]
;; sob.labcol = fltarr(nsob)+3
;; xmin=0.6
;; xmax=3.9
;; ymin=1.5
;; ymax=5.4
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 4)
;; segment.color = [0, 0, 3, 3]
;; segment.style = [1, 1, 0, 0]
;; segment.thick = [5, 5, 6, 6]
;; segment.x = [ [1.8, 1.8],  [1.8, xmax], [3.40, 3.25], [3.46, 3.62] ]
;; segment.y = [ [2.8, ymax], [2.8, 2.8],  [3.86, 4.37], [4.96, 5.12] ]
;; megaplot, 'i', 'z', 'i', 'y', win=0, cset=cset, cats=cats, sob=sob, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /ps

; J-H vs. y-J
;; nsob = 7
;; sob = replicate({x:0., y:0., xerr:0., yerr:0., color:0, symbol:[0,0], size:0., nir:0, $
;;                 xlab:0., ylab:0., label:'', labsize:0., labcol:0}, nsob)
;; sob.x = [2.06, 1.99, 2.21, 2.16, 2.32, 2.21, 2.67]
;; sob.y = [1.18, 1.06, 0.97, 1.02, 0.86, 0.79, 0.10]
;; sob.xerr = [0.04, 0.05, 0.04, 0.06, 0.06, 0.03, 0.04]
;; sob.yerr = [0.06, 0.07, 0.06, 0.08, 0.08, 0.04, 0.06]
;; sob.color = intarr(nsob)+3
;; sob.symbol = cmreplicate([45,46], nsob)
;; sob.size = fltarr(nsob)+3
;; sob.nir = intarr(nsob)+1
;; sob.xlab = [0., 0., 1.58, 0., 0., 1.38, 0.];sob.x - 1.25
;; sob.ylab = [0., 0., 1.70, 0., 0., 0.32, 0.];sob.y - 0.07
;; sob.label = ['', '', 'PSO J140.2+45 (L9.5)', '', '', 'PSO J307.6+07 (T1.5)', '']
;; sob.labsize = fltarr(nsob)+1
;; sob.labcol = intarr(nsob)+3
;; xmin=1
;; xmax=3.5
;; ymin=-0.8
;; ymax=2.0
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 3)
;; segment.color = [0, 3, 3]
;; segment.style = [2, 0, 0]
;; segment.thick = [5, 6, 6]
;; segment.x = [ [1.8, 1.8],   [2.02, 2.18], [1.86, 1.96] ]
;; segment.y = [ [ymin, ymax], [0.39, 0.74], [1.64, 1.12] ]
;; megaplot, 'y', 'J', 'J', 'H', cset=cset, cats=cats, sob=sob, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /ps

; J-H vs. J-K
;megaplot, 'JM', 'KM', 'JM', 'HM', cats=cats, xmin=-1, xmax=3, win=0;, /noerror;, /fullsym, /ps
; J-K vs. Spt
;megaplot, 'JM', 'KM', win=2, cats=cats;, /fullsym, /ps
; J-K vs. y-J
;megaplot, 'y', 'JM', 'JM', 'KM', cats=cats;, /fullsym, /ps
; H-K vs. J-H
;; sob = replicate({x:0., y:0., xerr:0., yerr:0., color:0, symbol:[0,0], size:0., nir:0, $
;;                 xlab:0., ylab:0., label:'', labsize:0.}, 2)
;; sob.x = [0.79, 0.10]
;; sob.y = [0.12, 0.04]
;; sob.xerr = [0.04, 0.06]
;; sob.yerr = [0.05, 0.11]
;; sob.color = [4, 6]
;; sob.symbol = [ [45,46], [45,46] ]
;; sob.size = [3.0, 3.0]
;; sob.nir = [1, 1]
;; sob.xlab = sob.x;[6.3, 6.3]
;; sob.ylab = sob.y;[1.45, 1.2]
;; sob.label = ['PSO J307.6+07.8 (T1.5)', 'PSO J339.0+51.0 (T5)']
;; sob.labsize = [1., 1.]
;; megaplot, 'J', 'H', 'H', 'K', cset=cset, cats=cats, sob=sob, xmin=-0.7, xmax=1.8, ymin=-1, /ps
; K-W1 vs. SpT
;megaplot, 'K', 'W1', cats=cats, minspt=5, ymax=1.7;, /fullsym, /ps
; K-W1 vs. y-W1
;megaplot, 'y', 'W1', 'KM', 'W1', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], ymin=-1, ymax=1.7, /right;, /ps
; W1 vs. W1-W2
;megaplot, 'W1', 'W2', 'W1', win=2, cset=cset, cats=cats, xmin=-1, xmax=3.5, /right;, /ps
megaplot, 'W1', 'W2', 'W1', win=2, cset=cset, cats=cats, xmin=0.38, xmax=2.12, ymin=11.8, ymax=17, /right;, /ps
; W1-W2 vs. i-z
;megaplot, 'i', 'z', 'W1', 'W2', win=4, cset=cset, cats=cats, xmin=-0.2, xmax=3.2, ymin=0.01, ymax=0.7;, /ps

; W1-W2 vs. z-y
;; xmin=-0.5
;; xmax=2.5
;; segment.style = [2, 1]
;; segment.thick = [5, 5]
;; segment.x = [ [0.6, 0.6], [0.6, xmax] ]
;; segment.y = [ [0.4, 3.5], [0.4, 0.4] ]
;; megaplot, 'z', 'y', 'W1', 'W2', win=1, cset=cset, cats=cats, xmin=xmin, xmax=xmax, segment=segment, /ps
; W1-W2 vs. z-W1
;megaplot, 'z', 'W1', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=1, xmax=8;, /ps
; W1-W2 vs. y-J
;megaplot, 'y', 'J', 'W1', 'W2', win=2, cset=cset, cats=cats, xmin=1, xmax=3.5;, /ps
; W1-W2 vs. y-H
;megaplot, 'y', 'H', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=2, xmax=5, /right;, /ps

; W1-W2 vs. y-W1
;; nsob = 7
;; sob = replicate({x:0., y:0., xerr:0., yerr:0., color:0, symbol:[0,0], size:0., nir:0, $
;;                 xlab:0., ylab:0., label:'', labsize:0., labcol:0}, nsob)
;; sob.x = [4.60, 4.16, 4.50, 4.51, 4.17, 3.48, 3.42]
;; sob.y = [0.57, 0.67, 0.47, 0.68, 0.56, 0.83, 1.35]
;; sob.xerr = [0.02, 0.02, 0.03, 0.02, 0.03, 0.03, 0.03]
;; sob.yerr = [0.03, 0.04, 0.03, 0.03, 0.04, 0.04, 0.04]
;; sob.color = intarr(nsob)+3;[4, 6]
;; sob.symbol = cmreplicate([45,46], nsob);[ [45,46], [45,46] ]
;; sob.size = fltarr(nsob)+3;[3.0, 3.0]
;; sob.nir = intarr(nsob)+1
;; sob.xlab = [0., 0., 4.54, 0., 0., 1.94, 0.];sob.x - 1.25
;; sob.ylab = [0., 0., 0.90, 0., 0., 0.60, 0.];sob.y - 0.07
;; ;sob.label = ['PSO J007.7+57 (L9)', 'PSO J140.2+45 (L9.5)', 'PSO J282.7+59 (L9.5)', 'PSO J103.0+41 (T0)', 'PSO J272.4-04 (T1)', 'PSO J307.6+07 (T1.5)', 'PSO J339.0+51 (T5)']
;; sob.label = ['', '', 'PSO J140.2+45 (L9.5)', '', '', 'PSO J307.6+07 (T1.5)', '']
;; sob.labsize = fltarr(nsob)+1;[1., 1.]
;; sob.labcol = fltarr(nsob)+3
;; xmin=1.8
;; xmax=6.5
;; ymin=-0.4
;; ymax=3.2
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 4)
;; segment.color = [0, 0, 3, 3]
;; segment.style = [2, 2, 0, 0]
;; segment.thick = [5, 5, 6, 6]
;; segment.x = [ [3.0, 3.0],  [3.0, xmax], [4.24, 4.60], [3.16, 3.41] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4],  [0.72, 0.90], [0.60, 0.75] ]
;; megaplot, 'y', 'W1', 'W1', 'W2', win=0, cset=cset, cats=cats, sob=sob, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, /right, segment=segment, /ps

; W1-W2 vs. J-H
;megaplot, 'J', 'H', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-1, xmax=2, /right;, /ps
; W1-W2 vs. J-K
;megaplot, 'J', 'K', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-1, xmax=3;, /ps
; W1-W2 vs. J-W1
;megaplot, 'J', 'W1', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-0.5, xmax=4.6, /right, /ps
; W1-W2 vs. H-K
;megaplot, 'H', 'K', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-1, xmax=2, /right;, /ps
; W1-W2 vs. K-W1
;megaplot, 'K', 'W1', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-0.6, xmax=1.7, /right, /ps
; W2-W3 vs. SpT
;megaplot, 'W2', 'W3', cats=cats, minspt=3;, /ps

; W2-W3 vs. z-y
;; nsob = 7
;; sob = replicate({x:0., y:0., xerr:0., yerr:0., color:0, symbol:[0,0], size:0., nir:0, $
;;                 xlab:0., ylab:0., label:'', labsize:0., labcol:0}, nsob)
;; sob.x = [1.22, 1.21, 1.29, 1.34, 1.20, 1.54, 1.87]
;; sob.y = [0.54, 0.64, 1.10, 0.35, 0.95, 1.16, 1.47]
;; sob.xerr = [0.02, 0.02, 0.02, 0.03, 0.02, 0.01, 0.05]
;; sob.yerr = [0.11, 0.26, 0.17, 0.48, 0.07, 0.11, 0.09]
;; sob.color = intarr(nsob)+3
;; sob.symbol = cmreplicate([45,46], nsob)
;; sob.size = fltarr(nsob)+3
;; sob.nir = intarr(nsob)+1
;; sob.xlab = [0., 0., 1.16, 0., 0., 1.36, 0.]
;; sob.ylab = [0., 0., 2.05, 0., 0., 0.40, 0.]
;; sob.label = ['', '', 'PSO J140.2+45 (L9.5)', '', '', 'PSO J307.6+07 (T1.5)', '']
;; sob.labsize = fltarr(nsob)+1;[1., 1.]
;; sob.labcol = fltarr(nsob)+3
;; xmin=-0.2
;; xmax=2.0
;; ymin=-0.9
;; ymax=2.8
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 4)
;; segment.color = [0, 0, 3, 3]
;; segment.style = [1, 2, 0, 0]
;; segment.thick = [5, 5, 6, 6]
;; segment.x = [ [0.6, 0.6],  [0.6, xmax], [1.36, 1.32], [1.49, 1.51] ]
;; segment.y = [ [ymin, 2.5], [2.5, 2.5],  [1.98, 1.21], [0.50, 1.07] ]
;; megaplot, 'z', 'y', 'W2', 'W3', win=0, cset=cset, cats=cats, sob=sob, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, /bottom, segment=segment, /ps

; W2-W3 vs. z-y
;megaplot, 'z', 'y', 'W2', 'W3', cats=cats, xmin=-0.3, xmax=2;, /noerror;, /ps
; W2-W3 vs. y-W1
;megaplot, 'y', 'W1', 'W2', 'W3', cats=cats, xmin=1.5, xmax=9.5, /right, /noerror;, /ps
; W2-W3 vs. J-K
;megaplot, 'JM', 'KM', 'W2', 'W3', cats=cats, xmin=-1, xmax=3, /right;, /noerror;, /ps
; W2-W3 vs. W1-W2
;megaplot, 'W1', 'W2', 'W2', 'W3', cats=cats, xmin=-.5, xmax=3.2;, /ps


;  USE
;      megaplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      F1, F2, [F3, F4] - Filters whose colors or magnitudes are to be plotted.
;         If four filters are entered, a plot of F3-F4 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If three filters are entered, a plot of F3 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If two filters are entered, a plot of F1-F2 (y-axis) vs. SpT (x-axis)
;         will be created.
;         If fewer than two filters are entered, the syntax and a list of
;         available filters are printed, and the program quits.
;
;  OPTIONAL KEYWORDS
;      CSET - Six-element vector containing the colors to be plotted.
;             Color table is set by lincolr_wb
;             Default: [2, 8, 3, 5, 4, 6]
;      CATS - Seven-element vector containing spectral types for the boundaries
;             of the plotted colors.  Default: [0, 5, 10, 15, 20, 25, 30]
;             The first element cannot be less than 0.
;             The last element cannot be greater than 30.
;      ERRORV - Six-element vector containing 0's and 1's.
;               Categories corresponding to 0's are plotted without error bars;
;               categories corresponding to 1's are plotted with error bars.
;               Default: all categories plotted with error bars.
;               If NOERROR is set, ERRORV is ignored.
;      FULLSYM - Force all symbols to be filled, irrespective of spectral type.
;      MINSPT - Minimum spectral type to fit.
;          Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;                 (no data exists for types later than T9.)
;          Default: 0 (M0)
;      MAXSPT - Maximum spectral type to fit (same scale as MINSPT).
;          Default: 29 (T9).
;      NOERROR - Don't plot the error bars.  Overrides ERRORVEC.
;      OUTFILE - Path for postscript output
;                Default: ~/Astro/colorplot1.ps
;      PS - Output to postscript
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;
;  Display colors:
;      Old scheme:
;      M0-M4: BRIGHT GREEN
;      M5-M9: ORANGE
;      L0-L4: RED
;      L5-L9: MAGENTA
;      T0-T4: BLUE
;      T5-T9: CYAN
;
;      New scheme:
;      M0-M4: LIGHT GREY diamond
;      M5-M9: GREY diamond
;      L0-L4: RED circle
;      L5-L9: BRIGHT GREEN square
;      T0-T4: BLUE triangle
;      T5-T9: CYAN triangle
;

END
