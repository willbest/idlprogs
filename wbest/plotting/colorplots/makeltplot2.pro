ps=1
;PRO MAKELTPLOT2, PS=ps

;  Wrapper for ltknownplot.pro and lteyeplot.pro
;
;  HISTORY
;  Written by Will Best (IfA), 08/13/2012
;
;  USE
;      megaplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]
;  KEYWORDS
;      PS - Output to postscript.
;

segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]

; Load the data table known_eye_dis2c.sav.
restore, '~/Astro/699-1/lttrans/plotting/known_eye_disc2.sav', description=sname
rename = 'list = temporary('+sname+')'
dummy = execute(rename)

cats = [3, 4, 5, 6]

; Output #1: known+eyeballed+discoveries, dec v. ra
;; outfile = '~/Astro/699-1/paper/objplots/radec_ked'
;; xmin = min(list.ra) - 2
;; xmax = max(list.ra) + 2
;; ymin = min(list.dec) - 2
;; ymax = max(list.dec) + 2
;; ltkedplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
;;            xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
;;            xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
;;            win=20, cats=cats, /noerror, /noleg

; Output #10b: known+eyeballed+discoveries, dec v. ra, incl. all objects
;; outfile = '~/Astro/699-1/paper/objplots/radec_ked_all'
;; ltkedplot, list.ra, list.dec, list.type, ps=ps, outfile=outfile, $
;;            xmin=xmax, xmax=xmin, ymin=ymin, ymax=ymax, $
;;            xtitle='Right Ascension (deg)', ytitle='Declination (deg)', $
;;            win=24, /noleg, /noerror

; Output #2: known, J-H (MKO) v. y-J
outfile = '~/Astro/699-1/paper/objplots/JHyJ_ked'
xmin = 0.4
xmax = 3.2
ymin = -0.7
ymax = 2.2
;; segment = {x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}
;; segment.color = 0
;; segment.style = 2
;; segment.thick = 5
;; segment.x = [1.9,  1.9 ]
;; segment.y = [ymin, ymax]
segment.x = [ [1.9,  1.9], [1.9, xmax] ]
segment.y = [ [ymin, 1.7], [1.7, 1.7 ] ]
ltkedplot, list.yj, list.jh, list.type, ps=ps, outfile=outfile, $
             xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
             xtitle='y!DPS1!N-J!DMKO!N', ytitle='J!DMKO!N-H!DMKO!N', $
             win=0, cats=cats

; Output #12a: known+eyeballed+discoveries, W1-W2 v. y-W1
outfile = '~/Astro/699-1/paper/objplots/w1w2yw1_ked'
xmin = 2.85
xmax = 6.3
ymin = 0.25
ymax = 2.5
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]
segment.x = [ [3.0, 3.0], [3.0, 6.3] ]
segment.y = [ [0.4, 2.5], [0.4, 0.4] ]
ltkedplot, list.yw1, list.w1w2, list.type, ps=ps, outfile=outfile, $
             xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, $
             xtitle='y!DPS1!N-W1', ytitle='W1-W2', $
             win=1, cats=cats, /right, /noerror


END
