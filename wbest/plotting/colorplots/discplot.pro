PRO DISCPLOT, F1, F2, F3, F4, CATS=cats, CATTOTS=cattots, ERRORV=errorv, FULLSYM=fullsym, $
              HORLINE=horline, LEGSIZE=legsize, MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, $
              OUTFILE=outfile, PS=ps, SEGMENT=segment, VERLINE=verline, WIN=win, $
              XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax, $
              _EXTRA=extrakey

;  Using objects from my L/T transition dwarf search, plots color-color and
;  color-magnitude diagrams of requested filter combinations.  By default, plots
;  objects with optical spectral type as open diamonds, and objects with
;  near-infrared spectral type as filled diamonds.  Currently ignores subdwarfs.
;
;  HISTORY
;  Written by Will Best (IfA), 10/09/2012
;  Adapted from megaplot.pro
;
;  USE
;      discplot, f1, f2 [, f3, f4, CATS=cats, CATTOTS=cattots, ERRORV=errorv, FULLYM=fullsym, $
;                HORLINE=horline, MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, PS=ps, $
;                SEGMENT=segment, VERLINE=verline, WIN=win, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      F1, F2, [F3, F4] - Filters whose colors or magnitudes are to be plotted.
;         If four filters are entered, a plot of F3-F4 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If three filters are entered, a plot of F3 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If two filters are entered, a plot of F1-F2 (y-axis) vs. SpT (x-axis)
;         will be created.
;         If fewer than two filters are entered, the syntax and a list of
;         available filters are printed, and the program quits.
;
;  OPTIONAL KEYWORDS
;      CSET - Six-element vector containing the colors to be plotted.
;             Color table is set by lincolr_wb
;             Default: [2, 8, 3, 5, 4, 6]
;      CATS - Seven-element vector containing spectral types for the boundaries
;             of the plotted colors.  Default: [0, 5, 10, 15, 20, 25, 30]
;             The first element cannot be less than 0.
;             The last element cannot be greater than 30.
;      CATTOTS - If a legend is being drawn, display the number of objects in
;                each category next to the name for each category in the legend.
;      ERRORV - Six-element vector containing 0's and 1's.
;               Categories corresponding to 0's are plotted without error bars;
;               categories corresponding to 1's are plotted with error bars.
;               Default: all categories plotted with error bars.
;               If NOERROR is set, ERRORV is ignored.
;      FULLSYM - Force all symbols to be filled, irrespective of spectral type.
;      HORLINE - Array of structures, containing information on horizontal lines to
;                draw on the plot.
;                horline.val = y-value of vertical line to draw.
;                horline.color = color for line
;                horline.style = linestyle for segment (keyword for OPLOT)
;                horline.thick = thickness for segment (keyword for OPLOT)
;      LEGSIZE - Multiplier for the size of the legend characters.
;                Default = 1
;      MINSPT - Minimum spectral type to fit.
;          Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;                 (no data exists for types later than T9.)
;          Default: 0 (M0)
;      MAXSPT - Maximum spectral type to fit (same scale as MINSPT).
;          Default: 29 (T9).
;      NOERROR - Don't plot the error bars.  Overrides ERRORVEC.
;      OUTFILE - Path for postscript output
;                Default: ~/Astro/colorplot1.ps
;      PS - Output to postscript.
;      SEGMENT - Array of structures, containing information on line segments to
;                draw on the plot.
;                segment.x = 2-element vectors containing x-coords of segment
;                endpoints.
;                segment.y = 2-element vectors containing y-coords of segment
;                endpoints.
;                segment.color = color for segment
;                segment.style = linestyle for segment (keyword for PLOTS)
;                segment.thick = thickness for segment (keyword for PLOTS)
;      VERLINE - Array of structures, containing information on vertical lines to
;                draw on the plot.
;                verline.val = x-value of vertical line to draw.
;                verline.color = color for line
;                verline.style = linestyle for segment (keyword for OPLOT)
;                verline.thick = thickness for segment (keyword for OPLOT)
;      WIN - Window number to use when calling the IDL window routine.
;            Usefull when a program calls colorplot multiple times.
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;
;  FUTURE IMPROVEMENTS
;      Default is to only fit dwarfs.
;          Add keyword to include subdwarfs.
;          Add keyword to only fit subdwarfs.
;      Add a /silent keyword.
;

;=======================

; Load L/T search objects. 
infile = '~/Astro/699-1/lttrans/plotting/lttrans.sav'
restore, infile

;=======================

; Create structure for filter names and polynomial coefficients
names = ['PS1 z', 'PS1 y', $
         '2MASS J', '2MASS H', '2MASS K', $
         'MKO Y', 'MKO J', 'MKO H', 'MKO K', $
         'WISE W1', 'WISE W2', 'WISE W3']
codes = ['z', 'y', $
         'J', 'H', 'K', $
         'YM', 'JM', 'HM', 'KM', $
         'W1', 'W2', 'W3']
refslt = [[16,17], [18,19], $
        [26,27], [28,29], [30,31], $
        [32,33], [34,35], [36,37], [38,39], $
        [20,21], [22,23], [24,25] ]
labels = ['z!DP1!N', 'y!DP1!N', $
         'J!D2MASS!N', 'H!D2MASS!N', 'K!D2MASS!N', $
         'Y!DMKO!N', 'J!DMKO!N', 'H!DMKO!N', 'K!DMKO!N', $
         'W1', 'W2', 'W3']

; Check for correct number of input parameters
nfilt = n_params()
if nfilt lt 2 or nfilt gt 4 then begin
    print, 'SYNTAX: megaplot, f1, f2 [, f3, f4, CATS=cats, CATTOTS=catttos, ERRORV=errorv, '
    print, '          FULLSYM=fullsym, HORLINE=horline, MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, '
    print, '          OUTFILE=outfile, PS=ps, SEGMENT=segment, VERLINE=verline, WIN=win, '
    print, '          XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax ]'
    print, 'Plots (1) f1-f2 vs. SpT  or  (2) f3 vs. f1-f2  or  (3) f3-f4 vs. f1-f2'
    print
    print, 'Filters available:'
    for i=0, n_elements(names)-1 do print, codes[i], ' = ', names[i]
    return
endif

; Check for sensisble minimum and maximum spectral types
if ((size(minspt, /type) ne 2) and (size(minspt, /type) ne 4)) then minspt = 0
if (minspt lt 0) then minspt = 0
if (minspt gt 29) then minspt = 29
if ((size(maxspt, /type) ne 2) and (size(maxspt, /type) ne 4)) then maxspt = 29
if (maxspt gt 29) then maxspt = 29
if (maxspt lt minspt) then maxspt = minspt

; Display the earliest and latest spectral types to be plotted
sptl = [replicate('M',10), replicate('L',10), replicate('T',10)]
if (maxspt eq minspt) then begin
    print, 'Only spectral type plotted will be '+sptl[fix(minspt)]+trim(minspt mod 10)
endif else begin
    print, 'Earliest spectral type plotted will be '+sptl[fix(minspt)]+trim(minspt mod 10)
    print, 'Latest spectral type plotted will be '+sptl[fix(maxspt)]+trim(maxspt mod 10)
endelse

; restrict spectral types as defined by minspt and maxspt
noneg = where((lttrans.spt ge minspt) and (lttrans.spt le maxspt))
lttrans = lttrans[noneg]

; Default cats vector
if n_elements(cats) eq 0 then cats = [0, 5, 10, 15, 20, 25, 30]
ncats = n_elements(cats) - 1

; Create arrays for photometry, colors, and errors
l = fltarr(13, n_elements(lttrans))
l[0,*] = lttrans.spt                                    ; Spectral Type

switch nfilt of
    4 : begin
        n4 = where(f4 eq codes)
        l[9,*] = lttrans.(refslt[0,n4])                ; f4
        l[10,*] = lttrans.(refslt[1,n4])               ; f4err
    end
    3 : begin
        n3 = where(f3 eq codes)
        l[7,*] = lttrans.(refslt[0,n3])                ; f3
        l[8,*] = lttrans.(refslt[1,n3])                ; f3
        if nfilt eq 4 then begin
            l[11,*] = l[7,*] - l[9,*]                  ; f3-f4
            l[12,*] = sqrt(l[8,*]^2 + l[10,*]^2)       ; f3-f4 err
        endif
    end
    else : begin
        n1 = where(f1 eq codes)
        l[1,*] = lttrans.(refslt[0,n1])                ; f1
        l[2,*] = lttrans.(refslt[1,n1])                ; f1err
        n2 = where(f2 eq codes)
        l[3,*] = lttrans.(refslt[0,n2])                ; f2
        l[4,*] = lttrans.(refslt[1,n2])                ; f2err
        l[5,*] = l[1,*] - l[3,*]                       ; f1-f2
        l[6,*] = sqrt(l[2,*]^2 + l[4,*]^2)             ; f1-f2 err
    end
endswitch

symbol = replicate({full:0, open:0, size:0.}, ncats)
; diamond, diamond, square, circle, triangle, triangle
symbol.full = [14, 14,  15,  16,  17,  17]
symbol.open = [ 4,  4,   6,   9,   5,   5]
symbol.size = [.8, 1.5, 1.5, 1.5, 1.5, 1.5]

; Check for error bar vector
if keyword_set(noerror) then errorv = intarr(ncats)
if n_elements(errorv) eq 0 then errorv = intarr(ncats)+1
if n_elements(errorv) ne ncats then begin
    print, 'ERRORV must have as many elements as CSET, all 0''s or 1''s'
    return
endif

; Build the legend structure
fl = strarr(ncats)
sptl = [replicate('M',10), replicate('L',10), replicate('T',10)]
if keyword_set(cattots) then begin
    ctot = intarr(ncats)
    for i=0, ncats-1 do begin
        cm = where((l[0,*] ge cats[i]) and (l[0,*] lt cats[i+1]))
        if cm[0] ne -1 then ctot[i] = n_elements(cm) 
        fl[i] = sptl[fix(cats[i])]+trim(cats[i] mod 10) + '-' $
                + sptl[fix(cats[i+1]-.5)]+trim((cats[i+1]-.5) mod 10) + ' : ' + trim(ctot[i])
    endfor
endif else begin
    for i=0, ncats-1 do fl[i] = sptl[fix(cats[i])]+trim(cats[i] mod 10) + '-' $
      + sptl[fix(cats[i+1]-.5)]+trim((cats[i+1]-.5) mod 10)
endelse

if n_elements(legsize) eq 0 then legsize = 1.
leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
leg.text = fl
leg.textc = intarr(ncats)
legchars = 1.5*legsize
leg.sym = symbol.full
if n_elements(cset) eq ncats then leg.symc = cset
leg.syms = symbol.size


; Load a color table
device, decomposed=0
lincolr_wb, /silent

; Set up for screen plotting or postcript output
if keyword_set(ps) then begin
    if n_elements(outfile) eq 0 then outfile = '~/Astro/colorchart1'
    ps_open, outfile, /color, /en, thick=6, /ps
endif else begin
    if n_elements(win) eq 0 then win = 0
    window, win, retain=2, xsize=800, ysize=600
endelse


case nfilt of

; 2 filters: F1-F2 vs. SpT
    2 : begin
        if not keyword_set(ymin) then ymin = min(l[5,*],/nan)-.5
        if not keyword_set(ymax) then ymax = max(l[5,*],/nan)+.5
        xmin = minspt-1
        xmax = maxspt+1
        colorplot, l[0,*], l[5,*], l[0,*], _extra=extrakey, $
                   xrange=[xmin,xmax], xtitle='Spectral Type', $
                   yrange=[ymin,ymax], ytitle=labels[n1]+'-'+labels[n2], yerr=l[6,*], $
                   cats=cats, errorvec=errorv, symbol=symbol, leg=leg, chars=legchars
    end

; 3 filters: F3 vs. F1-F2
    3 : begin
        if not keyword_set(xmin) then xmin = min(l[5,*],/nan)-2
        if not keyword_set(xmax) then xmax = max(l[5,*],/nan)+1
        if not keyword_set(ymin) then ymin = min(l[7,*],/nan)-.5
        if not keyword_set(ymax) then ymax = max(l[7,*],/nan)+.5
        colorplot, l[5,*], l[7,*], l[0,*], _extra=extrakey, $
                   xrange=[xmin,xmax], xtitle=labels[n1]+'-'+labels[n2], xerr=l[6,*], $
                   yrange=[ymax,ymin], ytitle=labels[n3], yerr=l[8,*], $
                   cats=cats, errorvec=errorv, symbol=symbol, leg=leg, chars=legchars
    end

; 4 filters: F3-F4 vs. F1-F2
    else : begin
        if not keyword_set(xmin) then xmin = min(l[5,*],/nan)-2
        if not keyword_set(xmax) then xmax = max(l[5,*],/nan)+1
        if not keyword_set(ymin) then ymin = min(l[11,*],/nan)-.5
        if not keyword_set(ymax) then ymax = max(l[11,*],/nan)+.5
        colorplot, l[5,*], l[11,*], l[0,*], _extra=extrakey, $
                   xrange=[xmin,xmax], xtitle=labels[n1]+'-'+labels[n2], xerr=l[6,*], $
                   yrange=[ymin,ymax], ytitle=labels[n3]+'-'+labels[n4], yerr=l[12,*], $
                   cats=cats, errorvec=errorv, symbol=symbol, leg=leg, chars=legchars
    end

endcase

;; west11 = 'Dropbox/panstarrs-BD/Known\ Objects/Mdwarfs_DR7_PS1WISE.csv'
;; readcol, west11, wy, ww1, ww2, skipline=1, delim=',', $
;;          format='x,x,x,x,x,x,x,x,x,x, x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,f,x,x,x,x,x,x,x,x,x,x,x,x,f,x,x,f'
;; oplot, wy-ww1, ww1-ww2, psym=3, color=0

; Plot any segments called
for i=0, n_elements(segment)-1 do plots, segment[i].x, segment[i].y, $
           color=segment[i].color, linestyle=segment[i].style, thick=segment[i].thick

; Plot any horizontal lines called
for i=0, n_elements(horline)-1 do hline, horline[i].val, $
           color=horline[i].color, linestyle=horline[i].style, thick=horline[i].thick

; Plot any vertical lines called
for i=0, n_elements(verline)-1 do vline, verline[i].val, $
           color=verline[i].color, linestyle=verline[i].style, thick=verline[i].thick


if keyword_set(ps) then ps_close

END
