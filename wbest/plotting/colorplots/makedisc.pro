;ps=1
;PRO MAKEDISC

;  Wrapper for discplot.pro
;
;  HISTORY
;  Written by Will Best (IfA), 10/10/2012
;
;  USE
;      discplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]

path = '~/Astro/699-1/observations/'
; L-T Transition: M0-M9.5, L0-L5.5, L6-L7.5, L8-L9.5, T0-T3.5, T4-T9.5
cats = [0, 10, 16, 18, 20, 24, 30]
cset = [15, 12, 2, 3, 4, 6]       ; new color scheme
;cset = [15, 12, 11, 2, 4, 6]       ; new color scheme
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 1)
;; segment.color = [4]
;; segment.style = [3]
;; segment.thick = [8]
verline = replicate({val:0., color:0, style:0, thick:0.}, 1)
verline.color = 0
verline.style = 2
verline.thick = 5
horline = verline

;; ; i-y vs. i-z
;; segment.x = [ [1.8, 1.8], [1.8, 3.2] ]
;; segment.y = [ [2.8, 4.5], [2.8, 2.8] ]
;; discplot, 'i', 'z', 'i', 'y', win=0, cset=cset, cats=cats, xmin=0.6, xmax=3.2, ymin=1.5, ymax=4.5, /fullsym, segment=segment, /ps, outfile='~/Astro/699-1/paper/iyiz'
; W1-W2 vs. z-y
xmin=0.2
xmax=2.5
ymin=0.2
ymax=2.5
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [2, 5]
segment.x = [ [0.6, 0.6], [0.6, xmax] ]
segment.y = [ [0.4, ymax], [0.4, 0.4] ]
discplot, 'z', 'y', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /right, ps=ps, outfile=path+'w1w2.zy'

;; ; W1 vs. W1-W2
;; xmin=-0.1;0.3
;; xmax=3;2.3
;; ymin=11.5;12
;; ymax=17.5;17.2
;; ;verline.val = 0.4
;; segment = {x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}
;; segment.color = 0
;; segment.style = 2
;; segment.thick = 5
;; segment.x = [xmin, (ymax-12.667)/2.833]
;; segment.y = [2.833*xmin+12.667, ymax]
;; discplot, 'W1', 'W2', 'W1', win=21, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, verline=verline, /right, ps=ps, outfile=path+'w1.w1w2'

; W1-W2 vs. y-W1
xmin=2.8
xmax=6.0
ymin=0.2
ymax=2.3
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
;; segment.color = [0, 0, 7]
;; segment.style = [2, 2, 2]
;; segment.thick = [5, 5, 8]
;; segment.x = [ [3.0, 3.0],  [3.0, xmax], [xmin,      (6-3.*ymin) < xmax] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4],  [2-xmin/3., ymin > (2-xmax/3.)] ]
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]
segment.x = [ [3.0, 3.0],  [3.0, xmax] ]
segment.y = [ [0.4, ymax], [0.4, 0.4]  ]
;segment.x = [xmin,      (6-3.*ymin) < xmax]
;segment.y = [2-xmin/3., ymin > (2-xmax/3.)]
discplot, 'y', 'W1', 'W1', 'W2', win=3, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /right, ps=ps, outfile=path+'w1w2.yw1'
; W1-W2 vs. i-z
;; segment.x = [ [1.8, 1.8], [1.8, 3.2] ]
;; segment.y = [ [0.4, 0.7], [0.4, 0.4] ]
;; discplot, 'i', 'z', 'W1', 'W2', win=4, cset=cset, cats=cats, xmin=-0.2, xmax=3.2, ymin=0.01, ymax=0.7, segment=segment;, /ps, outfile='~/Astro/699-1/paper/w1w2iz'
;; ; W1-W2 vs. z-W1
;; horline.val = 0.4
;; discplot, 'z', 'W1', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=1, xmax=8, horline=horline;, /ps, outfile='~/Astro/699-1/paper/w1w2zw1'

minspt=0
maxspt=28
; z-y vs. SpT
discplot, 'z', 'y', cats=cats, minspt=minspt, maxspt=maxspt, xtickname=['M0','M5','L0','L5','T0','T5','Y0'], xtickv=[0,5,10,15,20,25,30], ps=ps, outfile=path+'zy.spt'
;y-J vs. SpT
discplot, 'y', 'JM', cats=cats, minspt=minspt, maxspt=maxspt, xtickname=['M0','M5','L0','L5','T0','T5','Y0'], xtickv=[0,5,10,15,20,25,30], ps=ps, outfile=path+'yJ.spt'
; y-W1 vs. SpT
discplot, 'y', 'W1', cats=cats, ymin=0, ymax=7, minspt=minspt, maxspt=maxspt, xtickname=['M0','M5','L0','L5','T0','T5','Y0'], xtickv=[0,5,10,15,20,25,30], ps=ps, outfile=path+'yW1.spt'
; y-W2 vs. SpT
discplot, 'y', 'W2', cats=cats, ymax=7, minspt=minspt, maxspt=maxspt, xtickname=['M0','M5','L0','L5','T0','T5','Y0'], xtickv=[0,5,10,15,20,25,30], ps=ps, outfile=path+'yW2.spt'
; y vs. y-J
verline.val = 1.9
discplot, 'y', 'JM', 'y', win=2, cset=cset, cats=cats, verline=verline, ps=ps, xmin=0.9, xmax=3.5, outfile=path+'y.yJ'
; y-W1 vs. z-y
;discplot, 'z', 'y', 'y', 'W1', cats=cats, xmin=-1, ymax=7;, /ps
; J vs. J-W1
;discplot, 'JM', 'W1', 'JM', win=2, cset=cset, cats=cats, xmin=-0.5, xmax=3.7, ymin=11, ymax=18;, /ps, outfile='~/Desktop/J.Jw1'
; J-H vs. SpT
discplot, 'JM', 'HM', cats=cats, minspt=minspt, maxspt=maxspt, /right, xtickname=['M0','M5','L0','L5','T0','T5','Y0'], xtickv=[0,5,10,15,20,25,30], ps=ps, outfile=path+'JH.spt'
          
; J-H vs. y-J
xmin=1.4
xmax=3.5
ymin=-0.5
ymax=1.5
verline.val = 1.9
;discplot, 'y', 'JM', 'JM', 'HM', win=1, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, /right, verline=verline, ps=ps, outfile=path+'JH.yJ'
discplot, 'y', 'JM', 'JM', 'HM', win=6, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, /right, verline=verline, ps=ps, outfile=path+'JH.yJ'
xmin=1.0
xmax=3.5
ymin=-0.8
ymax=2.0
verline.val = 1.8
;discplot, 'y', 'J', 'J', 'H', win=1, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, /right, verline=verline, ps=ps, outfile=path+'JH.yJ'
discplot, 'y', 'J', 'J', 'H', win=1, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, /bottom, verline=verline, ps=ps, outfile=path+'J2H2.yJ2', cset=cset

; J-H vs. J-K
;discplot, 'J', 'K', 'J', 'H', cats=cats, xmin=-1, xmax=3, win=0;, /noerror;, /fullsym, /ps
; J-K vs. Spt
;discplot, 'J', 'K', cats=cats, win=2;, /ps
; J-W1 vs. Spt
;discplot, 'JM', 'W1', win=3, minspt=5, cats=cats, ymin=-0.5, ymax=3.7;, /ps, outfile=path+'Jw1.spt_new'
; H-K vs. J-H
;discplot, 'J', 'H', 'H', 'K', cats=cats, xmin=-0.7, xmax=1.8, ymin=-1, /noerror;, /fullsym, /ps
; W1 vs. W1-W2
;discplot, 'W1', 'W2', 'W1', win=2, cset=cset, cats=cats, xmin=-1, xmax=3.5, /right;, /ps
; W1-W2 vs. SpT
discplot, 'W1', 'W2', win=4, cats=cats, minspt=minspt, maxspt=maxspt, xtickname=['M0', 'M5','L0','L5','T0','T5','Y0'], xtickv=[0,5,10,15,20,25,30], ps=ps, outfile=path+'w1w2.spt'
; W1-W2 vs. i-z
;discplot, 'i', 'z', 'W1', 'W2', win=4, cset=cset, cats=cats, xmin=-0.2, xmax=3.2, ymin=0.01, ymax=0.7;, /ps
; W1-W2 vs. z-y
;discplot, 'z', 'y', 'W1', 'W2', win=1, cset=cset, cats=cats, xmin=-0.5, xmax=2.5;, /ps
; W1-W2 vs. z-W1
;discplot, 'z', 'W1', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=1, xmax=8;, /ps
; W1-W2 vs. y-J
;discplot, 'y', 'J', 'W1', 'W2', win=2, cset=cset, cats=cats, xmin=1, xmax=3.5;, /ps
; W1-W2 vs. y-W1
;discplot, 'y', 'W1', 'W1', 'W2', win=3, cset=cset, cats=cats, xmin=1, xmax=7;, /ps
; W1-W2 vs. J-K
;discplot, 'J', 'K', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-1, xmax=3;, /ps
; W1-W2 vs. H-K
;discplot, 'H', 'K', 'W1', 'W2', cats=cats, cset=cset, errorv=[0,0,1,1,1,1], xmin=-1, xmax=2, /right;, /ps
; W2-W3 vs. SpT
;discplot, 'W2', 'W3', cats=cats, minspt=3;, /ps
; W2-W3 vs. z-y
;discplot, 'z', 'y', 'W2', 'W3', cats=cats, xmin=-0.3, xmax=2;, /noerror;, /ps
; W2-W3 vs. y-W1
;discplot, 'y', 'W1', 'W2', 'W3', cats=cats, xmin=1.5, xmax=9.5, /right, /noerror;, /ps
; W2-W3 vs. W1-W2
xmin=-0.01
xmax=3.2
ymin=0
ymax=3
segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
segment.color = [0, 0]
segment.style = [2, 2]
segment.thick = [5, 5]
segment.x = [ [0.4, 0.4],  [0.4, xmax] ]
segment.y = [ [ymin, 2.5], [2.5, 2.5]  ]
discplot, 'W1', 'W2', 'W2', 'W3', cats=cats, cset=cset, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /bottom, /right, ps=ps, outfile=path+'w2w3.w1w2'


; L dwarfs only: M8-L1.5, L2-L3.5, L4-L5.5, L6-L7.5, L8-L9.5, T0-T9.5
;; cats = [8, 12, 14, 16, 18, 20, 30]
;; cset = [12, 2, 3, 4, 6, 15]       ; new color scheme

;; xmin=0.4
;; xmax=2.0
;; ymin=0.3
;; ymax=1.0
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
;; segment.color = [0, 0]
;; segment.style = [2, 2]
;; segment.thick = [2, 5]
;; segment.x = [ [0.6, 0.6], [0.6, xmax] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4] ]
;; discplot, 'z', 'y', 'W1', 'W2', win=5, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /right, ps=ps, outfile=path+'w1w2.zy_Lonly'

;; xmin=2.8
;; xmax=6.0
;; ymin=0.3
;; ymax=1.0
;; segment.color = [0, 0]
;; segment.style = [2, 2]
;; segment.thick = [5, 5]
;; segment.x = [ [3.0, 3.0],  [3.0, xmax] ]
;; segment.y = [ [0.4, ymax], [0.4, 0.4]  ]
;; discplot, 'y', 'W1', 'W1', 'W2', win=3, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /right, ps=ps, outfile=path+'w1w2.yw1_Lonly'

;; xmin=1.5
;; xmax=2.5
;; ymin=-0.3
;; ymax=1.5
;; discplot, 'y', 'JM', 'JM', 'HM', win=1, cset=cset, cats=cats, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, verline=verline, /bottom, ps=ps, outfile=path+'JH.yJ_Lonly'

;; xmin=0.2
;; xmax=2.0
;; ymin=0
;; ymax=2.7
;; segment = replicate({x:[0.,0.], y:[0.,0.], color:0, style:0, thick:0.}, 2)
;; segment.color = [0, 0]
;; segment.style = [2, 2]
;; segment.thick = [5, 5]
;; segment.x = [ [0.4, 0.4],  [0.4, xmax] ]
;; segment.y = [ [ymin, 2.5], [2.5, 2.5]  ]
;; discplot, 'W1', 'W2', 'W2', 'W3', cats=cats, cset=cset, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax, segment=segment, /bottom, /right, ps=ps, outfile=path+'w2w3.w1w2_Lonly'

; Histogram of spectral types
restore, '~/Astro/699-1/lttrans/plotting/lttrans.sav'

if keyword_set(ps) then begin
    fpath = path+'spt.w1cut.hist'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 7, retain=2, xsize=800, ysize=600
xmin=5
xmax=28
ymax=14
;; sptaxis = ['M0','M5','L0','L5','T0','T5']
;; sptval = [0,5,10,15,20,25]
; show all discoveries, W1 < 2.833(W1-W2) + 12.667, d<25
sptaxis = ['M5','L0','L5','T0','T5']
sptval = [5,10,15,20,25]
plothist, lttrans.spt, xtitle='Spectral Type', ytitle='Number Discovered', charsize=1.6, color=0, backg=1, bin=1, $
          xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, yminor=2, yrange=[0,ymax], ystyle=1, /nan, thick=6
vollim = where(lttrans.w1 lt (2.833*(lttrans.w1-lttrans.w2)+12.667))
plothist, lttrans[vollim].spt, color=4, bin=1, xrange=[xmin,xmax], /overplot, /nan, /fill, fcolor=4, /fline, forient=45, thick=6
distlim = where((lttrans.dist gt 0) and (lttrans.dist le 25.0))
plothist, lttrans[distlim].spt, color=3, bin=1, xrange=[xmin,xmax], /overplot, /nan, /fill, fcolor=3, thick=6
axis, color=0, xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, xstyle=1, charsize=1.6
cutlabel = 'W1 < 2.8(W1-W2) + 12.7 mag'
;cutlabel = 'W1 v. W1-W2 cut'
legend, ['All', cutlabel, 'd!Dphot!N < 25 pc'], $
        lines=[0,0,0], colors=[0,4,3], textcolors=[0,4,3], chars=1.3, box=0
if keyword_set(ps) then ps_close

if keyword_set(ps) then begin
    fpath = path+'spt.hist'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 8, retain=2, xsize=800, ysize=600
xmin=5
xmax=28
;; sptaxis = ['M0','M5','L0','L5','T0','T5']
;; sptval = [0,5,10,15,20,25]
; show all discoveries, y<19.3, d<25
sptaxis = ['M5','L0','L5','T0','T5']
sptval = [5,10,15,20,25]
plothist, lttrans.spt, xtitle='Spectral Type', ytitle='Number Discovered', charsize=1.6, color=0, backg=1, bin=1, $
          xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, yminor=2, /nan, thick=6
vollim = where(lttrans.y lt 19.3)
plothist, lttrans[vollim].spt, color=4, bin=1, xrange=[xmin,xmax], /overplot, /nan, $
          /fill, fcolor=4, thick=6
distlim = where((lttrans.dist gt 0) and (lttrans.dist le 25.0), comp=far)
plothist, lttrans[distlim].spt, color=3, bin=1, xrange=[xmin,xmax], /overplot, /nan, $
          /fill, fcolor=3, thick=6
axis, color=0, xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, xstyle=1, charsize=1.6
legend, ['All', 'y!DPS1!N!I !N<!I !N19.3 mag', 'dist!I !N<!I !N25 pc'], $
        lines=[0,0,0], colors=[0,4,3], textcolors=[0,4,3], chars=1.3, box=0
; show all discoveries, d<25
;; sptaxis = ['M5','L0','L5','T0','T5']
;; sptval = [5,10,15,20,25]
;; plothist, lttrans.spt, xtitle='Spectral Type', ytitle='Number Discovered', $
;;           charsize=1.6, color=0, backg=1, bin=1, $
;;           xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, yminor=2, /nan;, yrange=[0,13], /ystyle
;; distlim = where(lttrans.dist le 25.0)
;; plothist, lttrans[distlim].spt, color=3, bin=1, xrange=[5,28], /overplot, /nan, $
;;           /fill, fcolor=3
;; axis, color=0, xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, xstyle=1, charsize=1.6
;; legend, ['All', 'dist!I !N<!I !N25 pc'], lines=[0,0], colors=[0,3], textcolors=[0,3], chars=1.5, box=0
if keyword_set(ps) then ps_close

; Histogram of J-H
if keyword_set(ps) then begin
    fpath = path+'JH.hist'
    ps_open, fpath, /color, /en, thick=4
endif else window, 9, retain=2, xsize=800, ysize=600
plothist, lttrans.JM-lttrans.HM, xtitle='J-H', ytitle='Number', color=0, backg=1, /nan, $
          charsize=1.6, bin=.1, xrange=[-.4,1.7];, yminor=2
plothist, lttrans.J-lttrans.H, color=3, bin=.1, /nan, /overplot
legend, ['MKO', '2MASS'], lines=[0,0], colors=[0,3], textcolors=[0,3], chars=1.5
if keyword_set(ps) then ps_close

; Histogram of J-K
if keyword_set(ps) then begin
    fpath = path+'JK.hist'
    ps_open, fpath, /color, /en, thick=4
endif else window, 10, retain=2, xsize=800, ysize=600
plothist, lttrans.JM-lttrans.KM, xtitle='J-K', ytitle='Number', color=0, backg=1, /nan, $
          charsize=1.6, bin=.1, xrange=[-.1,2.4], yminor=2, yrange=[0,10], /ystyle
plothist, lttrans.J-lttrans.K, color=3, bin=.1, /nan, /overplot
legend, ['MKO', '2MASS'], lines=[0,0], colors=[0,3], textcolors=[0,3], chars=1.5, box=0
if keyword_set(ps) then ps_close

; Histogram of H-K
if keyword_set(ps) then begin
    fpath = path+'HK.hist'
    ps_open, fpath, /color, /en, thick=4
endif else window, 11, retain=2, xsize=800, ysize=600
plothist, lttrans.HM-lttrans.KM, xtitle='H-K', ytitle='Number', color=0, backg=1, /nan, $
          charsize=1.6, bin=.1, xrange=[-.3,1.4], yrange=[0,15], yminor=2
plothist, lttrans.H-lttrans.K, color=3, bin=.1, /nan, /overplot
legend, ['MKO', '2MASS'], lines=[0,0], colors=[0,3], textcolors=[0,3], chars=1.5
if keyword_set(ps) then ps_close

; Histogram of distances
if keyword_set(ps) then begin
    fpath = path+'dist.hist'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 12, retain=2, xsize=800, ysize=600
plothist, lttrans.dist, xtitle='Photometric Distance (pc)', ytitle='Number Discovered', color=0, backg=1, $
          /nan, charsize=1.6, bin=5, xrange=[0,60], yrange=[0,22], ystyle=1, yminor=1, thick=6;, /fill, fcolor=0
vline, 25, color=0, linestyle=2, thick=6
if keyword_set(ps) then ps_close

;;; W1 vs. W1-W2
; Plot boundaries
xmin=0.2
xmax=3;2.3
ymin=11.5;12
ymax=17.2
; Set up postcript output
if keyword_set(ps) then begin
    fpath = path+'w1.w1w2'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 20, retain=2, xsize=800, ysize=600
; Plot parameters
size = 1.2
plot, [0], color=0, backg=1, chars=1.6, $
      xrange=[xmin,xmax], xstyle=1, xtitle='W1-W2', $
      yrange=[ymax,ymin], ystyle=1, ytitle='W1'
; Trent's parallax data
cfhtfile = '~/Astro/699-1/vlm-plx-irac+wise.csv'
readcol, cfhtfile, c_par, c_w1, c_w1_err, c_w2, c_w2_err, $
         format='x,x,x,x,x,x,x,x,f,x,x,x,x,x,x,x,x,x,x,f,f,f,f', delim=',', /preserve_null
distlim = where((lttrans.dist gt 0) and (lttrans.dist le 25.0), comp=far)
parlim = where(c_par ge 40.0, comp=c_far)
oploterror, c_w1[c_far]-c_w2[c_far], c_w1[c_far], $
            sqrt(c_w1_err[c_far]^2+c_w2_err[c_far]^2), c_w1_err[c_far], $
            color=15, psym=cgsymcat(14), symsize=size, /nohat, errthick=2
oploterror, c_w1[parlim]-c_w2[parlim], c_w1[parlim], $
            sqrt(c_w1_err[parlim]^2+c_w2_err[parlim]^2), c_w1_err[parlim], $
            color=9, psym=cgsymcat(15), symsize=size, /nohat, errthick=2
; Plot discoveries
oploterror, lttrans[far].w1-lttrans[far].w2, lttrans[far].w1, $
            sqrt(lttrans[far].w1_err^2+lttrans[far].w2_err^2), lttrans[far].w1_err, $
            color=12, psym=cgsymcat(14), symsize=size, /nohat, errthick=2
oploterror, lttrans[distlim].w1-lttrans[distlim].w2, lttrans[distlim].w1, $
            sqrt(lttrans[distlim].w1_err^2+lttrans[distlim].w2_err^2), lttrans[distlim].w1_err, $
            color=3, psym=cgsymcat(15), symsize=size, /nohat, errthick=2
; Draw the prioritizing line
plots, [xmin, (ymax-12.667)/2.833], [2.833*xmin+12.667, ymax], color=0, lines=2, thick=9
; Legend
legend, ['Observed', 'd!Dphot!N!I !N<!I !N25 pc', 'Known parallax', 'd!Dpar!N!I !N<!I !N25 pc'], $
        box=0, outline=0, /right, $
        psym=[14,15,14,15], colors=[12,3,15,9], textcolors=[0,0,0,0], chars=1.6
if keyword_set(ps) then ps_close


;;; HISTOGRAM - COMPARISON WITH KNOWN OBJECTS
megafile = '~/Dropbox/panstarrs-BD/Known_Objects/MegaTable.csv'
readcol, megafile, k_j, k_j_err, k_h, k_h_err, k_k, k_k_err, k_jm, k_jm_err, k_hm, k_hm_err, k_km, k_km_err, $
         k_w2, k_plx, k_spt, format='x,x,x,x,x,x,x,x,x,x,x,x,f,f,f,f,f,f,x,x,f,f,f,f,f,f,x,x,f,x,x,x,x,x,x,x,x,x,x,x,x,x,f,x,x,x,x,x,x,x,x,x,x,x,x,x,f,x', $
         delim=',', comment='#', /preserve_null, /silent

if keyword_set(ps) then begin
    fpath = path+'spt.hist.known.zoom'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 13, retain=2, xsize=800, ysize=600
;k_histarr = abs(k_spt)
k_parlim = where(k_plx ge 40.0)
k_phodist = pw_w2dist(abs(k_spt), k_w2)
k_maglim = where((k_phodist gt 0) and (k_phodist le 25.0))
k_lim = [k_parlim, k_maglim]
k_distlim = k_lim[uniq(k_lim, sort(k_lim))]
k_histarr = abs(k_spt[k_distlim])
;lt_histarr = abs(lttrans.spt)
lt_histarr = abs(lttrans[distlim].spt)
xmin=16
xmax=25
ymax=20
sptaxis = ['L6','L8','T0','T2','T4', ' ']
sptval = [16,18,20,22,24,26]
plothist, k_histarr, xtitle='Spectral Type', ytitle='Number', charsize=1.6, color=0, backg=1, bin=1, $
          xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, yminor=2, /nan, yrange=[0,ymax], ystyle=1, thick=6
plothist, lt_histarr, color=13, bin=1, /overplot, /nan, /fill, fcolor=13, thick=6
axis, color=0, xrange=[xmin,xmax], xtickname=sptaxis, xtickv=spt, xstyle=1, charsize=1.6
;axis, yaxis=0, color=0, yrange=[0,ymax], ystyle=1, charsize=1.6
;axis, yaxis=1, color=0, yrange=[0,ymax], ystyle=1, ytickname=replicate(' ', 6)
legend, ['Previously Known, d < 25 pc', 'Discoveries, d!Dphot!N < 25 pc'], $
        lines=[0,0], colors=[0,13], textcolors=[0,13], chars=1.6, box=0
if keyword_set(ps) then ps_close

;;; M_J v. J-H - COMPARISON WITH KNOWN OBJECTS
; Plot boundaries
xmin=-1.5
xmax=2
ymin=9
ymax=19
; Set up postcript output
if keyword_set(ps) then begin
    fpath = path+'J.JH'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 14, retain=2, xsize=800, ysize=600
; Plot parameters
size = 1.6
plot, [0], color=0, backg=1, chars=1.6, $
      xrange=[xmin,xmax], xstyle=1, xtitle='J!DMKO!N-H!DMKO!N', $
      yrange=[ymax,ymin], ystyle=1, ytitle='M!DJ!N'
; Trent's parallax data
cfhtfile2 = '~/Astro/699-1/vlm-plx-mko+2mass.csv'
readcol, cfhtfile2, c2_par, c2_par_err, c2_jm, c2_jm_err, c2_hm, c2_hm_err, c2_j, c2_j_err, c2_h, c2_h_err, $
         format='x,x,x,x,x,x,x,x,f,f,x,x,f,f,f,f,x,x,x,x,x,x,f,f,f,f', delim=',', comment='#', /preserve_null
c2_m_jm = app2abs(c2_jm, c2_par, appmagerr=c2_jm_err, parerr=c2_par_err, absmagerr=c2_m_jm_err)
c2_m_j = app2abs(c2_j, c2_par, appmagerr=c2_j_err, parerr=c2_par_err, absmagerr=c2_m_j_err)
oploterror, c2_jm-c2_hm, c2_m_jm, $
            sqrt(c2_jm_err^2+c2_hm_err^2), c2_m_jm_err, $
            color=15, psym=cgsymcat(14), symsize=1., /nohat, errthick=2
; Plot discoveries
m_jm = app2abs_dist(lttrans.jm, lttrans.dist, appmagerr=lttrans.jm_err, disterr=lttrans.disterr, absmagerr=m_jm_err)
m_j = app2abs_dist(lttrans.j, lttrans.dist, appmagerr=lttrans.j_err, disterr=lttrans.disterr, absmagerr=m_j_err)
cats = [10, 16, 18, 20, 24, 30]
cols = [12, 2, 3, 4, 6]
syms = [14, 15, 16, 17, 17]
for i=0, n_elements(cats)-2 do begin
    plotcat = where((lttrans.spt ge cats[i]) and (lttrans.spt lt cats[i+1]))
    oploterror, lttrans[plotcat].jm-lttrans[plotcat].hm, m_jm[plotcat], $
                sqrt(lttrans[plotcat].jm_err^2+lttrans[plotcat].hm_err^2), m_jm_err[plotcat], $
                color=cols[i], psym=cgsymcat(syms[i]), symsize=size, /nohat, errthick=2
endfor
; Legend
legend, ['CFHT parallax', 'Discoveries L0-L5.5', 'Discoveries L6-L7.5', 'Discoveries L8-L9.5', $
         'Discoveries T0-T3.5', 'Discoveries T4-T6.5'], $
        box=0, $
        psym=[14, syms], symsize=[1, replicate(size,5)], colors=[15, cols], textcolors=intarr(6), chars=1.4
if keyword_set(ps) then ps_close

;;; M_J v. y-W1 - COMPARISON WITH KNOWN OBJECTS
; Plot boundaries
xmin=2.8
xmax=6
ymin=10
ymax=16
; Set up postcript output
if keyword_set(ps) then begin
    fpath = path+'J.yW1'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 15, retain=2, xsize=800, ysize=600
; Plot parameters
size = 1.6
plot, [0], color=0, backg=1, chars=1.6, $
      xrange=[xmin,xmax], xstyle=1, xtitle='y!DP1!N-W1', $
      yrange=[ymax,ymin], ystyle=1, ytitle='M!DJ!N'
; Plot discoveries
cats = [10, 16, 18, 20, 24, 30]
cols = [12, 2, 3, 4, 6]
syms = [14, 15, 16, 17, 17]
for i=0, n_elements(cats)-2 do begin
    plotcat = where((lttrans.spt ge cats[i]) and (lttrans.spt lt cats[i+1]))
    oploterror, lttrans[plotcat].y-lttrans[plotcat].w1, m_jm[plotcat], $
                sqrt(lttrans[plotcat].y_err^2+lttrans[plotcat].w1_err^2), m_jm_err[plotcat], $
                color=cols[i], psym=cgsymcat(syms[i]), symsize=size, /nohat, errthick=2
endfor
; Legend
legend, ['CFHT parallax', 'Discoveries L0-L5.5', 'Discoveries L6-L7.5', 'Discoveries L8-L9.5', $
         'Discoveries T0-T3.5', 'Discoveries T4-T6.5'], $
        box=0, $
        psym=[14, syms], symsize=[1, replicate(size,5)], colors=[15, cols], textcolors=intarr(6), chars=1.4
if keyword_set(ps) then ps_close

;;; M_J v. y-J - COMPARISON WITH KNOWN OBJECTS
; Plot boundaries
xmin=1.5
xmax=3.5
ymin=10
ymax=16
; Set up postcript output
if keyword_set(ps) then begin
    fpath = path+'J.yJ'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 16, retain=2, xsize=800, ysize=600
; Plot parameters
size = 1.6
plot, [0], color=0, backg=1, chars=1.6, $
      xrange=[xmin,xmax], xstyle=1, xtitle='y!DP1!N-J!DMKO!N', $
      yrange=[ymax,ymin], ystyle=1, ytitle='M!DJ!N'
; Plot discoveries
cats = [10, 16, 18, 20, 24, 30]
cols = [12, 2, 3, 4, 6]
syms = [14, 15, 16, 17, 17]
for i=0, n_elements(cats)-2 do begin
    plotcat = where((lttrans.spt ge cats[i]) and (lttrans.spt lt cats[i+1]))
    oploterror, lttrans[plotcat].y-lttrans[plotcat].jm, m_jm[plotcat], $
                sqrt(lttrans[plotcat].y_err^2+lttrans[plotcat].jm_err^2), m_jm_err[plotcat], $
                color=cols[i], psym=cgsymcat(syms[i]), symsize=size, /nohat, errthick=2
endfor
; Legend
legend, ['CFHT parallax', 'Discoveries L0-L5.5', 'Discoveries L6-L7.5', 'Discoveries L8-L9.5', $
         'Discoveries T0-T3.5', 'Discoveries T4-T6.5'], $
        box=0, $
        psym=[14, syms], symsize=[1, replicate(size,5)], colors=[15, cols], textcolors=intarr(6), chars=1.4
if keyword_set(ps) then ps_close

;;; M_J v. W1-W2 - COMPARISON WITH KNOWN OBJECTS
; Plot boundaries
xmin=0.3
xmax=3
ymin=10
ymax=16
; Set up postcript output
if keyword_set(ps) then begin
    fpath = path+'J.w1w2'
    ps_open, fpath, /color, /en, thick=6, /ps
endif else window, 17, retain=2, xsize=800, ysize=600
; Plot parameters
size = 1.6
plot, [0], color=0, backg=1, chars=1.6, $
      xrange=[xmin,xmax], xstyle=1, xtitle='W1-W2', $
      yrange=[ymax,ymin], ystyle=1, ytitle='M!DJ!N'
; Plot discoveries
cats = [10, 16, 18, 20, 24, 30]
cols = [12, 2, 3, 4, 6]
syms = [14, 15, 16, 17, 17]
for i=0, n_elements(cats)-2 do begin
    plotcat = where((lttrans.spt ge cats[i]) and (lttrans.spt lt cats[i+1]))
    oploterror, lttrans[plotcat].w1-lttrans[plotcat].w2, m_jm[plotcat], $
                sqrt(lttrans[plotcat].w1_err^2+lttrans[plotcat].w2_err^2), m_jm_err[plotcat], $
                color=cols[i], psym=cgsymcat(syms[i]), symsize=size, /nohat, errthick=2
endfor
; Legend
legend, ['CFHT parallax', 'Discoveries L0-L5.5', 'Discoveries L6-L7.5', 'Discoveries L8-L9.5', $
         'Discoveries T0-T3.5', 'Discoveries T4-T6.5'], $
        box=0, $
        psym=[14, syms], symsize=[1, replicate(size,5)], colors=[15, cols], textcolors=intarr(6), chars=1.4
if keyword_set(ps) then ps_close



;  USE
;      discplot, f1, f2 [, f3, f4, CATS=cats, ERRORV=errorv, FULLYM=fullsym, $
;                  MINSPT=minspt, MAXSPT=maxspt, NOERROR=noerror, OUTFILE=outfile, $
;                  PS=ps, XMIN=xmin, XMAX=xmax, YMIN=ymin, YMAX=ymax]
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      F1, F2, [F3, F4] - Filters whose colors or magnitudes are to be plotted.
;         If four filters are entered, a plot of F3-F4 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If three filters are entered, a plot of F3 (y-axis) vs. F1-F2 (x-axis)
;         will be created.
;         If two filters are entered, a plot of F1-F2 (y-axis) vs. SpT (x-axis)
;         will be created.
;         If fewer than two filters are entered, the syntax and a list of
;         available filters are printed, and the program quits.
;
;  OPTIONAL KEYWORDS
;      CSET - Six-element vector containing the colors to be plotted.
;             Color table is set by lincolr_wb
;             Default: [2, 8, 3, 5, 4, 6]
;      CATS - Seven-element vector containing spectral types for the boundaries
;             of the plotted colors.  Default: [0, 5, 10, 15, 20, 25, 30]
;             The first element cannot be less than 0.
;             The last element cannot be greater than 30.
;      ERRORV - Six-element vector containing 0's and 1's.
;               Categories corresponding to 0's are plotted without error bars;
;               categories corresponding to 1's are plotted with error bars.
;               Default: all categories plotted with error bars.
;               If NOERROR is set, ERRORV is ignored.
;      FULLSYM - Force all symbols to be filled, irrespective of spectral type.
;      MINSPT - Minimum spectral type to fit.
;          Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;                 (no data exists for types later than T9.)
;          Default: 0 (M0)
;      MAXSPT - Maximum spectral type to fit (same scale as MINSPT).
;          Default: 29 (T9).
;      NOERROR - Don't plot the error bars.  Overrides ERRORVEC.
;      OUTFILE - Path for postscript output
;                Default: ~/Astro/colorplot1.ps
;      PS - Output to postscript
;      XMIN - Minimum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      XMAX - Maximum color to be plotted on the x-axis.
;             Ignored for color vs. SpT plots.
;      YMIN - Minimum color or magnitude to be plotted on the y-axis.
;      YMAX - Maximum color or magnitude to be plotted on the y-axis.
;
;  Display colors:
;      Old scheme:
;      M0-M4: BRIGHT GREEN
;      M5-M9: ORANGE
;      L0-L4: RED
;      L5-L9: MAGENTA
;      T0-T4: BLUE
;      T5-T9: CYAN
;
;      New scheme:
;      M0-M4: LIGHT GREY diamond
;      M5-M9: GREY diamond
;      L0-L4: RED circle
;      L5-L9: BRIGHT GREEN square
;      T0-T4: BLUE triangle
;      T5-T9: CYAN triangle
;

END
