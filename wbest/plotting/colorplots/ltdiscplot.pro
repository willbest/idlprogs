PRO LTDISCPLOT, FILE, ERRORV=errorv, MINTYPE=minspt, MAXTYPE=maxspt, NOERROR=noerror, $
                _EXTRA=extrakey

;  Routine to plot color-color and color-magnitude diagrams for discoveries from
;  my first 699 project, on L-T transition dwarfs.
;  Plots showing the eyeballed objects:
;       passed eyeballing 3, discovered 5
;
;  HISTORY
;  Written by Will Best (IfA), 08/11/2012
;
;  CALLS
;      colorplot.pro
;
;  INPUTS
;      FILE - .csv file containing the data to be plotted
;
;  OPTIONAL KEYWORDS
;      ERRORV - Six-element vector containing 0's and 1's.
;               Categories corresponding to 0's are plotted without error bars;
;               categories corresponding to 1's are plotted with error bars.
;               Default: all categories plotted with error bars.
;               If NOERROR is set, ERRORV is ignored.
;      MINTYPE - Minimum category value to fit.
;                Default: 0
;      MAXTYPE - Maximum category value to fit.
;                Default: 5
;      NOERROR - Don't plot the error bars.  Overrides ERRORVEC.
;      PS - Output to postscript
;

;=======================

; Load the data table.
if n_elements(file) eq 0 then file = '~/Astro/699-1/lttrans/plotting/eye.sav'

restore, file
 
n = n_elements(eye.ra)

;=======================

; Check for sensisble minimum and maximum types
if ((size(mintype, /type) ne 2) and (size(mintype, /type) ne 4)) then mintype = 0
if (mintype lt 0) then mintype = 0
if (mintype gt 5) then mintype = 5
if ((size(maxtype, /type) ne 2) and (size(maxtype, /type) ne 4)) then maxtype = 5
if (maxtype gt 5) then maxtype = 5
if (maxtype lt mintype) then maxtype = mintype

; Create vector for filled and open symbols
opensym = bytarr(n)

; Default error bar vector
if keyword_set(noerror) then errorv = [0,0,0,0,0,0]
if n_elements(errorv) eq 0 then errorv = [0,0,1,1,1,1]

; Default color set
cset = [15, 12, 2, 3, 4, 6]

; Define structure with plotting symbols
symbol = replicate({full:0, open:0, size:0.}, 6)
; diamond, diamond, square, circle, triangle, triangle
symbol.full = [14, 14,  15,  16,  17,  17]
symbol.open = [ 4,  4,   6,   9,   5,   5]
symbol.size = [.8, 1.5, 1.5, 2.5, 1.5, 1.5]

; Define structure with only small points for plotting symbols
pts = replicate({full:0, open:0, size:0.}, 6)
; default symbol is a small square
pts.full = intarr(6) + 15
pts.open = intarr(6) + 15
pts.size = intarr(6) + .5

; Output #1: W1-W2 v. y-W1
cats = [3, 5, 6]
col = [15, 3]
ncats = n_elements(cats)-1
xmin = min(eye.yw1) - 0.2
xmax = 6.1
ymin = 0.2
ymax = max(eye.w1w2) + 0.2
leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
leg.text = ['Candidates', 'Confirmed L7-T4.5']
leg.textc = intarr(ncats)
legchars = 2
leg.sym = symbol[[1,3]].full
leg.syms = symbol[[1,3]].size
colorplot, eye.yw1, eye.w1w2, eye.type, leg=leg, chars=legchars, _extra=extrakey, $
           outfile='~/Astro/699-1/paper/objplots/disc', $
           xrange=[xmin,xmax], xtitle='y!LPS1!N-W1', win=0, cats=cats, cset=col, $
           yrange=[ymin,ymax], ytitle='W1-W2', /noerror, symbol=symbol[[1,3]], /right

; Output #2: W1-W2 v. z-y
cats = [3, 5, 6]
col = [15, 3]
ncats = n_elements(cats)-1
xmin = min(eye.zy) - 0.2
xmax = max(eye.zy) + 0.2
ymin = min(eye.w1w2) - 0.2
ymax = max(eye.w1w2) + 0.2
leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
leg = replicate({text:'', textc:0., sym:0, symc:-1, syms:-1.}, ncats)
leg.text = ['Candidates', 'Confirmed L7-T4.5']
leg.textc = intarr(ncats)
legchars = 2
leg.sym = symbol[[1,3]].full
leg.syms = symbol[[1,3]].size
colorplot, eye.zy, eye.w1w2, eye.type, leg=leg, chars=legchars, _extra=extrakey, $
           outfile='~/Astro/699-1/paper/objplots/disc2', $
           xrange=[xmin,xmax], xtitle='z!LPS1!N-y!LPS1!N', win=1, cats=cats, cset=col, $
           yrange=[ymin,ymax], ytitle='W1-W2', /noerror, symbol=symbol[[1,3]], /right


END
