PRO EQUGAL, RAH, RAM, RAS, DECD, DECM, DECS, L, B

;  Converts equatorial coordinates (RA and DEC) to galactic
;  coordinates (L and b).  Uses the following coordinates for
;  Galactic North Pole, epoch 2000.0:
;  RA = 12h51m26s, Dec = 27d7m42s
;  
;  HISTORY
;  Written by Will Best (IfA), 09/09/2011
;
;  USE:  EQUGAL, RAh, RAm, RAs, DECd, DECm, DECs
;        If the declination is -0dxxmxxs (i.e. negative, but less than
;        1 degree), enter 0 for DECd and a negative value for DECm.
;
;  INPUTS
;       RAH - RA hours of input coordinate
;       RAM - RA minutes of input coordinate
;       RAS - RA seconds of input coordinate
;       DECD - DEC hours of input coordinate
;       DECM - DEC minutes of input coordinate
;       DECS - DEC seconds of input coordinate
;
;  OUTPUTS
;       L - Galactic longitude, in decimal form
;       B - Galactic Latitude, in decimal form
;

; Convert coordinates to floating point number type
rah = float(rah)
ram = float(ram)
ras = float(ras)
decd = float(decd)
decm = float(decm)
decs = float(decs)

; Echo the coordinates entered by the user
print, string(rah,ram,ras,$
              format='(" RA =  ",i2,"h",i2,"m",f4.1,"s")')
if (decm lt 0) then begin
    print, string(abs(decm),decs,$
                  format='("DEC =  -0","d",i2,"m",f4.1,"s")')
endif else begin
    print, string(decd,decm,decs,$
                  format='("DEC = ",i3,"d",i2,"m",f4.1,"s")')
endelse

; Convert coordinates to decimal form
rain = (rah + ram/60 + ras/3600)*15.    ;from hours to decimal degrees
case 1 of
    decd gt 0 : decin = decd + decm/60 + decs/3600
    decd eq 0 and decm ge 0 : decin = decm/60 + decs/3600
    decm lt 0 : decin = decm/60 - decs/3600
    else : decin = decd - decm/60 - decs/3600
endcase

; Coordinates of the Galactic North Pole (2000.0)
rag = (12. + 51./60 + 26./3600)*15       ;hours to degrees
decg = 27. + 7./60 + 42./3600

; Convert it all to radians for the trig functions
rag = rag*!pi/180
decg = decg*!pi/180
rain = rain*!pi/180
decin = decin*!pi/180

; Calculate the galactic coordinates
sinb = sin(decg)*sin(decin) + cos(decg)*cos(decin)*cos(rain - rag)
b = asin(sinb)*180/!pi      ;back to degrees
sinthl = cos(decin)*sin(rain - rag)/cos(b*!pi/180)
;WARNING: Ambiguous case of sine here
thl = asin(sinthl)*180/!pi
L = 123 - thl

; Convert from decimal to DMS form
Ld = fix(L)
Lm = fix((L - Ld)*60)
Ls = (((L - Ld)*60) - Lm)*60
bd = fix(b)
bm = fix((b - bd)*60)
bs = abs((((b - bd)*60) - bm)*60)
if (bd lt 0) then bm = abs(bm)

; Display the output coordinates
print, string(Ld,Lm,Ls,$
              format='("L = ",i3,"d",i2,"m",f4.1,"s")')
if (decm lt 0) then begin
    print, string(abs(bm),bs,$
                  format='("b =  -0","d",i2,"m",f4.1,"s")')
endif else begin
    print, string(bd,bm,bs,$
                  format='("b = ",i3,"d",i2,"m",f4.1,"s")')
endelse

END
