PRO ADDTIME, H1, M1, S1, H2, M2, S2, HR, MR, SR, SUB=sub

;  Add or subtract two time values given in hours, minutes, and seconds.
;  
;  HISTORY
;  Written by Will Best (IfA), 09/10/2011
;
;  USE:  addtime, h1, m1, s1, h2, m2, s2[, /sub]
;
;  INPUTS
;       H1 - Hour value of time value #1
;       M1 - Minute value of time value #1
;       S1 - Second value of time value #1
;       H2 - Hour value of time value #2
;       M2 - Minute value of time value #2
;       S2 - Second value of time value #2
;
;  OUTPUTS
;       HR - Resulting hour value
;       MR - Resulting minute value
;       SR - Resulting second value
;
;  KEYWORDS
;       SUB - Subtract time values instead of adding them
;

; Convert coordinates to floating point number type
h1 = float(h1)
m1 = float(m1)
s1 = float(s1)
h2 = float(h2)
m2 = float(m2)
s2 = float(s2)

; Echo the coordinates entered by the user
print, string(h1,m1,s1,$
              format='("Time #1 = ",i2,"h",i2,"m",f4.1,"s")')
if keyword_set(sub) then print, 'minus' else print, 'plus'
print, string(h2,m2,s2,$
                  format='("Time #2 = ",i2,"h",i2,"m",f4.1,"s")')
print

; Convert times to decimal form
time1 = (h1 + m1/60 + s1/3600)
time2 = (h2 + m2/60 + s2/3600)

; Add or subtract, accounting for 24 hour day
if keyword_set(sub) then begin
    timer = time1 - time2
    if timer lt 0 then begin
        timer = timer + 24.
        shift = ' (previous day)'
    endif else shift = ''
endif else begin
    timer = time1 + time2
    if timer ge 24 then begin
        timer = timer - 24.
        shift = ' (next day)'
    endif else shift = ''
endelse

; Convert from decimal to HMS form
hr = fix(timer)
mr = fix((timer - hr)*60)
sr = (((timer - hr)*60) - mr)*60

;Display the output coordinates
print, string(hr,mr,sr,$
              format='("Time = ",i2,"h",i2,"m",f4.1,"s")'), shift

END
