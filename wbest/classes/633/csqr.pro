PRO CSQR, LOW=low, HIGH=high, STEP=step

;  Calculates the value of a specific chi-squared statistic.
;  
;  HISTORY
;  Written by Will Best (IfA), 09/24/2011
;
;  KEYWORDS
;       Low - low value for phase parameter (alpha)
;       High - high value for phase parameter (alpha)
;       Step - increment for phase parameter (alpha)
;

if not keyword_set(low) then low=0
if not keyword_set(high) then high=90
if not keyword_set(step) then step=1

; Observed counts at [0, 90, 180, 270] degrees
co = [5990, 4610, 4120, 5285]
theta = [0., 90., 180., 270.]
tr = theta*!pi/180.

tries = (high-low)/step + 1
aarr = fltarr(tries)
xarr = fltarr(tries)

for i=0, (tries - 1) do begin
    alpha = low + float(i*step)
    print
    print, '            Alpha: ', alpha

; Predicted counts at [0, 90, 180, 270] degrees
    ar = alpha*!pi/180.
    ct = 1000 * cos(tr + ar) + 5000

; Calculate value of chi-squared
    X2 = total( (co - ct)^2 / ct )
    X2r = X2 / (n_elements(co) - 1)
    print, '        Chi-squared: ', X2
    print, 'Reduced Chi-squared: ', X2r

    aarr[i] = alpha
    xarr[i] = X2

endfor

m = where (xarr eq min(xarr))
print
print, string(xarr[m], aarr[m], $
              format='("Minimum chi-squared value of ",f6.4," occurs at ",f4.1," degrees.")')

; Plot a graph of X2 vs. alpha
plot, aarr, xarr, psym=4, yrange=[min(xarr),max(xarr)]
;oplot, aarr, make_array(tries,value=6.398)

END
