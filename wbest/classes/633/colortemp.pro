PRO COLORTEMP, COLOR, L1, L2, COMP

;  Calculates the color temperature of a star given a color.
;  
;  HISTORY
;  Written by Will Best (IfA), 11/14/2011
;
;  INPUTS
;     COLOR - Color measurement of a star, e.g. V-R
;     L1 - Effective wavelength of the bluer filter, in microns.
;     L2 - Effective wavelength of the redder filter, in microns.
;     COMP - -2.5*log(f2/f1) + (m1-m2) for standard star (Vega).
;

color = float(color)
comp = float(comp)
l1 = l1 / 1.e6       ; convert to m
l2 = l2 / 1.e6       ; convert to m

; Fundamental constants
c = 2.998e8          ; m s-1
h = 6.626e-34        ; J s
k = 1.381e-23        ; J K-1

; Temperature and blackbody arrays
T = findgen(1+25000-2500) + 2500.
lrat = (l2/l1)^5
Brat = lrat * (exp(h*c/(l2*k*T)) - 1.) / (exp(h*c/(l1*k*T)) - 1.)

; Temperature Calculation
carr = -2.5*alog10(Brat) + comp
diff = abs(carr - color)
happy = where(min(diff) eq diff)

print, T[happy]

END
