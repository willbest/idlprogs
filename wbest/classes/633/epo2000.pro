PRO EPO2000, RAH, RAM, RAS, DECD, DECM, DECS, EPOCH, RAOUT, DECOUT, $
             TO2000=to2000

;  Converts equatorial coordinates (RA and DEC) from epoch 2000.0 to
;  any desired epoch, or from a given epoch to 2000.0.
;
;  Default is to convert from epoch 2000.0 into the given epoch.
;
;  HISTORY
;  Written by Will Best (IfA), 09/09/2011
;
;  USE:  EPO2000, RAh, RAm, RAs, DECd, DECm, DECs, year, /to2000
;        If the declination is -0dxxmxxs (i.e. negative, but less than
;        1 degree), enter 0 for DECd and a negative value for DECm.
;
;  INPUTS
;       RAH - RA hours of input coordinate
;       RAM - RA minutes of input coordinate
;       RAS - RA seconds of input coordinate
;       DECD - DEC hours of input coordinate
;       DECM - DEC minutes of input coordinate
;       DECS - DEC seconds of input coordinate
;       EPOCH - Epoch to convert to/from 2000.0
;
;  OUTPUTS
;       RAOUT - Converted RA, in decimal form
;       DECOUT - Converted Dec, in decimal form
;
;  KEYWORDS
;       TO2000 - Convert coordinates to epoch 2000.0

; Check for conversion direction, and echo to screen
If keyword_set(to2000) then begin
    print, 'Converting given coordinates from epoch ',strtrim(epoch,1),$
      ' into 2000.0'
    epochin = epoch
    epochout = 2000
endif else begin
    print, 'Converting given coordinates from epoch 2000.0 into ',$
      strtrim(epoch,1)
    epochin = 2000
    epochout = epoch
endelse

; Convert coordinates to floating point number type
rah = float(rah)
ram = float(ram)
ras = float(ras)
decd = float(decd)
decm = float(decm)
decs = float(decs)

; Echo the coordinates entered by the user
print, string(epochin,rah,ram,ras,$
              format='(" RA(",f6.1,") =  ",i2,"h",i2,"m",f4.1,"s")')
if (decm lt 0) then begin
    print, string(epochin,abs(decm),decs,$
                  format='("DEC(",f6.1,") =  -0","d",i2,"m",f4.1,"s")')
endif else begin
    print, string(epochin,decd,decm,decs,$
                  format='("DEC(",f6.1,") = ",i3,"d",i2,"m",f4.1,"s")')
endelse

; Convert coordinates to decimal form
rain = (rah + ram/60 + ras/3600)*15.    ;from hours to decimal degrees
case 1 of
    decd gt 0 : decin = decd + decm/60 + decs/3600
    decd eq 0 and decm ge 0 : decin = decm/60 + decs/3600
    decm lt 0 : decin = decm/60 - decs/3600
    else : decin = decd - decm/60 - decs/3600
endcase

; From 2012 Astronomical Almanac, p. B54
t = (epoch-2000.)/100.
m = 1.2811556689*t + 0.00038655131*t^2 + 0.000010079625*t^3 $
  - 9.60194e-9*T^4 - 1.68806e-10*T^5
n = 0.5567199731*t - 0.00011930372*t^2 - 0.000011617400*t^3 $
  - 1.96917e-9*t^4 - 3.5389e-11*t^5

; Calculate the new coordinates [trig functions require radians]
mraterm = m + n*sin(rain*!pi/180)*tan(decin*!pi/180)
if keyword_set(to2000) then mraterm = -mraterm
alpham = rain + 0.5*mraterm
mdecterm = 0.5*n*cos(alpham*!pi/180)
if keyword_set(to2000) then mdecterm = -mdecterm
deltam = decin + 0.5*mdecterm
raterm = m + n*sin(alpham*!pi/180)*tan(deltam*!pi/180)
if keyword_set(to2000) then raterm = -raterm
raout = rain + raterm
decterm = n*cos(alpham*!pi/180)
if keyword_set(to2000) then decterm = -decterm
decout = decin + decterm

raout = raout/15      ;from degrees back to hours

; Convert from decimal to DMS form
rah2 = fix(raout)
ram2 = fix((raout - rah2)*60)
ras2 = (((raout - rah2)*60) - ram2)*60
decd2 = fix(decout)
decm2 = fix((decout - decd2)*60)
decs2 = abs((((decout - decd2)*60) - decm2)*60)
if (decd2 lt 0) then decm2 = abs(decm2)

; Display the output coordinates
print, string(epochout,rah2,ram2,ras2,$
              format='(" RA(",f6.1,") =  ",i2,"h",i2,"m",f4.1,"s")')
if (decm lt 0) then begin
    print, string(epochout,abs(decm2),decs2,$
                  format='("DEC(",f6.1,") =  -0","d",i2,"m",f4.1,"s")')
endif else begin
    print, string(epochout,decd2,decm2,decs2,$
                  format='("DEC(",f6.1,") = ",i3,"d",i2,"m",f4.1,"s")')
endelse

END

