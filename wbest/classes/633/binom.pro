PRO BINOM, N, P

;  Calculates the terms of a binomial probability distribution.
;  
;  HISTORY
;  Written by Will Best (IfA), 09/17/2011
;
;  INPUTS
;       N - number of trials
;       P - probability of success
;

term = fltarr(n+1,/nozero)

for i=0, n do begin
    term[i] = factorial(n) / (factorial(i)*factorial(n-i)) $
              * p^i * (1-p)^(n-i)
    print, string(i, term[i], format='("Term ",i2,":  ",f8.6)')
endfor

print
print, string(total(term), format='("Total is ",f6.4)')

END
