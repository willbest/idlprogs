PRO SPEAR, A, B

;  Calculates the value of a Spearman Rank Correlation Coefficient for
;  two specific data vectors.
;  
;  HISTORY
;  Written by Will Best (IfA), 09/25/2011
;
;  INPUTS
;       A - First vector
;       B - Second vector
;

; Data arrays
If n_elements(b) eq 0 then begin
    a = [0.27, 2.77, 1.92, 1.00, 3.08, 0.00, 1.01, 2.31, 0.01, 3.15, 0.02, 1.93, 2.54, 0.31, $
         3.00, 0.03, 0.04, 2.32, 0.62, 1.67, 2.62, 3.16, 3.01, 1.68, 3.02, 0.69, 1.02, 2.60]
    b = [98.5, 106.4, 119.4, 119.3, 72.7, 129.2, 110.0, 102.0, 88.0, 98.4, 129.1, 91.3, 78.0, 129.0, $
         84.6, 91.4, 111.0, 73.0, 98.7, 128.8, 85.0, 91.2, 106.3, 128.9, 92.0, 98.6, 119.2, 128.7]
endif

; Create arrays of ranks of the data vectors
arank = rank(a)
brank = rank(b)

; Calculate the Spearman Coefficient
n = n_elements(a)
ds = (arank - brank)^2
dn = n^3 - n
r = 1 - 6*total(ds)/dn

; Report the result
window, 0
plot, arank, brank, psym=4
print, string(r, format='("Spearman r = ",f7.4)')

END
