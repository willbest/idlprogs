PRO SPECPROC, PS=ps

;  Calculations for Homework #9, determining an object's
;  spectrum from a FITS image.
;  
;  HISTORY
;  Written by Will Best (IfA), 11/28/2011
;
;  KEYWORDS
;      PS - write postscript file of the graph
;

; Read in raw image data
xmin = 646
xmax = 1696
ymin = 329
ymax = 861

objpath = '~/Desktop/Astro/classes/633/hw9/wfgs008.fits'
rawobj = mrdfits(objpath, 0, objhead, /fscale , /silent)
tobj = 900.
objgain = 1.74
rawobj = rawobj[xmin:xmax, ymin:ymax]    ;Use only the center part of the images

stdpath = '~/Desktop/Astro/classes/633/hw9/wfgs033.fits'
rawstd = mrdfits(stdpath, 0, stdhead, /fscale , /silent)
tstd = 30.
stdgain = 1.74
rawstd = rawstd[xmin:xmax, ymin:ymax]

arcpath = '~/Desktop/Astro/classes/633/hw9/wfgs030.fits'
rawarc = mrdfits(arcpath, 0, archead, /fscale , /silent)
rawarc = rawarc[xmin:xmax, ymin:ymax]

flatpath = '~/Desktop/Astro/classes/633/hw9/wfgs031.fits'
rawflat = mrdfits(flatpath, 0, flathead, /fscale , /silent)
rawflat = rawflat[xmin:xmax, ymin:ymax]

biaspath = '~/Desktop/Astro/classes/633/hw8/wfgs001.fits'
bias = mrdfits(biaspath, 0, biashead, /fscale , /silent)
bias = bias[xmin:xmax, ymin:ymax]

; Reduce the flat
flat = rawflat - bias
flat = flat / mean(flat)

; Reduce the images
obj = rawobj - bias
obj = obj / flat
std = rawstd - bias
std = std / flat
arc = rawarc - bias
arc = arc / flat

outpath = '~/Desktop/Astro/classes/633/obj.fits'
mwrfits, obj, outpath, rawhead, /create
outpath = '~/Desktop/Astro/classes/633/std.fits'
mwrfits, std, outpath, stdhead, /create
outpath = '~/Desktop/Astro/classes/633/arc.fits'
mwrfits, arc, outpath, archead, /create

; Determine the counts from the object spectrum
chiobj = 1.512                  ; Airmass
objbox = obj[0:1050, 222:226]   ;second brightest object
;objbox = obj[0:1050, 274:278]   ; brightest object
cosmicray = where(objbox ge 10000, c)    ;check for cosmic rays
if c ne 0 then objbox[cosmicray] = (objbox[cosmicray+1] + objbox[cosmicray-1]) / 2
objtot = avg(objbox, 1) * objgain
bgobox = obj[0:1050, 51:151]
bgorate = median(bgobox, dim=2) * objgain
objcounts = objtot - bgorate

; Determine the counts from the standard
chistd = 1.004                  ; Airmass
stdbox = std[0:1050, 176:180]
stdtot = avg(stdbox, 1) * stdgain
bgsbox = std[0:1050, 325:425]
bgsrate = median(bgsbox, dim=2) * stdgain
stdcounts = stdtot - bgsrate

; Use the Arc to calculate pixel position as a function of wavelength
arcbox = arc[0:1050, 200:300]
arccounts = median(arcbox, dim=2)

arclam = [4916.04, 5460.7, 5769.6, 6234.4, 6907.2, 7728.5, 8505.5]    ; Angstroms
dummy = max(arccounts[45:65], n)
arcpix = 45 + n
dummy = max(arccounts[195:215], n)
arcpix = [arcpix, 195 + n]
dummy = max(arccounts[280:300], n)
arcpix = [arcpix, 280 + n]
dummy = max(arccounts[405:415], n)
arcpix = [arcpix, 405 + n]
dummy = max(arccounts[581:591], n)
arcpix = [arcpix, 581 + n]
dummy = max(arccounts[772:782], n)
arcpix = [arcpix, 772 + n]
dummy = max(arccounts[996:1016], n)
arcpix = [arcpix, 996 + n]

fit = poly_fit(arcpix, arclam, 4)
pix = findgen(1051)
lambda = fit[0] + fit[1]*pix + fit[2]*pix^2 + fit[3]*pix^3 + fit[4]*pix^4

; Atmospheric extinction
lamchi = float([4500, 4750, 5000, 5250, 5500, 5750, 6000, 6500, 7000, 8000, 9000])
kappa = [.17, .14, .13, .12, .12, .12, .11, .11, .1, .07, .05]
kappalam = interpol(kappa, lamchi, lambda)
klam = kappalam / (2.5*alog10(exp(1.)))
extinction = exp(-klam * (chiobj-chistd))

; Standard magnitudes
lammag = float([4560, 4760, 4800, 4820, 4860, 4900, 4920, 4960, 5000, 5120, 5240, $
                5400, 5560, 5760, 6020, 6420, 6460, 6500, 6540, 6560, 6580, 6620, $
                6660, 6700, 6740, 6780, 7100, 7460, 7780, 8100, 8380, 8780])
stdmag = [9.637, 9.573, 9.558, 9.574, 9.679, 9.535, 9.533, 9.531, 9.520, 9.491, $
          9.465, 9.434, 9.405, 9.374, 9.337, 9.294, 9.289, 9.290, 9.348, 9.406, $
          9.344, 9.282, 9.274, 9.270, 9.267, 9.262, 9.233, 9.216, 9.211, 9.200, $
          9.194, 9.194]
stdmaglam = interpol(stdmag, lammag, lambda)
stdfnu = 10^(-(stdmaglam + 48.6)/2.5)
stdfnu = stdfnu * 1.e-3         ; convert to W m-2 Hz-1
stdflux = stdfnu * (2.998e8 / (lambda*1e-10)^2)
stdflux = stdflux * 1.e-6       ; convert to W m-2 um-1

; Calculate the flux array for the object spectrum
objflux = (tstd/tobj) * (objcounts/stdcounts) * stdflux / extinction

; Plot the spectrum as a function of wavelength
lincolr, /silent
device, decomposed=0
window, xsize=800, ysize=600
plot, lambda, objflux, xtitle='Wavelength (Angstroms)', color=0, background=1, $
      ytitle='Flux density (W m-2 um-1)', xrange=[4700,8600], yrange=[0,3.e-15], $
      title='Spectrum of Source 3050', charsize=1.5, xstyle=1, ystyle=1
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/633/hw9/spectrum'
    ps_open, fpath, /color, thick=4
    plot, lambda, objflux, xtitle='Wavelength (Angstroms)', color=0, background=1, $
          ytitle='Flux density (W m-2 um-1)', xrange=[4700,8600], yrange=[0,3.e-15], $
          title='Spectrum of Source 3050', charsize=1.5, xstyle=1, ystyle=1
    ps_close
endif

END
