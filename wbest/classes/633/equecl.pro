PRO EQUECL, RAH, RAM, RAS, DECD, DECM, DECS, LAMBDA, BETA

;  Converts equatorial coordinates (RA and DEC) to ecliptic
;  coordinates (lambda and beta).  Uses 23d26m for the obliquity of
;  the ecliptic.
;  
;  HISTORY
;  Written by Will Best (IfA), 09/09/2011
;
;  USE:  EQUECL, RAh, RAm, RAs, DECd, DECm, DECs
;        If the declination is -0dxxmxxs (i.e. negative, but less than
;        1 degree), enter 0 for DECd and a negative value for DECm.
;
;  INPUTS
;       RAH - RA hours of input coordinate
;       RAM - RA minutes of input coordinate
;       RAS - RA seconds of input coordinate
;       DECD - DEC hours of input coordinate
;       DECM - DEC minutes of input coordinate
;       DECS - DEC seconds of input coordinate
;
;  OUTPUTS
;       LAMBDA - Ecliptic longitude, in decimal form
;       BETA - Ecliptic latitude, in decimal form
;

; Convert coordinates to floating point number type
rah = float(rah)
ram = float(ram)
ras = float(ras)
decd = float(decd)
decm = float(decm)
decs = float(decs)

; Echo the coordinates entered by the user
print, string(rah,ram,ras,$
              format='(" RA =  ",i2,"h",i2,"m",f4.1,"s")')
if (decm lt 0) then begin
    print, string(abs(decm),decs,$
                  format='("DEC =  -0","d",i2,"m",f4.1,"s")')
endif else begin
    print, string(decd,decm,decs,$
                  format='("DEC = ",i3,"d",i2,"m",f4.1,"s")')
endelse

; Convert coordinates to decimal form
rain = (rah + ram/60 + ras/3600)*15.    ;from hours to decimal degrees
case 1 of
    decd gt 0 : decin = decd + decm/60 + decs/3600
    decd eq 0 and decm ge 0 : decin = decm/60 + decs/3600
    decm lt 0 : decin = decm/60 - decs/3600
    else : decin = decd - decm/60 - decs/3600
endcase

;Define the obliquity of the ecliptic
eps = 23. + 26./60

; Convert it all to radians for the trig functions
rain = rain*!pi/180
decin = decin*!pi/180
eps = eps*!pi/180

; Calculate the ecliptic coordinates
sinb = sin(decin)*cos(eps) - cos(decin)*sin(eps)*sin(rain)
beta = asin(sinb)*180/!pi      ;back to degrees
cosl = cos(decin)*cos(rain)/cos(beta*!pi/180)
lambda = acos(cosl)

lambda = lambda/15      ;from degrees back to hours

; Convert from decimal to DMS form
ld = fix(lambda)
lm = fix((lambda - ld)*60)
ls = (((lambda - ld)*60) - lm)*60
bd = fix(beta)
bm = fix((beta - bd)*60)
bs = abs((((beta - bd)*60) - bm)*60)
if (bd lt 0) then bm = abs(bm)

; Display the output coordinates
print, string(ld,lm,ls,$
              format='("lambda = ",i3,"d",i2,"m",f4.1,"s")')
if (decm lt 0) then begin
    print, string(abs(bm),bs,$
                  format='("  beta =  -0","d",i2,"m",f4.1,"s")')
endif else begin
    print, string(bd,bm,bs,$
                  format='("  beta = ",i3,"d",i2,"m",f4.1,"s")')
endelse

END
