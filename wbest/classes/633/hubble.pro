PRO HUBBLE

;  Calculations for the optics of the Hubble Space Telescope.
;  
;  HISTORY
;  Written by Will Best (IfA), 10/11/2011
;

; Telescope parameters
D = 2400D                       ;mm
D2 = 266.88D                    ;mm
R1 = -11040D                    ;mm
R2 = -1358D                     ;mm
K1 = -1.0022985D
;K1 = -1.0139D
K2 = -1.496D

; Calculated fundamental values
rho = R2/R1
k = D2/D
f1 = R1/2
f2 = R2/2

; Calculated values
m = rho/(rho - k)
print, 'secondary magnification', m
f = m*f1
print, 'system focal length', f, ' mm'
fn = abs(f/D)
print, 'system f number', fn
ps = abs(206265/f)
print, 'plate scale', ps, ' arcsec/mm'
bfd = abs(k*f*(1/m + 1) - f1)
print, 'back focal distance', bfd, ' mm'
adr = 1.22 * .00055/2400D
print, 'Airy diffraction resolution', adr*180/!dpi*3600, ' arcsec'

; Angular Spherical Aberration (ASA)
beta = abs(bfd/f1)
b3 = -m^3/(32*f^3) * (K1 + 1 - (m-1)^3*(1+beta)/(m^3*(m+1))*(K2 + ((m+1)/(m-1))^2))
print, 'B3', b3
asa = (m/(4*fn))^3 * (K1 + 1 - (m-1)^3*(1+beta)/(m^3*(m+1))*(K2 + ((m+1)/(m-1))^2))
print, 'ASA', abs(asa)*180/!dpi*3600, ' arcsec'

; Angular Tangential Coma
theta = 4D/60*!dpi/180   ;radians
b2 = theta/(4*f^2) * (1 - (m-1)^3*(m-beta)/(2*m*(m+1))*(K2 + ((m+1)/(m-1))^2))
print, 'B2', b2
atc = 3*theta/(4*fn)^2 * (1 - (m-1)^3*(m-beta)/(2*m*(m+1))*(K2 + ((m+1)/(m-1))^2))
print, 'angular tangential coma', abs(atc)*180/!dpi*3600, ' arcsec'

; Astigmatism
b1 = -theta^2/(2*f) * (1 + (m-1)*(m-beta)/(m*(m+1))*(1 - (m-1)^2*(m-beta)/(4*m*(1+beta))*(K2+1)))
print, 'B1', b1
ast = theta^2/(2*fn) * (1 + (m-1)*(m-beta)/(m*(m+1))*(1 - (m-1)^2*(m-beta)/(4*m*(1+beta))*(K2+1)))
print, 'astigmatism', abs(ast)*180/!dpi*3600, ' arcsec'

END
