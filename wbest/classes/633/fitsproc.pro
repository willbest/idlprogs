FUNCTION FITSGAIN, HEAD
g = head[where(strmid(head, 0, 4) eq 'GAIN')]
eqpos = strpos(g, '=')
slpos = strpos(g, '/')
g1 = strmid(g, eqpos+1, slpos-eqpos-1)
g2 = strtrim(g1, 2)
gain = double(g2)
return, gain
END

FUNCTION FITSTIME, HEAD
; Extracts the exposure time from a FITS header.
t = head[where(strmid(head, 0, 7) eq 'EXPTIME')]
eqpos = strpos(t, '=')
slpos = strpos(t, '/')
t1 = strmid(t, eqpos+1, slpos-eqpos-1)
t2 = strtrim(t1, 2)
time = double(t2)
return, time
END


PRO FITSPROC

;  Calculations for Homework #8, determining an object's
;  magnitude in a FITS image.
;  
;  HISTORY
;  Written by Will Best (IfA), 11/21/2011

; Read in raw image data
objpath = '~/Desktop/Astro/classes/633/hw8/wfgs004.fits'
rawobj = mrdfits(objpath, 0, objhead, /fscale , /silent)
tobj = fitstime(objhead)
objgain = fitsgain(objhead)
rawobj = rawobj[700:1400, 250:750]    ;Use only the center part of the images

stdpath = '~/Desktop/Astro/classes/633/hw8/wfgs024.fits'
rawstd = mrdfits(stdpath, 0, stdhead, /fscale , /silent)
tstd = fitstime(stdhead)
stdgain = fitsgain(stdhead)
rawstd = rawstd[700:1400, 250:750]

flatpath = '~/Desktop/Astro/classes/633/hw8/wfgs008.fits'
rawflat = mrdfits(flatpath, 0, flathead, /fscale , /silent)
rawflat = rawflat[700:1400, 250:750]

biaspath = '~/Desktop/Astro/classes/633/hw8/wfgs001.fits'
bias = mrdfits(biaspath, 0, biashead, /fscale , /silent)
bias = bias[700:1400, 250:750]

; Reduce the flat
flat = rawflat - bias
flat = flat / mean(flat)

; Reduce the images
obj = rawobj - bias
obj = obj / flat
std = rawstd - bias
std = std / flat

; Export the source image
outpath = '~/Desktop/Astro/classes/633/obj.fits'
mwrfits, obj, outpath, rawhead, /create
outpath = '~/Desktop/Astro/classes/633/std.fits'
mwrfits, std, outpath, stdhead, /create

; Determine the counts from the object
k_ic = 0.07                     ; Atmospheric extinction coefficient
chiobj = 1.592                  ; Airmass
objbox = obj[387:407, 262:282]
bgobox = obj[427:487, 262:282]
nobj = n_elements(objbox)
nbgobj = n_elements(bgobox)
bgorate = median(bgobox) * objgain
objcounts = total(objbox)*objgain - nobj*bgorate
sigmaobj = sqrt(objcounts + nobj*(1 + nobj/nbgobj)*bgorate)

; Determine the counts from the standard
chistd = 1.509                  ; Airmass
mstd = 13.662 + k_ic*chistd     ; Using object IX-10 for calibration
sigma_mstd = 0.032              ; Using object IX-10 for calibration
stdbox = std[386:406, 151:171]  ; Using object IX-10 for calibration
bgsbox = std[356:444, 119:140]
nstd = n_elements(stdbox)
nbgstd = n_elements(bgsbox)
bgsrate = median(bgsbox) * stdgain
stdcounts = total(stdbox)*stdgain - nstd*bgsrate
sigmastd = sqrt(stdcounts + nstd*(1 + nstd/nbgstd)*bgsrate)

; Calculate the magnitude of the object
mobj = mstd - 2.5 * alog10(objcounts/stdcounts * tstd/tobj) - k_ic*chiobj

; Determine the error
; BETTER METHOD: calculate magnitude many times using many standard stars, and
; the error is the dispersion of these calculated magnitudes.
sigma_mobj = -2.5 * alog10(1 - sqrt(sigmaobj^2/objcounts^2 + sigmastd^2/stdcounts^2))
sigma = sqrt(sigma_mobj^2 + sigma_mstd^2)

print, string(mobj, sigma, format='("object magnitude = ",f6.3," +/- ",f5.3)')

END
