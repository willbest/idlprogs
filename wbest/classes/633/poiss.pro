PRO POISS, N, L, T

;  Calculates the terms of a Poisson probability distribution.
;  
;  HISTORY
;  Written by Will Best (IfA), 09/17/2011
;
;  USE: poiss, n, l[, t]
;
;  INPUTS
;       N - number of terms
;       L - occurrence rate
;       T - time (default value is 1)
;

if n_elements(t) eq 0 then t=1
mu = l*t
term = fltarr(n+1,/nozero)

for i=0, n do begin
    term[i] = mu^i / factorial(i) * exp(-mu)
    print, string(i, term[i], format='("Term ",i2,":  ",f40.38)')
endfor

print
print, string(total(term), format='("Total is ",f40.38)')

END
