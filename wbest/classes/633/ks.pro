PRO KS, A, B

;  Calculates the Kolmogorov-Smirnov statistic, and its significance, for
;  two specific data vectors.
;  
;  HISTORY
;  Written by Will Best (IfA), 09/25/2011
;
;  INPUTS
;       A - First data vector
;       B - Second data vector
;

; Data arrays
If n_elements(b) eq 0 then begin
    a = [39, 32, 42, 37, 34, 38, 42, 34, 30, 26, 38, 43, 33, 34, 34]
    b = [54, 32, 29, 33, 32, 34, 32, 36, 33, 34, 42, 45, 38, 35, 36]
endif
na = n_elements(a)
nb = n_elements(b)

; Sort the data vectors
asort = a[sort(a)]
bsort = b[sort(b)]

Sna = findgen(na + 1) / na
Snb = findgen(nb + 1) / nb

; Calculate and plot the Sn step functions for the data
window, 0
device, decomposed=0
lincolr, /silent
plot, asort, Sna, psym=10, color=0, background=1
oplot, bsort, Snb, psym=10, color=3

; Calculate the Kolmogorov-Smirnov statistic D
asort = [asort, asort[na-1]]
D = 0.
for i=1, na do begin
    spot = where((asort[i-1] le bsort) and (asort[i] ge bsort))
    if (n_elements(spot) eq 1) then begin
        if (spot eq -1) then d1 = 0 else d1 = max(abs(Sna[i] - Snb[spot]))
    endif else d1 = max(abs(Sna[i] - Snb[spot]))
    if d1 gt D then D = d1
endfor

; Calculate the significance of D
N = float(na*nb) / (na + nb)
l = (sqrt(N) + .12 + .11/sqrt(N)) * D
q = 0.
i = 0
ll = -2. * l^2
term = 1.
while abs(term) gt .000001 and i lt 100 do begin
    term = (-1)^i * exp(ll*(i+1)^2)
    q = q + term
    i = i + 1
endwhile
q = q*2

; Report the results
print, string(D, format='("KS D = ",f6.4)')
print, string(q, format='("Significance = ",f6.4)')

END
