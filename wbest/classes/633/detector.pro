;showgraph=1
;PRO DETECTOR, SHOWGRAPH=showgraph

n = 3.45
r = (n-1)^2 / (n+1)^2
alpha = 12.5    ;mm
da = (findgen(1000)+1) / 1000
eta = (1-r)*(1-exp(-alpha*da))

lincolr, /silent
if keyword_set(showgraph) then begin
    device, decomposed=0
    window, 21, retain=2, xsize=800, ysize=600
endif else begin
    fpath = '~/classes/633/eta'
;    fpath = '~/Desktop/Astro\ stuff/classes/633/eta'
    ps_open, fpath, /color, thick=4
endelse
plot, da, eta, xtitle='Absorption Layer Thickness (da)', color=0, background=1, $
  ytitle='Quantum Efficiency (eta)', charsize=1.5
if not keyword_set(showgraph) then ps_close

END
