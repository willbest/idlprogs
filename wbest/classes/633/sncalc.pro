fwhm = 0.8
aper = 4*fwhm
bandpass = .1568
dark = 0.
diam = 2.2
eff = 0.7
lam = .66
mb = 20.3
ms = 25.37
rn = 0.
scale = 1.
tint = 3600.
vegaflux = 2.15e-8
vegam = 0.03
;PRO SNCALC, APER, BANDPASS, DARK, DIAM, EFF, FWHM, LAM, MB, MS, RN, SCALE, TINT, VEGAFLUX, VEGAM

;  Calculates the signal-to-noise ratio for star photometry.
;  
;  HISTORY
;  Written by Will Best (IfA), 11/14/2011
;
;  INPUTS
;     APER - Photometry aperature
;     BANDPASS - bandpass of filter
;     DARK - Dark rate of detector
;     DIAM - Telescope primary diameter
;     EFF - System efficiency
;     FWHM - FWHM of object
;     LAM - Effective wavelength of filter, in microns
;     MB - Background magnitude
;     MS - Star magnitude
;     RN - Read noise of detector
;     SCALE - Solid angle per pixel
;     TINT - integration time
;     VEGAFLUX - Flux of Vega through given filter
;     VEGAM - Magnitude of Vega in given filter
;

; Fundamental constants
c = 2.998e8          ; m s-1
h = 6.626e-34        ; J s

; Number of pixels
area = !pi*(diam/2.)^2
ns = !pi*(aper/2.)^2
;nb = !pi*((aper+8)/2.)^2 - ns

; Set up for the S/N equation
lam = lam / 1.e6
bflux = vegaflux * 10.^(-(mb-vegam)/2.5)
flux = vegaflux * 10.^(-(ms-vegam)/2.5)
B = bflux*bandpass*area*eff*scale*tint / (h*c/lam)
D = dark*ns*tint
F = flux*bandpass*area*eff*tint / (h*c/lam)

; S/N Calculation
;sn = F / sqrt(F + ns*(1 + ns/nb)*(B + D + rn^2))
sn = F / sqrt(F + ns*(B + D + rn^2))
print, 'S/N ratio', sn

snflim = sqrt(flux*bandpass*area*eff*tint / (h*c/lam))
print, 'Flux limited S/N', snflim
;print, sqrt(F)

snblim = flux * sqrt(bandpass*area*eff*tint / ((h*c/lam)*bflux*scale*ns))
print, 'Background limited S/N', snblim
;print, F / sqrt(ns*B)

END
