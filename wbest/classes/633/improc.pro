;PRO IMPROC

;  Calculations for Homework #6, reducing an image.
;  
;  HISTORY
;  Written by Will Best (IfA), 11/6/2011
;

; Raw image data
raw = float([ [1097, 1110, 1095, 1116, 1127], $
        [1075, 1091, 1150, 1087, 1150], $
        [1154, 1135, 1149, 1122, 1169], $
        [1113, 1144, 1148, 1136, 1186], $
        [1108, 1123, 1141, 1190, 1171] ])
traw = 100.
rawflat = float([ [5005, 5102, 4907, 5029, 4970], $
            [4853, 4924, 5155, 4813, 5055], $
            [5134, 5035, 5073, 4888, 5010], $
            [4957, 5121, 5014, 4951, 5124], $
            [4876, 4939, 4981, 5182, 4997] ])
tflat = 50.
rawdark = float([ [111, 111, 111, 132, 151], $
            [122, 122, 122, 144, 163], $
            [133, 133, 133, 156, 175], $
            [144, 144, 144, 168, 187], $
            [155, 155, 155, 179, 199] ])
tdark = 100.
bias = float([ [10, 0, -10, 0, 10], $
         [10, 0, -10, 0, 10], $
         [10, 0, -10, 0, 10], $
         [10, 0, -10, 0, 10], $
         [10, 0, -10, 0, 10] ])

; Reduce the dark
dark = rawdark - bias

; Reduce the flat
flat = rawflat - dark*(tflat/tdark) - bias
flat = flat / mean(flat)

; Reduce the image
new = raw - dark*(traw/tdark) - bias
new = new / flat

; Print the results
print
print, 'Reduced image counts per second'
print, new / traw
print
print, 'Reduced image, normalized to an average of 1'
print, new / mean(new)
print

END
