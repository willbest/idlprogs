PRO PLEIADES, LIKE=like, NBINS=nbins, PS=ps

;+
;  Estimates the IMF of the Pleiades using J-magnitudes and Bayesian inference.
;
;  Model:  hm_pli = high-mass power-law index
;          lm_pli = low-mass power-law index
;          pivot = mass at which index switches
;          amp1 = amplitude (no. of stars) at 1 solar mass
;      Using 50 x 50 x 50 x 50 grid
;  
;  HISTORY
;  Written by Will Best (IfA), 11/04/2013
;
;  KEYWORDS
;      LIKEs - Path to save file containing the likelihoods matrix.  This
;                    can be used to avoid recalculating the likelihoods every time. 
;      NBINS - Number of mass bins for the IMF
;      PS - Output plots to postscript
;-

;print, systime()

; Number of grid points for each model parameter
n_hm_pli = 50
n_lm_pli = 50
n_pivot = 50
n_amp1 = 50
gridsize = [n_hm_pli, n_lm_pli, n_pivot, n_amp1]

; Set up the parameter grid
hm_pli = findgen(n_hm_pli) / ((n_hm_pli - 1.) / 4) - 4
lm_pli = findgen(n_lm_pli) / ((n_lm_pli - 1.) / 4)
log_pivot = findgen(n_pivot) / (n_pivot - 1.) - 1
log_amp1 = findgen(n_amp1) / (n_amp1 - 1.) + 1

; Assume uniform priors for all parameters, in log space
priors = make_array(gridsize, value=0)

; Read in the Pleiades star data
restore, '~/idlprogs/wbest/classes/Bayesian/pleiades_info.sav'

; Convert apparent J-mags to absolute J-mags
dist = 133.                     ; distance to Pleiades = 133 pc
absJ = app2abs(pleiades_jmag, dist)

; Convert absolute J-mags to masses
jmag_order = sort(iso_jmag)
iso_jmag = iso_jmag[jmag_order]
iso_mass = iso_mass[jmag_order]
mass = interpol(iso_mass, iso_jmag, absJ)

; Partition the stars into logarithmic mass bins from -1.2 to 1.0
if n_elements(nbins) eq 0 then nbins = 22
binsize = 2.2/nbins
imf_meas = histogram(alog10(mass), bin=binsize, min=-1.2)
fact_imf_meas = alog(factorial(imf_meas))

; Compute the likelihood of the number of stars in each mass bin, for the full
; parameter grid
massbins = findgen(nbins)*binsize - 1.2
likelihoods = dblarr(gridsize)

; If likelihoods array already exists as a save file, load it.
;    If not, calculate it from scratch.
if keyword_set(like) then begin
    restore, like
endif else begin
    for i=0, n_hm_pli-1 do begin
        for j=0, n_lm_pli-1 do begin
            for k=0, n_pivot-1 do begin
                for l=0, n_amp1-1 do begin
                    ; Compute the expected IMF for each mass bin.
                    imf_expec = makeimf(massbins+(binsize/2.), $
                                        hm_pli[i], lm_pli[j], log_pivot[k], log_amp1[l], /log)
                    ; Compute the likelihood of the number of stars in each mass bin.
                    poisson = find_ln_poisson(imf_meas, 10^imf_expec, factmeas=fact_imf_meas)
                    ; Find the product (sum, in log space) of the likelihoods.
                    likelihoods[i,j,k,l] = total(poisson)
                endfor
            endfor
        endfor
    endfor
    save, likelihoods, filename='~/Astro/classes/Bayesian/likelihoods.sav'
endelse

; Apply Bayes' Theorem (in log space) to find the posterior PDF.
posterior = likelihoods + priors

; Find the sum of the posteriors over the entire grid.
pos_total = alog(total(exp(posterior)))

; Use this sum to normalize the grid and get the final PDF.
posterior = posterior - pos_total

; Convert back to normal space
posterior = exp(posterior)

; Marginalize to find the posterior PDF for each parameter.
post_hm_pli = total(total(total(posterior, 4), 3), 2)
post_lm_pli = total(total(total(posterior, 4), 3), 1)
post_log_pivot = total(total(total(posterior, 4), 2), 1)
post_log_amp1 = total(total(total(posterior, 3), 2), 1)

; Compute best-fit IMF
pmax_hm_pli = max(post_hm_pli, indml_hm_pli)
ml_hm_pli = hm_pli[indml_hm_pli]                 ; ml = most likely
pmax_lm_pli = max(post_lm_pli, indml_lm_pli)
ml_lm_pli = lm_pli[indml_lm_pli]                 ; ml = most likely
pmax_log_pivot = max(post_log_pivot, indml_log_pivot)
ml_log_pivot = log_pivot[indml_log_pivot]                 ; ml = most likely
pmax_log_amp1 = max(post_log_amp1, indml_log_amp1)
ml_log_amp1 = log_amp1[indml_log_amp1]                 ; ml = most likely

imf_bestfit = makeimf(massbins+(binsize/2.), $
                ml_hm_pli, ml_lm_pli, ml_log_pivot, ml_log_amp1, /log)


; Set up for plotting
device, decomposed=0
lincolr_wb, /silent     ; Load color table
if keyword_set(ps) then path = '~/Astro/classes/Bayesian/'
chars = 1.5                                         ; Generic character size for plots

; Histogram of measured pleiades masses, with highest-likelihood model
; overplotted.
if keyword_set(ps) then ps_open, path+'fit_mass_hist', /color, /en, thick=4 else window, 11
plothist, alog10(mass), charsize=chars, backg=1, bin=binsize, color=0, $
          xtit='log mass (Msun)', ytit='Number', /ylog
oplot, massbins+(binsize/2.), 10^imf_bestfit, psym=10, color=3
if keyword_set(ps) then ps_close

; Contour plots for pairs of parameters
covar_12 = total(total(posterior, 4), 3)
if keyword_set(ps) then ps_open, path+'contour_hm_pli_lm_pli', /color, /en, thick=4 else window, 5
contour, covar_12, hm_pli, lm_pli, charsize=chars, backg=1, color=0, $
          xtit='High-mass power law index', ytit='Low-mass power law index', ztit='P'
if keyword_set(ps) then ps_close

covar_13 = total(total(posterior, 4), 2)
if keyword_set(ps) then ps_open, path+'contour_hm_pli_log_pivot', /color, /en, thick=4 else window, 6
contour, covar_13, hm_pli, log_pivot, charsize=chars, backg=1, color=0, $
          xtit='High-mass power law index', ytit='log (pivot mass)', ztit='P'
if keyword_set(ps) then ps_close

covar_14 = total(total(posterior, 3), 2)
if keyword_set(ps) then ps_open, path+'contour_hm_pli_log_amp1', /color, /en, thick=4 else window, 7
contour, covar_14, hm_pli, log_amp1, charsize=chars, backg=1, color=0, $
          xtit='High-mass power law index', ytit='log (N at 1 Msun)', ztit='P'
if keyword_set(ps) then ps_close

covar_23 = total(total(posterior, 4), 1)
if keyword_set(ps) then ps_open, path+'contour_lm_pli_log_pivot', /color, /en, thick=4 else window, 8
contour, covar_23, lm_pli, log_pivot, charsize=chars, backg=1, color=0, $
          xtit='Low-mass power law index', ytit='log (pivot mass)', ztit='P'
if keyword_set(ps) then ps_close

covar_24 = total(total(posterior, 3), 1)
if keyword_set(ps) then ps_open, path+'contour_lm_pli_log_amp1', /color, /en, thick=4 else window, 9
contour, covar_24, lm_pli, log_amp1, charsize=chars, backg=1, color=0, $
          xtit='Low-mass power law index', ytit='log (N at 1 Msun)', ztit='P'
if keyword_set(ps) then ps_close

covar_34 = total(total(posterior, 2), 1)
if keyword_set(ps) then ps_open, path+'contour_log_pivot_log_amp1', /color, /en, thick=4 else window, 10
contour, covar_34, log_pivot, log_amp1, charsize=chars, backg=1, color=0, $
          xtit='log (pivot mass)', ytit='log (N at 1 Msun)', ztit='P'
if keyword_set(ps) then ps_close

; Make plots of the posterior distributions.
if keyword_set(ps) then ps_open, path+'post_hm_pli', /color, /en, thick=4 else window, 1
plot, hm_pli, post_hm_pli, psym=10, charsize=chars, backg=1, color=0, $
          xtit='High-mass power law index', ytit='P'
if keyword_set(ps) then ps_close

if keyword_set(ps) then ps_open, path+'post_lm_pli', /color, /en, thick=4 else window, 2
plot, lm_pli, post_lm_pli, psym=10, charsize=chars, backg=1, color=0, $
          xtit='Low-mass power law index', ytit='P'
if keyword_set(ps) then ps_close

if keyword_set(ps) then ps_open, path+'post_log_pivot', /color, /en, thick=4 else window, 3
plot, log_pivot, post_log_pivot, psym=10, charsize=chars, backg=1, color=0, $
          xtit='log (pivot mass)', ytit='P'
if keyword_set(ps) then ps_close

if keyword_set(ps) then ps_open, path+'post_log_amp1', /color, /en, thick=4 else window, 4
plot, log_amp1, post_log_amp1, psym=10, charsize=chars, backg=1, color=0, $
          xtit='log (N at 1 Msun)', ytit='P'
if keyword_set(ps) then ps_close

; Histogram of measured pleiades masses.
if keyword_set(ps) then ps_open, path+'meas_mass_hist', /color, /en, thick=4 else window, 0
plothist, alog10(mass), charsize=chars, backg=1, bin=binsize, color=0, $
          xtit='log mass (Msun)', ytit='Number', /ylog
if keyword_set(ps) then ps_close


; Compute and print some data
print
medind_hm_pli = closest(total(post_hm_pli, /cumu), .5)
lowind_hm_pli = closest(total(post_hm_pli, /cumu), .16)
highind_hm_pli = closest(total(post_hm_pli, /cumu), .84)
low2ind_hm_pli = closest(total(post_hm_pli, /cumu), .025)
high2ind_hm_pli = closest(total(post_hm_pli, /cumu), .975)
print, 'Median high-mass power-law index: ', hm_pli[medind_hm_pli]
print, '    68% confidence: [', hm_pli[lowind_hm_pli], ', ', hm_pli[highind_hm_pli], ']'
print, '    95% confidence: [', hm_pli[low2ind_hm_pli], ', ', hm_pli[high2ind_hm_pli], ']'
print

medind_lm_pli = closest(total(post_lm_pli, /cumu), .5)
lowind_lm_pli = closest(total(post_lm_pli, /cumu), .16)
highind_lm_pli = closest(total(post_lm_pli, /cumu), .84)
low2ind_lm_pli = closest(total(post_lm_pli, /cumu), .025)
high2ind_lm_pli = closest(total(post_lm_pli, /cumu), .975)
print, 'Median low-mass power-law index: ', lm_pli[medind_lm_pli]
print, '    68% confidence: [', lm_pli[lowind_lm_pli], ', ', lm_pli[highind_lm_pli], ']'
print, '    95% confidence: [', lm_pli[low2ind_lm_pli], ', ', lm_pli[high2ind_lm_pli], ']'
print

medind_log_pivot = closest(total(post_log_pivot, /cumu), .5)
lowind_log_pivot = closest(total(post_log_pivot, /cumu), .16)
highind_log_pivot = closest(total(post_log_pivot, /cumu), .84)
low2ind_log_pivot = closest(total(post_log_pivot, /cumu), .025)
high2ind_log_pivot = closest(total(post_log_pivot, /cumu), .975)
print, 'Median pivot mass: ', 10^log_pivot[medind_log_pivot], 'Msun'
print, '    68% confidence: [', 10^log_pivot[lowind_log_pivot], ', ', 10^log_pivot[highind_log_pivot], ']'
print, '    95% confidence: [', 10^log_pivot[low2ind_log_pivot], ', ', 10^log_pivot[high2ind_log_pivot], ']'
print

medind_log_amp1 = closest(total(post_log_amp1, /cumu), .5)
lowind_log_amp1 = closest(total(post_log_amp1, /cumu), .16)
highind_log_amp1 = closest(total(post_log_amp1, /cumu), .84)
low2ind_log_amp1 = closest(total(post_log_amp1, /cumu), .025)
high2ind_log_amp1 = closest(total(post_log_amp1, /cumu), .975)
print, 'Median N at 1 Msun: ', 10^log_amp1[medind_log_amp1]
print, '    68% confidence: [', 10^log_amp1[lowind_log_amp1], ', ', 10^log_amp1[highind_log_amp1], ']'
print, '    95% confidence: [', 10^log_amp1[low2ind_log_amp1], ', ', 10^log_amp1[high2ind_log_amp1], ']'
print

;print, systime()


END
