FUNCTION MAKEIMF, MASS, HM_PLI, LM_PLI, PIVOT, AMP1, LOG=log

;+
; Returns the expected number of stars of a given mass, for a 2-part power-law
; IMF.
; Normalized by AMP1, the number of 1 Msun stars.
;
; HISTORY
; Written by Will Best (IfA), 2013-11-04
;
; INPUTS
;     MASS = mass(es) at which the IMF is to be computed
;     HM_PLI = high-mass power-law index
;     LM_PLI = low-mass power-law index
;     PIVOT = mass at which index switches
;     AMP1 = amplitude (no. of stars) at 1 solar mass
;
; KEYWORDS
;     LOG - Compute IMF in log space.  PIVOT and AMP1 should be logarithmic.
;-

; Create vector for IMF mass values
imf = fltarr(n_elements(mass))

pli_diff = lm_pli - hm_pli

if keyword_set(log) then begin

; AMP1 normalizes the part of the IMF (high-mass or low-mass) that includes
; mass = 1 Msun.  The other side must be normalized so that the two parts meet
; at the pivot mass, i.e., lm_amp*pivot^lm_pli = hm_amp*pivot^hm_pli.
    if pivot ge 1 then ampx = amp1 + pli_diff*pivot else ampx = amp1 - pli_diff*pivot

; High-mass side of the IMF
    indh = where(mass ge pivot, comp=indl)
    if indh[0] ne -1 then begin            ; make sure at least one mass >= pivot
        imf[indh] = hm_pli*mass[indh]
        if pivot ge 1 then imf[indh] = ampx + imf[indh] else imf[indh] = amp1 + imf[indh]
    endif

; Low-mass side of the IMF
    if indl[0] ne -1 then begin            ; make sure at least one mass < pivot
        imf[indl] = lm_pli*mass[indl]
        if pivot ge 1 then imf[indl] = amp1 + imf[indl] else imf[indl] = ampx + imf[indl]
    endif

endif else begin

; AMP1 normalizes the part of the IMF (high-mass or low-mass) that includes
; mass = 1 Msun.  The other side must be normalized so that the two parts meet
; at the pivot mass, i.e., lm_amp*pivot^lm_pli = hm_amp*pivot^hm_pli.
    if pivot ge 1 then ampx = amp1 * pivot^pli_diff else ampx = amp1 * pivot^(-pli_diff)

; High-mass side of the IMF
    indh = where(mass ge pivot, comp=indl)
    if indh[0] ne -1 then begin            ; make sure at least one mass >= pivot
        imf[indh] = mass[indh]^hm_pli
        if pivot ge 1 then imf[indh] = ampx * imf[indh] else imf[indh] = amp1 * imf[indh]
    endif

; Low-mass side of the IMF
    if indl[0] ne -1 then begin            ; make sure at least one mass < pivot
        imf[indl] = mass[indl]^lm_pli
        if pivot ge 1 then imf[indl] = amp1 * imf[indl] else imf[indl] = ampx * imf[indl]
    endif

endelse

return, imf

END
