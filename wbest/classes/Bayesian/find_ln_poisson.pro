FUNCTION FIND_LN_POISSON, MEASURED_NUM, EXPECTED_NUM, FACTMEAS=factmeas

;+
; Returns the natural log of the Poisson probability, given expected and
; measured occurrence rates.
;
; HISTORY
; Written by Will Best (IfA), 2013-11-04
;
; INPUTS
;     MEASURED_NUM = measured occurrence rate
;     EXPECTED_NUM = expected occurrence rate
;
; KEYWORDS
;     FACTMEAS - Value(s) for alog10(factorial(measured_num)), which can be passed as a
;                keyword so that repeated calls to find_ln_poisson are not
;                slowed down by repeated factorial calculations.
;-

if keyword_set(factmeas) then begin
    ln_poisson = measured_num*alog(expected_num) - expected_num - factmeas
endif else begin
    ln_poisson = measured_num*alog(expected_num) - expected_num - alog(factorial(measured_num))
endelse

return, ln_poisson

END
