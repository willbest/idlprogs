;PRO SAVEBESJ

; Calculate and save the Bessel functions for later use

print, systime()
; etaarr, karr, xarr, Stwiarr
; eta0, Ho, len, lenk
;restore, '~/idlprogs/classes/627/sou.sav'
restore, '~/idlprogs/sou.sav'

lmax = 1200
larr = [2, 3, 4, 6, 8, 10, 12, 15, 20]
larr = [larr, indgen(8)*10 + 30]
larr = [larr, indgen(5)*20 + 120]
larr = [larr, indgen(4)*25 + 225]
larr = [larr, indgen(18)*50 + 350]
lenl = n_elements(larr)

sbjf = dblarr(lenl, len, lenk, /nozero)
for i=0, lenl-1 do begin
    for k=0, lenk-1 do sbjf[i,*,k] = sp_beselj(karr[k]*(eta0 - etaarr), larr[i])
    print, larr[i]
endfor

save, sbjf, lmax, larr, lenl, $
;  filename='~/idlprogs/classes/627/sbjf.sav'
  filename='~/idlprogs/sbjf.sav'

print, systime()

END
