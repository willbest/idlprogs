FUNCTION PEEBLES, z, xevals
return, [xevals[1] * (xevals[2]*(1. - xevals[0]) - xevals[3]*xevals[0]^2) / (-(1.+z)), 0, 0, 0]
END

PRO EXPFUNC, X, A, F, pder
bx = exp(A[1] * X)  
F = A[0] * bx + A[2]
IF N_PARAMS() GE 4 THEN $  
  pder = [[bx], [A[0] * X * bx], [replicate(1.0, N_ELEMENTS(X))]]  
END  

FUNCTION CMBSYSTEM1, x, v
;v = [  (0)phiarr[i,k], (1)vbarr[i,k], (2)deltabarr[i,k], (3)varr[i,k], 
;       (4)deltaarr[i,k], (5)thetaarr[0,i,k], (6)thetaarr[1,i,k],
;       (7)thetaarr[2,i,k], (8)phiprarr[i,k], (9)karr[k], (10)aarr[i], 
;       (11)HHarr[i], (12)psiarr[i,k], (13)Rarr[i], (14)tauprarr[i], 
;       (15)Ho, (16)omegam, (17)omegab, (18)omegar, (19)small, (20)vbprarr[i,k]
return, [ v[12] - v[9]^2*v[0]/(3.*v[11]^2) + v[15]^2/(2.*v[11]^2)*$
             (v[16]*v[4]/v[10] + v[17]*v[2]/v[10] + 4.*v[18]*v[5]/v[10]^2), $
;          -v[1] - v[9]*v[12]/v[11] + v[14]*v[13]*v[10]^2, $
          (-v[1] - v[9]*v[12]/v[11] + v[13]*(v[19] + v[9]/v[11]*(-v[5] + 2.*v[7]) - $
             v[9]*v[12]/v[11])) / (1. + v[13]), $
          v[9]*v[1]/v[11] - 3.*v[8], $
          -v[3] - v[9]*v[12]/v[11], $
          v[9]*v[3]/v[11] - 3.*v[8], $
          -v[9]*v[6]/v[11] - v[8], $
;          v[9]/(3*v[11])*(v[5] - 2*v[7] + v[12]) + v[14]*v[10]^2/3, $
          (v[19] - v[20]) / 3., $
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
END

FUNCTION CMBSYSTEM2, x, v
;v = [  (0)phiarr[i,k], (1)vbarr[i,k], (2)deltabarr[i,k], (3)varr[i,k], 
;       (4)deltaarr[i,k], (5)thetaarr[0,i,k], (6)thetaarr[1,i,k],
;       (7)thetaarr[2,i,k], (8)phiprarr[i,k], (9)karr[k], (10)aarr[i], 
;       (11)HHarr[i], (12)psiarr[i,k], (13)Rarr[i], (14)tauprarr[i], 
;       (15)Ho, (16)omegam, (17)omegab, (18)omegar, (19)vbprarr[i,k]
;       (20)Piarr[i,k], (21)thetaarr[3,i,k], (22)thetaarr[4,i,k], 
;       (23)thetaarr[5,i,k], (24)thetaarr[6,i,k], (25)etaarr[i] ]
return, [ v[12] - v[9]^2*v[0]/(3.*v[11]^2) + v[15]^2/(2.*v[11]^2)*$
             (v[16]*v[4]/v[10] + v[17]*v[2]/v[10] + 4.*v[18]*v[5]/v[10]^2), $
          -v[1] - v[9]*v[12]/v[11] + v[14]*v[13]*(3.*v[6] + v[1]), $
          v[9]*v[1]/v[11] - 3.*v[8], $
          -v[3] - v[9]*v[12]/v[11], $
          v[9]*v[3]/v[11] - 3.*v[8], $
          -v[9]*v[6]/v[11] - v[8], $
          v[9]/(3.*v[11])*(v[5] - 2.*v[7] + v[12]) + v[14]*(v[6] + v[1]/3.), $
          v[9]/(5.*v[11])*(2.*v[6] - 3.*v[21]) + v[14]*(v[7] - v[20]/10.), $
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, $
          v[9]/(7.*v[11])*(3.*v[7] - 4.*v[22]) + v[14]*v[21], $
          v[9]/(9.*v[11])*(4.*v[21] - 5.*v[23]) + v[14]*v[22], $
          v[9]/(11.*v[11])*(5.*v[22] - 6.*v[24]) + v[14]*v[23], $
          v[9]/v[11]*v[23] - (7./(v[11]*v[25]) + v[14])*v[24], $
          0 ]
END


PRO CMBALL, DAMPING=damping, ONEK=onek, POLAR=polar, PS=ps, RK=rk, SHOWGRAPHS=showgraphs, SI=si

;  Plots the power spectrum of the CMB.
;  
;  HISTORY
;  Written by Will Best (IfA), 12/13/2011
;
;  KEYWORDS
;       DAMPING - Include damping factor after tight coupling
;       ONEK - Only calculate for a single value of k = 340Ho
;       POLAR - Include polarization
;       PS - output the CMB plot to postscript file
;       RK - Use Runge-Kutta method for integration
;       SHOWGRAPHS - Display the graphs along the way.
;       SI - use SI units
;
;  REMAINING ISSUES  12/13/11
;     Integration of the system of equations for peturbations (in
;     sourcef.pro).  Currently only Euler's method works, and
;     it produces instabilities in the Source Function.  4th-order
;     Runge-Kutta has not worked.
;
;     The spherical Bessel function program that I'm using
;     (sp_beselj.pro) starts to produce weird spikes for l~800.
;     Better to write my own function, or modify the one I'm using?
;
;     Normalization of the final CMB power spectrum.  Still
;     haven't done it.


; Assume a flat universe.  kappa = 0

lincolr, /silent
device, decomposed=0
print, systime(), '   Starting'

alpha = 1 / 137.036D
h = 0.7D
if keyword_set(si) then begin
; Values for constants, in SI units
; http://pdg.lbl.gov/2011/reviews/rpp2011-rev-phys-constants.pdf
    c = double(2.99792e8)        ;m s-1
    epso = double(1.60218e-19)*13.6057   ;J
    hbar = double(1.05457e-34)   ;J s
    Ho = double(3.24086e-18)*h   ;s-1
    kB = double(1.38065e-23)      ;J K-1
    me = double(9.10938e-31)     ;kg
    mp = double(1.67262e-27)     ;kg
    Omegab = .046D               ;dimensionless
    Omegal = .72995D             ;dimensionless
    Omegam = .224D               ;dimensionless
    Omegar = double(5.042e-5)    ;dimensionless
    rhoc = double(1.87835e-26)*h^2 ;kg m-3
    To = 2.725D                  ;K
endif else begin
; Values for constants, in cosmology units
; http://pdg.lbl.gov/2011/reviews/rpp2011-rev-astrophysical-constants.pdf
    c = 1D
    epso = 13.6057D            ;eV
    hbar = 1D
    Ho = double(2.13317e-33)*h   ;eV hbar-1
    kB = 1D
    me = double(5.109989e5)      ;eV c-2
    mp = double(9.38272e8)    ;eV c-2
    Omegab = .046D               ;dimensionless
    Omegal = .72995D             ;dimensionless
    Omegam = .224D               ;dimensionless
    Omegar = double(5.042e-5)    ;dimensionless
    rhoc = double(8.098e-11)*h^2 ;eV4 hbar-3 c-3
    To = double(2.348e-4)       ;eV kB-1
endelse
mH = mp + me
sigmaT = 8*!dpi*(hbar*alpha)^2 / (3*(me*c)^2)
L21 = 8.227 * Ho / (3.24086e-18*h)     ;not sure why this works (besides units matching)

; Coefficients, to speed things up
cnb = (Omegab*rhoc) / mH
cs = (me/(2.*!dpi*hbar^2))^(-1.5)
cLa = (3*epso)^3 / (8*!dpi)^2      
calpha2 = 64*!dpi*hbar^2*alpha^2 / (sqrt(27*!dpi)*me^2*c)

;Build z array starting at z=1e8
zarr = reverse(dindgen(1000)*100000 + 100000)
zarr = [Zarr, reverse(dindgen(196)*500 + 2000)]
Xearr = replicate(1D,n_elements(zarr))

; Loop to use the Saha equation until Xe reduces to 0.99
Xe = 1D
z = 1999D    ;2500D is safe, 9000 is upper limit
while Xe ge 0.99 do begin
; Parameters
    a = 1 / (1 + z)
    Tb = To / a
    nb = cnb / a^3
; Calculation of Xe
    s = nb * cs * (kB*Tb)^(-1.5) * exp(epso/(kB*Tb))
    Xe = (-1 + sqrt(1 + 4*s)) / (2*s)
    Xearr = [Xearr, Xe]
    Zarr = [Zarr, z]
    z = z - 1
endwhile
Xe0 = Xe

; Time for the Peebles equation, down to z = 100
maxz = z
minz = 100D    ;90D is safe
step = (minz-maxz)/1000
for z = maxz, minz, step do begin
    a = 1 / (1 + z)
    Tb = To / a
    eTk = epso / (Tb*kB)
    nb = cnb / a^3
    Hub = Ho * sqrt((omegam+omegab)*a^(-3) + omegar*a^(-4) + omegal)
    n1s = (1 - Xe)*nb
    La = Hub * cLa / n1s
    phi2 = .448 * alog(eTk)
    alpha2 = calpha2 * sqrt(eTk) * phi2
    beta = alpha2 * (me*Tb*kB/(2*!dpi*hbar^2))^(1.5) * exp(-eTk)
    beta2 = beta * exp(3*eTk/4)
    Cr = (L21 + La) / (L21 + La + beta2)
;; ; Use slope (dXe) to step to next value of Xe (Euler's Method)
    ;; dXe = Cr/Hub * (beta*(1 - Xe) - nb*alpha2*Xe^2) / (-(1+z))
    ;; Xe = Xe + dXe * step
; Use RK4 function (Runge-Kutta) to determine next value of Xe
    xevals = [Xe, Cr/Hub, beta, nb*alpha2]
    dxedz = peebles(z,xevals)
    Xeres = RK4(xevals, dxedz, z, step, 'peebles', /double)
    Xe = Xeres[0]

    Xearr = [Xearr, Xe]
    Zarr = [Zarr, z]
endfor

; Fit an exponential to Xearr for the last values of z.
; Then extend this out to z=0.
lenz = n_elements(Xearr)
zfit = zarr[lenz-21:lenz-1]
Xefit = Xearr[lenz-21:lenz-1]
;Provide an initial guess of the function's parameters.  
param = [.00003, .005, .0002]  
;Compute the parameters, returned in vector param:
;   param[0] * exp(param[1]*z) + param[2]  
expfit = CURVEFIT(zfit, Xefit, weights, param, FUNCTION_NAME='expfunc', /DOUBLE)  
znew = reverse(dindgen((fix(z)+1)))
Xenew = param[0] * exp(param[1]*znew) + param[2]
Xearr = [Xearr, Xenew]
zarr = [zarr, znew]

; Remake the arrays, start with choices for x:
; a[0] = 1e-8   --->  x = -18.421
; during recomb: 200 points evenly distributed in x-space
; after recomb: 300 points evenly distributed in x-space
; 1630.4 < z < 614.2  --->  -7.397 < x < -6.42195
; Calculate a and z based on this, and go from there.
zold = zarr
Xeold = Xearr
xarr = dindgen(200)/200 * (7.397-6.422) - 7.397
xarr = [xarr, dindgen(300)/300 * (6.422) - 6.422]
xxx = 500
xarr = [dindgen(xxx)/xxx * (18.421-7.397) - 18.421, xarr]
len = n_elements(xarr)
aarr = exp(xarr)
zarr = (1/aarr) - 1
Xearr = cspline(zold, Xeold, zarr)

; Plot Xe vs. z
if keyword_set(showgraph) then begin
    window, 21, retain=2, xsize=800, ysize=600
    plot, zarr, Xearr, xtitle='Redshift (z)', color=0, background=1, $ ;xstyle=1, $
          max_val=1,  min_val=0, ytitle='Free Electron Fraction (Xe)',/ylog, $
          title='Free Electron Fraction during Recombination', charsize=1.5, $
          xrange=[1800,0]
endif

; Calculate tau, the optical depth, as a function of x = ln(a)
Hubarr = Ho * sqrt((omegam+omegab)*aarr^(-3) + omegar*aarr^(-4) + omegal)
nbarr = cnb / aarr^3
nearr = Xearr * nbarr
len = n_elements(xarr)
tauprarr = -nearr * sigmaT * c / Hubarr

tau = -tsum(xarr, tauprarr)
tauarr = tau
for i=1L, long(len-1) do begin
    tau = tsum(xarr[0:i],tauprarr[0:i]) + tauarr[0]
    tauarr = [tauarr, tau]
endfor

; Plot tau and tau' vs. x
if keyword_set(showgraph) then begin
    window, 22, retain=2, xsize=800, ysize=600
    plot, xarr, tauarr, xtitle='(x)', color=0, background=1, xstyle=1, ystyle=1, $
          ytitle='Optical Depth (tau, tau'')', xrange=[-19,0], yrange=[1e-9,1e9], $
          title='Optical Depth during Recombination', charsize=1.5, /ylog
    oplot, xarr, abs(tauprarr), linestyle=2, color=0
endif

; Calculate g, the visibility function and its derivatives
gtwiarr = -tauprarr * exp(-tauarr)
gtwiprarr = deriv(xarr,gtwiarr)
gtwiprprarr = deriv(xarr,gtwiprarr)

; Plot g, g', and g'' vs. x
if keyword_set(showgraph) then begin
    window, 23, retain=2, xsize=800, ysize=600
    plot, xarr, gtwiarr, xtitle='(x)', color=0, background=1, $ ;xstyle=1, $
          ytitle='Visibility (g~, g~'', g~'''')', xrange=[-7.5,-6], yrange=[-3,5], $
          title='Visibility during Recombination', charsize=1.5
    oplot, xarr, gtwiprarr/10, linestyle=2, color=0
    oplot, xarr, gtwiprprarr/300, linestyle=3, color=0
endif

!EXCEPT=0
; We'll need these
Rarr = 4*omegar / (3*omegab*aarr)
tauprprarr = deriv(xarr, tauprarr)

; Calculate the values for eta(x)
print, systime(), '   Calculating arrays for eta and k'
HHarr = aarr * Hubarr
HHprarr = deriv(xarr, HHarr)
etaprarr = 1/(aarr*HHarr)
etaarr = 0D
kdinvsqrarr = 0
for i=1, len-1 do begin
    eta = tsum(aarr[0:i],etaprarr[0:i])
    etaarr = [etaarr, eta]
endfor
eta0 = eta

; "It is also sufficient to use 100 different values of k between kmin = 0.1Ho
; and kmax = 1000Ho (for lmax = 1200). A bit of trial and error shows that we
; get good results when the k’s are distributed quadratically, that is, 
; ki = kmin + (kmax − kmin)*(i/100)^2."
if keyword_set(onek) then begin
    lenk = 1
    karr = 340*Ho  ;double(5.0251841e-31)
endif else begin
    lenk = 100
    karr = dindgen(lenk)
    karr = Ho*(.1 + (1000 - .1)*(karr/lenk)^2)
endelse

print, systime(), '   Setting initial conditions'
Piarr = dblarr(len,lenk,/nozero)
psiarr = dblarr(len,lenk,/nozero)
phiarr = dblarr(len,lenk,/nozero)
phiprarr = dblarr(len,lenk,/nozero)
varr = dblarr(len,lenk,/nozero)
vprarr = dblarr(len,lenk,/nozero)
vbarr = dblarr(len,lenk,/nozero)
vbprarr = dblarr(len,lenk,/nozero)
deltaarr = dblarr(len,lenk,/nozero)
deltaprarr = dblarr(len,lenk,/nozero)
deltabarr = dblarr(len,lenk,/nozero)
deltabprarr = dblarr(len,lenk,/nozero)
thetaarr = dblarr(7,len,lenk,/nozero)
thetaprarr = dblarr(7,len,lenk,/nozero)
if keyword_set(polar) then begin
    thetaParr = dblarr(7,len,lenk,/nozero)
    thetaPprarr = dblarr(7,len,lenk,/nozero)
endif 
small = dblarr(len,lenk,/nozero)

; Initial conditions
phiarr[0,*] = 1D
vbarr[0,*] = karr*phiarr[0] / (2*HHarr[0])
varr[0,*] = vbarr[0]
deltabarr[0,*] = 3*phiarr[0] / 2.
deltaarr[0,*] = deltabarr[0]
thetaarr[0,0,*] = phiarr[0] / 2.
thetaarr[1,0,*] = -karr*phiarr[0] / (6.*HHarr[0])
thetaarr[2,0,*] = -8*karr*thetaarr[1,0,*] / (15*HHarr[0]*tauprarr[0])
for l=3, 6 do thetaarr[l,0,*] = -l/(2.*l+1) * karr/(HHarr[0]*tauprarr[0]) * thetaarr[l-1,0,*]
if keyword_set(polar) then begin
    thetaParr[0,0,*] = 5*thetaarr[2,0,*] / 4.
    thetaParr[1,0,*] = -karr*thetaarr[2,0,*] / (4*HHarr[0]*tauprarr[0])
    thetaParr[2,0,*] = thetaarr[2,0,*] / 4.
    for l=3, 6 do thetaParr[l,0,*] = -l/(2.*l+1) * karr/(HHarr[0]*tauprarr[0]) * thetaParr[l-1,0,*]
endif 

print, systime(), '   Solving equations for the tight coupling regime'
; Before recombination = tight coupling regime
switcheroo = max(where(zarr ge 1630.4))    ;from footnote on p. 8
for i=0, switcheroo do begin
    if not keyword_set(polar) then Piarr[i,*] = thetaarr[2,i,*] $
      else Piarr[i,*] = thetaarr[2,i,*] + thetaParr[2,i,*] + thetaParr[0,i,*]
    psiarr[i,*] = -phiarr[i,*] - 12*(Ho/(karr*aarr[i]))^2 * omegar*thetaarr[2,i,*]
    small[i,*] = (-((1-Rarr[i])*tauprarr[i] + (1+Rarr[i])*tauprprarr[i])*$
             (3*thetaarr[1,i,*] + vbarr[i,*]) - karr*psiarr[i,*]/HHarr[i] + $
             (1 - HHprarr[i]/HHarr[i])*karr/HHarr[i]*(-thetaarr[0,i,*] + $
              2*thetaarr[2,i,*]) + karr/HHarr[i]*(-thetaprarr[0,i,*] + $
              2*thetaprarr[2,i,*])) / ((1+Rarr[i])*tauprarr[i] + HHprarr[i]/HHarr[i] - 1 )
    phiprarr[i,*] = psiarr[i,*] - karr^2*phiarr[i,*]/(3*HHarr[i]^2) + Ho^2/(2*HHarr[i]^2)*$
                    (omegam*deltaarr[i,*]/aarr[i] + omegab*deltabarr[i,*]/aarr[i] + $
                     4*omegar*thetaarr[0,i,*]/aarr[i]^2)
    vbprarr[i,*] = (-vbarr[i,*] - karr*psiarr[i,*]/HHarr[i] + Rarr[i]*$
                   (small + karr/HHarr[i]*(-thetaarr[0,i,*] + 2*thetaarr[2,i,*]) - $
                    karr*psiarr[i,*]/HHarr[i])) / (1 + Rarr[i])
    if keyword_set(rk) then begin
    ; Runge-Kutta method for integration
        for k=0, lenk-1 do begin
            vals = [phiarr[i,k], vbarr[i,k], deltabarr[i,k], varr[i,k], $
                    deltaarr[i,k], thetaarr[0,i,k], thetaarr[1,i,k], $
                    thetaarr[2,i,k], phiprarr[i,k], karr[k], aarr[i], $
                    HHarr[i], psiarr[i,k], Rarr[i], tauprarr[i], $
                    Ho, omegam, omegab, omegar, small[i,k], vbprarr[i,k] ]
            ddx = cmbsystem1(xarr[i], vals)
            step = xarr[i+1] - xarr[i]
            rung = rk4(vals, ddx, xarr[i], step, 'cmbsystem1', /DOUBLE)
            phiarr[i+1,k] = rung[0]
            vbarr[i+1,k] = rung[1]
            deltabarr[i+1,k] = rung[2]
            varr[i+1,k] = rung[3]
            deltaarr[i+1,k] = rung[4]
            thetaarr[0,i+1,k] = rung[5]
            thetaarr[1,i+1,k] = rung[6]
        endfor
    endif else begin
    ; Differential equations
        deltabprarr[i,*] = karr*vbarr[i,*]/HHarr[i] - 3*phiprarr[i,*]
        vprarr[i,*] = -varr[i,*] - karr*psiarr[i,*]/HHarr[i]
        deltaprarr[i,*] = karr*varr[i,*]/HHarr[i] - 3*phiprarr[i,*]
        if keyword_set(polar) then thetaPprarr[0,i,*] = -karr*thetaParr[1,i,*]/HHarr[i] + $
          tauprarr[i]*(thetaParr[0,i,*] - .5*Piarr[i,*])
        thetaprarr[0,i,*] = -karr*thetaarr[1,i,*]/HHarr[i] - phiprarr[i,*]
        thetaprarr[1,i,*] = (small - vbprarr[i,*]) / 3D
    ; Euler's Method for integration
        step = xarr[i+1] - xarr[i]
        phiarr[i+1,*] = phiarr[i,*] + phiprarr[i,*] * step
        vbarr[i+1,*] = vbarr[i,*] + vbprarr[i,*] * step
        deltabarr[i+1,*] = deltabarr[i,*] + deltabprarr[i,*] * step
        varr[i+1,*] = varr[i,*] + vprarr[i,*] * step
        deltaarr[i+1,*] = deltaarr[i,*] + deltaprarr[i,*] * step
        if keyword_set(polar) then thetaParr[0,i+1,*] = $
          thetaParr[0,i,*] + thetaPprarr[0,i,*] * step
        for l=0, 1 do thetaarr[l,i+1,*] = thetaarr[l,i,*] + thetaprarr[l,i,*] * step
    ; Direct computation of theta_l for l>=2 and of theta_P_l for l>=1
        thetaarr[2,i+1,*] = -8*karr*thetaarr[1,i+1,*] / (15*HHarr[i+1]*tauprarr[i+1])
        for l=3, 6 do thetaarr[l,i+1,*] = -l/(2.*l+1) * karr/(HHarr[i+1]*tauprarr[i+1]) $
          * thetaarr[l-1,i+1,*]
        if keyword_set(polar) then begin
            thetaParr[1,i+1,*] = -karr*thetaarr[2,i+1,*] / (4*HHarr[i+1]*tauprarr[i+1])
            thetaParr[2,i+1,*] = thetaarr[2,i+1,*] / 4.
            for l=3, 6 do thetaParr[l,i+1,*] = -l/(2.*l+1) * karr/(HHarr[i+1]*tauprarr[i+1]) $
              * thetaParr[l-1,i+1,*]
        endif 
    endelse 
endfor 

print, systime(), '   Solving equations for recombination and later'
; During and after recombination
for i=i, len-1 do begin
    if (keyword_set(damping) and zarr[i] ge 0) then $
      damp = exp(-.0001*(karr/Ho)*(xarr[i]-6.7)/(xarr[i-1] - xarr[i])) else damp = 1D
    if not keyword_set(polar) then Piarr[i,*] = thetaarr[2,i,*] * damp $
      else Piarr[i,*] = (thetaarr[2,i,*] + thetaParr[2,i,*] + thetaParr[0,i,*]) * damp
    psiarr[i,*] = (-phiarr[i,*] - 12D*Ho^2/(karr*aarr[i])^2 * omegar*thetaarr[2,i,*]) * damp
    phiprarr[i,*] = (psiarr[i,*] - karr^2*phiarr[i,*]/(3D*HHarr[i]^2) + Ho^2/(2D*HHarr[i]^2)*$
                    (omegam*deltaarr[i,*]/aarr[i] + omegab*deltabarr[i,*]/aarr[i] + $
                     4D*omegar*thetaarr[0,i,*]/aarr[i]^2)) * damp
    if keyword_set(rk) then begin
    ; Runge-Kutta method for integration
        for k=0, lenk-1 do begin
            vals = [phiarr[i,k]*damp[k], vbarr[i,k]*damp[k], deltabarr[i,k]*damp[k], varr[i,k]*damp[k], $
                    deltaarr[i,k]*damp[k], thetaarr[0,i,k]*damp[k], thetaarr[1,i,k]*damp[k], $
                    thetaarr[2,i,k]*damp[k], phiprarr[i,k], karr[k], aarr[i], $
                    HHarr[i], psiarr[i,k], Rarr[i], tauprarr[i], $
                    Ho, omegam, omegab, omegar, small[i,k], vbprarr[i,k]*damp[k], $
                    Piarr[i,k], thetaarr[3,i,k]*damp[k], thetaarr[4,i,k]*damp[k], $
                    thetaarr[5,i,k]*damp[k], thetaarr[6,i,k]*damp[k], etaarr[i] ]
            ddx = cmbsystem2(xarr[i], vals) 
            if i eq len-1 then begin
                vbprarr[i,k] = ddx[1] 
                deltabprarr[i,k] = ddx[2]
                vprarr[i,k] = ddx[3]
                deltaprarr[i,k] = ddx[4]
                thetaprarr[0,i,k] = ddx[5]
                thetaprarr[1,i,k] = ddx[6]
                thetaprarr[2,i,k] = ddx[7]
                thetaprarr[3,i,k] = ddx[22]
                thetaprarr[4,i,k] = ddx[23]
                thetaprarr[5,i,k] = ddx[24]
                thetaprarr[6,i,k] = ddx[25]
                break
            endif
            step = xarr[i+1] - xarr[i]
            rung = rk4(vals, ddx, xarr[i], step, 'cmbsystem2', /DOUBLE)
            phiarr[i+1,k] = rung[0]
            vbarr[i+1,k] = rung[1]
            deltabarr[i+1,k] = rung[2]
            varr[i+1,k] = rung[3]
            deltaarr[i+1,k] = rung[4]
            thetaarr[0,i+1,k] = rung[5]
            thetaarr[1,i+1,k] = rung[6]
            thetaarr[2,i+1,k] = rung[7]
            thetaarr[3,i+1,k] = rung[22]
            thetaarr[4,i+1,k] = rung[23]
            thetaarr[5,i+1,k] = rung[24]
            thetaarr[6,i+1,k] = rung[25]
        endfor
    endif else begin
    ; Differential equations
        vbprarr[i,*] = (-vbarr[i,*] - karr*psiarr[i,*]/HHarr[i] + tauprarr[i]*Rarr[i]*$
                        (3D*thetaarr[1,i,*] + vbarr[i,*])) * damp
        deltabprarr[i,*] = (karr*vbarr[i,*]/HHarr[i] - 3D*phiprarr[i,*]) * damp
        vprarr[i,*] = (-varr[i,*] - karr*psiarr[i,*]/HHarr[i]) * damp
        deltaprarr[i,*] = (karr*varr[i,*]/HHarr[i] - 3D*phiprarr[i,*]) * damp
        if keyword_set(polar) then begin
            thetaPprarr[0,i,*] = (-karr*thetaParr[1,i,*]/HHarr[i] + $
                                  tauprarr[i]*(thetaParr[0,i,*] - .5*Piarr[i,*])) * damp
            thetaPprarr[1,i,*] = (karr/(3*HHarr[i])*(thetaParr[0,i,*] - 2*thetaParr[2,i,*]) + $
                                  tauprarr[i]*thetaParr[1,i,*]) * damp
            thetaPprarr[2,i,*] = (karr/(5*HHarr[i])*(2*thetaParr[1,i,*] - 3*thetaParr[3,i,*]) + $
                                  tauprarr[i]*(thetaParr[2,i,*] - Piarr[i,*]/10.)) * damp
    ; only need to calculate multipoles up to l=6
            for l=3, 5 do thetaPprarr[l,i,*] = (karr/((2*l+1)*HHarr[i]) * (l*thetaParr[l-1,i,*] - $
                          (l+1)*thetaParr[l+1,i,*]) + tauprarr[i]*thetaParr[l,i,*]) * damp
            thetaPprarr[6,i,*] = (karr/HHarr[i]*thetaParr[5,i,*] - $
                                  (7/(HHarr[i]*etaarr[i]) + tauprarr[i])*thetaParr[6,i,*]) * damp
        endif 
        thetaprarr[0,i,*] = (-karr*thetaarr[1,i,*]/HHarr[i] - phiprarr[i,*]) * damp
        thetaprarr[1,i,*] = (karr/(3D*HHarr[i])*(thetaarr[0,i,*] - 2D*thetaarr[2,i,*] + $
                            psiarr[i,*]) + tauprarr[i]*(thetaarr[1,i,*] + vbarr[i,*]/3D)) * damp
        thetaprarr[2,i,*] = (karr/(5D*HHarr[i])*(2D*thetaarr[1,i,*] - 3D*thetaarr[3,i,*]) + $
                             tauprarr[i]*(thetaarr[2,i,*] - Piarr[i,*]/10D)) * damp
    ; only need to calculate multipoles up to l=6
        for l=3, 5 do thetaprarr[l,i,*] = (karr/((2.*l+1)*HHarr[i]) * (l*thetaarr[l-1,i,*] - $
                            (l+1)*thetaarr[l+1,i,*]) + tauprarr[i]*thetaarr[l,i,*]) * damp
        thetaprarr[6,i,*] = (karr/HHarr[i]*thetaarr[5,i,*] - $
                             (7/(HHarr[i]*etaarr[i]) + tauprarr[i])*thetaarr[6,i,*]) * damp
        if i eq len-1 then break
    ; Euler's Method for integration
        step = xarr[i+1] - xarr[i]
        phiarr[i+1,*] = phiarr[i,*] + phiprarr[i,*] * step
        vbarr[i+1,*] = vbarr[i,*] + vbprarr[i,*] * step
        deltabarr[i+1,*] = deltabarr[i,*] + deltabprarr[i,*] * step
        varr[i+1,*] = varr[i,*] + vprarr[i,*] * step
        deltaarr[i+1,*] = deltaarr[i,*] + deltaprarr[i,*] * step
        if keyword_set(polar) then for l=0, 6 do $
          thetaParr[l,i+1,*] = thetaParr[l,i,*] + thetaPprarr[l,i,*] * step
        for l=0, 6 do $
          thetaarr[l,i+1,*] = thetaarr[l,i,*] + thetaprarr[l,i,*] * step
    endelse 
endfor

print, systime(), '   Calculating the source function'
; Calculate the source function for all values of k and x.
psiprarr = dblarr(len,lenk,/nozero)
Piprarr = dblarr(len,lenk,/nozero)
Piprprarr = dblarr(len,lenk,/nozero)
Hgvprarr = dblarr(len,lenk,/nozero)
HgPprarr = dblarr(len,lenk,/nozero)
HgPprHprarr = dblarr(len,lenk,/nozero)
Stwiarr = dblarr(len,lenk,/nozero)

for k=0, lenk-1 do begin
; Arrays needed to calculate the source function
    psiprarr[*,k] = deriv(xarr, psiarr[*,k])
    Piprarr[*,k] = deriv(xarr, Piarr[*,k])
    Piprprarr[*,k] = deriv(xarr, Piprarr[*,k])
    Hgvprarr[*,k] = deriv(xarr, HHarr*gtwiarr*vbarr[*,k])
    HgPprarr[*,k] = deriv(xarr, HHarr*gtwiarr*Piarr[*,k])
    HgPprHprarr[*,k] = deriv(xarr, HHarr*HHprarr)*gtwiarr*Piarr[*,k] + $
                       3*HHarr*HHprarr*(gtwiprarr*Piarr[*,k] + gtwiarr*Piprarr[*,k]) + $
                       HHarr^2*(gtwiprprarr*Piarr[*,k] + 2*gtwiprarr*Piprarr[*,k] + $
                                gtwiarr*Piprprarr[*,k])
    Stwiarr[*,k] = gtwiarr*(thetaarr[0,*,k] + psiarr[*,k] + Piarr[*,k]/4) + $
                   exp(-tauarr)*(psiprarr[*,k] - phiprarr[*,k]) - $
                   Hgvprarr[*,k]/karr[k] + 3*HgPprHprarr[*,k]/(4*karr[k]^2)
endfor

; Spline to get Stwi and etaarr at intermediate values
;      Don't need to calculate Stwiarr before recombination
xarrold = xarr
lenold = len
xx = 500
xarr0 = xarrold[xx]
len = 5400
xarr = dindgen(len)/len * (-xarr0) + xarr0

Stwiarrold = Stwiarr
Stwiarr = dblarr(len,lenk,/nozero)
for k=0, lenk-1 do Stwiarr[*,k] = cspline(xarrold[xx:*], Stwiarrold[xx:*,k], xarr)

etaarrold = etaarr
etaarr = cspline(xarrold[xx:*], etaarrold[xx:*], xarr)

if keyword_set(showgraph) then begin
    print, systime(), '   Calculating the plot array'
    dummy = min(abs(karr - 340*Ho), kplot)
    st = min(where(xarr ge -8))
    plotintarr = Stwiarr[st:*,kplot] * sp_beselj(karr[kplot]*(eta0 - etaarr[st:*]),100) / 1.e-3
    window, 24, retain=2, xsize=800, ysize=600
    plot, xarr[st:*], plotintarr, xtitle='(x)', color=0, background=1, $ ;xstyle=1, $
          ytitle='Integrand for theta (S~(340Ho,x) * j_100(340Ho*(eta0-eta(x))) / 1e-3)', $
          xrange=[-8,0], title='Source Function Integrand', charsize=1.5;, yrange=[-1,4]
endif 

print, systime(), '   Loading the spherical Bessel j function array'
; sbjf, lmax, larr, lenl
restore, '~/idlprogs/sbjf.sav'

; Spectral index for Harrison-Zel'dovich spectrum
n = 1

; Calculate the theta_l values for x=0
print, systime(), '   Calculating the theta_l values for x=0'
thetalarr = dblarr(lenl, lenk);, /nozero)
for l=0, lenl-1 do begin
    dummy = min(abs(karr - 0.9*larr[l]/eta0), kmin)
    dummy = min(abs(karr - 2.*larr[l]/eta0), kmax)
    for k=kmin, kmax do thetalarr[l,k] = tsum(xarr, (Stwiarr[*,k] * sbjf[l,*,k]))
;    if fix(l)/100 eq fix(l)/100. then print, fix(l)
endfor

;Spline to get Stwi and the theta_l at intermediate k values
karrold = karr
lenkold = lenk
lenk = 5000
karr = Ho*(.1 + (1000 - .1)*(dindgen(lenk)/lenk))

;; Stwiarrold = Stwiarr
;; Stwiarr = dblarr(len,lenk,/nozero)
;; for x=0, len-1 do Stwiarr[x,*] = cspline(karrold, Stwiarrold[x,*], karr)

thetalarrtemp = thetalarr
thetalarr = dblarr(lenl,lenk)
for l=0, lenl-1 do thetalarr[l,*] = cspline(karrold, thetalarrtemp[l,*], karr)

;Plot theta_l^2(k) / k / (1e-6 * Ho^-1) vs. k / Ho
if keyword_set(showgraph) then begin
    print, systime(), '   Calculating the theta_l^2 plot array'
    l = where(larr eq 100)
    plotarr = thetalarr[l,*]^2/(karr) / (1.e-6 * Ho^(-1))
    window, 25, retain=2, xsize=800, ysize=600
    plot, karr/Ho, plotarr, xtitle='(k/Ho)', color=0, background=1, $ ;xstyle=1, $
          ytitle='Integrand for C_l (theta_l^2(k) / (k/Ho) / 1e-6)', xrange=[0,110], $
         title='C_l Coefficient Integrand', charsize=1.5, yrange=[0,2.5]
endif 

; section IV.B. says that we'll need C_l = tsum(karr, (karr/Ho)^(n-1) * (theta_l)^2 / karr)
;     Needs to be normalized.
print, systime(), '   Calculating the C_l coefficients'
Carr = dblarr(lenl, /nozero)
for l=0, lenl-1 do Carr[l] = tsum(karr, (karr/Ho)^(n-1)*thetalarr[l,*]^2/karr)
Carr[lenl-5:*] = 0   ; kill outliers

; One more spline...
larrold = larr
larr = indgen(lmax+1)
Carrold = Carr
Carr = cspline(larrold, Carrold, larr)

;Plot l * (l+1) * C_l / (2*!dpi) / 1e-9 vs. l
print, systime(), '   Plotting the CMB Power Spectrum'
plotcmbarr = larr*(larr+1.) * Carr/(2*!dpi) / 2.e-1
window, 26, retain=2, xsize=800, ysize=600
plot, larr, plotcmbarr, xtitle='moment (l)', color=0, background=1, $ ;xstyle=1, $
      ytitle='l*(l+1) * C_l/(2*pi) / 1e-9', $
      title='CMB Power Spectrum', charsize=1.5, yrange=[0,1]

if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/627/willbest.cmb'
    ps_open, fpath, /color, thick=4
    plot, larr, plotcmbarr, xtitle='moment (l)', color=0, background=1, $ ;xstyle=1, $
          ytitle='l*(l+1) * C_l/(2*pi) / 1e-9', $
          title='CMB Power Spectrum', charsize=1.5, yrange=[0,1]
    ps_close
endif

print, systime(), '   Done'

END
