;v = [  (0)phiarr[i,k], (1)vbarr[i,k], (2)deltabarr[i,k], (3)varr[i,k], 
;       (4)deltaarr[i,k], (5)thetaarr[0,i,k], (6)thetaarr[1,i,k],
;       (7)thetaarr[2,i,k], (8)phiprarr[i,k], (9)karr[k], (10)aarr[i], 
;       (11)HHarr[i], (12)psiarr[i,k], (13)Rarr[i], (14)tauprarr[i], 
;       (15)Ho, (16)omegam, (17)omegab, (18)omegar, (19)small, (20)vbprarr[i,k]
;       (21)Piarr[i,k], (22)thetaarr[3,i,k], (23)thetaarr[4,i,k], 
;       (24)thetaarr[5,i,k], (25)thetaarr[6,i,k], (26)etaarr[i] ]

FUNCTION CMBSYSTEM2, x, v
return, [ v[12] - v[9]^2*v[0]/(3*v[11]^2) + v[15]^2/(2*v[11]^2)*$
             (v[16]*v[4]/v[10] + v[17]*v[2]/v[10] + 4*v[18]*v[5]/v[10]^2), $
          -v[1] - v[9]*v[12]/v[11] + v[14]*v[13]*(3*v[6] + v[1]), $
          v[9]*v[1]/v[11] - 3*v[8], $
          -v[3] - v[9]*v[12]/v[11], $
          v[9]*v[3]/v[11] - 3*v[8], $
          -v[9]*v[6]/v[11] - v[8], $
          v[9]/(3*v[11])*(v[5] - 2*v[7] + v[12]) + v[14]*(3*v[6] + v[1])/3, $
          v[9]/(5*v[11])*(2*v[6] - 3*v[22]) + v[14]*(v[7] - v[21]/10), $
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, $
          v[9]/(7*v[11])*(3*v[7] - 4*v[23]) + v[14]*v[22], $
          v[9]/(9*v[11])*(4*v[22] - 5*v[24]) + v[14]*v[23], $
          v[9]/(11*v[11])*(5*v[23] - 6*v[25]) + v[14]*v[24], $
          v[9]/v[11]*v[24] - (7/(v[11]*v[26]) + v[14])*v[25], $
          0 ]

END

