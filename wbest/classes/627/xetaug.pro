PRO XETAUG, PS=ps, SHOWGRAPH=showgraph, SI=si

;  Plots the electron fraction, optical depth, and visibility functions during
;  recombination.
;  
;  HISTORY
;  Written by Will Best (IfA), 10/08/2011
;  10/23/11: Added tau and g calculations
;  12/08/11: Remake the x and other arrays after the Xe calculation
;
;  KEYWORDS
;       PS - output to postscript file
;       SHOWGRAPH - Display the graphs, instead of writing them to postscript files.
;       SI - use SI units
;

; Assume a flat universe.  kappa = 0

alpha = 1 / 137.036D
h = 0.7D
if keyword_set(si) then begin
; Values for constants, in SI units
; http://pdg.lbl.gov/2011/reviews/rpp2011-rev-phys-constants.pdf
    c = double(2.99792e8)        ;m s-1
    epso = double(1.60218e-19)*13.6057   ;J
    hbar = double(1.05457e-34)   ;J s
    Ho = double(3.24086e-18)*h   ;s-1
    kB = double(1.38065e-23)      ;J K-1
    me = double(9.10938e-31)     ;kg
    mp = double(1.67262e-27)     ;kg
    Omegab = .046D               ;dimensionless
    Omegal = .72995D             ;dimensionless
    Omegam = .224D               ;dimensionless
    Omegar = double(5.042e-5)    ;dimensionless
    rhoc = double(1.87835e-26)*h^2 ;kg m-3
    To = 2.725D                  ;K
endif else begin
; Values for constants, in cosmology units
; http://pdg.lbl.gov/2011/reviews/rpp2011-rev-astrophysical-constants.pdf
    c = 1D
    epso = 13.6057D            ;eV
    hbar = 1D
    Ho = double(2.13317e-33)*h   ;eV hbar-1
    kB = 1D
    me = double(5.109989e5)      ;eV c-2
    mp = double(9.38272e8)    ;eV c-2
    Omegab = .046D               ;dimensionless
    Omegal = .72995D             ;dimensionless
    Omegam = .224D               ;dimensionless
    Omegar = double(5.042e-5)    ;dimensionless
    rhoc = double(8.098e-11)*h^2 ;eV4 hbar-3 c-3
    To = double(2.348e-4)       ;eV kB-1
endelse
mH = mp + me
sigmaT = 8*!dpi*(hbar*alpha)^2 / (3*(me*c)^2)
L21 = 8.227 * Ho / (3.24086e-18*h)     ;not sure why this works (besides units matching)

; Coefficients, to speed things up
cnb = (Omegab*rhoc) / mH
cs = (me/(2.*!dpi*hbar^2))^(-1.5)
cLa = (3*epso)^3 / (8*!dpi)^2      
calpha2 = 64*!dpi*hbar^2*alpha^2 / (sqrt(27*!dpi)*me^2*c)

;Build z array starting at z=1e8
zarr = reverse(dindgen(1000)*100000 + 100000)
zarr = [Zarr, reverse(dindgen(196)*500 + 2000)]
Xearr = replicate(1D,n_elements(zarr))

; Loop to use the Saha equation until Xe reduces to 0.99
Xe = 1D
z = 1999D    ;2500D is safe, 9000 is upper limit
while Xe ge 0.99 do begin
; Parameters
    a = 1 / (1 + z)
    Tb = To / a
    nb = cnb / a^3
; Calculation of Xe
    s = nb * cs * (kB*Tb)^(-1.5) * exp(epso/(kB*Tb))
    Xe = (-1 + sqrt(1 + 4*s)) / (2*s)
    Xearr = [Xearr, Xe]
    Zarr = [Zarr, z]
    z = z - 1
endwhile
Xe0 = Xe

; Time for the Peebles equation, down to z = 100
maxz = z
minz = 100D    ;90D is safe
step = (minz-maxz)/1000
for z = maxz, minz, step do begin
    a = 1 / (1 + z)
    Tb = To / a
    eTk = epso / (Tb*kB)
    nb = cnb / a^3
    Hub = Ho * sqrt((omegam+omegab)*a^(-3) + omegar*a^(-4) + omegal)
    n1s = (1 - Xe)*nb
    La = Hub * cLa / n1s
    phi2 = .448 * alog(eTk)
    alpha2 = calpha2 * sqrt(eTk) * phi2
    beta = alpha2 * (me*Tb*kB/(2*!dpi*hbar^2))^(1.5) * exp(-eTk)
    beta2 = beta * exp(3*eTk/4)
    Cr = (L21 + La) / (L21 + La + beta2)
;; ; Use slope (dXe) to step to next value of Xe (Euler's Method)
    ;; dXe = Cr/Hub * (beta*(1 - Xe) - nb*alpha2*Xe^2) / (-(1+z))
    ;; Xe = Xe + dXe * step
; Use RK4 function (Runge-Kutta) to determine next value of Xe
    xevals = [Xe, Cr/Hub, beta, nb*alpha2]
    dxedz = peebles(z,xevals)
    Xeres = RK4(xevals, dxedz, z, step, 'peebles', /double)
    Xe = Xeres[0]

    Xearr = [Xearr, Xe]
    Zarr = [Zarr, z]
endfor

; Fit an exponential to Xearr for the last values of z.
; Then extend this out to z=0.
lenz = n_elements(Xearr)
zfit = zarr[lenz-21:lenz-1]
Xefit = Xearr[lenz-21:lenz-1]
;Provide an initial guess of the function's parameters.  
param = [.00003, .005, .0002]  
;Compute the parameters, returned in vector param:
;   param[0] * exp(param[1]*z) + param[2]  
expfit = CURVEFIT(zfit, Xefit, weights, param, FUNCTION_NAME='expfunc', /DOUBLE)  
znew = reverse(dindgen((fix(z)+1)))
Xenew = param[0] * exp(param[1]*znew) + param[2]
Xearr = [Xearr, Xenew]
zarr = [zarr, znew]

; Remake the arrays, start with choices for x:
; a[0] = 1e-8   --->  x = -18.421
; during recomb: 200 points evenly distributed in x-space
; after recomb: 300 points evenly distributed in x-space
; 1630.4 < z < 614.2  --->  -7.397 < x < -6.42195
; Calculate a and z based on this, and go from there.
zold = zarr
Xeold = Xearr
xarr = dindgen(200)/200 * (7.397-6.422) - 7.397
xarr = [xarr, dindgen(300)/300 * (6.422) - 6.422]
xxx = 500
xarr = [dindgen(xxx)/xxx * (18.421-7.397) - 18.421, xarr]
len = n_elements(xarr)
aarr = exp(xarr)
zarr = (1/aarr) - 1
Xearr = cspline(zold, Xeold, zarr)

; Plot Xe vs. z
if keyword_set(showgraph) or keyword_set(ps) then begin
    lincolr, /silent
    if keyword_set(showgraph) then begin
        device, decomposed=0
        window, 21, retain=2, xsize=800, ysize=600
    endif else begin
;        fpath = '~/classes/627/willbest.Xe'
        fpath = '~/Desktop/Astro/classes/627/willbest.Xe'
        ps_open, fpath, /color, thick=4
    endelse
    plot, zarr, Xearr, xtitle='Redshift (z)', color=0, background=1, $ ;xstyle=1, $
          max_val=1,  min_val=0, ytitle='Free Electron Fraction (Xe)',/ylog, $
          title='Free Electron Fraction during Recombination', charsize=1.5, $
          xrange=[1800,0]
    if keyword_set(ps) then ps_close
endif

; Calculate tau, the optical depth, as a function of x = ln(a)
Hubarr = Ho * sqrt((omegam+omegab)*aarr^(-3) + omegar*aarr^(-4) + omegal)
nbarr = cnb / aarr^3
nearr = Xearr * nbarr
len = n_elements(xarr)
tauprarr = -nearr * sigmaT * c / Hubarr

tau = -tsum(xarr, tauprarr)
tauarr = tau
for i=1L, long(len-1) do begin
    tau = tsum(xarr[0:i],tauprarr[0:i]) + tauarr[0]
    tauarr = [tauarr, tau]
endfor

; Plot tau and tau' vs. x
if keyword_set(showgraph) or keyword_set(ps) then begin
    if keyword_set(showgraph) then begin
        window, 22, retain=2, xsize=800, ysize=600
    endif else begin
;        fpath = '~/classes/627/willbest.tau'
        fpath = '~/Desktop/Astro/classes/627/willbest.tau'
        ps_open, fpath, /color, thick=4
    endelse
    plot, xarr, tauarr, xtitle='(x)', color=0, background=1, xstyle=1, ystyle=1, $
          ytitle='Optical Depth (tau, tau'')', xrange=[-19,0], yrange=[1e-9,1e9], $
          title='Optical Depth during Recombination', charsize=1.5, /ylog
    oplot, xarr, abs(tauprarr), linestyle=2, color=0
    if keyword_set(ps) then ps_close
endif

; Calculate g, the visibility function and its derivatives
gtwiarr = -tauprarr * exp(-tauarr)
gtwiprarr = deriv(xarr,gtwiarr)
gtwiprprarr = deriv(xarr,gtwiprarr)

; Plot g, g', and g'' vs. x
if keyword_set(showgraph) or keyword_set(ps) then begin
    if keyword_set(showgraph) then begin
        window, 23, retain=2, xsize=800, ysize=600
    endif else begin
;        fpath = '~/classes/627/willbest.g'
        fpath = '~/Desktop/Astro/classes/627/willbest.g'
        ps_open, fpath, /color, thick=4
    endelse
    plot, xarr, gtwiarr, xtitle='(x)', color=0, background=1, $ ;xstyle=1, $
          ytitle='Visibility (g~, g~'', g~'''')', xrange=[-7.5,-6], yrange=[-3,5], $
          title='Visibility during Recombination', charsize=1.5
    oplot, xarr, gtwiprarr/10, linestyle=2, color=0
    oplot, xarr, gtwiprprarr/300, linestyle=3, color=0
    if keyword_set(ps) then ps_close
endif

save, Xearr, Zarr, aarr, xarr, Hubarr, tauarr, tauprarr, gtwiarr, gtwiprarr, gtwiprprarr, $
      nearr, sigmaT, omegam, omegab, omegar, omegal, Ho, len, $
;  filename='~/idlprogs/classes/627/xet.sav'
  filename='~/idlprogs/xet.sav'

END
