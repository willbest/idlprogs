PRO CMBPOWER, PS=ps, SHOWGRAPH=showgraph

;  Plots the power spectrum of the CMB.
;  
;  HISTORY
;  Written by Will Best (IfA), 12/12/2011
;
;  KEYWORDS
;       PS - output to postscript file
;       SHOWGRAPH - Display the intermediate graph.
;

lincolr, /silent
device, decomposed=0

print, systime(), '   Loading the arrays'
; etaarr, karr, xarr, Stwiarr
; eta0, Ho, len, lenk
restore, '~/idlprogs/sou.sav'
; sbjf, lmax, larr, lenl
restore, '~/idlprogs/sbjf.sav'
;; lmax = 1200
;; larr = findgen(lmax+1)
;; lenl = n_elements(larr)

; Spectral index for Harrison-Zel'dovich spectrum
n = 1

; Calculate the theta_l values for x=0
print, systime(), '   Calculating the theta_l values for x=0'
thetalarr = dblarr(lenl, lenk);, /nozero)
for l=0, lenl-1 do begin
    dummy = min(abs(karr - 0.9*larr[l]/eta0), kmin)
    dummy = min(abs(karr - 2.*larr[l]/eta0), kmax)
    for k=kmin, kmax do thetalarr[l,k] = tsum(xarr, (Stwiarr[*,k] * sbjf[l,*,k]))
    ;; for k=kmin, kmax do thetalarr[l,k] = tsum(xarr, (Stwiarr[*,k] * $
    ;;                                       sp_beselj(karr[k]*(eta0 - etaarr),l)))
;    if fix(l)/100 eq fix(l)/100. then print, fix(l)
endfor

;Spline to get Stwi and the theta_l at intermediate k values
karrold = karr
lenkold = lenk
lenk = 5000
karr = Ho*(.1 + (1000 - .1)*(dindgen(lenk)/lenk))

;; Stwiarrold = Stwiarr
;; Stwiarr = dblarr(len,lenk,/nozero)
;; for x=0, len-1 do Stwiarr[x,*] = cspline(karrold, Stwiarrold[x,*], karr)

thetalarrtemp = thetalarr
thetalarr = dblarr(lenl,lenk)
for l=0, lenl-1 do thetalarr[l,*] = cspline(karrold, thetalarrtemp[l,*], karr)

;Plot theta_l^2(k) / k / (1e-6 * Ho^-1) vs. k / Ho
if keyword_set(showgraph) or keyword_set(ps) then begin
    print, systime(), '   Calculating the plot array'
    l = where(larr eq 100)
    plotarr = thetalarr[l,*]^2/(karr) / (1.e-6 * Ho^(-1))
    if keyword_set(showgraph) then begin
        window, 25, retain=2, xsize=800, ysize=600
    endif else begin
        fpath = '~/Desktop/Astro/classes/627/willbest.thetal'
        ps_open, fpath, /color, thick=4
    endelse
    plot, karr/Ho, plotarr, xtitle='(k/Ho)', color=0, background=1, $ ;xstyle=1, $
          ytitle='Integrand for C_l (theta_l^2(k) / (k/Ho) / 1e-6)', xrange=[0,110], $
         title='C_l Coefficient Integrand', charsize=1.5, yrange=[0,2.5]
    if keyword_set(ps) then ps_close
endif 

; section IV.B. says that we'll need C_l = tsum(karr, (karr/Ho)^(n-1) * (theta_l)^2 / karr)
;     Needs to be normalized.
print, systime(), '   Calculating the C_l coefficients'
Carr = dblarr(lenl, /nozero)
for l=0, lenl-1 do Carr[l] = tsum(karr, (karr/Ho)^(n-1)*thetalarr[l,*]^2/karr)
Carr[lenl-5:*] = 0   ; kill outliers

; One more spline...
larrold = larr
larr = indgen(lmax+1)
Carrold = Carr
Carr = cspline(larrold, Carrold, larr)
;; window, 26, retain=2, xsize=800, ysize=600
;; ps_open, 'Desktop/Astro/classes/627/willbest.Clspline', /color, thick=4
;; plot, larrold, 2.*larrold*(larrold+1.)*carrold, psym=4, color=0, background=1, yrange=[0,3]
;; oplot, larr, 2.*larr*(larr+1.)*carr, color=0
;; ps_close
;; stop

;Plot l * (l+1) * C_l / (2*!dpi) / 1e-9 vs. l
print, systime(), '   Plotting the CMB Power Spectrum'
plotcmbarr = larr*(larr+1.) * Carr/(2*!dpi) / 2.e-1
window, 26, retain=2, xsize=800, ysize=600
plot, larr, plotcmbarr, xtitle='moment (l)', color=0, background=1, $ ;xstyle=1, $
      ytitle='l*(l+1) * C_l/(2*pi) / 1e-9', $
      title='CMB Power Spectrum', charsize=1.5, yrange=[0,1]

if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/627/willbest.cmb'
    ps_open, fpath, /color, thick=4
    plot, larr, plotcmbarr, xtitle='moment (l)', color=0, background=1, $ ;xstyle=1, $
          ytitle='l*(l+1) * C_l/(2*pi) / 1e-9', $
          title='CMB Power Spectrum', charsize=1.5, yrange=[0,1]
    ps_close
endif

END
