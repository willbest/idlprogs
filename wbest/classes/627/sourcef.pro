FUNCTION CMBSYSTEM1, x, v
;v = [  (0)phiarr[i,k], (1)vbarr[i,k], (2)deltabarr[i,k], (3)varr[i,k], 
;       (4)deltaarr[i,k], (5)thetaarr[0,i,k], (6)thetaarr[1,i,k],
;       (7)thetaarr[2,i,k], (8)phiprarr[i,k], (9)karr[k], (10)aarr[i], 
;       (11)HHarr[i], (12)psiarr[i,k], (13)Rarr[i], (14)tauprarr[i], 
;       (15)Ho, (16)omegam, (17)omegab, (18)omegar, (19)small, (20)vbprarr[i,k]
return, [ v[12] - v[9]^2*v[0]/(3.*v[11]^2) + v[15]^2/(2.*v[11]^2)*$
             (v[16]*v[4]/v[10] + v[17]*v[2]/v[10] + 4.*v[18]*v[5]/v[10]^2), $
;          -v[1] - v[9]*v[12]/v[11] + v[14]*v[13]*v[10]^2, $
          (-v[1] - v[9]*v[12]/v[11] + v[13]*(v[19] + v[9]/v[11]*(-v[5] + 2.*v[7]) - $
             v[9]*v[12]/v[11])) / (1. + v[13]), $
          v[9]*v[1]/v[11] - 3.*v[8], $
          -v[3] - v[9]*v[12]/v[11], $
          v[9]*v[3]/v[11] - 3.*v[8], $
          -v[9]*v[6]/v[11] - v[8], $
;          v[9]/(3*v[11])*(v[5] - 2*v[7] + v[12]) + v[14]*v[10]^2/3, $
          (v[19] - v[20]) / 3., $
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
END

FUNCTION CMBSYSTEM2, x, v
;v = [  (0)phiarr[i,k], (1)vbarr[i,k], (2)deltabarr[i,k], (3)varr[i,k], 
;       (4)deltaarr[i,k], (5)thetaarr[0,i,k], (6)thetaarr[1,i,k],
;       (7)thetaarr[2,i,k], (8)phiprarr[i,k], (9)karr[k], (10)aarr[i], 
;       (11)HHarr[i], (12)psiarr[i,k], (13)Rarr[i], (14)tauprarr[i], 
;       (15)Ho, (16)omegam, (17)omegab, (18)omegar, (19)vbprarr[i,k]
;       (20)Piarr[i,k], (21)thetaarr[3,i,k], (22)thetaarr[4,i,k], 
;       (23)thetaarr[5,i,k], (24)thetaarr[6,i,k], (25)etaarr[i] ]
return, [ v[12] - v[9]^2*v[0]/(3.*v[11]^2) + v[15]^2/(2.*v[11]^2)*$
             (v[16]*v[4]/v[10] + v[17]*v[2]/v[10] + 4.*v[18]*v[5]/v[10]^2), $
          -v[1] - v[9]*v[12]/v[11] + v[14]*v[13]*(3.*v[6] + v[1]), $
          v[9]*v[1]/v[11] - 3.*v[8], $
          -v[3] - v[9]*v[12]/v[11], $
          v[9]*v[3]/v[11] - 3.*v[8], $
          -v[9]*v[6]/v[11] - v[8], $
          v[9]/(3.*v[11])*(v[5] - 2.*v[7] + v[12]) + v[14]*(v[6] + v[1]/3.), $
          v[9]/(5.*v[11])*(2.*v[6] - 3.*v[21]) + v[14]*(v[7] - v[20]/10.), $
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, $
          v[9]/(7.*v[11])*(3.*v[7] - 4.*v[22]) + v[14]*v[21], $
          v[9]/(9.*v[11])*(4.*v[21] - 5.*v[23]) + v[14]*v[22], $
          v[9]/(11.*v[11])*(5.*v[22] - 6.*v[24]) + v[14]*v[23], $
          v[9]/v[11]*v[23] - (7./(v[11]*v[25]) + v[14])*v[24], $
          0 ]
END

damping=1
;PRO SOURCEF, DAMPING=damping, ONEK=onek, POLAR=polar, PS=ps, RK=rk, SHOWGRAPH=showgraph

;  Plots the power spectrum of the CMB.
;  
;  HISTORY
;  Written by Will Best (IfA), 12/12/2011
;
;  KEYWORDS
;       DAMPING - Include damping factor after tight coupling
;       ONEK - Only calculate for a single value of k = 340Ho
;       POLAR - Include polarization
;       PS - output to postscript file
;       RK - Use Runge-Kutta method for integration
;       SHOWGRAPH - Display the graphs, instead of writing them to postscript files.
;

!EXCEPT=0
;Xearr, zarr, aarr, xarr, nearr, Hubarr, tauarr, tauprarr, gtwiarr, gtwiprarr,
;gtwiprprarr, sigmaT, omegam, omegab, omegar, omegal, Ho, len
;restore, '~/idlprogs/classes/627/xet.sav'
restore, '~/idlprogs/xet.sav'

; We'll need these
Rarr = 4*omegar / (3*omegab*aarr)
tauprprarr = deriv(xarr, tauprarr)

; Calculate the values for eta(x) and for the damping coefficients
print, systime(), '   Calculating arrays for eta and k'
HHarr = aarr * Hubarr
HHprarr = deriv(xarr, HHarr)
etaprarr = 1/(aarr*HHarr)
etaarr = 0D
kdinvsqrarr = 0
for i=1, len-1 do begin
    eta = tsum(aarr[0:i],etaprarr[0:i])
    etaarr = [etaarr, eta]
;    if keyword_set(damping) then begin
;        kdinvsqr = tsum(etaarr, 1/(6*(1+Rarr[0:i])*(nearr*sigmaT*aarr))*$
;                        (Rarr[0:i]^2/(1+Rarr[0:i]) + 8./9))
;        kdinvsqrarr = [kdinvsqrarr, kdinvsqr]
;    endif 
endfor
eta0 = eta

; "It is also sufficient to use 100 different values of k between kmin = 0.1Ho
; and kmax = 1000Ho (for lmax = 1200). A bit of trial and error shows that we
; get good results when the k’s are distributed quadratically, that is, 
; ki = kmin + (kmax − kmin)*(i/100)^2."
if keyword_set(onek) then begin
    lenk = 1
    karr = 340*Ho  ;double(5.0251841e-31)
endif else begin
    lenk = 100
    karr = dindgen(lenk)
    karr = Ho*(.1 + (1000 - .1)*(karr/lenk)^2)
endelse

print, systime(), '   Setting initial conditions'
Piarr = dblarr(len,lenk,/nozero)
psiarr = dblarr(len,lenk,/nozero)
phiarr = dblarr(len,lenk,/nozero)
phiprarr = dblarr(len,lenk,/nozero)
varr = dblarr(len,lenk,/nozero)
vprarr = dblarr(len,lenk,/nozero)
vbarr = dblarr(len,lenk,/nozero)
vbprarr = dblarr(len,lenk,/nozero)
deltaarr = dblarr(len,lenk,/nozero)
deltaprarr = dblarr(len,lenk,/nozero)
deltabarr = dblarr(len,lenk,/nozero)
deltabprarr = dblarr(len,lenk,/nozero)
thetaarr = dblarr(7,len,lenk,/nozero)
thetaprarr = dblarr(7,len,lenk,/nozero)
if keyword_set(polar) then begin
    thetaParr = dblarr(7,len,lenk,/nozero)
    thetaPprarr = dblarr(7,len,lenk,/nozero)
endif 
small = dblarr(len,lenk,/nozero)

; Initial conditions
phiarr[0,*] = 1D
vbarr[0,*] = karr*phiarr[0] / (2*HHarr[0])
varr[0,*] = vbarr[0]
deltabarr[0,*] = 3*phiarr[0] / 2.
deltaarr[0,*] = deltabarr[0]
thetaarr[0,0,*] = phiarr[0] / 2.
thetaarr[1,0,*] = -karr*phiarr[0] / (6.*HHarr[0])
thetaarr[2,0,*] = -8*karr*thetaarr[1,0,*] / (15*HHarr[0]*tauprarr[0])
for l=3, 6 do thetaarr[l,0,*] = -l/(2.*l+1) * karr/(HHarr[0]*tauprarr[0]) * thetaarr[l-1,0,*]
if keyword_set(polar) then begin
    thetaParr[0,0,*] = 5*thetaarr[2,0,*] / 4.
    thetaParr[1,0,*] = -karr*thetaarr[2,0,*] / (4*HHarr[0]*tauprarr[0])
    thetaParr[2,0,*] = thetaarr[2,0,*] / 4.
    for l=3, 6 do thetaParr[l,0,*] = -l/(2.*l+1) * karr/(HHarr[0]*tauprarr[0]) * thetaParr[l-1,0,*]
endif 

print, systime(), '   Solving equations for the tight coupling regime'
; Before recombination = tight coupling regime
switcheroo = max(where(zarr ge 1630.4))    ;from footnote on p. 8
for i=0, switcheroo do begin
    if not keyword_set(polar) then Piarr[i,*] = thetaarr[2,i,*] $
      else Piarr[i,*] = thetaarr[2,i,*] + thetaParr[2,i,*] + thetaParr[0,i,*]
    psiarr[i,*] = -phiarr[i,*] - 12*(Ho/(karr*aarr[i]))^2 * omegar*thetaarr[2,i,*]
    small[i,*] = (-((1-Rarr[i])*tauprarr[i] + (1+Rarr[i])*tauprprarr[i])*$
             (3*thetaarr[1,i,*] + vbarr[i,*]) - karr*psiarr[i,*]/HHarr[i] + $
             (1 - HHprarr[i]/HHarr[i])*karr/HHarr[i]*(-thetaarr[0,i,*] + $
              2*thetaarr[2,i,*]) + karr/HHarr[i]*(-thetaprarr[0,i,*] + $
              2*thetaprarr[2,i,*])) / ((1+Rarr[i])*tauprarr[i] + HHprarr[i]/HHarr[i] - 1 )
    phiprarr[i,*] = psiarr[i,*] - karr^2*phiarr[i,*]/(3*HHarr[i]^2) + Ho^2/(2*HHarr[i]^2)*$
                    (omegam*deltaarr[i,*]/aarr[i] + omegab*deltabarr[i,*]/aarr[i] + $
                     4*omegar*thetaarr[0,i,*]/aarr[i]^2)
    vbprarr[i,*] = (-vbarr[i,*] - karr*psiarr[i,*]/HHarr[i] + Rarr[i]*$
                    (small + karr/HHarr[i]*(-thetaarr[0,i,*] + 2*thetaarr[2,i,*]) - $
                     karr*psiarr[i,*]/HHarr[i])) / (1 + Rarr[i])
    if keyword_set(rk) then begin
    ; Runge-Kutta method for integration
        for k=0, lenk-1 do begin
            vals = [phiarr[i,k], vbarr[i,k], deltabarr[i,k], varr[i,k], $
                    deltaarr[i,k], thetaarr[0,i,k], thetaarr[1,i,k], $
                    thetaarr[2,i,k], phiprarr[i,k], karr[k], aarr[i], $
                    HHarr[i], psiarr[i,k], Rarr[i], tauprarr[i], $
                    Ho, omegam, omegab, omegar, small[i,k], vbprarr[i,k] ]
            ddx = cmbsystem1(xarr[i], vals)
            step = xarr[i+1] - xarr[i]
            rung = rk4(vals, ddx, xarr[i], step, 'cmbsystem1', /DOUBLE)
            phiarr[i+1,k] = rung[0]
            vbarr[i+1,k] = rung[1]
            deltabarr[i+1,k] = rung[2]
            varr[i+1,k] = rung[3]
            deltaarr[i+1,k] = rung[4]
            thetaarr[0,i+1,k] = rung[5]
            thetaarr[1,i+1,k] = rung[6]
        endfor
    endif else begin
    ; Differential equations
        vbprarr[i,*] = (-vbarr[i,*] - karr*psiarr[i,*]/HHarr[i] + Rarr[i]*$
                        (small + karr/HHarr[i]*(-thetaarr[0,i,*] + 2*thetaarr[2,i,*]) - $
                         karr*psiarr[i,*]/HHarr[i])) / (1 + Rarr[i])
        deltabprarr[i,*] = karr*vbarr[i,*]/HHarr[i] - 3*phiprarr[i,*]
        vprarr[i,*] = -varr[i,*] - karr*psiarr[i,*]/HHarr[i]
        deltaprarr[i,*] = karr*varr[i,*]/HHarr[i] - 3*phiprarr[i,*]
        if keyword_set(polar) then thetaPprarr[0,i,*] = -karr*thetaParr[1,i,*]/HHarr[i] + $
          tauprarr[i]*(thetaParr[0,i,*] - .5*Piarr[i,*])
        thetaprarr[0,i,*] = -karr*thetaarr[1,i,*]/HHarr[i] - phiprarr[i,*]
        thetaprarr[1,i,*] = (small - vbprarr[i,*]) / 3D
    ; Euler's Method for integration
        step = xarr[i+1] - xarr[i]
        phiarr[i+1,*] = phiarr[i,*] + phiprarr[i,*] * step
        vbarr[i+1,*] = vbarr[i,*] + vbprarr[i,*] * step
        deltabarr[i+1,*] = deltabarr[i,*] + deltabprarr[i,*] * step
        varr[i+1,*] = varr[i,*] + vprarr[i,*] * step
        deltaarr[i+1,*] = deltaarr[i,*] + deltaprarr[i,*] * step
        if keyword_set(polar) then thetaParr[0,i+1,*] = $
          thetaParr[0,i,*] + thetaPprarr[0,i,*] * step
        for l=0, 1 do thetaarr[l,i+1,*] = thetaarr[l,i,*] + thetaprarr[l,i,*] * step
    endelse 
    ; Direct computation of theta_l for l>=2 and of theta_P_l for l>=1
    thetaarr[2,i+1,*] = -8*karr*thetaarr[1,i+1,*] / (15*HHarr[i+1]*tauprarr[i+1])
    for l=3, 6 do thetaarr[l,i+1,*] = -l/(2.*l+1) * karr/(HHarr[i+1]*tauprarr[i+1]) $
      * thetaarr[l-1,i+1,*]
    if keyword_set(polar) then begin
        thetaParr[1,i+1,*] = -karr*thetaarr[2,i+1,*] / (4*HHarr[i+1]*tauprarr[i+1])
        thetaParr[2,i+1,*] = thetaarr[2,i+1,*] / 4.
        for l=3, 6 do thetaParr[l,i+1,*] = -l/(2.*l+1) * karr/(HHarr[i+1]*tauprarr[i+1]) $
          * thetaParr[l-1,i+1,*]
    endif 
endfor 

print, systime(), '   Solving equations for recombination and later'
; During and after recombination
for i=i, len-1 do begin
    if (keyword_set(damping) and zarr[i] ge 0) then $
      damp = exp(-.0001*(karr/Ho)*(xarr[i]-6.713)/(xarr[i-1] - xarr[i])) else damp = replicate(1D,lenk)
;      damp = exp(-.03*(6.713-xarr[i])/(xarr[i] - xarr[i-1])) else damp = 1D
    if not keyword_set(polar) then Piarr[i,*] = thetaarr[2,i,*] * damp $
      else Piarr[i,*] = (thetaarr[2,i,*] + thetaParr[2,i,*] + thetaParr[0,i,*]) * damp
    psiarr[i,*] = (-phiarr[i,*] - 12D*Ho^2/(karr*aarr[i])^2 * omegar*thetaarr[2,i,*]) * damp
    phiprarr[i,*] = (psiarr[i,*] - karr^2*phiarr[i,*]/(3D*HHarr[i]^2) + Ho^2/(2D*HHarr[i]^2)*$
                    (omegam*deltaarr[i,*]/aarr[i] + omegab*deltabarr[i,*]/aarr[i] + $
                     4D*omegar*thetaarr[0,i,*]/aarr[i]^2)) * damp
    if keyword_set(rk) then begin
    ; Runge-Kutta method for integration
        for k=0, lenk-1 do begin
            if n_elements(damp) gt 1 then dampr = damp[k] else dampr = damp
            vals = [phiarr[i,k]*dampr, vbarr[i,k]*dampr, deltabarr[i,k]*dampr, varr[i,k]*dampr,$
                    deltaarr[i,k]*dampr, thetaarr[0,i,k]*dampr, thetaarr[1,i,k]*dampr, $
                    thetaarr[2,i,k]*dampr, phiprarr[i,k], karr[k], aarr[i], $
                    HHarr[i], psiarr[i,k], Rarr[i], tauprarr[i], $
                    Ho, omegam, omegab, omegar, vbprarr[i,k]*dampr, $
                    Piarr[i,k], thetaarr[3,i,k]*dampr, thetaarr[4,i,k]*dampr, $
                    thetaarr[5,i,k]*dampr, thetaarr[6,i,k]*dampr, etaarr[i] ]
            ddx = cmbsystem2(xarr[i], vals) 
            if i eq len-1 then begin
                vbprarr[i,k] = ddx[1] 
                deltabprarr[i,k] = ddx[2]
                vprarr[i,k] = ddx[3]
                deltaprarr[i,k] = ddx[4]
                thetaprarr[0,i,k] = ddx[5]
                thetaprarr[1,i,k] = ddx[6]
                thetaprarr[2,i,k] = ddx[7]
                thetaprarr[3,i,k] = ddx[21]
                thetaprarr[4,i,k] = ddx[22]
                thetaprarr[5,i,k] = ddx[23]
                thetaprarr[6,i,k] = ddx[24]
                break
            endif
            step = xarr[i+1] - xarr[i]
            rung = rk4(vals, ddx, xarr[i], step, 'cmbsystem2', /DOUBLE)
            phiarr[i+1,k] = rung[0]
            vbarr[i+1,k] = rung[1]
            deltabarr[i+1,k] = rung[2]
            varr[i+1,k] = rung[3]
            deltaarr[i+1,k] = rung[4]
            thetaarr[0,i+1,k] = rung[5]
            thetaarr[1,i+1,k] = rung[6]
            thetaarr[2,i+1,k] = rung[7]
            thetaarr[3,i+1,k] = rung[21]
            thetaarr[4,i+1,k] = rung[22]
            thetaarr[5,i+1,k] = rung[23]
            thetaarr[6,i+1,k] = rung[24]
        endfor
    endif else begin
    ; Differential equations
        vbprarr[i,*] = (-vbarr[i,*] - karr*psiarr[i,*]/HHarr[i] + tauprarr[i]*Rarr[i]*$
                        (3D*thetaarr[1,i,*] + vbarr[i,*])) * damp
        deltabprarr[i,*] = (karr*vbarr[i,*]/HHarr[i] - 3D*phiprarr[i,*]) * damp
        vprarr[i,*] = (-varr[i,*] - karr*psiarr[i,*]/HHarr[i]) * damp
        deltaprarr[i,*] = (karr*varr[i,*]/HHarr[i] - 3D*phiprarr[i,*]) * damp
        if keyword_set(polar) then begin
            thetaPprarr[0,i,*] = (-karr*thetaParr[1,i,*]/HHarr[i] + $
                                  tauprarr[i]*(thetaParr[0,i,*] - .5*Piarr[i,*])) * damp
            thetaPprarr[1,i,*] = (karr/(3*HHarr[i])*(thetaParr[0,i,*] - 2*thetaParr[2,i,*]) + $
                                  tauprarr[i]*thetaParr[1,i,*]) * damp
            thetaPprarr[2,i,*] = (karr/(5*HHarr[i])*(2*thetaParr[1,i,*] - 3*thetaParr[3,i,*]) + $
                                  tauprarr[i]*(thetaParr[2,i,*] - Piarr[i,*]/10.)) * damp
    ; only need to calculate multipoles up to l=6
            for l=3, 5 do thetaPprarr[l,i,*] = (karr/((2*l+1)*HHarr[i]) * (l*thetaParr[l-1,i,*] - $
                          (l+1)*thetaParr[l+1,i,*]) + tauprarr[i]*thetaParr[l,i,*]) * damp
            thetaPprarr[6,i,*] = (karr/HHarr[i]*thetaParr[5,i,*] - $
                                  (7/(HHarr[i]*etaarr[i]) + tauprarr[i])*thetaParr[6,i,*]) * damp
        endif 
        thetaprarr[0,i,*] = (-karr*thetaarr[1,i,*]/HHarr[i] - phiprarr[i,*]) * damp
        thetaprarr[1,i,*] = (karr/(3D*HHarr[i])*(thetaarr[0,i,*] - 2D*thetaarr[2,i,*] + $
                            psiarr[i,*]) + tauprarr[i]*(thetaarr[1,i,*] + vbarr[i,*]/3D)) * damp
        thetaprarr[2,i,*] = (karr/(5D*HHarr[i])*(2D*thetaarr[1,i,*] - 3D*thetaarr[3,i,*]) + $
                             tauprarr[i]*(thetaarr[2,i,*] - Piarr[i,*]/10D)) * damp
    ; only need to calculate multipoles up to l=6
        for l=3, 5 do thetaprarr[l,i,*] = (karr/((2.*l+1)*HHarr[i]) * (l*thetaarr[l-1,i,*] - $
                            (l+1)*thetaarr[l+1,i,*]) + tauprarr[i]*thetaarr[l,i,*]) * damp
        thetaprarr[6,i,*] = (karr/HHarr[i]*thetaarr[5,i,*] - $
                             (7/(HHarr[i]*etaarr[i]) + tauprarr[i])*thetaarr[6,i,*]) * damp
        if i eq len-1 then break
    ; Euler's Method for integration
        step = xarr[i+1] - xarr[i]
        phiarr[i+1,*] = phiarr[i,*] + phiprarr[i,*] * step
        vbarr[i+1,*] = vbarr[i,*] + vbprarr[i,*] * step
        deltabarr[i+1,*] = deltabarr[i,*] + deltabprarr[i,*] * step
        varr[i+1,*] = varr[i,*] + vprarr[i,*] * step
        deltaarr[i+1,*] = deltaarr[i,*] + deltaprarr[i,*] * step
        if keyword_set(polar) then for l=0, 6 do $
          thetaParr[l,i+1,*] = thetaParr[l,i,*] + thetaPprarr[l,i,*] * step
        for l=0, 6 do $
          thetaarr[l,i+1,*] = thetaarr[l,i,*] + thetaprarr[l,i,*] * step
    endelse 
endfor

print, systime(), '   Calculating the source function'
; Calculate the source function for all values of k and x.
psiprarr = dblarr(len,lenk,/nozero)
Piprarr = dblarr(len,lenk,/nozero)
Piprprarr = dblarr(len,lenk,/nozero)
Hgvprarr = dblarr(len,lenk,/nozero)
HgPprarr = dblarr(len,lenk,/nozero)
HgPprHprarr = dblarr(len,lenk,/nozero)
Stwiarr = dblarr(len,lenk,/nozero)

for k=0, lenk-1 do begin
; Arrays needed to calculate the source function
    psiprarr[*,k] = deriv(xarr, psiarr[*,k])
    Piprarr[*,k] = deriv(xarr, Piarr[*,k])
    Piprprarr[*,k] = deriv(xarr, Piprarr[*,k])
    Hgvprarr[*,k] = deriv(xarr, HHarr*gtwiarr*vbarr[*,k])
    HgPprarr[*,k] = deriv(xarr, HHarr*gtwiarr*Piarr[*,k])
    HgPprHprarr[*,k] = deriv(xarr, HHarr*HHprarr)*gtwiarr*Piarr[*,k] + $
                       3*HHarr*HHprarr*(gtwiprarr*Piarr[*,k] + gtwiarr*Piprarr[*,k]) + $
                       HHarr^2*(gtwiprprarr*Piarr[*,k] + 2*gtwiprarr*Piprarr[*,k] + $
                                gtwiarr*Piprprarr[*,k])
    Stwiarr[*,k] = gtwiarr*(thetaarr[0,*,k] + psiarr[*,k] + Piarr[*,k]/4) + $
                   exp(-tauarr)*(psiprarr[*,k] - phiprarr[*,k]) - $
                   Hgvprarr[*,k]/karr[k] + 3*HgPprHprarr[*,k]/(4*karr[k]^2)
endfor

; Spline to get Stwi and etaarr at intermediate values
;      Don't need to calculate Stwiarr before recombination
xarrold = xarr
lenold = len
xx = 500
xarr0 = xarrold[xx]
len = 5400
xarr = dindgen(len)/len * (-xarr0) + xarr0

Stwiarrold = Stwiarr
Stwiarr = dblarr(len,lenk,/nozero)
for k=0, lenk-1 do Stwiarr[*,k] = cspline(xarrold[xx:*], Stwiarrold[xx:*,k], xarr)

etaarrold = etaarr
etaarr = cspline(xarrold[xx:*], etaarrold[xx:*], xarr)

if keyword_set(showgraph) or keyword_set(ps) then begin
    print, systime(), '   Calculating the plot array'
    dummy = min(abs(karr - 340*Ho), kplot)
    st = min(where(xarr ge -8))
    plotintarr = Stwiarr[st:*,kplot] * sp_beselj(karr[kplot]*(eta0 - etaarr[st:*]),100) / 1.e-3
    lincolr, /silent
    if keyword_set(showgraph) then begin
        device, decomposed=0
        window, 24, retain=2, xsize=800, ysize=600
    endif else begin
        fpath = '~/Desktop/Astro/classes/627/willbest.Stwi'
        ps_open, fpath, /color, thick=4
    endelse
    plot, xarr[st:*], plotintarr, xtitle='(x)', color=0, background=1, $ ;xstyle=1, $
          ytitle='Integrand for theta (S~(340Ho,x) * j_100(340Ho*(eta0-eta(x))) / 1e-3)', $
          xrange=[-8,0], title='Source Function Integrand', charsize=1.5;, yrange=[-1,4]
    if keyword_set(ps) then ps_close
endif 

save, etaarr, karr, xarr, Stwiarr, $
      eta0, Ho, len, lenk, $
;Xearr, Zarr, aarr, Hubarr, tauarr, tauprarr, gtwiarr, gtwiprarr, gtwiprprarr, $
;nearr, sigmaT, omegam, omegab, omegar, omegal, 
;  filename='~/idlprogs/classes/627/sou.sav'
  filename='~/idlprogs/sou.sav'

END
