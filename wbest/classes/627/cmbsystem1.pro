;v = [  (0)phiarr[i,k], (1)vbarr[i,k], (2)deltabarr[i,k], (3)varr[i,k], 
;       (4)deltaarr[i,k], (5)thetaarr[0,i,k], (6)thetaarr[1,i,k],
;       (7)thetaarr[2,i,k], (8)phiprarr[i,k], (9)karr[k], (10)aarr[i], 
;       (11)HHarr[i], (12)psiarr[i,k], (13)Rarr[i], (14)tauprarr[i], 
;       (15)Ho, (16)omegam, (17)omegab, (18)omegar, (19)small, (20)vbprarr[i,k]

FUNCTION CMBSYSTEM1, x, v
return, [ v[12] - v[9]^2*v[0]/(3*v[11]^2) + v[15]^2/(2*v[11]^2)*$
             (v[16]*v[4]/v[10] + v[17]*v[2]/v[10] + 4*v[18]*v[5]/v[10]^2), $
          -v[1] - v[9]*v[12]/v[11] + v[14]*v[13]*v[10]^2, $
;          (-v[1] - v[9]*v[12]/v[11] + v[13]*(v[19] + v[9]/v[11]*(-v[5] + 2*v[7]) - $
;             v[9]*v[12]/v[11])) / (1 + v[13]), $
          v[9]*v[1]/v[11] - 3*v[8], $
          -v[3] - v[9]*v[12]/v[11], $
          v[9]*v[3]/v[11] - 3*v[8], $
          -v[9]*v[6]/v[11] - v[8], $
          v[9]/(3*v[11])*(v[5] - 2*v[7] + v[12]) + v[14]*v[10]^2/3, $
;          (v[19] - v[20]) / 3, $
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

END

