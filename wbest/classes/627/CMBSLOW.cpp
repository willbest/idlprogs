#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>
#include <cstdlib>
#include <vector>
#include "NR/nr.h"
using namespace std;

const double pi = 2.0*asin(1.0);


//--------the fx I need in tight coupling regime------------
double RK_1(double,double,double,double,double,double,double,double,double,double,int,double,double);

double fphi(double,double,double,double,double,double,double,double,double,double);
double ftheta0(double,double,double,double,double,double,double,double,double,double);
double ftheta1(double,double,double,double,double,double,double,double,double,double);
double fdelta(double,double,double,double,double,double,double,double,double,double);
double fv(double,double,double,double,double,double,double,double,double,double);
double fdelta_b(double,double,double,double,double,double,double,double,double,double);
double fv_b(double,double,double,double,double,double,double,double,double,double);

//--------the fx I need after tight coupling regime-----------
double RK_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double,double,int,double,double);

double fphi_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta0_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta1_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double fdelta_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double fv_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double fdelta_b_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double fv_b_2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

//new variables we have to solve in non-tight coupling...........
double ftheta2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta3(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta4(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta5(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta6(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta_p0(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta_p1(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta_p2(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta_p3(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta_p4(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta_p5(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);

double ftheta_p6(double,double,double,double,double,double,double,double,double,double,double,double,double,double,double
,double,double,double,double,double,double,double,double);


//-------------the following are just some functions of Numerical Recipe--------------

// Driver for routine sphbes
void NR::sphbes(const int n, const DP x, DP &sj, DP &sy, DP &sjp, DP &syp)
{
	const DP RTPIO2=1.253314137315500501;
	DP factor,order,rj,rjp,ry,ryp;

	if (n < 0 || x <= 0.0) nrerror("bad arguments in sphbes");
	order=n+0.5;
	bessjy(x,order,rj,ry,rjp,ryp);
	factor=RTPIO2/sqrt(x);
	sj=factor*rj;
	sy=factor*ry;
	sjp=factor*rjp-sj/(2.0*x);
	syp=factor*ryp-sy/(2.0*x);
}

//bessjy
void NR::bessjy(const DP x, const DP xnu, DP &rj, DP &ry, DP &rjp, DP &ryp)
{
	const int MAXIT=10000;
	const DP EPS=numeric_limits<DP>::epsilon();
	const DP FPMIN=numeric_limits<DP>::min()/EPS;
	const DP XMIN=2.0, PI=3.141592653589793;
	DP a,b,br,bi,c,cr,ci,d,del,del1,den,di,dlr,dli,dr,e,f,fact,fact2,
		fact3,ff,gam,gam1,gam2,gammi,gampl,h,p,pimu,pimu2,q,r,rjl,
		rjl1,rjmu,rjp1,rjpl,rjtemp,ry1,rymu,rymup,rytemp,sum,sum1,
		temp,w,x2,xi,xi2,xmu,xmu2;
	int i,isign,l,nl;

	if (x <= 0.0 || xnu < 0.0)
		nrerror("bad arguments in bessjy");
	nl=(x < XMIN ? int(xnu+0.5) : MAX(0,int(xnu-x+1.5)));
	xmu=xnu-nl;
	xmu2=xmu*xmu;
	xi=1.0/x;
	xi2=2.0*xi;
	w=xi2/PI;
	isign=1;
	h=xnu*xi;
	if (h < FPMIN) h=FPMIN;
	b=xi2*xnu;
	d=0.0;
	c=h;
	for (i=0;i<MAXIT;i++) {
		b += xi2;
		d=b-d;
		if (fabs(d) < FPMIN) d=FPMIN;
		c=b-1.0/c;
		if (fabs(c) < FPMIN) c=FPMIN;
		d=1.0/d;
		del=c*d;
		h=del*h;
		if (d < 0.0) isign = -isign;
		if (fabs(del-1.0) <= EPS) break;
	}
	if (i >= MAXIT)
		nrerror("x too large in bessjy; try asymptotic expansion");
	rjl=isign*FPMIN;
	rjpl=h*rjl;
	rjl1=rjl;
	rjp1=rjpl;
	fact=xnu*xi;
	for (l=nl-1;l>=0;l--) {
		rjtemp=fact*rjl+rjpl;
		fact -= xi;
		rjpl=fact*rjtemp-rjl;
		rjl=rjtemp;
	}
	if (rjl == 0.0) rjl=EPS;
	f=rjpl/rjl;
	if (x < XMIN) {
		x2=0.5*x;
		pimu=PI*xmu;
		fact = (fabs(pimu) < EPS ? 1.0 : pimu/sin(pimu));
		d = -log(x2);
		e=xmu*d;
		fact2 = (fabs(e) < EPS ? 1.0 : sinh(e)/e);
		beschb(xmu,gam1,gam2,gampl,gammi);
		ff=2.0/PI*fact*(gam1*cosh(e)+gam2*fact2*d);
		e=exp(e);
		p=e/(gampl*PI);
		q=1.0/(e*PI*gammi);
		pimu2=0.5*pimu;
		fact3 = (fabs(pimu2) < EPS ? 1.0 : sin(pimu2)/pimu2);
		r=PI*pimu2*fact3*fact3;
		c=1.0;
		d = -x2*x2;
		sum=ff+r*q;
		sum1=p;
		for (i=1;i<=MAXIT;i++) {
			ff=(i*ff+p+q)/(i*i-xmu2);
			c *= (d/i);
			p /= (i-xmu);
			q /= (i+xmu);
			del=c*(ff+r*q);
			sum += del;
			del1=c*p-i*del;
			sum1 += del1;
			if (fabs(del) < (1.0+fabs(sum))*EPS) break;
		}
		if (i > MAXIT)
			nrerror("bessy series failed to converge");
		rymu = -sum;
		ry1 = -sum1*xi2;
		rymup=xmu*xi*rymu-ry1;
		rjmu=w/(rymup-f*rymu);
	} else {
		a=0.25-xmu2;
		p = -0.5*xi;
		q=1.0;
		br=2.0*x;
		bi=2.0;
		fact=a*xi/(p*p+q*q);
		cr=br+q*fact;
		ci=bi+p*fact;
		den=br*br+bi*bi;
		dr=br/den;
		di = -bi/den;
		dlr=cr*dr-ci*di;
		dli=cr*di+ci*dr;
		temp=p*dlr-q*dli;
		q=p*dli+q*dlr;
		p=temp;
		for (i=1;i<MAXIT;i++) {
			a += 2*i;
			bi += 2.0;
			dr=a*dr+br;
			di=a*di+bi;
			if (fabs(dr)+fabs(di) < FPMIN) dr=FPMIN;
			fact=a/(cr*cr+ci*ci);
			cr=br+cr*fact;
			ci=bi-ci*fact;
			if (fabs(cr)+fabs(ci) < FPMIN) cr=FPMIN;
			den=dr*dr+di*di;
			dr /= den;
			di /= -den;
			dlr=cr*dr-ci*di;
			dli=cr*di+ci*dr;
			temp=p*dlr-q*dli;
			q=p*dli+q*dlr;
			p=temp;
			if (fabs(dlr-1.0)+fabs(dli) <= EPS) break;
		}
		if (i >= MAXIT) nrerror("cf2 failed in bessjy");
		gam=(p-f)/q;
		rjmu=sqrt(w/((p-f)*gam+q));
		rjmu=SIGN(rjmu,rjl);
		rymu=rjmu*gam;
		rymup=rymu*(p+q/gam);
		ry1=xmu*xi*rymu-rymup;
	}
	fact=rjmu/rjl;
	rj=rjl1*fact;
	rjp=rjp1*fact;
	for (i=1;i<=nl;i++) {
		rytemp=(xmu+i)*xi2*ry1-rymu;
		rymu=ry1;
		ry1=rytemp;
	}
	ry=rymu;
	ryp=xnu*xi*rymu-ry1;
}

//beschb
void NR::beschb(const DP x, DP &gam1, DP &gam2, DP &gampl, DP &gammi)
{
	const int NUSE1=7, NUSE2=8;
	static const DP c1_d[7] = {
		-1.142022680371168e0,6.5165112670737e-3,
		3.087090173086e-4,-3.4706269649e-6,6.9437664e-9,
		3.67795e-11,-1.356e-13};
	static const DP c2_d[8] = {
		1.843740587300905e0,-7.68528408447867e-2,
		1.2719271366546e-3,-4.9717367042e-6,-3.31261198e-8,
		2.423096e-10,-1.702e-13,-1.49e-15};
	DP xx;
	static Vec_DP c1(c1_d,7),c2(c2_d,8);

	xx=8.0*x*x-1.0;
	gam1=chebev(-1.0,1.0,c1,NUSE1,xx);
	gam2=chebev(-1.0,1.0,c2,NUSE2,xx);
	gampl= gam2-x*gam1;
	gammi= gam2+x*gam1;
}


//chebev
DP NR::chebev(const DP a, const DP b, Vec_I_DP &c, const int m, const DP x)
{
	DP d=0.0,dd=0.0,sv,y,y2;
	int j;

	if ((x-a)*(x-b) > 0.0)
		nrerror("x not in range in routine chebev");
	y2=2.0*(y=(2.0*x-a-b)/(b-a));
	for (j=m-1;j>0;j--) {
		sv=d;
		d=y2*d-dd+c[j];
		dd=sv;
	}
	return y*d-dd+0.5*c[0];
}


//qsimp
DP NR::qsimp(DP func(const DP), const DP a, const DP b)
{
	const int JMAX=20;
	const DP EPS=1.0e-10;
	int j;
	DP s,st,ost=0.0,os=0.0;

	for (j=0;j<JMAX;j++) {
		st=trapzd(func,a,b,j+1);
		s=(4.0*st-ost)/3.0;
		if (j > 5)
			if (fabs(s-os) < EPS*fabs(os) ||
				(s == 0.0 && os == 0.0)) return s;
		os=s;
		ost=st;
	}
	nrerror("Too many steps in routine qsimp");
	return 0.0;
}

//trapzed
DP NR::trapzd(DP func(const DP), const DP a, const DP b, const int n)
{
	DP x,tnm,sum,del;
	static DP s;
	int it,j;

	if (n == 1) {
		return (s=0.5*(b-a)*(func(a)+func(b)));
	} else {
		for (it=1,j=1;j<n-1;j++) it <<= 1;
		tnm=it;
		del=(b-a)/tnm;
		x=a+0.5*del;
		for (sum=0.0,j=0;j<it;j++,x+=del) sum += func(x);
		s=0.5*(s+(b-a)*sum/tnm);
		return s;
	}
}


DP func(const DP x)
{


   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;

   double H_0 = 2.133*h*(1e-33);

   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(x,-3) + Omega_r*pow(x,-4) + Omega_l );


        return 1./(x*x*H);
}

//-------------the following are just some functions of Numerical Recipe--------------




int main()
{

        ofstream outFile( "tau.txt", ios::out );

        if ( !outFile ){
          cerr<<"File could not be opened"<<endl;
          exit( 1 );  
        }

/*
        ofstream outFile2( "photons.txt", ios::out );

        if ( !outFile2 ){
          cerr<<"File could not be opened"<<endl;
          exit( 1 );  
        }

        ofstream outFile3( "polarization.txt", ios::out );

        if ( !outFile3 ){
          cerr<<"File could not be opened"<<endl;
          exit( 1 );  
        }

        ofstream outFile4( "else.txt", ios::out );

        if ( !outFile4 ){
          cerr<<"File could not be opened"<<endl;
          exit( 1 );  
        }
*/

        ofstream outFile5( "result.txt", ios::out );

        if ( !outFile5 ){
        cerr<<"File could not be opened"<<endl;
        exit( 1 );  
  
        }



   double dx;
   double a,h,Omega_b,rho_c,m_H,n_b,m_e,k_b,T_b,factor,Omega_m,Omega_r,Omega_l;
   int i,j;
   double A1,A2,A3;
   double alpha,phi_2,alpha_2,beta,n_1s,beta_2,Lambda_2s1s,H_0,H,Lambda_a,Cr;



//--------calulation of electron fraction----------


   alpha = 1./137.0359997650;

   h = 0.7;
   Omega_m = 0.224;
   Omega_b = 0.046;
   Omega_r = 5.042e-5;
   Omega_l = 0.72995;

   rho_c = 8.098*h*h*(1e-11);
   m_H = 938.738e6;

   m_e = 0.510998902e6;

   k_b = 8.61734215e-5;

   H_0 = 2.133*h*(1e-33);


   vector<double> x(1501);
   vector<double> Xe(1501);
   vector<double> tau(1501);
   vector<double> taup(1501);

   double sigma_T;

   sigma_T = 8*pi*alpha*alpha/(3.*m_e*m_e);  


   //set x-grid => 1000 steps between x = -8~-6 (During Recombination), only 500 steps before -8
   for (i=0;i<1501;i++){

   if (i<=500) x[i] = -8*log(10) + i*(-8.+8*log(10)) /500.;  
   if (i>500)  x[i] = -8 + (i-500.)*(-6.+8.) /1000.;  

   }



   for (i=0;i<1500;i++){

 
   a = exp(x[i]);

   n_b = Omega_b*rho_c/(m_H*a*a*a);




   T_b = k_b*2.725/a;

   factor = (1./n_b)*pow(m_e*T_b/(2.0*pi), 1.5)*exp(-13.605698/T_b);




   A1 = 1.0;
   A2 = factor;
   A3 = -factor;

   Xe[i] = ( -A2+sqrt(A2*A2-4.*A1*A3) ) / (2.*A1);


   H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

   taup[i] = -(Xe[i]*n_b*sigma_T)/H;


   if (Xe[i] < 0.99)  break;


   }

   double dXdx;


   while (i < 1500){

   
   i++;

   dx = x[i]-x[i-1];

   a = exp(x[i-1]);
   n_b = Omega_b*rho_c/(m_H*a*a*a);
   T_b = k_b*2.725/a;
   phi_2 = 0.448*log(13.605698/T_b);
   alpha_2 = 64.*sqrt(pi)*alpha*alpha*sqrt(13.605698/T_b)*phi_2/(m_e*m_e*sqrt(27));
   beta = alpha_2*pow(m_e*T_b/(2.0*pi), 1.5)*exp(-13.605698/T_b);
   n_1s = (1-Xe[i-1])*n_b;
   beta_2 = beta*exp( 3*13.605698/(4.*T_b) );
   Lambda_2s1s = 8.227;       
   Lambda_2s1s = Lambda_2s1s*(2.133*(1e-33))*(3.0856e+19)/100.;
   H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );
   Lambda_a = H*pow(3*13.605698,3) / ( pow(8.*pi,2)*n_1s );
   Cr = (Lambda_2s1s + Lambda_a)/(Lambda_2s1s + Lambda_a + beta_2); 

   dXdx = (Cr/H)*( beta*(1.-Xe[i-1])-n_b*alpha_2*pow(Xe[i-1],2.) );




    if (x[i]<=-8) Xe[i] = 1;

    Xe[i] = Xe[i-1]+ dXdx*dx;


    taup[i] = -(Xe[i]*n_b*sigma_T)/H;


   }


//--------calulation of tau and g,g',g'' ----------
   vector<double> g(1501);
   vector<double> gp(1501);
   vector<double> gpp(1501);

   for (i=0;i<1501;i++){

   
         tau[i]=0;       

         for (j=i; j<1500; j++){


         dx = x[j+1]-x[j];
         tau[i] += dx*(-taup[j]-taup[j+1])/2.;
 

         }

   g[i] = -taup[i]*exp(-tau[i]);

   }


   for (i=0;i<1501;i++){
   gp[i] = (g[i+1]-g[i-1])/(x[i+1]-x[i-1]); 

   if (i==0 || i==1500) gp[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   gp[500]=  gp[499] + (gp[501]-gp[499])*  (x[500]-x[499]) / (x[501]-x[499]);



   for (i=0;i<1501;i++){

   gpp[i] = (gp[i+1]-gp[i-1])/(2.*dx);

   if (i==0 || i==1500) gpp[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   gpp[500]=  gpp[499] + (gpp[501]-gpp[499])*  (x[500]-x[499]) / (x[501]-x[499]);


   //output x,z,Xe,tau,-tau',g,g',g''
   for (i=0;i<1501;i++){
   outFile<<setprecision(8);
   outFile<<setw(10)<<x[i]<<setw(15)<<exp(-x[i])-1<<setw(15)<<Xe[i]<<setw(15);
   outFile<<tau[i]<<setw(15)<<-taup[i]<<setw(15)<<g[i]<<setw(15)<<gp[i]/10.<<setw(15)<<gpp[i]/300.<<endl;
   }


//------getting eta as a function of x and eta_0---------
   vector<double> eta(1501);
   double eta_0;

   for (i=0;i<1501;i++){

   a = exp(x[i]);
   eta[i] = NR::qsimp(func,1e-8,a);

   }

   eta_0 = NR::qsimp(func,1e-8,1.0);
   cout<<eta_0*H_0<<endl;


//------solving coupled differential eqs-----------

   vector<double> psi(1501);
   vector<double> phi(1501);

   vector<double> delta(1501);
   vector<double> delta_b(1501);

   vector<double> v(1501);
   vector<double> v_b(1501);

   vector<vector<double> > theta(7, vector<double>(1501));
   vector<vector<double> > theta_p(7, vector<double>(1501));

   vector<double> PI(1501);

   h =0.7;
   H_0 = 2.133*h*(1e-33);



   vector<double> C_l(120);
   vector<vector<double> > theta_lk(120, vector<double>(501));

   vector<vector<vector<double> > > integral(120, vector<vector<double> >(501, vector<double>(1501) )  ) ;

   double k;  
   int kk,ll,l;
   double k_min,k_max,k1,k2; 

   //turn off loops of l and k and input one pair of (l,k) if one wants to see the curve of source fx

   /*
   cout<<"l :";
   cin>>l;
   cout<<"k(H_0) :";
   cin>>k;
   k=k*H_0;
   */

   for (ll=0;ll<120;ll++){ //runing different l

   l = 10+10*ll; 

   k_min = 0.9*l/eta_0;  //the range of k ????????????
   k_max = 15.*l/eta_0;  

   for (kk=0;kk<501;kk++){ ////runing different k

   k = k_min+(k_max-k_min)*kk/500.;

   cout<<ll<<" "<<kk<<endl;





   // initial condition
   a = 1e-8;
   H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   phi[0]=1.;

   delta[0]=1.5*phi[0];
   delta_b[0]=1.5*phi[0];

   v[0]= (k/(2.*a*H))*phi[0];
   v_b[0]= (k/(2.*a*H))*phi[0];

   theta[0][0]= 0.5*phi[0];
   theta[1][0]= (-k/(6.*a*H))*phi[0];
   // 



   double p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23;


   for (i=0;i<1501;i++){


     a = exp(x[i]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

     dx = x[i]-x[i-1];  
 

     if ( fabs(taup[i])>10. && fabs(k/(a*H*taup[i]))<0.1 ){


       if (i!=0){

       p1 = phi[i-1];
       p2 = taup[i-1];// a function of x
       p3 = taup[i];// a function of x
       p4 = theta[0][i-1];
       p5 = theta[1][i-1];
       p6 = delta[i-1];
       p7 = v[i-1];
       p8 = delta_b[i-1];
       p9 = v_b[i-1];


       phi[i] = phi[i-1]+ RK_1( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,1,k,dx);

       theta[0][i] = theta[0][i-1]+RK_1( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,2,k,dx);

       theta[1][i] = theta[1][i-1]+ RK_1( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,3,k,dx);

       delta[i] = delta[i-1]+ RK_1( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,4,k,dx);

       v[i] = v[i-1]+ RK_1( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,5,k,dx);

       delta_b[i] = delta_b[i-1]+ RK_1( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,6,k,dx);

       v_b[i] = v_b[i-1]+ RK_1( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,7,k,dx); 


       }



       theta[2][i] =  (-8.*k/(15.*a*H*taup[i]))*theta[1][i];   
       theta_p[1][i] =  (-k/(4.*a*H*taup[i]))*theta[2][i]; 

       theta_p[0][i] = (5./4.)*theta[2][i];
       theta_p[2][i] = (1./4.)*theta[2][i];
       PI[i] = (5./2.)*theta[2][i];

       for (j=3;j<7;j++){
       theta[j][i] = (-j*k/ ((2.*j+1.)*(a*H*taup[i])) )*theta[j-1][i];   
       theta_p[j][i] = (-j*k/ ((2.*j+1.)*(a*H*taup[i])) )*theta_p[j-1][i]; 
       }



     }
     else{


       p1 = phi[i-1];
       p2 = taup[i-1];//// a function of x
       p3 = taup[i];//// a function of x
       p4 = theta[0][i-1];
       p5 = theta[1][i-1];
       p6 = theta[2][i-1];
       p7 = theta[3][i-1];
       p8 = theta[4][i-1];   
       p9 = theta[5][i-1];
       p10 = theta[6][i-1];   
       p11 = theta_p[0][i-1];
       p12 = theta_p[1][i-1];
       p13 = theta_p[2][i-1];
       p14 = theta_p[3][i-1];
       p15 = theta_p[4][i-1];
       p16 = theta_p[5][i-1];
       p17 = theta_p[6][i-1];       
       p18 = delta[i-1];
       p19 = v[i-1];
       p20 = delta_b[i-1];
       p21 = v_b[i-1];
       p22 = eta[i-1];//// a function of x   
       p23 = eta[i];//// a function of x




       phi[i] = phi[i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,1,k,dx);

       theta[0][i] = theta[0][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,2,k,dx);
       theta[1][i] = theta[1][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,3,k,dx);
       theta[2][i] = theta[2][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,4,k,dx);
       theta[3][i] = theta[3][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,5,k,dx);
       theta[4][i] = theta[4][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,6,k,dx);
       theta[5][i] = theta[5][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,7,k,dx);
       theta[6][i] = theta[6][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,8,k,dx);

       theta_p[0][i] = theta_p[0][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,9,k,dx);
       theta_p[1][i] = theta_p[1][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,10,k,dx);
       theta_p[2][i] = theta_p[2][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,11,k,dx);
       theta_p[3][i] = theta_p[3][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,12,k,dx);
       theta_p[4][i] = theta_p[4][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,13,k,dx);
       theta_p[5][i] = theta_p[5][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,14,k,dx);
       theta_p[6][i] = theta_p[6][i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,15,k,dx);

       delta[i] = delta[i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,16,k,dx);

       v[i] = v[i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,17,k,dx);

       delta_b[i] = delta_b[i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,18,k,dx);

       v_b[i] = v_b[i-1]+ RK_2( x[i-1],p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13,p14,p15,p16,p17,p18,p19,p20,p21,p22,p23,19,k,dx);





     PI[i] = theta[2][i]+theta_p[0][i]+theta_p[2][i];

     } 
     
   psi[i] = -phi[i]-(12.*H_0*H_0/(a*a*k*k)) * (Omega_r*theta[2][i]);

   }




/*
   for (i=0;i<1501;i++){


   outFile2<<setprecision(5);
   for(j=0;j<7;j++){
   outFile2<<setw(15)<<theta[j][i];
   }
   outFile2<<endl;

   outFile3<<setprecision(5);
   for(j=0;j<7;j++){
   outFile3<<setw(15)<<theta_p[j][i];
   }
   outFile3<<endl;

   outFile4<<setprecision(5);
   outFile4<<setw(15)<<phi[i];
   outFile4<<setw(15)<<psi[i];
   outFile4<<setw(15)<<delta[i];
   outFile4<<setw(15)<<v[i];
   outFile4<<setw(15)<<delta_b[i];
   outFile4<<setw(15)<<v_b[i]<<endl;


   }
*/


//---------getting source fx----------
// source function = Bessel*(part1+part2+part3+part4)

   vector<double> part1(1501);


   for (i=0;i<1501;i++){


     part1[i]=g[i]*(theta[0][i]+psi[i]+0.25*PI[i]);

   }

   vector<double> part2(1501);
   vector<double> psip(1501);
   vector<double> phip(1501);

   for (i=0;i<1501;i++){


   psip[i] = (psi[i+1]-psi[i-1])/(x[i+1]-x[i-1]); 

   if (i==0 || i==1500) psip[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   psip[500]=  psip[499] + (psip[501]-psip[499])*  (x[500]-x[499]) / (x[501]-x[499]);



   for (i=0;i<1501;i++){


     a = exp(x[i]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

     phip[i]= -phi[i]-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta[2][i])-k*k/(3.*a*a*H*H)*phi[i]+H_0*H_0/(2.*a*a*H*H)*(Omega_m/a*delta[i]+Omega_b/a*delta_b[i]+4.*Omega_r/(a*a)*theta[0][i]) ;





   part2[i] = exp(-tau[i])*( psip[i]-phip[i]);

   }


   vector<double> part3(1501);
   vector<double> part3p(1501);

   for (i=0;i<1501;i++){


     a = exp(x[i]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );
     part3p[i]=(a*H*g[i]*v_b[i]);

   }


   for (i=0;i<1501;i++){


   part3[i] = -1./k*(part3p[i+1]-part3p[i-1])/(x[i+1]-x[i-1]); 

   if (i==0 || i==1500) part3[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   part3[500]=  part3[499] + (part3[501]-part3[499])*  (x[500]-x[499]) / (x[501]-x[499]);




   vector<double> part4(1501);
   vector<double> part4p1(1501);
   vector<double> part4p2(1501);

   
   vector<double> Hp(1501);
   vector<double> HH(1501);
   vector<double> PIp(1501);
   vector<double> PIpp(1501);

   for (i=0;i<1501;i++){


     a = exp(x[i+1]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

     Hp[i] = a*H;

     a = exp(x[i-1]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

     Hp[i] = (Hp[i]-a*H)/(x[i+1]-x[i-1]);

 
     if (i==0 || i==1500) Hp[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   Hp[500]=  Hp[499] + (Hp[501]-Hp[499])*  (x[500]-x[499]) / (x[501]-x[499]);


   for (i=0;i<1501;i++){


     a = exp(x[i+1]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

     HH[i] = a*H*Hp[i+1];

     a = exp(x[i-1]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

     HH[i] = (HH[i]-a*H*Hp[i-1])/(x[i+1]-x[i-1]);

     if (i==0 || i==1500) HH[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   HH[500]=  HH[499] + (HH[501]-HH[499])*  (x[500]-x[499]) / (x[501]-x[499]);


   for (i=0;i<1501;i++){


     PIp[i] = (PI[i+1]-PI[i-1])/(x[i+1]-x[i-1]); 

     if (i==0 || i==1500) PIp[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   PIp[500]=  PIp[499] + (PIp[501]-PIp[499])*  (x[500]-x[499]) / (x[501]-x[499]);

   for (i=0;i<1501;i++){


     PIpp[i] = (PI[i+1]-PI[i-1])/(x[i+1]-x[i-1]); 

     if (i==0 || i==1500) PIp[i]=0;

   }

   // recalculate the slope at x=-8 using interpolation
   PIpp[500]=  PIpp[499] + (PIpp[501]-PIpp[499])*  (x[500]-x[499]) / (x[501]-x[499]);


   for (i=0;i<1501;i++){


     a = exp(x[i]);
     H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );

     part4[i] = 3./(4.*k*k) *( HH[i]*g[i]*PI[i]+3.*a*H*Hp[i]*(gp[i]*PI[i]+g[i]*PIp[i])+a*H*a*H*(gpp[i]*PI[i]+2.*gp[i]*PIp[i]+g[i]*PIpp[i]) ) ; 

   }




   vector<double> source(1501);

   for (i=0;i<1501;i++)   source[i]=part1[i]+part2[i]+part3[i]+part4[i];

//---------output source fx--------------
//turn off loops of l and k and input one pair of (l,k) if one wants to see the curve of source fx


   DP xsj,xsy,xsjp,xsyp;

   for (i=0;i<1501;i++) {



   NR::sphbes(l,k*(eta_0-eta[i]),xsj,xsy,xsjp,xsyp);
  


   integral[ll][kk][i] = xsj*source[i];


   //outFile5<<setw(5)<<x[i]<<setw(20)<<1000.*xsj*source[i]<<endl;
      }



//---------integrate source function over x --------------

     for(i=500;i<1200;i++){

     dx = x[i+1]-x[i];  

     theta_lk[ll][kk] +=  dx*(integral[ll][kk][i]+integral[ll][kk][i+1])/2.;


     }


     }//end loop for different k



 //---------integrate theta over k--------------   

     for(i=0;i<500;i++){

      k1 = k_min+(k_max-k_min)*i/500.;
      k2 = k_min+(k_max-k_min)*(i+1.)/500.;

     C_l[ll] +=  (k2-k1)*( pow(theta_lk[ll][i],2.)/k1 + pow(theta_lk[ll][i+1],2.)/k2 )/2.;


     }


     }//end loop for different l


   
     double nor = C_l[0];


     for(i=0;i<120;i++){

     C_l[i]= C_l[i]/nor;
     l = 10+10*i; 

     outFile5<<setw(5)<<l<<setw(20)<<l*(l+1.)*C_l[i]/(2.*pi)<<"  "<<C_l[i]<<endl;

     }





   return 0;



}


//-------the functions I used in tight-coupling regime---------
double RK_1(double x,double phi,double taup,double taupnext,double theta0,double theta1,double delta,double v,double delta_b,double v_b,int n,double k,double dx){

   
   double k11 = dx*fphi(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);
   double k21 = dx*ftheta0(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);
   double k31 = dx*ftheta1(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);
   double k41 = dx*fdelta(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);
   double k51 = dx*fv(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);
   double k61 = dx*fdelta_b(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);
   double k71 = dx*fv_b(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);


   double k12 = dx*fphi(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,delta+.5*k41,v+.5*k51,delta_b+.5*k61,v_b+.5*k71,k);
   double k22 = dx*ftheta0(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,delta+.5*k41,v+.5*k51,delta_b+.5*k61,v_b+.5*k71,k);
   double k32 = dx*ftheta1(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,delta+.5*k41,v+.5*k51,delta_b+.5*k61,v_b+.5*k71,k);
   double k42 = dx*fdelta(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,delta+.5*k41,v+.5*k51,delta_b+.5*k61,v_b+.5*k71,k);
   double k52 = dx*fv(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,delta+.5*k41,v+.5*k51,delta_b+.5*k61,v_b+.5*k71,k);
   double k62 = dx*fdelta_b(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,delta+.5*k41,v+.5*k51,delta_b+.5*k61,v_b+.5*k71,k);
   double k72 = dx*fv_b(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,delta+.5*k41,v+.5*k51,delta_b+.5*k61,v_b+.5*k71,k);


   double k13 = dx*fphi(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,delta+.5*k42,v+.5*k52,delta_b+.5*k62,v_b+.5*k72,k);
   double k23 = dx*ftheta0(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,delta+.5*k42,v+.5*k52,delta_b+.5*k62,v_b+.5*k72,k);
   double k33 = dx*ftheta1(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,delta+.5*k42,v+.5*k52,delta_b+.5*k62,v_b+.5*k72,k);
   double k43 = dx*fdelta(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,delta+.5*k42,v+.5*k52,delta_b+.5*k62,v_b+.5*k72,k);
   double k53 = dx*fv(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,delta+.5*k42,v+.5*k52,delta_b+.5*k62,v_b+.5*k72,k);
   double k63 = dx*fdelta_b(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,delta+.5*k42,v+.5*k52,delta_b+.5*k62,v_b+.5*k72,k);
   double k73 = dx*fv_b(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,delta+.5*k42,v+.5*k52,delta_b+.5*k62,v_b+.5*k72,k);


   double k14 = dx*fphi(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,delta+k43,v+k53,delta_b+k63,v_b+k73,k);
   double k24 = dx*ftheta0(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,delta+k43,v+k53,delta_b+k63,v_b+k73,k);
   double k34 = dx*ftheta1(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,delta+k43,v+k53,delta_b+k63,v_b+k73,k);
   double k44 = dx*fdelta(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,delta+k43,v+k53,delta_b+k63,v_b+k73,k);
   double k54 = dx*fv(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,delta+k43,v+k53,delta_b+k63,v_b+k73,k);
   double k64 = dx*fdelta_b(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,delta+k43,v+k53,delta_b+k63,v_b+k73,k);
   double k74 = dx*fv(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,delta+k43,v+k53,delta_b+k63,v_b+k73,k);



   if (n==1)   return k11/6.+k12/3.+k13/3.+k14/6.;
   if (n==2)   return k21/6.+k22/3.+k23/3.+k24/6.;
   if (n==3)   return k31/6.+k32/3.+k33/3.+k34/6.;
   if (n==4)   return k41/6.+k42/3.+k43/3.+k44/6.;
   if (n==5)   return k51/6.+k52/3.+k53/3.+k54/6.;
   if (n==6)   return k61/6.+k62/3.+k63/3.+k64/6.;
   if (n==7)   return k71/6.+k72/3.+k73/3.+k74/6.;

}


double fphi(double x, double phi, double taup,  double theta0, double theta1, double delta, double v, double delta_b, double v_b,double k){

   
   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );



   double theta2 = -8.*k/(15.*a*H*taup)*theta1;


   return -phi-12.*H_0*H_0/(a*a*k*k) *(Omega_r*theta2)-k*k/(3.*a*a*H*H)*phi+ H_0*H_0/(2.*a*a*H*H)*(Omega_m*delta/a+Omega_b*delta_b/a+4.*Omega_r*theta0/(a*a));


}


double ftheta0(double x, double phi, double taup,  double theta0, double theta1, double delta, double v, double delta_b, double v_b,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   return -k/(a*H)*theta1-fphi(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);
  

}



double ftheta1(double x, double phi, double taup,  double theta0, double theta1, double delta, double v, double delta_b, double v_b,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double theta2 = -8.*k/(15.*a*H*taup)*theta1;


   return k/(3.*a*H)*theta0-2.*k/(3.*a*H)*theta2+ k/(3.*a*H)*(-phi-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta2))+taup*(a*a/3.);


}




double fdelta(double x, double phi, double taup,  double theta0, double theta1, double delta, double v, double delta_b, double v_b,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   return k/(a*H)*v-3.*fphi(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);

}



double fv(double x, double phi, double taup,  double theta0, double theta1, double delta, double v, double delta_b, double v_b,double k){ 

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double theta2 = -8.*k/(15.*a*H*taup)*theta1;

   return -v-k/(a*H)*(-phi-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta2));

}






double fdelta_b(double x, double phi, double taup,  double theta0, double theta1, double delta, double v, double delta_b, double v_b,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );



   return k/(a*H)*v_b-3.*fphi(x,phi,taup,theta0,theta1,delta,v,delta_b,v_b,k);

}



double fv_b(double x, double phi, double taup,  double theta0, double theta1, double delta, double v, double delta_b, double v_b,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double theta2 = -8.*k/(15.*a*H*taup)*theta1;

   return -v_b-k/(a*H)*(-phi-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta2))+taup*(4.*Omega_r/(3.*Omega_b*a))*a*a;

}


//-------the functions I used after tight-coupling regime---------

double RK_2(double x,double phi,double taup,double taupnext,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double etanext,int n,double k,double dx){



double k11 = dx*fphi_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k21 = dx*ftheta0_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k31 = dx*ftheta1_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k41 = dx*ftheta2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k51 = dx*ftheta3(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k61 = dx*ftheta4(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k71 = dx*ftheta5(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k81 = dx*ftheta6(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k91 = dx*ftheta_p0(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double k01 = dx*ftheta_p1(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l11 = dx*ftheta_p2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l21 = dx*ftheta_p3(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l31 = dx*ftheta_p4(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l41 = dx*ftheta_p5(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l51 = dx*ftheta_p6(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l61 = dx*fdelta_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l71 = dx*fv_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l81 = dx*fdelta_b_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

double l91 = dx*fv_b_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

//

double k12 = dx*fphi_2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k22 = dx*ftheta0_2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k32 = dx*ftheta1_2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k42 = dx*ftheta2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k52 = dx*ftheta3(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k62 = dx*ftheta4(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k72 = dx*ftheta5(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k82 = dx*ftheta6(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k92 = dx*ftheta_p0(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double k02 = dx*ftheta_p1(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l12 = dx*ftheta_p2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l22 = dx*ftheta_p3(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l32 = dx*ftheta_p4(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l42 = dx*ftheta_p5(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l52 = dx*ftheta_p6(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l62 = dx*fdelta_2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l72 = dx*fv_2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l82 = dx*fdelta_b_2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

double l92 = dx*fv_b_2(x+.5*dx,phi+.5*k11,.5*(taup+taupnext),theta0+.5*k21,theta1+.5*k31,theta2+.5*k41,theta3+.5*k51,theta4+.5*k61,theta5+.5*k71,theta6+.5*k81, theta_p0+.5*k91,theta_p1+.5*k01,theta_p2+.5*l11,theta_p3+.5*l21,theta_p4+.5*l31,theta_p5+.5*l41,theta_p6+.5*l51,delta+.5*l61,v+.5*l71,delta_b+.5*l81,v_b+.5*l91,.5*(eta+etanext),k);

//

double k13 = dx*fphi_2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k23 = dx*ftheta0_2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k33 = dx*ftheta1_2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k43 = dx*ftheta2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k53 = dx*ftheta3(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k63 = dx*ftheta4(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k73 = dx*ftheta5(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k83 = dx*ftheta6(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k93 = dx*ftheta_p0(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double k03 = dx*ftheta_p1(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l13 = dx*ftheta_p2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l23 = dx*ftheta_p3(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l33 = dx*ftheta_p4(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l43 = dx*ftheta_p5(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l53 = dx*ftheta_p6(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l63 = dx*fdelta_2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l73 = dx*fv_2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l83 = dx*fdelta_b_2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

double l93 = dx*fv_b_2(x+.5*dx,phi+.5*k12,.5*(taup+taupnext),theta0+.5*k22,theta1+.5*k32,theta2+.5*k42,theta3+.5*k52,theta4+.5*k62,theta5+.5*k72,theta6+.5*k82, theta_p0+.5*k92,theta_p1+.5*k02,theta_p2+.5*l12,theta_p3+.5*l22,theta_p4+.5*l32,theta_p5+.5*l42,theta_p6+.5*l52,delta+.5*l62,v+.5*l72,delta_b+.5*l82,v_b+.5*l92,.5*(eta+etanext),k);

///
double k14 = dx*fphi_2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k24 = dx*ftheta0_2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k34 = dx*ftheta1_2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k44 = dx*ftheta2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k54 = dx*ftheta3(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k64 = dx*ftheta4(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k74 = dx*ftheta5(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k84 = dx*ftheta6(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k94 = dx*ftheta_p0(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double k04 = dx*ftheta_p1(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l14 = dx*ftheta_p2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l24 = dx*ftheta_p3(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l34 = dx*ftheta_p4(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l44 = dx*ftheta_p5(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l54 = dx*ftheta_p6(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l64 = dx*fdelta_2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l74 = dx*fv_2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l84 = dx*fdelta_b_2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);

double l94 = dx*fv_b_2(x+dx,phi+k13,taupnext,theta0+k23,theta1+k33,theta2+k43,theta3+k53,theta4+k63,theta5+k73,theta6+k83, theta_p0+k93,theta_p1+k03,theta_p2+l13,theta_p3+l23,theta_p4+l33,theta_p5+l43,theta_p6+l53,delta+l63,v+l73,delta_b+l83,v_b+l93,etanext,k);




   if (n==1)   return k11/6.+k12/3.+k13/3.+k14/6.;
   if (n==2)   return k21/6.+k22/3.+k23/3.+k24/6.;
   if (n==3)   return k31/6.+k32/3.+k33/3.+k34/6.;
   if (n==4)   return k41/6.+k42/3.+k43/3.+k44/6.;
   if (n==5)   return k51/6.+k52/3.+k53/3.+k54/6.;
   if (n==6)   return k61/6.+k62/3.+k63/3.+k64/6.;
   if (n==7)   return k71/6.+k72/3.+k73/3.+k74/6.;
   if (n==8)   return k81/6.+k82/3.+k83/3.+k84/6.;
   if (n==9)   return k91/6.+k92/3.+k93/3.+k94/6.;
   if (n==10)   return k01/6.+k02/3.+k03/3.+k04/6.;
   if (n==11)   return l11/6.+l12/3.+l13/3.+l14/6.;
   if (n==12)   return l21/6.+l22/3.+l23/3.+l24/6.;
   if (n==13)   return l31/6.+l32/3.+l33/3.+l34/6.;
   if (n==14)   return l41/6.+l42/3.+l43/3.+l44/6.;
   if (n==15)   return l51/6.+l52/3.+l53/3.+l54/6.;
   if (n==16)   return l61/6.+l62/3.+l63/3.+l64/6.;
   if (n==17)   return l71/6.+l72/3.+l73/3.+l74/6.;
   if (n==18)   return l81/6.+l82/3.+l83/3.+l84/6.;
   if (n==19)   return l91/6.+l92/3.+l93/3.+l94/6.;


}


double fphi_2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   
   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );



   return -phi-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta2)-k*k/(3.*a*a*H*H)*phi+H_0*H_0/(2.*a*a*H*H)*(Omega_m/a*delta+Omega_b/a*delta_b+4.*Omega_r/(a*a)*theta0);


}




double ftheta0_2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );




   return -k/(a*H)*theta1-fphi_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);
  

}


double ftheta1_2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


 


   return k/(3.*a*H)*theta0-2.*k/(3.*a*H)*theta2+k/(3.*a*H)*(-phi-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta2))+taup*(theta1+v_b/3.);


}




double fdelta_2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


 
   return k/(a*H)*v-3.*fphi_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

}





double fv_2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );



   return -v-k/(a*H)*(-phi-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta2));

}



double fdelta_b_2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );



   return k/(a*H)*v_b-3.*fphi_2(x,phi,taup,theta0,theta1,theta2,theta3,theta4,theta5,theta6, theta_p0,theta_p1,theta_p2,theta_p3,theta_p4,theta_p5,theta_p6,delta,v,delta_b,v_b,eta,k);

}


double fv_b_2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );



   return -v_b-k/(a*H)*(-phi-12.*H_0*H_0/(a*a*k*k)*(Omega_r*theta2))+taup*(4.*Omega_r/(3.*Omega_b*a))*(3.*theta1+v_b);

}


//new
double ftheta2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 2.;
   double PI = theta2+theta_p0+theta_p2;

   return l*k/(2.*l+1.)/(a*H)*theta1-(l+1.)*k/(2.*l+1.)/(a*H)*theta3+taup*(theta2-0.1*PI);

}



double ftheta3(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 3.;


   return l*k/(2.*l+1.)/(a*H)*theta2-(l+1.)*k/(2.*l+1.)/(a*H)*theta4+taup*theta3;

}



double ftheta4(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 4.;


   return l*k/(2.*l+1.)/(a*H)*theta3-(l+1.)*k/(2.*l+1.)/(a*H)*theta5+taup*theta4;

}





double ftheta5(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 5.;


   return l*k/(2.*l+1.)/(a*H)*theta4-(l+1.)*k/(2.*l+1.)/(a*H)*theta6+taup*theta5;

}



double ftheta6(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 6.;


   return k/(a*H)*theta5-(l+1.)/(a*H)/eta*theta6+taup*theta6;

}




double ftheta_p0(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){
   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double PI = theta2+theta_p0+theta_p2;


   return -k/(a*H)*theta_p1 + taup*(theta_p0 -0.5*PI );

}




double ftheta_p1(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 1.;

   return l*k/(2.*l+1.)/(a*H)*theta_p0-(l+1.)*k/(2.*l+1.)/(a*H)*theta_p2+taup*theta_p1;
}


double ftheta_p2(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 2.;
   double PI = theta2+theta_p0+theta_p2;

   return l*k/(2.*l+1.)/(a*H)*theta_p1-(l+1.)*k/(2.*l+1.)/(a*H)*theta_p3+taup*(theta_p2-0.1*PI);



}


double ftheta_p3(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 3.;


   return l*k/(2.*l+1.)/(a*H)*theta_p2-(l+1.)*k/(2.*l+1.)/(a*H)*theta_p4+taup*theta_p3;
}




double ftheta_p4(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 4.;


   return l*k/(2.*l+1.)/(a*H)*theta_p3-(l+1.)*k/(2.*l+1.)/(a*H)*theta_p5+taup*theta_p4;
}




double ftheta_p5(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 5.;


   return l*k/(2.*l+1.)/(a*H)*theta_p4-(l+1.)*k/(2.*l+1.)/(a*H)*theta_p6+taup*theta_p5;
}




double ftheta_p6(double x,double phi,double taup,double theta0,double theta1,double theta2,double theta3,double theta4,double theta5,double theta6,double theta_p0,double theta_p1,double theta_p2,double theta_p3,double theta_p4,double theta_p5,double theta_p6,double delta,double v,double delta_b,double v_b,double eta,double k){

   double a = exp(x);
   double h = 0.7;
   double Omega_m = 0.224;
   double Omega_b = 0.046;
   double Omega_r = 5.042e-5;
   double Omega_l = 0.72995;
   double H_0 = 2.133*h*(1e-33);
   double H = H_0*sqrt( (Omega_m+Omega_b)*pow(a,-3) + Omega_r*pow(a,-4) + Omega_l );


   double l = 6.;


   return k/(a*H)*theta_p5-(l+1.)/(a*H)/eta*theta_p6+taup*theta_p6;

}

