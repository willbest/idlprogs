PRO HW6_622, PS=ps

;  Calculations for Problem Set #6 for Astro 622 (ISM), plotting the observed
;  brightness temperature along a line of sight as a function of the fraction of CNM.
;
;  HISTORY
;  Written by Will Best (IfA), 03/05/2012
;
;  KEYWORDS
;      PS - output to postscript

T1 = 80.                        ; K
;T2 = 8000.                      ; K
T2 = [10000., 8000., 6000.]     ; K

f = findgen(1001) / 1000.

;T = (f/T1 + (1-f)/T2)^(-1)
T = fltarr(1001,3)
for i=0, 2 do T[*,i] = (f/T1 + (1-f)/T2[i])^(-1)

if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/622/tbright'
    ps_open, fpath, /en, /color, thick=4
endif else window, retain=2, xsize=800, ysize=600

lincolr, /silent
device, decomposed=0
lset = [2, 0, 1]
;plot, f, T, ytitle='Brightness Temperature (K)', color=0, $
;      background=1, xtitle='N!ICNM!N / (N!ICNM!N + N!IWNM!N)', charsize=1.5;, /ylog
plot, f, T[*,0], ytitle='Brightness Temperature (K)', color=0, /nodata, $
      background=1, xtitle='N!ICNM!N / (N!ICNM!N + N!IWNM!N)', charsize=1.5;, /ylog
for i=0, 2 do oplot, f, T[*,i], color=0, lines=lset[i]
gl = ['T!IWNM!N = 10000 K', 'T!IWNM!N = 8000 K', 'T!IWNM!N = 6000 K']
legend, gl, chars=1.5, colors=0, textcolors=0, lines=lset, outline=0, /right

if keyword_set(ps) then ps_close

END

