PRO HW11b_622, PS=ps, STEP=step, XMAX=xmax

;  Calculations for Homework #11, concerning the density profile of a spherical
;  isothermal core in hydrostatic equilibrium.
;  
;  HISTORY
;  Written by Will Best (IfA), 05/03/2012
;
;  KEYWORDS
;      PS - write postscript file of the graph
;      STEP - step size for numerical integration.  Default is .01.
;      XMAX - outer bound for integration.  Default is 1000.
;

;c = 3e10                        ; cm s-1
G = 6.672e-8                    ; cm2
;h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1
mH = 1.67e-24                   ; g
mu = 2.                         ; mean molecular weight
Msun = 1.989e33                 ; g
n0 = [1e3, 1e4, 1e5, 1e6]       ; cm-3
nn0 = n_elements(n0)
T = 10.                         ; K

mm = mu*mH                      ; mean particle mass
a2 = k*T/mm                     ; sound speed squared
rho0 = mm*n0
r0 = sqrt(a2/(4*!pi*G*rho0))    ; radius scaling parameter
m0 = sqrt(a2^3/(Msun*4*!pi*G^3*rho0*Msun))    ; mass scaling parameter in solar masses

; Set up arrays and step size for integration grid.
if not keyword_set(step) then step = .01
if not keyword_set(xmax) then xmax = 1000.
el = xmax/step + 1
x = findgen(el)*step
y = fltarr(el)
z = fltarr(el)
emz = fltarr(el)
dzdx = fltarr(el)
r = fltarr(el, nn0)

; Initial conditions
y[0] = 0.
z[0] = 0.
dzdx[0] = 0.

; PART B: Integrate assuming 0 density at infinite distance
for j=0L, el-2 do begin
    emz[j] = exp(-z[j])
    dydx = x[j]^2 * emz[j]
    y[j+1] = y[j] + dydx*step

    if (j eq 0) then d2zdx2 = emz[j] else d2zdx2 = emz[j] - 2/x[j]*dzdx[j]
    dzdx[j+1] = dzdx[j] + d2zdx2*step

    z[j+1] = z[j] + dzdx[j]*step
endfor

emz[el-1] = exp(-z[j])
r = r0 ## x                     ; radius, in cm
r = r / 3.086e18                ; convert radius to parsecs
n = n0 ## emz

; PART C: mass
m = m0 ## y                     ; mass, in solar masses
ext = intarr(nn0)
rext = fltarr(nn0)
for i=0, nn0-1 do begin
    ext[i] = closest(n[*,i], 600.)
    rext[i] = r[ext[i],i]
endfor


; Load a color table.
device, decomposed=0
cubehelix
black = 0
white = 255
green = 100
red = 150
blue = 200

; Postscript or window output
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/622/hw11_1'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 20, retain=2, xsize=800, ysize=600
endelse

; Plot n(r) vs. r in x parameter
lset = [0,1,2,3]
j = 0
plot, x, n[*,j], title='Molecular Cloud Density Profile, T = 10 K', $
      background=white, color=black, charsize=1.5, linestyle=lset[j], $
      xtitle='x = r/r!D0!N', xrange=[0,100], $
      ytitle='Number Density (cm!E-3!N)', /ylog, yrange=[.1,1000000.], ystyle=1
for i=0, nn0-1 do oplot, x, n[*,i], linestyle=lset[i], color=black
nleg = ['n!D0!N = 10!E3!N cm!E-3!N', 'n!D0!N = 10!E4!N cm!E-3!N', $
      'n!D0!N = 10!E5!N cm!E-3!N', 'n!D0!N = 10!E6!N cm!E-3!N']
nlegr = reverse(nleg)
lsetr = reverse(lset)
legend, nlegr, chars=1.5, lines=lsetr, outline=black, color=black, textcolor=black, /right

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Desktop/Astro/classes/622/hw11_2'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 21, retain=2, xsize=800, ysize=600
endelse

; Plot n(r) vs. r in parsecs
plot, r[*,j], n[*,j], title='Molecular Cloud Density Profile, T = 10 K', $
      background=white, color=black, charsize=1.5, linestyle=lset[j], $
      xtitle='Radius (pc)', xrange=[0,1], $
      ytitle='Number Density (cm!E-3!N)', /ylog, yrange=[.1,1000000.], ystyle=1
for i=0, nn0-1 do oplot, r[*,i], n[*,i], linestyle=lset[i], color=black
legend, nlegr, chars=1.5, lines=lsetr, outline=black, color=black, textcolor=black, /right

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Desktop/Astro/classes/622/hw11_3'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 23, retain=2, xsize=800, ysize=600
endelse

; Plot mass(r) vs. r in parsecs
plot, r[*,j], m[*,j], title='Molecular Cloud Mass Profile, T = 10 K', $
      background=white, color=black, charsize=1.5, linestyle=lset[j], $
      xtitle='Radius (pc)', xrange=[0,0.3], $
      ytitle='Mass (M!Dsun!N)', yrange=[0,7.], ystyle=1
for i=0, nn0-1 do begin
    oplot, r[*,i], m[*,i], linestyle=lset[i], color=black
    vline, rext[i], linestyle=lset[i], color=black
endfor
legend, nlegr, chars=1.5, lines=lsetr, outline=black, color=black, textcolor=black

if keyword_set(ps) then ps_close

END
