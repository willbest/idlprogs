; Calculations for Problem Set #4 for Astro 622 (ISM), determining the
; properties of a Stromgren Sphere.
;
;  HISTORY
;  Written by Will Best (IfA), 2/13/2012

c = 3e10                        ; cm s-1
h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1

dist = 1000.                    ; pc
flux = 10                       ; Jy
flux = flux * 1e-23             ; erg s-1 cm-2 Hz-1
T = 1e4                         ; K

nu = 10e9                       ; Hz
nuG = nu/1e9                    ; GHz
B = (2*h*nu^2*nu)/c^2 / (exp(h*nu/(k*T))-1) ; erg s-1 cm-2 Hz-1 
alph2 = 2.6e-13 * (1e4/T)^.85   ; cm3 s-1
d = dist * 2.06e5 * 1.5e13      ; cm

; PART C: Number of ionizing photons
NLyc = 4*d*alph2*d*nuG^2.1*T^1.35*flux / (3*8.235e-2*B)
print, 'Number of ionizing photons', NLyc*3.09e18

; PART D: Radius of Stromgren Sphere
EM = nuG^2.1 * T^1.35 / 8.235e-2
Rs = sqrt(3*NLyc/(4*!pi*alph2*EM)) ; cm
Rs = Rs / (2.06e5 * 1.5e13)
print, 'Radius of Stromgren Sphere (pc)', Rs
om = !pi*Rs^2 / dist^2
om = om * (180/!pi)^2 * 3600 * 3600
print, 'Angular size of Stromgren Sphere (sq arcsec)', om

END

