PRO DISKFLUX, PS=ps

;  Calculations for Problem Set #2 for Astro 622 (ISM), determining the flux and
;  SED for a protostellar disk.
;
;  HISTORY
;  Written by Will Best (IfA), 1/28/2012
;
;  KEYWORDS
;      PS - output to postscript

c = 3e10                        ; cm s-1
h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1

d = 400.                        ; pc
;lam = 0.1                       ; cm
lam = findgen(998)*.001 + .003  ; cm
m = 0.01                        ; mSun
T = 20.                         ; K

nu = c / lam                    ; Hz
B = (2*h*nu^2*nu)/c^2 / (exp(h*nu/(k*T))-1)          ; erg s-1 cm-2 Hz-1 
;B = (2*nu^2)/c^2 * k*T          ; erg s-1 cm-2 Hz-1 
kap = .1*(nu/1.2e12)            ; cm2 g-1
mass = m*2e33                   ; g
dist = d * 2.06e5 * 1.5e13      ; cm

flux = (B*kap*mass) / dist / dist    ; erg s-1 cm-2 Hz-1
flux = flux / 1e-23             ; Jy

; PART A: Flux at 1 mm
mm = where(lam eq .1)           ; 1 mm
print, 'Flux in Jy', flux[mm]

; PART B: Plot of the SED
lincolr, /silent
device, decomposed=0
window, 21, retain=2, xsize=800, ysize=600
plot, lam*10, flux*1000, ytitle='Flux (mJy)', color=0, /xlog, /ylog, $
      background=1, xtitle='Wavelength (mm)', charsize=1.5, $
      title='SED of Protostellar Disk, T = 20 K'
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/622/diskSED'
    ps_open, fpath, /color, thick=4
    plot, lam*10, flux*1000, ytitle='Flux (mJy)', color=0, /xlog, /ylog, $
          background=1, xtitle='Wavelength (mm)', charsize=1.5, $
          title='SED of Protostellar Disk, T = 20 K'
    ps_close
endif

END

