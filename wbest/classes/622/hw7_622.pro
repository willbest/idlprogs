PRO hw7_622, CUBE=cube, PS=ps

;  Calculations for Homework #7 for Astro 622 (ISM), plotting the
;  relative rotational populations of a CO molecule.
;
;  HISTORY
;  Written by Will Best (IfA), 03/13/2012
;
;  KEYWORDS
;      CUBE - Use CubeHelix colors
;      PS - output to postscript


; Constants
h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1

B = 57.6e9                      ; Hz
T = [10., 30., 100.]            ; K
Jlim = 20
Jplot = 15

; Calculate arrays
nrows = n_elements(T)
Z = fltarr(nrows)
J = indgen(Jlim+1)
nJ = fltarr(Jlim+1, nrows)
nrat = fltarr(Jplot+1, nrows)

for i=0, nrows-1 do begin
; Calculate arrays for nJ
    nJ[*,i] = (2*J+1) * exp(-h*B*J*(J+1)/(k*T[i]))
; Partition function
    Z[i] = total(nJ[*,i])
; Calculate the population ratios
    nrat[*,i] = nJ[0:Jplot,i] / Z[i]
endfor

; Load a color table.  Default is lincolr.pro.
device, decomposed=0
if keyword_set(cube) then begin
    cubehelix
    black = 0
    white = 255
    green = 100
    red = 150
    blue = 200
endif else begin
    lincolr, /silent
    black = 0
    white = 1
    green = 2
    red = 3
    blue = 4
endelse

; Postscript or window output
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/622/rotpop'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 20, retain=2, xsize=800, ysize=600
endelse

; Special characters
plotsym, 0, .5, /fill
;betachar='!7b!X'
;deltachar='!7d!X'
;deltacap='!7D!X'
;phichar='!7u!X'
;lamchar='!7k!X'
;tauchar='!7s!X'
;angstrom = '!sA!r!u!9 %!X!n'

; Plot
cset = [blue, red, green, black]
gl = strarr(nrows)
plot, nrat[*,0], title='Rotational Population Levels for the CO Molecule', $
      background=white, color=black, /nodata, $
      ;xrange=[-wide, wide], xstyle=1, $;yrange=[0.3, 1.1], ystyle=1, $
      xtitle='Rotational level J', charsize=1.5, $
      ytitle='Population fraction'
for i=0, nrows-1 do begin
    oplot, nrat[*,i], color=cset[i]
    oplot, nrat[*,i], psym=8, color=black
    gl[i] = 'T = '+trim(fix(T[i]))+' K'
endfor
glr = reverse(gl)
csetr = reverse(cset)
legend, gl, chars=1.5, colors=cset, textcolors=cset, linestyle=0, outline=black, /right

;; if keyword_set(ps) then begin
;;     ps_close
;;     fpath = '~/Desktop/Astro/classes/631/profline2'
;;     ps_open, fpath, /color, /en, thick=4
;; endif else begin
;;     window, 21, retain=2, xsize=800, ysize=600
;; endelse

;; plot, dellam, Irat[*,0], title='H'+deltachar+' Absorption at T = 10,000K', $
;;       background=white, color=black, /nodata, $
;;       yrange=[0.0, 1.1], ystyle=1, $
;;       xtitle=deltacap+lamchar, charsize=1.5, $
;;       ytitle='I('+deltacap+lamchar+') / I!I0!N'
;; for j=0, nrows-1 do oplot, dellam, Irat[*,j], color=cset[j]
;; legend, gl, chars=1.2, colors=cset, textcolors=cset, linestyle=0, outline=black, /bottom

if keyword_set(ps) then ps_close

END

