FUNCTION TAU, U, V, PEAK, SIGMA
return, peak*exp(-(u-v)^2/(2*sigma^2))
END

FUNCTION TBRIGHT, Tf, tauf, Tb, taub
return, Tf*(1-exp(-tauf)) + Tb*(1-exp(-taub))*exp(-tauf)
END

PRO HW8_622, PS=ps

;  Calculations for Homework #8 for Astro 622 (ISM), plotting the
;  profile of a spectral line from a two-layer cloud.
;
;  HISTORY
;  Written by Will Best (IfA), 03/27/2012
;
;  KEYWORDS
;      PS - output to postscript


; Constants
;h = 6.63e-27                    ; erg s
;k = 1.38e-16                    ; erg K-1

sigma = 0.5                      ; km s-1
Tb = 20.                         ; K
Tf = 5.                          ; K

; Plotting array
grid = 1000
wide = 6
u = findgen(grid+1)*2*wide/grid - wide

; PART B
peakb = 3.
peakf = 3.
v = 0.

taub = tau(u, 0., peakb, sigma)
tauf = tau(u, v, peakf, sigma)
T0 = tbright(Tf, tauf, Tb, taub)

; PART C
nrows = 6
boost = 1.
T = fltarr(grid+1, nrows)
Tcl = fltarr(grid+1, nrows)
for i=0, nrows-1 do begin
    v = boost*i
    tauf = tau(u, v, peakf, sigma)
    T[*,i] = tbright(Tf, tauf, Tb, taub)
    Tcl[*,i] = tbright(20., tauf, 5., taub)
endfor

; PART D
peakb = .3
peakf = .3
taub = tau(u, 0., peakb, sigma)
Tthin = fltarr(grid+1, nrows)
for i=0, nrows-1 do begin
    v = boost*i
    tauf = tau(u, v, peakf, sigma)
    Tthin[*,i] = tbright(20., tauf, 5., taub)
endfor

T1 = fltarr(grid+1, 6)
; Tb = 5, Tf = 20, two optically thick layers, v = 1
T1[*,0] = Tcl[*,1]
; Tb = 5, Tf = 20, two optically thin layers, v = 1
T1[*,1] = Tthin[*,1]
; Tb = 20, Tf = 5, two optically thick layers, v = 1
T1[*,2] = T[*,1]
; Tb = 20, Tf = 5, two optically thin layers, v = 1
tauf = tau(u, 1., peakf, sigma)
T1[*,3] = tbright(Tf, tauf, Tb, taub)
; Tb = 5, Tf = 20, twenty optically thin layers, v = 1
vl = fltarr(20)
Tl = fltarr(20)
for j=0, 19 do begin
    vl[j] = j/19.
    tauf = tau(u, vl[j], .03, sigma)
    Tl[j] = 5 + j*15./19
    T1[*,4] = Tl[j]*(1-exp(-tauf)) + T1[*,4]*exp(-tauf)
endfor
; Tb = 20, Tf = 5, twenty optically thin layers, v = 1
for j=0, 19 do begin
    tauf = tau(u, vl[j], .03, sigma)
    T1[*,5] = Tl[19-j]*(1-exp(-tauf)) + T1[*,5]*exp(-tauf)
endfor

T3 = fltarr(grid+1, 6)
; Tb = 5, Tf = 20, two optically thick layers, v = 3
T3[*,0] = Tcl[*,3]
; Tb = 5, Tf = 20, two optically thin layers, v = 3
T3[*,1] = Tthin[*,3]
; Tb = 20, Tf = 5, two optically thick layers, v = 3
T3[*,2] = T[*,3]
; Tb = 20, Tf = 5, two optically thin layers, v = 3
tauf = tau(u, 3., peakf, sigma)
T3[*,3] = tbright(Tf, tauf, Tb, taub)
; Tb = 5, Tf = 20, twenty optically thin layers, v = 3
for j=0, 19 do begin
    vl[j] = 3.*j/19.
    tauf = tau(u, vl[j], .03, sigma)
    T3[*,4] = Tl[j]*(1-exp(-tauf)) + T3[*,4]*exp(-tauf)
endfor
; Tb = 20, Tf = 5, twenty optically thin layers, v = 3
for j=0, 19 do begin
    tauf = tau(u, vl[j], .03, sigma)
    T3[*,5] = Tl[19-j]*(1-exp(-tauf)) + T3[*,5]*exp(-tauf)
endfor


; Load the cubehelix color table.
device, decomposed=0
cubehelix
black = 0
white = 255

; Postscript or window output
if keyword_set(ps) then begin
    fpath = '~/Astro/classes/622/linestat'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 20, retain=2, xsize=800, ysize=600
endelse

; Special characters
plotsym, 0, .5, /fill
;betachar='!7b!X'
;deltachar='!7d!X'
;deltacap='!7D!X'
;phichar='!7u!X'
;lamchar='!7k!X'
sigmachar='!7r!X'
tauchar='!7s!X'
;angstrom = '!sA!r!u!9 %!X!n'
kps = 'km s!E-1!N'

plot, u, T0, title='Emission Line Profile for Two-Layer Static Cloud', $
      subtitle='T!If!N = 5 K   T!Ib!N = 20 K   '+tauchar+'!If!N = '+tauchar+'!Ib!N = 3'+$
      '   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=1.5, $
      xtitle='Velocity relative to cloud average ('+kps+')', $
      ytitle='Brightness Temperature (K)', ymargin=[5,2]
legend, 'v = 0 '+kps, chars=1.2, colors=black, textcolors=black, linestyle=0, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Astro/classes/622/linemove'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 21, retain=2, xsize=800, ysize=600
endelse

plot, u, T[*,5], title='Emission Line Profile for Two-Layer Cloud', $
      subtitle='T!If!N = 5 K   T!Ib!N = 20 K   '+tauchar+'!If!N = '+tauchar+'!Ib!N = 3'+$
      '   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=1.5, /nodata, $
      xtitle='Velocity relative to observer ('+kps+')', $
      ytitle='Brightness Temperature (K)', ymargin=[5,2]
cset = indgen(nrows)*140/(nrows-1)+60
cset[0] = 0
gl = strarr(nrows)
for i=0, nrows-1 do begin
    oplot, u, T[*,i], color=cset[i]
    gl[i] = 'v = '+trim(fix(boost*i))+' '+kps
endfor
legend, gl, chars=1.2, colors=cset, textcolors=cset, linestyle=0, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Astro/classes/622/linecoll'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 22, retain=2, xsize=800, ysize=600
endelse

plot, u, Tcl[*,2], title='Emission Line Profile for Two-Layer Collapsing Cloud', $
      subtitle='T!If!N = 20 K   T!Ib!N = 5 K   '+tauchar+'!If!N = '+tauchar+'!Ib!N = 3'+$
      '   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=1.5, $
      xtitle='Velocity relative to observer ('+kps+')', $
      ytitle='Brightness Temperature (K)', ymargin=[5,2]
legend, gl[2], chars=1.2, colors=black, textcolors=black, linestyle=0, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Astro/classes/622/linethin'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 23, retain=2, xsize=800, ysize=600
endelse

plot, u, Tthin[*,0], title='Emission Line Profile for Two-Layer, Optically Thin Cloud', $
      subtitle='T!If!N = 5 K   T!Ib!N = 20 K   '+tauchar+'!If!N = '+tauchar+'!Ib!N = 0.1'+$
      '   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=1.5, /nodata, $
      xtitle='Velocity relative to observer ('+kps+')', $
      ytitle='Brightness Temperature (K)', ymargin=[5,2]
for i=0, nrows-1 do oplot, u, Tthin[*,i], color=cset[i]
legend, gl, chars=1.2, colors=cset, textcolors=cset, linestyle=0, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Astro/classes/622/linegrid1'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 24, retain=2, xsize=800, ysize=600
endelse

!p.multi = [0, 2, 2, 0, 0]
chars = 1.3
xchars = .9
ychars = .8
plot, u, T1[*,0], title='Two Optically Thick Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[8,-2], $
      ytitle='Brightness Temperature (K)', ytickname=' ', ymargin=[0,4], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 20 K', 'T!Ib!N = 5 K', 'v = 1 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T1[*,4], title='Twenty Optically Thin Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[2,4], $
      ytickname=replicate(' ',5), ymargin=[0,4], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T1[*,2], subtitle=tauchar+'!If!N = '+tauchar+'!Ib!N = 3'+$
      '   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xmargin=[8,-2], xcharsize=xchars, $
      ytitle='Brightness Temperature (K)', ymargin=[5,0], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 5 K', 'T!Ib!N = 20 K', 'v = 1 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T1[*,5], subtitle=tauchar+'!Dlayer!N = 0.03   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xtickname=' ', xmargin=[2,4], xcharsize=xchars, $
      ytickname=replicate(' ',5), ymargin=[5,0], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Astro/classes/622/linegrid3'
    ps_open, fpath, /color, /en, thick=4
    !p.multi = [0, 2, 2, 0, 0]
endif else begin
    window, 25, retain=2, xsize=800, ysize=600
endelse

plot, u, T3[*,0], title='Two Optically Thick Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[8,-2], $
      ytitle='Brightness Temperature (K)', ytickname=' ', ymargin=[0,4], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 20 K', 'T!Ib!N = 5 K', 'v = 3 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T3[*,4], title='Twenty Optically Thin Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[2,4], $
      ytickname=replicate(' ',5), ymargin=[0,4], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T3[*,2], subtitle=tauchar+'!If!N = '+tauchar+'!Ib!N = 3'+$
      '   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xmargin=[8,-2], xcharsize=xchars, $
      ytitle='Brightness Temperature (K)', ymargin=[5,0], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 5 K', 'T!Ib!N = 20 K', 'v = 3 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T3[*,5], subtitle=tauchar+'!Dlayer!N = 0.03   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xtickname=' ', xmargin=[2,4], xcharsize=xchars, $
      ytickname=replicate(' ',5), ymargin=[5,0], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Astro/classes/622/linegrid1t'
    ps_open, fpath, /color, /en, thick=4
    !p.multi = [0, 2, 2, 0, 0]
endif else begin
    window, 26, retain=2, xsize=800, ysize=600
endelse

plot, u, T1[*,1], title='Two Optically Thin Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[8,-2], $
      ytitle='Brightness Temperature (K)', ytickname=' ', ymargin=[0,4], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 20 K', 'T!Ib!N = 5 K', 'v = 1 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T1[*,4], title='Twenty Optically Thin Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[2,4], $
      ytickname=replicate(' ',5), ymargin=[0,4], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T1[*,3], subtitle=tauchar+'!Dlayer!N = 0.3   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xmargin=[8,-2], xcharsize=xchars, $
      ytitle='Brightness Temperature (K)', ymargin=[5,0], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 5 K', 'T!Ib!N = 20 K', 'v = 1 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T1[*,5], subtitle=tauchar+'!Dlayer!N = 0.03   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xtickname=' ', xmargin=[2,4], xcharsize=xchars, $
      ytickname=replicate(' ',5), ymargin=[5,0], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Astro/classes/622/linegrid3t'
    ps_open, fpath, /color, /en, thick=4
    !p.multi = [0, 2, 2, 0, 0]
endif else begin
    window, 27, retain=2, xsize=800, ysize=600
endelse

plot, u, T3[*,1], title='Two Optically Thin Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[8,-2], $
      ytitle='Brightness Temperature (K)', ytickname=' ', ymargin=[0,4], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 20 K', 'T!Ib!N = 5 K', 'v = 3 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T3[*,4], title='Twenty Optically Thin Layers', $
      background=white, color=black, charsize=chars, $
      xtickname=replicate(' ',7), xmargin=[2,4], $
      ytickname=replicate(' ',5), ymargin=[0,4], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T3[*,3], subtitle=tauchar+'!Dlayer!N = 0.3   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xmargin=[8,-2], xcharsize=xchars, $
      ytitle='Brightness Temperature (K)', ymargin=[5,0], yrange=[0,20], ycharsize=ychars
gl = ['T!If!N = 5 K', 'T!Ib!N = 20 K', 'v = 3 '+kps]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

plot, u, T3[*,5], subtitle=tauchar+'!Dlayer!N = 0.03   '+sigmachar+' = 0.5 '+kps, $
      background=white, color=black, charsize=chars, $
      xtitle='Velocity relative to observer ('+kps+')', xtickname=' ', xmargin=[2,4], xcharsize=xchars, $
      ytickname=replicate(' ',5), ymargin=[5,0], yrange=[0,20]
legend, gl, chars=1, textcolors=black, linestyle=-99, outline=black

!p.multi = [0, 0, 1, 0, 0]
if keyword_set(ps) then ps_close

END

