;PRO HW11c_622, PS=ps, STEP=step, XMAX=xmax

;  Calculations for Homework #11, concerning the density profile of a spherical
;  isothermal core in hydrostatic equilibrium.
;  
;  HISTORY
;  Written by Will Best (IfA), 05/03/2012
;
;  KEYWORDS
;      PS - write postscript file of the graph
;      STEP - step size for numerical integration.  Default is .01.
;      XMAX - outer bound for integration.  Default is 100.
;

;c = 3e10                        ; cm s-1
G = 6.672e-8                    ; cm2
;h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1
mH = 1.67e-24                   ; g
mu = 2.                         ; mean molecular weight
Msun = 1.989e33                 ; g
nmin = 3
nmax = 6
npoints = 1000
n0 = 10^(findgen(npoints+1)*(nmax-nmin)/npoints + nmin)
nn0 = n_elements(n0)
T = 10.                         ; K

mm = mu*mH                      ; mean particle mass
a2 = k*T/mm                     ; sound speed squared
rho0 = mm*n0
r0 = sqrt(a2/(4*!pi*G*rho0))    ; radius scaling parameter
m0 = sqrt(a2^3/(Msun*4*!pi*G^3*rho0*Msun))    ; mass scaling parameter in solar masses

; Set up arrays and step size for integration grid.
if not keyword_set(step) then step = .01
if not keyword_set(xmax) then xmax = 100.
el = xmax/step + 1
x = findgen(el)*step
y = fltarr(el)
z = fltarr(el)
emz = fltarr(el)
dzdx = fltarr(el)
r = fltarr(el, nn0)

; Initial conditions
y[0] = 0.
z[0] = 0.
dzdx[0] = 0.

; PART C: Integrate 
for j=0L, el-2 do begin
    emz[j] = exp(-z[j])
    dydx = x[j]^2 * emz[j]
    y[j+1] = y[j] + dydx*step

    if (j eq 0) then d2zdx2 = emz[j] else d2zdx2 = emz[j] - 2/x[j]*dzdx[j]
    dzdx[j+1] = dzdx[j] + d2zdx2*step

    z[j+1] = z[j] + dzdx[j]*step
endfor

emz[el-1] = exp(-z[j])
r = r0 ## x                     ; radius, in cm
r = r / 3.086e18                ; convert radius to parsecs
n = n0 ## emz
m = m0 ## y                     ; mass, in solar masses

ext = intarr(nn0)
mext = fltarr(nn0)
for i=0, nn0-1 do begin
    ext[i] = closest(n[*,i], 600.)
    mext[i] = m[ext[i],i]
endfor

maxm = max(mext, mind)
print, maxm, format='("Maximum possible mass at the lower density limit = ",f4.2," Msun")'
print, alog10(n0[mind]), n0[mind], $
       format='("Occurs for central density = 10^",f4.2," = ",e8.2," cm-3")'
print, n0[mind]/600., format='("Ratio of central density to density limit = ",f5.2)'

; Load a color table.  Default is lincolr.pro.
device, decomposed=0
cubehelix
black = 0
white = 255
green = 100
red = 150
blue = 200

; Postscript or window output
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/622/hw11_4'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 24, retain=2, xsize=800, ysize=600
endelse

; Plot m(rext) vs. n0
plot, n0, mext, title='Mass at Lower Density Limit, T = 10 K', $
      background=white, color=black, charsize=1.5, $
      xtitle='Central density (cm!E-3!N)', /xlog, xrange=[1000, 1000000], xstyle=1, $
      ytitle='Mass (M!Dsun!N)'

if keyword_set(ps) then ps_close

END
