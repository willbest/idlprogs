PRO HW5_622, PS=ps

;  Calculations for Problem Set #5 for Astro 622 (ISM), plotting a forbidden
;  line ratio.
;
;  HISTORY
;  Written by Will Best (IfA), 1/28/2012
;
;  KEYWORDS
;      PS - output to postscript

c = 3e10                        ; cm s-1
h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1

Te = findgen(15001) + 5000      ; K

l32 = 2.40e-7 * exp(-6.20e4/Te)
l21 = (1.16e-6 * exp(-2.87e4/Te) + l32) * 0.75

linerat = l32 / l21

if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/622/lineratio'
    ps_open, fpath, /en, /color, thick=4
endif else window, retain=2, xsize=800, ysize=600

ang = '!sA!r!u!9 %!X!n'
lincolr, /silent
device, decomposed=0
plot, Te, linerat, ytitle='[4636 '+ang+'] / [5007 '+ang+']', color=0, $
      background=1, xtitle='Temperature (K)', charsize=1.5;, /ylog
if keyword_set(ps) then ps_close

END

