PRO PROFLINE, CUBE=cube, PS=ps

;  Calculations for Homework #5 for Astro 631 (Radiative Transfer), plotting
;  the profile and intensity ratio in a spectral line.
;
;  HISTORY
;  Written by Will Best (IfA), 03/03/2012
;
;  KEYWORDS
;      CUBE - Use CubeHelix colors
;      PS - output to postscript
;

; Constants
lam0 = 4.10173e3               ; Ang
T = 1e4                         ; K
dellamD = 4.2846e-7 * lam0 * sqrt(T)     ; Ang

n = [1e10, 1e11, 1e12, 1e13]    ; cm-3
F0 = 1.25e-9 * n^(2./3)          ; 
K = 0.01944                     ; 

; Calculate arrays
nrows = n_elements(n)
wide = 50.
grid = 1000.
dellam = ((findgen(grid+1)/grid * 2*wide) - wide) * dellamD
phi = fltarr(grid+1, nrows, /nozero)
Irat = fltarr(grid+1, nrows, /nozero)

for j=0, nrows-1 do begin
; Calculate arrays from 0 to 490
    phiD = !pi^(-.5) / dellamD * exp(-(dellam[0:grid/2.-10]/dellamD)^2)
    phiS = F0[j]^(1.5) * K * abs(dellam[0:grid/2-10])^(-2.5)
; Find where phiD is first greater than phiS
    tran = min(where(phiD gt phiS))
; Generate the array to be phiS outside of the transition, and phiD inside
    phi[0:tran-1,j] = phiS[0:tran-1]
    phi[tran:grid-tran,j] = !pi^(-.5) / dellamD * exp(-(dellam[tran:grid-tran]/dellamD)^2)
    phi[grid-tran+1:grid,j] = reverse(phiS[0:tran-1])
endfor

; Calculate the intensity ratio
Irat = .2*(4*exp(-1e4*phi) + 1)

;for j=0, 2 do Irat[*,j] = .5 + 1/(2*(1 + beta0[j]*exp(-x^2)))

; Load a color table.  Default is lincolr.pro.
device, decomposed=0
if keyword_set(cube) then begin
    cubehelix
    black = 0
    white = 255
    green = 100
    red = 150
    blue = 200
endif else begin
    lincolr, /silent
    black = 0
    white = 1
    green = 2
    red = 3
    blue = 4
endelse

; Postscript or window output
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/631/profline1'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 20, retain=2, xsize=800, ysize=600
endelse

; Special characters
;plotsym, 0, .5, /fill
betachar='!7b!X'
deltachar='!7d!X'
deltacap='!7D!X'
phichar='!7u!X'
lamchar='!7k!X'
;tauchar='!7s!X'
;angstrom = '!sA!r!u!9 %!X!n'

; Plot
cset = [blue, red, green, black]
;plot, dellam, phi[*,0], title='H'+deltachar+' Line Profile at T = 10,000K', $
plot, dellam, alog10(phi[*,0]), title='H'+deltachar+' Line Profile at T = 10,000K', $
      background=white, color=black, /nodata, $
      ;xrange=[-wide, wide], xstyle=1, $;yrange=[0.3, 1.1], ystyle=1, $
      xtitle=deltacap+lamchar, charsize=1.5, $
      ytitle='log['+phichar+'('+deltacap+lamchar+')]'
;for j=0, nrows-1 do oplot, dellam, phi[*,j], color=cset[j]
for j=0, nrows-1 do oplot, dellam, alog10(phi[*,j]), color=cset[j]
gl = ['n!Ie!N = 10!E10!N cm!E-3!N', 'n!Ie!N = 10!E11!N', 'n!Ie!N = 10!E12!N', 'n!Ie!N = 10!E13!N']
glr = reverse(gl)
csetr = reverse(cset)
legend, glr, chars=1.2, colors=csetr, textcolors=csetr, linestyle=0, outline=black

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Desktop/Astro/classes/631/profline2'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 21, retain=2, xsize=800, ysize=600
endelse

plot, dellam, Irat[*,0], title='H'+deltachar+' Absorption at T = 10,000K', $
      background=white, color=black, /nodata, $
      yrange=[0.0, 1.1], ystyle=1, $
      xtitle=deltacap+lamchar, charsize=1.5, $
      ytitle='I('+deltacap+lamchar+') / I!I0!N'
for j=0, nrows-1 do oplot, dellam, Irat[*,j], color=cset[j]
legend, gl, chars=1.2, colors=cset, textcolors=cset, linestyle=0, outline=black, /bottom

if keyword_set(ps) then ps_close

END

