;PRO HW3_631, PS=ps

;  Calculations for Homework #3, concerning the limb darkening of the Sun.
;  
;  HISTORY
;  Written by Will Best (IfA), 2/15/2012
;
;  KEYWORDS
;      PS - write postscript file of the graph
;

c = 3e10                        ; cm s-1
h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1
mH = 1.67e-24                   ; g

; Read in raw image data
tpath = '~/Astro/classes/Spring\ 2012/631\ Stellar\ Atmospheres/limbd.txt'
readcol, tpath, lam, I01, mu10, mu08, mu06, mu05, mu04, mu03, mu02, mu01, $
         comment='#', /silent
I0mu = transpose([[mu10], [mu08], [mu06], [mu05], [mu04], [mu03], [mu02], [mu01]])
nrows = n_elements(lam)

; Fit polynomials to I-ratio as a function of mu
muarr = [1., .8, .6, .5, .4, .3, .2, .1]
mu = reverse((findgen(91)+10)/100)
degree = 2
fit = fltarr(degree+1, nrows)
Irat = fltarr(n_elements(mu), nrows, /nozero)
for j=0, nrows-1 do begin
    fit[*,j] = poly_fit(muarr, I0mu[*,j], degree)
    Irat[*,j] = poly(mu, fit[*,j])
endfor

; Calculate temperature as a function of optical depth for each wavelength
lamcm = lam * 1e-8    ;convert to cm
tau = findgen(2000)*.001 + .001
Blam = fltarr(n_elements(tau), nrows, /nozero)
T = fltarr(n_elements(tau), nrows, /nozero)
for j=0, nrows-1 do begin
    Blam[*,j] = I01[j]*1e13 * (fit[0,j] + fit[1,j]*tau + fit[2,j]/2*tau^2)
    T[*,j] = h*c / (lamcm[j]*k*alog(2*h*c^2/(Blam[*,j]*lamcm[j]^5) + 1))
endfor 

; Calculate optical depth for T = 6000K as a function of wavelength
tau6000 = fltarr(nrows)
for j=0, nrows-1 do tau6000[j] = tau[closest(T[*,j], 6000)]

; Calculate the wavelength dependence of the absorption coefficient, normed at
; 5000 Angstroms.
ind5000 = where(lam eq 5000)
ind5000 = ind5000[0]       ;switch from 1-element array to scalar
tau65 = tau6000[ind5000]   ;optical depth for T = 6000K at wavelength 5000A
kapparat = tau6000 / tau65    ;normalize all T=6000K optical depths by this wavelength

; Calculate temperature as a function of 5000A optical depth for each wavelength
; i.e. use the coefficients for the 5000A fit for every Planck function calculation
T5 = fltarr(n_elements(tau), nrows, /nozero)
for j=0, nrows-1 do T5[*,j] = h*c / (lamcm[j]*k*alog(2*h*c^2/(Blam[*,ind5000]*lamcm[j]^5) + 1))

; Calculate density as a function of optical depth for lam = 5000A, T = 6000K
g = 10^4.44
kap0 = 1e6
T6 = 6000
H = k*T6 / (g*mH)
rho = sqrt(2*tau/(kap0*H)) / mH


; Get ready to graph
lincolr, /silent
device, decomposed=0
plotsym, 0, .5, /fill
cubehelix                       ;nifty colors
kapchar='!7j!X'
lamchar='!7k!X'
muchar='!7l!X'
tauchar='!7s!X'
angstrom = '!sA!r!u!9 %!X!n'

; Plot the I-ratio as a function of mu
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/631/graph1'
    ps_open, fpath, /color, thick=2
endif else window, 0, xsize=800, ysize=600
plot, mu, Irat[*,0], title='Graph 1', xtitle=muchar, color=0, background=255, charsize=1.5, $
      ytitle='I!I'+lamchar+'!N(0,'+muchar+') / I!I'+lamchar+'!N(0,1)', $
      xrange=[0, 1.1], yrange=[0, 1.1], xstyle=1, ystyle=1, /nodata
for j=0, nrows-1 do begin
    oplot, muarr, I0mu[*,j], psym=8, color=j*15
    oplot, mu, Irat[*,j], color=j*15
endfor 

; Plot temperature as a function of optical depth
if not keyword_set(ps) then window, 1, xsize=800, ysize=600
plot, tau, T[*,0], xtitle=tauchar, color=0, background=255, charsize=1.5, /nodata, $
      title='Graph 2a', ytitle='Temperature (K)', /xlog, yrange=[4000, 8000]
for j=0, nrows-1 do oplot, tau, T[*,j], color=j*15

; Plot absorption coefficient as a function of wavelength
if not keyword_set(ps) then window, 2, xsize=800, ysize=600
plot, lam, kapparat, xtitle='wavelength ('+angstrom+')', color=0, background=255, charsize=1.5, $
      ytitle='!7j!Ik!N!X / !7j!Ik!X=5000!sA!r!9%!X!n', psym=-4, title='Graph 2b     T = 6000K'

; Write this to a table
kaptab = '~/Desktop/Astro/classes/631/kaptab.txt'
header = ['WAVELENGTH   KAPPA RATIO', '----------   -----------']
forprint, lam, kapparat, textout=kaptab, comment=header, format='(2x,i5,9x,f5.3)'

; Plot temperature as a function of 5000A optical depth, for all wavelengths
if not keyword_set(ps) then window, 3, xsize=800, ysize=600
;plot, tau, T5[*,ind5000], xtitle=tauchar, color=0, background=255, charsize=1.5, thick=6, $
plot, tau/kapparat[ind5000], T[*,ind5000], xtitle=tauchar, color=0, background=255, $
      ytitle='Temperature (K)', /xlog, yrange=[4000, 8000], thick=4, charsize=1.5, $
      title='Graph 2c     !7s!Ik!N!X = !7s!X(!7k!X=5000'+angstrom+')'
;for j=0, nrows-1 do oplot, tau, T5[*,j], color=j*15
for j=0, nrows-1 do oplot, tau/kapparat[j], T[*,j], color=j*15

; Plot density as a function of 5000A optical depth, for fixed T = 6000K
if not keyword_set(ps) then window, 4, xsize=800, ysize=600
plot, tau, rho, xtitle=tauchar, color=0, background=255, charsize=1.5, $
      ytitle='Density (cm!E-3!N)', /xlog, /ylog, $
      title='Graph 2d     '+lamchar+' = 5000'+angstrom+'     T = 6000K'

if keyword_set(ps) then ps_close

END
