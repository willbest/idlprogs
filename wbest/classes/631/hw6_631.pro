PRO HW6_631, PS=ps

;  Calculations for Homework #6, concerning atmospheric absorption of the Sun.
;  
;  HISTORY
;  Written by Will Best (IfA), 3/18/2012
;
;  KEYWORDS
;      PS - write postscript file of the graph
;

c = 3e10                        ; cm s-1
g = [1e2, 1e4]                  ; cm s-2
ng = n_elements(g)
h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1
mH = 1.67e-24                   ; g
sig0 = 3.7e8                    ; cm5 s-3 K.5
sige = 6.6e-25                  ; cm2
Teff = 20000.                   ; K

; PART 1: T vs. n_e
;n_e = findgen(9991)*1e13 + 1e12
n_e = 10.^(findgen(51)/10.+12.)
Tff = (n_e*(h/k)^3/196. * sig0/sige)^(2./7)


; PART 2: Total absorbtion
lenn_e = n_elements(n_e)
kapesc = n_e*sige

;T = findgen(491)*100 + 1000
T = 10.^(findgen(90)*.02 + 3)
lenT = n_elements(T)
denom = 8.*(!pi*k/h)^3/(15*c^2)*!pi*k*T^3

; frequency min 1e13, max 1e16
nustep = 1e13
nu = findgen(1e16/nustep+1)*nustep + nustep
lennu = n_elements(nu)

kapR = fltarr(lenn_e, lenT)
numer = fltarr(lenn_e, lenT)
for j=0, lenT-1 do begin
    bolt = exp(h*nu/k/T[j])
    dBdT = 2/k*(h*nu^2/c/T[j])^2 * bolt/(bolt-1)/(bolt-1)
    for i=0, lenn_e-1 do begin
        kapsum = n_e[i]*(n_e[i]/nu*sig0/nu^2/sqrt(T[j])*(1-1./bolt) + sige)
        integ = dBdT/kapsum
        numin = min(where(integ gt max(integ)/1e3), max=numax)
        numer[i,j] = tsum(nu[numin:numax], integ[numin:numax])
    endfor
    kapR[*,j] = denom[j] / numer[*,j]
endfor


; PART 3: mmm, strata
tau = findgen(10001)/100
dtau = tau[1] - tau[0]
ntau = n_elements(tau)

; Temperature stratification
Ts = (.75 * Teff^4 * (tau + 2./3))^.25

; Pressure stratification
denom2 = 8.*(!pi*k/h)^3/(15*c^2)*!pi*k*Ts^3
kapR2 = fltarr(ng)
Prs = fltarr(ntau, ng)
Prs[0,*] = 0.
Prs[1,*] = g*mH*tau[1]/sige
for j=2, ntau-1 do begin
    nem1 = Prs[j-1,*] / (2*k*Ts[j])
    bolt = exp(h*nu/k/Ts[j])
    dBdT = 2/k*(h*nu^2/c/Ts[j])^2 * bolt/(bolt-1)/(bolt-1)
    for i=0, ng-1 do begin
        kapsum = nem1[i]*(nem1[i]/nu*sig0/nu^2/sqrt(Ts[j])*(1-1./bolt) + sige)
        integ = dBdT/kapsum
        numin = min(where(integ gt max(integ)/1e3), max=numax)
        numer2 = tsum(nu[numin:numax], integ[numin:numax])
        kapR2[i] = denom2[j] / numer2
    endfor
    dPrs = g*mH*Prs[j-1,*]/(2.*k*Ts[j]*kapR2)
    Prs[j,*] = Prs[j-1,*] + dPrs * dtau
endfor

; Particle density stratification
ns = 2*[ [Prs[*,0]/(k*Ts)], [Prs[*,1]/(k*Ts)] ]


; Load a color table.  Default is lincolr.pro.
device, decomposed=0
cubehelix
black = 0
white = 255
green = 100
red = 150
blue = 200

; Postscript or window output
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/631/hw61'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 20, retain=2, xsize=800, ysize=600
endelse

; Get ready to graph
plotsym, 0, 1, /fill
kapchar='!7j!X'
;lamchar='!7k!X'
;muchar='!7l!X'
tauchar='!7s!X'
;angstrom = '!sA!r!u!9 %!X!n'

; Plot log T vs. log n_e
plot, alog10(n_e), alog10(Tff), title='Opacity Due to Free Electrons', $
      background=white, color=black, charsize=1.5, linestyle=2, $
      xtitle='log n!De!N', $
      ytitle='log T', yrange=[3,4.8], ystyle=1
; Upper left -- scattering is dominant
xyouts, min(alog10(n_e))+.5, max(alog10(Tff))-.4, color=black, charsize=2.5, $
        kapchar+'!De!S!E-!R sc!N is dominant'
; Lower right -- free-free absorption is dominant
xyouts, max(alog10(n_e))-.5, min(alog10(Tff))+.2, color=black, charsize=2.5, align=1, $
        '!S!U-!R'+kapchar+'!S!DRoss!R!Uff!N   is dominant'
; Along the line
xyouts, (min(alog10(n_e))+max(alog10(n_e)))/2., (min(alog10(Tff))+max(alog10(Tff)))/2.+.06, $
         color=black, charsize=2.5, align=.5, orientation=28, $
         kapchar+'!De!S!E-!R sc!N = !S!U-!R'+kapchar+'!S!DRoss!R!Uff!N'

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Desktop/Astro/classes/631/hw62'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 21, retain=2, xsize=800, ysize=600
endelse

; Plot the total Roseeland opacity on the log T vs. log n_e plane
plot, alog10(n_e), alog10(Tff), title='Opacity Due to Free Electrons', $
      background=white, color=black, charsize=1.5, linestyle=2, $
      xtitle='log n!De!N', $
      ytitle='log T', yrange=[3,4.8], ystyle=1
maxkap = max(alog10(kapR), min=minkap)
span = maxkap - minkap
cset = 255 - 255.*(alog10(kapR) - minkap)/span
for i=0, lenn_e-1 do begin
    for j=0, lenT-1 do plots, alog10(n_e[i]), alog10(T[j]), psym=8, color=cset[i,j]
endfor

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Desktop/Astro/classes/631/hw63'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 22, retain=2, xsize=800, ysize=600
endelse

; Plot the temperature structure
plot, alog10(tau), Ts, title='Temperature structure, T!Deff!N = 20,000 K', $
      background=white, color=black, charsize=1.5, $
      xtitle='log '+tauchar, $
      ytitle='Temperature (K)'

if keyword_set(ps) then begin
    ps_close
    fpath = '~/Desktop/Astro/classes/631/hw64'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 23, retain=2, xsize=800, ysize=600
endelse

; Plot the pressure structure
plot, alog10(tau), alog10(Prs[*,0]), title='Pressure structure, T!Deff!N = 20,000 K', $
;plot, alog10(tau), Prs[*,0], title='Pressure structure, T!Deff!N = 20,000 K', $
      background=white, color=black, charsize=1.5, $
      xtitle='log '+tauchar, $
      ytitle='log Pressure (dyne cm!E-2!N)', yrange=[0,6]
oplot, alog10(tau), alog10(Prs[*,1]), linestyle=2, color=black
gl = ['g = 10!E2!N cm s!E-2!N', 'g = 10!E4!N cm s!E-2!N']
lset = [0,2]
glr = reverse(gl)
lsetr = reverse(lset)
legend, glr, chars=1.5, lines=lsetr, outline=black, color=black, textcolor=black


if keyword_set(ps) then begin
    ps_close
    fpath = '~/Desktop/Astro/classes/631/hw65'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 24, retain=2, xsize=800, ysize=600
endelse

; Plot the particle density structure
plot, alog10(tau), alog10(ns[*,0]), title='Particle density structure, T!Deff!N = 20,000 K', $
;plot, alog10(tau), nrs[*,0], title='Pressure structure, T!Deff!N = 20,000 K', $
      background=white, color=black, charsize=1.5, $
      xtitle='log '+tauchar, $
      ytitle='log n (cm!E-3!N)', yrange=[12,17]
oplot, alog10(tau), alog10(ns[*,1]), linestyle=2, color=black
gl = ['g = 10!E2!N cm s!E-2!N', 'g = 10!E4!N cm s!E-2!N']
lset = [0,2]
glr = reverse(gl)
lsetr = reverse(lset)
legend, glr, chars=1.5, lines=lsetr, outline=black, color=black, textcolor=black


if keyword_set(ps) then ps_close

END
