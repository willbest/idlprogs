PRO SPECLINE, CUBE=cube, PS=ps

;  Calculations for Problem Set #4 for Astro 631 (Radiative Transfer), plotting
;  the intensity ratio in a spectral line.
;
;  HISTORY
;  Written by Will Best (IfA), 02/23/2012
;
;  KEYWORDS
;      CUBE - Use CubeHelix colors
;      PS - output to postscript

; Calculate arrays
beta0 = [1., 9., 999.]
nrows = n_elements(beta0)
x = findgen(1001)/100 - 5       ; x represents delta-lambda / characteristic half-width
Irat = fltarr(n_elements(x), nrows, /nozero)
for j=0, 2 do Irat[*,j] = .5 + 1/(2*(1 + beta0[j]*exp(-x^2)))

; Load a color table.  Default is lincolr.pro.
device, decomposed=0
if keyword_set(cube) then begin
    cubehelix
    black = 0
    white = 255
    green = 100
    red = 150
    blue = 200
endif else begin
    lincolr, /silent
    black = 0
    white = 1
    green = 2
    red = 3
    blue = 4
endelse

; Postscript or window output
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/631/specline'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, retain=2, xsize=800, ysize=600
endelse

; Special characters
;plotsym, 0, .5, /fill
betachar='!7b!X'
deltacap='!7D!X'
;kapchar='!7j!X'
lamchar='!7k!X'
;tauchar='!7s!X'
;angstrom = '!sA!r!u!9 %!X!n'

; Plot
plot, x, Irat[*,0], title='Spectral Line Absorption', background=white, color=black, lines=2, $
      xrange=[-5, 5], yrange=[0.3, 1.1], xstyle=1, ystyle=1, $
      xtitle=deltacap+lamchar+' / '+deltacap+lamchar+'!ID!N', charsize=1.5, $
      ytitle='I!I'+deltacap+lamchar+'!N(0) / I!Ic!N(0)'
oplot, x, Irat[*,1], lines=0, color=black;color=red
oplot, x, Irat[*,2], lines=4, color=black;color=blue
gl = [betachar+'!I0!N = 1', betachar+'!I0!N = 99', betachar+'!I0!N = 999']
;cset = [black, red, blue]
;legend, gl, chars=1.5, colors=cset, textcolors=cset, linestyle=0, outline=black, /bottom
legend, gl, chars=1.5, linestyle=[2, 0, 4], outline=black, /bottom, colors=black, textcolors=black

if keyword_set(ps) then ps_close

END

