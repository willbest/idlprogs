PRO GRAVTAUNE, PS=ps

;  Calculations for Problem Set #2 for Astro 631 (Radiative Transfer), plotting
; electron density as a function of optical depth and gravity.
;
;  HISTORY
;  Written by Will Best (IfA), 2/5/2012
;
;  KEYWORDS
;      PS - output to postscript

c = 3e10                        ; cm s-1
h = 6.63e-27                    ; erg s
k = 1.38e-16                    ; erg K-1
mp = 1.67e-24                   ; g

a = 1.61e-13                    ; cm3
T = 1e4                         ; K
sig = 6.6e-25                   ; cm2

; PART A: log n_e vs. log tau
tau = findgen(100000)*.001 + .001
g = 10^.8
HH1 = 2*k*T/(g*mp)
n_e1 = (-1. + sqrt(1+2*a*tau/(HH1*sig))) / a
g = 10^4.
HH2 = 2*k*T/(g*mp)
n_e2 = (-1. + sqrt(1+2*a*tau/(HH2*sig))) / a

lincolr, /silent
device, decomposed=0
window, 21, retain=2, xsize=800, ysize=600
plot, alog10(tau), alog10(n_e1), ytitle='log(Electron Denisty) [cm-3]', color=0, $
      background=1, xtitle='log(tau)', charsize=1.5, thick=2, yrange=[5, 20], $
      title='Electron Density in H-delta, T = 10000 K'
oplot, alog10(tau), alog10(n_e2), color=3, thick=2
gl = ['log g = 4.0', 'log g = 0.8']
cset = [3, 0]
legend, gl, chars=1.5, colors=cset, textcolors=cset, linestyle=0
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/631/nevtau'
    ps_open, fpath, /color, thick=4
    plot, alog10(tau), alog10(n_e1), ytitle='log(Electron Denisty) [cm-3]', color=0, $
          background=1, xtitle='log(tau)', charsize=1.5, thick=4, yrange=[5, 20], $
          title='Electron Density in H-delta, T = 10000 K'
    oplot, alog10(tau), alog10(n_e2), color=3, thick=4
    gl = ['log g = 4.0', 'log g = 0.8']
    cset = [3, 0]
    legend, gl, chars=1.5, colors=cset, textcolors=cset, linestyle=0
    ps_close
endif


; PART B: log n_e vs. log g
ta = 0.5
g = findgen(10000)+1
HH = 2*k*T/(g*mp)
n_e = (-1. + sqrt(1+2*a*ta/(HH*sig))) / a

window, 22, retain=2, xsize=800, ysize=600
plot, alog10(g), alog10(n_e), ytitle='log(Electron Denisty) [cm-3]', color=0, $
      background=1, xtitle='log(g)', charsize=1.5, thick=2, xrange=[0.5, 4], yrange=[5, 20], $
      title='Electron Density in H-delta, T = 10000 K, tau = 0.5'
if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/classes/631/nevg'
    ps_open, fpath, /color, thick=4
    plot, alog10(g), alog10(n_e), ytitle='log(Electron Denisty) [cm-3]', color=0, $
          background=1, xtitle='log(g)', charsize=1.5, thick=2, xrange=[0.5, 4], yrange=[5, 20], $
          title='Electron Density in H-delta, T = 10000 K, tau = 0.5'
    ps_close
endif

END

