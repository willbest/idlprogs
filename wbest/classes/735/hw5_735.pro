PRO HW5_735, X1, Y1, X2, Y2, FPATH, CUBE=cube, PS=ps, XRAN1=xran1, XRAN2=xran2

; KEYWORDS
;     CUBE - use the Cubehelix color table
;     PS - Output to postscript file
;     FILE - Path/name for postscript output

el1 = n_elements(x1)
el2 = n_elements(x2)
hline1 = replicate(median(y1), el1)
hline2 = replicate(median(y2), el2)

; Load a color table.  Default is lincolr.pro.
device, decomposed=0
if keyword_set(cube) then begin
    cubehelix
    black = 0
    white = 255
    red = 150
    blue = 200
endif else begin
    lincolr, /silent
    black = 0
    white = 1
    red = 3
    blue = 4
endelse

; Special symbols
plotsym, 0, .5, /fill
angstrom = '!sA!r!u!9 %!X!n'

; Postscript or window output
if keyword_set(ps) then begin
    if size(fpath, /type) ne 7 then fpath = '~/Desktop/Astro/classes/735\ Research\ Tech/plot'
    ps_open, fpath, /color, /en, /portrait, thick=2
endif else begin
    window, retain=2, xsize=600, ysize=800
endelse

; Set two vertically stacked plot panels
!p.multi = [0, 0, 2, 0, 0]

; Plot the data
plot, x1, y1, xrange=xran1, background=white, color=black, psym=8, $
      xtickname=replicate(' ',6), ytitle='Flux', charsize=1.5, ytickname=' ', $
      ymargin=[0,4]  ; removes the bottom margin
oplot, x1, y1, color=red
oplot, x1, hline1, color=blue, thick=8, linestyle=2
xyouts, 6548, 2, '[NII]', color=black, charsize=1.5, align=0.5
xyouts, 6563, 10.6, 'H!7a!X', color=black, charsize=1.5, align=0.5
xyouts, 6583.2, 4.5, '!3[NII]', color=black, charsize=1.5, align=0.5

plot, x2, y2, xrange=xran2, background=white, color=black, psym=8, $
      xtitle='!6Wavelength, '+angstrom+'!6', ytitle='Flux', charsize=1.5, $
      yrange=[-3,20], ystyle=1, ymargin=[4,0] ; removes the top margin
oplot, x2, y2, color=red
oplot, x2, hline2, color=blue, thick=8, linestyle=2
xyouts, 6548.3, 6.5, '!6[NII]', color=black, charsize=1.5, align=0.5
xyouts, 6562.6, 17.8, '!6H!7a!X', color=black, charsize=1.5, align=0.5
xyouts, 6583.2, 7.5, '!3[NII]', color=black, charsize=1.5, align=0.5

if keyword_set(ps) then ps_close
!p.multi = [0, 0, 1, 0, 0]

END

