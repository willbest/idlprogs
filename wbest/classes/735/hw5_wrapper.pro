PRO HW5_WRAPPER, FPATH, CUBE=cube, PS=ps

; KEYWORDS
;     CUBE - use the Cubehelix color table
;     PS - Output to postscript file
;     FILE - Path/name for postscript output

; Read in the spectrum data
readcol, '~/idlprogs/wbest/classes/735/spectra1.txt', wave1, flux1, err1, format='f,f,f'
el1 = n_elements(wave1)
readcol, '~/idlprogs/wbest/classes/735/spectra2.txt', wave2, flux2, err2, format='f,f,f'
el2 = n_elements(wave2)

; Make arrays of structures for the data
spec1 = replicate({w:0., f:0., e:0.}, el1)
spec1.w = wave1
spec1.f = flux1
spec1.e = err1

spec2 = replicate({w:0., f:0., e:0.}, el2)
spec2.w = wave2
spec2.f = flux2
spec2.e = err2

xr = [6540, 6590]

hw5_735, spec1.w, spec1.f, spec2.w, spec2.f, fpath, cube=cube, ps=ps, $
         xran1=xr, xran2=xr

END

