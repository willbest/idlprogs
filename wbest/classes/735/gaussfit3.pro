PRO GAUSSFIT3, WAVE, FLUX, ERR, FPATH, CUBE=cube, PS=ps

;  Fit Gaussian functions to the three prominent lines in a spectrum.
;
;  HISTORY
;  Written by Will Best (IfA), 04/10/2012
;
;  INPUTS
;      WAVE - vector of wavelengths
;      FLUX - vector of flux values to be fit
;      ERR - flux errors to be fit
;      FPATH - path for postcript file output
;
;  KEYWORDS
;      CUBE - use the Cubehelix color table
;      PS - Output to postscript file
;

if n_params() lt 3 then message, 'gaussfit3, wave, flux, err [, fpath, cube=cube, ps=ps]'

; Make an array of structures for the data
wmin = 6540.
imin = closest(wave, 6540)
wmax = 6590.
imax = closest(wave, 6590)
el = n_elements(wave[imin:imax])

spec = replicate({w:0., f:0., e:0.}, el)
spec.w = wave[imin:imax]
spec.f = flux[imin:imax]
spec.e = err[imin:imax]

backg = median(spec.f)
;hline = replicate(backg, el)

half = 50
;; npeaks = 3
;; peak = fltarr(npeaks)
;; dev = fltarr(npeaks)
;; wv = fltarr(half*2, npeaks)
;; gfit = fltarr(half*2, npeaks)
;; parms = fltarr(3, npeaks)
;; for i=0, npeaks-1 do begin
;;     peak[i] = max(spec[i*el/3:(i+1)*el/3-1].f, maxind)
;;     maxind = maxind + i*el/3
;;     lowend = (maxind-half)>0
;;     highend = (maxind+half)<el-1
;;     wv[*,i] = spec[lowend:highend].w
;;     fl = spec[lowend:highend].f
;;     er = spec[lowend:highend].e
;;     dev[i] = abs(wv[closest(fl, .5*peak[i]),i] - spec[maxind].w)
;; print, dev[i]
;;     start_parms = [wv[half,i], peak[i], dev[i]]
;;     parms[*,i] = mpfitfun('gaussw', wv[*,i], fl, er, start_parms, yfit=yfit, /quiet)
;;     gfit[*,i] = yfit ;gaussw(wv[*,i], parms[*,i])
;; endfor
peak = fltarr(3)
dev = fltarr(3)
maxind = intarr(3)
gfit3 = fltarr(el, 3)

start_parms3 = [backg]
for i=0, 2 do begin
    peak[i] = max(spec[i*el/3:(i+1)*el/3-1].f)
    maxind[i] = where(spec.f eq peak[i])
    lowend = (maxind[i]-half)>0
    highend = (maxind[i]+half)<el-1
    wv3 = spec[lowend:highend].w
    fl3 = spec[lowend:highend].f
    dev[i] = abs(wv3[closest(fl3, .5*peak[i])] - spec[maxind[i]].w) / (2*alog(2))
    start_parms3 = [start_parms3, spec[maxind[i]].w, peak[i], dev[i]]
endfor
parinfo = replicate({value:0.d, fixed:0b, tied:''}, 10)
parinfo.value = start_parms3
;parinfo[0].fixed = 1
parinfo[6].tied = 'P[6]'
parinfo[9].tied = 'P[6]'
parms3 = mpfitfun('gauss3w', spec.w, spec.f, spec.e, parinfo=parinfo, yfit=gfit3, $
                  covar=covar, nfree=nfree, bestnorm=chis, /quiet)
;parms3 = mpfitfun('gauss3w', spec.w, spec.f, spec.e, start_parms3, yfit=gfit3, /quiet)
;gfit31 = mpcurvefit(spec.w, spec.f, 1./spec.e^2, start_parms3, function='gauss3wp', /quiet, /noder)
;parms31 = start_parms3

; Area under Gaussian curve = amplitude * stddev * sqrt(2*pi)
area = fltarr(3)
arerr = fltarr(3)
for i=0, 2 do begin
covars = [ [covar[0,0], covar[0,3*i+1], covar[0,3*i+2], covar[0,3*i+3]], $
           [covar[3*i+1,0], covar[3*i+1,3*i+1], covar[3*i+1,3*i+2], covar[3*i+1,3*i+3]], $
           [covar[3*i+2,0], covar[3*i+2,3*i+1], covar[3*i+2,3*i+2], covar[3*i+2,3*i+3]], $
           [covar[3*i+3,0], covar[3*i+3,3*i+1], covar[3*i+3,3*i+2], covar[3*i+3,3*i+3]] ]
area[i] = gaussarea(parms3[0], parms3[3*i+2], parms3[3*i+3], covars, err=err)
arerr[i] = err
print, parms3[3*i+1], area[i], arerr[i], $
       format='("Flux of line centered at ",f7.2," A is ",f6.3," +/- ",f5.3," units")'
endfor
print, 'nfree = ', trim(nfree)
print, 'chi^2 = ', trim(chis)
print, 'reduced chi^2 = ', trim(chis/(el-nfree))

; Load a color table.  Default is lincolr.pro.
device, decomposed=0
if keyword_set(cube) then begin
    cubehelix
    black = 0
    white = 255
    red = 150
    blue = 200
endif else begin
    lincolr, /silent
    black = 0
    white = 1
    green = 2
    red = 3
    blue = 4
endelse

; Special symbols
plotsym, 0, .5, /fill
angstrom = '!sA!r!u!9 %!X!n'

; Postscript or window output
if keyword_set(ps) then begin
    if size(fpath, /type) ne 7 then fpath = '~/Desktop/Astro/classes/735\ Research\ Tech/plotgauss'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, retain=2, xsize=700, ysize=525
endelse

; Plot the data
plot, spec.w, spec.f, background=white, color=black, psym=8, $
      xtitle='!6Wavelength ('+angstrom+'!6)', ytitle='Flux Density', charsize=1.5
oplot, spec.w, spec.f, color=red
;for i=0, 2 do oplot, wv[*,i], gfit[*,i], color=green, thick=2, lines=2
oplot, spec.w, gfit3, color=blue, thick=6, lines=3
;oplot, spec.w, gfit31, color=green, thick=2, lines=2
;oplot, spec.w, hline, color=green, thick=2, linestyle=2
xyouts, 6548, 2, '!6[NII]', color=black, charsize=1.5, align=0.5
;xyouts, 6548, spec[where(spec.w eq 6548)].f + .4, '!6[NII]', color=black, charsize=1.5, align=0.5
xyouts, 6563, 10.6, '!6H!7a!X', color=black, charsize=1.5, align=0.5
xyouts, 6583.2, 4.5, '!3[NII]', color=black, charsize=1.5, align=0.5

if keyword_set(ps) then ps_close

END

