;week = {day:'', byte:0b, hour:intarr(24), am_pm:bytarr(24)}
;week = replicate(week, 7)
;week.day = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
;week.byte = [1,1,1,1,1,0,0]
;week.hour = [indgen(12)+1, indgen(12)+1]
;week.am_pm = [replicate(0, 12), replicate(1, 12)]

;week = replicate({day:'', wkdy:0b, hour:[12, indgen(11)+1, 12, indgen(11)+1], am_pm:[replicate(0B, 12), replicate(1B, 12)]}, 7)
week = replicate({day:'', wkdy:0b, hour:[12, indgen(11)+1, 12, indgen(11)+1], am_pm:[bytarr(12), bytarr(12)+1B]}, 7)
week.day = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
;week.wkdy = [1,1,1,1,1,0,0]
week[0:4].wkdy = 1

END

