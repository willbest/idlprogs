PRO HW4_735, FPATH, CUBE=cube, PS=ps

; KEYWORDS
;     CUBE - use the Cubehelix color table
;     PS - Output to postscript file
;     FILE - Path/name for postscript output

; Read in the spectrum data
readcol, '~/idlprogs/classes/735/spectra1.txt', wave, flux, err, format='f,f,f'
el = n_elements(wave)

; Make an array of structures for the data
spec = replicate({w:0., f:0., e:0.}, el)
spec.w = wave
spec.f = flux
spec.e = err

; Save the data
save, spec, file='~/idlprogs/classes/735/spectra1.sav'
outfits = '~/idlprogs/classes/735/spectra1.fits'
mwrfits, [[spec.w], [spec.f], [spec.e]], outfits, /create, alias=al, status=s

hline = replicate(median(spec.f), el)

; Load a color table.  Default is lincolr.pro.
device, decomposed=0
if keyword_set(cube) then begin
    cubehelix
    black = 0
    white = 255
    red = 150
    blue = 200
endif else begin
    lincolr, /silent
    black = 0
    white = 1
    red = 3
    blue = 4
endelse

; Special symbols
plotsym, 0, .5, /fill
angstrom = '!sA!r!u!9 %!X!n'
!p.multi = [0, 0, 1, 0, 0]

; Postscript or window output
if keyword_set(ps) then begin
    if size(fpath, /type) ne 7 then fpath = '~/Desktop/Astro/classes/735\ Research\ Tech/plot'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, retain=2, xsize=700, ysize=525
endelse

; Plot the data
plot, spec.w, spec.f, xrange=[6540, 6590], background=white, color=black, psym=8, $
      xtitle='!6Wavelength, '+angstrom+'!6', ytitle='Flux', charsize=1.5
oplot, spec.w, spec.f, color=red
oplot, spec.w, hline, color=blue, thick=2, linestyle=2
xyouts, 6548, 2, '!6[NII]', color=black, charsize=1.5, align=0.5
;xyouts, 6548, spec[where(spec.w eq 6548)].f + .4, '!6[NII]', color=black, charsize=1.5, align=0.5
xyouts, 6563, 10.6, '!6H!7a!X', color=black, charsize=1.5, align=0.5
xyouts, 6583.2, 4.5, '!3[NII]', color=black, charsize=1.5, align=0.5

if keyword_set(ps) then ps_close

END

