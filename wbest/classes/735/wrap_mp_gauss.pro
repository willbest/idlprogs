pro wrap_mp_gauss, x, y, errors, PARAM=param, SIGMA=sigma

parinfo = {value:0d, limited:[0b,0], limits:[0d,0d]}
parinfo = replicate(parinfo, 9)

start_param = [a, b, ... 9 values ]

yfit = mpcurvefit(x, y, errors, start_param, function_name='gaussfitw', /no_deriv)

param = start_param

END
