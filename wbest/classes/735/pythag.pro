FUNCTION PYTHAG, leg1, leg2, area=area, ang1=ang1, ang2=ang2, degree=degree

el = n_elements(leg1)
stop
hyp = sqrt(leg1^2 + leg2^2)
area = .5*leg1*leg2
ang1 = acos(leg1/hyp)
ang2 = acos(leg2/hyp)

if keyword_set(degree) then begin
    rtg = 180/!pi
    ang1 = ang1*rtg
    ang2 = ang2*rtg
endif

return, hyp

END
