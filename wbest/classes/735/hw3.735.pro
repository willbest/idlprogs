PRO HW3.735

; Will Best
; 02/08/12

; PROBLEM #1
n = 10000
p1 = randomn(seed, n)
dummy = moment(p1, sdev=sd)
print, 'standard devation = ', sd
n1 = n_elements(where((p1 ge -sd) and (p1 le sd))) 
print, 100.*n1/n, ' percent of data is within 1 standard deviation.'
n2 = n_elements(where((p1 ge -2*sd) and (p1 le 2*sd))) 
print, 100.*n2/n, ' percent of data is within 1 standard deviation.'
n3 = n_elements(where((p1 ge -3*sd) and (p1 le 3*sd))) 
print, 100.*n3/n, ' percent of data is within 1 standard deviation.'

; PROBLEM #2
n = 1000
;p2 = randomn(seed, n, 2, /uni)*10
p2 = randomu(seed, n, 2)*10
p2[n:*] = p2[n:*] - 5
x2 = p2[0:n-1]
y2 = p2[n:*]
avgx = moment(x2)
avgy = moment(y2)
print, 'Average x value: ', avgx[0]
print, 'Average y value: ', avgy[0]
r1 = where((x2 gt 5) and (y2 lt -2))
print, n_elements(r1), ' pairs with x>5 and y<-2'
r2 = where((x2 gt 2) and (x2 lt 7) and (y2 gt -2.5) and (y2 lt 2.5))
print, 100.*n_elements(r2)/n, ' percent of pairs with 2<x<7 and -2.5<y<2.5'
r3 = where(((x2 lt 3) or (x2 gt 8)) and ((y2 lt -2.5) or (y2 gt 2.5)))
print, 100.*n_elements(r3)/n, ' percent of pairs with x<3 or x>8 and y<-2.5 or y>2.5'
r4 = where(sqrt((x2-5)^2 + y2^2) le 2)
print, 100.*n_elements(r4)/n, ' percent of points within 2 units of (5,0)'

; PROBLEM #3
p3 = randomn(seed, 20)
p3 = p3 - min(p3)
p3 = p3 / max(p3) * 20
p3 = fix(round(p3[sort(p3)]))
print, p3[uniq(p3)]

; PROBLEM #4
p4 = indgen(10)
index = where(p4 eq 5)
;index = index[0]
print, p4[index] + p4

END
