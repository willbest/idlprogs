PRO HW6_735, FPATH, _EXTRA=ex

; Wrapper for gaussfit3.pro
;
; KEYWORDS
;     CUBE - use the Cubehelix color table
;     PS - Output to postscript file
;

; Read in the spectrum data
readcol, '~/idlprogs/wbest/classes/735/spectra1.txt', wave, flux, err, format='f,f,f', /silent

if size(fpath, /type) ne 7 then fpath = '~/Desktop/Astro/classes/735\ Research\ Tech/plotgauss'

gaussfit3, wave, flux, err, fpath, _extra=ex

END

