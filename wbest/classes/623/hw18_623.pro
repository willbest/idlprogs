;ps=1
;PRO HW15_623, PS=ps

; Written by Will Best, 11/23/2012

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
Vsun = 4./3*!pi*Rsun^3          ; cm3
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
Rearth = 6.3781e8               ; cm
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
T_c = 1.5e7                       ; K
rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
ergeV = 1.60218e-12             ; erg eV-1
XI = ergeV * 13.6057            ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #33
Wn = 0.770140
rhorat = 5.99070
T = 2e6

Qn = 3.*rhorat / (4*!pi)
A = G * m_u * Wn / kB / Qn
B = .2 * (3./8/!pi)^(2./3) * h * Qn^(2./3) / kB / m_e * h / m_u^(2./3)
co1 = (4*B/A^2)^(.75)
co2 = co1 / 2e33 * (1e6)^(.75)
mu_e = 2./(1+X)
mu = 4. / (3 + 5*X - Z)
Mig = co2 * mu^(-.75) * mu_e^(-1.25) * (T/1e6)^(.75)
print
print, 'Q_n = '+trim(Qn)
print, 'A = '+trim(A)+' * mu_c'
print, 'B = '+trim(B)+' * mu_c / (mu_e,c)^(5/3)'
print
print, 'Coefficient 1 for M_ig: '+trim(co1)
print, 'Coefficient 2 for M_ig: '+trim(co2)
print
print, 'Minimum M_ig for H burning: '+trim(Mig)+' Msun'
print


END

