;ps=1
;PRO HW19_623

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
Vsun = 4./3*!pi*Rsun^3          ; cm3
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
Rearth = 6.3781e8               ; cm
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
T_c = 1.5e7                       ; K
rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
ergeV = 1.60218e-12             ; erg eV-1
XI = ergeV * 13.6057            ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #34
delm_p = 7.289
delm_4He = 2.425
Eneu = 0.263
delm_C = 0.
E0 = 4*delm_p - delm_4He
Ealpha = E0 - 2*Eneu
print
print, 'Energy generated in producing an alpha particle: '+trim(E0)+' MeV'
print, 'Subtracting the neutrino energy: '+trim(Ealpha)+' MeV'

EC = 3*delm_4He - delm_C
print
print, 'Energy generated producing three alpha particles: '+trim(3*Ealpha)+' MeV'
print, 'Energy generated procucing one 12C nucleus: '+trim(EC)+' MeV'

hblife = 1e10*EC / (50.*3*Ealpha)
print
print, 'Horizontal Branch liftime: '+trim(hblife)+' years'


; Problem #35
P = 300. * 8.64e4      ; s
M = 1.5*Msun/1e33      ; g

rho = 3. / (4*!pi*G*P^2)
R = (3.*M/(4*!pi*rho))^(1./3) * 1e11
print
print, 'Period: '+trim(P)+' sec'
print, 'Density: '+trim(rho)+' g cm-3'
print, 'Radius: '+trim(R)+' cm  =  '+trim(R/Rsun)+' solar radii'

print
END

