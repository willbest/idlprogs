ps=1
;PRO HW6_623

; cgs units
a = 7.566e-15                   ; erg cm-3 K-4
k = 1.38065e-16                 ; erg K-1
T = 1.5e7                       ; K
rho = 150.                      ; g cm-3
m_u = 1.66054e-24               ; g
;XI = 1.60218e-12 * 13.6057      ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.35                        ; Hydrogen fraction
Z = 0.02                        ; metal fraction

;mu_e = 2./(1+X)
;n_e = rho/mu_e/m_u

;; ; Calculate the density of neutral H atoms
;; n_H = rho*X/m_u
;; n_I = n_H / (ion + 1.)
;; print, 'n_I = '+trim(n_I)+' cm^-3'
;; 
;; ; Calculate the radius of the volume occupied by each neutral H
;; r_I = (3./4/!pi/n_I)^(1./3)
;; print, 'r_I = '+trim(r_I)+' cm'
;; print, 'r_I = '+trim(r_I*1e8)+' Angstroms'

; Problem #12
logrho = findgen(1801)/100-9
mu1 = 1
logT1 = (logrho + alog10(3*k/a/mu1/m_u)) / 3.
mu2 = 4. / (3 + 5*X - Z)
logT2 = (logrho + alog10(3*k/a/mu2/m_u)) / 3.

device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    fpath = '~/Astro/classes/623/homework/logT_logrho'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
endelse

plot, logrho, logT1, backg=1, color=0, charsize=1.5, $;psym=8, $
      xtitle='log !7q!X (g cm!U-3!N)', xrange=[-9,9], xstyle=1, $
      ytitle='log T (K)', yrange=[3,9], ystyle=1
oplot, logrho, logT2, lines=2, color=0
legend, ['Pure H gas', 'X=0.35, Z=0.02'], textc=0, outline=0, colors=0, $
        lines=[0,2], chars=1.5, /right, /bottom
; Upper left -- radiation pressure is dominant
xyouts, -8, 8, 'Radiation pressure', color=0, charsize=2.5
xyouts, -8, 7.65, 'is dominant', color=0, charsize=2.5
; Lower right -- gas pressure is dominant
xyouts, 0, 6, 'Gas pressure', color=0, charsize=2.5
xyouts, 0, 5.65, 'is dominant', color=0, charsize=2.5

if keyword_set(ps) then ps_close


;Problem #13

; Ratio of pressures
mu_e = 2. / (1 + X)
Prat = 1.004e13 * (mu2*m_u/k/T)/mu_e * rho^(2./3)
print, 'P_e / P_gas = '+trim(Prat)

; Boundary separating nondegenerate and degenerate
logTd = 13. + alog10(mu2*m_u/k/(mu_e^(5./3))) + 2/3.*logrho

; Boundary separating nonrelativistic and relativistic degenerate
mu_e = 2.
logrhod = 6.268 + alog10(mu_e)

if keyword_set(ps) then begin
    fpath = '~/Astro/classes/623/homework/logT_logrho.2'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 1, retain=2, xsize=800, ysize=600
endelse

plot, logrho, logT2, backg=1, color=0, charsize=1.5, $;psym=8, $
      xtitle='log !7q!X (g cm!U-3!N)', xrange=[-9,9], xstyle=1, $
      ytitle='log T (K)', yrange=[3,9], ystyle=1, lines=2
; Upper left -- radiation pressure is dominant
xyouts, -8, 8, 'Radiation pressure', color=0, charsize=2
xyouts, -8, 7.7, 'is dominant', color=0, charsize=2
; Lower right -- gas pressure is dominant
xyouts, -3, 4.1, 'Gas pressure', color=0, charsize=2
xyouts, -3, 3.8, 'is dominant', color=0, charsize=2
; Put the Sun's core on the plot
plots, alog10(rho), alog10(T), psym=symcat(16), color=4, symsize=3
xyouts, alog10(rho)-2.4, alog10(T)-.1, "Sun's", color=4, charsize=2
xyouts, alog10(rho)-2.4, alog10(T)-.4, "core", color=4, charsize=2
; Line separating nondegenerate and degenerate
oplot, logrho, logTd, lines=2, color=3
; Upper left -- nondegenerate
xyouts, -4, 5.5, 'Nondegenerate', color=3, charsize=1.6
; Lower right -- degenerate (nonrelativistic)
xyouts, 1.9, 5.0, 'Degenerate', color=3, charsize=1.6
xyouts, 1.8, 4.7, '(nonrelativistic)', color=3, charsize=1.6
; Line separating nonrelativistic and relativistic degenerate
plots, [logrhod, logrhod], [3, 9], color=3, lines=1
xyouts, 7.3, 5.5, 'Degenerate', color=3, charsize=1.6, orient=90
xyouts, 8, 5.45, '(relativistic)', color=3, charsize=1.6, orient=90

if keyword_set(ps) then ps_close

END

