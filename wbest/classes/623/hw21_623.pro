;PRO HW21_623

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
Vsun = 4./3*!pi*Rsun^3          ; cm3
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
Rearth = 6.3781e8               ; cm
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
T_c = 1.5e7                       ; K
rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
ergeV = 1.60218e-12             ; erg eV-1
XI = ergeV * 13.6057            ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #36
kappa0 = 4e25
mu_e = 2.
mu = 1.38
M = .6*Msun

coeffA = 16.*!pi*G*a*c/3.
coeffA1 = coeffA*kB/(kappa0*m_u)
coeffT35 = (1.2e-8)^2 * 17.
coeffT352 = coeffT35/coeffA1
coeff101 = (coeffT352*kB*kB*mu_e^2/mu/m_u/m_u/M)^(2./7)
coeff102 = coeff101*(Lsun)^(2./7)
print
print, 'Coefficient for A: '+trim(coeffA)
print, 'Coefficient for A1: '+trim(coeffA1)
print, 'Coefficient for T^3.5: '+trim(coeffT35)
print, 'Coefficient for T^3.5, part 2: '+trim(coeffT352)
print, 'Coefficient for equation 10, part 1: '+trim(coeff101)
print, 'Coefficient for equation 10, part 2: '+trim(coeff102)

Lrat = [.01, .0001, .00003]
Ttr = coeff102 * Lrat^(2./7)
print
print, 'Transition zone temperatures: '+trim(Ttr)

coeffL = m_u/kB*m_u/coeffT352/kB/mu_e^2
coeffL2 = coeffL*Msun
mu_i = 12.
coefftcool = 3.*kB*Msun/(5.*m_u*Lsun)
tcool = coefftcool*.6*Ttr[2]/(mu_i*Lrat[2])
print
print, 'Coefficient for L: '+trim(coeffL)
print, 'Coefficient for L, part 2: '+trim(coeffL2)
print, 'Coefficient for t_cool: '+trim(coefftcool)
print
print, 't_cool = '+trim(tcool)


print
END

