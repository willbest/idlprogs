;ps=1
;PRO HW10_623

; cgs units
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
;T_c = 1.5e7                       ; K
;rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
;XI = 1.60218e-12 * 13.6057      ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #19
musun = 1*.91 + 4*.09
kappa0 = 1.6e-33
a = 0.4
b = 9.3
Teff = 5778

P1 = (G*Msun*(a+1)/Rsun^2/kappa0)^(1/(a+1.))
P2 = (kb/musun/m_u)^(a/(a+1.))
P3 = Teff^((a-b)/(a+1.))
P = P1*P2*P3
print
print, 'Photospheric pressure: '+trim(P)+' dyne cm-2'
print, 'log(P) = '+trim(alog10(P))

rho1 = (G*Msun*(a+1)/Rsun^2/kappa0)^(1/(a+1.))
rho2 = (kb/musun/m_u)^(-1/(a+1.))
rho3 = Teff^(-(b+1.)/(a+1.))
rho = rho1*rho2*rho3
print
print, 'Photospheric pressure: '+trim(rho)+' g cm-3'
print, 'log(rho) = '+trim(alog10(rho))

print
END

