;ps=1
;PRO HW13_623

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
;T_c = 1.5e7                       ; K
;rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
;XI = 1.60218e-12 * 13.6057      ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #23
eps = Lsun/Msun
print
print, 'Energy generation rate per unit mass in the Sun: '+trim(eps)+' erg s-1 g-1'

hum = 2000.*4184.*1e7/80./1000./24./3600.
print, 'Energy generation rate per unit mass for a human: '+trim(hum)+' erg s-1 g-1'

car = 80.*1000.*1e7/1521./1000.
print, 'Energy generation rate per unit mass for a Nissan Leaf: '+trim(car)+' erg s-1 g-1'


; Problem #24
delm1 = 14.93134
delm2 = 14.93134
delm3 = 7.28899
delm4 = 2.42475
Q = (delm1 + delm2 - 2*delm3 - delm4)
print
print, 'Energy produced by He-3 to He-4: '+trim(Q)+' MeV'


; Problem #25
b = 1.25e-3
bk = b*kB
coeff = (bk/2.*1e6)^(2./3)
print
print, 'b*k_B = '+trim(bk)+' Z_1^2*Z_2^2*A erg3/2 K-1'
print, 'Coefficient: '+trim(coeff)+' erg'
print, 'Coefficient: '+trim(coeff/1.6e-9)+' keV'

kB = kB/1.602e-9
ratio = 1.22*(1^2*1^2*.5*15.^2)^(1./3) / 1.5/kB/15e6
print
print, 'Ratio for proton-proton reaction = '+trim(ratio)

print
END

