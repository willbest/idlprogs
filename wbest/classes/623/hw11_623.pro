;ps=1
;PRO HW1!_623

; cgs units
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
;T_c = 1.5e7                       ; K
;rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
;XI = 1.60218e-12 * 13.6057      ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #21
T = 3.39e6
rho = 1.66
musun = 4. / (3 + 5*X - Z)
l = 1e9
r = Rsun/2.
m = 0.9*Msun

C_p = 5*kB/2/musun/m_u
F = Lsun/4/!pi/r^2

dnT = ( 2*F*T^(.5)/l^2/rho/C_p * r/G^(.5)/m^(.5) )^(2./3)

print
print, 'delta nabla T: '+trim(dnT)+' K cm-1'

v = l/2. * sqrt(G*m/r^2/T*dnT)

print, 'average speed: '+trim(v)+' cm s-1'

print
END

