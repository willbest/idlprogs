;ps=1
;PRO HW8_623

; cgs units
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
k = 1.38065e-16                 ; erg K-1
;T = 1.5e7                       ; K
;rho = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
;XI = 1.60218e-12 * 13.6057      ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Z = 0.02                        ; metal fraction


; Problem #16
print
print, 'White Dwarf'
Kw = 1.004e13/2.^(5./3)
Nn = 0.42422
Mw = 0.6*Msun
Rw = Kw/G/Nn/Mw^(1./3)
print, 'Radius of a white dwarf: '+trim(Rw)+' cm'

print
print, 'Neutron Star'
c1 = (3/!pi)^(2./3)*h/20./m_n*h
print, 'P_n = '+trim(c1)+' * (rho/m_n)^(5/3)'
Kn = c1/m_n/m_n^(2./3)
print, 'P_n = '+trim(Kn)+' * rho^(5/3)'
Mn = 1.5*Msun
Rn = Kn/G/Nn/Mn^(1./3)
print, 'Radius of a neutron star: '+trim(Rn)+' cm'
print

; Problem #17
print, 'Problem #17'
xi1 = 6.89685
blob = 2.01824
;rho_c = xi1^3/blob*Msun/4./!pi/Rsun^3
rho_c = xi1/3./0.04243*3*Msun/4./!pi/Rsun^3
print, 'Central density: '+trim(rho_c)+' g cm-3'
Ksun = G*Msun^(2./3)*0.36394
print, 'Ksun = '+trim(Ksun)
musun = 4. / (3 + 5*X - Z)
Tc = Ksun*musun*m_u/k*rho_c^(1./3)
print, 'Central temperature: '+trim(Tc)+' K'
print

print, 'Halfway out, density = '+trim(rho_c*0.022)+' g cm-3'
print, 'Halfway out, temperature = '+trim(Tc*0.28)+' K'

END

