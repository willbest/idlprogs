;ps=1
;PRO HW5_623

; Saha equation:
; n_II/n_I = 1/n_e * 2.*gII/gI * (2*!pi*m_e*k*T/h^2)^1.5 * exp(-XI/k/T)
;
; INPUTS for saha.pro
;     T - vector of temperatures
;     P - 3-element vector of parameters, as below
;         P[0] - n_e, electron denisty
;         P[1] - g_I, statistical weight for neutral species
;         P[2] - g_II, statistical weight for inoized species
;         P[3] - XI, ionization 

; cgs units
a = 7.566e-15                   ; erg cm-3 K-4
k = 1.38065e-16                 ; erg K-1
T = 1.5e7                       ; K
rho = 150.                      ; g cm-3
m_u = 1.66054e-24               ; g
XI = 1.60218e-12 * 13.6057      ; erg
g_I = 2.                        ; statistical weight for neutral species
g_II = 1.                       ; statistical weight for inoized species
X = 0.35                        ; Hydrogen fraction
Z = 0.02                        ; metal fraction

mu_e = 2./(1+X)
n_e = rho/mu_e/m_u

; Make the P array 
P = [n_e, g_I, g_II, XI]

; Calculate the ionization fraction using the Saha equation
ion = saha(T, P)
print, 'n_II/n_I = '+trim(ion)

; Calculate the density of neutral H atoms
n_H = rho*X/m_u
n_I = n_H / (ion + 1.)
print, 'n_I = '+trim(n_I)+' cm^-3'

; Calculate the radius of the volume occupied by each neutral H
r_I = (3./4/!pi/n_I)^(1./3)
print, 'r_I = '+trim(r_I)+' cm'
print, 'r_I = '+trim(r_I*1e8)+' Angstroms'

END

