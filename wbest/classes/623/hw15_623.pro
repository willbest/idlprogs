ps=1
;PRO HW15_623, PS=ps

; Written by Will Best, 11/08/2012

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
Vsun = 4./3*!pi*Rsun^3          ; cm3
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
Rearth = 6.3781e8               ; cm
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
T_c = 1.5e7                       ; K
rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
ergeV = 1.60218e-12             ; erg eV-1
XI = ergeV * 13.6057            ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #28
T = T_c                ; K
Z1 = 7.
Z2 = 1.
A1 = 14.
A2 = 1.
A = A1*A2/(A1+A2)
etax = 42.47*(Z1^2*Z2^2*A)^(1./3)
eta = etax * (T/1e6)^(-1./3)
n_temp = (eta-2.)/3
print
print, 'eta = '+trim(eta)
print, 'n = '+trim(n_temp)
print


; Problem #29
T = 1e8                ; K
rho = 1e5              ; g cm-3
Y = 1
malpha = (4 - 2.42475/931.478)*m_u
Nalpha = rho / (4*m_u)
enum = 92e3*1.6e-12/kB/1e8
ratio = Nalpha*h / (2*!pi*kB*T)^(1.5) *h / malpha^(1.5) *h * exp(-enum/(T/1e8))
eps = 3.9e11 * rho^2 * Y^3 * (T/1e8)^(-3) * exp(-43./(T/1e8))
print
print, 'Numerator for exponent: '+trim(enum)
print, 'Ratio N(8Be)/N(4HE): '+trim(ratio)
print, 'Energy generation rate: '+trim(eps)+' erg g-1 s-1'


; Problem #30
device, decomposed=0
lincolr_wb, /silent
xichar='!7n!X'
thetachar='!7h!X'
colors = [2, 0, 3, 4, 5, 6, 7, 8]

if keyword_set(ps) then begin
    fpath = '~/Astro/classes/623/homework/laneemden'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 2, retain=2, xsize=800, ysize=600
endelse

n = [2., 3., 4., 5.]                 ; polytropic index
runs = n_elements(n)
cset = colors[0:runs-1]
h = 0.001              ; step size
niter = 10000          ; number of steps
xi = h*findgen(niter)

for j=0, runs-1 do begin
    theta = fltarr(niter+1)
    z = theta

    theta[0] = 1.
    theta[1] = 1 - h^2/6.       ; first step
    z[1] = -h^3/3.              ; first step

    for i=1, niter-1 do begin
        dydx = laneemden(xi[i], [theta[i], z[i], n[j]])
        y = rk4([theta[i], z[i], n[j]], dydx, xi[i], h, 'laneemden')
        theta[i+1] = y[0]
        z[i+1] = y[1]
    endfor
    theta = theta[0:niter-1]

; Plot theta vs. xi
    if j eq 0 then plot, xi, theta, /nodata, title=thetachar+' for a polytrope of index n', $
                         background=1, color=0, charsize=1.5, xtitle=xichar, $
                         ytitle=thetachar, yrange=[0,1]
    oplot, xi, theta, color=cset[j]

endfor

; Write the legend
leg = 'n = '+trim(n)
lset = 0
legend, leg, chars=1.5, lines=lset, outline=0, color=cset, textcolor=cset, /right

print
if keyword_set(ps) then ps_close

END

