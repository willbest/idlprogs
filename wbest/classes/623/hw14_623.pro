;ps=1
;PRO HW14_623

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
Vsun = 4./3*!pi*Rsun^3          ; cm3
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
Rearth = 6.3781e8               ; cm
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
T_c = 1.5e7                       ; K
rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
ergeV = 1.60218e-12             ; erg eV-1
XI = ergeV * 13.6057            ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #26
delm_p = 7.289
delm_4He = 2.425
Eneu = 0.263
E0 = 4*delm_p - delm_4He
Ealpha = E0 - 2*Eneu
print
print, 'Energy generated in producing an alpha particle: '+trim(E0)+' MeV'
print, 'Subtracting the neutrino energy: '+trim(Ealpha)+' MeV'

rateneu = 2*Lsun/Ealpha/1e6/ergeV
print
print, 'Rate of neutrino production: '+trim(rateneu)+' neutrinos s-1'

sig = 1e-46 ; too small for IDL
rho = Msun/Vsun
musun = 2.
nSun = rho/musun/m_u
Pneu = 1e-23*nSun*Rsun*100.*1e-23
print
print, 'Density of the Sun: '+trim(rho)+' g cm-3'
print, 'mu_Sun: '+trim(musun)
print, 'Nucleon density of the Sun: '+trim(nSun)+ ' cm-3'
print, 'Percentage of neutrinos that are absorbed: '+trim(Pneu)

Fneu = rateneu*(Rearth^2/dsun^2)
print
print, 'Flux of solar neutrinos through the Earth: '+trim(Fneu)+' neutrinos s-1'

csec = 300     ; cm2 -- my vertical cross-section area
hme = 183      ; cm
rhome = 1.01   ; g cm-3
mume = 14; 16*.65 + 12*.18 + 1*.1 + 14*.03 + 40*.014 + 30*.011
Fneume = rateneu*(csec/dsun^2)*tyear    ; neutrinos yr-1
nme = rhome/mume/m_u
Preact = 1e-23*nme*hme*1e-23
print
print, 'mu_me: '+trim(mume)
print, 'Probability of neutrino reaction in my body: '+trim(Preact)
print, 'Flux of solar neutrinos through my body: '+trim(Fneume)+' neutrinos per year'
print, 'Number of solar neutrino reactions in my body per year: '+trim(Fneume*Preact)
print


; Problem #27
So = 2.5e-4 * 1e-24    ; keV cm2
T = T_c                ; K
Z1 = 1.
Z2 = 1.
A1 = 1.
A2 = 2.
X1 = 0.35
A = A1*A2/(A1+A2)
b = 31.29*Z1*Z2*A^.5   ; keV(1/2)
etax = 42.47*(Z1^2*Z2^2*A)^(1./3)
eta = etax * (T/1e6)^(-1./3)
;sigv = 4e-5 * 8*sqrt(2)/(9*sqrt(3)) * So/(b*sqrt(A*m_u)) * eta^2 * exp(-eta)
coeff = 4e-5 * 8*sqrt(2)/(9*sqrt(3)) * So/(b*sqrt(A*m_u)) * etax^2 / (A1*A2)/m_u/m_u
print
print, 'b = '+trim(b)+' keV^(1/2)'
print, 'eta_x = '+trim(etax)
;print, '<sigma*v> = '+trim(sigv)
print, 'coefficient for reaction rate = '+trim(coeff)

taupd = (T/1e6)^(2./3)*exp(eta) / (A2*m_u*coeff*X1*rho_c)
print
print, 'reaction timescale = '+trim(taupd)+' sec'

DHrat = taupd / (2*2.8e17)
print
print, 'D/H equilibrium ratio = '+trim(DHrat)

print
END

