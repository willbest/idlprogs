;ps=1
;PRO HW20_623

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
Vsun = 4./3*!pi*Rsun^3          ; cm3
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
Rearth = 6.3781e8               ; cm
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
T_c = 1.5e7                       ; K
rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
ergeV = 1.60218e-12             ; erg eV-1
XI = ergeV * 13.6057            ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #36
coeff4 = 20.*!pi*G*Msun*a*c/3./Lsun*1e24
coeff6a = 4e7*m_u/kB*(coeff4/1e14)
coeff6b = a*1e14/3./(coeff4/1e14)
coeff7 = G*Msun/Rsun
coeff8 = G/coeff4/4.*0.792/Rsun*Msun
print
print, 'Coefficient for eq.4: '+trim(coeff4)
print, 'Coefficient outside eq.6: '+trim(coeff6a)
print, 'Coefficient inside eq.6: '+trim(coeff6b)
print, 'Coefficient for eq.7: '+trim(coeff7)
print, 'Coefficient for eq.8: '+trim(coeff8)


print
END

