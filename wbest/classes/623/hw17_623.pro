;ps=1
;PRO HW15_623, PS=ps

; Written by Will Best, 11/08/2012

; cgs units
c = 2.998e10                    ; cm s-2
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
Vsun = 4./3*!pi*Rsun^3          ; cm3
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
Rearth = 6.3781e8               ; cm
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
h = 6.63e-27
kB = 1.38065e-16                 ; erg K-1
T_c = 1.5e7                       ; K
rho_c = 150.                      ; g cm-3
m_e = 9.10938e-28               ; g
m_n = 1.67493e-24               ; g
m_u = 1.66054e-24               ; g
ergeV = 1.60218e-12             ; erg eV-1
XI = ergeV * 13.6057            ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
X = 0.7                         ; Hydrogen fraction
Y = 0.28                        ; Helium fraction
Z = 0.02                        ; metal fraction


; Problem #32
edis = 4.5*ergeV                ; erg
eion = 13.6*ergeV               ; erg
Rf = 2*G*Msun*m_u/(edis + 2*eion)
T = G*Msun*m_u/6/kB/Rf
print
print, 'Radius of protostar: '+trim(Rf)+' cm'
print, 'Radius of protostar: '+trim(Rf/Rsun)+' Rsun'
;print, 'Radius of protostar: '+trim(Rf/Rearth)+' Rearth'
print
print, 'Temperature: '+trim(T)+' K'
print

END

