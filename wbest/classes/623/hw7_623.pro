;ps=1
;PRO HW&_623

; cgs units
G = 6.672e-8                    ; cm3 g-1 s-2
Msun = 2e33                     ; g
Rsun = 7e10                     ; cm
Lsun = 3.85e33                  ; erg s-1
dsun = 1.5e13                   ; cm
gsun = 2.72e4                   ; cm s-2
gearth = 981                    ; cm s-2
tyear = 3.16e07                 ; s
;a = 7.566e-15                   ; erg cm-3 K-4
k = 1.38065e-16                 ; erg K-1
;T = 1.5e7                       ; K
;rho = 150.                      ; g cm-3
m_u = 1.66054e-24               ; g
;XI = 1.60218e-12 * 13.6057      ; erg
;g_I = 2.                        ; statistical weight for neutral species
;g_II = 1.                       ; statistical weight for inoized species
;X = 0.35                        ; Hydrogen fraction
;Z = 0.02                        ; metal fraction


; Problem #14
dRdt = -2.*Rsun^2/Msun*Lsun/G/Msun
dRyear = dRdt*tyear
print, 'dR/dt = '+trim(dRdt)+'cm s^-1.'
print, 'Annual change is '+trim(dRyear)+' cm.'

dthetayear = 2./dsun*dRyear * 206265.
print, 'Annual change is '+trim(dthetayear)+' arcsec.'

print, 'It would take '+trim(-.1/dthetayear)+' years to notice the change.'


;Problem #15
print
Tsun = 6000                      ; K
Tearth = 300                     ; K
; Solar atmosphere scale height
musun = 1*.91 + 4*.09
Hsun = k*Tsun/gsun/musun/m_u
print, "The scale height of the Sun's atmosphere is "+trim(Hsun)+" cm."

; Eath's atmosphere scale height
muearth = 28    ; Assume all molecular nitrogen
Hearth = k*Tearth/gearth/muearth/m_u

; Air density
rhorat = exp(-9.08/8.85)
rhoo = 1.2                       ; g cm-3
print, "The scale height of the Earth's atmosphere is "+trim(Hearth)+" cm."
print
print, "The ratio of the density of air at the top of Mount Everest"
print, "   to sea level is "+trim(rhorat)+"."
print, "The air density at sea level is "+trim(rhoo)+" g cm^-3,"
print, "   so the air density at the top of Mount Everest is "+trim(rhoo*rhorat)+" g cm^-3."

END

