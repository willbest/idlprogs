FUNCTION CHISQ_PROB_LN, X, P

; Returns the chi squared probability for a model given data and uncertainty.
; Computed IN NATURAL LOG SPACE.
;
; HISTORY
; Written by Will Best (IfA), 2013-11-13
;
; INPUTS
;     X - Variable for which chi squared probability is needed.
;     P[0] = center = mu
;     P[1] = standard deviation = sigma

Y = -(X - P[0])^2 / (2.*P[1]^2)

return, Y

END
