FUNCTION GAUSS3W, X, P

; Fit a linear combination of three Gaussian functions to input data.
;
; HISTORY
; Written by Will Best (IfA), 03/29/2012
;
; INPUTS
;     X - vector of wavelengths
;     P - 10-element vector of parameters, described below

; P[0] = offset from zero background

; P[1] = center = mu1
; P[2] = amplitude = a1
; P[3] = standard deviation = sigma1

; P[4] = center = mu2
; P[5] = amplitude = a2
; P[6] = standard deviation = sigma2

; P[7] = center = mu3
; P[8] = amplitude = a3
; P[9] = standard deviation = sigma3

Y1 = (P[2] - P[0]) * exp( -(X - P[1])^2 / (2*P[3]^2) )
Y2 = (P[5] - P[0]) * exp( -(X - P[4])^2 / (2*P[6]^2) )
Y3 = (P[8] - P[0]) * exp( -(X - P[7])^2 / (2*P[9]^2) )

return, Y1 + Y2 + Y3 + P[0]

END
