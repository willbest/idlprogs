FUNCTION GAUSSW, X, P

; P[0] = center = mu
; P[1] = amplitude
; P[2] = standard deviation = sigma

Y = P[1] * exp( -(X - P[0])^2 / (2*P[2]^2) )

return, Y

END
