FUNCTION LOGISTIC, X, P

; Fit a logistic function to input data.
;
; HISTORY
; Written by Will Best (IfA), 07/28/2012
;
; INPUTS
;     X - vector of inputs
;     P - 3-element vector of parameters, as below

Y = 1. / (p[0] * p[1]^(X) + p[2])

return, Y

END
