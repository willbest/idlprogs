FUNCTION DISK_GAS_DENSITY, R, THETA, HSCALE, GAMMA=gamma, MGAS=mgas, $
                           MINRHO=minrho, RC=Rc, TEMP=temp, _EXTRA=extrakaey

;+
; Returns an array for density of gas in a protoplanetary disk, as a
; function of distance r from the star and angle theta above the midplane.
;
; The model uses the surface density from Andrews et al. (2009), eq. 4 and 5, and
; Williams & Andrews (2011), eq. 4.
; Vertical density structure is from Hughes et al (2008, eq. 4), Williams &
; Andrews (2011, eq. 5), and Andrews et al (2012, eq. 1).
;
; IF HSCALE is not supplied, calls disk_gas_scaleheight.pro to determine the gas
; scale height.
;
; HISTORY
; Written by Will Best (IfA), 03/14/2013
;
; INPUTS
;     R - Scalar/vector of distance(s) from the central star
;     THETA - Scalar/vector of angles(s) above the midplane of the disk (0
;             is at the pole).
;
; OPTIONAL INPUTS
;     HSCALE - Scalar/vector of scale height(s) for the gas.
;              If HSCALE is omitted, the TEMP keyword must be set, so that the 
;
; KEYWORDS
;     GAMMA - Parameter in tapered disk model
;           Default: 1
;     MGAS - Gas mass of disk
;           Default: 1
;     MINRHO - Minimum allowed density (to avoid division by zero?)
;           Default: 1e-23
;     RC - Characteristic radius of disk from the tapered model
;           Default: 100 AU
;     TEMP - Scalar/vector of temperature(s) at the midplane.
;            If HSCALE is passed, TEMP is ignored.
;-


; Check inputs
if (n_params()) lt 2 then begin
    print
    print, 'Use:  result = disk_gas_density(R, THETA [, HSCALE, GAMMA=gamma, MGAS=mgas, $'
    print, '                     MINRHO=minrho, RC=Rc, TEMP=temp, EXTRA=extrakaey] )'
    print
    return, 0
endif
if n_elements(Hscale) eq 0 then if not keyword_set(temp) then begin
    print
    print, 'You must supply HSCALE or TEMP to disk_gas_density().'
    print
    return, 0
endif
if not keyword_set(gamma) then gamma = 1.
if not keyword_set(mgas) then mgas = 1.
if not keyword_set(Rc) then Rc = 100d
if not keyword_set(minrho) then minrho = 1d-23

; Calculate the surface density of the disk
g2 = 2.d0-gamma
;const = g2 * mgas * exp((rin/rc)^g2) / (2*!dpi*disk.rc^2)
const = g2 * mgas / (2*!dpi*Rc^2)
sigma = const * (Rc/r)^gamma / exp((r/Rc)^g2)

; If needed, calculate the scale height
if n_elements(Hscale) eq 0 then $
  Hscale = disk_gas_scaleheight(r, temp, _extra=extrakey)

; Calculate the density structure
nr = n_elements(r)
nt = n_elements(theta)
rho = dblarr(nr,nt)
for ir=0, nr-1 do begin
;  if rcen[ir] gt rin and rcen[ir] le rout then begin
    sigR = sigma[ir] / (Hscale[ir] * sqrt(2d0*!dpi))
    expZ = exp(-0.5*(tan(!dpi/2. - theta)*r[ir]/Hscale[ir])^2)
    rho[ir,*] = (sigR * expZ) > minrho
;  endif
endfor

return, rho

END
