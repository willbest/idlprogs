FUNCTION SAHA, T, P

; Finds the ionization ratio for stellar gas with temperature T.
;
; Xe = 1/n_e * 2.*gII/gI * (2*!pi*m_e*k*T/h^2)^1.5 * exp(-XI/k/T)
;
; HISTORY
; Written by Will Best (IfA), 09/08/2012
;
; INPUTS
;     T - vector of temperatures
;     P - 3-element vector of parameters, as below
;         P[0] - n_e, electron denisty
;         P[1] - g_I, statistical weight for neutral species
;         P[2] - g_II, statistical weight for inoized species
;         P[3] - XI, ionization 
;

; cgs units
h = 6.62607e-27                    ; erg s
k = 1.38065e-16                    ; erg K-1
m_e = 9.10938e-28                  ; g

ion = 1/P[0] * 2.*P[2]/P[1] * (2*!pi*m_e/h*k*T/h)^1.5 * exp(-P[3]/k/T)

return, ion

END
