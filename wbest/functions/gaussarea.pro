FUNCTION GAUSSAREA, OFFSET, AMP, DEV, COVAR, ERR=err

; Calculate the area under a Gaussian function.
;
; HISTORY
; Written by Will Best (IfA), 04/10/2012
;
; INPUTS
;     OFFSET - background level (vertical shift of Gaussian curve)
;     AMP - amplitude of Gaussian
;     DEV - sigma = standard deviation of Gaussian
;     COVAR - covariance matrix from Gaussian fit.  Required for error calculation.
;
; KEYWORD OUTPUTS
;     ERR - error in area under the Gaussian curve

; P[0] = offset from zero background

; P[1] = center = mu1
; P[2] = amplitude = a1
; P[3] = standard deviation = sigma1

area = (amp-offset) * dev * sqrt(2*!pi)
err = area * sqrt( (covar[2,2]+covar[0,0])/(amp-offset)^2 + covar[3,3]/dev^2) * sqrt(2*!pi)

return, area

END
