FUNCTION LOGERROR, X, S

; Propogate an uncertainty through a base 10 logarithmic function.
;
; Returns the uncertainty for the logarithmic function.
;
; HISTORY
; Written by Will Best (IfA), 2013-10-08
;
; INPUTS
;     X - Measured quantity
;     S - Measurement uncertainty

S1 = 0.434294 * S / X

return, S1

END
