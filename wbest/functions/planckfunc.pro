PRO PLANCKFUNC, X, A, F, pder

c = 2.998e8
h = 6.626e-34
k = 1.381e-23

denom = exp(h*c/(x*k*a[1])) - 1 
F = a[0]*2*h*c^2/x^5 * 1./denom  

END  

