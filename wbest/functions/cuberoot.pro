FUNCTION CUBEROOT, X, P
;PRO CUBEROOT, X, P, Y

; Fit a cube root function to input data.
;
; HISTORY
; Written by Will Best (IfA), 07/28/2012
;
; INPUTS
;     X - vector of inputs
;     P - 3-element vector of parameters, as below

Y = fltarr(n_elements(X))
a = X - p[1]
neg = where(a lt 0, complement=pos)

Y[pos] = p[0] * a[pos]^(1./3) + p[2]
if neg[0] ne -1 then Y[neg] = -p[0] * (-a[neg])^(1./3) + p[2]

return, Y

END
