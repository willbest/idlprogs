FUNCTION LANEEMDEN, X, Y

; The Lane-Emden equation, expressed as a system of two first-order
; differerntial equations.
;
; d(theta)/d(xi) = z / (xi)^2     dz/d(xi) = -(theta)^n * (xi)^2
;
; HISTORY
; Written by Will Best (IfA), 11/08/2012
;
; INPUTS
;     X - xi (the free parameter)
;     Y - 2-element vector containing the dependent variables
;         Y[0] - theta
;         Y[1] - z
;         Y[2] - n (polytropic index)
;

return, [Y[1]/X^2, -Y[0]^Y[2]*X^2, 0]

END
