PRO GAUSS3WP, X, P, Y

; P[0] = center = mu1
; P[1] = amplitude = a1
; P[2] = standard deviation = sigma1

; P[3] = center = mu2
; P[4] = amplitude = a2
; P[5] = standard deviation = sigma2

; P[6] = center = mu3
; P[7] = amplitude = a3
; P[8] = standard deviation = sigma3

Y1 = P[1] * exp( -(X - P[0])^2 / (2*P[2]^2) )
Y2 = P[4] * exp( -(X - P[3])^2 / (2*P[5]^2) )
Y3 = P[7] * exp( -(X - P[6])^2 / (2*P[8]^2) )

Y = Y1 + Y2 + Y3

END
