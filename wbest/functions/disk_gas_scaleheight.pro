FUNCTION DISK_GAS_SCALEHEIGHT, R, T, MSTAR=mstar

;+
; Returns a vector for the scale height of gas in a protoplanetary disk, as a
; function of distance r from the star and temperature T of the gas.
;
; The equation for scale height is from from Hughes et al. (2008), eq. 3.
;
; HISTORY
; Written by Will Best (IfA), 03/14/2013
;
; INPUTS
;     R - Scalar/vector of distance(s) from the central star
;     T - Scalar/vector of temperature(s) at the midplane.
;         T[i] corresponds to R[i].
;
; KEYWORDS
;     MSTAR - Mass of central star
;           Default: 1 Msun
;-

; Check inputs
if(n_params()) lt 2 then begin
    print
    print, 'Use:  result = disk_gas_scaleheight(r, T [, mstar=mstar] )'
    print
    return, 0
endif
if not keyword_set(Mstar) then Mstar = 1.98892d33

; Load the standard astronomical constants
@natconst

; Scale height
H = sqrt( (r^3 * kk * T) / (GG * Mstar * muh2*mp) ) * AU^.5

return, H

END
