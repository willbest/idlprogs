;graphs=1
;minspt=6
;maxspt=28
PRO SPTFIT, GRAPHS=graphs, LIST=list, MINSPT=minspt, MAXSPT=maxspt, PS=ps

;  Using Kimberley Aller's table of known cool dwarfs, plots
;  absolute magnitude vs. spectral type graphs.  Fits a 5th degree
;  polynomial to the absolute magnitudes, graphs the polynomial, and
;  prints out the polynomial coefficients.
;  Saves the polynomials and the fit values for each SpT to .sav files.
;  Currently plots only M, L and T dwarfs.
;
;  HISTORY
;  Written by Will Best (IfA), 12/08/2011
;  02/23/2012: Added minspt and maxspt keywords.
;  02/25/2012: Changed to make fits for all filters at once.
;  02/25/2012: Added /list keyword
;  08/02/2012: Report 1 sigma spread of abs mags for each SpT value.
;              Clip >5sigma data points.
;  05/28/2013: Added PS keyword and postcript capability.
;
;  KEYWORDS
;      GRAPHS - Display fitting graphs one at a time; any key displays the next graph.
;      LIST - If set, the program prints a list of the filters currently
;             available for fitting, then returns.
;      MINSPT - Minimum spectral type to fit.
;          Scale: 0 = M0, 10 = L0, 20 = T0, 29 = T9
;                 (no data exists for types later than T9.)
;          Default: 10 (L0)
;      MAXSPT - Maximum spectral type to fit (same scale as MINSPT).
;          Default: 28 (T8).
;
;  FUTURE IMPROVEMENTS
;      Deal with the magnitude errors
;      Optional input for file name to be saved to.
;      Filters to include:
;          SDSS
;          CFHT (where would the photometry come from?)
;          Fits from literature, including Kirkpatrick and Looper.
;          Anything else from Kimberly's mega-table.
;      Actually enable fitting of M types.
;      Default is to only fit dwarfs.
;          Add keyword to include subdwarfs.
;          Add keyword to only fit subdwarfs.
;      Add a /silent keyword.
;

; Filter names
names = ['PS1 z', 'PS1 y', $
         '2MASS J', '2MASS H', '2MASS K', $
         'UKIDSS Y', 'UKIDSS J', 'UKIDSS H', 'UKIDSS K', $
         'WISE W1', 'WISE W2', 'WISE W3' ];, 'WISE W4']
shorts = ['z', 'y', $
         'J2', 'H2', 'K2', $
         'YM', 'JM', 'HM', 'KM', $
         'W1', 'W2', 'W3' ];, 'W4']

; If requested, print the filter names and quit
if keyword_set(list) then begin
    print, 'Currently fitting absolute magnitudes for the following filters:'
    print, names, format='(a)'
    return
endif

; Load Kimberly's megatable into the structure "wise_new", with these tags: 
;; NAME JMAG E_JMAG HMAG E_HMAG KMAG E_KMAG PMRA E_PMRA PMDE E_PMDE OSPT NIRSPT SPT
;; SPECTRALTYPE DIS E_DIS F_DIS VTAN E_VTAN NOTE EPOCH RA2010 DEC2010 MM MJ Y_2 J H
;; K YERR_2 JERR HERR JHERR KERR RA DEC G GERR GNPHOT GABS R RERR RNPHOT RABS I
;; IERR INPHOT IABS Z ZERR ZNPHOT ZABS Y YERR YNPHOT YABS GROUPID GROUPSIZE
;; SEPARATION GR GRERR GY GYERR RI RIERR RY RYERR IZ IZERR IY IYERR ZY ZYERR
;; YJ_2MASS YJ_2MASSERR ZJ_2MASS ZJ_2MASSERR JH_2MASS JH_2MASSERR HK_2MASS
;; HK_2MASSERR YJ_MKO YJ_MKOERR ZJ_MKO ZJ_MKOERR JH_MKO JH_MKOERR HK_MKO HK_MKOERR
;; Y_MKOJ_MKO Y_MKOJ_MKOERR YY YYERR NAME_DA DISC_PAPER NAME_2MASS RA_HOURS DEC_DEG
;; J_DA J_DA_ERR H_DA H_DA_ERR K_DA K_DA_ERR PLX PLX_ERR PLX_PAPER PM_TOT
;; PM_TOT_ERR PM_PA PM_PA_ERR PM_PAP SPT_O SPT_O_PAPER SPT_IR SPT_IR_PAPER NOTE_DA
;; RA_WISE DEC_WISE DIST_WISE W1 W1ERR W1ABS W1SNR W2 W2ERR W2ABS W2SNR W3 W3ERR
;; W3ABS W3SNR W4 W4ERR W4ABS W4SNR CC_FLAGS PH_QUAL EXT_FLAG DEBLEND_FLAG
;; NOTE_WISE NOTE_BINARY
; More info in Dropbox > panstarrs-BD > Known Objects > mlt_dwarfs > README

restore, '~/Astro/PS1BD/wise_da_legg_complete_duplicate_3pi.sav'

; Bins for determining magnitude errors
chunk = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 24, 26]

;; ; Create a structure for object magnitudes & errors, SpT, J, and distance
;; phot = replicate({z:0., y:0., jmag:0., hmag:0., kmag:0., y_2:0., j:0., h:0., k:0., $
;;                   w1:0D, w2:0D, w3:0D, w4:0D, }, n_elements(wise_new))

; Create arrays for object magnitudes and errors, in each filter
phot = [ [wise_new.z], [wise_new.y], $
         [wise_new.jmag], [wise_new.hmag], [wise_new.kmag], $
         [wise_new.y_2], [wise_new.j], [wise_new.h], [wise_new.k], $
         [wise_new.w1], [wise_new.w2], [wise_new.w3] ];, [wise_new.w4] ]
n = n_elements(names)
npho = size(phot)
if npho[2] ne n then message, 'Make sure the names vector matches the phot vector!!'
photerr = [ [wise_new.zerr], [wise_new.yerr], $
            [wise_new.e_jmag], [wise_new.e_hmag], [wise_new.e_kmag], $
            [wise_new.yerr_2], [wise_new.jerr], [wise_new.herr], [wise_new.kerr], $
            [wise_new.w1err], [wise_new.w2err], [wise_new.w3err] ];, [wise_new.w4err] ]

; Create arrays for object SpT, J, and distance
spt = wise_new.spt        ; Spectral Type array
mj = wise_new.mj          ; Absolute J-magnitude
dis = wise_new.dis        ; Distance (either photometric or parallax, in parsecs)
diserr = wise_new.e_dis   ; Distance error

; Create structure for filter names and polynomial coefficients
filter = replicate({name:'', poly:fltarr(6)}, n)
filter.name = names

; Display the earliest and latest spectral types plotted
sptname = ['M' + strtrim(indgen(10), 1), 'L' + strtrim(indgen(10), 1), 'T' + strtrim(indgen(10), 1)]

if n_elements(minspt) eq 0 then minspt = 10
if ((size(minspt, /type) ne 2) and (size(minspt, /type) ne 4)) or (minspt lt 0) then minspt = 10
if (minspt gt 28) then minspt = 28
if n_elements(maxspt) eq 0 then maxspt = 28
if ((size(maxspt, /type) ne 2) and (size(maxspt, /type) ne 4)) or (maxspt gt 29) then maxspt = 28
if (maxspt lt minspt) then maxspt = minspt

if (maxspt eq minspt) then begin
    if minspt-fix(minspt) eq .5 then begin
        print, 'Only spectral type fit will be '+sptname[fix(minspt)]+'.5'
    endif else print, 'Only spectral type fit will be '+sptname[minspt]
endif else begin
    if minspt-fix(minspt) eq .5 then begin
        print, 'Earliest spectral type fit will be '+sptname[fix(minspt)]+'.5'
    endif else print, 'Earliest spectral type fit will be '+sptname[minspt]
    if maxspt-fix(maxspt) eq .5 then begin
        print, 'Latest spectral type fit will be '+sptname[fix(maxspt)]+'.5'
    endif else print, 'Latest spectral type fit will be '+sptname[maxspt]
endelse

;How many SpT values will be fit by this programs?
nfits = (maxspt-minspt)*2+1
sptvec = findgen(nfits)/2. + minspt         ; vector of spectral type values

; Create structure for fit absolute magnitude values and errors
fits = replicate({name:'', short:'', spt:sptvec, M:fltarr(nfits), Merr:fltarr(nfits)}, n)
fits.name = names
fits.short = shorts

print, strtrim(n, 1), format='("Fitting polynomials for ", a, " filters.")'

;case filter of
;    'z' : band = wise_new.z      ; PS1 z-magnitudes
;    'J' : band = wise_new.jmag   ; 2MASS J-magnitudes
;    'H' : band = wise_new.hmag   ; 2MASS H-magnitudes
;    'K' : band = wise_new.kmag   ; 2MASS K-magnitudes
;    'U_Y' : band = wise_new.y_2  ; UKIDSS Y-magnitudes
;    'U_J' : band = wise_new.j    ; UKIDSS J-magnitudes
;    'U_H' : band = wise_new.h    ; UKIDSS H-magnitudes
;    'U_K' : band = wise_new.k    ; UKIDSS K-magnitudes
;    'W2' : band = wise_new.w2    ; WISE W2-magnitudes
;    else : band = wise_new.y     ; PS1 y-magnitudes
;endcase

; Re-format the notes field to remove "        " type notes
quotes = where(strpos(wise_new.note, '"') eq 0)
wise_new[quotes].note = strmid(wise_new[quotes].note, 1, 1#strlen(wise_new[quotes].note)-2)
wise_new.note = trim(wise_new.note)
; remove binaries
nobin = where(strlen(wise_new.note) eq 0)    ; these objects are not binaries or unusual
phot = phot[nobin,*]
photerr = photerr[nobin,*]
spt = spt[nobin]
mj = mj[nobin]
dis = dis[nobin]
diserr = diserr[nobin]

;; ; select only objects with parallaxes, which means they have an absolute J-mag
;; plx = where(mj gt 0)
;; spt = spt[plx]
;; band = band[plx]
;; dis = dis[plx]

; select only objects which have distances
hasdis = where(dis gt 0)
phot = phot[hasdis,*]
photerr = photerr[hasdis,*]
spt = spt[hasdis]
dis = dis[hasdis]
diserr = diserr[hasdis]

; remove negative spectral types (subdwarfs) and M dwarfs
noneg = where((spt ge minspt) and (spt le maxspt))       ; only keep stp >= 10, i.e. L and T
phot = phot[noneg,*]
photerr = photerr[noneg,*]
spt = spt[noneg]
dis = dis[noneg]
diserr = diserr[noneg]

spt1 = spt
dis1 = dis
diserr1 = diserr

; Set up plotting window
if keyword_set(graphs) then begin
    lincolr, /silent
    device, decomposed=0
    sptaxis = ['L0','L5','T0','T5','Y0']
    xr = [minspt-2, maxspt+2]
    sptval = indgen(maxspt-minspt+5)+(minspt-2)
endif

for i=0, n-1 do begin
    band = phot[*,i]
    banderr = photerr[*,i]

; remove non-detections
    detect = where((band gt 0) and (banderr ge 0))
    spt = spt1[detect]
    band = band[detect]
    banderr = banderr[detect]
    dis = dis1[detect]
    diserr = diserr1[detect]

; Preliminary fit
    M = band - 5.*alog10(dis/10.)                       ; calculate the absolute magnitude
;    Merr = sqrt(banderr^2 + (5.*0.4343*diserr/dis)^2)   ; abs mag error -- NOT USING CURRENTLY
;    c = poly_fit(spt, M, 5, mea=Merr)                   ; get the coefficients of a 5th order polynomial
    c = poly_fit(spt, M, 5, yerror=cerror)               ; get the coefficients of a 5th order polynomial
    Mfit = poly(sptvec, c)                              ; plug the SpT vector into the polynomial

;NEW STUFF
; Get magnitude errors from residuals, binned by SpT intervals
    nchunk = n_elements(chunk)
    Merr = fltarr(nchunk)                               ; vector for magnitude errors
    errind = lookup(spt, chunk)
    sigout = -1                                         ; reset the outlier tracker to null
;;     for j=0, nchunk-1 do begin
;;         spt2 = where(errind eq j)
;;         if spt2[0] eq -1 then continue
;;         resid = M[spt2] - poly(spt[spt2], c)
;;         Merr[j] = stddev(resid)
;; 
;; ; Check for 5 sigma outliers
;;         sigout2 = where((abs(resid) - 5*Merr[j]) ge 0)
;;         if sigout2[0] gt -1 then sigout = [sigout, sigout2]
;;     endfor
;; 
;; ; If 5sigma outliers exist, remove them and re-do the polynomial fit
;; ;    while n_elements(sigout) gt 1 do begin
;;     if n_elements(sigout) gt 1 then begin
;;         sigout = sigout[1:*]
;;         nooutl = indgen(n_elements(M))
;;         remove, sigout, nooutl
;;         spt = spt[nooutl]
;;         M = M[nooutl]
;;         ; Re-do the poynomial fit
;;         c = poly_fit(spt, M, 5)           ; get the coefficients of a 5th order polynomial
;;         Mfit = poly(sptvec, c)            ; plug the SpT vector into the polynomial
;;         ; Re-calculate the magnitude errors
;;         errind = lookup(spt, chunk)
;;         sigout = -1                       ; reset the outlier tracker to null
;;         for j=0, nchunk-1 do begin
;;             spt2 = where(errind eq j)
;;             if spt2[0] eq -1 then continue
;;             resid = M[spt2] - poly(spt[spt2], c)
;;             Merr[j] = stddev(resid)
;;         ; Check for 5 sigma outliers
;;             sigout2 = where((abs(resid) - 5*Merr[j]) ge 0)
;;             if sigout2[0] gt -1 then sigout = [sigout, sigout2]
;;         endfor
;;     endif
;; ;    endwhile 

    dummy = lookup(sptvec, chunk, Merr, Merrvec)     ; store the fit absolute magnitude errors
    fits[i].Merr = Merrvec
    filter[i].poly = c                               ; store the polynomial coefficients in their structure
    fits[i].M = Mfit                                 ; store the fit absolute magnitude values

; Print the polynomial coefficients
    print, names[i], c, format='(a, T12, "[",5(f11.7,","),f11.7,"]")'

; Plot the absolute magnitudes and the polynomial fit
    if keyword_set(graphs) then begin
        if keyword_set(ps) then begin
            fpath = '~/Astro/distlim.spt'
            ps_open, fpath, /color, /en, thick=4
        endif else window, retain=2, xsize=800, ysize=600

        plot, spt, M, color=0, background=1, psym=4, title=names[i], yrange=[16,8], /ystyle, $
              xrange=[minspt-2,maxspt+2], xstyle=1, xtickname=sptaxis, xtickv=sptval, $
              xtitle='Spectral Type', ytitle='Absolute Magnitude', charsize=2
        oplot, sptvec, Mfit, color=0, thick=8

        if keyword_set(ps) then ps_close

        wait = get_kbrd()
    endif

endfor

save, filter, file='~/idlprogs/wbest/survdepth/Mpoly.sav'
save, fits, file='~/idlprogs/wbest/ltbayes/Mfits.sav'

END
