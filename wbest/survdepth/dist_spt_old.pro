;  Plots a distance modulus vs. spectral type graph for various filter
;  band limiting magnitudes, using polynomial functions to generate
;  the absolute magnitudes of the spectral types.
;  Absolute magnitudes are drawn from Kimberly Aller's mega-table of cool
;  dwarfs, unless otherwise noted.
;
;  HISTORY
;  Written by Will Best (IfA), 12/07/2011
;  02/24/2012: Added UKIDSS fits and ideas for future improvements.
;  02/25/2012: Read in all the fits from 
;
;  KEYWORDS
;      PS - output a postscript graph
;
;  FUTURE IMPROVEMENTS
;     Load all the fits from a .sav file.
;     Optional input of one to x filters to plot simultaneously.
;     Designate fixed colors for each band -- can be altered by a keyword.
;          If more than one filter is in same band, use solid/dashed/dotted/etc.
;          Add a filter byte (j=0, h=1, etc.) to the structure -- in the sptfit program?
;     Keyword to just list known filters, then return.
;     If no input filters, give the calling syntax, then return.
;     Lots of printed information, with a /silent keyword.
;

; Polynomial for PS1 z-magnitudes
FUNCTION ZFUNC, S
c = [14.7855, 0.1398, 0.1594, -0.02632, 0.001559, -0.0000311]
;M = c[0] + c[1]*s +c[2]*s^2 + c[3]*s^3 + c[4]*s^4 + c[5]*s^5
M = poly(s, c)
return, M
END

; Polynomial for PS1 y-magnitudes
FUNCTION YFUNC, S
c = [13.8344, 0.08587, 0.1700, -0.02612, 0.001397, -0.0000242]
M = poly(s, c)
return, M
END

; Polynomial for 2MASS J-magnitudes
FUNCTION JFUNC, S
c = [11.7853, 0.004830, 0.1524, -0.01888, 0.0007011, -0.0000042]
M = poly(s, c)
return, M
END

; Polynomial for 2MASS J-magnitudes, from Looper et al. (2008)
FUNCTION JFUNC_L, S
c = [11.817, 0.1255, 0.03690, 0.01663, -0.003915, 0.0002595, -0.000005462]
M = poly(s, c)
return, M
END

; Polynomial for 2MASS H-magnitudes
FUNCTION HFUNC, S
c = [11.0215, -0.06040, 0.1583, -0.02171, 0.001098, -0.0000177]
M = poly(s, c)
return, M
END

; Polynomial for 2MASS K-magnitudes
FUNCTION KFUNC, S
c = [10.5059, -0.07067, 0.1359, -0.01708, 0.0008119, -0.0000117]
M = poly(s, c)
return, M
END

; Polynomial for UKIDSS Y-magnitudes
; only through T6
FUNCTION U_YFUNC, S
c = [13.2132, -0.06816, 0.1390, -0.01536, 0.0004185, 0.0000036]
M = poly(s, c)
return, M
END

; Polynomial for UKIDSS J-magnitudes
; only through T7
FUNCTION U_JFUNC, S
c = [11.8097, -0.2368, 0.2227, -0.02640, 0.001016, -0.0000083]
M = poly(s, c)
return, M
END

; Polynomial for UKIDSS H-magnitudes
; only through T7
FUNCTION U_HFUNC, S
c = [11.1783, -0.3527, 0.2618, -0.03468, 0.001760, -0.0000293]
M = poly(s, c)
return, M
END

; Polynomial for UKIDSS K-magnitudes
; only through T7
FUNCTION U_KFUNC, S
c = [10.6458, -0.3878, 0.2587, -0.03497, 0.001924, -0.0000365]
M = poly(s, c)
return, M
END

; Polynomial for WISE W2-magnitudes
FUNCTION W2FUNC, S
c = [9.8102, 0.01734, 0.06533, -0.007117, 0.0002635, -0.0000019]
M = poly(s, c)
return, M
END

; Polynomial for WISE W2-magnitudes, from Kirkpatrick et al. (2011)
FUNCTION W2FUNC_K, S
c = [9.692, 0.3602, -0.02660, 0.001020]
M = poly(s, c)
return, M
END


PRO DIST_SPT, PS=PS

spt = findgen(19)
; Limiting magnitudes, S/N = 5
lim_zs = 20.5                   ;SDSS z limiting magnitude
lim_z = 21.3                    ;PS1 z limiting magnitude
lim_zmd = 22.05 or 23.18                 ;PS1 Medium-Deep z limiting magnitude
lim_y = 20.3                    ;PS1 3pi y limiting magnitude
lim_ymd = 20.75 or 21.88                 ;PS1 Medium-Deep y limiting magnitude
lim_J = 16.6                    ;2MASS J limiting magnitude
lim_H = 15.9                    ;2MASS H limiting magnitude
lim_K = 15.1                    ;2MASS K limiting magnitude
lim_U_Y = 20.2                  ;UKIDSS Y limiting magnitude
lim_U_J = 19.6                  ;UKIDSS J limiting magnitude
lim_U_H = 18.8                  ;UKIDSS H limiting magnitude
lim_U_K = 18.2                  ;UKIDSS K limiting magnitude
lim_W2 = 15.8                   ;WISE W2 limiting magnitude

sptz = spt
dmodzmd = lim_zmd - zfunc(sptz)
spty = spt
dmodymd = lim_ymd - yfunc(spty)

sptj = spt
dmodj = lim_J - jfunc(sptj)
spth = spt
dmodh = lim_H - hfunc(spth)
sptk = spt
dmodk = lim_K - kfunc(sptk)

sptu_y = spt[0:16]
dmodu_y = lim_U_Y - u_yfunc(sptu_y)
sptu_j = spt[0:17]
dmodu_j = lim_U_J - u_jfunc(sptu_j)
sptu_h = spt[0:17]
dmodu_h = lim_U_H - u_hfunc(sptu_h)
sptu_k = spt[0:17]
dmodu_k = lim_U_K - u_kfunc(sptu_k)

if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/distance1.spt'
    ps_open, fpath, /color, thick=4
endif else window, retain=2, xsize=800, ysize=600

device, decomposed=0
lincolr, /silent
sptaxis = ['L0','L5','T0','T5']
sptval = [0,5,10,15]
plot, spt, dmodzmd, color=0, /nodata, background=1, xstyle=1, ystyle=5, $
      xmargin=[8,8], ymargin=[4,2.5], yrange=[-2,10], $
      xtickname=sptaxis, xrange=[0,19], xtickv=spt, xtitle='Spectral Type', $
      title='Survey Depth by Spectral Type', charsize=2
axis, yaxis=0, color=0, ytitle='Distance (pc)', charsize=2, /ylog, ystyle=1, $
      yrange=10.^((!Y.CRANGE+5.)/5.)  
axis, yaxis=1, color=0, ytitle='Distance Modulus, m - M', charsize=2
oplot, sptz, dmodzmd, color=5, thick=4
oplot, spty, dmodymd, color=4, thick=4
;oplot, sptj, dmodj, color=13, thick=4
;oplot, spth, dmodh, color=8, thick=4
;oplot, sptk, dmodk, color=3, thick=4
oplot, sptu_y, dmodu_y, color=4, thick=4, linestyle=2
oplot, sptu_j, dmodu_j, color=13, thick=4, linestyle=2
oplot, sptu_h, dmodu_h, color=8, thick=4, linestyle=2
oplot, sptu_k, dmodu_k, color=3, thick=4, linestyle=2

;fl = ['2MASS K','2MASS H','2MASS J','PS-MD y','PS-MD z']
fl = ['UKIDSS K','UKIDSS H','UKIDSS J','UKIDSS Y','PS-MD y','PS-MD z']
;cset=[3,8,13,4,5]
cset=[3,8,13,4,4,5]
lset=[2,2,2,2,0,0]
legend, fl, chars=1.2, linestyle=lset, outline=0, /right, colors=cset, textcolors=cset
;lmag = ['LIMITING MAGNITUDES', $
;        string(lim_K,format='("2MASS K = ",f4.1)'), $
;        string(lim_H,format='("2MASS H = ",f4.1)'), $
;        string(lim_J,format='("2MASS J = ",f4.1)'), $
;        string(lim_ymd,format='("PS-MD y = ",f4.1)'), $
;        string(lim_zmd,format='("PS-MD z = ",f4.1)') ]
lmag = ['LIMITING MAGNITUDES', $
        string(lim_U_K,format='("UKIDSS K = ",f4.1)'), $
        string(lim_U_H,format='("UKIDSS H = ",f4.1)'), $
        string(lim_U_J,format='("UKIDSS J = ",f4.1)'), $
        string(lim_U_Y,format='("UKIDSS Y = ",f4.1)'), $
        string(lim_ymd,format='("PS-MD y = ",f4.1)'), $
        string(lim_zmd,format='("PS-MD z = ",f4.1)') ]
mset = [0, cset]
legend, lmag, chars=1.2, outline=1, /bottom, textcolors=mset

if keyword_set(ps) then ps_close

END

