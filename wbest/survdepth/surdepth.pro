; Polynomial for 2MASS J-magnitudes, from Looper et al. (2008)
;FUNCTION JFUNC_L, S
;c = [11.817, 0.1255, 0.03690, 0.01663, -0.003915, 0.0002595, -0.000005462]
;M = poly(s, c)
;return, M
;END

; Polynomial for WISE W2-magnitudes, from Kirkpatrick et al. (2011)
;FUNCTION W2FUNC_K, S
;c = [9.692, 0.3602, -0.02660, 0.001020]
;M = poly(s, c)
;return, M
;END

;PRO SURDEPTH, PS=PS

;  Plots a distance modulus vs. spectral type graph for various filter
;  band limiting magnitudes, using polynomial functions to generate
;  the absolute magnitudes of the spectral types.
;  Absolute magnitudes are drawn from Kimberly Aller's mega-table of cool
;  dwarfs, unless otherwise noted.
;
;  HISTORY
;  Written by Will Best (IfA), 12/07/2011
;  02/24/2012: Added UKIDSS fits and ideas for future improvements.
;  02/25/2012: Read in all the fits from a .sav file.
;
;  KEYWORDS
;      PS - output a postscript graph
;
;  FUTURE IMPROVEMENTS
;     Designate fixed colors for each band -- can be altered by a keyword.
;          If more than one filter is in same band, use solid/dashed/dotted/etc.
;          Add a filter byte (j=0, h=1, etc.) to the structure -- in the sptfit program?
;     Optional input of one to x filters to plot simultaneously.
;     Keyword to just list known filters, then return.
;     If no input filters, give the calling syntax, then return.
;     Lots of printed information, with a /silent keyword.
;

;plotthis = ['PS1 MD z', 'PS1 MD y', $
;            'UKIDSS DXS J', 'UKIDSS DXS K', $
;            'WISE W1', 'WISE W2']

plotthis = ['WISE W2', 'WISE W1', $
            '2MASS J', 'PS1 3pi y', $
            'PS1 3pi z', 'SDSS z']

; Read in the structure with filter names and polynomial fits
restore, '~/idlprogs/wbest/survdepth/Mpoly.sav'

; Structure with curve names, filter names, limiting mags (S/N=5), polynomials
v = replicate({name:'', filt:'', lmag:0., poly:fltarr(6)}, 20)

f = 'PS1 z'
c = filter[where(filter.name eq f)].poly
v[0].name = 'PS1 3pi z' & v[0].filt = f & v[0].lmag = 21.3 & v[0].poly = c
v[1].name = 'PS1 MD z' & v[1].filt = f & v[1].lmag = 22.05 & v[1].poly = c
v[19].name = 'SDSS z' & v[19].filt = f & v[19].lmag = 20.5 & v[19].poly = c

f = 'PS1 y'
c = filter[where(filter.name eq f)].poly
v[2].name = 'PS1 3pi y' & v[2].filt = f & v[2].lmag = 20.3 & v[2].poly = c
v[3].name = 'PS1 MD y' & v[3].filt = f & v[3].lmag = 20.75 & v[3].poly = c

f = '2MASS J'
c = filter[where(filter.name eq f)].poly
v[4].name = '2MASS J' & v[4].filt = f & v[4].lmag = 16.6 & v[4].poly = c

f = '2MASS H'
c = filter[where(filter.name eq f)].poly
v[5].name = '2MASS H' & v[5].filt = f & v[5].lmag = 15.9 & v[5].poly = c

f = '2MASS K'
c = filter[where(filter.name eq f)].poly
v[6].name = '2MASS K' & v[6].filt = f & v[6].lmag = 15.1 & v[6].poly = c

f = 'UKIDSS Y'
c = filter[where(filter.name eq f)].poly
v[7].name = 'UKIDSS LAS Y' & v[7].filt = f & v[7].lmag = 20.2 & v[7].poly = c

f = 'UKIDSS J'
c = filter[where(filter.name eq f)].poly
v[9].name = 'UKIDSS LAS J' & v[9].filt = f & v[9].lmag = 19.6 & v[9].poly = c
v[10].name = 'UKIDSS DXS J' & v[10].filt = f & v[10].lmag = 21.65 & v[10].poly = c

f = 'UKIDSS H'
c = filter[where(filter.name eq f)].poly
v[11].name = 'UKIDSS LAS H' & v[11].filt = f & v[11].lmag = 18.8 & v[11].poly = c

f = 'UKIDSS K'
c = filter[where(filter.name eq f)].poly
v[13].name = 'UKIDSS LAS K' & v[13].filt = f & v[13].lmag = 18.2 & v[13].poly = c
v[14].name = 'UKIDSS DXS K' & v[14].filt = f & v[14].lmag = 20.2 & v[14].poly = c

f = 'WISE W1'
c = filter[where(filter.name eq f)].poly
v[15].name = 'WISE W1' & v[15].filt = f & v[15].lmag = 17.1 & v[15].poly = c

f = 'WISE W2'
c = filter[where(filter.name eq f)].poly
v[16].name = 'WISE W2' & v[16].filt = f & v[16].lmag = 15.8 & v[16].poly = c

f = 'WISE W3'
c = filter[where(filter.name eq f)].poly
v[17].name = 'WISE W3' & v[17].filt = f & v[17].lmag = 11.7 & v[17].poly = c

;f = 'WISE W4'
;c = filter[where(filter.name eq f)].poly
;v[18].name = 'WISE W4' & v[18].filt = f & v[18].lmag = 8.0 & v[18].poly = c

;f = 'SDSS z'
;c = filter[where(filter.name eq f)].poly
;v[19].name = 'SDSS z' & v[19].filt = f & v[19].lmag = 20.5 & v[19].poly = c

match, v.name, plotthis, ind     ; ind = vector of matching indices
;ind = ind(sort(ind))
ind = ind[[5,4,0,1,2,3]]
nc = n_elements(ind)
ns = 19
spt = findgen(ns)+10
dmod = fltarr(ns, nc)
for i=0, nc-1 do dmod[*,i] = v[ind[i]].lmag - poly(spt, v[ind[i]].poly)

if keyword_set(ps) then begin
    fpath = '~/Desktop/Astro/distance2.spt'
    ps_open, fpath, /color, /en, thick=4
endif else window, retain=2, xsize=800, ysize=600

device, decomposed=0
lincolr_wb, /silent
sptaxis = ['L0','L5','T0','T5']
sptval = [0,5,10,15]
plot, spt, dmod[*,0], color=0, /nodata, background=1, xstyle=1, ystyle=5, $
      xmargin=[8,8], ymargin=[4,2.5], yrange=[-2,12], $
      xtickname=sptaxis, xrange=[10,29], xtickv=spt, xtitle='Spectral Type', $
      title='Survey Depth by Spectral Type', charsize=2
axis, yaxis=0, color=0, ytitle='Distance (pc)', charsize=2, /ylog, ystyle=1, $
      yrange=10.^((!Y.CRANGE+5.)/5.)  
axis, yaxis=1, color=0, ytitle='Distance Modulus, m - M', charsize=2

;cset=[4,13,8,3,7,0]
cset = [7,3,8,13,4,6]
;lset=[0,0,2,2,2,2]
lset = 0
for i=0, nc-1 do oplot, spt, dmod[*,i], color=cset[i], linestyle=lset, thick=4

fl = v[ind].name
legend, fl, chars=1.2, linestyle=lset, outline=0, /right, colors=cset, textcolors=cset

ll = strarr(nc)
for i=0, nc-1 do ll[i] = string(v[ind[i]].name, v[ind[i]].lmag, format='(a," = ",f4.1)')
lmag = ['LIMITING MAGNITUDES', ll ]
mset = [0, cset]
legend, lmag, chars=1.2, outline=1, /bottom, textcolors=mset

if keyword_set(ps) then ps_close

END

