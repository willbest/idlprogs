PRO FITSPEC_REVIEW, FILELIST, FDIR=fdir

; HISTORY
; Written 2013-10-03 by Will Best (IfA)

if not file_test(filelist) then begin
    message, 'File list does not exist.'
endif

if not keyword_set(fdir) then fdir = ''

readcol, filelist, list, format='a'
n = n_elements(list)

for i=0, n-1 do begin

    if not file_test(fdir+list[i]) then message, 'File '+fdir+list[i]+' does not exist.'

    gv, fdir+list[i]
    checkcontinue

endfor

END
