pro tdwarf_indices_go_lttrans, infile, $
                               spt_avg, spt_rms, type_ratios, bbk_ratios, $
                               latex_str, label_str, $
                               csv_str, $
                               noendpoints=noendpoints, $
                               nobbk = nobbk, $
                               allbbk = allbbk, $                ; default is to only print K/J ratio
                               plot = plot, $
                               fdir = fdir, $                    ; directory containing spectra files
                               altname = altname, $              ; alternate format for list of input spectra
                               silent = silent

;+
; wrapper for running TDWARF_INDICES.PRO on a list of spectra
; 08/11/10 MCL
;
; added /allbbk to print all the BBK indices as output
; 02/14/12 MCL
;
; added csv_str, to get all info for comparison to Burgasser et al. (2010) plots
; 10/03/13 WB
;-

; tdwarf_indices_go_lttrans, 'PWlist.txt', fdir='~/Dropbox/panstarrs-BD/SPECTRA/'

; Check for directory containing spectra files
if not(keyword_set(fdir)) then fdir = ''
; Check for input file
if n_params() eq 0 then begin
   print, 'pro tdwarf_indices_go, infile,'
   print, '                       spt_avg, spt_rms, type_ratios, bbk_ratios, ' 
   print, '                       [noendpoints], [nobbk], [plot],'
   print, '                       [fdir=], [altname], [silent]'
   return
endif


; Read in the list of files
if keyword_set(altname) then $
   readcol2, infile, filelist, namelist, form = 'a,x,x,x,x,a', /silent $
else begin
   readcol2, infile, filelist, form = 'a', /silent
   namelist = filelist
endelse
   

nspec = n_elements(filelist)
spt_avg = fltarr(nspec)
spt_rms = fltarr(nspec)
latex_str = strarr(nspec)
latex_bbk_str = strarr(nspec)
csv_str = strarr(nspec)
message, 'number of input spectra = '+strc(nspec), /info
for i = 0, nspec-1 do begin

   if (i mod 10) eq 0 then $
      print, form = '($," ",a)', strc(i)

   ; compute indices
   tdwarf_indices_lttrans, fdir+filelist(i), dummy, $
                           tt, bb, savg, srms, $
                           ll_str, lab_str, type_str, $
                           ll_bbk_str, lab_bbk_str, bbk_str, $    ; string outputs for BBK indices
                           hdrat, ll_hd_str, hd_str, $
                           cstr, $
                           noendpoints=noendpoints, $
                           nobbk = nobbk, $
                           plot = plot, /silent
   if keyword_set(plot) then $
      checkcontinue

   ; save results
   spt_avg(i) = savg
   spt_rms(i) = srms
   csv_str[i] = cstr
   if (i eq 0) then begin
      type_ratios = fltarr(n_elements(tt), nspec)
      bbk_ratios = fltarr(n_elements(bb), nspec)
   endif
   type_ratios(*, i) = tt
   bbk_ratios(*, i) = bb
   latex_str(i) = ll_str 
   latex_bbk_str(i) = ll_bbk_str 

endfor
print


; merge object name to front and K/J ratio to back of Latex string
; if /allbbk, then append all the BBK ratios not just K/J
nlen = max(strlen(namelist))
label_str = string('% Object', form = '(a-'+strc(nlen)+'," & ")') + repstr(lab_str, '%', ' ')
label_bbk_str = repstr(lab_bbk_str, '%', ' ')
latex_str = string(namelist, form = '(a-'+strc(nlen)+'," & ")') + latex_str
;csv_pos2 = strpos(namelist, '-', 3)
;csv_str = strmid(namelist,3,1#csv_pos2-3) + strmid(namelist,1#csv_pos2+1,6) + ',' + csv_str
csv_str = strcompress(string(namelist, form = '(a-'+strc(nlen)+',",")'), /remove_all) + csv_str
forprint, csv_str, textout='~/Desktop/CFHT_T_spexprism.csv', format='a', $
          comment='#name,H2O-J,H2O-J_t,CH4-J,CH4-J_t,H2O-H,H2O-H_t,CH4-H,CH4-H_t,CH4-K,CH4-K_t,H2O-K,H2O-K_t,K/J,H-dip'

;label_str = string('% Object', form = '(a-'+strc(nlen)+'," & ")') + repstr(label_str, '%', ' ')+'             K/J'
;latex_str = string(namelist, form = '(a-'+strc(nlen)+'," & ")') + latex_str + string(bbk_ratios(where(bbk_str eq 'K/J'), *), form = '(f8.3," &")')
if keyword_set(allbbk) then begin
   label_str = label_str + label_bbk_str
   latex_str = latex_str + latex_bbk_str
endif else begin
   label_str = label_str + '             K/J'
   latex_str = latex_str + string(bbk_ratios(where(bbk_str eq 'K/J'), *), form = '(f8.3," &")')
endelse
; print final table
if not(keyword_set(silent)) then begin
   print, label_str
   print, latex_str
endif


end


