pro tdwarf_compare_standards, wl, flux, $
                              fullspectrum = fullspectrum, $
                              Ldwarf = Ldwarf, lateT = lateT, $
                              fdir = fdir, $
                              _extra = _extra
                              
;+
; Quick script to visually classify T dwarfs using Spex Prism Library data
; xx/xx/10 MCL
;
; added /lateT option to compare to late-T spectra from a variety of sources
; 06/26/11 MCL
;
; added forward/backward functionality for viewing spectra (nice!)
; added L8 and L9 standards from Kirkpatrick et al (2010)
; updated paths to use new Spex Prism Library location
; 09/21/11 MCL
; 
; added \Ldwarf using Kirkpatrick et al (2010) standards
; note that these are defined only for 0.9-1.4 micron region
; 03/27/13 MCL
;-

if not(keyword_set(fdir)) then fdir = ''
if n_params() lt 1 then begin
  print, 'pro tdwarf_compare_standards, wl, flux, [fullspectrum], [lateT], [_extra=]'
  return
endif


; load the spectra, if desired
if size(wl, /tname) eq 'STRING' then begin
   filebreak, wl, ext = ext
   label1 = wl
   if (ext eq 'fits') then begin
      im = readfits(fdir+wl, /silent)
      wl = im(*, 0)
      flux = im(*, 1)
   endif else begin
      readcol2, wl, a, b
      wl = a
      flux = b
   endelse
endif



if keyword_set(Ldwarf) then begin
   ;stddir = '~/work/brown-spectra/spex-prism/all/'
   stddir = '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-prism/all/'
   stdlist = [ $
             ['spex-prism_VB10_20030919_BUR04B.txt',                    'M8 vB 10'], $
             ['spex-prism_LHS2924_20040312_BUR06B.txt',                 'M9 LHS 2924'], $
             ['spex-prism_2MASPJ0345432+254023_20030905_BUR06B.txt',    'L0 2MASP J0345+2540'], $
             ['spex-prism_2MASSWJ2130446-084520_20080713_KIR10A.txt',   'L1 2MASSW J2130-0845'], $
             ['spex-prism_Kelu-1_20060411_BUR07D.txt',                  'L2 Kelu-1AB'], $
             ['spex-prism_2MASSWJ1506544+132106_20060410_BUR07C.txt',   'L3 2MASSW J1506+1321'], $
             ['spex-prism_2MASSJ21580457-1550098_20070927_KIR10A.txt',  'L4 2MASS J2158-1550'], $
             ['spex-prism_SDSSJ083506.16+195304.4_20050123_CHI06A.txt', 'L5 SDSS J0835+1953'], $
             ['spex-prism_2MASSIJ1010148-040649_20050324_REI06A.txt',   'L6 2MASSI J1010-0406'], $
             ['spex-prism_2MASSIJ0103320+193536_20030919_CRU04A.txt',   'L7 2MASSI J0103+1935'], $
             ['spex-prism_2MASSWJ1632291+190441_20030905_BUR07C.txt',   'L8 2MASSW J1632+1904'], $
             ['spex-prism_DENIS-PJ0255-4700_20040908_BUR06C.txt',       'L9 DENIS-P J0255-4700'], $
             ['spex-prism_SDSSJ120747.17+024424.8_20061221_LOO07A.txt', 'T0 SDSS J1207+0244'], $
             ['spex-prism_SDSSJ015141.69+124429.6_20030919_BUR04B.txt', 'T1 SDSS J0151+1244'] $
             ]
endif else if keyword_set(lateT) then begin
   ;stddir = '~/work/brown-spectra/'
   stddir = '~/Dropbox/GROUP/PUBLIC/brown-spectra/'
   stdlist = [ $
             ['old_spex-prism/published/spex_prism_0727+1710_040310.txt',    'T7'], $
             ['old_spex-prism/published/spex_prism_0050-3322_040907.txt',    'T7'], $
             ['old_spex-prism/published/spex_prism_1114-2618_040312.txt',    'T7.5'], $
             ['old_spex-prism/published/spex_prism_1217-0311_040311.txt',    'T7.5'], $
             ['old_spex-prism/published/spex_prism_1457-2124_030522.txt',    'T7.5'], $
             ['old_spex-prism/published/spex_prism_hd3651b_060903.txt',      'T7.5'], $
             ['burgasser/spex/T7.5_gl570d.spex.txt',                         'T7.5'], $
             ['old_spex-prism/published/spex_prism_0729-3954_061208dl.txt',  'T8'], $
             ['old_spex-prism/published/spex_prism_0939-2448_040312.txt',    'T8'], $
             ['old_spex-prism/published/spex_prism_0415-0935_030917.txt',    'T8'], $
             ;
             ['ukidss/burningham08/T8_ulasj1017_JHK_full.burningham08.txt',  'T8'], $
             ['ukidss/burningham08/T8.5_ulas2146_JHK.burningham08.txt',      'T8.5'], $
             ['ukidss/burningham08/T8.5_ulasj1238_3150s_K.burningham08.txt', 'T8.5'], $
             ['ukidss/burningham08/T8.5_ulasj1238_JH_127n.burningham08.txt', 'T8.5'], $
             ['late-Ts/T8.5_CFBDS005910-011401_JHK-delorme08.dat',           'T8.5'], $
             ['late-Ts/T8.5_fire_prism_Ross458c-t1g_100403.fits',            'T8.5'], $
             ['late-Ts/T8.5_w940b_nir_irs.leggett10.fl',                     'T8.5'], $
             ['ukidss/burningham08/T9_ulasj1335_YJHK.burningham08.txt',      'T9'], $
             ['late-Ts/T9_WISE0458_mainzer10.txt',                           'T9'], $
             ['late-Ts/T10_ugps0722_JHK_lucas10.txt',                        'T10'] $
             ]
endif else begin
   ;stddir = '~/work/brown-spectra/spex-prism/all/'
   stddir = '~/Dropbox/GROUP/PUBLIC/brown-spectra/spex-prism/all/'
   stdlist = [['spex-prism_2MASSWJ1632291+190441_20030905_BUR07C.txt',    'L8 2MASSW J1632+1904'], $
              ['spex-prism_DENIS-PJ0255-4700_20040908_BUR06C.txt',        'L9 DENIS-P J0255-4700'], $
              ['spex-prism_SDSSJ120747.17+024424.8_20061221_LOO07A.txt',  'T0 SDSS J1207+0244'], $
              ['spex-prism_SDSSJ015141.69+124429.6_20030919_BUR04B.txt',  'T1 SDSS J0151+1244'], $
              ['spex-prism_SDSSpJ125453.90-012247.4_20030522_BUR04B.txt', 'T2 SDSSp J1254-0122'], $
              ['spex-prism_SDSSJ120602.51+281328.7_20050408_CHI06A.txt',  'T3 SDSS J1206+2813'], $      
              ['spex-prism_2MASSJ12095613-1004008_20030523_BUR04B.txt',   'T3 2MASS J1209-1004 (bad)'], $    
              ['spex-prism_2MASSIJ2254188+312349_20030918_BUR04B.txt',    'T4 2MASSI J2254+3123'], $
              ['spex-prism_2MASSJ15031961+2525196_20030522_BUR04B.txt',   'T5 2MASS J1503+2525'], $
              ['spex-prism_SDSSpJ162414.37+002915.6_20040312_BUR06D.txt', 'T6 SDSSp J1624+0029'], $
              ['spex-prism_2MASSIJ0727182+171001_20040310_BUR06D.txt',    'T7 2MASSI J0727+1710'], $
              ['spex-prism_2MASSIJ0415195-093506_20030917_BUR04B.txt',    'T8 2MASSI J0415-0935']]
   ;stddir = '~/work/brown-spectra/old_spex-prism/published/'
   ;stdlist = [['spex_prism_1207+0244_061221dl.txt', 'T0'], $
              ;['spex_prism_sdss0151+1244_030919.txt', 'T1 '], $
              ;['spex_prism_sdss1254-0122_030522.txt', 'T2'], $
              ;['spex_prism_sdss1206+28_chiu06.txt', 'T3'], $       ; my recommnded T3 standard, since 2m1209-10 is binary
              ;['spex_prism_1209-1004_030523.txt', 'T3 (bad)'], $    
              ;['spex_prism_2254+3123_030918.txt', 'T4'], $
              ;['spex_prism_1503+2525_030522.txt', 'T5'], $
              ;['spex_prism_sdss1624+0029_040312.txt', 'T6'], $
              ;['spex_prism_0727+1710_040310.txt', 'T7'], $
              ;['spex_prism_0415-0935_030917.txt', 'T8']]
endelse



nstd = n_elements(stdlist(0, *))


; open window
if (!d.window(0) eq -1) then $
   window, 0 $
else $
   wshow


; compare spectra
i = 0
done = 0
cr = string("12b)		; carriage return
repeat begin
   win, 0, xs = 1000, ys = 1200, /match, /wshow, /check
   yr = [0, max(flux(between(wl, 0.9, 2.4)))]
   compare_spectra, [[wl], [flux]], stddir+stdlist(0, i), $
                    label1 = label1, label2 = stdlist(1, i), legend = 1.5, $
                    jhk = (keyword_set(fullspectrum) eq 0), /peak, $
                    yr = yr, _extra = _extra, /silent
   win, 1, xs = 1000, ys = 600, /match, /wshow, /check
   compare_spectra, [[wl], [flux]], stddir+stdlist(0, i), $
                    label1 = label1, label2 = stdlist(1, i), legend = 1.5, $
                    wl = [0.95, 2.35], /peak, $
                    yr = yr, _extra = _extra, /silent

   repeat begin
      print, format = '($,A)', 'advance Forward, Backward or Quit? (<f>,b,q) '
      ans = 1
      case get_kbrd(1) of
         'f': i = (i+1) < (nstd-1)
         'b': i = (i-1) > 0
         'q': done = 1
         ;'q': i = nstd
         cr: i = (i+1) < (nstd-1)
         else: ans = -1
      endcase
      print
   endrep until (ans ge 0)

endrep until (i eq nstd) or (done eq 1)
print, 'finished science target ', label1
print, 'final comparison spectrum = ', stdlist(1, i)

end
