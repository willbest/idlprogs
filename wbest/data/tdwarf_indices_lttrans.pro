pro tdwarf_indices_lttrans, wl_orig, flux, $                        ; inputs
                    type_ratios, bbk_ratios, spt_avg, spt_rms, $    ; output values
                    latex_str, label_str, type_str, $               ; string outputs for SpT indices
                    latex_bbk_str, label_bbk_str, bbk_str, $        ; string outputs for BBK indices
                    hdip_ratio, latex_hdip_str, hdip_str, $         ; hdip index stuff
                    csv_str, $                                      ; CSV-format string with results
                    noendpoints=noendpoints, $
                    nobbk = nobbk, $
                    plot = plot, $
                    silent = silent

;+
; measure spectral indices for T dwarfs
; as defined by Burgasser et al (2006) papers (spectral type & BBK analysis)
; 10/30/06 MCL
;
; added conversion of indices to SpTs based on polynomial fits from Burgasser (2007)
; 06/29/08 MCL
;
; added /nobbk
; prints avg SpT and RMS at the end
; 10/07/10 MCL
;
; added ability to read FITS and text files
; added Latex output line
; added /plot
; fixed typo for polynomial fits vs SpT for CH4-K index 
;   which gave erroneously late SpTs for objects earlier than ~T4
; added check to see if index is in the defined range
; 11/18/10 MCL
;
; added H2O-K to indices being computed
; 11/21/10 MCL
;
; preserves name of input file if passed as string to 'wl' variable
; 06/19/11 MCL
;
; added W_J to list of indices
; note: no need to account for the defining factor of 2 in the denominator of this index
;       since SPECBAND.PRO computes the *average* flux in the bandpass,
;       and thus the factor of 2 is already implicit in the calculation
; adjusted 'type_range' values to reflect the lower numerical range of 
;   index values from Burgasser et al (2006), Table 5
; 07/04/11 MCL
;
; bug fix: upper range for CH4-K fit corrected from T0 to L0
; 08/07/11 MCL
;
; added 'spt_avg', 'spt_rms', and '*_str' as output variables
; 08/10/11 MCL
;
; added full set of string output for BBK indices, by adding 'latex_bbk_str' and 'label_bbk_str' as outputs
; one side effect is that position of 'bbk_str' output variable has changed in the program call
; 02/14/12 MCL
;
; added H-dip index from Burgasser et al. (2010)
; 10/03/13 WB
;-



if n_params() lt 1 then begin
    print, 'pro tdwarf_indices, wl, flux, type_ratios, bbk_ratios, [noendpoints]'
    print, '   or'
    print, 'pro tdwarf_indices, (filename), (dummy), type_ratios, bbk_ratios, [noendpoints]'
    return
endif


;----------------------------------------------------------------------
; spectral indices for classification
;----------------------------------------------------------------------
type_str = ['H2O-J', 'CH4-J', 'H2O-H', 'CH4-H', 'CH4-K', 'H2O-K', 'W_J', 'NH3-H']

; range of SpTs for which fit is valid
type_range = [[ 0, 17.32], $   ; 08/11/11: corrected upper value from 18.76
              [10, 17.64], $   ; 08/11/11: corrected upper value from 18.47
              [ 0, 19.13], $
              [11, 17.51], $   ; 08/10/11: corrected upper value from 18.72
              [ 0, 16.34], $   ; 08/07/11: corrected lower value from T0
              [-10, -10], $    ; no polynomial fit for H2O-K available
              [-10, -10], $    ; no polynomial fit for W_J available
              [-10, -10] $     ; no polynomial fit for NH3-H available
             ]
;type_range = [[0, 18.76], $   
;              [10, 18.47], $  
;              [0, 19.13], $
;              [11, 18.72], $
;              [0, 16.34], $    ; corrected on 08/07/11, previously upper range was set to T0
;              [-10, -10], $    ; no polynomial fit for H2O-K available
;              [-10, -10], $    ; no polynomial fit for W_J available
;              [-10, -10] $     ; no polynomial fit for NH3-H available
;             ]

; wl definition of indices
type_wl = [[[1.14, 1.165],  [1.26, 1.285]], $
           [[1.315, 1.34],  [1.26, 1.285]], $
           [[1.48, 1.52],   [1.56, 1.60]], $
           [[1.635, 1.675], [1.56, 1.60]], $
           [[2.215, 2.255], [2.08, 2.12]], $
           [[1.975, 1.995], [2.08, 2.10]], $
           [[1.18, 1.23],   [1.26, 1.285]], $
           [[1.53, 1.56],   [1.57, 1.60]] $
           ]

; along with 4th order polynomial fits to index vs SpT from Burgasser (2007)
;   where SpT=0 is L0, =1 is L1, SpT=10 is T0, etc.
type_coeff = [[1.949E1, -3.919E1, 1.312E2, -2.156E2, 1.038E2], $
              [2.098E1, -1.978E1, 2.527E1, -3.221E1, 9.087E-1], $
              [2.708E1, -8.450E1, 2.424E2, -3.381E2, 1.491E2], $
              [2.013E1, -2.291E1, 4.361E1, -5.068E1, 2.084E1], $
              [1.885E1, -2.246E1, 2.534E1, -4.734E0, -1.259E1], $
              ;[1.885E1, -2.246E1, 2.534E1, -4.734E0, 1.259E1]]   ; fixed 11/19/10
              [-50.0, -50.0, -50.0, -50.0, -50.0], $
              [-50.0, -50.0, -50.0, -50.0, -50.0], $
              [-50.0, -50.0, -50.0, -50.0, -50.0] $
             ]

; resulting (string) SpTs from each index
type_ratios_sptstr = strarr(n_elements(type_str))   

; moderate-width spectral indices for BBK analysis of log(g) and Teff
bbk_str = ['H2O-J', 'H2O-H', 'Y/J', 'K/J', 'K/H']
bbk_wl = [[[1.14, 1.165], [1.26, 1.285]], $
         [[1.48, 1.52], [1.56, 1.60]], $
         [[1.005, 1.045], [1.25, 1.29]], $
         [[2.06, 2.10], [1.25, 1.29]], $
         [[2.06, 2.10], [1.56, 1.60]]]

; H-dip spectral index for analysis of 1.65 um methane feature in T dwarfs
hdip_str = 'H-dip'
hdip_wl = [[1.61, 1.64], [1.56, 1.59], [1.66, 1.69]]

;----------------------------------------------------------------------
; load FITS file or text file, if passed
if (size(wl_orig, /tname)) eq 'STRING' then begin
   message, 'reading in file "'+wl_orig+'"', /info, noprint = silent
   filebreak, wl_orig, ext = ext
   if (ext eq 'fits') then begin
      im = readfits(wl_orig, /silent)
      wl = im(*, 0)
      flux = im(*, 1)
   endif else begin
      readcol2, wl_orig, a, b, /silent
      wl = a
      flux = b
   endelse
endif else $
   wl = wl_orig


; loop over two sets of ratios
if keyword_set(plot) then $
   plot, wl, flux, chars = 2, xtit = 'wavelength', ytit = 'flux', tit = 'spectral typing indices', ps = 10
for i=0, 1-keyword_set(nobbk) do begin
    if (i eq 0) then begin
        rat_wl = type_wl
        rat_str = type_str
        if not(keyword_set(silent)) then begin
           print, '   Spectral indices: Burgasser et al, and others'
        endif
     endif else begin
        rat_wl = bbk_wl
        rat_str = bbk_str
        if not(keyword_set(silent)) then begin
           print, '   BBK indices'
        endif
    endelse

    ; compute ratios
    out = fltarr(n_elements(rat_str))
    spt = fltarr(n_elements(rat_str))
    flag = intarr(n_elements(rat_str)) + 1
    flagstr = strarr(n_elements(rat_str))
    for j=0, n_elements(rat_str)-1 do begin
        f1 = specband(wl, flux, rat_wl(0, 0, j), rat_wl(1, 0, j), $
                      noendpoints=noendpoints)
        f2 = specband(wl, flux, rat_wl(0, 1, j), rat_wl(1, 1, j), $
                      noendpoints=noendpoints)
        out(j) = f1/f2
        spt(j) = poly(out(j), type_coeff(*, j))

        ; for SpT indices, check if index is within its defined range
        if (i eq 0) and (spt(j) lt type_range(0, j)) or (spt(j) gt type_range(1, j)) then begin
           flag(j) = 0
           flagstr(j) = 'out of range'
           if (spt(j) lt type_range(0, j)) then $
              spt(j) = -99 $
           else if (spt(j) gt type_range(1, j)) then $
              spt(j) = 99 
           if (type_range (0, j) eq type_range(1, j)) then $    ; no SpT-to-index relation exists
              spt(j) = !values.f_nan
        endif

        if keyword_set(plot) then begin
           plots, rat_wl(0, 0, j)+[0, 0], !y.crange*[1, 0.9], col = j+2
           plots, rat_wl(1, 0, j)+[0, 0], !y.crange*[1, 0.9], col = j+2
           plots, rat_wl(0, 1, j)+[0, 0], !y.crange*[1, 0.9], col = j+2, line = 5
           plots, rat_wl(1, 1, j)+[0, 0], !y.crange*[1, 0.9], col = j+2, line = 5
           xyouts, avg(rat_wl(*, *, j)), !y.crange(1)*(0.95-0.02*(j mod 2)), align = 0.5, col = j+2, rat_str(j)
        endif

     endfor

    ; compute SpT
    ; using only indices that are within the defined range
    if (i eq 0) then begin
       wg = where(flag eq 1, ng, comp = wb, ncomp = nb)
       type_ratios_sptstr(wg) = sptypenum(spt(wg)+10., /reverse)

       ; assign upper/lower limits for indices outside of defined range
       if (nb ge 1) then begin
          for j = 0, nb-1 do begin
              wlow = where(spt eq -99, nlow)
              whigh = where(spt eq 99, nhigh)
              wnone = where(finite(spt) eq 0, nnone)
              if (nlow ge 1) then $
                 type_ratios_sptstr(wlow) = '< '+sptypenum(type_range(0, wlow)+10, /reverse)
              if (nhigh ge 1) then $
                 type_ratios_sptstr(whigh) = '>='+sptypenum(ceil(type_range(1, whigh)+10), /reverse)
              if (nnone ge 1) then $
                 type_ratios_sptstr(wnone) = ' -- '
           endfor
       endif
              
       ; compute average SpT and its RMS using the good index values
       if (ng ge 1) then begin
          spt_avg = avg(spt(wg)) + 10.
          if (ng gt 1) then $
             spt_rms = stdev(spt(wg)) $
          else $
             spt_rms = -99
       endif else begin
          spt_avg = -99
          spt_rms = -99
       endelse
    endif

    ; print results
    if (i eq 0) then begin
       if not(keyword_set(silent)) then $
          forprint, '     '+rat_str, out, '  '+type_ratios_sptstr, '  '+flagstr, $
                    form = 'a12,f10.3,a7,a15' 
    endif else if not(keyword_set(silent)) then $
       forprint, '     '+rat_str, out, form = 'a12,f10.3' 

    ; store result to output variable
    if (i eq 0) then $
      type_ratios = out $
      else $
      bbk_ratios = out

    ; compute average SpT and RMS
    if (i eq 0) and not(keyword_set(silent)) then begin
       print, '     SpT(avg)=', sptypenum(spt_avg, /reverse), $
              '  rms=', string(spt_rms, '(f3.1)')
    endif

    if keyword_set(plot) then begin
       checkcontinue
       plot, wl, flux, chars = 2, xtit = 'wavelength', ytit = 'flux', tit = 'BBK indices'
    endif

 endfor


; H-dip index
rat_wl = hdip_wl
rat_str = hdip_str
if not(keyword_set(silent)) then print, '   H-dip spectral index';: Burgasser et al. (2010)'

; compute ratios
out = fltarr(n_elements(rat_str))
for j=0, n_elements(rat_str)-1 do begin
    f1 = specband(wl, flux, rat_wl[0, 0, j], rat_wl[1, 0, j], $
                  noendpoints=noendpoints)
    f2 = specband(wl, flux, rat_wl[0, 1, j], rat_wl[1, 1, j], $
                  noendpoints=noendpoints)
    f3 = specband(wl, flux, rat_wl[0, 2, j], rat_wl[1, 2, j], $
                  noendpoints=noendpoints)
    out[j] = f1/(f2+f3)

    if keyword_set(plot) then begin
        plots, rat_wl(0, 0, j)+[0, 0], !y.crange*[1, 0.9], col = j+2
        plots, rat_wl(1, 0, j)+[0, 0], !y.crange*[1, 0.9], col = j+2
        plots, rat_wl(0, 1, j)+[0, 0], !y.crange*[1, 0.9], col = j+2, line = 5
        plots, rat_wl(1, 1, j)+[0, 0], !y.crange*[1, 0.9], col = j+2, line = 5
        plots, rat_wl(0, 0, j)+[0, 0], !y.crange*[1, 0.9], col = j+2
        plots, rat_wl(1, 0, j)+[0, 0], !y.crange*[1, 0.9], col = j+2
        plots, rat_wl(0, 2, j)+[0, 0], !y.crange*[1, 0.9], col = j+2, line = 5
        plots, rat_wl(1, 2, j)+[0, 0], !y.crange*[1, 0.9], col = j+2, line = 5
        xyouts, avg(rat_wl(*, *, j)), !y.crange(1)*(0.95-0.02*(j mod 2)), align = 0.5, col = j+2, rat_str(j)
    endif

endfor

; print results
if not(keyword_set(silent)) then $
  forprint, '     '+rat_str, out, form = 'a12,f10.3' 

; store result to output variable
hdip_ratio = out

if keyword_set(plot) then begin
    checkcontinue
    plot, wl, flux, chars = 2, xtit = 'wavelength', ytit = 'flux', tit = 'H-dip index'
endif

;;;;;;;;;;;;;;;

; print results in Latex format
nt = string(n_elements(type_str))
nbbk = string(n_elements(bbk_str))
nhdip = string(n_elements(hdip_str))
ss = sptypenum(type_ratios_sptstr, /silent)

label_str = string(type_str, form = '("   %"'+strc(nt)+'(a11,"   "), "      SpT:avg/RMS")')
latex_str = '  '
for i = 0, nt-1 do $
   latex_str = latex_str + string(type_ratios(i), form = '(f6.3)') + ' ('+type_ratios_sptstr(i)+') &'

label_bbk_str = string(bbk_str, form = '("   %"'+strc(nbbk)+'(a11,"   "))')
latex_bbk_str = '  '
for i = 0, nbbk-1 do $
   latex_bbk_str = latex_bbk_str + string(bbk_ratios(i), form = '(f6.3)') + ' &'

latex_hdip_str = '  '
for i = 0, nhdip-1 do $
   latex_hdip_str = latex_hdip_str + string(hdip_ratio[i], form = '(f6.3)')

latex_str = latex_str + ' ' + sptypenum(spt_avg, /rev) + '$\pm$' + string(spt_rms, form = '(f3.1)') + ' \\'


if not(keyword_set(silent)) then begin
   ;sptavg = sptypenum(avg(ss), /reverse)
   ;sptrms = stdev(ss)
   print
   print, label_str
   print, latex_str
   ;print, type_str, form = '("   %"'+strc(nt)+'(a11,"   "), "      avg/RMS")'
   ;print, latex_str, ' ', sptypenum(spt_avg, /rev), '$\pm$', string(spt_rms, form = '(f3.1)'), ' \\'
   ;print, '  ', string(type_ratios, form = '(f5.3)')+' ('+type_ratios_sptstr+') &', $
   ;       sptavg, '+/-', string(sptrms, form = '(f3.1)')
   print
endif

; string for CSV file
csv_str = trim(type_ratios[0], '(f6.3)') + ',' + type_ratios_sptstr[0] + ','
for i=1, 5 do $
  csv_str = csv_str + trim(type_ratios[i], '(f6.3)') + ',' + type_ratios_sptstr[i] + ','
csv_str = csv_str + trim(bbk_ratios[3], '(f6.3)') + ','
csv_str = csv_str + trim(hdip_ratio[0], '(f6.3)')
;print, csv_str

end
