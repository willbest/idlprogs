;PRO MAKETABLE

;  Wrapper for tablegen.pro
;
;  HISTORY
;  Written by Will Best (IfA), 02/07/2013
;
;  USE
;      tablegen, objlist [, OUTFILE, DATAFILE=datafile, CAPTION=caption, ROTATE=rotate, $
;                SCRIPTSIZE=scriptsize, PS1RA=ps1ra, PS1DEC=ps1dec, GAL=gal, PM=pm, $
;                TMDES=tmdes, WDES=wdes, PS1PHOT=ps1phot, TMPHOT=tmphot, MKOPHOT=mkophot, $
;                WPHOT=wphot, H2OJ=h2oj, CH4J=ch4j, H2OH=h2oh, CH4H=ch4h, H2OK=h2ok, $
;                CH4K=ch4k, SPT=spt, PDIST=pdist, VTAN=vtan, TEFF=teff, LUM=lum]

;objlist = ['PSO J307.6784+07.8263', 'PSO J339.0734+51.0978']
objlist = ['PSO J007.7921+57.8267', 'PSO J103.0927+41.4601', 'PSO J140.2308+45.6487', 'PSO J272.4689-04.8036', 'PSO J282.7576+59.5858', 'PSO J307.6784+07.8263', 'PSO J339.0734+51.0978']
;objlist = ['PSO J007.7921+57.8267', 'PSO J103.0927+41.4601', 'PSO J140.2308+45.6487', 'PSO J272.4689-04.8036']
;objlist = ['PSO J282.7576+59.5858', 'PSO J307.6784+07.8263', 'PSO J339.0734+51.0978']
outfile = '~/Astro/699-1/table1.tex'
cap = 'Properties of New Discoveries \label{tbl2}'

tablegen, objlist, outfile, /ps1ra, /ps1dec, /gal, /pm, /tmdes, /wdes, /ps1phot, /tmphot, /mkophot, $
          /wphot, /h2oj, /ch4j, /h2oh, /ch4h, /h2ok, /ch4k, /spt, /pdist, /vtan, $;/teff, /lum, $
          caption=cap, /scriptsize;, /rotate

;  CALLS
;      trim
;
;  INPUTS
;      OBJLIST - List of object PS1 names, used to generate the table.
;      OUTFILE - Path for file output.  If not set, output will be to screen.
;
;  TABLE KEYWORDS
;      PS1RA - PS1 R.A in decimal degrees and hms
;      PS1DEC - PS1 Dec
;      GAL - Galactic coordinates, in decimal degrees
;      PM - 
;      TMDES - 2MASS designation
;      WDES - WISE designation
;      PS1PHOT - 
;      TMPHOT - 2MASS photometry (J H Ks)
;      MKOPHOT - 
;      WPHOT - 
;      H2OJ - 
;      CH4J - 
;      H2OH - 
;      CH4H - 
;      H2OK - 
;      CH4K - 
;      SPT - 
;      PDIST - Photometric distance, in pc
;      VTAN - 
;      TEFF - 
;      LUM - 
;
;  OPTIONAL KEYWORDS
;      DATAFILE - Location of the save file containing the data structure.
;                 Default: '~/Astro/699-1/lttrans/plotting/lttrans.sav'
;      CAPTION - Caption for the table
;      ROTATE - Rotate the table to landscape orientation
;      SCRIPTSIZE - Table font size = scriptsize


END
