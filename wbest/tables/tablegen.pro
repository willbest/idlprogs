;; objlist = ['PSO J007.7921+57.8267', 'PSO J307.6784+07.8263', 'PSO J339.0734+51.0978']
;; rotate=1
;; caption='Properties of New Discoveries \label{tbl2}'
;; scriptsize=1
;; ps1ra=1
;; ps1dec=1
;; gal=1
;; pm=1
;; tmdes=1
;; wdes=1
;; ps1phot=1
;; tmphot=1
;; mkophot=1
;; wphot=1
;; h2oj=1
;; ch4j=1
;; h2oh=1
;; ch4h=1
;; h2ok=1
;; ch4k=1
;; spt=1
;; pdist=1
;; vtan=1
;teff=1
;lum=1
PRO TABLEGEN, OBJLIST, OUTFILE, DATAFILE=datafile, CAPTION=caption, ROTATE=rotate, SCRIPTSIZE=scriptsize, $
              PS1RA=ps1ra, PS1DEC=ps1dec, GAL=gal, PM=pm, TMDES=tmdes, WDES=wdes, PS1PHOT=ps1phot, $
              TMPHOT=tmphot, MKOPHOT=mkophot, WPHOT=wphot, H2OJ=h2oj, CH4J=ch4j, H2OH=h2oh, CH4H=ch4h, $
              H2OK=h2ok, CH4K=ch4k, SPT=spt, PDIST=pdist, VTAN=vtan, TEFF=teff, LUM=lum

;+
;  Generate latex deluxetable code for inclusion in papers.
;
;  HISTORY
;  Written by Will Best (IfA), 02/07/2013
;
;  USE
;      tablegen, objlist [, OUTFILE, DATAFILE=datafile, CAPTION=caption, ROTATE=rotate, $
;                SCRIPTSIZE=scriptsize, PS1RA=ps1ra, PS1DEC=ps1dec, GAL=gal, PM=pm, $
;                TMDES=tmdes, WDES=wdes, PS1PHOT=ps1phot, TMPHOT=tmphot, MKOPHOT=mkophot, $
;                WPHOT=wphot, H2OJ=h2oj, CH4J=ch4j, H2OH=h2oh, CH4H=ch4h, H2OK=h2ok, $
;                CH4K=ch4k, SPT=spt, PDIST=pdist, VTAN=vtan, TEFF=teff, LUM=lum]
;
;  CALLS
;      trim
;
;  INPUTS
;      OBJLIST - List of object PS1 names, used to generate the table.
;      OUTFILE - Path for file output.  If not set, output will be to screen.
;
;  TABLE KEYWORDS
;      PS1RA - PS1 R.A in decimal degrees and hms
;      PS1DEC - PS1 Dec
;      GAL - Galactic coordinates, in decimal degrees
;      PM - 
;      TMDES - 2MASS designation
;      WDES - WISE designation
;      PS1PHOT - 
;      TMPHOT - 2MASS photometry (J H Ks)
;      MKOPHOT - 
;      WPHOT - 
;      H2OJ - 
;      CH4J - 
;      H2OH - 
;      CH4H - 
;      H2OK - 
;      CH4K - 
;      SPT - 
;      PDIST - Photometric distance, in pc
;      VTAN - 
;      TEFF - 
;      LUM - 
;
;  OPTIONAL KEYWORDS
;      DATAFILE - Location of the save file containing the data structure.
;                 Default: '~/Astro/699-1/lttrans/plotting/lttrans.sav'
;      CAPTION - Caption for the table
;      ROTATE - Rotate the table to landscape orientation
;      SCRIPTSIZE - Table font size = scriptsize
;-

; Check for correct number of input parameters
if n_params() lt 1 then begin
    print
    print, 'SYNTAX: tablegen, objlist [, OUTFILE, DATAFILE=datafile, CAPTION=caption, ROTATE=rotate'
    print, '          SCRIPTSIZE=scriptsize, PS1RA=ps1ra, PS1DEC=ps1dec, GAL=gal, PM=pm, '
    print, '          TMDES=tmdes, WDES=wdes, PS1PHOT=ps1phot, TMPHOT=tmphot, MKOPHOT=mkophot, '
    print, '          WPHOT=wphot, H2OJ=h2oj, CH4J=ch4j, H2OH=h2oh, CH4H=ch4h, H2OK=h2ok, '
    print, '          CH4K=ch4k, SPT=spt, PDIST=pdist, VTAN=vtan, TEFF=teff, LUM=lum ]'
    print
    return
endif

nobj = n_elements(objlist)

; Load L/T search objects.
if n_elements(datafile) eq 0 then datafile = '~/Astro/699-1/lttrans/plotting/lttrans.sav'
restore, datafile

;obj = lttrans[where(lttrans.name eq objlist)]
match, lttrans.name, objlist, objind
obj = lttrans[objind]

; DeluxeTable set-up
tbl = '\begin{deluxetable}{l'+strjoin(replicate('c',nobj))+'}'
tbl = [tbl, '\tablecolumns{'+trim(nobj+1)+'}']
tbl = [tbl, '\tablewidth{0pc}']
if keyword_set(rotate) then tbl = [tbl, '\rotate']
if keyword_set(scriptsize) then tbl = [tbl, '\tabletypesize{\scriptsize}']
if keyword_set(caption) then tbl = [tbl, '\tablecaption{'+caption+'}']

; Column Headings
tbl = [tbl, '\tablehead{   % column headings']
tbl = [tbl, '  \colhead{Property} &']
if nobj ge 2 then tbl = [tbl, '  \colhead{'+objlist[0:nobj-2]+'} &']
tbl = [tbl, '  \colhead{'+objlist[nobj-1]+'}']
tbl = [tbl, '}']

; Start the Table
tbl = [tbl, '\startdata']
tbl = [tbl, '\cutinhead{Astrometry}']

; PS1 coordinates
if keyword_set(ps1ra) then begin
    row1 = 'PS1 R.A. (J2000) & '
    row2 = ' & '
    if nobj ge 2 then begin
        row1 = row1 + strjoin('$'+strtrim(string(obj[0:nobj-2].ra,format='(f8.4)'),1)+'^\circ$ & ')
        row2 = row2 + strjoin('$'+strmid(obj[0:nobj-2].rahms,0,2)+'^h'+strmid(obj[0:nobj-2].rahms,3,2)+'^m'+$
               strmid(obj[0:nobj-2].rahms,6,5)+'^s$ & ')
    endif
    row1 = row1 + '$'+strtrim(string(obj[nobj-1].ra,format='(f8.4)'),1)+'^\circ$ \\'
    row2 = row2 + '$'+strmid(obj[nobj-1].rahms,0,2)+'^h'+strmid(obj[nobj-1].rahms,3,2)+'^m'+$
               strmid(obj[nobj-1].rahms,6,5)+'^s$ \\'
    tbl = [tbl, row1, row2]
endif
if keyword_set(ps1dec) then begin
    row1 = 'PS1 Dec. (J2000) & '
    row2 = ' & '
    if nobj ge 2 then begin
        row1 = row1 + strjoin('$'+textsign(obj[0:nobj-2].dec,/pos)+$
                              strtrim(string(obj[0:nobj-2].dec,format='(f8.4)'),1)+'^\circ$ & ')
        row2 = row2 + strjoin('$'+strmid(obj[0:nobj-2].decdms,0,3)+'^\circ'+strmid(obj[0:nobj-2].decdms,4,2)+"'"+$
               strmid(obj[0:nobj-2].decdms,7,5)+"''$ & ")
    endif
    row1 = row1 + '$'+textsign(obj[nobj-1].dec,/pos)+strtrim(string(obj[nobj-1].dec,format='(f8.4)'),1)+'^\circ$ \\'
    row2 = row2 + '$'+strmid(obj[nobj-1].decdms,0,3)+'^\circ'+strmid(obj[nobj-1].decdms,4,2)+"'"+$
               strmid(obj[nobj-1].decdms,7,5)+"''$ \\"
    tbl = [tbl, row1, row2]
endif

; Galactic coordinates
if keyword_set(gal) then begin
    row1 = 'Galactic longitude & '
    row2 = 'Galactic latitude & '
    if nobj ge 2 then begin
        row1 = row1 + strjoin('$'+strtrim(string(obj[0:nobj-2].glon,format='(f8.4)'),1)+'^\circ$ & ')
        row2 = row2 + strjoin('$'+strtrim(string(obj[0:nobj-2].glat,format='(f8.4)'),1)+'^\circ$ & ')
    endif
    row1 = row1 + '$'+strtrim(string(obj[nobj-1].glon,format='(f8.4)'),1)+'^\circ$ \\'
    row2 = row2 + '$'+strtrim(string(obj[nobj-1].glat,format='(f8.4)'),1)+'^\circ$ \\'
    tbl = [tbl, row1, row2]
endif

; Proper motion
if keyword_set(pm) then begin
    row1 = '$\mu_\alpha$\,cos\,$\delta$ (mas\,yr$^{-1}$) & '
    row2 = '$\mu_\delta$ (mas\,yr$^{-1}$) & '
    if nobj ge 2 then begin
        row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].ura)+'\pm'+trim(obj[0:nobj-2].uraerr)+'$ & ')
        row2 = row2 + strjoin('$'+trim(obj[0:nobj-2].udec)+'\pm'+trim(obj[0:nobj-2].udecerr)+'$ & ')
    endif
    row1 = row1 + '$'+trim(obj[nobj-1].ura)+'\pm'+trim(obj[nobj-1].uraerr)+'$ \\'
    row2 = row2 + '$'+trim(obj[nobj-1].udec)+'\pm'+trim(obj[nobj-1].udecerr)+'$ \\'
    tbl = [tbl, row1, row2]
endif

; 2MASS designation
if keyword_set(tmdes) then begin
    row1 = '2MASS Designation & '
    if nobj ge 2 then row1 = row1 + strjoin(strmid(obj[0:nobj-2].name_other, $
                                                   1#strpos(obj[0:nobj-2].name_other,'J'),50)+' & ')
    row1 = row1 + strmid(obj[nobj-1].name_other,strpos(obj[nobj-1].name_other,'J'),50)+' \\'
    tbl = [tbl, row1]
endif

; WISE designation
if keyword_set(wdes) then begin
    row1 = 'WISE Designation & '
    if nobj ge 2 then row1 = row1 + strjoin(strmid(obj[0:nobj-2].name_wise, $
                                                   1#strpos(obj[0:nobj-2].name_wise,'J'),50)+' & ')
    row1 = row1 + strmid(obj[nobj-1].name_wise,strpos(obj[nobj-1].name_wise,'J'),50)+' \\'
    tbl = [tbl, row1]
endif

tbl = [tbl, '\cutinhead{Photometry}']

; PS1 photometry
if keyword_set(ps1phot) then begin
;    row1 = 'PS1 $i$ (AB mag) & '
    row2 = 'PS1 $z$ (AB mag) & '
    row3 = 'PS1 $y$ (AB mag) & '
    if nobj ge 2 then begin
;        row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].i,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].i_err,'(f4.2)')+'$ & ')
        row2 = row2 + strjoin('$'+trim(obj[0:nobj-2].z,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].z_err,'(f4.2)')+'$ & ')
        row3 = row3 + strjoin('$'+trim(obj[0:nobj-2].y,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].y_err,'(f4.2)')+'$ & ')
    endif
;    row1 = row1 + '$'+trim(obj[nobj-1].i,'(f5.2)')+'\pm'+trim(obj[nobj-1].i_err,'(f4.2)')+'$ \\'
    row2 = row2 + '$'+trim(obj[nobj-1].z,'(f5.2)')+'\pm'+trim(obj[nobj-1].z_err,'(f4.2)')+'$ \\'
    row3 = row3 + '$'+trim(obj[nobj-1].y,'(f5.2)')+'\pm'+trim(obj[nobj-1].y_err,'(f4.2)')+'$ \\[9pt]'
    tbl = [tbl, row2, row3];row1, row2, row3]
endif

; 2MASS photometry
if keyword_set(tmphot) then begin
    row1 = '2MASS $J$ (mag) & '
    row2 = '2MASS $H$ (mag) & '
    row2a = '$'+trim(obj.H,'(f5.2)')+'\pm'+trim(obj.H_err,'(f4.2)')+'$'
    nan2 = where(finite(obj.H, /nan))
    if nan2[0] ne -1 then row2a[nan2] = '\nodata'
    row3 = '2MASS $K_s$ (mag) & '
    if nobj ge 2 then begin
        row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].J,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].J_err,'(f4.2)')+'$ & ')
        row2 = row2 + strjoin(row2a[0:nobj-2]+' & ')
;        row2 = row2 + strjoin('$'+trim(obj[0:nobj-2].H,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].H_err,'(f4.2)')+'$ & ')
        row3 = row3 + strjoin('$'+trim(obj[0:nobj-2].K,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].K_err,'(f4.2)')+'$ & ')
    endif
    row1 = row1 + '$'+trim(obj[nobj-1].J,'(f5.2)')+'\pm'+trim(obj[nobj-1].J_err,'(f4.2)')+'$ \\'
    row2 = row2 + row2a[nobj-1]+' \\'
    row3 = row3 + '$'+trim(obj[nobj-1].K,'(f5.2)')+'\pm'+trim(obj[nobj-1].K_err,'(f4.2)')+'$ \\[9pt]'
    tbl = [tbl, row1, row2, row3]
endif

; MKO photometry
if keyword_set(mkophot) then begin
    row1 = 'MKO $Y$ (mag) & '
    row1a = '$'+trim(obj.YM,'(f5.2)')+'\pm'+trim(obj.YM_err,'(f4.2)')+'$'
    nan1 = where(finite(obj.YM, /nan))
    if nan1[0] ne -1 then row1a[nan1] = '\nodata'
    row2 = 'MKO $J$ (mag) & '
    row3 = 'MKO $H$ (mag) & '
    row3a = '$'+trim(obj.HM,'(f5.2)')+'\pm'+trim(obj.HM_err,'(f4.2)')+'$'
    nan3 = where(finite(obj.HM, /nan))
    if nan3[0] ne -1 then row3a[nan3] = '\nodata'
    if nobj ge 2 then begin
        row1 = row1 + strjoin(row1a[0:nobj-2]+' & ')
        row2 = row2 + strjoin('$'+trim(obj[0:nobj-2].JM,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].JM_err,'(f4.2)')+'$ & ')
        row3 = row3 + strjoin(row3a[0:nobj-2]+' & ')
    endif
    row1 = row1 + row1a[nobj-1]+' \\'
    row2 = row2 + '$'+trim(obj[nobj-1].JM,'(f5.2)')+'\pm'+trim(obj[nobj-1].JM_err,'(f4.2)')+'$ \\'
    row3 = row3 + row3a[nobj-1]+' \\[9pt]'
    tbl = [tbl, row1, row2, row3]
endif

; WISE photometry
if keyword_set(wphot) then begin
    row1 = 'WISE $W1$ (mag) & '
    row2 = 'WISE $W2$ (mag) & '
    row3 = 'WISE $W3$ (mag) & '
    if nobj ge 2 then begin
        row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].W1,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].W1_err,'(f4.2)')+'$ & ')
        row2 = row2 + strjoin('$'+trim(obj[0:nobj-2].W2,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].W2_err,'(f4.2)')+'$ & ')
        row3 = row3 + strjoin('$'+trim(obj[0:nobj-2].W3,'(f5.2)')+'\pm'+trim(obj[0:nobj-2].W3_err,'(f4.2)')+'$ & ')
    endif
    row1 = row1 + '$'+trim(obj[nobj-1].W1,'(f5.2)')+'\pm'+trim(obj[nobj-1].W1_err,'(f4.2)')+'$ \\'
    row2 = row2 + '$'+trim(obj[nobj-1].W2,'(f5.2)')+'\pm'+trim(obj[nobj-1].W2_err,'(f4.2)')+'$ \\'
    row3 = row3 + '$'+trim(obj[nobj-1].W3,'(f5.2)')+'\pm'+trim(obj[nobj-1].W3_err,'(f4.2)')+'$ \\'
    tbl = [tbl, row1, row2, row3]
endif

tbl = [tbl, '\cutinhead{Spectral Indices}']

; Spectral indices
if keyword_set(h2oj) then begin
    row1 = 'H$_2$O-$J$ & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].h2oj,'(f5.3)')+'$ ('+obj[0:nobj-2].h2ojt+') & ')
    row1 = row1 + '$'+trim(obj[nobj-1].h2oj,'(f5.3)')+'$ ('+obj[nobj-1].h2ojt+') \\'
    tbl = [tbl, row1]
endif

if keyword_set(ch4j) then begin
    row1 = 'CH$_4$-$J$ & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].ch4j,'(f5.3)')+'$ ('+obj[0:nobj-2].ch4jt+') & ')
    row1 = row1 + '$'+trim(obj[nobj-1].ch4j,'(f5.3)')+'$ ('+obj[nobj-1].ch4jt+') \\'
    tbl = [tbl, row1]
endif

if keyword_set(h2oh) then begin
    row1 = 'H$_2$O-$H$ & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].h2oh,'(f5.3)')+'$ ('+obj[0:nobj-2].h2oht+') & ')
    row1 = row1 + '$'+trim(obj[nobj-1].h2oh,'(f5.3)')+'$ ('+obj[nobj-1].h2oht+') \\'
    tbl = [tbl, row1]
endif

if keyword_set(ch4h) then begin
    row1 = 'CH$_4$-$H$ & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].ch4h,'(f5.3)')+'$ ('+obj[0:nobj-2].ch4ht+') & ')
    row1 = row1 + '$'+trim(obj[nobj-1].ch4h,'(f5.3)')+'$ ('+obj[nobj-1].ch4ht+') \\'
    tbl = [tbl, row1]
endif

if keyword_set(ch4k) then begin
    row1 = 'CH$_4$-$K$ & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].ch4k,'(f5.3)')+'$ ('+obj[0:nobj-2].ch4kt+') & ')
    row1 = row1 + '$'+trim(obj[nobj-1].ch4k,'(f5.3)')+'$ ('+obj[nobj-1].ch4kt+') \\'
    tbl = [tbl, row1]
endif

if keyword_set(h2ok) then begin
    row1 = 'H$_2$O-$K$ & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].h2ok,'(f5.3)')+'$ ('+obj[0:nobj-2].h2okt+') & ')
    row1 = row1 + '$'+trim(obj[nobj-1].h2ok,'(f5.3)')+'$ ('+obj[nobj-1].h2okt+') \\'
    tbl = [tbl, row1]
endif

tbl = [tbl, '\cutinhead{Physical Properties}']

; Spectral Type
if keyword_set(spt) then begin
    row1 = 'Spectral Type (NIR) & '
    if nobj ge 2 then row1 = row1 + strjoin(obj[0:nobj-2].sptstr+' & ')
    row1 = row1 + obj[nobj-1].sptstr+' \\'
    tbl = [tbl, row1]
endif

; Photometric distance
if keyword_set(pdist) then begin
    row1 = 'Photometric Distance (pc) & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].dist,'(f5.1)')+$
                                            '\pm'+trim(obj[0:nobj-2].disterr,'(f4.1)')+'$ & ')
    row1 = row1 + '$'+trim(obj[nobj-1].dist,'(f5.1)')+'\pm'+trim(obj[nobj-1].disterr,'(f4.1)')+'$ \\'
    tbl = [tbl, row1]
endif

; Tangential velocity
if keyword_set(vtan) then begin
    row1 = '$v_\mathrm{tan}$ (km\,s$^{-1}$) & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].vtan)+'\pm'+trim(obj[0:nobj-2].vtanerr)+'$ & ')
    row1 = row1 + '$'+trim(obj[nobj-1].vtan)+'\pm'+trim(obj[nobj-1].vtanerr)+'$ \\'
    tbl = [tbl, row1]
endif

; Effective temperature
if keyword_set(Teff) then begin
    row1 = '$T_\mathrm{eff}$ (K) & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].teff)+'\pm100$ & ')
    row1 = row1 + '$'+trim(obj[nobj-1].teff)+'\pm100$ \\'
    tbl = [tbl, row1]
endif

; Luminosity
if keyword_set(lum) then begin
    row1 = '$\log L/L_\odot$ & '
    if nobj ge 2 then row1 = row1 + strjoin('$'+trim(obj[0:nobj-2].L,'(f5.2)')+'$ & ')
    row1 = row1 + '$'+trim(obj[nobj-1].L,'(f5.2)')+'$ \\'
    tbl = [tbl, row1]
endif

; Wrap it up
tbl = [tbl, '\enddata']
tbl = [tbl, '\end{deluxetable}']


; OUTPUT
if n_elements(outfile) eq 0 then begin
; output to screen
    print
    print, tbl, format='(a)'
    print
endif else begin
; output to file
    forprint, tbl, textout=outfile, format='(a)', /silent, /nocom, width=1000
endelse


END
