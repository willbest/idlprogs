targlist='~/Astro/observing/proposals/IRTF/2013B.csv'
timelist='~/Astro/observing/proposals/IRTF/2013B_time.txt'
outfile='~/Astro/observing/proposals/IRTF/2013B_list.tex'
;caption=''
;scriptsize=1
;PRO IRTFLIST

;+
;  Generate latex deluxetable list of targets for inclusion in IRTF proposals.
;
;  HISTORY
;  Written by Will Best (IfA), 03/31/2013
;
;  CALLS
;      trim
;
;  INPUTS
;      OBJLIST - List of object PS1 names, used to generate the table.
;      OUTFILE - Path for file output.  If not set, output will be to screen.
;
;  OPTIONAL KEYWORDS
;      DATAFILE - Location of the save file containing the data structure.
;                 Default: '~/Astro/699-1/lttrans/plotting/lttrans.sav'
;      CAPTION - Caption for the table
;      ROTATE - Rotate the table to landscape orientation
;      SCRIPTSIZE - Table font size = scriptsize
;-

readcol, targlist, id, ra, dec, Jmag, format='a,x,x,x,a,a,f,x,x', comment='#', delim=','
readcol, timelist, tint, format='x,x,i,x,x,x,x', comment='#'
nobj = n_elements(id)

; DeluxeTable set-up
tbl = '\centering'
tbl = [tbl, '\begin{tabular}{l c c c l}']
tbl = [tbl, '\hline\hline']
if keyword_set(scriptsize) then tbl = [tbl, '\tabletypesize{\scriptsize}']
if keyword_set(caption) then tbl = [tbl, '\tablecaption{'+caption+'}']

; Column Headings
tbl = [tbl, 'Object & Coordinates & Magnitude & $t_{\rm int}$ & Comments \\']
tbl = [tbl, ' & & & (min) & \\']
tbl = [tbl, '\hline']

; Make the columns
id = 'PW-'+strmid(id,0,1#strlen(id)-6)+'-'+strmid(id,1#strlen(id)-6,6)
coord = '${\rm '+strmid(ra,0,2)+'^h'+strmid(ra,3,2)+'^m}$ $'+strmid(dec,0,3)+'^\circ'+strmid(dec,4,2)+"'$"
mag = string(Jmag, format='("$J=",f4.1,"$")')
tint = trim(tint)

; Make the rows
tbl = [tbl, id+' & '+coord+' & '+mag+' & '+tint+' & \\']

; Wrap it up
tbl = [tbl, '\hline']
tbl = [tbl, '\end{tabular}']


; OUTPUT
if n_elements(outfile) eq 0 then begin
; output to screen
    print
    print, tbl, format='(a)'
    print
endif else begin
; output to file
    forprint, tbl, textout=outfile, format='(a)', /silent, /nocom, width=1000
endelse


END
