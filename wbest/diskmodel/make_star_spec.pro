pro make_star_spec,star=star,silent=silent
;
; read in kurucz model stellar spectrum
; and interpolate/extrapolate onto the wavelength
; grid for radmc3d.  Write out stars.inp file.
;
; hacked from radmc3d herbig example
; --------------------------------------------------------------
;@natural_constants.pro
;@natconst.pro
if keyword_set(silent) then silent = 1 else silent = 0

;
; Natural constants
;
LS       = 3.86d33
RS       = 6.96d10
MS       = 1.99d33
ss       = 5.671d-5
GG       = 6.673d-8
cc       = 2.9979d10
pc       = 3.08572d18
hh       = 6.62608d-27
kb       = 1.3806d-16

;
; For safety: close all
;
close,/all

;
; Read the frequency table
;
openr,1,'wavelength_micron.inp'
nlam = 0
readf,1,nlam
lambda = dblarr(nlam)
readf,1,lambda
close,1
freq = 1d4*cc/lambda


;
; stellar parameters
;
Lstar = star.l*LS
Tstar = star.t
Rstar = sqrt(Lstar/(4d*!dpi*ss))/Tstar^2
Mstar = star.m*MS
logg  = star.g

case 1 of
  (Tstar lt 3625): Tkurucz = 3500
  (Tstar ge 3625 and Tstar lt 3875): Tkurucz = 3750
  (Tstar ge 3875 and Tstar lt 4125): Tkurucz = 4000
  (Tstar ge 4125 and Tstar lt 4375): Tkurucz = 4250
  (Tstar ge 4375 and Tstar lt 4625): Tkurucz = 4500
  (Tstar ge 4625 and Tstar lt 4875): Tkurucz = 4750
  (Tstar ge 4875 and Tstar lt 5125): Tkurucz = 5000
  (Tstar ge 5125 and Tstar lt 5375): Tkurucz = 5250
  (Tstar ge 5375 and Tstar lt 5625): Tkurucz = 5500
  (Tstar ge 5625 and Tstar lt 5875): Tkurucz = 5750
  (Tstar ge 5875 and Tstar lt 6125): Tkurucz = 6000
else: print,'Kurucz Teff case statement out of range'
endcase
Rkurucz = sqrt(Lstar/(4.*!DPI*ss*double(Tkurucz)^4))

case 1 of
  (logg lt 2.75): print,'*** Really low gravity!'
  (logg ge 2.75 and logg lt 3.25): gkurucz = 3.0
  (logg ge 3.25 and logg lt 3.75): gkurucz = 3.5
  (logg ge 3.75 and logg lt 4.25): gkurucz = 4.0
  (logg gt 4.25): print,'*** Really high gravity!'
else: print,'Kurucz logg case statement out of range'
endcase


;
; Read in closest fit Kurucz model
;
kmodel = atlas(Tkurucz,gkurucz)
lam = kmodel[0,*]*1e-3         ; convert from nano to micron
flux = kmodel[1,*]

nu = 1d4*cc/lam
s1 = nu
s2 = flux
LL = int_tabulated(s1,s2,/sort,/double)
lnu = flux*Lstar/LL
s1 = nu
s2 = lnu
LL1 = int_tabulated(s1,s2,/double,/sort)
lnusmooth = lnu

;
; plot the Kurucz model unless /silent keyword set
;
if silent ne 1 then begin
   dumm=nu*lnusmooth/LS
   plot,3d14/nu,dumm $
       ,xra=[0.01,10000],/xlog $
       ,yrange=[1d-14,1]*max(dumm),/ylog
endif

;
; Interpolate on the coarse grid for the simulations
; for lambda < 100 microns
; and exrapolate as a blackbody beyond
; (assumes that the wavelength grid is sorted)
;
lint = interpol(lnusmooth,nu,freq)
longlam = where(freq lt 1d4*cc/100d0)
l0 = lint[longlam[0]-1]
n0 = freq[longlam[0]-1]
lint[longlam] = l0*(freq[longlam]/n0)^2

;
; Overplot this interpolated spectrum
;
if silent ne 1 then begin
   oplot,3d14/freq,freq*lint/LS,psy=2
endif

;
; Overplot a blackbody
;
if silent ne 1 then begin
   bb = 2.0*hh*freq^3 / cc^2 / (exp(hh*freq/(kb*Tkurucz)) - 1.0)
   oplot,3d14/freq,freq*bb*4*!DPI^2*Rkurucz^2/LS,line=1
endif

;
; Write the spectrum normalized to 1pc
; to stars.inp
;
pstar = [0,0,0]       ; assumed that star is at center
;
openw,2,'stars.inp'
printf,2,2
printf,2,1,nlam
printf,2,' '
printf,2,Rstar,Mstar,pstar[0],pstar[1],pstar[2]
printf,2,' '
for ilam=0,nlam-1 do printf,2,lambda[ilam]
factor=1.d0/(4*!dpi*pc^2)
for ilam=0,nlam-1 do printf,2,lint[ilam]*factor
close,2

;
; Check the error
;
s1 = freq
s2 = lint
LL1 = int_tabulated(s1,s2,/double,/sort)
;print, "make_star_spec: Kurucz model relative error = ",100*abs(LL1/Lstar-1.d0),"%"

end
