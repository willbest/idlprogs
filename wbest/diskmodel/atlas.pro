;This function returns one of the Kurucz model atmospheres, 
;given a temp and log g, used in Cloudy.
;
;Program started on 10-12-2000 by SRM.

function atlas,t,g
openr,1,'~/Astro/models/starspectrum/kurucz.ascii'
data=dblarr(1221,413)
readf,1,data
close,1

starlabels={num:0,teff:'',temp:0.,grav:'',logg:0.,j1:''}
starlabels=replicate(starlabels,412)
a=''
openr,1,'~/Astro/models/starspectrum/kurucz.list'
readf,1,a
readf,1,starlabels,format='(i5,A5,f8.0,a9,f8.5,a52)'
close,1

ilook=where(starlabels.temp eq t and starlabels.logg eq g)
if ilook(0) eq -1 then begin 
getclose=where(abs(starlabels.temp - t) le 5000.)
for i=0,n_elements(getclose)-1 do print,getclose(i),starlabels(getclose(i)).temp,starlabels(getclose(i)).logg
read,'try one of these',retry
ilook=retry
endif

atm=[transpose(data(*,0)),transpose(data(*,ilook))]
return,atm
end
