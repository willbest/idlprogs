nsets=2
;PRO GET_TEMP_RESULTS, FILEPATH=filepath, OUTPATH=outpath, SETNAMES=setnames

;+
;  Reads the output temperatures from the make_temp_... simulations.
;  Specifically, obtains the report.txt file(s) from the top of the output
;  directory tree.  
;  Obtains the line_results_... files from the directories, and bundles them
;  into a tarball in outpath.
;  
;  HISTORY
;  Written by Will Best (IfA), 2/12/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/tempgrid/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      OUTPATH - folder to receive the report.txt and line_results files.
;                Default: ~/tempgrid/
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;-

; mark the start time
starttime = systime()

; set up generic paths
if not keyword_set(filepath) then filepath = '~/tempgrid/'
if not keyword_set(outpath) then outpath = '~/tempgrid/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1

; start the loops
cd, outpath
for i=0, nsets-1 do begin

; set up paths for this loop
    readpath = setnames+trim(i+1)+'_output/'

; build the list of files to read into tarball
    print, 'Building list of files #'+trim(i+1)
    filelist = readpath+'report.txt'
    
    readcol, filelist, run, star, disk, $
             delim=' ', comment='#', format='a,a,a'
    for j=0, n_elements(run)-1 do filelist = [filelist, $
          readpath+star[j]+disk[j]+'dust_temperature'+run[j]+'.dat' ]

; make the tarball
    print, 'Making tarball #'+trim(i+1)+' of files to copy'
    tarcom = 'tar -cf tempout'+trim(i+1)+'.tar '+strjoin(filelist, ' ')
;    copcom = readpath+'lineout '+writepath
    openw, x, 'tarcom'+trim(i+1)+'.txt', /get_lun
    printf, x, tarcom
    close, x
    spawn, 'source tarcom'+trim(i+1)+'.txt'

endfor

print
print, 'Done!!'
print
print, 'Start time: '+starttime
print, 'Finish time: '+systime()
print

END

