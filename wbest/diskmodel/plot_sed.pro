pro plot_sed,filename=filename,incl=incl,src=src,disk=disk,PDF=pdf,ps=ps

asp=8.5/11.0
if keyword_set(ps) or keyword_set(pdf) then begin
  if n_elements(filename) eq 0 then filename='idl.ps'
  set_plot,'ps',/interpolate
  device,filename=filename,bits_per_pixel=8 $
        ,xsize=11.0,ysize=11.0*asp,xoff=0.0,yoff=11.0 $
        ,/inches,/land,/encapsulated,/color
  cs=1.7
  ct=4
endif else begin
  device,decomposed=0
  xsize=1000
  window,0,xsize=xsize,ysize=xsize*asp
  cs=1.8
  ct=1
endelse
pos=[0.17,0.15,0.9,0.92]
tic=0.04

ncol=250
loadct,0,ncolors=ncol
tvlct,fsc_color('Red',/triple),251
tvlct,fsc_color('Dark Grey',/triple),252
tvlct,fsc_color('Green',/triple),253
tvlct,fsc_color('Royal Blue',/triple),254
tvlct,fsc_color('White',/triple),255
red=251
grey=252
green=253
blue=254
white=255


; SED
readsed,'sed.txt',Av,lambda,Fnu
; get Kurucz model and plot star
phi=findgen(41)*!pi/20
usersym,cos(phi),sin(phi),/fill
plot,lambda,Fnu $
    ,position=pos $
    ,xra=[0.1,5000.0],xsty=1,/xlog $
    ,yra=[0.1,1e4],ysty=1,/ylog $
    ,xtitle='!7k !6(!7l!6m)' $
    ,ytitle='!6F!D!7m!N !6(mJy)' $
    ,xthick=ct,ythick=ct $
    ,charthick=ct,charsize=cs $
    ,/nodata
oir=where(lambda lt 3.0,complement=longwave)
lambda_ext=lambda[oir]
Fnu_ext=Fnu[oir]
EBV=Av/3.1
ccm_unred,10^4*lambda_ext,Fnu_ext,EBV,Fnu_unred
;plots,lambda_ext,Fnu_ext,psym=6
;plots,lambda_ext,Fnu_unred,psym=8
;plots,lambda[longwave],Fnu[longwave],psym=8

; plot model SED
; note scaling by 1d26 for mJy and dividing by distance^2 in pc
readcol,'sed.out',format='(f,f)',lam_fit,flux_fit,skipline=3
plots,lam_fit[0],flux_fit[0]
oplot,lam_fit,flux_fit*1d26/140.^2,thick=5,color=blue

; plot labels
xyouts,0.22,0.82,/normal,align=0,charthick=ct,charsize=1.6*cs,src
xyouts,0.68,0.36,/normal,align=0,charthick=ct,charsize=cs,string(format='("M =",e9.2," M!D!9n!6!N")',disk.m)
xyouts,0.68,0.32,/normal,align=0,charthick=ct,charsize=cs,string(format='("!7c!6 = ",f6.3)',disk.gamma)
xyouts,0.68,0.28,/normal,align=0,charthick=ct,charsize=cs,string(format='("R!Dc!N = ",f5.1," AU")',disk.rc)
xyouts,0.68,0.24,/normal,align=0,charthick=ct,charsize=cs,string(format='("H!D100!N = ",f4.1," AU")',disk.H100)
xyouts,0.68,0.20,/normal,align=0,charthick=ct,charsize=cs,string(format='("h =",f6.3)',disk.h)
if n_elements(incl) gt 0 then begin
    dgdg = cgsymbol('deg')
    xyouts,0.68,0.40,/normal,align=0,charthick=ct,charsize=cs,string(format='("i =",f5.1,a)',incl,dgdg)
endif

if keyword_set(ps) or keyword_set(pdf) then begin
  device,/close
  set_plot,'x'
  !p.font=-1
  print,"Created color postscript file: "+filename
endif

if keyword_set(pdf) then begin
    if keyword_set(ps) then cgps2pdf, filename, /showcmd else cgps2pdf, filename, /showcmd, /delete_ps
endif

return
end
