@readradmc.pro
@radmc3dfits.pro

pro makevis
;
; these are the salient parts of the code to create a
; visibility profile of a radmc3d model
; the model is assumed spherically symmetric, centered
; at the origin, and noiseless (I don't radially average
; but just take a cut along the u-axis)
;
; the units are klambda in uv-distance
; and mJy in flux
;
; 11/08/12  jpw
; ---------------------------------------------------------



; make face on, circular image from -500 to 500 AU in x,y
npix = 256
lambda_microns = 1333.3
cmd_image = string(format='("radmc3d image lambda ",f6.1," npix ",i0," sizeau 1000.0 nostar noline")',lambda_microns,npix)
spawn,cmd_image


;;;;; scale flux to Taurus and fourier transform
im = readimage()
dx = im.sizepix_x                 ; cm
dist = 140.0                      ; distance to Taurus in pc
omega = dx*dx/(dist*pc)^2         ; cell size in ster
Fim = im.image*omega*1d26         ; image flux in mJy
Ffft = fft(shift(Fim,npix/2,npix/2),-1,/double)
df = 1./(npix*dx)                 ; units cm-1
uv = findgen(npix/2)*df

; uv distance defined such that lambda/R = theta = dx/dist
; normalize to klambda
Rfft = dist*pc*uv/1d3

Freal = real_part(Ffft)
Freal = Freal[0:npix/2-1,0]       ; only use first half of x-axis (symmetric)

; scale so that zero point in FT is total flux
Freal = Freal * total(Fim)/Freal[0]


return
end
