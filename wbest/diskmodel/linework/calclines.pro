PRO CALCLINES, INCL, GD=gd, WORKING=working, _EXTRA=extrakey

;+
; Create CO number density file for varying g/d ratios
; from dust density, temperature and calculate line spectra.
;
;  HISTORY
;  10/29/12 (jpw): hacked from what was plot_spectra.pro
;  11/27/12 (WB):  added INCL as a parameter
;                  added GD and WORKING keywords
;  12/11/12 (WB):  Added _EXTRA keyword
;                  Moved the call to plot_profile from make_cont_lines_core to
;                  this level, to incorporate the varying A_V_dissoc, t_freeze
;                  and gas-to-dust ratio in the profiles.
;
;  INPUTS
;      INCL - Disk's inclination angle.  Default = 60 degrees.
;
;  KEYWORDS
;      GD - Scalar or vector of gas-to-dust ratios to be used in calculating the
;           line spectra.
;           Default: [1,3,10,30,100,300]
;      WORKING - working directory to point to
;                Default: '~/Astro/699-2/radmc_output'
;
;      _EXTRA can be used to pass A_V_dissoc and t_freeze through calclines.pro to
;      plot_profile.pro and make_co_profile.pro.
;-

;@stardisk_parameters
;@make_cont_lines_parameters
@natconst

; position angle
if n_elements(incl) eq 0 then incl = 60.
PA=0.0

; set number of spectral channels over line width
nchan = 20

if not keyword_set(working) then working = '~/Astro/699-2/radmc_output'
cd, working

if n_elements(gd) eq 0 then gd = [1,3,10,30,100,300]
for n = 0,n_elements(gd)-1 do begin
  print,format='("---> gas-to-dust ratio = ",i0)',gd[n]
  plot_profile, gas_to_dust=gd[n], _extra=extrakey

  make_co_profile, gas_to_dust=gd[n], _extra=extrakey

  print,'---> Making CO 1-0 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 1 iline 1 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_co10.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite

  print,'---> Making CO 2-1 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 1 iline 2 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_co21.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite

  print,'---> Making CO 3-2 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 1 iline 3 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_co32.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite


  print,'---> Making 13CO 1-0 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 2 iline 1 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_13co10.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite

  print,'---> Making 13CO 2-1 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 2 iline 2 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_13co21.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite

  print,'---> Making 13CO 3-2 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 2 iline 3 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_13co32.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite


  print,'---> Making C18O 1-0 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 3 iline 1 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_c18o10.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite

  print,'---> Making C18O 2-1 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 3 iline 2 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_c18o21.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite

  print,'---> Making C18O 3-2 spectrum'
  file_delete,'spectrum.out',/allow_nonexistent
  cmd=string(format='("radmc3d spectrum noscat imolspec 3 iline 3 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
  fileout = string(format='("spectrum_gd",i0,"_c18o32.out")',gd[n])
  file_move,'spectrum.out',fileout,/overwrite

endfor
print,'---> *** calclines complete! ***' 

return
end
