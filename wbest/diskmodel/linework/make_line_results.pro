PRO MAKE_LINE_RESULTS, STAR, DISK, CHEM, INCL, GD=gd, WORKING=working

;+
; Loop through predefined gas-to-dust ratio values and integrate spectra,
; writing results out to line_results_xx.txt, where xx is the transition.
;
;  HISTORY
;  Written 10/29/12 (jpw)
;  11/28/12 (WB): Added loop to calculate all three transitions (1-0, 2-1, 3-2)
;                 added GD and WORKING keywords
;  12/12/12 (WB): Added CHEM as a parameter
;
;  INPUTS
;      STAR - Structure containing stellar parameters:
;             l:luminosity, t:temperature, g:gravity, m:mass
;      DISK - Structure containing disk parameters:
;             m:mass, gamma:gamma, rc:R_c, H100:H_100, h:h
;      CHEM - Structure containing CO chemical parameters:
;             avd:A_V_dissoc, tf:T_freeze
;      INCL - Disk's inclination angle.  Default = 60 degrees.
;
;  KEYWORDS
;      GD - Scalar or vector of gas-to-dust ratios to be used in calculating the
;           line spectra.
;           Default: [1,3,10,30,100,300]
;      WORKING - working directory to point to
;                Default: '~/Astro/699-2/radmc_output'
;-

@natconst

; predefined gas-to-dust ratios
if n_elements(gd) eq 0 then gd = [1,3,10,30,100,300]

; rest wavelength in microns
lambda0_co10   = 2600.7576d
lambda0_co21   = 1300.4036557964414d
lambda0_co32   = 866.96337d

lambda0_13co10 = 2720.4063d
lambda0_13co21 = 1360.2279849627178d
lambda0_13co32 = 906.84625d

lambda0_c18o10 = 2730.7936d
lambda0_c18o21 = 1365.4216364738502d
lambda0_c18o32 = 910.30867d

lambda0 = { mco:[lambda0_co10, lambda0_co21, lambda0_co32], $
            m13co:[lambda0_13co10, lambda0_13co21, lambda0_13co32], $
            mc18o:[lambda0_c18o10, lambda0_c18o21, lambda0_c18o32] }

;if not keyword_set(working) then working = '~/Astro/699-2/radmc_output'
if not keyword_set(working) then working = '.'
cd, working

; Calculate integrated line intensities
trans = ['10', '21', '32']

for t=0, n_elements(trans)-1 do begin
    get_lun,lun
    openw,lun,'line_results_'+trans[t]+'.txt'
    if n_elements(star) ne 0 then $
      printf,lun,'% Star:  '+string(format='("L = ",e8.2," Lsun,  T = ",i0," K")', star.l, star.t)
    if n_elements(disk) ne 0 then $
      printf,lun,'% Disk:  '+string(format='("M = ",e8.2," Msun,  gamma = ",f4.2,",  ")', disk.m, disk.gamma)+$
             string(format='("Rc = ",f5.1," AU,  H100 = ",f4.1," AU,  h = ",f4.2)', disk.rc, disk.H100, disk.h)
    if n_elements(chem) ne 0 then $
      printf,lun,'% Chemistry:  '+$
             string(format='("A_V_dissoc = ",f3.1," mag,  T_freeze = ",f5.2," K")', chem.avd, chem.tf)
    if n_elements(incl) ne 0 then $
      printf,lun,'% Inclination = '+trim(incl)+' degrees'
    if n_elements(star)+n_elements(disk)+n_elements(incl) ne 0 then printf,lun,'%'
    printf,lun,'% Transition:  '+strmid(trans[t],0,1)+'-'+strmid(trans[t],1,1)
    printf,lun,'%'
    printf,lun,'% G/D      CO      13CO      C18O'
    printf,lun,'% ----------------------------------------'

    for n=0, n_elements(gd)-1 do begin
        filein = string(format='("spectrum_gd",i0,"_co",a,".out")', gd[n], trans[t])
        if not file_test(filein) then begin
            print,format='(a0," not found... exiting!")',filein
            return
        endif
        readcol,filein,format='d,d',lambda,F_co,skipline=3
        v_co = cc*(lambda-lambda0.mco[t])/lambda0.mco[t]
        v_co = v_co / 1d5

        filein = string(format='("spectrum_gd",i0,"_13co",a,".out")',gd[n], trans[t])
        if not file_test(filein) then begin
            print,format='(a0," not found... exiting!")',filein
            return
        endif
        readcol,filein,format='d,d',lambda,F_13co,skipline=3
        v_13co = cc*(lambda-lambda0.m13co[t])/lambda0.m13co[t]
        v_13co = v_13co / 1d5

        filein = string(format='("spectrum_gd",i0,"_c18o",a,".out")',gd[n], trans[t])
        if not file_test(filein) then begin
            print,format='(a0," not found... exiting!")',filein
            return
        endif
        readcol,filein,format='d,d',lambda,F_c18o,skipline=3
        v_c18o = cc*(lambda-lambda0.mc18o[t])/lambda0.mc18o[t]
        v_c18o = v_c18o / 1d5

; convert fluxes to Jy as observed at 140 pc
        F_co = F_co * 1d23/140.^2
        F_13co = F_13co * 1d23/140.^2
        F_c18o = F_c18o * 1d23/140.^2

; subtract continuum (assuming no line emission in the first channel)
        F_co = F_co - F_co[0]
        F_13co = F_13co - F_13co[0]
        F_c18o = F_c18o - F_c18o[0]

; integrate line emission (Jy km/s)
        I_co = 0.0d
        for i = 0,n_elements(v_co)-2 do I_co = I_co + F_co[i]*(v_co[i+1]-v_co[i])
        I_13co = 0.0d
        for i = 0,n_elements(v_13co)-2 do I_13co = I_13co + F_13co[i]*(v_13co[i+1]-v_13co[i])
        I_c18o = 0.0d
        for i = 0,n_elements(v_c18o)-2 do I_c18o = I_c18o + F_c18o[i]*(v_c18o[i+1]-v_c18o[i])

        printf,lun,format='(2x,i3,3(3x,f7.3))',gd[n],I_co,I_13co,I_c18o
    endfor

    printf,lun,'% ----------------------------------------'
;    close,lun
    free_lun,lun

endfor

END
