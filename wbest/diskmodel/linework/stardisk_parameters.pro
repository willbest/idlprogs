; stellar parameters
star = {l:4.4d-1, t:3.705d3, g:3.81, m:0.61}

; disk parameters
disk = {m:6.539d-4, gamma:0.346, rc:78.7, H100:5.93, h:1.057}

