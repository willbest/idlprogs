@readradmc.pro

pro make_co_profile, A_V_DISSOC=A_V_dissoc, gas_to_dust=gas_to_dust, T_FREEZE=t_freeze

;+
; Grid up the CO/13CO/C18O gas density profiles, based on a radially constant
; gas-to-dust ratio and depleting/dissociating the molecules in the
; interior/exterior of the disk.
;
; the gas-to-dust ratio is a free parameter
; the molecular abundances relative to H2 are fixed at
; [CO]/[H2] = 1e-04
; [CO/13CO] = 70
; [CO/C18O] = 500
; see discussion in Section 4.3 of Qi et al. 2011
; and Section 4.2 of Isella et al. 2007
;
;  HISTORY
;  Written by jpw
;  12/11/12 (WB): added A_V_DISSOC and T_FREEZE keywords
;
;  INPUTS
;      INCL - Disk's inclination angle.  Default = 60 degrees.
;
;  KEYWORDS
;      A_V_DISSOC - Extinction cutoff for CO dissociation due to UV irradiation.
;                   Default: 0.5
;      GAS_TO_DUST - Gas-to-dust ratio to be used in calculating the line spectra.
;                    Default: 100
;      T_FREEZE - Temperature of CO depletion (freeze-out) in the interior of
;                 the disk.
;                 Default: 20 K
;-

@natconst.pro

;
; CO abundance (relative to H2) and isotope fractions
;
x_co = 1d-4
f_13co = 1./70d
f_c18o = 1./500d

;
; use canned radmc3d procedure to read in the
; dust_density.inp and dust_temperature.dat files
;
struct = read_data(/ddens,/dtemp)
minrho = 1d-23

;
; read in polar coordinate grid parameters
;
r = struct.grid.r/au
nr = struct.grid.nr
theta = struct.grid.theta
ntheta = struct.grid.ntheta
nphi = struct.grid.nphi
if (nphi ne 1) then begin
  print,'Oops... these are not polar coordinates!'
  stop
endif

;
; derive H2 gas number density from dust
;
if n_elements(gas_to_dust) eq 0 then gas_to_dust = 100.
n_h2 = struct.rho * gas_to_dust / (muh2 * mp)
t_gas = struct.temp
n_co = x_co * n_h2
n_co_min = x_co * minrho * gas_to_dust / (muh2 * mp)

;
; depletion due to freeze-out in midplane
;
if n_elements(t_freeze) eq 0 then t_freeze = 20.0d
freeze = where(t_gas lt t_freeze and t_gas gt 0, nfreeze)
if (nfreeze gt 0) then n_co[freeze] = n_co_min

;
; dissociation in upper layer (above A_V < A_V_dissoc)
; use N(H2)/A_V = 1e21 cm-2/mag
; and assumes H2 self-shielding (not dust) prevents CO dissociation
; use A_V_dissoc = 0.5 mag based on Gorti & Hollenbach 2008
;
if n_elements(A_V_dissoc) eq 0 then A_V_dissoc = 0.5d
for i = 0,nr-1 do begin
  A_V = 0.0d
  j = 0
  while (A_V lt A_V_dissoc and j lt ntheta-1) do begin
    j = j+1
    A_V = A_V+n_h2[i,j]*r[i]*au*(theta[j]-theta[j-1])/sin(theta[j])^2/1d21
  endwhile
  n_co[i,0:j] = n_co_min
endfor


;
; Write the molecular density files
;
file_delete,'numberdens_co.inp',/allow_nonexistent
openw,1,'numberdens_co.inp'
printf,1,1
printf,1,struct.grid.ncell
for k=0,nphi-1 do begin
   for j=0,ntheta-1 do begin
      for i=0,nr-1 do begin
         printf,1,n_co[i,j]
      endfor
   endfor
endfor
close,1

file_delete,'numberdens_13co.inp',/allow_nonexistent
openw,2,'numberdens_13co.inp'
printf,2,1
printf,2,struct.grid.ncell
for k=0,nphi-1 do begin
   for j=0,ntheta-1 do begin
      for i=0,nr-1 do begin
         printf,2,f_13co*n_co[i,j]
      endfor
   endfor
endfor
close,2

file_delete,'numberdens_c18o.inp',/allow_nonexistent
openw,3,'numberdens_c18o.inp'
printf,3,1
printf,3,struct.grid.ncell
for k=0,nphi-1 do begin
   for j=0,ntheta-1 do begin
      for i=0,nr-1 do begin
         printf,3,f_c18o*n_co[i,j]
      endfor
   endfor
endfor
close,3


return
end
