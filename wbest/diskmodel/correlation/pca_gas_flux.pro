PRO PCA_GAS_FLUX, DATA, CHOSEN, NTRANS, TRANS, NISO, ISO, THIN, PALAB, CORRMAT, OUTPATH=outpath

;+
;  Called by pca_gas.pro
;
;  Performs PCA analysis the CO fluxes from the make_gas simulations.
;
;  HISTORY
;  Written by Will Best (IfA), 04/16/2013
;
;  INPUTS
;
;  KEYWORDS
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/pca_gas_flux.txt
;-

if not keyword_set(outpath) then outpath = '~/Astro/699-2/results/gas/pca_gas_test.txt'

for j=0, ntrans-1 do begin      ; Loop over chosen transitions
    data = [[data], [chosen.(iso[0]+13).(trans[j]-1)[thin-1]]]
    for k=1, niso-1 do data = [[data], [chosen.(iso[k]+13).(trans[j]-1)[thin-1]]]  ; Loop over chosen isotopologues
endfor

pca, data, eigenval, eigenvect, percent, proj_obj, proj_atr, matrix=corrmat, textout=outpath;, /silent

trlab = ['J=1-0', 'J=2-1', 'J=3-2']
islab = ['12CO', '13CO', 'C18O']

col = n_elements(corrmat[*,0]) - ntrans*niso

for j=0, ntrans-1 do begin      ; Loop over chosen transitions
    palabo = strarr(col,niso)
    perco = fltarr(col,niso)

    for k=0, niso-1 do begin    ; Loop over chosen isotopologues
        row = k + j*niso + 7
        corr = corrmat[0:col-1, row]
        perc = abs(corr)*100 / total(abs(corr))
        
        order = reverse(sort(perc))
        perco[*,k] = perc[order]
        palabo[*,k] = palab[order]

        ;; print
        ;; print
        ;; print, islab[iso[k]-1], format='(t11,a)'
        ;; print, trlab[trans[j]-1], format='(t11,a)'
        ;; print
        ;; print, ' Parameter    Percentage'
        ;; print, ' ---------    ----------'
        ;; for i=0, col-1 do print, palabo[i,k], perco[i,k], format='(t4,a,t18,f4.1)'
        ;; print
    endfor

    print
    print
    print, islab[iso-1], format='(t11,a,t43,a,t75,a)'
    print, replicate(trlab[trans[j]-1], niso), format='(t11,a,t43,a,t75,a)'
    print
    print, replicate(' Parameter    Percentage', niso), format='(a,t33,a,t65,a)'
    print, replicate(' ---------    ----------', niso), format='(a,t33,a,t65,a)'
    for i=0, col-1 do print, reform([palabo[i,*], trim(perco[i,*])], 2*niso), $
                             format='(t4,a,t18,f4.1,t36,a,t50,f4.1,t68,a,t82,f4.1)'
    print
endfor
;STOP
return

END

