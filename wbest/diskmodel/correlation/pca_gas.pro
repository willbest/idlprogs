PRO PCA_GAS, GAS=gas, FILEPATH=filepath, OUTPATH=outpath, _EXTRA=extrakey, $
             MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, TA1=ta1, QA=qa, $
             NPD=npd, TFREEZE=tfreeze, INCL=incl, ISO=iso, TRANS=trans, THIN=thin, $
             FLUX=flux, FRACTHIN=fracthin, FRACFRO=fracfro, FRACDIS=fracdis

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.
;  Performs principal component analysis to determine the most significant
;  parameters.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the PCA will incorporate fluxes only from disks with those
;  parameter values.
;
;  If no value is specified for a given parameter, then PCA is performed
;  for disks of all values of that parameter (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 04/16/2013
;  Definitely still a work in progress
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/gas
;      OUTPATH - path for PCA report.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used in the PCA.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used in the PCA.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      THIN - Use the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

npar = 14

;;; GET THE DATA
if n_elements(gas) eq 0 then begin
    if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/gas/'
    restore, filepath+'gas.sav'
endif


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Make a structure containing the values from the keywords
if not keyword_set(mstar) then mstar = -1
if not keyword_set(mgas) then mgas = -1
if not keyword_set(gamma) then gamma = -1
if not keyword_set(rin) then rin = -1
if not keyword_set(rc) then rc = -1
if not keyword_set(tm1) then tm1 = -1
if not keyword_set(qm) then qm = -1
if not keyword_set(ta1) then ta1 = -1
if not keyword_set(qa) then qa = -1
if not keyword_set(npd) then npd = -1
if not keyword_set(tfreeze) then tfreeze = -1
if not keyword_set(incl) then incl = -1
if not keyword_set(iso) then iso = [1, 2, 3]
if not keyword_set(trans) then trans = [1, 2, 3]
if not keyword_set(thin) then thin = 3
ntrans = n_elements(trans)
niso = n_elements(iso)

keys = {mstar:mstar, mgas:mgas, gamma:gamma, rin:rin, rc:rc, tm1:tm1, qm:qm, ta1:ta1, qa:qa, $
        npd:npd, tfreeze:tfreeze, incl:incl}

;;; Build a large string, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters to select disks with the chosen parameters
for i=0, (npar-3) do begin
    if (keys.(i)[0] ge 0) then begin    ; if the keyword has value(s)], go on
        match, gas[uniq(gas.(i), sort(gas.(i)))].(i), keys.(i), xx, ind  ; where parameter matches keyword value(s)
        nind = n_elements(ind)
        if ind[0] ge 0 then begin                    ; if parameter matches keyword value(s) at least once, go on
            if nind gt 1 then fstr = fstr + $
                   '(' + strjoin('(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
            fstr = fstr + '(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
            if nind gt 1 then fstr = fstr + ')'
            fstr = fstr + ' and '
        endif
    endif
endfor


;;; GET THE GAS DISK VALUES
; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'  ; replace the last ' and ' with ')'
    dummy = execute(fstr)
    chosen = gas[select]
endif else chosen = gas


;;; Choose the parameters with more than one value
params = ['M_star', 'M_gas', 'gamma', 'R_in', 'R_c', 'T_mid1', 'q_mid', $
         'T_atm1', 'q_atm', 'N_pd', 'T_freeze', 'incl']

data = fltarr(n_elements(chosen))
palab = 'dummy'
for i=0, npar-3 do begin
    if n_elements(chosen[uniq(chosen.(i), sort(chosen.(i)))].(i)) gt 1 then begin
        data = [[data], [chosen.(i)]]
        palab = [palab, params[i]]
    endif
endfor
data = data[*,1:*]
palab = palab[1:*]

if n_elements(flux)+n_elements(fracthin)+n_elements(fracfro)+n_elements(fracdis) eq 0 then flux=1

if keyword_set(flux) then pca_gas_flux, data, chosen, ntrans, trans, niso, iso, thin, $
                                        palab, corrmat, outpath=outpath

if keyword_set(fracthin) then hist_gas_thin, p, npar, ntrans, hcom, chosen, niso, iso, trans, $
                                thin, islab1, thlab, label, vals, tr, tname, binsize=binsize, outpath=outpath

if keyword_set(fracfro) then hist_gas_fro, p, npar, chosen, niso, nvals, iso, $
                               label, vals, binsize=binsize, outpath=outpath

if keyword_set(fracdis) then hist_gas_dis, p, npar, chosen, niso, nvals, iso, $
                               label, vals, binsize=binsize, outpath=outpath

END

