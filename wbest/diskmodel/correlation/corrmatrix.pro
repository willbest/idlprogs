PRO CORRMATRIX, DATA, LABELS, ABS=abs, CORRMAT=corrmat, NONUM=nonum, OUTGRID=outgrid, $
                OUTTEXT=outtext, PS=ps, SILENT=silent, $
                COLMIN=colmin, COLMAX=colmax, ROWMIN=rowmin, ROWMAX=rowmax

;+
;  Calculates the correlation matrix for a data set.  Displays the matrix as a
;  color-mapped grid for easier viewing.  Red means positive correlation, blue
;  means negative correlation, white means no correlation.  Darker shades
;  indicate stronger correlation.
;
;  What is the correlation matrix, you say?
;  First, for any m x n matrix A, the covariance matrix is the n x n matrix
;     trans(A) # A.  Each element of this symmetric matrix gives you the covariance
;     of the two columns of A corresponding to the location of the element.
;     The diagonal of the covariance matrix gives the variance for each column
;     of A.
;  The correlation matrix for A is the covariance matrix, with each element
;     divided by the standard deviations of the two columns of A corresponding to
;     the location of the element.  This normalizes the matrix so that each element
;     is between -1 and 1, and therefore...
;        >>> each element gives the correlation coefficient of the two columns of A
;        >>> corresponding to the location of the element.
;     The diagonal of the correlation matrix is all 1's, because each column
;     of A correlates with itself.
;
;  HISTORY
;  Written by Will Best (IfA), 04/26/2013
;  05/21/2013 (WB): Split this part off to generalize it.
;
;  INPUTS
;      DATA - m x n matrix containing the values for m instances of n variables.
;             m = number of rows - arbitrary number.
;             n = number of columns - one for each parameter.
;             m and n must both be at least two.
;
;  OPTIONAL INPUTS
;      LABELS - vector of strings containing the row and column labels to be
;               printed with the formatted correlation matrix.
;               Must contain n elements, *even if you are not printing all of
;               the rows and/or columns*.  (Just uses spaces for non-needed
;               labels.) 
;               If LABELS is not supplied, the rows and columns will be labeled
;               with integers, starting with 1.
;
;  OPTIONAL KEYWORDS
;      ABS - Print the formatted matrix with colors representing the absolute
;            values of the correlation coefficients; in other words, only use red.
;      COLMIN, COLMAX - Integers for the first and last columns to be printed in
;               the formatted correlation matrix.
;               Defaults:  colmin = 1, colmax = n   (i.e. all columns)
;               To confirm the integers you want to use, first print the entire
;               correlation matrix without supplying labels.
;      CORRMAT - Set this equal to a variable that will receive the n x n
;                correlation matrix.
;      NONUM - Don't print the numbers in the formatted matrix -- just colors.
;      OUTGRID - Set this equal to the path/filename for postscript output.
;                Default: ~/Astro/corrmatrix.eps
;      OUTTEXT - To print the correlation matrix values to a text file, set
;                OUTTEXT equal to the path/filename.
;      PS - Print the formatted correlation matrix to a postscript file.
;      ROWMIN, ROWMAX - Integers for the first and last rows to be printed in
;               the formatted correlation matrix.
;               Defaults:  rowmin = 1, rowmax = n   (i.e. all rows)
;               To confirm the integers you want to use, first print the entire
;               correlation matrix without supplying labels.
;      SILENT - Suppress text output to the screen.
;
;  CALLS
;      correlate
;      trim
;
;  FUTURE WORK
;      Keywords to define the zooming in on specific parts of the correlation matrix.
;-

;;; Check that the DATA matrix has at least two rows and two columns.
datainfo = size(data)        ; get number of dimensions and size of each dimension of DATA
ncol = fix(datainfo[1])      ; number of columns in the DATA matrix, force to be an integer
nrow = datainfo[2]           ; number of rows in the DATA matrix
if (datainfo[0] ne 2) or (ncol lt 2) or (nrow lt 2) then begin   ; must be a 2-D matrix
    print
    print, 'Use:  CORRMATRIX, DATA [, LABELS, ABS=abs, CORRMAT=corrmat, NONUM=nonum, OUTGRID=outgrid, '
    print, '          OUTTEXT=outtext, PS=ps, SILENT=silent, $'
    print, '          COLMIN=colmin, COLMAX=colmax, ROWMIN=rowmin, ROWMAX=rowmax]'
    print
    print, 'DATA must be a 2-D array with n columns and m rows.'
    print, 'The n columns represent each of the variables to be correlated.'
    print, 'The m rows contain the values for each instance of the varaibles.'
    print, 'm and n must each be at least 2.'
    print
    return
endif

;;; Check that the LABELS vector has n elements
; n = number of variables = number of columns in the DATA matrix.
if keyword_set(labels) then begin
    labelsinfo = size(labels)
    if (labelsinfo[0] ne 1) or (labelsinfo[1] ne ncol) or (labelsinfo[2] ne 7) $
      then begin                  ; must be a vector of strings with ncol elements
        print
        print, 'Use:  CORRMATRIX, DATA [, LABELS, ABS=abs, CORRMAT=corrmat, NONUM=nonum, OUTGRID=outgrid, '
        print, '          OUTTEXT=outtext, PS=ps, SILENT=silent, $'
        print, '          COLMIN=colmin, COLMAX=colmax, ROWMIN=rowmin, ROWMAX=rowmax]'
        print
        print, 'LABELS must be a vector of strings with n elements.'
        print
        return
    endif
endif else begin
; If LABELS was not supplied by the user, make it a vector of integers starting at 1.
    labels = trim(indgen(ncol)+1)
endelse

;;; Check that any user-supplied min and max rows and columns to print are integers and sensible.
if n_elements(rowmin) eq 0 then rowmin = 1
if n_elements(rowmax) eq 0 then rowmax = ncol
if n_elements(colmin) eq 0 then colmin = 1
if n_elements(colmax) eq 0 then colmax = ncol
if (size(rowmin,/type) ne 2) or (size(rowmax,/type) ne 2) or (size(colmin,/type) ne 2) or $
  (size(colmax,/type) ne 2) or (rowmin lt 1) or (rowmax gt ncol) or (colmin lt 1) or (colmax gt ncol) or $
  (rowmin gt rowmax) or (colmin gt colmax) then begin
    print
    print, 'Use:  CORRMATRIX, DATA [, LABELS, ABS=abs, CORRMAT=corrmat, NONUM=nonum, OUTGRID=outgrid, '
    print, '          OUTTEXT=outtext, PS=ps, SILENT=silent, $'
    print, '          COLMIN=colmin, COLMAX=colmax, ROWMIN=rowmin, ROWMAX=rowmax]'
    print
    print, 'COLMIN, COLMAX, ROWMIN, and ROWMAX must be integers between 1 and the number of variables'
    print, 'being correlated.'
    print
    return
endif


;;; Calcualte the correlation matrix
corrmat = correlate(data)


;;; Print the text versions of the correlation matrix
; Print the correlation matrix values as text to the screen
form = '('+trim(ncol)+'(2x,g9.3))'    ; format for the text output
if not keyword_set(silent) then begin
    print
    print
    print, 'CORRELATION MATRIX'
    print
    print, corrmat, format=form
endif

; Print the correlation matrix values as text to a file
if keyword_set(outpath) then begin
    get_lun, lun
    wid = 100 > (ncol*11+1)     ; make sure the output is wide enough that each row is on one line
    openw, lun, outpath, width=wid
    printf, lun, 'CORRELATION MATRIX'
    printf, lun
    printf, lun, corrmat, format=form
    free_lun, lun
endif


;;; Set up for plotting
device, decomposed=0
;lincolr_wb, /silent       ; Load color table

; Postscript or screen
if keyword_set(ps) then begin
    if n_elements(outgrid) eq 0 then outgrid = '~/Astro/corrmatrix'
    ps_open, outgrid, /color, /en, thick=4
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
endelse

; Dimensions of the formatted correlation matrix
colmod = colmax - colmin + 1
rowmod = rowmax - rowmin + 1
sizemod = rowmod > colmod          ; adjustment for scaling on the formatted matrix
xr = [colmin-1, colmax]            ; plotting range for the columns
yr = [rowmax, rowmin-1]            ; plotting range for the rows
labax = replicate(' ', ncol+1)     ; blank spaces for the tick labels


;;; Define a color table
; {green,red,blue}: {0,0,0}=black, {255,255,255}=white, {0,255,0}=green, etc.
; 0 --> 100 = blue --> white
; 100 --> 200 = white --> red
; 201 = black
;; nlev = 200
;; green = fltarr(nlev+2)   ; array with elements 0 to 201
;; green[nlev/2] = 1.       ; element 100 is max value for green
;; red = green              ; same for red
;; blue = green             ; same for blue
;; for i=0, (nlev/2)-1 do begin
;;     green[i] = i / (nlev/2.)      ; green 0 --> 1  for first 100 elements of color table
;;     red[i] = i / (nlev/2.)        ; red 0 --> 1  for first 100 elements of color table
;;     blue[i] = 1.                  ; blue always 1  for first 100 elements of color table
;;     green[nlev-i] = green[i]      ; green 1 --> 0  for second 100 elements of color table
;;     red[nlev-i] = 1.              ; red always 1  for second 100 elements of color table
;;     blue[nlev-i] = green[i]       ; blue 1 --> 0  for second 100 elements of color table
;; endfor

;; ; Load the new color table
;; tvlct, fix(sqrt(red)^2*255), fix(sqrt(green)^2*255), fix(sqrt(blue)*255) ; sqrt makes better visual transition
;; white = nlev/2           ; element 100 is white
;; black = nlev + 1         ; element 201 is black

red_blue_colorbar, white, black, nlev

;;; Plot the formatted correlation matrix
; Make the basic correlation matrix gridgrid
plot, [0], backg=white, color=black, thick=4, /nodata, $
      xrange=xr, /xstyle, xticklen=1, xgridstyle=0, xtickint=1, xminor=1, xtickn=labax, xmargin=[50./colmod, 11], $
      yrange=yr, /ystyle, yticklen=1, ygridstyle=0, ytickint=1, yminor=1, ytickn=labax, ymargin=[2, 28./rowmod]

for i=colmin-1, colmax-1 do begin    ; loop over the columns (proceed horizontally)
    xyouts, i+0.5, rowmin-1.2, labels[i], /data, color=black, chars=10./colmod, align=0.5    ; plot column label
    for j=rowmin-1, rowmax-1 do begin    ; loop over the rows (proceed vertically)
        if i eq colmin-1 then xyouts, colmin-1.3, j+0.5, labels[j], /data, color=black, chars=16./sizemod, $
          align=0.5;, orient=90       ; first time through, plot the row labels
        xvec = [i, i+1, i+1, i]       ; x-coords of box to fill with color
        yvec = [j, j, j+1, j+1]       ; y-coords of box to fill with color
        if keyword_set(abs) then heatcol = (nlev/2.)*(abs(corrmat[i,j]) + 1) $
          else heatcol = (nlev/2.)*(corrmat[i,j] + 1)    ; map the color to the correlation coefficient
        polyfill, xvec, yvec, /data, color=heatcol       ; plot the color to the grid
        if not keyword_set(nonum) then xyouts, i+0.5, j+0.5, trim(corrmat[i,j], '(f5.2)'), $
          /data, color=black, chars=12./sizemod, align=0.5    ; overplot the number to the grid
    endfor
endfor
cgcolorbar, ncolors=200, /vert, /right, tickn=trim([-1, -.5, 0, .5, 1]), $    ; make the colorbar on the right
            chars=1, pos=[0.93, 0.045, 0.95, 0.915]

if keyword_set(ps) then ps_close

END

