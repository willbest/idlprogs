PRO CORR_GAS, GAS=gas, FILEPATH=filepath, OUTGRID=outgrid, OUTTEXT=outtext, _EXTRA=extrakey, $
              MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, TA1=ta1, QA=qa, $
              NPD=npd, TFREEZE=tfreeze, INCL=incl, ISO=iso, TRANS=trans, THIN=thin, $
              FRACTHIN=fracthin, FRACFRO=fracfro, FRACDIS=fracdis

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.
;  Calculates the correlation matrix to determine the most significant
;  parameters.
;
;  What is the correlation matrix, you say?
;  First, for any m x n matrix A, the covariance matrix is the n x n matrix
;     trans(A) # A.  Each element of this symmetric matrix gives you the covariance
;     of the two columns of A corresponding to the location of the element.
;     The diagonal of the covariance matrix gives the variance for each column
;     of A.
;  The correlation matrix for A is the covariance matrix, with each element
;     divided by the standard deviations of the two columns of A corresponding to
;     the location of the element.  This normalizes the matrix so that each element
;     is between -1 and 1, and therefore...
;        >>> each element gives the correlation coefficient of the two columns of A
;        >>> corresponding to the location of the element.
;     The diagonal of the correlation matrix is all 1's, because each column
;     of A correlates with itself.
;
;  If specific values for various disk parameters are supplied (via keyword),
;  then the correlation matrix will incorporate fluxes only from disks with those
;  parameter values.
;
;  If no value is specified for a given parameter, then the correlation matrix
;  includes disks of all values of that parameter (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 04/26/2013
;  05/22/2013 (WB): Split off the matrix calculation and formatted printing to
;                   the corrmatrix.pro routine.
;                   This routine specifically deals with the gas disk simulations.
;
;  OPTIONAL KEYWORDS
;      ABS - Print the formatted matrix with colors representing the absolute
;            values of the correlation coefficients; in other words, only use red.
;      COLMIN, COLMAX - Integers for the first and last columns to be printed in
;               the formatted correlation matrix.
;               Defaults:  colmin = 1, colmax = n   (i.e. all columns)
;               To confirm the integers you want to use, first print the entire
;               correlation matrix without supplying labels.
;      CORRMAT - Set this equal to a variable that will receive the n x n
;                correlation matrix.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/gas
;      GAS - Structure containing the data to plot.  If none is supplied, the
;            program will load "gas.sav" from FILEPATH.
;      NONUM - Don't print the numbers in the formatted matrix -- just colors.
;      OUTGRID - Set this equal to the path/filename for postscript output.
;                Default: ~/Astro/699-2/results/gas/corr_gas_grid.eps
;      OUTTEXT - To print the correlation matrix values to a text file, set
;                OUTTEXT equal to the path/filename.
;      PS - Print the formatted correlation matrix to a postscript file.
;      ROWMIN, ROWMAX - Integers for the first and last rows to be printed in
;               the formatted correlation matrix.
;               Defaults:  rowmin = 1, rowmax = n   (i.e. all rows)
;               To confirm the integers you want to use, first print the entire
;               correlation matrix without supplying labels.
;      SILENT - Suppress text output to the screen.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used in the correlation analysis.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used in the correlation analysis.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      THIN - Use the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;
;      FRACDIS - Include the photodissociated CO fraction as a variable in the
;                correlation matrix.
;      FRACFRO - Include the frozen CO fraction as a variable in the
;                correlation matrix. 
;      FRACTHIN - Include the optically thin CO emission fraction as a variable
;                 in the correlation matrix.
;
;  CALLS
;      corrmatrix
;      trim
;-

;;; SET UP PARAMETERS AND PATHS
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/gas/'
if not keyword_set(gas) then gas='gas.sav'
if not keyword_set(outgrid) then outgrid='~/Astro/699-2/results/gas/corr_gas_grid'
if not keyword_set(outtext) then outtext='~/Astro/699-2/results/gas/corr_gas_result.txt'

; Labels for the parameters and emission lines
params = ['M_star', 'M_gas', 'gamma', 'R_in', 'R_c', 'T_mid1', 'q_mid', $
         'T_atm1', 'q_atm', 'N_pd', 'T_freeze', 'incl']
labarr = textoidl(['M_{star}', 'M_{gas}', '\gamma', 'R_{in}', 'R_c', 'T_{m1}', 'q_m', $
                   'T_{a1}', 'q_a', 'N_{pd}', 'T_{fr}', 'i'])
npar = n_elements(params)
islab1 = textoidl(['^{12}CO', '^{13}CO', 'C^{18}O'])
trlab1 = textoidl(['(1-0)', '(2-1)', '(3-2)'])


;;; GET THE DATA
restore, filepath+gas


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Make a structure containing the values from the keywords
; If keywordsare not set, use all of the possible values for the associated parameter.
if not keyword_set(mstar) then mstar = -1
if not keyword_set(mgas) then mgas = -1
if n_elements(gamma) eq 0 then gamma = -1
if not keyword_set(rin) then rin = -1
if not keyword_set(rc) then rc = -1
if not keyword_set(tm1) then tm1 = -1
if not keyword_set(qm) then qm = -1
if not keyword_set(ta1) then ta1 = -1
if not keyword_set(qa) then qa = -1
if not keyword_set(npd) then npd = -1
if not keyword_set(tfreeze) then tfreeze = -1
if not keyword_set(incl) then incl = -1
if not keyword_set(iso) then iso = [1, 2, 3]
if not keyword_set(trans) then trans = [1, 2, 3]
if not keyword_set(thin) then thin = 3
ntrans = n_elements(trans)
niso = n_elements(iso)

keys = {mstar:mstar, mgas:mgas, gamma:gamma, rin:rin, rc:rc, tm1:tm1, qm:qm, ta1:ta1, qa:qa, $
        npd:npd, tfreeze:tfreeze, incl:incl}

; Build a large string, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters to select disks with the chosen parameters
for i=0, npar-1 do begin
    if (keys.(i)[0] ge 0) then begin    ; if the keyword has value(s)], go on
        match, gas[uniq(gas.(i), sort(gas.(i)))].(i), keys.(i), xx, ind  ; where parameter matches keyword value(s)
        nind = n_elements(ind)
        if ind[0] ge 0 then begin                    ; if parameter matches keyword value(s) at least once, go on
            if nind gt 1 then fstr = fstr + $
                   '(' + strjoin('(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
            fstr = fstr + '(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
            if nind gt 1 then fstr = fstr + ')'
            fstr = fstr + ' and '
        endif
    endif
endfor


;;; GET THE GAS DISK VALUES
; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'  ; replace the last ' and ' with ')'
    dummy = execute(fstr)
    chosen = gas[select]
endif else chosen = gas


;;; CREATE THE DATA MATRIX FOR WHICH TO CALCULATE THE CORRELATION MATRIX
; Choose the parameters with more than one value, and build the data matrix
data = fltarr(n_elements(chosen))
palab = 'dummy'
labels = 'dummy'
for i=0, npar-1 do begin
    if n_elements(chosen[uniq(chosen.(i), sort(chosen.(i)))].(i)) gt 1 then begin
        data = [[data], [chosen.(i)]]    ; add column to the data matrix
        palab = [palab, params[i]]       ; add parameter name to the labels vector for text tables
        labels = [labels, labarr[i]]     ; add parameter name to the formatted matrix labels vector
    endif
endfor
data = data[*,1:*]
palab = palab[1:*]
labels = labels[1:*]

; Add the chosen line fluxes to the matrix
for j=0, ntrans-1 do begin      ; Loop over chosen transitions
    data = [[data], [chosen.(iso[0]+13).(trans[j]-1)[thin-1]]]    ; add column to the data matrix
    labels = [labels, islab1[iso[0]-1]+trlab1[trans[j]-1]]        ; add line name to the formatted matrix labels vector
    if keyword_set(fracthin) then begin    ; Optically thin fraction
        data = [[data], [chosen.(iso[0]+13).(trans[j]-1)[1] / chosen.(iso[0]+13).(trans[j]-1)[2]]]
        labels = [labels, 'f!Dthin!N']
    endif
    for k=1, niso-1 do begin    ; Loop over chosen isotopologues
        data = [[data], [chosen.(iso[k]+13).(trans[j]-1)[thin-1]]]    ; add column to the data matrix
        labels = [labels, islab1[iso[k]-1]+trlab1[trans[j]-1]]        ; add line name to the formatted matrix labels vector
        if keyword_set(fracthin) then begin    ; Optically thin fraction
            data = [[data], [chosen.(iso[k]+13).(trans[j]-1)[1] / chosen.(iso[k]+13).(trans[j]-1)[2]]]
            labels = [labels, 'f!Dthin!N']
        endif
    endfor
endfor    

; Frozen-out CO fraction
if keyword_set(fracfro) then begin
    data = [[data], [chosen.freeze.f]]
    labels = [labels, 'f!Dfrozen!N']
endif

; Photodissociated CO fraction
if keyword_set(fracdis) then begin
    data = [[data], [chosen.dissoc.f]]
    labels = [labels, 'f!Ddissoc!N']
endif


;;; GET AND PRINT THE CORRELATION MATRIX
data = transpose(data)
corrmatrix, data, labels, corrmat=corrmat, outgrid=outgrid, outtext=outtext, /silent, _extra=extrakey


;;; PRINT CORRELATON COEFFICIENTS IN TABLES
trlab = ['J=1-0', 'J=2-1', 'J=3-2']
islab = ['12CO', '13CO', 'C18O']

ncol = n_elements(corrmat[*,0])   ; number of columns in the data matrix
col = ncol - ntrans*niso          ; number of parameter columns

; Set up the parameter tables
for j=0, ntrans-1 do begin      ; Loop over chosen transitions
    palabo = strarr(col,niso)   ; array for ordered parameter labels
    corro = fltarr(col,niso)    ; array for ordered correlation coefficients

    for k=0, niso-1 do begin    ; Loop over chosen isotopologues
        row = k + j*niso + col  ; choose a row corresponding to a line flux
        corr = corrmat[0:col-1, row]   ; get the correlation coeffs with parameters for that flux
        
        order = reverse(sort(abs(corr)))  ; sort the coeffs into descending order
        corro[*,k] = corr[order]
        palabo[*,k] = palab[order]
    endfor

; Print to screen
    print
    print
    print, islab[iso-1], format='(t11,a,t43,a,t75,a)'
    print, replicate(trlab[trans[j]-1], niso), format='(t11,a,t43,a,t75,a)'
    print
    print, replicate(' Parameter    Correlation', niso), format='(a,t33,a,t65,a)'
    print, replicate(' ---------    -----------', niso), format='(a,t33,a,t65,a)'
    for i=0, col-1 do print, reform([palabo[i,*], trim(corro[i,*])], 2*niso), $
                             format='(t4,a,t18,f5.2,t36,a,t50,f5.2,t68,a,t82,f5.2)'
    print

; Output to file
    if keyword_set(outtext) then begin
        get_lun, lun
        openw, lun, outtext, /append
        printf, lun
        printf, lun
        printf, lun, islab[iso-1], format='(t11,a,t43,a,t75,a)'
        printf, lun, replicate(trlab[trans[j]-1], niso), format='(t11,a,t43,a,t75,a)'
        printf, lun
        printf, lun, replicate(' Parameter    Correlation', niso), format='(a,t33,a,t65,a)'
        printf, lun, replicate(' ---------    -----------', niso), format='(a,t33,a,t65,a)'
        for i=0, col-1 do printf, lun, reform([palabo[i,*], trim(corro[i,*])], 2*niso), $
                                  format='(t4,a,t18,f5.2,t36,a,t50,f5.2,t68,a,t82,f5.2)'
        printf, lun
        free_lun, lun
    endif
endfor

END

