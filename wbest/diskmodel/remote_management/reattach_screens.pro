PRO REATTACH_SCREENS, FIRST=first, LAST=last

;+
;  Open new terminals and reattach SCREENs running in the background.
;  Default is to reattach all SCREENs, unless the FIRST and/or LAST keywords are set.
;  
;  HISTORY
;  Written by Will Best (IfA), 06/26/2013
;
;  OPTIONAL KEYWORDS
;      FIRST - Number of the first loop to reattach.
;              Default: 1
;      LAST - Number of the last loop to reattach.
;             Default: the last one
;
;  CALLS
;      trim
;-

; get the list of active screens
spawn, 'screen -ls', rawlist
print
print, rawlist
print

; parse out the sys names of the screen
n = n_elements(rawlist)
list = trim(strmid(rawlist[1:n-3], 0, 1#strpos(rawlist[1:n-3], '(')-1))

; pop open windows and reattach screens
if n_elements(first) eq 0 then first = 1
if n_elements(last) eq 0 then last = n-3

for i=first-1, last-1 do spawn, 'xterm -sb -sl 1000 -e screen -r '+list[i]+' &'

END
