PRO CREATE_SCREENS, NUM, FIRST

;+
;  Open new terminals and create SCREEN sessions in each.
;  
;  HISTORY
;  Written by Will Best (IfA), 06/26/2013
;
;  INPUTS
;      NUM - Number of screens to create.
;
;  OPTIONAL INPUTS
;      FIRST - Number of the first screen to create.
;              Default: 1
;
;  CALLS
;      trim
;-

if n_elements(num) eq 0 then begin
    message, 'Use:  create_screens, num [, first]'
endif

if n_elements(first) eq 0 then first = 1
last = first + num - 1

for i=first, last do begin
    ;spawn, 'cd ~/gasdisks/biggas/loop'+trim(i)
    spawn, 'xterm -sb -sl 1000 -e screen -h 1000 -t loop'+trim(i)+' &'
endfor

END
