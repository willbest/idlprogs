PRO DISKHIST_COUPLED, PARAM, SIMS=sims, FILEPATH=filepath, PS=ps, _EXTRA=extrakey, $
                      LSTAR=lstar, TSTAR=tstar, MDISK=mdisk, GAMMA=gamma, RC=rc, H100=h100, $
                      HSCALE=hscale, AV=av, TFREEZE=tfreeze, INCL=incl, GD=gd, ISO=iso, TRANS=trans

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  Makes a histogram of fluxes from disks with different values of the parameter
;  indicated by PARAM.
;  If specific values for various other parameters are supplied (via keyword),
;  then the histogram will incorporate fluxes only from disks with those
;  parameter values.
;  If no value is specified for a given parameter, then the flux of is summed
;  for disks of all values of that parameters (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 02/09/2013
;
;  INPUTS
;      PARAM - Show totals for different values of this parameter.
;                 1 - L_star
;                 2 - T_star
;                 3 - M_disk
;                 4 - gamma
;                 5 - R_c
;                 6 - H_100
;                 7 - h
;                 8 - A_V
;                 9 - T_freeze
;                 10 - i
;                 11 - g/d ratio
;                 12 - Isotopologues of CO
;                 13 - CO transitions
;              DEFAULT: 3 - M_disk
;              Any values supplied via keyword (see below) to the parameter
;                 indicated by PARAM are ignored.
;
;  KEYWORDS
;      SIMS - "sims" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the sum of the fluxes from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, the sum of the fluxes from disks with all values
;    for that parameter will be used to make the histogram.
;      LSTAR - L_star
;      TSTAR - T_star
;      MDISK - M_disk
;      GAMMA - gamma
;      RC - R_c
;      H100 - H_100
;      HSCALE - h
;      AV - A_V
;      TFREEZE - T_freeze
;      INCL - i
;      GD - g/d ratio
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : continuum
;              2 : (1-0)
;              3 : (2-1)
;              4 : (3-2)
;-

; Check input parameter
npar = 13
if n_elements(param) eq 0 then begin
    print, 'You must enter a parameter!'
    return
endif
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif

; get the data
if n_elements(sims) eq 0 then begin
    if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/'
    restore, filepath+'sims.sav'
endif


;;; DETERMINE THE PARAMETER VALUES FOR THE HISTOGRAMS
if n_elements(param) eq 0 then param = 3    ; Default histogram is flux vs. disk masses
p = param-1

case p of
    npar-1 : vals = [1, 2, 3, 4]     ; Transitions
    npar-2 : vals = [1, 2, 3]        ; Isotopologues
    else : vals = sims[uniq(sims.(p), sort(sims.(p)))].(p)
endcase

nvals = n_elements(vals)


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Make a structure containing the values from the keywords
if not keyword_set(lstar) then lstar = -1
if not keyword_set(tstar) then tstar = -1
if not keyword_set(mdisk) then mdisk = -1
if not keyword_set(gamma) then gamma = -1
if not keyword_set(rc) then rc = -1
if not keyword_set(h100) then h100 = -1
if not keyword_set(hscale) then hscale = -1
if not keyword_set(av) then av = -1
if not keyword_set(tfreeze) then tfreeze = -1
if not keyword_set(incl) then incl = -1
if not keyword_set(gd) then gd = -1
if not keyword_set(iso) then iso = [1,2,3]
if not keyword_set(trans) then trans = -1

keys = {lstar:lstar, tstar:tstar, mdisk:mdisk, gamma:gamma, rc:rc, h100:h100, $
        hscale:hscale, av:av, tfreeze:tfreeze, incl:incl, gd:gd, iso:iso, trans:trans}

;;; Build a large string, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters other than isotopologue and transition
for i=0, (npar-3) do begin
    if (keys.(i)[0] ge 0) and (p ne i) then begin    ; if the keyword has value(s) and is not the PARAM, go on
        match, sims[uniq(sims.(i), sort(sims.(i)))].(i), keys.(i), xx, ind  ; where parameter matches keyword value(s)
        nind = n_elements(ind)
        if ind[0] ge 0 then begin                    ; if parameter matches keyword value(s) at least once, go on
            if nind gt 1 then fstr = fstr + $
                   '(' + strjoin('(sims.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
            fstr = fstr + '(sims.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
            if nind gt 1 then fstr = fstr + ')'
            fstr = fstr + ' and '
        endif
    endif
endfor


;;; GET THE FLUX VALUES
; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'  ; replace the last ' and ' with a )
    dummy = execute(fstr)
    chosen = sims[select]
endif else chosen = sims
;chosen = chosen[sort(chosen.(p))]

; Continuum flux units are erg cm-2 s-1 Hz-1, i.e. flux density
; Convert to Jy as observed at 140 pc
chosen.cont = chosen.cont * 1d23/140.^2

; Oh, the structure
;ntot = total(nsim) * ninc * ngas
;sims = replicate({L:0., T:0, Md:0., g:0., Rc:0, H100:0, h:0., AV:0., Tf:0, $
;                  in:0, gd:0, cont:0d, I10:[0., 0., 0.], I21:[0., 0., 0.], I32:[0., 0., 0.]}, ntot)
; cont = 1.3 mm continuum emission
; 1-0, 2-1, 3-2
; CO, 13CO, C18O

; Get the values
tot = fltarr(nvals)
case p of
    npar-1 : begin           ; If PARAM is set to the CO transitions
        for i=0, nvals-1 do begin
            if (i gt 0) then begin                         ; skipping the 1.3 mm continuum...
                tot[i] = tot[i] + total(chosen.(i+npar-2)[keys.iso-1]) ; Include only identified isotopologues in total
            endif else tot[i] = total(chosen.(i+npar-2))   ; Each bar is for a single transition
        endfor
    end
    npar-2 : begin           ; If PARAM is set to the CO isotopologues
        for i=0, nvals-1 do begin
            if (keys.trans[0] ge 0) then begin           ; if TRANS has value(s)...
                for j=0, n_elements(keys.trans)-1 do $   ; Include only identified transitions in total
                  tot[i] = tot[i] + total(chosen.(keys.trans[j]+npar-3)[i])   
            endif else $                                 ; add up the three line fluxes
              tot[i] = total(chosen.I10[i]) + total(chosen.I21[i]) + total(chosen.I32[i])
        endfor
    end
    else : begin             ; If PARAM is set to anything else
        for i=0, nvals-1 do begin
            index = where(chosen.(p) eq vals[i])
            ; Specific transitions
            if (keys.trans[0] ge 0) then begin           ; if TRANS has value(s)...
                for j=0, n_elements(keys.trans)-1 do $   ; Include identified transitions in total
                  tot[i] = tot[i] + total(chosen[index].(keys.trans[j]+npar-3)[keys.iso-1])   
            endif else $        ; add up the three line fluxes
              tot[i] = total(chosen[index].I10) + total(chosen[index].I21) + total(chosen[index].I32)
        endfor
    end
endcase


;;; SET-UP FOR PLOTTING
; Array for x-axis titles
pname = ['Luminosity of central star (L!D!9n!X!N)', $
         'Temperature of central star (K)', $
         'Dust mass of disk (M!D!9n!X!N)', $
         textoidl('\gamma'), $
         'R!Dc!N (AU)', $
         'H!D100!N (AU)', $
         'h', $
         'A!DV!N (mag)', $
         'T!Dfreeze!N (K)', $
         'Inclination (degrees)', $
         'Gas-to-Dust ratio', $
         'Isotopologues of CO', $
         'CO Transition' ]

; Histogram labels
case param of
    3 : labels = textoidl(['1\times10^{-5}','3\times10^{-5}','1\times10^{-4}','3\times10^{-4}'])
    12 : labels = textoidl(['^{12}CO','^{13}CO','C^{18}O'])
    13 : labels = textoidl(['1.3 mm','1-0','2-1','3-2'])
    else : labels = trim(vals)
endcase


;;; MAKE THE PLOT
if keyword_set(ps) then begin
    fpath = '~/Astro/699-2/results/hist'
    ps_open, fpath, /color, /en, thick=4
endif else cgdisplay, 800, 600 ;window, 0, retain=2, xsize=800, ysize=600

;cgdisplay, 800, 600
cgloadct, 33, clip=[10,245]
cgbarplot, tot, xtitle=pname[p], ytitle='CO flux (Jy)', $
           barnames=labels, background='white', chars=1.9, _extra=extrakey

if keyword_set(ps) then ps_close

stop

END

