;PRO MAKEDISKHIST_RFREEZE

; Wrapper for diskhist_rfreeze.pro

;lstar = [0.4, 0.6, 0.8, 1.0, 1.2]
;tstar = [3600, 3800, 4000, 4200]
lstar = [0.5, 1, 2, 5, 10]
tstar = [3000, 4000, 5000, 6000]
mdisk = [1e-6, 3e-6, 1e-5, 3e-5, 1e-4]
;gamma = [0., 0.5, 1., 1.5]
;rc = 100
;h100 = 10
;hscale = 1

ps=1

diskhist_rfreeze, 1, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
    gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, outpath='~/Astro/699-2/results/temp/Lstar_hist_rfreeze'
if n_elements(ps) eq 0 then begin
    print, 'Press ENTER for the next graph'
    dummy = get_kbrd()
endif
diskhist_rfreeze, 2, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
    gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, outpath='~/Astro/699-2/results/temp/Tstar_hist_rfreeze'
if n_elements(ps) eq 0 then begin
    print, 'Press ENTER for the next graph'
    dummy = get_kbrd()
endif
diskhist_rfreeze, 3, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
    gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, outpath='~/Astro/699-2/results/temp/Mdust_hist_rfreeze'
if n_elements(ps) eq 0 then begin
    print, 'Press ENTER for the next graph'
    dummy = get_kbrd()
endif
diskhist_rfreeze, 4, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
    gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, outpath='~/Astro/699-2/results/temp/gamma_hist_rfreeze'
if n_elements(ps) eq 0 then begin
    print, 'Press ENTER for the next graph'
    dummy = get_kbrd()
endif
diskhist_rfreeze, 5, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
    gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, outpath='~/Astro/699-2/results/temp/Rc_hist_rfreeze'
if n_elements(ps) eq 0 then begin
    print, 'Press ENTER for the next graph'
    dummy = get_kbrd()
endif
diskhist_rfreeze, 6, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
    gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, outpath='~/Astro/699-2/results/temp/H100_hist_rfreeze'
if n_elements(ps) eq 0 then begin
    print, 'Press ENTER for the next graph'
    dummy = get_kbrd()
endif
diskhist_rfreeze, 7, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
    gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, outpath='~/Astro/699-2/results/temp/h_hist_rfreeze'

;+
;  Reads the output plane temperature linear fits from the make_temp
;  simulations.
;      log(T) = a + b*log(R)       -- a and b for each disk
;  Makes histograms of R_freeze from disks with different values of the parameter
;  indicated by PARAM.
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate fits only from disks with those
;  parameter values.
;  If no value is specified for a given parameter, then the histograms are made
;  for disks of all values of that parameters (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 03/03/2013
;
;  INPUTS
;      PARAM - Make histograms for different values of this parameter.
;                 1 - L_star
;                 2 - T_star
;                 3 - M_disk
;                 4 - gamma
;                 5 - R_c
;                 6 - H_100
;                 7 - h
;              DEFAULT: 3 - M_disk
;              Any values supplied via keyword (see below) to the parameter
;                 indicated by PARAM are ignored.
;
;  KEYWORDS
;      FITS - "tempfits" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      OUTPATH - path for output postscript file.
;                 Default: ~/Astro/699-2/results/bartemp1
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the a and b values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, the a and b values from disks with all values
;    for that parameter will be used to make the histograms.
;      LSTAR - L_star
;      TSTAR - T_star
;      MDISK - M_disk
;      GAMMA - gamma
;      RC - R_c
;      H100 - H_100
;      HSCALE - h
;-

END

