;filepath='~/Astro/699-2/results/test9/'
;param=7
;ps=1
PRO DISKHIST_COUPLED_TOTAL, PARAM, FILEPATH=filepath, PS=ps, _EXTRA=extrakey

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  
;  HISTORY
;  Begun by Will Best (IfA), 1/29/2013
;
;  INPUTS
;      PARAM - Show totals for different values of this parameter.
;                 1 - L_star
;                 2 - T_star
;                 3 - M_disk
;                 4 - gamma
;                 5 - R_c
;                 6 - H_100
;                 7 - h
;                 8 - A_V
;                 9 - T_freeze
;                 10 - i
;                 11 - g/d ratio
;              DEFAULT: 3 - M_disk
;
;  KEYWORDS
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      PS - send output to postscript
;-

; get the data
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/'
restore, filepath+'sims.sav'

; Oh, the structure
;ntot = total(nsim) * ninc * ngas
;sims = replicate({L:0., T:0, Md:0., g:0., Rc:0, H100:0, h:0., AV:0., Tf:0, $
;                  in:0, gd:0, cont:0d, I10:[0., 0., 0.], I21:[0., 0., 0.], I32:[0., 0., 0.]}, ntot)
; cont = 1.3 mm continuum emission
; 1-0, 2-1, 3-2
; CO, 13CO, C18O

if n_elements(param) eq 0 then param = 3

p = param-1
vals = sims[uniq(sims.(p), sort(sims.(p)))].(p)
nvals = n_elements(vals)
tot = fltarr(nvals)
for i=0, nvals-1 do begin
    index = where(sims.(p) eq vals[i])
;    tot[i] = total(sims[index].I10) + total(sims[index].I21) + total(sims[index].I32)
    tot[i] = total(sims[index].I21)
endfor
labels = trim(vals)

if param eq 3 then labels = textoidl(['1\times10^{-5}','3\times10^{-5}','1\times10^{-4}','3\times10^{-4}'])

case param of
    3 : begin
        pname = 'Dust mass of disk (M!D!9n!X!N)'
    end
    4 : begin
        pname = textoidl('\gamma')
    end
    5 : begin
        pname = 'R!Dc!N (AU)'
    end
    6 : begin
        pname = 'H!D100!N (AU)'
    end
    7 : begin
        pname = 'h'
    end
    8 : begin
        pname = 'A!DV!N (mag)'
    end
    9 : begin
        pname = 'T!Dfreeze!N (K)'
    end
    10 : begin
        pname = 'inclination (degrees)'
    end
    11 : begin
        pname = 'Gas-to-Dust ratio'
    end
    else : begin
        print, 'Invalid parameter chosen.'
        return
    end
endcase

;; device, decomposed=0
;; lincolr_wb, /silent
if keyword_set(ps) then begin
    fpath = '~/Astro/699-2/results/hist'
    ps_open, fpath, /color, /en, thick=4
endif else cgdisplay, 800, 600 ;window, 0, retain=2, xsize=800, ysize=600

;cgdisplay, 800, 600
cgloadct, 33, clip=[10,245]
cgbarplot, alog10(tot), xtitle=pname, ytitle='Total CO (2-1) flux (arbitrary log units)', $
           barnames=labels, background='white', chars=1.9, _extra=extrakey
;;        charsize=1.5, color=0, backg=1, psym=10, /xlog
;; plot, vals, tot, xtitle=pname, ytitle='Combined CO flux from all model disks (arbitrary units)', $
;;        charsize=1.5, color=0, backg=1, psym=10, /xlog
;; ;          xrange=[10, 31], xtickname=sptaxis, xtickv=spt

if keyword_set(ps) then ps_close


END

