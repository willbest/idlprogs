;PRO MAKEDISKHIST_COUPLED

; Wrapper for diskhist_coupled.pro

mdisk = 3e-4
gamma = 1.2
rc = 100
h100 = 10
hscale = 1
av = 0.5
tfreeze = 20
incl = 30
gd = 100
;iso = [2,3]
trans = 3

diskhist_coupled, 3, filepath='~/Astro/699-2/results/', lstar=lstar, tstar=tstar, $
  mdisk=mdisk, gamma=gamma, rc=rc, h100=h100, hscale=hscale, av=av, tfreeze=tfreeze, $
  incl=incl, gd=gd, iso=iso, trans=trans;, /ylog;, ps=ps

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  Makes a histogram of fluxes from disks with different values of the parameter
;  indicated by PARAM.
;  If specific values for various other parameters are supplied (via keyword),
;  then the histogram will incorporate fluxes only from disks with those
;  parameter values.
;  If no value is specified for a given parameter, then the flux of is summed
;  for disks of all values of that parameters (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 02/09/2013
;
;  INPUTS
;      PARAM - Show totals for different values of this parameter.
;                 1 - L_star
;                 2 - T_star
;                 3 - M_disk
;                 4 - gamma
;                 5 - R_c
;                 6 - H_100
;                 7 - h
;                 8 - A_V
;                 9 - T_freeze
;                 10 - i
;                 11 - g/d ratio
;                 12 - Isotopologues of CO
;                 13 - CO transitions
;              DEFAULT: 3 - M_disk
;              Any values supplied via keyword (see below) to the parameter
;                 indicated by PARAM are ignored.
;
;  KEYWORDS
;      SIMS - "sims" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the sum of the fluxes from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, the sum of the fluxes from disks with all values
;    for that parameter will be used to make the histogram.
;      LSTAR - L_star
;      TSTAR - T_star
;      MDISK - M_disk
;      GAMMA - gamma
;      RC - R_c
;      H100 - H_100
;      HSCALE - h
;      AV - A_V
;      TFREEZE - T_freeze
;      INCL - i
;      GD - g/d ratio
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : continuum
;              2 : (1-0)
;              3 : (2-1)
;              4 : (3-2)
;-

END

