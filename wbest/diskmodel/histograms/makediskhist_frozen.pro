;PRO MAKEDISKHIST_FROZEN

; Wrapper for diskhist_frozen.pro

;mdisk = [1e-5, 3e-5, 1e-4]
;gamma = 1.2
;rc = 100
;h100 = 10
;hscale = 1

ps=1

diskhist_frozen, 3, filepath='~/Astro/699-2/results/frozen/', lstar=lstar, tstar=tstar, mgas=mgas, gamma=gamma, rc=rc, $
    tempa=tempa, tempb=tempb, tfreeze=tfreeze, ps=ps, outpath='~/Astro/699-2/results/frozen/Mgas_hist_frozen'
diskhist_frozen, 4, filepath='~/Astro/699-2/results/frozen/', lstar=lstar, tstar=tstar, mgas=mgas, gamma=gamma, rc=rc, $
    tempa=tempa, tempb=tempb, tfreeze=tfreeze, ps=ps, outpath='~/Astro/699-2/results/frozen/gamma_hist_frozen'
diskhist_frozen, 5, filepath='~/Astro/699-2/results/frozen/', lstar=lstar, tstar=tstar, mgas=mgas, gamma=gamma, rc=rc, $
    tempa=tempa, tempb=tempb, tfreeze=tfreeze, ps=ps, outpath='~/Astro/699-2/results/frozen/Rc_hist_frozen'
diskhist_frozen, 6, filepath='~/Astro/699-2/results/frozen/', lstar=lstar, tstar=tstar, mgas=mgas, gamma=gamma, rc=rc, $
    tempa=tempa, tempb=tempb, tfreeze=tfreeze, ps=ps, outpath='~/Astro/699-2/results/frozen/a_hist_frozen'
diskhist_frozen, 7, filepath='~/Astro/699-2/results/frozen/', lstar=lstar, tstar=tstar, mgas=mgas, gamma=gamma, rc=rc, $
    tempa=tempa, tempb=tempb, tfreeze=tfreeze, ps=ps, outpath='~/Astro/699-2/results/frozen/b_hist_frozen'
diskhist_frozen, 8, filepath='~/Astro/699-2/results/frozen/', lstar=lstar, tstar=tstar, mgas=mgas, gamma=gamma, rc=rc, $
    tempa=tempa, tempb=tempb, tfreeze=tfreeze, ps=ps, outpath='~/Astro/699-2/results/frozen/Tfreeze_hist_frozen'

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  Makes a histogram  of fluxes from disks with different values of the parameter
;  indicated by PARAM.
;  If specific values for various other parameters are supplied (via keyword),
;  then the histogram will incorporate fluxes only from disks with those
;  parameter values.
;  If no value is specified for a given parameter, then the flux of is summed
;  for disks of all values of that parameters (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Begun by Will Best (IfA), 1/29/2013
;
;  INPUTS
;      PARAM - Show totals for different values of this parameter.
;                 1 - L_star
;                 2 - T_star
;                 3 - M_disk
;                 4 - gamma
;                 5 - R_c
;                 6 - H_100
;                 7 - h
;              DEFAULT: 3 - M_disk
;              Any values supplied via keyword (see below) to the parameter
;                 indicated by PARAM are ignored.
;
;  KEYWORDS
;      SIMS - "sims" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the sum of the fluxes from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, the sum of the fluxes from disks with all values
;    for that parameter will be used to make the histogram.
;      LSTAR - L_star
;      TSTAR - T_star
;      MDISK - M_disk
;      GAMMA - gamma
;      RC - R_c
;      H100 - H_100
;      HSCALE - h
;      AV - A_V
;      TFREEZE - T_freeze
;      INCL - i
;      GD - g/d ratio
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : continuum
;              2 : (1-0)
;              3 : (2-1)
;              4 : (3-2)
;-

END

