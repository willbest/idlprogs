PRO DISKHIST_ISOGAS, PARAM, ISOGAS=isogas, FILEPATH=filepath, OUTPATH=outpath, PS=ps, _EXTRA=extrakey, $
                     LSTAR=lstar, TSTAR=tstar, MGAS=mgas, GAMMA=gamma, RC=rc, TEMPA=tempa, TEMPB=tempb, $
                     AVD=avd, TFREEZE=tfreeze, INCL=incl, ISO=iso, TRANS=trans

;+
;  Reads the output CO gas emission from the make_isogas_loop
;  simulations (vertically isothermal disk model).
;  Makes histograms of the frozen fraction from disks with different values of the parameter
;  indicated by PARAM.
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate fits only from disks with those
;  parameter values.
;  If no value is specified for a given parameter, then the histograms are made
;  for disks of all values of that parameters (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 03/19/2013
;
;  INPUTS
;      PARAM - Make histograms for different values of this parameter.
;                 1 - L_star
;                 2 - T_star
;                 3 - M_gas
;                 4 - gamma
;                 5 - R_c
;                 6 - a
;                 7 - b
;                 8 - A_V_dissoc
;                 9 - T_freeze
;                10 - incl
;                11 - Isotopologues of CO
;                12 - CO transitions
;              DEFAULT: 3 - M_gas
;              Any values supplied via keyword (see below) to the parameter
;                 indicated by PARAM are ignored.
;
;  KEYWORDS
;      ISOGAS - "isogas" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      OUTPATH - path for output postscript file.
;                 Default: ~/Astro/699-2/results/bartemp1
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the a and b values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, the a and b values from disks with all values
;    for that parameter will be used to make the histograms.
;      LSTAR - L_star
;      TSTAR - T_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RC - R_c
;      TEMPA - a
;      TEMPB - b
;      AVD - A_V_dissoc
;      TFREEZE - T_freeze
;      INCL - i
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;-


;;; DETERMINE THE PARAMETER VALUES FOR THE BARS
if n_elements(param) eq 0 then param = 3    ; Default histogram disk masses
p = param-1

; Check input parameter
npar = 12
if param lt 0 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif

;; case p of
;;     npar-1 : vals = [1, 2, 3]     ; Transitions
;;     npar-2 : vals = [1, 2, 3]        ; Isotopologues
;;     else : vals = sims[uniq(sims.(p), sort(sims.(p)))].(p)
;; endcase

;; nvals = n_elements(vals)


;;; GET THE DATA
if n_elements(isogas) eq 0 then begin
    if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/isogas/'
    restore, filepath+'isogas.sav'
endif


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Make a structure containing the values from the keywords
if not keyword_set(lstar) then lstar = -1
if not keyword_set(tstar) then tstar = -1
if not keyword_set(mgas) then mgas = -1
if not keyword_set(gamma) then gamma = -1
if not keyword_set(rc) then rc = -1
if not keyword_set(tempa) then tempa = -1
if not keyword_set(tempb) then tempb = -1
if not keyword_set(avd) then avd = -1
if not keyword_set(tfreeze) then tfreeze = -1
if not keyword_set(incl) then incl = -1
if not keyword_set(iso) then iso = [1,2,3]
if not keyword_set(trans) then trans = -1

keys = {lstar:lstar, tstar:tstar, mgas:mgas, gamma:gamma, rc:rc, a:tempa, b:tempb, $
        avd:avd, tfreeze:tfreeze, incl:incl, iso:iso, trans:trans}

;;; Build a large string, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters to select disks with the chosen parameters
for i=0, (npar-3) do begin
    if (keys.(i)[0] ge 0) then begin    ; if the keyword has value(s) and is not the PARAM, go on
        match, isogas[uniq(isogas.(i), sort(isogas.(i)))].(i), keys.(i), xx, ind  ; where parameter matches keyword value(s)
        nind = n_elements(ind)
        if ind[0] ge 0 then begin                    ; if parameter matches keyword value(s) at least once, go on
            if nind gt 1 then fstr = fstr + $
                   '(' + strjoin('(frozen.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
            fstr = fstr + '(frozen.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
            if nind gt 1 then fstr = fstr + ')'
            fstr = fstr + ' and '
        endif
    endif
endfor


;;; GET THE FLUX VALUES
; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'  ; replace the last ' and ' with a )
    dummy = execute(fstr)
    chosen = isogas[select]
endif else chosen = isogas


; Break the subset up by values of the chosen parameter (if one has been set as PARAM)
    frac = chosen.frac

case p of
    -1 : vals = 1
    npar-1 : vals = [1, 2, 3]     ; Transitions
    npar-2 : vals = [1, 2, 3]     ; Isotopologues
    else : vals = isogas[uniq(isogas.(p), sort(isogas.(p)))].(p)
endcase
nvals = n_elements(vals)

flux = fltarr(n_elements(chosen)/nvals, nvals)
for i=0, nvals-1 do begin
    index = where(chosen.(p) eq vals[i])
    frac[*,i] = chosen[index].frac
endfor

; Get the values
flux = fltarr(n_elements(chosen)/nvals, nvals)
case p of
    npar-1 : begin           ; If PARAM is set to the CO transitions
        for i=0, nvals-1 do begin
            flux[*,i] = chosen.(i+npar-1)[keys.iso-1]) ; Include only identified isotopologues in total
            endif else tot[i] = total(chosen.(i+npar-2))   ; Each bar is for a single transition
        endfor
    end
    npar-2 : begin           ; If PARAM is set to the CO isotopologues
        for i=0, nvals-1 do begin
            if (keys.trans[0] ge 0) then begin           ; if TRANS has value(s)...
                for j=0, n_elements(keys.trans)-1 do $   ; Include only identified transitions in total
                  tot[i] = tot[i] + total(chosen.(keys.trans[j]+npar-2)[i])   
            endif else $                                 ; add up the three line fluxes
              tot[i] = total(chosen.I10[i]) + total(chosen.I21[i]) + total(chosen.I32[i])
        endfor
    end
    else : begin             ; If PARAM is set to anything else
        for i=0, nvals-1 do begin
            index = where(chosen.(p) eq vals[i])
            ; Specific transitions
            if (keys.trans[0] ge 0) then begin           ; if TRANS has value(s)...
                for j=0, n_elements(keys.trans)-1 do $   ; Include identified transitions in total
                  flux[*,i] = flux[*,i] + total(chosen[index].(keys.trans[j]+npar-2)[keys.iso-1])   
            endif else $        ; add up the three line fluxes
              flux[*,i] = total(chosen[index].I10) + total(chosen[index].I21) + total(chosen[index].I32)
        endfor
    end
endcase


; Make labels for the histograms
case p of
    0 : label = ['L!Dstar!N = '+trim(vals)+' L!D!9n!X!N']
    1 : label = ['T!Dstar!N = '+trim(vals)+' K']
    2 : label = ['M!Dgas!N = '+string(vals, format='(e6.0)')+' M!D!9n!X!N']
    3 : label = [textoidl('\gamma')+' = '+trim(vals)]
    4 : label = ['R!Dc!N = '+trim(vals)+' AU']
    5 : label = ['a = '+trim(vals)]
    6 : label = ['b = '+trim(vals)]
    7 : label = ['T!Dfreeze!N = '+trim(vals)+' K']
    else : label = ' '
endcase

;;; PLOT THE HISTOGRAMS
; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/frozen/frozenfrac1'
    ps_open, outpath, /color, /en, thick=4
endif else window, 2, retain=2, xsize=800, ysize=800
!p.multi = [0, 1, nvals, 0, 0]
xmarg = [9,3]
xfrac = [min(chosen.frac), max(chosen.frac)]
yarr = [6, 3.8, 1.8, 1.2, 1, 0.8]
ystretch = yarr[nvals-1]
charsarr = [1.4, 1.4, 2.2, 2.2, 2.2]
chars = charsarr[nvals-1]

; Make the plots
for i=0, nvals-2 do begin
    ymarg = [ystretch*i, ystretch*(1-i)]

    plothist, frac[*,i], charsize=chars, backg=1, bin=.05, color=3, fcolor=3, /fill, xrange=xfrac, $
              xtickname=replicate(' ',6), ytickname=' ', yminor=2, xmargin=xmarg, ymargin=ymarg
    fracmed = median(frac[*,i])
    vline, fracmed, lines=2, color=0        ; Vertical line at median value
    legend, label[i], box=0, textcolor=0, charsize=0.9     ; label for the parameter
    fracleg = [' ', textoidl('\mu_{R_f}')+' = '+string(fracmed, format='(f5.2)'), $
             textoidl('\sigma_{R_f}')+' = '+string(stddev(frac[*,i]), format='(f5.2)')]
    legend, fracleg, box=0, textcolor=3, charsize=0.9     ; label for the median and scatter
endfor

if nvals eq 1 then ymarg = [4,2] else ymarg = [ystretch*(nvals-1),ystretch*(2-nvals)]

i = nvals-1
plothist, frac[*,i], xtitle='Frozen CO Fraction', backg=1, bin=.05, color=3, fcolor=3, /fill, $
          charsize=chars, xchars=1.1, xrange=xfrac, yminor=2, xmargin=xmarg, ymargin=ymarg
fracmed = median(frac[*,i])
vline, fracmed, lines=2, color=0                            ; Vertical line at median value
legend, label[i], box=0, textcolor=0, charsize=0.9       ; label for the parameter
fracleg = [' ', textoidl('\mu_{R_f}')+' = '+string(fracmed, format='(f5.2)'), $
         textoidl('\sigma_{R_f}')+' = '+string(stddev(frac[*,i]), format='(f5.2)')]
legend, fracleg, box=0, textcolor=3, charsize=0.9   ; label for the median and scatter

xyouts, .04, .52, 'Number of Disks', color=0, chars=2, align=0.5, orient=90, /normal

if keyword_set(ps) then ps_close
!p.multi = [0, 0, 1, 0, 0]

END

