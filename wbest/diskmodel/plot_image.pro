PRO PLOT_IMAGE, FILENAME=filename, PDF=pdf, PS=ps

;+
;  Plot an image of a model disk output by radmc3D.
;  
;  HISTORY
;  Written by Will Best (IfA), 11/18/2012
;  11/28/12 (WB): Switched from ps_open.pro to manual postcript call, so that
;                 the image would not stretch to fit a page.
;  11/30/12 (WB): Added PDF keyword
;
;  INPUTS
;      IMAGE - Structure containing image generated by radmc3D, as generated by
;              readimage()
;
;  KEYWORDS
;      FILENAME - file path+name for postcript output
;      PDF - If set, create pdf output
;      PS - If set, create postscript output
;-

lognum = 5

if keyword_set(ps) or keyword_set(pdf) then begin
    if n_elements(filename) eq 0 then filename = 'image' else begin
        filen2 = filename
        if strpos(filename,'.eps') le 0 then filename = filename+'.eps'
;        if strpos(filename,'.ps') ge 1 then filename = strmid(filename,0,strpos(filename,'.ps'))
    endelse
    set_plot,'ps',/interpolate
    device,filename=filename,bits_per_pixel=8 $
           ,xsize=8.5,ysize=8.5,xoff=0.0,yoff=8.5 $
           ,/inches,/encapsulated,/color, /port
;    cs=1.7
;    ct=4
endif

;endif else begin
;    xsize=1000
;    window,0,xsize=xsize,ysize=xsize*asp
;    cs=1.8
;    ct=1
;endelse

;; device, decomposed=0
;; if keyword_set(ps) then begin
;;     ps_open, filename, /en, /color, thick=4;, /portrait
;; endif
;; endif else begin
;;     window, lognum
;; endelse

loadct, 39
im = readimage()
plotimage, im, maxlog=lognum, /log, /au, /contour, dev=lognum

if keyword_set(ps) or keyword_set(pdf) then begin
  device,/close
  set_plot,'x'
  !p.font=-1
  print,"Created color postscript file: "+filename
endif

if keyword_set(pdf) then begin
    if keyword_set(ps) then cgps2pdf, filename, /showcmd else cgps2pdf, filename, /showcmd, /delete_ps
endif

;; if keyword_set(ps) then begin
;;     ps_close
;; ;    print, "Created color postscript file: "+filen2
;; endif


END

