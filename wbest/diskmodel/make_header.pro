pro make_header,name,RA,DEC

data=mrdfits('image.fits',0,h)
sxaddpar,h,'OBJECT',name
sxaddpar,h,'TELESCOP','SMA    '
sxaddpar,h,'NAXIS',4
sxaddpar,h,'CTYPE1','RA---SIN',before='CDELT1'
sxaddpar,h,'CRVAL1',RA,before='CDELT1'
sxaddpar,h,'CTYPE2','DEC--SIN',before='CDELT2'
sxaddpar,h,'CRVAL2',DEC,before='CDELT2'
sxaddpar,h,'NAXIS3',1,after='NAXIS2'
sxaddpar,h,'NAXIS4',1,after='NAXIS3'
sxaddpar,h,'CTYPE3','FREQ    ',after='CRPIX2'
sxaddpar,h,'CRVAL3',2.19457d11,after='CTYPE3'
sxaddpar,h,'CDELT3',1.968d9,after='CRVAL3'
sxaddpar,h,'CRPIX3',1.0,after='CDELT3'
sxaddpar,h,'CUNIT3','Hz      ',after='CRPIX3'
sxaddpar,h,'CTYPE4','STOKES  ',after='CUNIT3'
sxaddpar,h,'CRVAL4',1.0,after='CTYPE4'
sxaddpar,h,'CDELT4',1.0,after='CRVAL4'
sxaddpar,h,'CRPIX4',1.0,after='CDELT4'
sxaddpar,h,'CUNIT4','        ',after='CRPIX4'

;sxaddpar,h,'FREQ',2.25d11

; add 3rd and 4th dimension to array
; and use a slightly modified mwrfits procedure
; to preserve NAXIS = 4
; this needed for casa to work...
s = size(data)
data4 = reform(data,s[1],s[2],1,1)

fitsfile = name+'_model.fits'
;my_mwrfits,data4,fitsfile,h,/create
mwrfits,data4,fitsfile,h,/create

return
end
