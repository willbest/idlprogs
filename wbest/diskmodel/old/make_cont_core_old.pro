@radmc3dfits.pro

pro make_cont_core, mloop, gloop, rcloop, H100loop, hloop, $
                    filename=filename,fine=fine,PDF=pdf, plot=plot,ps=ps

;
; make an SED and mm continuum image
; 1. create a dust profile
; 2. use radmc3d to calculate dust temperature structure
;    - output model SED and image
; 3. plot results (just SED for now - you can add code to plot image...)
;
; -----------------------------------------------------------

; mark the start time
starttime = systime()

@make_cont_parameters_loop.pro
close,/all

; Set working directory
working = '~/Astro/699-2/radmc_output'
cd, working

if keyword_set(plot) then goto, plot

goto, debug
debug:

;;;;;;;;;;
; create dust profile
if keyword_set(fine) then $
  make_dust_profile,star=star,disk=disk,/fine $
else make_dust_profile,star=star,disk=disk


;;;;;;;;;;
; create star spectrum
make_star_spec,star=star,/silent


;;;;;;;;;;
; calculate dust temperature
spawn,'radmc3d mctherm'


;;;;;;;;;;
; output model continuum image at SMA observing wavelength
npix = 256
lambda_microns = 1333.3
cmd=string(format='("radmc3d image lambda ",f6.1," npix ",i0," incl ",f5.1," posang ",f6.1, " nostar noline")',lambda_microns,npix,INCL,PA)
print,cmd
spawn,cmd

; make a fits image
file_delete,'image.fits',/allow_nonexistent
radmcimage_to_fits,'image.out','image.fits',140.
file_delete,src+'_model.fits',/allow_nonexistent
make_header,src,RA,DEC


;;;;;;;;;;
; output SED
; first create camera wavelength file
readcol,'sed.txt',lambda,flux,format='f,f',skipline=10
sed_lambda = [lambda[0:n_elements(lambda)-2],[200., 350., 450., 850., 1333.3]]
get_lun,lun
sedfile='camera_wavelength_micron.inp'
file_delete,sedfile,/allow_nonexistent
openw,lun,sedfile
nlambda=n_elements(sed_lambda)
printf,lun,format='(3x,i0)',nlambda
for n=0,nlambda-1 do printf,lun,format='(3x,f9.4)',sed_lambda[n]
close,lun

; then calculate SED
file_delete,'sed.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum loadlambda incl ",f6.1," posang ",f6.1, " noline")',INCL,PA)
print,cmd
spawn,cmd
spawn, 'mv spectrum.out sed.out'


;;;;;;;;;;
; calculate visibilities and simulated image
;cmd=string(format='("casapy -c simobs.py -n",a0)',src)
;print,cmd
;spawn,cmd


;;;;;;;;;;
; plot results
plot:
if keyword_set(ps) or keyword_set(pdf) then begin
    if n_elements(filename) eq 0 then filename='sed.eps'
    plot_sed, src=src, disk=disk, filename=filename, incl=incl, pdf=pdf, ps=ps

    sedpos = strpos(filename, 'sed')
    filename2 = strmid(filename,0,sedpos)+'profile'+strmid(filename,sedpos+3,10)
    plot_profile, filename=filename2, pdf=pdf, ps=ps

    filename3 = strmid(filename,0,sedpos)+'image'+strmid(filename,sedpos+3,10)
    plot_image, filename=filename3, pdf=pdf, ps=ps
endif else begin
    plot_sed, src=src, disk=disk
    plot_profile
    plot_image
endelse

; display the start and finish times
if not keyword_set(plot) then begin
    print
    print, 'Start time for this simulation: '+starttime
    print, 'Finish time for this simulation: '+systime()
    print
endif

return
end
