PRO MAKE_CONT_LOOP

;+
;  Runs make_cont_core multiple times, varying the input disk parameters
;  
;  Output files are named sed1, sed2, etc.
;
;  HISTORY
;  Written by Will Best (IfA), 11/06/2012
;-

; mark the start time
startloop = systime()

; Path for output
outpath = '~/Astro/699-2/imagefiles/'

; model disk parameters
; m    = dust mass in units of Msun
; gamma = index for surface density profile
; rc   = characteristic radius in units of AU
; H100 = scale height at r=100AU in AU
; h    = power law exponent of H(r)

m_std = 1d-4
gamma_std = 1.
rc_std = 100.
H100_std = 10.
h_std = 1.1

marr = [1d-6, 1d-5, 1d-4, 1d-3, 1d-2]
garr = [0.5, 0.7, 0.9, 1.3]
rcarr = [50., 75., 150., 200.]
H100arr = [2., 5., 20., 50.]
harr = [0.9, 1.0, 1.2, 1.3]

mloop = m_std
gloop = gamma_std
rcloop = rc_std
H100loop = H100_std
hloop = h_std

c = 0

; Vary the m parameter
for i=0, n_elements(marr)-1 do begin
    mloop = marr[i]
    print
    print, 'Run number '+trim(c+1)
    print
    make_cont_core, mloop, gloop, rcloop, H100loop, hloop, $
                    filename=outpath+'sed'+trim(c+1)+'.eps', /ps
    c++
endfor
mloop = m_std

; Vary the gamma parameter
for i=0, n_elements(garr)-1 do begin
    gloop = garr[i]
    print
    print, 'Run number '+trim(c+1)
    print
    make_cont_core, mloop, gloop, rcloop, H100loop, hloop, $
                    filename=outpath+'sed'+trim(c+1)+'.eps', /ps
    c++
endfor
gloop = gamma_std

; Vary the rc parameter
for i=0, n_elements(rcarr)-1 do begin
    rcloop = rcarr[i]
    print
    print, 'Run number '+trim(c+1)
    print
    make_cont_core, mloop, gloop, rcloop, H100loop, hloop, $
                    filename=outpath+'sed'+trim(c+1)+'.eps', /ps
    c++
endfor
rcloop = rc_std

; Vary the H100 parameter
for i=0, n_elements(H100arr)-1 do begin
    H100loop = H100arr[i]
    print
    print, 'Run number '+trim(c+1)
    print
    make_cont_core, mloop, gloop, rcloop, H100loop, hloop, $
                    filename=outpath+'sed'+trim(c+1)+'.eps', /ps
    c++
endfor
H100loop = H100_std

; Vary the h parameter
for i=0, n_elements(harr)-1 do begin
    hloop = harr[i]
    print
    print, 'Run number '+trim(c+1)
    print
    make_cont_core, mloop, gloop, rcloop, H100loop, hloop, $
                    filename=outpath+'sed'+trim(c+1)+'.eps', /ps
    c++
endfor
hloop = h_std

; display the start and finish times
print
print, 'Done!!'
print
print, 'Start time for the whole run: '+startloop
print, 'Finish time for the whole run: '+systime()
print

END

