src='TEST'
RA=15*ten(4,17,33.73)
DEC=ten(28,20,46.9)
INCL=60.0   ;; best fit from uvfit gaussian
PA=0.0

; stellar parameters (luminosity and effective temperature)
star = {l:4.4d-1, t:3.705d3, g:3.81, m:0.61}

; model disk parameters
; m    = dust mass in units of Msun
; gamma = index for surface density profile
; rc   = characteristic radius in units of AU
; H100 = scale height at r=100AU in AU
; h    = power law exponent of H(r)
;disk = {m:1d-4, gamma:1.00, rc:100.0, H100:10.0, h:1.10}
disk = {m:mloop, gamma:gloop, rc:rcloop, H100:H100loop, h:hloop}

