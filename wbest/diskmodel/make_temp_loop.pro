PRO MAKE_TEMP_LOOP, OUTPATH=outpath

;+
;  Runs make_temp_core multiple times, varying the input disk parameters
;  
;  Output files are named sed1, sed2, etc.
;
;  HISTORY
;  Written by Will Best (IfA), 11/06/2012
;  02/09/2013 (WB): Major update for full looping
;  05/24/2013 (WB): Include Lstar and Tstar in the looping
;
;  KEYWORDS
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;
;  CALLS
;      make_temp_core
;-

; mark the start time
startloop = systime()

; working directory
working = '~/Astro/699-2/radmc_output/'

; set up output path
dataroot = '~/Astro/699-2/results/temp/dal4_output/'
;dataroot = '~/gasdisks/tempgrid/loop3_output/'
report = dataroot+'report.txt'

; model disk parameters
; lstar = stellar luminosity in units of Lsun
; tstar = stellar temperature in units of K
; m     = dust mass in units of Msun
; gamma = index for surface density profile
; rc    = characteristic radius in units of AU
; H100  = scale height at r=100AU in AU
; h     = power law exponent of H(r)

larr = 5.91d
tarr = 6000.
marr = 9d-5
garr = [0.0, 1.0]
rcarr = [50., 150.]
H100arr = [10., 20.]
harr = [1.0, 1.1]

;; larr = [4, 10]*.1d
;; tarr = [3800d, 4200d]
;; marr = [1d-5]
;; garr = [1.0]
;; rcarr = [100.]
;; H100arr = [10.]
;; harr = [1.05]

simnum = 1

; set up to check for previous incomplete run
file_flag = 0
if file_test(report) eq 1 then begin
    readcol, report, runnum, format='i,x,x', comment='#'
    if n_elements(runnum) eq 0 then runnum = 0   ; case where no report lines have yet been written
    file_flag = 1
endif

; Vary the l parameter
for i=0, n_elements(larr)-1 do begin
    lloop = larr[i]
    ; Vary the t parameter
    for i2=0, n_elements(tarr)-1 do begin
        tloop = tarr[i2]
        ; Vary the m parameter
        for i3=0, n_elements(marr)-1 do begin
            mloop = marr[i3]
            ; Vary the gamma parameter
            for i4=0, n_elements(garr)-1 do begin
                gloop = garr[i4]
                ; Vary the rc parameter
                for i5=0, n_elements(rcarr)-1 do begin
                    rcloop = rcarr[i5]
                    ; Vary the H100 parameter
                    for i6=0, n_elements(H100arr)-1 do begin
                        H100loop = H100arr[i6]
                        ; Vary the h parameter
                        for i7=0, n_elements(harr)-1 do begin
                            hloop = harr[i7]
                            print
                            print, 'Run number '+trim(simnum)
                            if file_flag eq 1 then begin
                                prev = where(runnum eq simnum)
                                if prev[0] ge 0 then begin
                                    print, '    This run is already complete.'
                                    simnum++
                                    continue
                                endif
                                file_flag = 0
                            endif
                            print
                            make_temp_core, lloop, tloop, mloop, gloop, rcloop, H100loop, hloop, simnum, $
                                            dataroot=dataroot, outpath=outpath, working=working
                            simnum++
                        endfor
                    endfor
                endfor
            endfor
        endfor
    endfor
endfor

spawn, 'mv '+working+'amr_grid.inp '+dataroot

; display the start and finish times
print
print, 'Done!!'
print
print, 'Start time for the whole run: '+startloop
print, 'Finish time for the whole run: '+systime()
print

END

