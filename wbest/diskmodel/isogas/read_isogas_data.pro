;nsets=2
;filepath='~/Astro/699-2/results/test9/'
;PRO READ_ISOGAS_DATA, FILEPATH=filepath, NSETS=nsets, SETNAMES=setnames;

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  
;  HISTORY
;  Written by Will Best (IfA), 03/19/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/isogas/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;-

; set up generic paths
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/isogas/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1
readpath = filepath

; Oh, the structure
nsim = intarr(nsets)
for i=0, nsets-1 do begin
    if nsets gt 1 then readpath = filepath+setnames+trim(i+1)+'_output/'
    cd, readpath
    nsim[i] = file_lines('report.txt') - 1
endfor
readcol, 'report.txt', inc, delim=' ', comment='#', format='x,x,x,x,x,a', numline=2, /silent
inc = strsplit(inc, ',', /ex)
ninc = n_elements(inc)
ntot = total(nsim) * ninc
isogas = replicate({L:0., T:0, Mg:0., g:0., Rc:0, a:0, b:0., avd:0., tf:0, in:0, frozen:0., $
                  I10:[0., 0., 0.], I21:[0., 0., 0.], I32:[0., 0., 0.]}, ntot)

; start the loops
counter = 0
for i=0, nsets-1 do begin
    if nsets gt 1 then begin
        readpath = filepath+setnames+trim(i+1)+'_output/'
        print, 'Loop #'+trim(i+1)
    endif
    cd, readpath
    readcol, 'report.txt', run, star, disk, temp, chem, incl, delim=' ', comment='#', format='f,a,a,a,a,a'

    num = nsim[i] *ninc
    L = rebin(float(strmid(star, 1, 1#strpos(star, '_T')-1)), num, /sample)
    T = rebin(fix(strmid(star, 1#strpos(star, '_T')+2, 1#strpos(star, '/')-1#strpos(star, '_T')-2)), num, /sample)
    Mg = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), num, /sample)
    g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rc')-1#strpos(disk, '_g')-2)), num, /sample)
    Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '/')-1#strpos(disk, '_Rc')-3)), num, /sample)
    a = rebin(float(strmid(temp, 1, 1#strpos(temp, '_b')-1)), num, /sample)
    b = rebin(float(strmid(temp, 1#strpos(temp, '_b')+2, 1#strpos(temp, '/')-1#strpos(disk, '_b')-2)), num, /sample)
    avd = rebin(float(strmid(chem, 3, 1#strpos(chem, '_Tf')-3)), num, /sample)
    Tf = rebin(fix(strmid(chem, 1#strpos(chem, '_Tf')+3, 1#strpos(chem, '/')-1#strpos(chem, '_Tf')-3)), num, /sample)
    in = rebin(fix(strsplit(strjoin(incl, ','), ',', /ex)), num, /sample)

    isogas[counter:counter+num-1].L = L
    isogas[counter:counter+num-1].T = T
    isogas[counter:counter+num-1].Mg = Mg
    isogas[counter:counter+num-1].g = g
    isogas[counter:counter+num-1].Rc = Rc
    isogas[counter:counter+num-1].a = a
    isogas[counter:counter+num-1].b = b
    isogas[counter:counter+num-1].avd = avd
    isogas[counter:counter+num-1].tf = tf
    isogas[counter:counter+num-1].in = in

    run = trim(rebin(run, num, /sample))
    star2 = reform(transpose(cmreplicate(star, ninc)), num)
    disk2 = reform(transpose(cmreplicate(disk, ninc)), num)
    temp2 = reform(transpose(cmreplicate(temp, ninc)), num)
    chem2 = reform(transpose(cmreplicate(chem, ninc)), num)
    for j=0, num-1 do begin
        readcol, star2[j]+disk2[j]+temp2[j]+chem2[j]+'i'+trim(in[j])+'/line_results_10_'+run[j]+'.i'+trim(in[j])+'.txt', $
                 co, co13, c18o, delim=' ', comment='%', format='f,f,f', /silent
        isogas[j+counter].I10 = [co, co13, c18o]

        readcol, star2[j]+disk2[j]+temp2[j]+chem2[j]+'i'+trim(in[j])+'/line_results_21_'+run[j]+'.i'+trim(in[j])+'.txt', $
                 co, co13, c18o, delim=' ', comment='%', format='f,f,f', /silent
        isogas[j+counter].I21 = [co, co13, c18o]

        readcol, star2[j]+disk2[j]+temp2[j]+chem2[j]+'i'+trim(in[j])+'/line_results_32_'+run[j]+'.i'+trim(in[j])+'.txt', $
                 co, co13, c18o, delim=' ', comment='%', format='f,f,f', /silent
        isogas[j+counter].I32 = [co, co13, c18o]

        readcol, star2[j]+disk2[j]+temp2[j]+chem2[j]+'i'+trim(in[j])+'/frozen_co.out', frozen, comment='%', format='f', /silent
        isogas[j+counter].frozen = frozen
    endfor

    counter = counter + num

endfor

cd, filepath
forprint, isogas, textout='isogas.txt', width=135, /nocomment, /silent
save, isogas, filename='isogas.sav'

print
print, 'Done!!'
print

END

