PRO MAKE_ISOGASLINE_RESULTS, STAR, DISK, TEMP, CHEM, INCL, GD=gd, WORKING=working

;+
; Integrate spectra, writing results out to line_results_xx.txt, where xx is the transition.
;
;  HISTORY
;  03/18/13 (WB): Hacked from make_line_results.pro
;
;  INPUTS
;      STAR - Structure containing stellar parameters:
;             l:luminosity, t:temperature, g:gravity, m:mass
;      DISK - Structure containing disk parameters:
;             m:mass, gamma:gamma, rc:R_c
;      TEMP - structure containing disk midplane temperature parameters
;             a:a, b:b
;      CHEM - Structure containing CO chemical parameters:
;             avd:A_V_dissoc, tf:T_freeze
;      INCL - Disk's inclination angle.  Default = 60 degrees.
;
;  KEYWORDS
;      WORKING - working directory to point to
;                Default: '~/Astro/699-2/radmc_output'
;-

@natconst

; rest wavelength in microns
lambda0_co10   = 2600.7576d
lambda0_co21   = 1300.4036557964414d
lambda0_co32   = 866.96337d

lambda0_13co10 = 2720.4063d
lambda0_13co21 = 1360.2279849627178d
lambda0_13co32 = 906.84625d

lambda0_c18o10 = 2730.7936d
lambda0_c18o21 = 1365.4216364738502d
lambda0_c18o32 = 910.30867d

lambda0 = { mco:[lambda0_co10, lambda0_co21, lambda0_co32], $
            m13co:[lambda0_13co10, lambda0_13co21, lambda0_13co32], $
            mc18o:[lambda0_c18o10, lambda0_c18o21, lambda0_c18o32] }

if not keyword_set(working) then working = '~/Astro/699-2/radmc_output'
;if not keyword_set(working) then working = '.'
cd, working

; Calculate integrated line intensities
trans = ['10', '21', '32']

for t=0, n_elements(trans)-1 do begin
    get_lun,lun
    openw,lun,'line_results_'+trans[t]+'.txt'
    if n_elements(star) ne 0 then printf, lun, '% Star:  '+$
      string(format='("L = ",e8.2," Lsun,  T = ",i0," K,  M = ",f4.2," Msun")', star.l, star.t, star.m)
    if n_elements(disk) ne 0 then printf, lun, '% Disk:  '+$
      string(format='("M = ",e8.2," Msun,  gamma = ",f4.2,",  Rc = ",f5.1," AU")', disk.m, disk.gamma, disk.rc)
    if n_elements(temp) ne 0 then printf, lun, '% Temperature:  '+$
      string(format='("a = ",f4.2,",  b = ",f5.2)', temp.a, temp.b)
    if n_elements(chem) ne 0 then printf, lun, '% Chemistry:  '+$
      string(format='("A_V_dissoc = ",f3.1," mag,  T_freeze = ",i2," K")', chem.avd, chem.tf)
    if n_elements(incl) ne 0 then $
      printf, lun, '% Inclination = '+trim(incl)+' degrees'
    if n_elements(star)+n_elements(disk)+n_elements(temp)+n_elements(chem)+$
      n_elements(incl)ne 0 then printf,lun,'%'
    printf,lun,'% Transition:  '+strmid(trans[t],0,1)+'-'+strmid(trans[t],1,1)
    printf,lun,'%'
    printf,lun,'% CO      13CO      C18O'
    printf,lun,'% -------------------------------'

    filein = string(format='("spectrum_co",a,".out")', trans[t])
    if not file_test(filein) then begin
        print,format='(a0," not found... exiting!")',filein
        return
    endif
    readcol,filein,format='d,d',lambda,F_co,skipline=3
    v_co = cc*(lambda-lambda0.mco[t])/lambda0.mco[t]
    v_co = v_co / 1d5

    filein = string(format='("spectrum_13co",a,".out")', trans[t])
    if not file_test(filein) then begin
        print,format='(a0," not found... exiting!")',filein
        return
    endif
    readcol,filein,format='d,d',lambda,F_13co,skipline=3
    v_13co = cc*(lambda-lambda0.m13co[t])/lambda0.m13co[t]
    v_13co = v_13co / 1d5

    filein = string(format='("spectrum_c18o",a,".out")', trans[t])
    if not file_test(filein) then begin
        print,format='(a0," not found... exiting!")',filein
        return
    endif
    readcol,filein,format='d,d',lambda,F_c18o,skipline=3
    v_c18o = cc*(lambda-lambda0.mc18o[t])/lambda0.mc18o[t]
    v_c18o = v_c18o / 1d5

; convert fluxes to Jy as observed at 140 pc
    F_co = F_co * 1d23/140.^2
    F_13co = F_13co * 1d23/140.^2
    F_c18o = F_c18o * 1d23/140.^2

; subtract continuum (assuming no line emission in the first channel)
    F_co = F_co - F_co[0]
    F_13co = F_13co - F_13co[0]
    F_c18o = F_c18o - F_c18o[0]

; integrate line emission (Jy km/s)
    I_co = 0.0d
    for i = 0,n_elements(v_co)-2 do I_co = I_co + F_co[i]*(v_co[i+1]-v_co[i])
    I_13co = 0.0d
    for i = 0,n_elements(v_13co)-2 do I_13co = I_13co + F_13co[i]*(v_13co[i+1]-v_13co[i])
    I_c18o = 0.0d
    for i = 0,n_elements(v_c18o)-2 do I_c18o = I_c18o + F_c18o[i]*(v_c18o[i+1]-v_c18o[i])

    printf,lun,format='(2x,i3,3(3x,f7.3))',I_co,I_13co,I_c18o
    printf,lun,'% ----------------------------------------'
;    close,lun
    free_lun,lun

endfor

END
