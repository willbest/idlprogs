@radmc3dfits.pro

PRO MAKE_ISOGAS_CORE, MLOOP, GLOOP, RCLOOP, ALOOP, BLOOP, AVDARR, TFARR, SIMNUM, $
                   DATAROOT=dataroot, FINE=fine, INCOMP=incomp, OUTPATH=outpath, $
                   PDF=pdf, PLOT=plot, PS=ps

;+
;  Generates gas density and thermal profiles, determines the CO freeze-out
;  zone, and calculates the integrated line intensities (CO isotopologues) 
;  for a model vertically isothermal protoplanetary disk.
;
;  Parameters for the star and disk should be entered in make_isogas_parameters_loop.pro
;  
;  HISTORY
;  Begun by Will Best (IfA), 03/18/2013
;
;  INPUTS
;      MLOOP, GLOOP, RCLOOP, ALOOP, BLOOP  - Parameters passed by
;             make_frozen_loop.pro, used by make_frozen_parameters_loop.pro
;      SIMNUM - simulation number.  Used to name output files.
;
;  KEYWORDS
;      DATAROOT - top level of the data output folder structure.
;                 report.txt goes here.
;      FINE - Use a finer grid for the dust continuum model
;      INCOMP - If set, check for an incomplete run
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;      PDF - If set, create pdf output
;      PLOT - If set, skip the continuum modeling (assuming that it is already
;             done) and go straight to plotting the SED, profiles, and image.
;      PS - If set, create postscript output
;
;  CALLS
;;      calclines
;      make_frozen_parameters_loop
;;      make_header
;;      make_line_results
;;      plot_image
;;      plot_profile
;;      plot_sed
;;      radmcimage_to_fits
;      trim
;-

goto, debug
debug:

; mark the start time
starttime = systime()

; Star and disk parameters
@make_isogas_parameters_loop.pro

; Generic working directory
working = '~/Astro/699-2/radmc_output'
;working = '.'

; Data output paths
if n_elements(dataroot) eq 0 then dataroot = '~/Astro/699-2/results/isogas/'
spawn, 'mkdir -p '+dataroot
level1 = 'L'+trim(star.l)+'_T'+trim(star.t)+'/'
level2 = 'Mg'+trim(disk.m)+'_g'+trim(disk.gamma)+'_Rc'+trim(disk.rc)+'/'
level3 = 'a'+trim(temp.a)+'_b'+trim(temp.b)+'/'

; Close any lingering open write files
close, /all

; Point to generic working directory
cd, working

; Start the report file if necessary
report = dataroot+'report.txt'
if file_test(report) eq 0 then begin
    openw, 33, report, width=200
    printf, 33, '#Run', 'Star', 'Disk', 'Temperature', 'Chemistry', 'Incl', $
            format='(a,t9,a,t24,a,t47,a,t63,a,t85,a)'
    close, 33
endif

; Check for iteration number
if n_elements(simnum) eq 0 then simnum = 0

; Vary the A_V_dissoc parameter
for iav=0, n_elements(avdarr)-1 do begin
    A_V_dissoc = avdarr[iav]

; Vary the t_freeze parameter
    for it=0, n_elements(tfarr)-1 do begin
        t_freeze = tfarr[it]
        level4 = 'AVd'+trim(A_V_dissoc)+'_Tf'+trim(t_freeze)+'/'
        chem = {avd:A_V_dissoc, tf:t_freeze}

; Vary the inclination parameter
        for ii=0, n_elements(iarr)-1 do begin
            incl = iarr[ii]

; make the output folders
            runnum = trim(simnum)+'.'+trim(n_elements(tfarr)*iav+it+1)
            numlabel = runnum+'.i'+trim(incl)
            level5 = 'i'+trim(incl)+'/'
            ips_out = dataroot+level1+level2+level3+level4+level5
            line_out = ips_out+'spectra/'
            spawn, 'mkdir -p '+line_out
            if keyword_set(outpath) then filename=outpath else filename=ips_out

; check for a previous incomplete run
            if keyword_set(incomp) then begin
    ; if the third line_results file already exists, write the report line
                if file_test(ips_out+'line_results_32_'+numlabel+'.txt') ne 0 then begin
                    print, 'Run '+runnum+' with inclination '+trim(incl) $
                           +' is already completed.'
                    continue
                endif
    ; if the spectrum files already exist, jump to line calculations
                spawn, 'ls '+line_out, speccheck
                if n_elements(speccheck) gt 1 then begin
                    print, 'Spectra already generated for inclination '+trim(incl)
                    print, '    Now calculating line strengths.'
                    goto, calclines
                endif
    ; if the image files already exit, jump to spectrum calculations
                ;; if file_test(ips_out+'image'+numlabel+'.eps') ne 0 then begin
                ;;     print, 'SED and images already generated for inclination '+trim(incl)
                ;;     print, '    Now generating spectra.'
                ;;     goto, spectra
                ;; endif
            endif

            if keyword_set(plot) then goto, plot

;;;;;;;;;;
plot:
; plot results
            ;; plot_sed, src=src, disk=disk, filename=filename+'sed'+numlabel+'.eps', incl=incl, pdf=pdf, ps=ps
            ;; plot_image, filename=filename+'image'+numlabel+'.eps', pdf=pdf, ps=ps

spectra:
; generate the emission spectra and profile plot
            calcisogaslines, incl, working=working, star=star, disk=disk, temp=temp, chem=chem ;, $
                ;filename=filename+'profile'+numlabel+'.eps', pdf=pdf, ps=ps

; move the frozen_co file over to the data directory
            spawn, 'cp frozen_co.out '+ips_out
; copy the spectrum files over to the data directories
            spawn, 'cp spectrum*out '+line_out

calclines:
; calculate the integrated line intensities
            make_isogasline_results, star, disk, temp, chem, incl, working=working

; move the line_results files over to the data directories
            spawn, 'mv line_results_10.txt '+ips_out+'line_results_10_'+numlabel+'.txt'
            spawn, 'mv line_results_21.txt '+ips_out+'line_results_21_'+numlabel+'.txt'
            spawn, 'mv line_results_32.txt '+ips_out+'line_results_32_'+numlabel+'.txt'

; delete the spectrum files in the working directory
            spawn, 'rm spectrum*out'

        endfor

; Write to the report file
        if keyword_set(incomp) then begin
            readcol, report, r2, format='f,x,x,x,x,x', comment='#'
            if n_elements(r2) gt 0 then begin
                prev = where(runnum eq r2)
                if prev[0] gt -1 then goto, finish    ; skip writing the report line if it's already there
            endif
        endif
        print
        print, 'Writing report line for run '+runnum
        openw, 33, report, width=200, /append
        printf, 33, runnum, level1, level2, level3, level4, strjoin(trim(iarr),','), $
                format='(a,t9,a,t24,a,t47,a,t63,a,t85,a)'
        close, 33

finish:
; display the start and finish times
        print
        print, 'Start time for this simulation: '+starttime
        print, 'Finish time for this simulation: '+systime()
        print

    endfor
endfor

return
END
