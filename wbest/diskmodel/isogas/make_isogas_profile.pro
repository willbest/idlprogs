@readradmc.pro

;; star = {l:4.4d-1, t:3.705d3, g:3.81, m:0.61}
;; disk = {m:1e-3, gamma:1., rc:100.}
;; temp = {a:2.0, b:-0.55}
;; chem = {avd:1.0, tf:20.}
;; working = '~/Astro/699-2/radmc_output'
;; cd, working
PRO MAKE_ISOGAS_PROFILE, STAR=star, DISK=disk, TEMP=temp, CHEM=chem

;+
; Determine the fraction of CO that is frozen out in a protoplanetary disk,
; based on the freeze-out radius for the midplane of the disk.
;
;  HISTORY
;  Adapted from make_co_profile.pro by Will Best (IfA), 03/15/13
;
;  KEYWORDS
;      STAR - Structure containing stellar parameters:
;             l:luminosity, t:temperature, g:gravity, m:mass
;      DISK - Structure containing disk parameters:
;             m:mass, gamma:gamma, rc:R_c
;      TEMP - structure containing disk midplane temperature parameters
;             a:a, b:b
;      CHEM - Structure containing CO chemical parameters:
;             avd:A_V_dissoc, tf:T_freeze
;-

@natconst.pro

; CO abundance (relative to H2) and isotope fractions
x_co = 1d-4
f_13co = 1./70d
f_c18o = 1./500d

; use canned radmc3d procedure to read in the disk grid
struct = read_data()
minrho = 1d-23
mgas = disk.m * MS        ; arbitrary, for freeze-out zone calculation
mstar = star.m * MS
Rc = disk.rc * AU

; read in polar coordinate grid parameters
r = struct.grid.r/au
nr = struct.grid.nr
theta = struct.grid.theta
ntheta = struct.grid.ntheta
nphi = struct.grid.nphi
if (nphi ne 1) then begin
  print,'Oops... these are not polar coordinates!'
  stop
endif

; establish the mid-plane temperature based on a and b
; log T = a + b log R, so    T = 10^(a + b log R) = 10^a * 10^(b log R) = 10^a * R^b
t_gas = dblarr(nr,ntheta)       
t_gas[*,ntheta-1] = 10^temp.a * r^temp.b    ; midplane temperature values from a and b parameters

; establish the vertical temperature structure
t_gas[*,0:ntheta-2] = cmreplicate(t_gas[*,ntheta-1], ntheta-1)       ; vertically isothermal

; Get the scale height
Hr = disk_gas_scaleheight(r, t_gas[*,ntheta-1], mstar=mstar)

; calculate CO gas number density
rho = disk_gas_density(r*AU, theta, Hr*AU, gamma=disk.gamma, mgas=mgas, minrho=minrho, $
                       rc=Rc, temp=t_gas[*,ntheta-1], mstar=mstar)
n_h2 = rho / (muh2 * mp)
n_co = x_co * n_h2
n_co_min = x_co * minrho / (muh2 * mp)

; calculate total N_CO
zint = dblarr(nr)
for i=0, nr-1 do zint[i] = int_tabulated(tan(!dpi/2. - theta)*r[i], n_co[i,*], /sort)
n_tot = int_tabulated(r, r*zint)

; CO depletion due to freeze-out
freeze = where(t_gas lt chem.tf, nfreeze)
if (nfreeze gt 0) then n_co[freeze] = n_co_min

; calcualte N_CO after freeze-out
zintf = dblarr(nr)
for i=0, nr-1 do zintf[i] = int_tabulated(tan(!dpi/2. - theta)*r[i], n_co[i,*], /sort)
n_unf = int_tabulated(r, r*zintf)

frozen = (n_tot - n_unf) / n_tot
print, 'Fraction of CO gas frozen out: '+trim(frozen)

; dissociation in upper layer (above A_V < A_V_dissoc)
; use N(H2)/A_V = 1e21 cm-2/mag
; and assumes H2 self-shielding (not dust) prevents CO dissociation
if n_elements(chem) eq 0 then A_V_dissoc = 0.5d else A_V_dissoc = chem.avd
for i=0, nr-1 do begin
    A_V = 0.0d
    j = 0
    while (A_V lt A_V_dissoc and j lt ntheta-1) do begin
        j = j+1
        A_V = A_V + n_h2[i,j]*r[i]*au*(theta[j]-theta[j-1]) / sin(theta[j])^2 / 1d21
    endwhile
    n_co[i,0:j] = n_co_min
endfor


; Write the molecular density files
file_delete,'numberdens_co.inp',/allow_nonexistent
openw,1,'numberdens_co.inp'
printf,1,1
printf,1,struct.grid.ncell
for k=0,nphi-1 do begin
   for j=0,ntheta-1 do begin
      for i=0,nr-1 do begin
         printf,1,n_co[i,j]
      endfor
   endfor
endfor
close,1

file_delete,'numberdens_13co.inp',/allow_nonexistent
openw,2,'numberdens_13co.inp'
printf,2,1
printf,2,struct.grid.ncell
for k=0,nphi-1 do begin
   for j=0,ntheta-1 do begin
      for i=0,nr-1 do begin
         printf,2,f_13co*n_co[i,j]
      endfor
   endfor
endfor
close,2

file_delete,'numberdens_c18o.inp',/allow_nonexistent
openw,3,'numberdens_c18o.inp'
printf,3,1
printf,3,struct.grid.ncell
for k=0,nphi-1 do begin
   for j=0,ntheta-1 do begin
      for i=0,nr-1 do begin
         printf,3,f_c18o*n_co[i,j]
      endfor
   endfor
endfor
close,3


; Write the frozen fraction
file_delete, 'frozen_co.out', /allow_nonexistent
openw, 2, 'frozen_co.out'
printf, 2, '% Star:  '+string(format='("L = ",f4.2," Lsun,  T = ",i0," K,  M = ",f4.2," Msun")', $
                              star.l, star.t, star.m)
printf, 2, '% Disk:  '+string(format='("M = ",e8.2," Msun,  gamma = ",f4.2,",  Rc = ",f5.1," AU")', $
                              disk.m, disk.gamma, disk.rc)
printf, 2, '% Temperature:  '+string(format='("a = ",f4.2,",  b = ",f5.2)', temp.a, temp.b)
printf, 2, '% Chemistry:  '+string(format='("A_V_dissoc = ",f3.1,",  T_freeze = ",i2," K")', $
                                   chem.avd, chem.tf)
printf, 2
printf, 2, '% Freeze-out fraction of gas'
printf, 2, string(format='(f5.3)', frozen)
close, 2 

return
end
