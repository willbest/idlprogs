src=''
RA=15*ten(4,17,33.73)
DEC=ten(28,20,46.9)
;INCL = iloop
PA = 0.0

; stellar parameters (luminosity and effective temperature)
star = {l:4.4d-1, t:3.705d3, g:3.81, m:0.61}

; model disk parameters
; m    = dust mass in units of Msun
; gamma = index for surface density profile
; rc   = characteristic radius in units of AU
; H100 = scale height at r=100AU in AU
; h    = power law exponent of H(r)
;disk = {m:1d-6, gamma:1.00, rc:100.0, H100:10.0, h:1.10}
disk = {m:mloop, gamma:gloop, rc:rcloop}

; Temperature parameters for the disk
;Rfarr = [10., 20., 30., 40.]
temp = {a:aloop, b:bloop}

; geometric parameters
iarr = [0., 30., 60.]

