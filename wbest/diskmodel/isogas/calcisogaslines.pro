PRO CALCISOGASLINES, INCL, WORKING=working, _EXTRA=extrakey

;+
; Create CO number density file and calculate line spectra.
;
;  HISTORY
;  03/18/13 (WB):  Hacked from calclines.pro
;
;  INPUTS
;      INCL - Disk's inclination angle.  Default = 60 degrees.
;
;  KEYWORDS
;      WORKING - working directory to point to
;                Default: '~/Astro/699-2/radmc_output'
;
;      _EXTRA can be used to pass star, disk, temp, and chem through 
;      calcisogaslines.pro to plot_profile.pro and make_isogas_profile.pro.
;-

@natconst

; position angle
if n_elements(incl) eq 0 then incl = 60.
PA=0.0

; set number of spectral channels over line width
nchan = 20

if not keyword_set(working) then working = '~/Astro/699-2/radmc_output'
cd, working

;plot_profile, _extra=extrakey

make_isogas_profile, _extra=extrakey

print,'---> Making CO 1-0 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 1 iline 1 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_co10.out', /overwrite

print,'---> Making CO 2-1 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 1 iline 2 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_co21.out', /overwrite

print,'---> Making CO 3-2 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 1 iline 3 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_co32.out', /overwrite


print,'---> Making 13CO 1-0 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 2 iline 1 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_13co10.out', /overwrite

print,'---> Making 13CO 2-1 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 2 iline 2 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_13co21.out', /overwrite

print,'---> Making 13CO 3-2 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 2 iline 3 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_13co32.out', /overwrite


print,'---> Making C18O 1-0 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 3 iline 1 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
  spawn,cmd
file_move,'spectrum.out', 'spectrum_c18o10.out', /overwrite

print,'---> Making C18O 2-1 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 3 iline 2 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_c18o21.out', /overwrite

print,'---> Making C18O 3-2 spectrum'
file_delete,'spectrum.out',/allow_nonexistent
cmd=string(format='("radmc3d spectrum noscat imolspec 3 iline 3 widthkms 10.0 linenlam ",i0," incl ",f6.1," posang ",f6.1)',nchan,INCL,PA)
spawn,cmd
file_move,'spectrum.out', 'spectrum_c18o32.out', /overwrite

print,'---> *** calcfreeze complete! ***' 

return
end
