pro readsed,sedfile,Av,lambda,flux

line=''
get_lun,lun
openr,lun,sedfile

for n=1,4 do readf,lun,line
readf,lun,line
reads,strmid(line,9,5),Av,format='(f0)'
readf,lun,line
reads,strmid(line,9,5),Teff,format='(f0)'
readf,lun,line
reads,strmid(line,9,5),Mbol,format='(f0)'

for n=1,5 do readf,lun,line
i=0
lambda=fltarr(99)
flux=fltarr(99)
while ~eof(lun) do begin
  readf,lun,line
  reads,line,l,f,format='(f0,f0)'
  lambda[i]=l
  flux[i]=f
  i=i+1
endwhile
lambda=lambda[0:i-1]
flux=flux[0:i-1]
close,lun

return
end
