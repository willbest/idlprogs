PRO MAKE_CONT_LOOP, OUTPATH=outpath

;+
;  Runs make_cont_core multiple times, varying the input disk parameters
;  
;  Output files are named sed1, sed2, etc.
;
;  HISTORY
;  Written by Will Best (IfA), 11/06/2012
;  02/09/2013 (WB): Major update for full looping
;
;  KEYWORDS
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;
;  CALLS
;      make_cont_core
;-

; mark the start time
startloop = systime()

; model disk parameters
; m    = dust mass in units of Msun
; gamma = index for surface density profile
; rc   = characteristic radius in units of AU
; H100 = scale height at r=100AU in AU
; h    = power law exponent of H(r)

;marr = [1d-6, 3d-6, 1d-5, 3d-5, 1d-4, 3d-4]
;garr = [0.0, 0.5, 1.0, 1.5]
;rcarr = [50., 100., 150.]
;H100arr = [5., 10., 15., 20.]
;harr = [1.0, 1.05, 1.1, 1.15]

marr = [1d-5, 1d-4]
garr = [1.0]
rcarr = [100.]
H100arr = [10.]
harr = [1.0, 1.1]

simnum = 1

; set up output path
dataroot = '~/Astro/699-2/test_output/'
;dataroot = '~/data_output/'
report = dataroot+'report.txt'

; Vary the m parameter
for i=0, n_elements(marr)-1 do begin
    mloop = marr[i]
    ; Vary the gamma parameter
    for i2=0, n_elements(garr)-1 do begin
        gloop = garr[i2]
        ; Vary the rc parameter
        for i3=0, n_elements(rcarr)-1 do begin
            rcloop = rcarr[i3]
            ; Vary the H100 parameter
            for i4=0, n_elements(H100arr)-1 do begin
                H100loop = H100arr[i4]
                ; Vary the h parameter
                for i5=0, n_elements(harr)-1 do begin
                    hloop = harr[i5]
                    print
                    print, 'Run number '+trim(simnum)
                    print
                    make_cont_core, mloop, gloop, rcloop, H100loop, hloop, simnum, $
                                          dataroot=dataroot, outpath=outpath, /pdf, /ps
                    simnum++
                endfor
            endfor
        endfor
    endfor
endfor

; display the start and finish times
print
print, 'Done!!'
print
print, 'Start time for the whole run: '+startloop
print, 'Finish time for the whole run: '+systime()
print

END

