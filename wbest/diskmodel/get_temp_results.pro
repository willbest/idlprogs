filepath='~/Astro/699-2/test_output/'
outpath='~/Astro/699-2/results/test/'
;PRO GET_TEMP_RESULTS, FILEPATH=filepath, OUTPATH=outpath, SETNAMES=setnames

;+
;  Reads the output temperatures from the make_temp_... simulations.
;  Specifically, obtains the report.txt file(s) from the top of the output
;  directory tree.  
;  Obtains the dust_temperature.dat files from the directories, and copies them into
;  outpath.
;  
;  HISTORY
;  Written by Will Best (IfA), 2/9/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/tempgrid/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      OUTPATH - folder to receive the report.txt and line_results files.
;                Default: ~/Astro/699-2/results/test/
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;-

; mark the start time
starttime = systime()

; set up generic paths
if not keyword_set(filepath) then filepath = '~/tempgrid/'
if not keyword_set(outpath) then outpath = '~/Astro/699-2/results/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1

; start the loops
for i=0, nsets-1 do begin

; set up paths for this loop
    readpath = filepath
    writepath = outpath
    if nsets gt 1 then begin
        readpath = readpath+setnames+trim(i+1)+'_output/'
        writepath = writepath+setnames+trim(i+1)+'/'
    endif

; build the list of files to read over
    print, 'Building list of files'
    filelist = 'report.txt'
    
    cd, readpath
    readcol, filelist, run, star, disk, $
             delim=' ', comment='#', format='a,a,a'
    for j=0, n_elements(run)-1 do filelist = [filelist, $
          star[j]+disk[j]+'/dust_temperature'+run[j]+'.dat' ]

; create the tarballing and copying commands
    tarcom = 'tar -cf '+'tempout.tar '+strjoin(filelist, ' ')
    copcom = readpath+'tempout '+writepath
    untarcom = 'tar -xf '+writepath+'tempout.tar'

; copy the files
    spawn, 'mkdir -p '+outpath
    print, 'Making tarball of files to copy'
    spawn, tarcom
    print, 'Copying the tarball'
    copcom = 'mv '+readpath+'tempout.tar '+writepath
    spawn, copcom
    print, 'Unpacking the tarball'
    cd, writepath
    spawn, untarcom
endfor

print
print, 'Done!!'
print
print, 'Start time: '+starttime
print, 'Finish time: '+systime()
print

END

