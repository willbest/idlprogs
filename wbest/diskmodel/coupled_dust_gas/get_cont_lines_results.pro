filepath='~/Astro/699-2/test_outputs/test9_output/'
outpath='~/Astro/699-2/results/test9/'
;PRO GET_CONT_LINES_RESULTS, FILEPATH=filepath, NSETS=nsets, OUTPATH=outpath, $
;                     SETNAMES=setnames;, SERVER=server

;+
;  Specifically, obtains the report.txt file(s) from the top of the output
;  directory tree.  
;  Obtains the line_results_... files from the directories, and copies them into
;  outpath.
;  
;  HISTORY
;  Written by Will Best (IfA), 1/27/2013
;  01/27/13 (WB): Disabled remote server stuff.
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/gasgrid/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      OUTPATH - folder to receive the report.txt and line_results files.
;                Default: ~/Astro/699-2/results/
;      SERVER - DISABLED. server where the output files are stored.
;               If not set, files are assumed to be on this computer. 
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;-

; mark the start time
starttime = systime()

; set up generic paths
if not keyword_set(filepath) then filepath = '~/gasgrid/'
;if keyword_set(server) then getpath = 'wbest@'+server+':'+getpath
if not keyword_set(outpath) then outpath = '~/Astro/699-2/results/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1

; start the loops
for i=0, nsets-1 do begin

; set up paths for this loop
    readpath = filepath
    writepath = outpath
    if nsets gt 1 then begin
        readpath = readpath+setnames+trim(i+1)+'_output/'
        writepath = writepath+setnames+trim(i+1)+'/'
    endif

; build the list of files to read over
    print, 'Building list of files'
    filelist = 'report.txt'
    
    cd, readpath
    readcol, 'report.txt', run, star, disk, chem, incl, gd, $
             delim=' ', comment='#', format='a,a,a,a,a,a'
    for j=0, n_elements(run)-1 do begin
        incl2 = strsplit(incl[j], ',', /ex)
        gd2 = strsplit(gd[j], ',', /ex)
        for k=0, n_elements(incl2)-1 do filelist = [filelist, $
          star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/line_results_10_'+run[j]+'.i'+incl2[k]+'.txt', $
          star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/line_results_21_'+run[j]+'.i'+incl2[k]+'.txt', $
          star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/line_results_32_'+run[j]+'.i'+incl2[k]+'.txt', $
          star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/sed'+run[j]+'.i'+incl2[k]+'.out' ]
    endfor

; create the tarballing and copying commands
    tarcom = 'tar -cf '+'lineout.tar '+strjoin(filelist, ' ')
    copcom = readpath+'lineout '+writepath
    untarcom = 'tar -xf '+writepath+'lineout.tar'

; copy the files
    spawn, 'mkdir -p '+outpath
    ;; if keyword_set(server) then begin
    ;;     print, 'Making tarball of files to copy'
    ;;     spawn, 'ssh wbest@'+server+' '+tarcom
    ;;     print, 'Copying the tarball'
    ;;     copcom = 'scp wbest@'+server+':'+readpath+'lineout.tar '+writepath
    ;;     spawn, copcom
    ;; endif else begin
        print, 'Making tarball of files to copy'
;        cd, readpath
        spawn, tarcom
        print, 'Copying the tarball'
        copcom = 'mv '+readpath+'lineout.tar '+writepath
        spawn, copcom
    ;; endelse
    print, 'Unpacking the tarball'
    cd, writepath
    spawn, untarcom
endfor

print
print, 'Done!!'
print
print, 'Start time: '+starttime
print, 'Finish time: '+systime()
print

END

