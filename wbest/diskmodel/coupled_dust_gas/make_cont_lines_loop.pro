PRO MAKE_CONT_LINES_LOOP, OUTPATH=outpath

;+
;  Runs make_cont_lines_core multiple times, varying the input disk parameters
;  
;  Output files are named sed1, sed2, etc.
;
;  HISTORY
;  Written by Will Best (IfA), 12/01/2012
;
;  KEYWORDS
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;
;  CALLS
;      make_cont_lines_core
;-

; mark the start time
startloop = systime()

; model disk parameters
; m    = dust mass in units of Msun
; gamma = index for surface density profile
; rc   = characteristic radius in units of AU
; H100 = scale height at r=100AU in AU
; h    = power law exponent of H(r)

;marr = [1d-5, 1d-4, 1d-3]
;garr = [0.6, 0.9, 1.2]
;rcarr = [50., 100., 150.]
;H100arr = [3., 10., 30.]
;harr = [0.9, 1.1, 1.3]

marr = [1d-5]
garr = [1.0]
rcarr = [100.]
H100arr = [10.]
harr = [1.1]

simnum = 1

; set up output path
dataroot = '~/Astro/699-2/test_output/'
;dataroot = '~/data_output/'
report = dataroot+'report.txt'

; set up to check for previous incomplete run
file_flag = 0
if file_test(report) eq 1 then begin
    readcol, report, runnum, format='f,x,x,x,x,x', comment='#'
    if n_elements(runnum) eq 0 then runnum = 0   ; case where no report lines have yet been written
    file_flag = 1
endif

; Vary the m parameter
for i=0, n_elements(marr)-1 do begin
    mloop = marr[i]
    ; Vary the gamma parameter
    for i2=0, n_elements(garr)-1 do begin
        gloop = garr[i2]
        ; Vary the rc parameter
        for i3=0, n_elements(rcarr)-1 do begin
            rcloop = rcarr[i3]
            ; Vary the H100 parameter
            for i4=0, n_elements(H100arr)-1 do begin
                H100loop = H100arr[i4]
                ; Vary the h parameter
                for i5=0, n_elements(harr)-1 do begin
                    hloop = harr[i5]
                    print
                    print, 'Run number '+trim(simnum)
                    if file_flag eq 1 then begin
                        prev = where(simnum eq round(runnum))
                        if n_elements(prev) eq 4 then begin
                            print, '    This run is already complete.'
                            simnum++
                            continue
                        endif else incomp = 1
                        file_flag = 0
                    endif
                    print
                    make_cont_lines_core, mloop, gloop, rcloop, H100loop, hloop, avdarr, tfarr, simnum, $
                                          dataroot=dataroot, incomp=incomp, outpath=outpath, /pdf, /ps
                    incomp = 0
                    simnum++
                endfor
            endfor
        endfor
    endfor
endfor

print
print, 'Done!!'
print
print, 'Start time for the whole run: '+startloop
print, 'Finish time for the whole run: '+systime()
print

END

