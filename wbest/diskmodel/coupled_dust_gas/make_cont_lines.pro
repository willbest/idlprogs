@radmc3dfits.pro

PRO MAKE_CONT_LINES, FINE=fine, GD=gd, OUTPATH=outpath, PDF=pdf, PLOT=plot, PS=ps

;+
;  Generates an SED, mm continuum image, spectra, and integrated line
;  intensities (CO isotopologues) for a model protoplanetary disk.
;  1. Creates a dust profile
;  2. Uses radmc-3d to calculate dust temperature structure
;     - output model SED, profiles and image
;  3. plot results
;  4. Calculate spectra and line intensities
;
;  Parameters for the star and disk should be entered in make_cont_lines_parameters.pro
;  
;  HISTORY
;  Begun by Will Best (IfA), 11/27/2012
;
;  KEYWORDS
;      FINE - Use a finer grid for the dust continuum model
;      GD - Scalar or vector of gas-to-dust ratios to be used in calculating the
;           line spectra.
;           Default: [1,3,10,30,100,300]
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;      PDF - If set, create pdf output
;      PLOT - If set, skip the continuum modeling (assuming that it is already
;             done) and go straight to plotting the SED, profiles, and image.
;      PS - If set, create postscript output
;-

; mark the start time
starttime = systime()

; Star and disk parameters
@make_cont_lines_parameters.pro
iarr = [0., 30., 60.]

; Gas-to-dust ratio
gd = [1,3,10,30,100,300]

; Generic working directory
working = '~/Astro/699-2/radmc_output'

; Data output paths
dataroot = '~/Astro/699-2/test_output/'
level1 = 'L'+trim(star.l)+'_T'+trim(star.t)+'/'
level2 = 'Md'+trim(disk.m)+'_g'+trim(disk.gamma)+'_Rc'+trim(disk.rc)+'_H'+trim(disk.H100)+'_h'+trim(disk.h)+'/'

; Close any lingering open write files
close, /all

; Point to generic working directory
cd, working

goto, debug
debug:

for i=0, n_elements(iarr)-1 do begin
    incl = iarr[i]

    if keyword_set(plot) then goto, plot

; make the output folders
    level3 = 'i'+trim(incl)+'/'
    ips_out = dataroot+level1+level2+level3
    line_out = ips_out+'spectra'
    spawn, 'mkdir -p '+line_out

;;;;;;;;;;
; create dust profile
    if keyword_set(fine) then $
      make_dust_profile,star=star,disk=disk,/fine $
    else make_dust_profile,star=star,disk=disk

;;;;;;;;;;
; create star spectrum
    make_star_spec,star=star,/silent

;;;;;;;;;;
; calculate dust temperature
    spawn,'radmc3d mctherm'

;;;;;;;;;;
; output model continuum image at SMA observing wavelength
    npix = 256
    lambda_microns = 1333.3
    cmd=string(format='("radmc3d image lambda ",f6.1," npix ",i0," incl ",f5.1," posang ",f5.1, " nostar noline")',lambda_microns,npix,INCL,PA)
    print,cmd
    spawn,cmd

; make a fits image
    file_delete,'image.fits',/allow_nonexistent
    radmcimage_to_fits,'image.out','image.fits',140.
    file_delete,src+'_model.fits',/allow_nonexistent
    make_header,src,RA,DEC


;;;;;;;;;;
; output SED
; first create camera wavelength file
    readcol,'sed.txt',lambda,flux,format='f,f',skipline=10
    sed_lambda = [lambda[0:n_elements(lambda)-2],[200., 350., 450., 850., 1333.3]]
    get_lun,lun
    sedfile='camera_wavelength_micron.inp'
    file_delete,sedfile,/allow_nonexistent
    openw,lun,sedfile
    nlambda=n_elements(sed_lambda)
    printf,lun,format='(3x,i0)',nlambda
    for n=0,nlambda-1 do printf,lun,format='(3x,f9.4)',sed_lambda[n]
    close,lun

; then calculate SED
    file_delete,'sed.out',/allow_nonexistent
    cmd=string(format='("radmc3d spectrum loadlambda incl ",f5.1," posang ",f5.1, " noline")',INCL,PA)
    print,cmd
    spawn,cmd

; copy files to data directory
    spawn, 'cp image.* '+ips_out
    spawn, 'mv spectrum.out sed.out'
    spawn, 'cp sed.out '+ips_out

;;;;;;;;;;
; calculate visibilities and simulated image
;cmd=string(format='("casapy -c simobs.py -n",a0)',src)
;print,cmd
;spawn,cmd

;;;;;;;;;;
; plot results
plot:
    if keyword_set(outfile) then filename=outfile else filename=ips_out
;    if keyword_set(filename) then begin
    plot_sed, src=src, disk=disk, filename=filename+'sed.eps', incl=incl, pdf=pdf, ps=ps
    plot_profile, filename=filename+'profile.eps', pdf=pdf, ps=ps
    plot_image, filename=filename+'image.eps', pdf=pdf, ps=ps

;; ; generate the emission spectra
;;     calclines, incl, gd=gd, working=working
;; 
;; ; calculate the integrated line intensities
;;     make_line_results, star, disk, incl, gd=gd
;; 
;; ; move the spectrum files over to the data directories
;;     spawn, 'mv spectrum_gd* '+line_out
;; 
;; ; move the line_results files over to the data directories
;;     spawn, 'mv line_results_* '+ips_out

endfor

; display the start and finish times
if not keyword_set(plot) then begin
    print
    print, 'Start time: '+starttime
    print, 'Finish time: '+systime()
    print
endif

return
END
