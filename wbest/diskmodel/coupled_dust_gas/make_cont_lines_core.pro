@radmc3dfits.pro

PRO MAKE_CONT_LINES_CORE, MLOOP, GLOOP, RCLOOP, H100LOOP, HLOOP, AVDARR, TFARR, SIMNUM, $
                          DATAROOT=dataroot, FINE=fine, INCOMP=incomp, OUTPATH=outpath, $
                          PDF=pdf, PLOT=plot, PS=ps

;+
;  Generates an SED, mm continuum image, spectra, and integrated line
;  intensities (CO isotopologues) for a model protoplanetary disk.
;  1. Creates a dust profile
;  2. Uses radmc-3d to calculate dust temperature structure
;     - output model SED, profiles and image
;  3. plot results
;  4. Calculate spectra and line intensities
;
;  Parameters for the star and disk should be entered in make_cont_lines_parameters_loop.pro
;  
;  HISTORY
;  Written by Will Best (IfA), 12/08/2012
;  12/11/12 (WB): Moved the call to plot_profile to calclines.pro.
;                 Removed the GD keyword.
;                 Put g/d ratio in make_cont_lines_parameters_loop.pro
;
;  INPUTS
;      MLOOP, GLOOP, RCLOOP, H100LOOP, HLOOP, AVDLOOP, TFLOOP - Parameters passed by
;             make_cont_lines_loop.pro, used by make_cont_lines_parameters_loop.pro
;      SIMNUM - simulation number.  Used to name output files.
;
;  KEYWORDS
;      DATAROOT - top level of the data output folder structure.
;                 report.txt goes here.
;      FINE - Use a finer grid for the dust continuum model
;;      GD - Scalar or vector of gas-to-dust ratios to be used in calculating the
;;           line spectra.
;;           Default: [1,3,10,30,100,300]
;      INCOMP - If set, check for an incomplete run
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;      PDF - If set, create pdf output
;      PLOT - If set, skip the continuum modeling (assuming that it is already
;             done) and go straight to plotting the SED, profiles, and image.
;      PS - If set, create postscript output
;
;  CALLS
;      calclines
;      make_cont_lines_parameters_loop
;      make_dust_profile
;      make_header
;      make_line_results
;      make_star_spec
;      plot_image
;      plot_profile
;      plot_sed
;      radmcimage_to_fits
;      trim
;-

goto, debug
debug:

; mark the start time
starttime = systime()

; Star and disk parameters
@make_cont_lines_parameters_loop.pro

; Generic working directory
working = '~/Astro/699-2/radmc_output'
;working = '.'

; Data output paths
if n_elements(dataroot) eq 0 then dataroot = '~/Astro/699-2/test_output/'
spawn, 'mkdir -p '+dataroot
level1 = 'L'+trim(star.l)+'_T'+trim(star.t)+'/'
level2 = 'Md'+trim(disk.m)+'_g'+trim(disk.gamma)+'_Rc'+trim(disk.rc)+'_H'+trim(disk.H100)+'_h'+trim(disk.h)+'/'

; Close any lingering open write files
close, /all

; Point to generic working directory
cd, working

; Start the report file if necessary
report = dataroot+'report.txt'
if file_test(report) eq 0 then begin
    openw, 33, report, width=200
    printf, 33, '#Run', 'Star', 'Disk', 'Chemistry', 'Incl', 'G/D', $
            format='(a,t9,a,t24,a,t56,a,t72,a,t85,a)'
    close, 33
endif

; Check for iteration number
if n_elements(simnum) eq 0 then simnum = 0

; Vary the A_V_dissoc parameter
for ia=0, n_elements(avdarr)-1 do begin
    A_V_dissoc = avdarr[ia]

; Vary the t_freeze parameter
    for it=0, n_elements(tfarr)-1 do begin
        t_freeze = tfarr[it]

        level3 = 'AVd'+trim(A_V_dissoc)+'_tf'+trim(t_freeze)+'/'
;;         ips_orig = strarr(n_elements(iarr))
;;         num_orig = strarr(n_elements(iarr))

; Vary the inclination parameter
        for ii=0, n_elements(iarr)-1 do begin
            incl = iarr[ii]

; make the output folders
            runnum = trim(simnum)+'.'+trim(n_elements(tfarr)*ia+it+1)
            numlabel = runnum+'.i'+trim(incl)
            level4 = 'i'+trim(incl)+'/'
            ips_out = dataroot+level1+level2+level3+level4
            line_out = ips_out+'spectra/'
            spawn, 'mkdir -p '+line_out
            if keyword_set(outpath) then filename=outpath else filename=ips_out
            ;; if (ia+it eq 0) then begin
            ;;     ips_orig[ii] = ips_out
            ;;     num_orig[ii] = numlabel
            ;; endif

; check for a previous incomplete run
            if keyword_set(incomp) then begin
    ; if the third line_results file already exists, write the report line
                if file_test(ips_out+'line_results_32_'+numlabel+'.txt') ne 0 then begin
                    print, 'Run '+runnum+' with inclination '+trim(incl) $
                           +' is already completed.'
                    continue
                endif
    ; if the spectrum files already exist, jump to line calculations
                spawn, 'ls '+line_out, speccheck
                if n_elements(speccheck) gt 1 then begin
                    print, 'Spectra already generated for inclination '+trim(incl)
                    print, '    Now calculating line strengths.'
                    goto, calclines
                endif
    ; if the image files already exit, jump to spectrum calculations
                if file_test(ips_out+'image'+numlabel+'.eps') ne 0 then begin
                    print, 'SED and images already generated for inclination '+trim(incl)
                    print, '    Now generating spectra.'
                    goto, spectra
                endif
            endif

            if keyword_set(plot) then goto, plot

            if (ia+it eq 0) then begin   ; only generate disk if first time through
            ;;;;;;;;;;
            ; create dust profile
                if keyword_set(fine) then $
                  make_dust_profile,star=star,disk=disk,/fine $
                else make_dust_profile,star=star,disk=disk

            ;;;;;;;;;;
            ; create star spectrum
                make_star_spec,star=star,/silent

            ;;;;;;;;;;
            ; calculate dust temperature
                spawn,'radmc3d mctherm'

            ;;;;;;;;;;
            ; output model continuum image at SMA observing wavelength
                npix = 256
                lambda_microns = 1333.3
                cmd=string(format='("radmc3d image lambda ",f6.1," npix ",i0," incl ",f5.1," posang ",f5.1, " nostar noline")',lambda_microns,npix,INCL,PA)
                print,cmd
                spawn,cmd

            ; make a fits image
                file_delete,'image.fits',/allow_nonexistent
                radmcimage_to_fits,'image.out','image.fits',140.
                file_delete,src+'_model.fits',/allow_nonexistent
                make_header,src,RA,DEC

            ;;;;;;;;;;
            ; output SED
                ; calculate SED
                file_delete,'sed.out',/allow_nonexistent
                cmd=string(format='("radmc3d spectrum loadlambda incl ",f5.1," posang ",f5.1, " noline")',INCL,PA)
                print,cmd
                spawn,cmd

                ; copy files to data directory
                spawn, 'cp image.fits '+ips_out+'image'+numlabel+'.fits'
                file_delete,'image_'+trim(incl)+'.fits',/allow_nonexistent
                spawn, 'cp image.fits image_'+trim(incl)+'.fits'

                spawn, 'cp image.out '+ips_out+'image'+numlabel+'.out'
                file_delete,'image_'+trim(incl)+'.out',/allow_nonexistent
                spawn, 'cp image.out image_'+trim(incl)+'.out'

                spawn, 'mv spectrum.out sed.out'
                spawn, 'cp sed.out '+ips_out+'sed'+numlabel+'.out'
                file_delete,'sed_'+trim(incl)+'.out',/allow_nonexistent
                spawn, 'cp sed.out sed_'+trim(incl)+'.out'

            ;;;;;;;;;;
            ; calculate visibilities and simulated image
                ;cmd=string(format='("casapy -c simobs.py -n",a0)',src)
                ;print,cmd
                ;spawn,cmd

            endif else begin      ; otherwise use previous image files
                file_delete,'image.fits',/allow_nonexistent
                spawn, 'cp image_'+trim(incl)+'.fits image.fits'
                spawn, 'cp image.fits '+ips_out+'image'+numlabel+'.fits'

                file_delete,'image.out',/allow_nonexistent
                spawn, 'cp image_'+trim(incl)+'.out image.out'
                spawn, 'cp image.out '+ips_out+'image'+numlabel+'.out'

                file_delete,'sed.out',/allow_nonexistent
                spawn, 'cp sed_'+trim(incl)+'.out sed.out'
                spawn, 'cp sed.out '+ips_out+'sed'+numlabel+'.out'

                ;; spawn, 'cp '+ips_orig[ii]+'image'+num_orig+'.fits '+ips_out+'image'+numlabel+'.fits'
                ;; spawn, 'cp '+ips_orig[ii]+'image'+num_orig+'.out '+ips_out+'image'+numlabel+'.out'
                ;; spawn, 'cp '+ips_orig[ii]+'image'+num_orig+'.eps '+ips_out+'image'+numlabel+'.eps'
                ;; spawn, 'cp '+ips_orig[ii]+'image'+num_orig+'.pdf '+ips_out+'image'+numlabel+'.pdf'
                ;; spawn, 'cp '+ips_orig[ii]+'profile'+num_orig+'.eps '+ips_out+'profile'+numlabel+'.eps'
                ;; spawn, 'cp '+ips_orig[ii]+'profile'+num_orig+'.pdf '+ips_out+'profile'+numlabel+'.pdf'
                ;; spawn, 'cp '+ips_orig[ii]+'sed'+num_orig+'.out '+ips_out+'sed'+numlabel+'.out'
                ;; spawn, 'cp '+ips_orig[ii]+'sed'+num_orig+'.eps '+ips_out+'sed'+numlabel+'.eps'
                ;; spawn, 'cp '+ips_orig[ii]+'sed'+num_orig+'.pdf '+ips_out+'sed'+numlabel+'.pdf'
            endelse

;;;;;;;;;;
plot:
; plot results
;            if keyword_set(outpath) then filename=outpath else filename=ips_out
            plot_sed, src=src, disk=disk, filename=filename+'sed'+numlabel+'.eps', incl=incl, pdf=pdf, ps=ps
;            plot_profile, filename=filename+'profile'+numlabel+'.eps', pdf=pdf, ps=ps, $
;                         A_V_dissoc=A_V_dissoc, gas_to_dust=gd, t_freeze=t_freeze
            plot_image, filename=filename+'image'+numlabel+'.eps', pdf=pdf, ps=ps

spectra:
; generate the emission spectra and profile plot
            calclines, incl, gd=gd, working=working, A_V_dissoc=A_V_dissoc, t_freeze=t_freeze, $
                       filename=filename+'profile'+numlabel+'.eps', pdf=pdf, ps=ps

; copy the spectrum files over to the data directories
            spawn, 'cp spectrum_gd* '+line_out

calclines:
; calculate the integrated line intensities
            chem = {avd:A_V_dissoc, tf:t_freeze}
            make_line_results, star, disk, chem, incl, gd=gd

; move the line_results files over to the data directories
            spawn, 'mv line_results_10.txt '+ips_out+'line_results_10_'+numlabel+'.txt'
            spawn, 'mv line_results_21.txt '+ips_out+'line_results_21_'+numlabel+'.txt'
            spawn, 'mv line_results_32.txt '+ips_out+'line_results_32_'+numlabel+'.txt'

; delete the spectrum files in the working directory
            spawn, 'rm spectrum_gd*'

        endfor

; Write to the report file
        if keyword_set(incomp) then begin
            readcol, report, r2, format='f,x,x,x,x,x', comment='#'
            if n_elements(r2) gt 0 then begin
                prev = where(runnum eq r2)
                if prev[0] gt -1 then goto, finish     ; skip writing the report line if it's already there
            endif
        endif
        print
        print, 'Writing report line for run '+runnum
        openw, 33, report, width=200, /append
        printf, 33, runnum, level1, level2, level3, strjoin(trim(iarr),','), strjoin(trim(gd),','), $
                format='(a,t9,a,t24,a,t56,a,t72,a,t85,a)'
        close, 33

finish:
; display the start and finish times
        if not keyword_set(plot) then begin
            print
            print, 'Start time for this simulation: '+starttime
            print, 'Finish time for this simulation: '+systime()
            print
        endif

    endfor
endfor

return
END
