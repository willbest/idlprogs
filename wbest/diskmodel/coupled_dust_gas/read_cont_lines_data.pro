nsets=8
;filepath='~/Astro/699-2/results/test9/'
;PRO READ_CONT_LINES_DATA, FILEPATH=filepath, NSETS=nsets, SETNAMES=setnames;

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  
;  HISTORY
;  Written by Will Best (IfA), 1/29/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;-

; set up generic paths
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1
readpath = filepath

; Oh, the structure
nsim = intarr(nsets)
for i=0, nsets-1 do begin
    if nsets gt 1 then readpath = filepath+setnames+trim(i+1)+'_output/'
    cd, readpath
    nsim[i] = file_lines('report.txt') - 1
endfor
readcol, 'report.txt', inc, gas, delim=' ', comment='#', format='x,x,x,x,a,a', numline=2, /silent
inc = strsplit(inc, ',', /ex)
ninc = n_elements(inc)
gas = strsplit(gas, ',', /ex)
ngas = n_elements(gas)
ntot = total(nsim) * ninc * ngas
sims = replicate({L:0., T:0, Md:0., g:0., Rc:0, H100:0, h:0., AV:0., Tf:0, $
                  in:0, gd:0, cont:0d, I10:[0., 0., 0.], I21:[0., 0., 0.], I32:[0., 0., 0.]}, ntot)
; cont = 1.3 mm continuum emission
; 1-0, 2-1, 3-2
; CO, 13CO, C18O

; start the loops
counter = 0
for i=0, nsets-1 do begin
    print, 'Loop #'+trim(i+1)
    if nsets gt 1 then readpath = filepath+setnames+trim(i+1)+'_output/'
    cd, readpath
    readcol, 'report.txt', run, star, disk, chem, incl, gd, $
             delim=' ', comment='#', format='f,a,a,a,a,a'

    num = nsim[i] * ninc * ngas
    L = rebin(float(strmid(star, 1, 1#strpos(star, '_T')-1)), num, /sample)
    T = rebin(fix(strmid(star, 1#strpos(star, '_T')+2, 1#strpos(star, '/')-1#strpos(star, '_T')-2)), num, /sample)
    Md = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), num, /sample)
    g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rc')-1#strpos(disk, '_g')-2)), num, /sample)
    Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '_H')-1#strpos(disk, '_Rc')-3)), num, /sample)
    H100 = rebin(fix(strmid(disk, 1#strpos(disk, '_H')+2, 1#strpos(disk, '_h')-1#strpos(disk, '_H')-2)), num, /sample)
    h = rebin(float(strmid(disk, 1#strpos(disk, '_h')+2, 1#strpos(disk, '/')-1#strpos(disk, '_h')-2)), num, /sample)
    AV = rebin(float(strmid(chem, 3, 1#strpos(chem, '_tf')-3)), num, /sample)
    Tf = rebin(fix(strmid(chem, 1#strpos(chem, '_tf')+3, 1#strpos(chem, '/')-1#strpos(chem, '_tf')-3)), num, /sample)
    in = rebin(fix(strsplit(strjoin(incl, ','), ',', /ex)), num, /sample)
    gd = rebin(fix(strsplit(strjoin(reform(transpose(cmreplicate(gd, ninc)), nsim[i]*ninc), ','), ',', /ex)), num, /sample)

    sims[counter:counter+num-1].L = L
    sims[counter:counter+num-1].T = T
    sims[counter:counter+num-1].Md = Md
    sims[counter:counter+num-1].g = g
    sims[counter:counter+num-1].Rc = Rc
    sims[counter:counter+num-1].H100 = H100
    sims[counter:counter+num-1].h = h
    sims[counter:counter+num-1].AV = AV
    sims[counter:counter+num-1].Tf = Tf
    sims[counter:counter+num-1].in = in
    sims[counter:counter+num-1].gd = gd

    run = trim(rebin(run, num, /sample))
    star2 = reform(transpose(cmreplicate(star, ninc*ngas)), num)
    disk2 = reform(transpose(cmreplicate(disk, ninc*ngas)), num)
    chem2 = reform(transpose(cmreplicate(chem, ninc*ngas)), num)
    for j=0, num-1, ngas do begin
        readcol, star2[j]+disk2[j]+chem2[j]+'i'+trim(in[j])+'/line_results_10_'+run[j]+'.i'+trim(in[j])+'.txt', $
                 gd2, co, co13, c18o, delim=' ', comment='%', format='i,f,f,f', /silent
        for k=0, ngas-1 do begin
            gdind = where(sims[j+counter:j+counter+ngas-1].gd eq gd2[k])
            sims[j+counter+gdind].I10 = [co[gdind], co13[gdind], c18o[gdind]]
        endfor

        readcol, star2[j]+disk2[j]+chem2[j]+'i'+trim(in[j])+'/line_results_21_'+run[j]+'.i'+trim(in[j])+'.txt', $
                 gd2, co, co13, c18o, delim=' ', comment='%', format='i,f,f,f', /silent
        for k=0, ngas-1 do begin
            gdind = where(sims[j+counter:j+counter+ngas-1].gd eq gd2[k])
            sims[j+counter+gdind].I21 = [co[gdind], co13[gdind], c18o[gdind]]
        endfor

        readcol, star2[j]+disk2[j]+chem2[j]+'i'+trim(in[j])+'/line_results_32_'+run[j]+'.i'+trim(in[j])+'.txt', $
                 gd2, co, co13, c18o, delim=' ', comment='%', format='i,f,f,f', /silent
        for k=0, ngas-1 do begin
            gdind = where(sims[j+counter:j+counter+ngas-1].gd eq gd2[k])
            sims[j+counter+gdind].I32 = [co[gdind], co13[gdind], c18o[gdind]]
        endfor

        readcol, star2[j]+disk2[j]+chem2[j]+'i'+trim(in[j])+'/sed'+run[j]+'.i'+trim(in[j])+'.out', $
                 contw, contf, delim=' ', comment='%', format='f,d', skipline=3, /silent
        contind = where(contw eq 1333.3)
        sims[j+counter:j+counter+ngas-1].cont = contf[contind[0]]
    endfor

    counter = counter + num
endfor

cd, filepath
forprint, sims, textout='sims.txt', width=135, /nocomment, /silent
save, sims, filename='sims.sav'

print
print, 'Done!!'
print

END

