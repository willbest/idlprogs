nsets=8
;PRO GET_CONT_LINES_RESULTS, FILEPATH=filepath, OUTPATH=outpath, SETNAMES=setnames

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  Specifically, obtains the report.txt file(s) from the top of the output
;  directory tree.  
;  Obtains the line_results_... files from the directories, and bundles them
;  into a tarball in outpath.
;  
;  HISTORY
;  Written by Will Best (IfA), 1/27/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/gasgrid/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      OUTPATH - folder to receive the report.txt and line_results files.
;                Default: ~/Astro/699-2/results/
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;-

; mark the start time
starttime = systime()

; set up generic paths
if not keyword_set(filepath) then filepath = '~/gasgrid/'
if not keyword_set(outpath) then outpath = '~/gasgrid/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1

; start the loops
cd, outpath
for i=0, nsets-1 do begin

; set up paths for this loop
;    writepath = outpath
    readpath = setnames+trim(i+1)+'_output/'
;        writepath = writepath+setnames+trim(i+1)+'/'

; build the list of files to read into tarball
    print, 'Building list of files #'+trim(i+1)
    filelist = readpath+'report.txt'
    
    readcol, filelist, run, star, disk, chem, incl, gd, $
             delim=' ', comment='#', format='a,a,a,a,a,a'
    for j=0, n_elements(run)-1 do begin
        incl2 = strsplit(incl[j], ',', /ex)
        gd2 = strsplit(gd[j], ',', /ex)
        for k=0, n_elements(incl2)-1 do filelist = [filelist, $
          readpath+star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/line_results_10_'+run[j]+'.i'+incl2[k]+'.txt', $
          readpath+star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/line_results_21_'+run[j]+'.i'+incl2[k]+'.txt', $
          readpath+star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/line_results_32_'+run[j]+'.i'+incl2[k]+'.txt', $
          readpath+star[j]+disk[j]+chem[j]+'i'+incl2[k]+'/sed'+run[j]+'.i'+incl2[k]+'.out' ]
    endfor

; make the tarball
    print, 'Making tarball #'+trim(i+1)+' of files to copy'
    tarcom = 'tar -cf lineout'+trim(i+1)+'.tar '+strjoin(filelist, ' ')
;    copcom = readpath+'lineout '+writepath
    openw, x, 'tarcom'+trim(i+1)+'.txt', /get_lun
    printf, x, tarcom
    close, x
    spawn, 'source tarcom'+trim(i+1)+'.txt'

endfor

print
print, 'Done!!'
print
print, 'Start time: '+starttime
print, 'Finish time: '+systime()
print

END

