PRO MAKE_GAS_LOOP, FINE=fine, OUTPATH=outpath, PROFILE=profile

;+
;  Runs make_gas_core multiple times, varying the input disk parameters
;  
;  HISTORY
;  Written by Will Best (IfA), 03/24/2013
;
;  KEYWORDS
;      FINE - Use a finer grid for the gas model
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;      PROFILE - Only generate the profile stats and images.
;                Don't calculate line intensities.
;                Don't check for previous runs.
;                Don't write to the report file.
;
;  CALLS
;      make_gas_core
;-

; mark the start time
startloop = systime()

; working directory
working = '~/gasdisks/newabun/LOOPNUM/'

; set up output path
dataroot = '~/gasdisks/newabun/LOOPOUT/'
report = dataroot+'report.txt'

; STELLAR PARAMETERS
; Ms = mass in units of Msun
msarr = [0.7]*1d

; GAS DISK STRUCTURAL PARAMETERS
; Mg = gas mass in units of Msun
mgarr = [1, 3, 10, 30, 100, 300, 1000]*1d-4
; gamma = index for surface density profile
garr = [0.0, 0.8, 1.5]*1d
; r_in = inner radius in units of AU
rinarr = 0.05d
; rc = characteristic radius in units of AU
rcarr = [RCVAL]*1d

; GAS DISK TEMPERATURE PARAMETERS
; Tm1 = midplane temperature at 1 AU
Tm1arr = [150, 200, 300]*1d
; qm = index for midplane radial temperature profile
qmarr = [0.45, 0.65]*1d
; Ta1 = characteristic disk temperature above the midplane at 1 AU
Ta1arr = [TA1VAL]*1d
; qa = index for vertical temperature profile
qaarr = [0.45, 0.65]*1d

; CHEMICAL PARAMETERS for CO
; Npd = surface density for dissociation
npdarr = 1.6d
; T_freeze = freeze-out temperature for CO
tfarr = 20d

; GEOMETRIC PARAMETERS
; i = inclination for observer
iarr = [0, 45, 90]*1d

; ISOTOPOLOGUES of CO
; sp = species
sparr = ['co', '13co', 'c18o']

; TRANSITIONS of CO
; j = upper level of transition
jarr = [1, 2, 3]

limit = n_elements(iarr)

simnum = 1

; set up to check for previous incomplete run
file_flag = 0
if file_test(report) eq 1 and not keyword_set(profile) then begin
    readcol, report, runnum, format='f,x,x,x,x,x', comment='#'
    if n_elements(runnum) eq 0 then runnum = 0   ; case where no report lines have yet been written
    file_flag = 1
endif

; Vary the M_star parameter
for i=0, n_elements(msarr)-1 do begin
  ; Vary the M_gas parameter
  for i1=0, n_elements(mgarr)-1 do begin
    ; Vary the gamma parameter
    for i2=0, n_elements(garr)-1 do begin
      ; Vary the R_in parameter
      for i3=0, n_elements(rinarr)-1 do begin
        ; Vary the Rc parameter
        for i4=0, n_elements(rcarr)-1 do begin
          ; Vary the T_m1 parameter
          for i5=0, n_elements(tm1arr)-1 do begin
            ; Vary the q_m parameter
            for i6=0, n_elements(qmarr)-1 do begin
              ; Vary the T_a1 parameter
              for i7=0, n_elements(ta1arr)-1 do begin
                if ta1arr[i7] le tm1arr[i5] then continue
                ; Vary the q_a parameter
                for i8=0, n_elements(qaarr)-1 do begin
                  ; Vary the N_pd parameter
                  for i9=0, n_elements(npdarr)-1 do begin
                    ; Vary the T_freeze parameter
                    for i10=0, n_elements(tfarr)-1 do begin
                      print
                      print, 'Run number '+trim(simnum)
                      if file_flag eq 1 then begin
                          prev = where(simnum eq round(runnum))
                          if ((prev[0] ge 0) and (n_elements(prev) eq limit)) then begin
                              print, '    This run is already complete.'
                              simnum++
                              continue
                          endif else incomp = 1
                          file_flag = 0
                      endif
                      print
                      star = {m:msarr[i]}
                      disk = {m:mgarr[i1], gamma:garr[i2], rin:rinarr[i3], rc:rcarr[i4]}
                      temp = {tm1:tm1arr[i5], qm:qmarr[i6], ta1:ta1arr[i7], qa:qaarr[i8]}
                      chem = {npd:npdarr[i9], tf:tfarr[i10]}
                      make_gas_core, star, disk, temp, chem, iarr, sparr, jarr, simnum, fine=fine, $
                                     dataroot=dataroot, incomp=incomp, outpath=outpath, working=working, $
                                     profile=profile, /ps;, /pdf
                      incomp = 0
                      simnum++
                    endfor
                  endfor
                endfor
              endfor
            endfor
          endfor
        endfor
      endfor
    endfor
  endfor
endfor

print
print, 'Done!!'
print
print, 'Start time for the whole run: '+startloop
print, 'Finish time for the whole run: '+systime()
print

END

