PRO CALC_GAS_MASS, FLUX

;+
;  Calculate the gas mass of a protoplanetary disk using its C18O (1-0) flux.
;  WARNING: Experimental!!
;  
;  HISTORY
;  Written by Will Best (IfA), 4/29/2013
;
;  INPUTS
;      FLUX - C18O (1-0) flux in Jy
;-

; coefficients for gas mass
am = -1.24e-2
bm = -4.77
cm = 1.18e-2

; coefficients for uncertainty
au = -2.90e-3
bu = -20.4
cu = 2.47e-3

; calculations
Mg = am * exp(bm*flux) + cm
un = au * exp(bu*flux) + cu

; tell the world
print
print, 'Input disk flux:  '+trim(flux)+' Jy'
print
print, Mg, format='("Estimated gas mass:  ",e8.1," Msun")'
print, un, format='("Estimated uncertainty:  ",e8.1," Msun")'
print

END

