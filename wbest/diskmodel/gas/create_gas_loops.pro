PRO CREATE_GAS_LOOPS, FIRST

;+
;  Set up the make_gas_loop files.
;  To be run on the nodes.
;  
;  HISTORY
;  Written by Will Best (IfA), 2014-01-04
;
;  INPUTS
;      FIRST - Number of the first loop
;              Default: 1
;-

; Make sure FIRST has a values
if n_elements(first) eq 0 then first = 1

;;; PARAMETERS TO USE FOR DIFFERENT LOOPS
; Coarse grid
;; gamma = [0.0, 0.8, 1.5]
;; ng = n_elements(gamma)
;; qm = [0.45, 0.55, 0.65]
;; nqm = n_elements(qm)
;; qa = [0.45, 0.55, 0.65]
;; nqa = n_elements(qa)
; Fine grid
;; gamma = [0.0, 0.4, 0.8, 1.2, 1.5]
;; ng = n_elements(gamma)
;; rc = [30, 60, 100, 200, 300]
;; nr = n_elements(rc)
; New Abundance grid
rc = [30, 60, 100, 200, 300]
nr = n_elements(rc)
ta1 = [500, 750, 1000, 1500, 2000]
nta1 = n_elements(ta1)

base = '~/gasdisks/newabun/'

;;; LOOPS TO CREATE THE LOOPS!
; Coarse grid
;; for i=0, ng-1 do begin
;;     for j=0, nqm-1 do begin
;;         for k=0, nqa-1 do begin
;;             lnum = first + nqa*(nqm*i + j) + k
; Fine grid
;; for i=0, ng-1 do begin
;;     for j=0, nr-1 do begin
;;         lnum = first + nr*i + j
; New Abundance grid
for i=0, nta1-1 do begin
    for j=0, nr-1 do begin
        lnum = first + nr*i + j

; go to the directory where the loop directories live
            cd, base

; copy the nextloop directory into the new loop directory
            spawn, 'cp -r nextloop loop'+trim(lnum)
            cd, 'loop'+trim(lnum)

; make the substitutions using sed
            ; coarse grid for sine
            ;; spawn, 'sed -e "s/LOOPNUM/loop'+trim(lnum)+'/" -e "s/LOOPOUT/loop'+trim(lnum)+'_output/" '+$
            ;;        '-e "s/GAMMAVAL/'+trim(gamma[i])+'/" -e "s/QMVAL/'+trim(qm[j])+'/" '+$
            ;;        '-e "s/QAVAL/'+trim(qa[k])+'/" <make_gas_template.pro >make_gas_loop.pro'

        ; fine grid for sine
        ;; spawn, 'sed -e "s/LOOPNUM/loop'+trim(lnum)+'/" -e "s/LOOPOUT/loop'+trim(lnum)+'_output/" '+$
        ;;   '-e "s/GAMMAVAL/'+trim(gamma[i])+'/" -e "s/RCVAL/'+trim(rc[j])+'/" <make_gas_template.pro >make_gas_loop.pro'

        ; New Abundance grid for sine
        spawn, 'sed -e "s/LOOPNUM/loop'+trim(lnum)+'/" -e "s/LOOPOUT/loop'+trim(lnum)+'_output/" '+$
          '-e "s/TA1VAL/'+trim(ta1[i])+'/" -e "s/RCVAL/'+trim(rc[j])+'/" <make_gas_template.pro >make_gas_loop.pro'

        ;; endfor
    endfor
endfor


END
