PRO ROSENFELD_GAS, R, Z

;+
;  Quick script to reproduce T_gas values from Rosenfeld et al. (2013), p. 8
;
;  HISTORY
;      Written by Will Best (IfA), 2013-12-03.
;
;  INPUTS
;      R - Radial position in the disk
;      Z - Vertical height above the midplane
;-

; Make everything double-precision
r = double(r)
z = double(z)

; Intermediate quantities
delta = .0034d * (r - 200) + 2.5
z_q = 63d * (r/200)^1.3 * exp(-(r/800)^2)
T_m = 19d * (r/155)^(-.3)
T_a = 55d * (sqrt(r^2 + z^2)/200)^(-.5)

; T_gas calculation
if z gt z_q then T_gas = T_a $
  else T_gas = T_a + (T_m - T_a) * (cos(!dpi*z/2/z_q))^(2*delta)

; Output to screen
print
print, '  z_q = '+trim(z_q, '(f6.1)')+' AU'
print, '  T_m = '+trim(T_m, '(f6.1)')+' K'
print, '  T_a = '+trim(T_a, '(f6.1)')+' K'
print
print, 'T_gas = '+trim(T_gas, '(f6.1)')+' K'
print

END
