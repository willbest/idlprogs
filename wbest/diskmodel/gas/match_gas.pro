PRO MATCH_GAS, ISO, TRANS, FLUX, UNCERT, MEANGAS, MEANGASU, PARAM, GAS=gas, FILEPATH=filepath, $
               MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, TA1=ta1, QA=qa, $
               NPD=npd, TFREEZE=tfreeze, INCL=incl, THIN=thin, LOG=log

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.
;  Returns the mean and standard deviation for the parameter indicated by PARAM 
;  over all of the disks matching the given fluxes and uncertainties for
;  specified emission lines.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the mean and stddev will incorporate only disks with those parameter values.
;
;  If no value is specified for a given parameter, then the mean and stddev will
;  include disks with all values of that parameter (that have the values given
;  for other parameters).
;
;  This program can be called directly to match disks and parameters to single
;  sets of line fluxes.  To loop over multiple sets of fluxes, use
;  matchloop_gas.pro to call matchcore_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-16
;
;  INPUTS
;      ISO - Vector (or scalar) indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Vector (or scalar) indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Vector (or scalar) containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - Vector (or scalar) containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      MEANGAS - Mean PARAM value for the matched disks.
;      MEANGASU - Standard deviation of PARAM for the matched disks.
;    To return mean and log(PARAM value) for the matched disks, set the /LOG flag.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      LOG - Compute and return mean and standard deviation in log space.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

;;; DETERMINE THE PARAMETER FOR WHICH TO FIND MEAN AND STANDARD DEVIATION
if n_elements(param) eq 0 then param = 2    ; Default histogram partition is gas mass
p = param - 1

; Check input parameter
npar = 12
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif


;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO, TRANS, FLUX, and UNCERT must be equal length
niso = n_elements(iso)
if (niso lt 1) or (n_elements(trans) ne niso) or (n_elements(flux) ne niso) $
  or (n_elements(uncert) ne niso) then begin
    print
    print, 'Use:  matchcore_gas, ISO, TRANS, FLUX, UNCERT, MEANGAS, MEANGASU, PARAM [, gas=gas, filepath=filepath,'
    print, '           mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa,'
    print, '           npd=npd, tfreeze=tfreeze, incl=incl, thin=thin, log=log]'
    print, '    ISO, TRANS, FLUX, and UNCERT must be scalars or vectors of equal length.'
    print
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif


;;; GET THE DATA
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/gas/'
if not keyword_set(gas) then gas='gas.sav'
restore, filepath+gas


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
; in a vector called 'select'.
select = gas_select_disks(gas, keys, npar+2)

; Choose the disks with the requested parameter values
chosen = gas[select]


;;; FIND THE DISKS THAT MATCH THE FLUX(ES) WITHIN THE UNCERTAINTY
for i=0, niso-1 do begin                         ; Loop over the input line fluxes
    lineflux = chosen.(iso[i]+13).(trans[i]-1)[thin-1]
    matched = where((lineflux ge flux[i]-uncert[i]) and (lineflux le flux[i]+uncert[i]))
; intersect matched with previous matched vectors.
    if i eq 0 then begin
        inters = matched
    endif else begin
        inters = setintersection(inters, matched)
    endelse
endfor

matched = inters


;;; FIND THE MEAN AND STANDARD DEVIATION
if keyword_set(log) then begin
    meangas = mean(alog10(chosen[matched].(p)))
    meangasu = stddev(alog10(chosen[matched].(p)))
endif else begin
    meangas = mean(chosen[matched].(p))
    meangasu = stddev(chosen[matched].(p))
endelse


END
