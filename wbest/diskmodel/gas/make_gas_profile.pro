PRO MAKE_GAS_PROFILE, STAR=star, DISK=disk, TEMP=temp, CHEM=chem, FINE=fine, $
                      DILUTE_DUST_FACTOR=dilute_dust_factor, FILENAME=filename

;+
;  1. parameterize the gas temperature distribution
;     based on d'Alessio models, bootstrapping off
;     a given (input) dust midplane temperature profile.
;
;  2. integrate the equation of hydrostatic equilibrium
;     to derive the vertical density structure of the gas.
;
;  3. freeze-out and dissociate to produce CO/13CO/C18O
;     density inputs for radmc
;
;  the molecular abundances relative to H2 are fixed at
;  [CO]/[H2] = 8.18e-05
;  [CO/13CO] = 70
;  [CO/C18O] = 550
;  see discussion in Section 4.3 of Qi et al. 2011
;  and Section 4.2 of Isella et al. 2007
;  and Section 4 of Rosenfeld et al. 2013, in prep
;
;  HISTORY
;      Written by jpw, 3/14/13
;      03/24/13 (WB): added all keywords except FINE
;                     to be called by make_gas_core.pro
;      06/14/13 (WB): Updated to use Rosenfeld et al. (2013) values
;
;  KEYWORDS
;      STAR - Mass of the central star
;              Default:  1.0 Msun
;      DISK - Structure containing disk parameters:
;             m:mass, gamma:gamma, rin:R_in, rc:R_c
;             Defaults: {0.01 Msun, 1.0, 0.05 AU, 50 AU}
;      TEMP - Structure containing disk temperature parameters
;             tm1:T_m1, qm:q_m, ta1:T_a1, qa:q_a
;             Defaults: {100 K, 0.5, 500 K, 0.5}
;      CHEM - Structure containing CO chemical parameters:
;             npd:N_photodissoc, tf:T_freeze
;             Defaults: {1d21, 20 K}
;      FINE - Use finer grid
;      DILUTE_DUST_FACTOR - Dilute "dust" density, which is actually gas
;                           density, so that RADMC-3D does not contaminate the
;                           line intensities with dust emission.
;                           Default:  1d6
;      FILENAME - file path+name for gas data text file output
;-

@natconst.pro

;;; stellar parameters
;
if keyword_set(star) then begin
    mstar = star.m*MS
endif else begin
    mstar = MS
endelse

;;; fiducial disk parameters
;
if keyword_set(disk) then begin
    mgas  = disk.m*MS
    rin   = disk.rin*AU
    rc    = disk.rc*AU
    gamma = disk.gamma
endif else begin
    mgas  = 1d-2*MS
    rin   = 0.05d*AU
    rc    = 60.0d*AU
    gamma = 0.8d
endelse

;;; temperature radial profile parameters
;
if keyword_set(temp) then begin
    Tmid1 = temp.tm1
    qmid  = temp.qm
    Tatm1 = temp.ta1
    qatm  = temp.qa
endif else begin
    Tmid1 = 100d
    qmid  = 0.5d
    Tatm1 = 500d
    qatm  = 0.5d
endelse

;---------------------------------------------------------
; CO abundance (relative to H2) and isotope fractions
; [CO]/[H2] = 1d-4
; [CO/13CO] = 70
; [CO/C18O] = 550 -- Rosenfeld
; see discussion in Section 4.3 of Qi et al. 2011
; and Section 4.2 of Isella et al. 2007

;x_co = 5d-5
x_co = 1d-4
f_13co = 1./70d
f_c18o = 1./550d

min_rho = 1d-23
if not keyword_set(dilute_dust_factor) then dilute_dust_factor = 1d6
;---------------------------------------------------------

;;; spherical grid parameters
;
nr       = 100L
nt       = 60L
nphi     = 1L

if keyword_set(fine) then begin
  nr = 200L
  nt = 120L
  nphi = 1L
endif

;;; create the coordinate system
;;; logarithmic in radius
;;; linear in theta (but don't let it go all the way to the pole)
;
rout     = 600*AU
r        = rin * (rout/rin)^(dindgen(nr+1)/nr)
tmin     = 0.29d;0.3d  -- 0.29 allows disks with incl<28 to work
tmax     = 1.0d
t        = tmin + (tmax-tmin)*dindgen(nt+1)/nt
t        = !dpi/2.0 * t

phi      = [0.,0.]
rcen     = 0.5d0 * (r[0:nr-1] + r[1:nr])
tcen     = 0.5d0 * (t[0:nt-1] + t[1:nt])


;;; write the grid file
;
file_delete,'amr_grid.inp',/allow_nonexistent
openw,1,'amr_grid.inp'
printf,1,1                      ; iformat
printf,1,0                      ; AMR grid style  (0=regular grid, no AMR)
printf,1,100                    ; Coordinate system
printf,1,0                      ; gridinfo
printf,1,1,1,0                  ; Include x,y,z coordinate
printf,1,nr,nt,nphi             ; Size of grid
for i=0,nr do printf,1,r[i]     ; X coordinates (cell walls)
for i=0,nt do printf,1,t[i]     ; Y coordinates (cell walls)
for i=0,nphi do printf,1,phi[i] ; Z coordinates (cell walls)
close,1


;;; midplane and atmospheric temperature radial profile
;
T_mid    = Tmid1 / (rcen/AU)^qmid
T_atm    = Tatm1 / (rcen/AU)^qatm

;;; midplane pressure scale height, H = c_s/Omega
;
mu       = 2.37d    ; matches Rosenfeld et al. (2013)
Hp       = sqrt(kk * rcen^3 * T_mid / (GG*mstar * mu*mp))

;;; gas temperature profile
;
delta2   = 4.0d
;delta2 = 2*(.0034d * (rcen/AU - 200) + 2.5)    ; Rosenfeld
T_g      = dblarr(nr,nt)
for ir=0, nr-1 do begin
  ;zq = 4*Hp[ir]
  zq = 10*Hp[ir]     ; to allow more of a constant value at low z to match the Kamp and Dullemond models
  ;zq = 63*AU * (rcen[ir]/(200*AU))^1.3 * exp(-(rcen[ir]/(800*AU))^2)     ; Rosenfeld
  zcen = rcen[ir]/tan(tcen)
  ;T_atm = Tatm1 / (sqrt(rcen[ir]^2 + zcen^2)/AU)^qatm     ; Rosenfeld
  T_g[ir,*] = T_atm[ir]
  ;T_g[ir,*] = T_atm     ; Rosenfeld   [ir]
  it = where(zcen lt zq, n_it)
  ;if (n_it gt 0) then T_g[ir,it] = T_atm[ir] + (T_mid[ir] - T_atm[ir]) * (cos(!pi*zcen[it]/(2*zq)))^delta2
  if (n_it gt 0) then T_g[ir,it] = T_mid[ir] + (T_atm[ir] - T_mid[ir]) * (sin(!pi*zcen[it]/(2*zq)))^delta2
  ;if (n_it gt 0) then T_g[ir,it] = T_atm[it] + (T_mid[ir] - T_atm[it]) * (sin(!pi*zcen[it]/(2*zq)))^delta2[ir]    ; Rosenfeld
  ;if (n_it gt 0) then T_g[ir,it] = T_mid[ir] + (T_atm[it] - T_mid[ir]) * (sin(!pi*zcen[it]/(2*zq)))^delta2[ir]    ; Rosenfeld
;if rcen[ir]/AU gt 155 then stop
endfor

;;; write the gas temperature file
;;; (but named dust temperature because that's what radmc wants)
;
file_delete,'dust_temperature.dat',/allow_nonexistent
openw,1,'dust_temperature.dat'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
printf,1,1                      ; Nr of dust species
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,T_g[i,j]
      endfor
   endfor
endfor
close,1


;;; make the gas density model
;;; note that the surface density is halved here
;;; since radmc3d will symmeterize above and below the midplane
;
rho      = dblarr(nr,nt)
g2       = 2.d0-gamma
const    = g2 * mgas * exp((rin/rc)^g2) / (2*!dpi*rc^2)
  ; exp((rin/rc)^g2) removes the gas from within R_in. (not in Andrews/Rosenfeld equations) 
Sigma    = 0.5 * const * (rc/rcen)^gamma / exp((rcen/rc)^g2)
  ; the 0.5 is explained above

;;; integrate the equation of vertical hydrostatic equilibrium
;;; (code borrowed from Meredith Hughes 10/22/12)
;
gconst = GG*mstar*mu*mp/kk
for ir=0, nr-1 do begin
; vertical height, ascending from midplane
  zcen = reverse(rcen[ir]/tan(tcen))

; calculate derivatives, integrating up from midplane
  dlnrho_dz = dblarr(nt)
  dlnrho_dz[0] = -1d0*gconst*zcen[0]/(T_g[ir,0]*(rcen[ir]^2+zcen[0]^2)^(1.5d0))
  for iz=1,nt-1 do begin
    dz = zcen[iz]-zcen[iz-1]
    dlnt_dz = (alog(T_g[ir,iz])-alog(T_g[ir,iz-1]))/dz
    dlnrho_dz[iz] = -1d0*gconst*zcen[iz]/(T_g[ir,iz]*(rcen[ir]^2+zcen[iz]^2)^(1.5d0)) - dlnt_dz
  endfor

; integrate
  lnrho = dblarr(nt)
  lnrho[0] = dlnrho_dz[0]*(zcen[1]-zcen[0])
  for iz=1,nt-1 do lnrho[iz]=int_tabulated(zcen[0:iz],dlnrho_dz[0:iz],/double)

; normalize
  rho_z = exp(lnrho)/int_tabulated(zcen,exp(lnrho),/double)
  rho[ir,*] = (Sigma[ir]*reverse(rho_z)) > min_rho
endfor

;;; write the dust density file
;;; radmc needs the dust density to calculate the continuum
;;; but I don't want it (it contaminates my spectral analysis)
;;; so I multiply by a small number!
;
file_delete,'dust_density.inp',/allow_nonexistent
openw,1,'dust_density.inp'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
printf,1,1                      ; Nr of dust species
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,rho[i,j] / dilute_dust_factor
      endfor
   endfor
endfor
close,1


;;; define freeze-out and photodissociation zones
;
if keyword_set(chem) then begin
    t_freeze = chem.tf
    N_pd     = chem.npd * 1d21
endif else begin
    t_freeze = 20d
    N_pd     = 1.3d21;1d21
    N_pd     = 1.6d21    ; total column (corresponding the N(H2) = 1.3e21 as in paper)
endelse

rho_freeze = rho
rho_co = rho
ind_mol = where(T_g gt t_freeze,n_mol,complement=ind_freeze,ncomplement=n_freeze)
if (n_mol gt 0) then rho_freeze[ind_mol] = 0.0d
if (n_freeze gt 0) then rho_co[ind_freeze] = rho_co[ind_freeze] / 1d8;0.0
Sigma_freeze = dblarr(nr)

rho_pd = rho
Sigma_pd = dblarr(nr)

for ir=0, nr-1 do begin
  zcen = reverse(rcen[ir]/tan(tcen))
  rho_r = reverse(reform(rho_freeze[ir,*]))
  Sigma_freeze[ir] = int_tabulated(zcen,rho_r,/double)

  rho_r = reverse(reform(rho[ir,*]))
  for iz=0, nt-2 do begin
    columnh2=int_tabulated(zcen[iz:nt-1],rho_r[iz:nt-1],/double)/(mu*mp)
    if (columnh2 ge N_pd) then rho_pd[ir,nt-1-iz] = 0.0d $
       else rho_co[ir,nt-1-iz] = 0.0d
  endfor
  rho_pd[ir,0] = 0.0d
  rho_co[ir,0] = 0.0d
  rho_r = reverse(reform(rho_pd[ir,*]))
  Sigma_pd[ir] = int_tabulated(zcen,rho_r,/double)
endfor
;;; note doubling due to symmetry above and below the midplane
mgas_freeze = 2*int_tabulated(rcen,2*!dpi*rcen*Sigma_freeze)
mgas_pd = 2*int_tabulated(rcen,2*!dpi*rcen*Sigma_pd)
;n_co = x_co * rho_co / (mu*mp)
n_H2 = 0.818 * rho_co / (mu*mp)    ; 9 H2 for every 11 particles (2 He)
n_co = x_co * n_H2

; print to screen
print
print,format='("Total gas mass  = ",f6.4," Msun")',mgas/MS
;print,format='("Total gas mass  = ",f10.8," Msun")',mgas/MS
print,format='("Frozen gas mass = ",f6.4," Msun")',mgas_freeze/MS
print,format='("Frozen gas fraction =",f5.1,"%")',100*mgas_freeze/mgas
;print,format='("Frozen gas mass = ",f10.8," Msun")',mgas_freeze/MS
;print,format='("Frozen gas fraction =",f7.3,"%")',100*mgas_freeze/mgas
print,format='("Correction factor = ",f4.1)',1.0/(1-mgas_freeze/mgas)
print,format='("Dissociated gas mass = ",f6.4," Msun")',mgas_pd/MS
print,format='("Dissociated gas fraction =",f5.1,"%")',100*mgas_pd/mgas
;print,format='("Dissociated gas mass = ",f10.8," Msun")',mgas_pd/MS
;print,format='("Dissociated gas fraction =",f7.3,"%")',100*mgas_pd/mgas

print,format='("Correction factor = ",f4.1)',1.0/(1-mgas_pd/mgas)
print

; output to file
get_lun, lun
if n_elements(filename) eq 0 then filename='gasdata.txt'
openw, lun, filename
printf, lun, '% Data for gas disk'
printf, lun, '%---------------------------------------------------------------------------------'
if n_elements(star) ne 0 then $
  printf, lun, '% Star:  '+string(format='("M = ",f5.2," Msun")', star.m)
if n_elements(disk) ne 0 then $
  printf, lun, '% Disk:  '+string(format='("Mgas = ",e8.2," Msun,  gamma = ",f4.2,",  ")', disk.m, disk.gamma)+$
          string(format='("R_in = ",f5.2," AU,  R_c = ",f5.1," AU")', disk.rin, disk.rc)
if n_elements(temp) ne 0 then $
  printf, lun, '% Temperature:  '+string(format='("T_m1 = ",f6.1," K,  q_m = ",f4.2,",  ")', temp.tm1, temp.qm)+$
          string(format='("T_a1 = ",f6.1," K,  q_a = ",f4.2)', temp.ta1, temp.qa)
if n_elements(chem) ne 0 then $
  printf, lun, '% Chemistry:  '+$
          string(format='("N_pd = ",e8.2," cm^-2,  T_fr = ",f4.1," K")', chem.npd, chem.tf)
printf, lun, '%---------------------------------------------------------------------------------'
printf, lun, '%'
printf, lun, "%Total gas mass"
printf, lun, "%Frozen gas mass"
printf, lun, "%Frozen gas fraction"
printf, lun, "%Correction factor for frozen CO gas"
printf, lun, "%Dissociated gas mass"
printf, lun, "%Dissociated gas fraction"
printf, lun, "%Correction factor for dissociated CO gas"
printf, lun, '%'
printf, lun, '%    M_gas        M_fr            f_fr     C_fr     M_dis          f_dis     C_dis'
printf, lun, '%---------------------------------------------------------------------------------'
printf, lun, format='(3(3x,g10.4),3x,f4.1,x,2(3x,g10.4),3x,f4.1)', mgas/MS, mgas_freeze/MS, $
        mgas_freeze/mgas, 1.0/(1-mgas_freeze/mgas), mgas_pd/MS, mgas_pd/mgas, 1.0/(1-mgas_pd/mgas)
printf, lun, '%---------------------------------------------------------------------------------'
free_lun, lun

;;; Write the molecular density files
;
file_delete,'numberdens_co.inp',/allow_nonexistent
openw,1,'numberdens_co.inp'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,n_co[i,j]
      endfor
   endfor
endfor
close,1

file_delete,'numberdens_13co.inp',/allow_nonexistent
openw,2,'numberdens_13co.inp'
printf,2,1                      ; Format number
printf,2,nr*nt*nphi             ; Nr of cells
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,2,f_13co*n_co[i,j]
      endfor
   endfor
endfor
close,2

file_delete,'numberdens_c18o.inp',/allow_nonexistent
openw,3,'numberdens_c18o.inp'
printf,3,1                      ; Format number
printf,3,nr*nt*nphi             ; Nr of cells
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,3,f_c18o*n_co[i,j]
      endfor
   endfor
endfor
close,3


;;; write the lines.inp control file
;
file_delete,'lines.inp',/allow_nonexistent
openw,1,'lines.inp'
printf,1,'2'
printf,1,'3'
printf,1,'co    leiden  0  0  0'
printf,1,'13co  leiden  0  0  0'
printf,1,'c18o  leiden  0  0  0'
close,1

;;; Write the gas velocity field
;
vr = dblarr(nr,nt,nphi)
vtheta = dblarr(nr,nt,nphi)
vphi = dblarr(nr,nt,nphi)
for ir=0,nr-1 do vphi[ir,*,*] = sqrt(GG*mstar/r[ir])
file_delete,'gas_velocity.inp',/allow_nonexistent
openw,1,'gas_velocity.inp'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,vr[i,j,k],vtheta[i,j,k],vphi[i,j,k]
      endfor
   endfor
endfor
close,1

;;; write the microturbulence file
;
; add a constant turbulent motion of 0.01 km/s -- same as Rosenfeld
vturb0 = 1d3;1d4
vturb = dblarr(nr,nt,nphi) + vturb0
file_delete,'microturbulence.inp',/allow_nonexistent
openw,1,'microturbulence.inp'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,vturb[i,j,k]
      endfor
   endfor
endfor
close,1

;;; Write the wavelength file
;
lambda_oir1 = 0.1
lambda_oir2 = 25.0
n_oir = 20
lambda_oir = lambda_oir1 * (lambda_oir2/lambda_oir1)^(dindgen(n_oir)/(n_oir-1))
lambda_mm  = [880.0, 1330.0, 2700.0]
lambda = [lambda_oir, lambda_mm]
nlam = n_elements(lambda)
file_delete,'wavelength_micron.inp',/allow_nonexistent
openw,1,'wavelength_micron.inp'
printf,1,nlam
for ilam=0,nlam-1 do printf,1,lambda[ilam]
close,1

;;; Dust opacity control file
;
file_delete,'dustopac.inp',/allow_nonexistent
openw,1,'dustopac.inp'
printf,1,'2               Format number of this file'
printf,1,'1               Nr of dust species'
printf,1,'============================================================================'
printf,1,'1               Way in which this dust species is read'
printf,1,'0               0=Thermal grain'
printf,1,'andrews         Extension of name of dustkappa_***.inp file'
printf,1,'----------------------------------------------------------------------------'
close,1

;;; Write the radmc3d.inp control file
;
nphot = 100000
file_delete,'radmc3d.inp',/allow_nonexistent
openw,1,'radmc3d.inp'
printf,1,'nphot = ',nphot
printf,1,'scattering_mode_max = 0'
printf,1,'istar_sphere = 1'
printf,1,'tgas_eq_tdust = 1'
printf,1,'lines_mode = 1'
close,1

return
end
;---------------------------------------------------------
