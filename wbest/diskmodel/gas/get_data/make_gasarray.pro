PRO MAKE_GASARRAY, FILEPATH=filepath, GAS=gas

;+
;  Reads the array of structures containing the date from the make_gas models.
;  Stores the fluxes in an array named "gasarray", and the freeze-out and
;  dissociated fractions in an array named "fracarray".
;
;  The array of structures uses the following flags:
;      Ms = stellar mass (Msun)
;      Mg = gas mass (Msun)
;      g = gamma -- gradient parameter for disk surface density profile
;      Rin = inner radius of the disk (AU)
;      Rc = characteristic radius of the disk (AU)
;      Tm1 = characteristic midplane temperature of the disk (K)
;      qm = exponential index for the midplane temperature of the disk
;      Ta1 = characteristic atmospheric temperature of the disk (K)
;      qm = exponential index for the atmospheric temperature of the disk
;      Npd = photodissociation column depth (10^21 cm2)
;      Tf = freeze-out temperature for CO (K)
;      in = disk inclination angle (deg)
;      freeze = frozen-out CO M:mass (Msun), f:fraction, C:correction factor
;      dissoc = photodissociated CO M:mass (Msun), f:fraction, C:correction factor
;      co12 = CO flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;      co13 = 13CO flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;      c18o = C18O flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;
;  gasarray has the following structure:
;     gasarray = dblarr( n(Ms), n(Mg), n(g), n(Rin), n(Rc), n(Tm1), n(qm), n(Ta1), n(qm), n(Npd), n(Tf), n(in), 3, 3 )
;  Think of it as a 12-D grid whose axes are the model parameters.  At each grid
;  point is a 3x3 matrix containing the nine fluxes.
;
;  fracarray has the following structure:
;     gasarray = dblarr( n(Ms), n(Mg), n(g), n(Rin), n(Rc), n(Tm1), n(qm), n(Ta1), n(qm), n(Npd), n(Tf), n(in), 2 )
;  Think of it as a 12-D grid whose axes are the model parameters.  At each grid
;  point is a 2-element vector containing [f_dissoc, f_frozen].
;
;  The two arrays are output in an IDL save file called gasarray.sav, in the
;  directory indicated by FILEPATH.
;  
;  HISTORY
;  Written by Will Best (IfA), 11/10/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/biggas/
;      GAS - "gas" structure containing the data.  If none is supplied, the
;            program will load 'gas.sav' from FILEPATH.
;-

;;; GET THE DATA
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/biggas/'
if not keyword_set(gas) then gas='gas.sav'
restore, filepath+gas

; Number of disk parameters
npar = 12


;;; BUILD THE ARRAYS

; Size of the disk parameter space
dimsize = intarr(npar)
for i=0, npar-1 do dimsize[i] = n_elements(gas[uniq(gas.(i), sort(gas.(i)))].(i))

; Create gasarray and fracarray
gasarray = dblarr([dimsize, 3, 3])
fracarray = dblarr([dimsize, 2])

STOP
; start the loops
for i=0, nsets-1 do begin
    flag_fr = 0
    flag_dis = 0
    print
    print, 'Loop #'+trim(start+i)
    print, systime()
    ; go where the data is
    if nsets gt 1 then readpath = filepath+setnames+trim(start+i)+'_output/'
    cd, readpath
    ; count the number of models we're getting the results for
    nsim = long(file_lines('report.txt') - 1)
    print, 'Number of models to be read: '+trim(nsim)
    ; read the report file, to get the paths to the data files
    readcol, 'report.txt', run, star, disk, temp, chem, incl, $
             delim=' ', comment='#', format='f,a,a,a,a,a', /silent
    ; create the array of structures
    gas = replicate({Ms:0., Mg:0., g:0., Rin:0., Rc:0, Tm1:0, qm:0., Ta1:0, qa:0., Npd:0., Tf:0, in:0, $
                 freeze:{M:0., f:0., C:0.}, $
                 dissoc:{M:0., f:0., C:0.}, $
                 co12:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)}, $
                 co13:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)}, $
                 c18o:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)} }, nsim)

    ; count the number of unique inclinations
    nincl = n_elements(uniq(incl, sort(incl)))
    print, '   '+trim(nincl)+' unique inclination(s)'
    ; extract the parameter values for each of the models from the report file paths,
    ;     and save them in vectors
    gas.Ms = rebin(float(strmid(star, 2, 1#strpos(star, '/')-2)), nsim, /sample)
    gas.Mg = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), nsim, /sample)
    gas.g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rin')-1#strpos(disk, '_g')-2)), nsim, /sample)
    gas.Rin = rebin(float(strmid(disk, 1#strpos(disk, '_Rin')+4, 1#strpos(disk, '_Rc')-1#strpos(disk, '_Rin')-4)), nsim, /sample)
    gas.Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '/')-1#strpos(disk, '_Rc')-3)), nsim, /sample)
    gas.Tm1 = rebin(fix(strmid(temp, 3, 1#strpos(temp, '_qm')-3)), nsim, /sample)
    gas.qm = rebin(float(strmid(temp, 1#strpos(temp, '_qm')+3, 1#strpos(temp, '_Ta1')-1#strpos(disk, '_qm')-3)), nsim, /sample)
    gas.Ta1 = rebin(fix(strmid(temp, 1#strpos(temp, '_Ta1')+4, 1#strpos(temp, '_qa')-1#strpos(disk, '_Ta1')-4)), nsim, /sample)
    gas.qa = rebin(float(strmid(temp, 1#strpos(temp, '_qa')+3, 1#strpos(temp, '/')-1#strpos(disk, '_qa')-3)), nsim, /sample)
    gas.Npd = rebin(float(strmid(chem, 3, 1#strpos(chem, '_Tf')-3)), nsim, /sample)
    gas.Tf = rebin(fix(strmid(chem, 1#strpos(chem, '_Tf')+3, 1#strpos(chem, '/')-1#strpos(chem, '_Tf')-3)), nsim, /sample)
    gas.in = rebin(fix(strmid(incl, 1, 1#strpos(star, '/')-1)), nsim, /sample)

    ; for each set of disk paramters, save the freeze-out and dissociation fractions in the "gas" structure
    for j=0, nsim-1 do begin

        ; only need to read in the freeze-out and dissociation fractions
        ;    once per set of inclinations
        if j/nincl eq j/float(nincl) then readcol, $
          star[j]+disk[j]+temp[j]+chem[j]+'gasdata'+trim((j/nincl)+1)+'.txt', $
          Mg_2, M_fr, f_fr, C_fr, M_dis, f_dis, C_dis, $
          delim=' ', comment='%', format='f,f,f,a,f,f,a', /silent
        ; check for problematic correction fractions (if >99.9, will be '****' in data file)
        if strmid(C_fr,0,1) eq '*' then begin
            C_fr = 1. / (1.-M_fr/Mg_2)
            flag_fr = flag_fr + 1
        endif else C_fr = float(C_fr)
        if strmid(C_dis,0,1) eq '*' then begin
            C_dis = 1. / (1.-M_dis/Mg_2)
            flag_dis = flag_dis + 1
        endif else C_dis = float(C_dis)

        gas[j].freeze.M = M_fr
        gas[j].freeze.f = f_fr
        gas[j].freeze.C = C_fr
        gas[j].dissoc.M = M_dis
        gas[j].dissoc.f = f_dis
        gas[j].dissoc.C = C_dis

        ; for each set of disk paramters + inclination, save the fluxes in the "gas" structure
        for k=0, n_elements(isot)-1 do begin
            readcol, readpath+star[j]+disk[j]+temp[j]+chem[j]+incl[j]+isot[k]+$
              '/line_results'+trim((j/nincl)+1)+'_'+isot[k]+'_'+strmid(incl[j],0,strlen(incl[j])-1)+'.txt', $
              thick, thin, total, delim=' ', comment='%', format='f,f,f', /silent
            gas[j].(14+k/3).(k mod 3) = [thick, thin, total]
        endfor
 
    endfor

    ; write the data to file(s)
    cd, filepath
    if nsets gt 1 then begin
        ; make a copy of the gas structure named gasxx, where xx is the number of
        ;    the data set
        gasxx = 'gas'+trim(start+i)
        makegasstr = gasxx+' = gas'
        dummy = execute(makegasstr)
        ; save gasxx to an IDL save file
        savegasstr = 'save, '+gasxx+', filename="'+gasxx+'.sav"'
        dummy = execute(savegasstr)
        ; wipe the gasxx variable, so it doesn't clog up memory
        wipegasstr = 'gas = temporary('+gasxx+')'
        dummy = execute(wipegasstr)
    endif else begin
        forprint, gas, textout='gas.txt', width=135, /nocomment, /silent
        save, gas, filename='gas.sav'
    endelse

    if flag_fr gt 0 then print, trim(flag_fr)+' C_fr values greater than 99.9 found in this loop.'
    if flag_dis gt 0 then print, trim(flag_dis)+' C_dis values greater than 99.9 found in this loop.'

endfor

print
print, 'Done!!'
print

END

