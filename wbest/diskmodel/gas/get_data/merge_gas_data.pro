;nsets=46
;start=10
PRO MERGE_GAS_DATA, NSETS, FILEPATH=filepath, SETNAMES=setnames, START=start

;+
;  Merge save files containing output line strengths from the make_gas simulations.
;  Stores the merged files in a single array of structures, named "gas".
;  Saves "gas" in two formats:
;      Text:           gas.txt
;      IDL save file:  gas.sav
;
;  Run this program after running READ_GAS_DATA
;  
;  HISTORY
;  Written by Will Best (IfA), 08/09/2013
;
;  INPUTS
;      NSETS - Number of save files to merge.
;              Default:  all .sav files in the FILEPATH directory with root
;              name given by SETNAMES, starting with number START.
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/gas/
;      SETNAMES - The root of the names for the save files to be merged.
;                 e.g. for gas1.sav, gas2.sav, etc., setnames='gas'
;                 Default = 'gas'
;      START - Number of first set
;              Default: 1
;-

; set up generic paths
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/gas/'
if not keyword_set(setnames) then setnames = 'gas'
if not keyword_set(start) then start = 1

cd, filepath

; if needed, count the number of files to merge
if n_elements(nsets) eq 0 then begin
    spawn, 'ls '+setnames+'*.sav', rawlist
    setlen = strlen(setnames)
    list = fix(strmid(rawlist, setlen, 1#strpos(rawlist, '.sav')-setlen))
    list = list[where(list ge start)]
    nsets = n_elements(list)
    start = min(list)
endif

; start the loops
for i=0, nsets-1 do begin
    ; read in the save file
    gasxx = setnames+trim(start+i)
    restore, gasxx+'.sav'
    ; merge the restored save file into the 'gas' structure
    mergestr = 'if i eq 0 then gas = temporary('+gasxx+') else gas = [gas, temporary('+gasxx+')]'
    dummy = execute(mergestr)
endfor

; write the data to files
;forprint, gas, textout='gas.txt', /nocomment, /silent;, width=135
header = string('#M_star', 'M_gas', 'gamma', 'R_in', 'R_c', 'T_m1', 'q_m', 'T_a1', 'q_a', 'N_pd', $
         'T_freeze', 'incl', 'M_freeze', 'f_freeze', 'C_freeze', 'M_dissoc', 'f_dissoc', $
         'C_dissoc', 'f_1-0_12co_thick', 'f_1-0_12co_thin', 'f_1-0_12co_total', 'f_2-1_12co_thick', $
         'f_2-1_12co_thin', 'f_2-1_12co_total', 'f_3-2_12co_thick', 'f_3-2_12co_thin', 'f_3-2_12co_total', $
         'f_1-0_13co_thick', 'f_1-0_13co_thin', 'f_1-0_13co_total', 'f_2-1_13co_thick', $
         'f_2-1_13co_thin', 'f_2-1_13co_total', 'f_3-2_13co_thick', 'f_3-2_13co_thin', 'f_3-2_13co_total', $
         'f_1-0_c18o_thick', 'f_1-0_c18o_thin', 'f_1-0_c18o_total', 'f_2-1_c18o_thick', $
         'f_2-1_c18o_thin', 'f_2-1_c18o_total', 'f_3-2_c18o_thick', 'f_3-2_c18o_thin', 'f_3-2_c18o_total', $
         format='(a,44(",",a))')
forprint2, gas.MS, gas.Mg, gas.g, gas.Rin, gas.Rc, gas.Tm1, gas.qm, gas.Ta1, gas.qa, gas.Npd, $
           gas.Tf, gas.in, gas.freeze.M, gas.freeze.f, gas.freeze.C, gas.dissoc.M, gas.dissoc.f, $
           gas.dissoc.C, gas.co12.J10[0], gas.co12.J10[1], gas.co12.J10[2], gas.co12.J21[0], $
           gas.co12.J21[1], gas.co12.J21[2], gas.co12.J32[0], gas.co12.J32[1], gas.co12.J32[2], $
           gas.co13.J10[0], gas.co13.J10[1], gas.co13.J10[2], gas.co13.J21[0], gas.co13.J21[1], $
           gas.co13.J21[2], gas.co13.J32[0], gas.co13.J32[1], gas.co13.J32[2], gas.c18o.J10[0], $
           gas.c18o.J10[1], gas.c18o.J10[2], gas.c18o.J21[0], gas.c18o.J21[1], gas.c18o.J21[2], $
           gas.c18o.J32[0], gas.c18o.J32[1], gas.c18o.J32[2], $
           format='(f5.2,",",e8.2,",",f4.2,",",f5.2,",",f5.1,2(",",f6.1,",",f4.2),",",e8.2,",",f4.1'+$
             ',",",i0,33(",",f7.3))', textout='gas.csv', comment=header, /silent ;, width=135
save, gas, filename='gas.sav'

print
print, 'Done!!'
print

END

