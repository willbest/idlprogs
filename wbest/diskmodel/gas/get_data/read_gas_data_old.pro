;nsets=46
;start=10
;filepath='~/Astro/699-2/results/gas/'
PRO READ_GAS_DATA, FILEPATH=filepath, NSETS=nsets, SETNAMES=setnames;

;+
;  Reads the output line strengths from the make_gas simulations.
;  Stores the results in an array of structures, named "gas", with the
;  following flags:
;      Ms = stellar mass (Msun)
;      Mg = gas mass (Msun)
;      g = gamma -- gradient parameter for disk surface density profile
;      Rin = inner radius of the disk (AU)
;      Rc = characteristic radius of the disk (AU)
;      Tm1 = characteristic midplane temperature of the disk (K)
;      qm = exponential index for the midplane temperature of the disk
;      Ta1 = characteristic atmospheric temperature of the disk (K)
;      qm = exponential index for the atmospheric temperature of the disk
;      Npd = photodissociation column depth (10^21 cm2)
;      Tf = freeze-out temperature for CO (K)
;      in = disk inclination angle (deg)
;      freeze = frozen-out CO M:mass (Msun), f:fraction, C:correction factor
;      dissoc = photodissociated CO M:mass (Msun), f:fraction, C:correction factor
;      co12 = CO flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;      co13 = 13CO flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;      c18o = C18O flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;  Saves "gas" in two formats:
;      Text:           gas.txt
;      IDL save file:  gas.sav
;  
;  HISTORY
;  Written by Will Best (IfA), 04/02/2013
;  08/03/2013 (WB): Added more screen outputs
;  08/07/2013 (WB): Changed counter and nsim to long integer format
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/gas/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;      START - Number of first loop
;              Default: 1
;-

; set up generic paths
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/gas/'
readpath = filepath
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1
if not keyword_set(start) then start = 1

; set up isotopolgue-transition paths
isot = ['co10', 'co21', 'co32', '13co10', '13co21', '13co32', 'c18o10', 'c18o21', 'c18o32']

; Oh, the structure
nsim = lonarr(nsets)
print
print, 'Counting the models to be read'
for i=0, nsets-1 do begin
    if nsets gt 1 then readpath = filepath+setnames+trim(start+i)+'_output/'
    cd, readpath
    nsim[i] = file_lines('report.txt') - 1
endfor
ntot = long(total(nsim, /integer))
print, 'Total number of models: '+trim(ntot)
gas = replicate({Ms:0., Mg:0., g:0., Rin:0., Rc:0, Tm1:0, qm:0., Ta1:0, qa:0., Npd:0., Tf:0, in:0, $
                 freeze:{M:0., f:0., C:0.}, $
                 dissoc:{M:0., f:0., C:0.}, $
                 co12:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)}, $
                 co13:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)}, $
                 c18o:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)} }, ntot)

; start the loops
counter = 0L
for i=0, nsets-1 do begin
    print
    print, 'Loop #'+trim(start+i)
    if nsets gt 1 then readpath = filepath+setnames+trim(start+i)+'_output/'
    cd, readpath
    readcol, 'report.txt', run, star, disk, temp, chem, incl, $
             delim=' ', comment='#', format='f,a,a,a,a,a'

    nincl = n_elements(uniq(incl, sort(incl)))
    print, '   '+trim(nincl)+' unique inclination(s)'
    num = nsim[i]
    Ms = rebin(float(strmid(star, 2, 1#strpos(star, '/')-2)), num, /sample)
    Mg = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), num, /sample)
    g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rin')-1#strpos(disk, '_g')-2)), num, /sample)
    Rin = rebin(float(strmid(disk, 1#strpos(disk, '_Rin')+4, 1#strpos(disk, '_Rc')-1#strpos(disk, '_Rin')-4)), num, /sample)
    Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '/')-1#strpos(disk, '_Rc')-3)), num, /sample)
    Tm1 = rebin(fix(strmid(temp, 3, 1#strpos(temp, '_qm')-3)), num, /sample)
    qm = rebin(float(strmid(temp, 1#strpos(temp, '_qm')+3, 1#strpos(temp, '_Ta1')-1#strpos(disk, '_qm')-3)), num, /sample)
    Ta1 = rebin(fix(strmid(temp, 1#strpos(temp, '_Ta1')+4, 1#strpos(temp, '_qa')-1#strpos(disk, '_Ta1')-4)), num, /sample)
    qa = rebin(float(strmid(temp, 1#strpos(temp, '_qa')+3, 1#strpos(temp, '/')-1#strpos(disk, '_qa')-3)), num, /sample)
    Npd = rebin(float(strmid(chem, 3, 1#strpos(chem, '_Tf')-3)), num, /sample)
    Tf = rebin(fix(strmid(chem, 1#strpos(chem, '_Tf')+3, 1#strpos(chem, '/')-1#strpos(chem, '_Tf')-3)), num, /sample)
    in = rebin(fix(strmid(incl, 1, 1#strpos(star, '/')-1)), num, /sample)

    gas[counter:counter+num-1].Ms = Ms
    gas[counter:counter+num-1].Mg = Mg
    gas[counter:counter+num-1].g = g
    gas[counter:counter+num-1].Rin = Rin
    gas[counter:counter+num-1].Rc = Rc
    gas[counter:counter+num-1].Tm1 = Tm1
    gas[counter:counter+num-1].qm = qm
    gas[counter:counter+num-1].Ta1 = Ta1
    gas[counter:counter+num-1].qa = qa
    gas[counter:counter+num-1].Npd = Npd
    gas[counter:counter+num-1].Tf = Tf
    gas[counter:counter+num-1].in = in

    for j=0, num-1 do begin
        if j/nincl eq j/float(nincl) then readcol, $
          star[j]+disk[j]+temp[j]+chem[j]+'gasdata'+trim((j/nincl)+1)+'.txt', $
          Mg_2, M_fr, f_fr, C_fr, M_dis, f_dis, C_dis, delim=' ', comment='%', format='f,f,f,f,f,f,f', /silent
        gas[j+counter].freeze.M = M_fr
        gas[j+counter].freeze.f = f_fr
        gas[j+counter].freeze.C = C_fr
        gas[j+counter].dissoc.M = M_dis
        gas[j+counter].dissoc.f = f_dis
        gas[j+counter].dissoc.C = C_dis

        for k=0, n_elements(isot)-1 do begin
            readcol, readpath+star[j]+disk[j]+temp[j]+chem[j]+incl[j]+isot[k]+$
              '/line_results'+trim((j/nincl)+1)+'_'+isot[k]+'_'+strmid(incl[j],0,strlen(incl[j])-1)+'.txt', $
              thick, thin, total, delim=' ', comment='%', format='f,f,f', /silent
            gas[j+counter].(14+k/3).(k mod 3) = [thick, thin, total]
 
        endfor
    endfor

    counter = counter + num

endfor

cd, filepath
forprint, gas, textout='gas.txt', width=135, /nocomment, /silent
save, gas, filename='gas.sav'

print
print, 'Done!!'
print

END

