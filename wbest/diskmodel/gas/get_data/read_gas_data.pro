;nsets=46
;start=10
PRO READ_GAS_DATA, FILEPATH=filepath, NSETS=nsets, SETNAMES=setnames, START=start

;+
;  Reads the output line strengths from the make_gas models.
;  Stores the results in an array of structures, named "gas", with the
;  following flags:
;      Ms = stellar mass (Msun)
;      Mg = gas mass (Msun)
;      g = gamma -- gradient parameter for disk surface density profile
;      Rin = inner radius of the disk (AU)
;      Rc = characteristic radius of the disk (AU)
;      Tm1 = characteristic midplane temperature of the disk (K)
;      qm = exponential index for the midplane temperature of the disk
;      Ta1 = characteristic atmospheric temperature of the disk (K)
;      qm = exponential index for the atmospheric temperature of the disk
;      Npd = photodissociation column depth (10^21 cm2)
;      Tf = freeze-out temperature for CO (K)
;      in = disk inclination angle (deg)
;      freeze = frozen-out CO M:mass (Msun), f:fraction, C:correction factor
;      dissoc = photodissociated CO M:mass (Msun), f:fraction, C:correction factor
;      co12 = CO flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;      co13 = 13CO flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;      c18o = C18O flux values for J10:J=1-0 transition, J32:J=1-0 transition, J32:J=3-2 transition
;  If there is only one set of data, the "gas" structure is saved in two formats:
;          Text:           gas.txt
;          IDL save file:  gas.sav
;  If there is more than one set of data, then the structures are named "gasxx",
;      where xx is the number for each set.  They are saved only as IDL save
;      files named "gasxx.sav".  These can be merged together using the program 
;      merge_gas_data.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 04/02/2013
;  08/03/2013 (WB): Added more screen outputs
;  08/07/2013 (WB): Changed counter and nsim to long integer format
;  08/09/2013 (WB): Save results from each set of models separately.
;                   These can be merged together with merge_gas_data.pro.
;  2013-08-22 (WB): Handle C_fr and C_dis values that are saved as '****'.
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/gas/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;      START - Number of first loop
;              Default: 1
;-

; set up generic paths
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/gas/'
readpath = filepath
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1
if not keyword_set(start) then start = 1

; set up isotopolgue-transition paths
isot = ['co10', 'co21', 'co32', '13co10', '13co21', '13co32', 'c18o10', 'c18o21', 'c18o32']


; start the loops
for i=0, nsets-1 do begin
    flag_fr = 0
    flag_dis = 0
    print
    print, 'Loop #'+trim(start+i)
    print, systime()
    ; go where the data is
    if nsets gt 1 then readpath = filepath+setnames+trim(start+i)+'_output/'
    cd, readpath
    ; count the number of models we're getting the results for
    nsim = long(file_lines('report.txt') - 1)
    print, 'Number of models to be read: '+trim(nsim)
    ; read the report file, to get the paths to the data files
    readcol, 'report.txt', run, star, disk, temp, chem, incl, $
             delim=' ', comment='#', format='f,a,a,a,a,a', /silent
    ; create the array of structures
    gas = replicate({Ms:0., Mg:0., g:0., Rin:0., Rc:0, Tm1:0, qm:0., Ta1:0, qa:0., Npd:0., Tf:0, in:0, $
                 freeze:{M:0., f:0., C:0.}, $
                 dissoc:{M:0., f:0., C:0.}, $
                 co12:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)}, $
                 co13:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)}, $
                 c18o:{J10:fltarr(3), J21:fltarr(3), J32:fltarr(3)} }, nsim)

    ; count the number of unique inclinations
    nincl = n_elements(uniq(incl, sort(incl)))
    print, '   '+trim(nincl)+' unique inclination(s)'
    ; extract the parameter values for each of the models from the report file paths,
    ;     and save them in vectors
    gas.Ms = rebin(float(strmid(star, 2, 1#strpos(star, '/')-2)), nsim, /sample)
    gas.Mg = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), nsim, /sample)
    gas.g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rin')-1#strpos(disk, '_g')-2)), nsim, /sample)
    gas.Rin = rebin(float(strmid(disk, 1#strpos(disk, '_Rin')+4, 1#strpos(disk, '_Rc')-1#strpos(disk, '_Rin')-4)), nsim, /sample)
    gas.Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '/')-1#strpos(disk, '_Rc')-3)), nsim, /sample)
    gas.Tm1 = rebin(fix(strmid(temp, 3, 1#strpos(temp, '_qm')-3)), nsim, /sample)
    gas.qm = rebin(float(strmid(temp, 1#strpos(temp, '_qm')+3, 1#strpos(temp, '_Ta1')-1#strpos(disk, '_qm')-3)), nsim, /sample)
    gas.Ta1 = rebin(fix(strmid(temp, 1#strpos(temp, '_Ta1')+4, 1#strpos(temp, '_qa')-1#strpos(disk, '_Ta1')-4)), nsim, /sample)
    gas.qa = rebin(float(strmid(temp, 1#strpos(temp, '_qa')+3, 1#strpos(temp, '/')-1#strpos(disk, '_qa')-3)), nsim, /sample)
    gas.Npd = rebin(float(strmid(chem, 3, 1#strpos(chem, '_Tf')-3)), nsim, /sample)
    gas.Tf = rebin(fix(strmid(chem, 1#strpos(chem, '_Tf')+3, 1#strpos(chem, '/')-1#strpos(chem, '_Tf')-3)), nsim, /sample)
    gas.in = rebin(fix(strmid(incl, 1, 1#strpos(star, '/')-1)), nsim, /sample)

    ; for each set of disk paramters, save the freeze-out and dissociation fractions in the "gas" structure
    for j=0, nsim-1 do begin

        ; only need to read in the freeze-out and dissociation fractions
        ;    once per set of inclinations
        if j/nincl eq j/float(nincl) then readcol, $
          star[j]+disk[j]+temp[j]+chem[j]+'gasdata'+trim((j/nincl)+1)+'.txt', $
          Mg_2, M_fr, f_fr, C_fr, M_dis, f_dis, C_dis, $
          delim=' ', comment='%', format='f,f,f,a,f,f,a', /silent
        ; check for problematic correction fractions (if >99.9, will be '****' in data file)
        if strmid(C_fr,0,1) eq '*' then begin
            C_fr = 1. / (1.-M_fr/Mg_2)
            flag_fr = flag_fr + 1
        endif else C_fr = float(C_fr)
        if strmid(C_dis,0,1) eq '*' then begin
            C_dis = 1. / (1.-M_dis/Mg_2)
            flag_dis = flag_dis + 1
        endif else C_dis = float(C_dis)

        gas[j].freeze.M = M_fr
        gas[j].freeze.f = f_fr
        gas[j].freeze.C = C_fr
        gas[j].dissoc.M = M_dis
        gas[j].dissoc.f = f_dis
        gas[j].dissoc.C = C_dis

        ; for each set of disk paramters + inclination, save the fluxes in the "gas" structure
        for k=0, n_elements(isot)-1 do begin
            readcol, readpath+star[j]+disk[j]+temp[j]+chem[j]+incl[j]+isot[k]+$
              '/line_results'+trim((j/nincl)+1)+'_'+isot[k]+'_'+strmid(incl[j],0,strlen(incl[j])-1)+'.txt', $
              thick, thin, total, delim=' ', comment='%', format='f,f,f', /silent
            gas[j].(14+k/3).(k mod 3) = [thick, thin, total]
        endfor
 
    endfor

    ; write the data to file(s)
    cd, filepath
    if nsets gt 1 then begin
        ; make a copy of the gas structure named gasxx, where xx is the number of
        ;    the data set
        gasxx = 'gas'+trim(start+i)
        makegasstr = gasxx+' = gas'
        dummy = execute(makegasstr)
        ; save gasxx to an IDL save file
        savegasstr = 'save, '+gasxx+', filename="'+gasxx+'.sav"'
        dummy = execute(savegasstr)
        ; wipe the gasxx variable, so it doesn't clog up memory
        wipegasstr = 'gas = temporary('+gasxx+')'
        dummy = execute(wipegasstr)
    endif else begin
        forprint, gas, textout='gas.txt', width=135, /nocomment, /silent
        save, gas, filename='gas.sav'
    endelse

    if flag_fr gt 0 then print, trim(flag_fr)+' C_fr values greater than 99.9 found in this loop.'
    if flag_dis gt 0 then print, trim(flag_dis)+' C_dis values greater than 99.9 found in this loop.'

endfor

print
print, 'Done!!'
print

END

