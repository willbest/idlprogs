PRO CHECK_GAS_DATA_NODE34, FILEPATH=filepath, NSETS=nsets, SETNAMES=setnames, START=start

;+
;  Reads the output line strengths from the make_gas models.
;  Checks the freeze-out and dissocation correction factors, C_fr and C_dis, for
;  outputs of '****', indicating correction 100 or greater.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-08-22
;  Adapted from read_gas_data.pro
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/gasdisks/biggas/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;      START - Number of first loop
;              Default: 1
;-

; set up generic paths
if not keyword_set(filepath) then filepath = '~/gasdisks/biggas/'
readpath = filepath
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1
if not keyword_set(start) then start = 1

; set up isotopolgue-transition paths
isot = ['co10', 'co21', 'co32', '13co10', '13co21', '13co32', 'c18o10', 'c18o21', 'c18o32']


; start the loops
for i=0, nsets-1 do begin
    flag = 0
    print
    print, 'Loop #'+trim(start+i)
    print, systime()
    ; go where the data is
    if nsets gt 1 then readpath = filepath+setnames+trim(start+i)+'_output/'
    cd, readpath
    ; count the number of models we're getting the results for
    nsim = long(file_lines('report.txt') - 1)
    print, 'Number of models to be read: '+trim(nsim)
    ; read the report file, to get the paths to the data files
    readcol, 'report.txt', run, star, disk, temp, chem, incl, $
             delim=' ', comment='#', format='f,a,a,a,a,a', /silent

    ; count the number of unique inclinations
    nincl = n_elements(uniq(incl, sort(incl)))
    print, '   '+trim(nincl)+' unique inclination(s)'
    ; extract the parameter values for each of the models from the report file paths,
    ;     and save them in vectors
    Ms = rebin(float(strmid(star, 2, 1#strpos(star, '/')-2)), nsim, /sample)
    Mg = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), nsim, /sample)
    g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rin')-1#strpos(disk, '_g')-2)), nsim, /sample)
    Rin = rebin(float(strmid(disk, 1#strpos(disk, '_Rin')+4, 1#strpos(disk, '_Rc')-1#strpos(disk, '_Rin')-4)), nsim, /sample)
    Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '/')-1#strpos(disk, '_Rc')-3)), nsim, /sample)
    Tm1 = rebin(fix(strmid(temp, 3, 1#strpos(temp, '_qm')-3)), nsim, /sample)
    qm = rebin(float(strmid(temp, 1#strpos(temp, '_qm')+3, 1#strpos(temp, '_Ta1')-1#strpos(disk, '_qm')-3)), nsim, /sample)
    Ta1 = rebin(fix(strmid(temp, 1#strpos(temp, '_Ta1')+4, 1#strpos(temp, '_qa')-1#strpos(disk, '_Ta1')-4)), nsim, /sample)
    qa = rebin(float(strmid(temp, 1#strpos(temp, '_qa')+3, 1#strpos(temp, '/')-1#strpos(disk, '_qa')-3)), nsim, /sample)
    Npd = rebin(float(strmid(chem, 3, 1#strpos(chem, '_Tf')-3)), nsim, /sample)
    Tf = rebin(fix(strmid(chem, 1#strpos(chem, '_Tf')+3, 1#strpos(chem, '/')-1#strpos(chem, '_Tf')-3)), nsim, /sample)
    in = rebin(fix(strmid(incl, 1, 1#strpos(star, '/')-1)), nsim, /sample)

    ; for each set of disk paramters, read the freeze-out and dissociation fractions in the "gas" structure
    ; only need to read in the freeze-out and dissociation fractions
    ;    once per set of inclinations
    for j=0, nsim-1, nincl do begin
        readcol, star[j]+disk[j]+temp[j]+chem[j]+'gasdata'+trim((j/nincl)+1)+'.txt', $
          Mg_2, M_fr, f_fr, C_fr, M_dis, f_dis, C_dis, $
          delim=' ', comment='%', format='f,f,f,a,f,f,a', /silent
        ; check for problematic correction fractions (if >99.9, will be '****' in data file)
        if strmid(C_fr,0,1) eq '*' then begin
            print, 'C_fr problem'
            flag = 1
        endif else C_fr = float(C_fr)
        if strmid(C_dis,0,1) eq '*' then begin
            print, 'C_dis problem'
            flag = 1
        endif else C_dis = float(C_dis)
    endfor

    if flag eq 0 then print, 'No problems found in this loop.'

endfor

print
print, 'Done!!'
print

END

