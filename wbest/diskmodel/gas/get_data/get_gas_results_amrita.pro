nsets=1
start=28
;nincl=1
;PRO GET_GAS_RESULTS, FILEPATH=filepath, OUTPATH=outpath, SETNAMES=setnames

;+
;  Reads the output line strengths from the make_gas simulations.
;  Specifically, obtains the report.txt file(s) from the top of the output
;  directory tree.  
;  Obtains the line_results_... files from the directories, and bundles them
;  into a tarball in outpath.
;  
;  HISTORY
;  Written by Will Best (IfA), 4/2/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/gasgrid/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      OUTPATH - folder to receive the report.txt and line_results files.
;                Default: ~/Astro/699-2/results/
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;      START - Number of first loop
;              Default: 1
;-

; mark the start time
starttime = systime()

; set up generic paths
if not keyword_set(filepath) then filepath = '~/gasgrid/'
if not keyword_set(outpath) then outpath = '~/gasgrid/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1
if not keyword_set(start) then start = 1


; set up isotopolgue-transition paths
isot = ['co10', 'co21', 'co32', '13co10', '13co21', '13co32', 'c18o10', 'c18o21', 'c18o32']

; start the loops
cd, outpath
for i=start-1, nsets-1+start-1 do begin

; set up paths for this loop
    readpath = setnames+trim(i+1)+'_output/'

; build the list of files to read into tarball
    print, 'Building list of files #'+trim(i+1)
    filelist = readpath+'report.txt'
    
    readcol, filelist, run, star, disk, temp, chem, incl, $
             delim=' ', comment='#', format='a,a,a,a,a,a'
    nincl = n_elements(uniq(incl, sort(incl)))
    print, '   '+trim(nincl)+' unique inclination(s)'

    for j=0, n_elements(run)-1 do begin
        filelist = [filelist, readpath+star[j]+disk[j]+temp[j]+chem[j]+'gasdata'+trim((j/nincl)+1)+'.txt' ]
        for k=0, n_elements(isot)-1 do filelist = [filelist, $
          readpath+star[j]+disk[j]+temp[j]+chem[j]+incl[j]+isot[k]+$
              '/line_results'+trim((j/nincl)+1)+'_'+isot[k]+'_'+strmid(incl[j],0,strlen(incl[j])-1)+'.txt' ]
    endfor

; make the tarball
    print, 'Making tarball #'+trim(i+1)+' of files to copy'
    tarcom = 'tar -cf gasout'+trim(i+1)+'.tar '+strjoin(filelist, ' ')
;    copcom = readpath+'lineout '+writepath
    openw, x, 'tarcom'+trim(i+1)+'.txt', /get_lun
    printf, x, tarcom
    close, x
    spawn, 'source tarcom'+trim(i+1)+'.txt'

endfor

print
print, 'Done!!'
print
print, 'Start time: '+starttime
print, 'Finish time: '+systime()
print

END

