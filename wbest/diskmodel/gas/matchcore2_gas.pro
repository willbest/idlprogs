PRO MATCHCORE2_GAS, CHOSEN, ISO, TRANS, FLUX, UNCERT, MEANGAS, MEANGASU, NMATCH, PARAM, $
                    THIN=thin, LOG=log

;+
;  Returns the mean and standard deviation for the parameter indicated by PARAM 
;  over all of the disks matching the given fluxes and uncertainties for two
;  specified emission lines.
;
;  This program is designed to be called by matchloop2_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-17
;
;  INPUTS
;      CHOSEN - Structure containing disk data to match to line flux(es).
;      ISO - 2-element vector indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - 2-element vector indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - 2-element vector containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - 2-element vector containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      MEANGAS - Mean PARAM value for the matched disks.
;      MEANGASU - Standard deviation of PARAM for the matched disks.
;    To return mean and log(PARAM value) for the matched disks, set the /LOG flag.
;      NMATCH - Number of disks matched to the line flux.
;
;  KEYWORDS
;      LOG - Compute and return mean and standard deviation in log space.
;      THIN - Match the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO, TRANS, FLUX, and UNCERT must be equal length
niso = n_elements(iso)
if (niso ne 2) or (n_elements(trans) ne niso) or (n_elements(flux) ne niso) $
  or (n_elements(uncert) ne niso) then begin
    print
    print, 'Use:  matchcore2_gas, CHOSEN, ISO, TRANS, FLUX, UNCERT, MEANGAS, MEANGASU, PARAM,'
    print, '                      NAMTCH [, thin=thin, log=log]'
    print
    print, '    ISO, TRANS, FLUX, and UNCERT must be two-element vectors.'
    print
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif


;;; FIND THE DISKS THAT MATCH THE FLUX(ES) WITHIN THE UNCERTAINTY
for i=0, niso-1 do begin                         ; Loop over the input line fluxes
    lineflux = chosen.(iso[i]+13).(trans[i]-1)[thin-1]
    matched = where((lineflux ge flux[i]-uncert[i]) and (lineflux le flux[i]+uncert[i]))
; intersect matched with previous matched vectors.
    if i eq 0 then begin
        inters = matched
    endif else begin
        inters = setintersection(inters, matched)
    endelse
endfor

matched = inters


;;; FIND THE MEAN AND STANDARD DEVIATION
p = param - 1

; If no disks matched, set mean and uncertainty to zero.
if matched[0] eq -1 then begin
    nmatch = 0
    meangas = 0.
    meangasu = 0.
endif else begin
    nmatch = n_elements(matched)
    ; If only one disk matched, or all matched disks have same parameter value, set uncertainty to zero.
    maxgas = max(chosen[matched].(p), min=mingas)
    if keyword_set(log) then begin
        meangas = mean(alog10(chosen[matched].(p)))
        if (nmatch eq 1) or (maxgas eq mingas) then meangasu = 0. $
          else meangasu = stddev(alog10(chosen[matched].(p)))
    endif else begin
        meangas = mean(chosen[matched].(p))
        if (nmatch eq 1) or (maxgas eq mingas) then meangasu = 0. $
          else meangasu = stddev(chosen[matched].(p))
    endelse
endelse

END
