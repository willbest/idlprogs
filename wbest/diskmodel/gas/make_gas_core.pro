@radmc3dfits.pro

PRO MAKE_GAS_CORE, STAR, DISK, TEMP, CHEM, IARR, SPARR, JARR, SIMNUM, $
                   DATAROOT=dataroot, FINE=fine, INCOMP=incomp, OUTPATH=outpath, $
                   WORKING=working, PDF=pdf, PS=ps, PROFILE=profile

;+
;  Generates gas density and thermal profiles, determines the CO freeze-out
;  zone, and calculates the integrated line
;  intensities (CO isotopologues) for a model protoplanetary disk.
;
;  Parameters for the star and disk should be entered in make_gas_loop.pro
;  
;  HISTORY
;  Written by Will Best (IfA), 03/24/2013
;  06/19/2013 (WB): Forced formatting for some variables in directory names and report lines
;
;  INPUTS
;      STAR, DISK, TEMP, CHEM, IARR, SPARR, JARR   - Parameters passed by
;             make_gas_loop.pro
;      SIMNUM - simulation number.  Used to name output files.
;
;  KEYWORDS
;      DATAROOT - top level of the data output folder structure.
;                 report.txt goes here.
;      FINE - Use a finer grid for the gas model
;      INCOMP - If set, check for an incomplete run
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;      WORKING - RADMC-3D working directory
;      PDF - If set, create pdf output
;      PS - If set, create postscript output
;      PROFILE - Only generate the profile stats and images.
;                Don't calculate line intensities.
;                Don't check for previous runs.
;                Don't write to the report file.
;
;  CALLS
;
;-

dilute_dust_factor = 1d6
goto, debug
debug:

; mark the start time
starttime = systime()

; Generic working directory
if not keyword_set(working) then working = '~/Astro/699-2/radmc_output'

; Data output paths
if n_elements(dataroot) eq 0 then dataroot = '~/Astro/699-2/results/gas/'
level1 = 'Ms'+trim(star.m,'(f5.2)')+'/'
level2 = 'Mg'+trim(disk.m)+'_g'+trim(disk.gamma,'(f4.2)')+'_Rin'+trim(disk.rin,'(f4.2)')+'_Rc'+trim(disk.rc)+'/'
level3 = 'Tm1'+trim(temp.tm1)+'_qm'+trim(temp.qm,'(f4.2)')+'_Ta1'+trim(temp.ta1)+'_qa'+trim(temp.qa,'(f4.2)')+'/'
level4 = 'Npd'+trim(chem.npd,'(f3.1)')+'_Tf'+trim(chem.tf)+'/'
disk_out = dataroot+level1+level2+level3+level4
spawn, 'mkdir -p '+disk_out

; Close any lingering open write files
close, /all

; Point to generic working directory
cd, working

; Check for iteration number
if n_elements(simnum) eq 0 then simnum = 0

; Start the report file if necessary
if not keyword_set(profile) then begin
    report = dataroot+'report.txt'
    if file_test(report) eq 0 then begin
        openw, 33, report, width=200
        printf, 33, '#Run', 'Star', 'Disk', 'Temperature', 'Chemistry', 'Incl', $
                format='(a,t9,a,t19,a,t50,a,t80,a,t95,a)'
        close, 33
    endif
endif

;;;;;;;;;;
; create gas profile and plot image of profile
if keyword_set(fine) then begin
    make_gas_profile, star=star, disk=disk, temp=temp, chem=chem, dilute=dilute_dust_factor, /fine, $
                      filename=disk_out+'gasdata'+trim(simnum)+'.txt'
    plot_gas_profile, filename=disk_out+'profile'+trim(simnum)+'.eps', pdf=pdf, ps=ps, $
                      chem=chem, dilute=dilute_dust_factor, /fine
endif else begin
    make_gas_profile, star=star, disk=disk, temp=temp, chem=chem, dilute=dilute_dust_factor, $
                      filename=disk_out+'gasdata'+trim(simnum)+'.txt'
    plot_gas_profile, filename=disk_out+'profile'+trim(simnum)+'.eps', pdf=pdf, ps=ps, $
                      chem=chem, dilute=dilute_dust_factor
endelse

if keyword_set(profile) then return

;;;;;;;;;;
; calculate line intensities

; Vary the inclination parameter
for ii=0, n_elements(iarr)-1 do begin
    incl = iarr[ii]

; make the output folders
    runnum = trim(simnum)+'.'+trim(ii+1)
    level5 = 'i'+trim(incl)+'/'
    i_out = disk_out+level5

; check for a previous incomplete run
    if keyword_set(incomp) then begin
    ; if the last line_profile file already exists, write the report line
        linefile = string(format='("line_profile",a0,"_c18o32_i",i0,".ps")', trim(simnum), incl)
        if file_test(i_out+'c18o32/'+linefile) ne 0 then begin
            print, 'Run '+runnum+' with inclination '+trim(incl)+' is already completed.'
            continue
        endif
    endif

; Vary the CO isotopologue
    for is=0, n_elements(sparr)-1 do begin
        species = sparr[is]

; Vary the CO transition
        for ij=0, n_elements(jarr)-1 do begin
            j_high = jarr[ij]

; make output folders
            level6 = species+trim(j_high)+trim(j_high-1)+'/'
            line_out = i_out+level6
            spawn, 'mkdir -p '+line_out
            if keyword_set(outpath) then linefile = outpath $
              else linefile = line_out + string(format='("line_profile",a0,"_",a0,i0,i0,"_i",i0,".ps")', $
                                                trim(simnum),species,J_high,J_high-1,incl)

; check for a previous incomplete run
            if keyword_set(incomp) then begin
            ; if the line_profile file already exists, skip to the next loop
                if file_test(linefile) ne 0 then begin
                    print, 'Run '+runnum+' with species '+species+' and transition '+$
                           trim(J_high)+'-'+trim(J_high-1)+' is already completed.'
                    continue
                endif
            endif

; calculate the integrated line intensities
            calc_thin_fraction, star=star, disk=disk, temp=temp, chem=chem, species=species, $
                                j_high=j_high, incl=incl, filename=linefile, pdf=pdf, ps=ps

; move files over to the output directory
            resfile = string(format='("line_results_",a0,i0,i0,"_i",i0,".txt")',species,J_high,J_high-1,incl)
            resfilenum = string(format='("line_results",a0,"_",a0,i0,i0,"_i",i0,".txt")',$
                              trim(simnum),species,J_high,J_high-1,incl)
            spawn, 'mv '+resfile+' '+line_out+resfilenum
            specfile = string(format='("line_spectra_",a0,i0,i0,"_i",i0,".txt")',species,J_high,J_high-1,incl)
            specfilenum = string(format='("line_spectra",a0,"_",a0,i0,i0,"_i",i0,".txt")',$
                              trim(simnum),species,J_high,J_high-1,incl)
            spawn, 'mv '+specfile+' '+line_out+specfilenum

; move gas intensity image to output directory
            imfile = string(format='("image_",a0,i0,i0,"_i",i0)',species,J_high,J_high-1,incl)
            imfilenum = string(format='("image",a0,"_",a0,i0,i0,"_i",i0)',trim(simnum),species,J_high,J_high-1,incl)
            radmcimage_to_fits, imfile+'.out', imfile+'.fits', 140.
            spawn, 'mv '+imfile+'.fits '+line_out+imfilenum+'.fits'
            spawn, 'rm '+imfile+'.out'

; move tau=1 surface image to output directory
            taufile = string(format='("tau_",a0,i0,i0,"_i",i0)',species,J_high,J_high-1,incl)
            taufilenum = string(format='("tau",a0,"_",a0,i0,i0,"_i",i0)',trim(simnum),species,J_high,J_high-1,incl)
            radmcimage_to_fits, taufile+'.out', taufile+'.fits', 140.
            spawn, 'mv '+taufile+'.fits '+line_out+taufilenum+'.fits'
            spawn, 'rm '+taufile+'.out'

; remove the tausurface data cube
            spawn, 'rm tausurface_3d.out'

        endfor
    endfor

; Write to the report file
    if keyword_set(incomp) then begin
        readcol, report, r2, format='f,x,x,x,x,x', comment='#'
        if n_elements(r2) gt 0 then begin
            prev = where(runnum eq r2)
            if prev[0] gt -1 then continue ; skip writing the report line if it's already there
        endif
    endif
    print
    print, 'Writing report line for run '+runnum
    openw, 33, report, width=200, /append
    printf, 33, runnum, level1, level2, level3, level4, level5, $
            format='(a,t9,a,t19,a,t50,a,t80,a,t95,a)'
    close, 33
endfor

; display the start and finish times
print
print, 'Start time for this simulation: '+starttime
print, 'Finish time for this simulation: '+systime()
print

return
END
