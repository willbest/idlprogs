;PRO MAKEHISTMATCHLOOP_GAS

; Wrapper for histmatch_gas.pro

;mstar = [1.0] ;[0.5, 1.0, 2.0]
;mgas = [1e-4, 1.5e-4, 2.2e-4, 3e-4, 4.7e-4, 6.8e-4, 1e-3, 1.5e-3, 2.2e-3, 3e-3, 4.7e-3, 6.8e-3, 1e-2, 1.5e-2, 2.2e-2, 3e-2]
;gamma = [0.0, 0.4, 0.8, 1.2, 1.5] ;[0.0, 0.8, 1.5]
;rin = [0.05]
;rc = [30., 60., 100., 200., 300.]
;tm1 = [50., 100., 250., 500.]
;qm = [0.55] ;[0.45, 0.55, 0.65]
;ta1 = [300., 500., 1000., 1500.]
;qa = [0.45, 0.55, 0.65]
;npd = [1.3]
;tfreeze = [20]
;incl = [30] ;[0, 30, 60, 90]

thin=3              ; 1 = opt thick, 2 = opt thin, 3 = all emission

flux=[1.26,0.0,2.7,0.81,0.43,1.52,0.0,3.95,0.48,4.45]           ; Jy km s-1
uncert=[1.26/5.,0.24,2.7/5.,0.81/5.,0.43/5.,1.52/5.,0.24,3.95/5,0.48/5.,4.45/5.]
nflux=n_elements(flux)
iso=cmreplicate(2, nflux)              ; 1 = 12CO, 2 = 13CO, 3 = C18O
trans=cmreplicate(2, nflux)            ; 1 = 1-0, 2 = 2-1, 3 = 3-2
;flux=[[1.26,0],[0,0],[2.7,0],[0.81,0],[0.43,0.12],[1.52,0],[0,0],[3.95,0.47],[0.48,0],[4.45,0]]           ; Jy km s-1
;uncert=[[1.26/5,.27],[0.24,0.21],[2.7/5,0.39],[0.81/5,.15],[0.43/5,0.05],[1.52/5,0.3],[0.24,0.21],[3.95/5,0.1],[0.48/5,0.21],[4.45/5,0.18]]
;nflux=10;n_elements(flux)
;iso=cmreplicate([2,3], nflux)              ; 1 = 12CO, 2 = 13CO, 3 = C18O
;trans=cmreplicate([2,2], nflux)            ; 1 = 1-0, 2 = 2-1, 3 = 3-2
title=['AA Tau', 'BP Tau', 'CI Tau', 'CY Tau', 'DL Tau', 'DO Tau', 'DQ Tau', 'Haro 6-13', 'IQ Tau', 'IRAS 04385']

colors=[13]

meanbar=1
;plim=-2.
;silent=1
;ps=1

for i=0, nflux-1 do begin
    print
    print
    print, 'Object: '+title[i]
    histmatch_gas, iso[i], trans[i], flux[i], uncert[i], $
    ;histmatch_gas, iso[*,i], trans[*,i], flux[*,i], uncert[*,i], $
                   filepath='~/Astro/699-2/results/finegas/', datafile='allgas.sav', $
                   mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, $
                   tfreeze=tfreeze, incl=incl, thin=thin, ps=ps, colors=colors, $
                   meanbar=meanbar, silent=silent, title=title[i], $
                   outpath='~/Astro/699-2/results/finegas/Taurus/'+title[i]
    if i lt nflux-1 then if not getyn('Press ENTER to continue or n to quit') then break
    print
endfor

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.  Makes
;  histograms of the various parameter values for all of the disks matching 
;  given fluxes and uncertainties for specified emission lines.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate only disks with those parameter values.
;
;  If no value is specified for a given parameter, then the histograms will
;  include disks with all values of that parameter (that have the values given
;  for other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-08
;
;  INPUTS
;      ISO - Vector (or scalar) indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Vector (or scalar) indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Vector (or scalar) containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - Vector (or scalar) containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/histmatch.eps
;      PS - send output to postscript
;      COLORS - Vector of colors for histograms (using lincolr_wb)
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

END

