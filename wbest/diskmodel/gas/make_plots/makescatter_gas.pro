;PRO MAKESCATTER_GAS

; Wrapper for scatter_gas.pro

;mstar=1.0 ;[0.5, 1.0, 2.0]
;mgas = [3e-4, 1e-3, 3e-3, 1e-2] ;[1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]
;gamma = 0.8 ;[0.0, 0.8, 1.5]
;rin = [0.05]
;rc = [30., 60., 100., 200., 300.]
;tm1 = [50., 100., 250., 500.]
;qm = 0.55 ;[0.45, 0.55, 0.65]
;ta1 = [300., 500., 1000., 1500.]
;qa = [0.45, 0.55, 0.65]
;npd = [1.3]
;tfreeze = [20]
;incl = 30 ;[0, 30, 60, 90]
iso=[2]
trans=[1]
thin=3

;vert=0.0075
;xrange = [0,.5]

;colors=[3]
;fit=1
;expfit=1
logaxis=1
;medmark=1
;reverse=1
spread=1

;ps=1

scatter_gas, 2, filepath='~/Astro/699-2/results/biggas/', mstar=mstar, mgas=mgas, gamma=gamma, $
;scatter_gas, 2, filepath='~/Astro/699-2/results/finegas/', gas='finegas.sav', mstar=mstar, mgas=mgas, gamma=gamma, $
          rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, $
          iso=iso, trans=trans, thin=thin, logaxis=logaxis, fit=fit, spread=spread, ps=ps, expfit=expfit, $
          reverse=reverse, colors=colors, xrange=xrange, yrange=yrange, medmark=medmark, vert=vert
          ;, outpath='~/Astro/699-2/results/frozen/Mgas_hist_frozen'

;; parr = [1, 2, 3, 5, 6, 7, 8, 9, 12];, 13, 14]
;; for i=0, n_elements(parr)-1 do begin
;; scatter_gas, parr[i], filepath='~/Astro/699-2/results/biggas/', mstar=mstar, mgas=mgas, gamma=gamma, $
;;           rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, $
;;           iso=iso, trans=trans, thin=thin, ps=ps;, /reverse, outpath='~/Astro/699-2/results/frozen/Mgas_hist'
;; print, trim(parr[i])+'  Press ENTER for the next graph'
;; dummy = get_kbrd()
;; endfor

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.
;  Makes histograms of the fluxes [[and any of the fractions from disks]] with
;  different values of the parameter indicated by PARAM.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate fluxes only from disks with those
;  parameter values.
;
;  If no value is specified for a given parameter, then histograms are made
;  for disks of all values of that parameter (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 04/05/2013
;
;  INPUTS
;      PARAM - Make bar plots for different values of this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;                13 - Isotopologues of CO
;                14 - CO transitions
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      BINSIZE - size of bins for histograms.
;                Default: 20 bins spanning (max - min)
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/gas
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/gashist.eps
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the bar plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;-

END

