;PRO MAKECORR_GAS

; Wrapper for corr_gas.pro

;ps=1

;mstar=[.5]
;mgas = [3e-3, 1e-2, 3e-2];[3e-4, 1e-3, 3e-3, 1e-2]
;gamma = [0.5];[0.0, 0.5, 1.0]
;rin = [0.05]
;rc = [100.];[20., 50., 100., 200.]
;tm1 = [100]
;qm = [0.3, 0.5]
;ta1 = [500];[300, 500, 1000]
;qa = [0.5]
;npd = [0.5, 1.0, 2.0]
;tfreeze = [20];[20, 40]
;incl = [60];[30, 60, 90]
;iso=[2,3]
;trans=[2]
thin=3
colmin=7;10
rowmax=6;9

;corr_gas, filepath='~/Astro/699-2/results/biggas/', mstar=mstar, mgas=mgas, gamma=gamma, $
corr_gas, filepath='~/Astro/699-2/results/finegas/', gas='finegas.sav', mstar=mstar, mgas=mgas, gamma=gamma, $
          rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=ndp, tfreeze=tfreeze, incl=incl, $
          iso=iso, trans=trans, thin=thin, ps=ps, $;/fracfro, /fracdis, $
          colmin=colmin, colmax=colmax, rowmin=rowmin, rowmax=rowmax

END

