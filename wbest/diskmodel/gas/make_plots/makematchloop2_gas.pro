;PRO MAKEMATCHLOOP2_GAS

; Wrapper for matchloop2_gas.pro

;mstar=1.0 ;[0.5, 1.0, 2.0]
;mgas = [3e-4, 1e-3, 3e-3, 1e-2] ;[1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]
;gamma = 0.8 ;[0.0, 0.8, 1.5]
;rin = [0.05]
;rc = [30., 60., 100., 200., 300.]
;tm1 = [50., 100., 250., 500.]
;qm = 0.55 ;[0.45, 0.55, 0.65]
;ta1 = [300., 500., 1000., 1500.]
;qa = [0.45, 0.55, 0.65]
;npd = [1.3]
;tfreeze = [20]
;incl = 30 ;[0, 30, 60, 90]

thin=3              ; 1 = opt thick, 2 = opt thin, 3 = all emission

;; iso=[2,2]             ; 13CO, 13CO
;; trans=[1,3]           ; 1-0, 3-2
;; ;flux={line_21:findgen(51)/50*5, line_23:findgen(101)/100*128}          ; full span
;; ;uncert = {line_21:findgen(51)/50*5/5 > .25, line_23:findgen(101)/100*128/5 > .25}
;; flux={line_21:findgen(51)/50*2.2, line_23:findgen(101)/100*55}          ; >1 mass bin
;; uncert = {line_21:findgen(51)/50*2.2/5 > .25, line_23:findgen(101)/100*55/5 > .25}

;; iso=[2,3]             ; 13CO, C18O
;; trans=[1,1]           ; 1-0, 1-0
;; ;flux={line_21:findgen(51)/50*5, line_31:findgen(41)/40*1.1}          ; full span
;; ;uncert = {line_21:findgen(51)/50*5/5 > .25, line_31:findgen(41)/40*1.1/5 > .25}
;; ;flux={line_21:(findgen(46)+5)/50*5, line_31:findgen(41)/40*1.1}          ; full span, shifted
;; ;uncert = {line_21:(findgen(46)+5)/50*5/5 > .25, line_31:findgen(41)/40*1.1/5 > .25}
;; flux={line_21:findgen(51)/50*2.2, line_31:findgen(41)/40*0.5}          ; >1 mass bin
;; uncert = {line_21:findgen(51)/50*2.2/5 > .25, line_31:findgen(41)/40*0.5/5 > .25}
;; ;flux={line_21:(findgen(41)+10)/50*2.2, line_31:findgen(41)/40*0.4}          ; >1 mass bin, shifted
;; ;uncert = {line_21:(findgen(41)+10)/50*2.2/5 > .25, line_31:findgen(41)/40*0.4/5 > .25}

;; iso=[2,3]             ; 13CO, C18O
;; trans=[1,3]           ; 1-0, 3-2
;; ;flux={line_21:findgen(51)/50*5, line_33:findgen(101)/100*32}          ; full span
;; ;uncert = {line_21:findgen(51)/50*5/5 > .25, line_33:findgen(101)/100*32/5 > .25}
;; flux={line_21:findgen(51)/50*2.2, line_33:findgen(101)/100*13}          ; >1 mass bin
;; uncert = {line_21:findgen(51)/50*2.2/5 > .25, line_33:findgen(101)/100*13/5 > .25}

;; iso=[2,3]             ; 13CO, C18O
;; trans=[3,1]           ; 3-2, 1-0
;; ;flux={line_23:findgen(101)/100*128, line_31:findgen(41)/40*1.1}          ; full span
;; ;uncert = {line_23:findgen(101)/100*128/5 > .25, line_31:findgen(41)/40*1.1/5 > .25}
;; flux={line_23:findgen(101)/100*55, line_31:findgen(41)/40*0.5}          ; >1 mass bin
;; uncert = {line_23:findgen(101)/100*55/5 > .25, line_31:findgen(41)/40*0.5/5 > .25}

;; iso=[2,3]             ; 13CO, C18O
;; trans=[3,3]           ; 3-2, 3-2
;; ;flux={line_23:findgen(101)/100*128, line_33:findgen(101)/100*32}          ; full span
;; ;uncert = {line_23:findgen(101)/100*128/5 > .25, line_33:findgen(101)/100*32/5 > .25}
;; flux={line_23:findgen(101)/100*55, line_33:findgen(101)/100*13}          ; >1 mass bin
;; uncert = {line_23:findgen(101)/100*55/5 > .25, line_33:findgen(101)/100*13/5 > .25}

iso=[3,3]             ; C18O, C18O
trans=[1,3]           ; 1-0, 3-2
flux={line_31:findgen(41)/40*1.1, line_33:findgen(101)/100*32}          ; full span
uncert = {line_31:findgen(41)/40*1.1/5 > .25, line_33:findgen(101)/100*32/5 > .25}
;flux={line_31:findgen(41)/40*0.5, line_33:findgen(101)/100*13}          ; >1 mass bin
;uncert = {line_31:findgen(41)/40*0.5/5 > .25, line_33:findgen(101)/100*13/5 > .25}

log=1
basenum=200
;list=1
sublim=1

bottom=1
right=1
ps=1

;matchloop2_gas, iso, trans, flux, uncert, 2, meangasarr, meangasuarr, nmatcharr, filepath='~/Astro/699-2/results/biggas/', $
matchloop2_gas, iso, trans, flux, uncert, 2, meangasarr, meangasuarr, nmatcharr, $
                filepath='~/Astro/699-2/results/finegas/', gas='finegas.sav', $
                mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, $
                tfreeze=tfreeze, incl=incl, thin=thin, log=log, basenum=basenum, list=list, sublim=sublim, $
                bottom=bottom, right=right, ps=ps
;print, meangasarr, meangasuarr

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.  Makes
;  histograms of the various parameter values for all of the disks matching 
;  given fluxes and uncertainties for specified emission lines.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate only disks with those parameter values.
;
;  If no value is specified for a given parameter, then the histograms will
;  include disks with all values of that parameter (that have the values given
;  for other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-08
;
;  INPUTS
;      ISO - Vector (or scalar) indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Vector (or scalar) indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Vector (or scalar) containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - Vector (or scalar) containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/histmatch.eps
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

END

