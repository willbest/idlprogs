;PRO MAKEBAYESSELF3LOOP_GAS

; Wrapper for bayesself3_gas.pro

mstar=1.0 ;[0.5, 1.0, 2.0]
;mgas = [3e-4, 1e-3, 3e-3, 1e-2] ;[1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]
;gamma = 0.8 ;[0.0, 0.8, 1.5]
;rin = [0.05]
;rc = [30., 60., 100., 200., 300.]
;tm1 = [50., 100., 250., 500.]
qm = 0.55 ;[0.45, 0.55, 0.65]
;ta1 = [300., 500., 1000., 1500.]
;qa = [0.45, 0.55, 0.65]
;npd = [1.3]
;tfreeze = [20]
incl = 30 ;[0, 30, 60, 90]

thin=3              ; 1 = opt thick, 2 = opt thin, 3 = all emission

iso=[2, 2, 2, 3, 3, 3]
trans=[1, 2, 3, 1, 2, 3]
uncert=[.05, .1, .1, .05, .1, .1]
thresh=[.0066, .007, .0075, .0066, .007, .0075]
;zoom=[.5, 3., 5., .2, .5, 1.]

;plim=-2.
ps=1

print
for i=0, 5 do begin
    for j=i+1, 5 do begin
        for k=j+1, 5 do begin
            title = 'bayes.self.fine.'+trim(iso[i])+trim(trans[i])+'.'+trim(iso[j])+trim(trans[j]) $
                    +'.'+trim(iso[k])+trim(trans[k])+'.full'
            print
            print, 'Starting '+title
            print
            start = systime()
;            bayesself3_gas, [iso[i],iso[j],iso[k]], [trans[i],trans[j],trans[k]], $
;                            [uncert[i],uncert[j],uncert[k]], [thresh[i],thresh[j],thresh[k]], 2, outvals, $
;                            filepath='~/Astro/699-2/results/biggas/', $
;                            outpath='~/Astro/699-2/results/biggas/bayesplots/ALMA/'+title, $
            bayesself3_gas, [iso[i],iso[j],iso[k]], [trans[i],trans[j],trans[k]], $
                            [uncert[i],uncert[j],uncert[k]], [thresh[i],thresh[j],thresh[k]], 2, outvals, $
                            filepath='~/Astro/699-2/results/finegas/', datafile='allgas.sav', zoom=zoom, $
                            outpath='~/Astro/699-2/results/finegas/bayesplots/ALMA/'+title, $
                            mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, $
                            tfreeze=tfreeze, incl=incl, thin=thin, plim=plim, sublim=sublim, ps=ps, title=title, fluxmax=fluxmax
            print, ' Start: ', start
            print, 'Finish: ', systime()
            print
        endfor
    endfor
endfor

;+
;  Runs the Bayesian fitting routine for the parameter indicated by PARAM 
;  over all model fluxes and uncertainties for three specified emission
;  lines. Saves the summary data to an IDL save file named by the OUTPATH
;  keyword.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the fitting will incorporate only disks with those parameter
;  values.
;
;  If no value is specified for a given parameter, then the fitting will include
;  disks with all values of that parameter (that have the values given for other
;  parameters).
;  
;  This program calls bayesselfplots3_gas.pro.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-30
;
;  INPUTS
;      ISO - Three-element vector indicating the CO isotopologue of the emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Three-element vector indicating the CO rotational transition of the
;              emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      UNCERT - Structure containing three vectors (can be one-element) of the
;               uncertainties in flux of the lines to be matched to the disk
;               models.  Entries correspond to those in ISO and TRANS.
;      THRESH - Three-element vector equal to detection threshold for line fluxes.
;               Fluxes less than this value will be considered non-detections.
;               Default: 0.
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      OUTVALS - Array of structures containing values summarizing the results
;                of the Bayesian fitting:
;              { maxbin:most likely Mgas bin, maxp:max. posterior, 
;                nmatch:no. of disks with unnormalized posterior > plim, 
;                expec:expected Mgas, unc:uncertainty in expected Mgas }
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load DATAFILE.
;      DATAFILE - name of the IDL save file containing the "gas".  If none is
;                supplied, the program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      FLUXMAX - Only fit disks whose fluxes do not exceed these values.
;      NOPLOT - Don't make the plot.  Just save the data.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/bayesself.eps
;      PLIM - Count the number of disks with unnormalized posterior above this
;             value.
;      PS - send output to postscript
;      ZOOM - Make a second scatter plot, with xrange=[0, zoom[0]] 
;             and yrange=[0, zoom[1]].
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the plot.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

END

