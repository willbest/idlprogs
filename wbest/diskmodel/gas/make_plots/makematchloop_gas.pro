;PRO MAKEMATCHLOOP_GAS

; Wrapper for matchloop_gas.pro

;mstar=1.0 ;[0.5, 1.0, 2.0]
;mgas = [3e-4, 1e-3, 3e-3, 1e-2] ;[1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]
;gamma = 0.8 ;[0.0, 0.8, 1.5]
;rin = [0.05]
;rc = [30., 60., 100., 200., 300.]
;tm1 = [50., 100., 250., 500.]
;qm = 0.55 ;[0.45, 0.55, 0.65]
;ta1 = [300., 500., 1000., 1500.]
;qa = [0.45, 0.55, 0.65]
;npd = [1.3]
;tfreeze = [20]
;incl = 30 ;[0, 30, 60, 90]

thin=3              ; 1 = opt thick, 2 = opt thin, 3 = all emission

;; iso=[1]             ; 12CO
;; trans=[1]           ; 1-0
;; ;flux={line_11:findgen(101)/100*48}          ; full span
;; ;uncert = {line_11:findgen(101)/100*48/5 > .25}
;; flux={line_11:findgen(101)/100*29}          ; >1 mass bin
;; uncert = {line_11:findgen(101)/100*29/5 > .25}

;; iso=[1]             ; 12CO
;; trans=[2]           ; 2-1
;; ;flux={line_12:findgen(101)/100*245}          ; full span
;; ;uncert = {line_12:findgen(101)/100*245/5 > .25}
;; flux={line_12:findgen(101)/100*170}          ; >1 mass bin
;; uncert = {line_12:findgen(101)/100*170/5 > .25}

;; iso=[1]             ; 12CO
;; trans=[3]           ; 3-2
;; ;flux={line_13:findgen(101)/100*580}          ; full span
;; ;uncert = {line_13:findgen(101)/100*580/5 > .25}
;; flux={line_13:findgen(101)/100*410}          ; >1 mass bin
;; uncert = {line_13:findgen(101)/100*410/5 > .25}

iso=[2]             ; 13CO
trans=[1]           ; 1-0
;flux={line_21:findgen(51)/50*5}          ; full span
;uncert = {line_21:findgen(51)/50*5/5 > .25}
flux={line_21:findgen(51)/50*2.2}          ; >1 mass bin
uncert = {line_21:findgen(51)/50*2.2/5 > .25}

;; iso=[2]             ; 13CO
;; trans=[2]           ; 2-1
;; ;flux={line_22:findgen(101)/100*38}          ; full span
;; ;uncert = {line_22:findgen(101)/100*38/5 > .25}
;; flux={line_22:findgen(101)/100*18}          ; >1 mass bin
;; uncert = {line_22:findgen(101)/100*18/5 > .25}

;; iso=[2]             ; 13CO
;; trans=[3]           ; 3-2
;; ;flux={line_23:findgen(101)/100*128}          ; full span
;; ;uncert={line_23:findgen(101)/100*128/5 > .25}
;; flux={line_23:findgen(101)/100*55}          ; >1 mass bin
;; uncert={line_23:findgen(101)/100*55/5 > .25}

;; iso=[3]             ; C18O
;; trans=[1]           ; 1-0
;; ;flux={line_31:findgen(41)/40*1.1}          ; full span
;; ;uncert={line_31:findgen(41)/40*1.1/5 > .25}
;; flux={line_31:findgen(41)/40*0.4}          ; >1 mass bin
;; uncert={line_31:findgen(41)/40*0.4/5 > .25}

;; iso=[3]             ; C18O
;; trans=[2]           ; 2-1
;; ;flux={line_32:findgen(101)/100*10.2}          ; full span
;; ;uncert = {line_32:findgen(101)/100*10.2/5 > .25}
;; flux={line_32:findgen(51)/50*4}          ; >1 mass bin
;; uncert = {line_32:findgen(51)/50*4/5 > .25}

;; iso=[3]             ; C18O
;; trans=[3]           ; 3-2
;; ;flux={line_33:findgen(101)/100*32}          ; full span
;; ;uncert={line_33:findgen(101)/100*32/5 > .25}
;; flux={line_33:findgen(101)/100*13}          ; >1 mass bin
;; uncert={line_33:findgen(101)/100*13/5 > .25}

log=1
list=1
;sublim=1

;bottom=1
right=1
;ps=1

matchloop_gas, iso, trans, flux, uncert, 2, meangasarr, meangasuarr, nmatcharr, $
               filepath='~/Astro/699-2/results/biggas/', $
;matchloop_gas, iso, trans, flux, uncert, 2, meangasarr, meangasuarr, $
;               filepath='~/Astro/699-2/results/finegas/', gas='finegas.sav', $
               mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, $
               tfreeze=tfreeze, incl=incl, thin=thin, log=log, list=list, sublim=sublim, $
               bottom=bottom, right=right, ps=ps
;print, meangasarr, meangasuarr

;+
;  Gets the means and standard deviations for the parameter indicated by PARAM 
;  over all of the disks matching vectors of fluxes and uncertainties for a
;  single specified emission line.
;  Makes a plot indicating the mean and uncertainty of the matched disk
;  parameter values for each flux.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the means and stddevs will incorporate only disks with those parameter
;  values.
;
;  If no value is specified for a given parameter, then the means and stddevs
;  will include disks with all values of that parameter (that have the values
;  given for other parameters).
;  
;  This program calls matchcore2_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-17
;
;  INPUTS
;      ISO - Scalar indicating the CO isotopologue of the emission line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Scalar indicating the CO rotational transition of the
;              emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Structure containing a single vector (can be one-element) of the
;             fluxes, in Jy km/s, of the line to be matched to the disk models.  
;             Format: { line_1:[fluxes] }
;      UNCERT - Structure containing a single vector (can be one-element) of the
;               uncertainties in flux of the line to be matched to the disk
;               models.  Entries correspond to those in FLUX, using the same 
;               format as FLUX. 
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      MEANGASARR - Vector of mean PARAM values for the matched disks.
;      MEANGASUARR - Vector of standard deviations of PARAM for the matched disks.
;    To return mean and log(PARAM value) for the matched disks, set the /LOG flag.
;      NMATCHARR - Vector of number of matched disks for each line flux.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      LIST - Print all the flux-nmatch-mean-stddev combinations to the screen.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/matchloop.eps
;      PS - send output to postscript
;      SUBLIM - only plot points for which the uncertainty is less than the
;               limiting case of sigma = 0.25.
;      LOG - Compute and return mean and standard deviation in log space.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the plot.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

END

