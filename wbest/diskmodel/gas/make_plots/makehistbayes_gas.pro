;PRO MAKEHISTBAYES_GAS

; Wrapper for histbayes_gas.pro

;mstar=1.0 ;[0.5, 1.0, 2.0]
;mgas = [3e-4, 1e-3, 3e-3, 1e-2] ;[1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]
;gamma = 0.8 ;[0.0, 0.8, 1.5]
;rin = [0.05]
;rc = [30., 60., 100., 200., 300.]
;tm1 = [50., 100., 250., 500.]
;qm = 0.55 ;[0.45, 0.55, 0.65]
;ta1 = [300., 500., 1000., 1500.]
;qa = [0.45, 0.55, 0.65]
;npd = [1.3]
;tfreeze = [20]
;incl = 30 ;[0, 30, 60, 90]

thin=3              ; 1 = opt thick, 2 = opt thin, 3 = all emission

iso=[2,3]              ; 1 = 12CO, 2 = 13CO, 3 = C18O
trans=[2,2]            ; 1 = 1-0, 2 = 2-1, 3 = 3-2
flux=[.43,0.12]           ; Jy km s-1
uncert=flux/5.         ; 20% error
thresh=0.

colors=[13]

conline=1
meanbar=1
;plim=-2.
;noplot=1
;silent=1
title='DL Tau'
;ps=1

histbayes_gas, iso, trans, flux, uncert, thresh, sumvals, filepath='~/Astro/699-2/results/biggas/', $
               mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, $
               tfreeze=tfreeze, incl=incl, thin=thin, ps=ps, colors=colors, conline=conline, meanbar=meanbar, $
               noplot=noplot, plim=plim, silent=silent, title=title, $
               outpath='~/Astro/699-2/results/biggas/'+title

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.  
;  Performs Bayesian inference to determine the values of each disk parameter  
;  given fluxes and uncertainties for specified emission lines.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate only disks with those parameter values.
;
;  If no value is specified for a given parameter, then the histograms will
;  include disks with all values of that parameter (that have the values given
;  for other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-11-10
;
;  INPUTS
;      ISO - Vector (or scalar) indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Vector (or scalar) indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Vector (or scalar) containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - Vector (or scalar) containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;      THRESH - Scalar equal to detection threshold for line fluxes.  Fluxes
;               less than this value will be considered non-detections.
;               Default: 0.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/histmatch.eps
;      PS - send output to postscript
;      COLORS - Vector of colors for histograms (using lincolr_wb)
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

END

