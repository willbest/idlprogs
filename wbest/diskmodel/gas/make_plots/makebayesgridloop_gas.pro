;PRO MAKEBAYESGRIDLOOP_GAS

; Wrapper for bayesgrid_gas.pro

mstar=1.0 ;[0.5, 1.0, 2.0]
;mgas = [3e-4, 1e-3, 3e-3, 1e-2] ;[1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]
;gamma = 0.8 ;[0.0, 0.8, 1.5]
;rin = [0.05]
;rc = [30., 60., 100., 200., 300.]
;tm1 = [50., 100., 250., 500.]
qm = 0.55 ;[0.45, 0.55, 0.65]
;ta1 = [300., 500., 1000., 1500.]
;qa = [0.45, 0.55, 0.65]
;npd = [1.3]
;tfreeze = [20]
incl = 30 ;[0, 30, 60, 90]

thin=3              ; 1 = opt thick, 2 = opt thin, 3 = all emission

iso=[2,2,2,2,2,2,3,3,3,3,3,3]
trans=[1,1,2,2,3,3,1,1,2,2,3,3]
flux=[ {line_21:findgen(101)/100*5}, {line_21z:findgen(101)/100*.8}, $
       {line_22:findgen(101)/100*38}, {line_22z:findgen(101)/100*7}, $
       {line_23:findgen(101)/100*128}, {line_23z:findgen(101)/100*23}, $
       {line_31:findgen(101)/100*1.1}, {line_31z:findgen(101)/100*0.3}, $
       {line_32:findgen(101)/100*10.2}, {line_32z:findgen(101)/100*3}, $
       {line_33:findgen(101)/100*32}, {line_33z:findgen(101)/100*9} ]
uncert=[ {line_21:sqrt((findgen(101)/100*5/20)^2 + (.0066/3)^2)}, {line_21z:sqrt((findgen(101)/100*.8/20)^2 + (.0066/3)^2)}, $
         {line_22:sqrt((findgen(101)/100*38/10)^2 + (.007/3)^2)}, {line_22z:sqrt((findgen(101)/100*7/10)^2 + (.007/3)^2)}, $
         {line_23:sqrt((findgen(101)/100*128/10)^2 + (.0075/3)^2)}, {line_23z:sqrt((findgen(101)/100*23/10)^2 + (.0075/3)^2)}, $
         {line_31:sqrt((findgen(101)/100*1.1/20)^2 + (.0066/3)^2)}, {line_31z:sqrt((findgen(101)/100*0.3/20)^2 + (.0066/3)^2)}, $
         {line_32:sqrt((findgen(101)/100*10.2/10)^2 + (.007/3)^2)}, {line_32z:sqrt((findgen(101)/100*3/10)^2 + (.007/3)^2)}, $
         {line_33:sqrt((findgen(101)/100*32/10)^2 + (.0075/3)^2)}, {line_33z:sqrt((findgen(101)/100*9/10)^2 + (.0075/3)^2)} ]
thresh=[.0066,.0066,.007,.007,.0075,.0075,.0066,.0066,.007,.007,.0075,.0075]
title=[ 'bayes.grid.fine.21.full', 'bayes.grid.fine.21.zoom', $
        'bayes.grid.fine.22.full', 'bayes.grid.fine.22.zoom', $
        'bayes.grid.fine.23.full', 'bayes.grid.fine.23.zoom', $
        'bayes.grid.fine.31.full', 'bayes.grid.fine.31.zoom', $
        'bayes.grid.fine.32.full', 'bayes.grid.fine.32.zoom', $
        'bayes.grid.fine.33.full', 'bayes.grid.fine.33.zoom' ]

;plim=-2.
ps=1

for i=0, 11 do begin
    print
    print
    print, 'Starting '+title[i]
    print
;    bayesgrid_gas, iso[i], trans[i], flux[i], uncert[i], thresh[i], 2, outvals, $
;                   filepath='~/Astro/699-2/results/biggas/', $
;                   outpath='~/Astro/699-2/results/biggas/bayesplots/ALMA/'+title[i], $
    bayesgrid_gas, iso[i], trans[i], flux[i], uncert[i], thresh[i], 2, outvals, $
                   filepath='~/Astro/699-2/results/finegas/', datafile='allgas.sav', $
                   outpath='~/Astro/699-2/results/finegas/bayesplots/ALMA/'+title[i], $
                   mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, $
                   tfreeze=tfreeze, incl=incl, thin=thin, plim=plim, sublim=sublim, ps=ps, title=title[i]
endfor

;+
;  Runs the Bayesian fitting routine for the parameter indicated by PARAM 
;  over a set of fluxes and uncertainties for a single specified emission line.
;  Saves the summary data to an IDL save file named by the OUTPATH keyword.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the fitting will incorporate only disks with those parameter
;  values.
;
;  If no value is specified for a given parameter, then the fitting will include
;  disks with all values of that parameter (that have the values given for other
;  parameters).
;  
;  This program calls bayesscat_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-11-11
;  2013-11-25 (WB): Adapted to call bayesscat_gas.pro
;  2013-11-27 (WB): Added NOPLOT keyword.
;  2013-11-29 (WB): Changed name to bayesgrid_gas.
;
;  INPUTS
;      ISO - Scalar indicating the CO isotopologue of the emission line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Scalar indicating the CO rotational transition of the
;              emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Structure containing a single vector (can be one-element) of the
;             fluxes, in Jy km/s, of the line to be matched to the disk models.  
;             Format: { line_1:[fluxes] }
;      UNCERT - Structure containing a single vector (can be one-element) of the
;               uncertainties in flux of the line to be matched to the disk
;               models.  Entries correspond to those in FLUX, using the same 
;               format as FLUX. 
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;      THRESH - Scalar equal to detection threshold for line fluxes.  Fluxes
;               less than this value will be considered non-detections.
;               Default: 0.
;
;  OUTPUTS
;      OUTVALS - Array of structures containing values summarizing the results
;                of the Bayesian fitting:
;              { maxbin:most likely Mgas bin, maxp:max. posterior, 
;                nmatch:no. of disks with unnormalized posterior > plim, 
;                expec:expected Mgas, unc:uncertainty in expected Mgas }
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load DATAFILE.
;      DATAFILE - name of the IDL save file containing the "gas".  If none is
;                supplied, the program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/bayesgrid.sav
;      NOPLOT - Don't make the plot.  Just save the data.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/bayesgrid.eps
;      PLIM - Count the number of disks with unnormalized posterior above this
;             value.
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the plot.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

END

