;PRO CALLGETFLUXES_GAS

;+
; Script to call getfluxes_gas.pro.
;
; HISTORY
;  Written by Will Best (IfA), 2013-11-14
;-

mstar = 1.0 ;[0.5, 1.0, 2.0]
mgas = 3e-3 ;[1e-4, 3e-4, 1e-3, 3e-3, 1e-2, 3e-2]
gamma = 0.8 ;[0.0, 0.8, 1.5]
rin = 0.05
rc = 100. ;[30., 60., 100., 200., 300.]
tm1 = 100. ;[50., 100., 250., 500.]
qm = 0.55 ;[0.45, 0.55, 0.65]
ta1 = 500. ;[300., 500., 1000., 1500.]
qa = 0.55 ;[0.45, 0.55, 0.65]
npd = 1.3
tfreeze = 20
incl = 30 ;[0, 30, 60, 90]

thin=3

getfluxes_gas, mstar, mgas, gamma, rin, rc, tm1, qm, ta1, qa, npd, tfreeze, incl, chosen, $
               gas=gas, filepath='~/Astro/699-2/results/biggas/', thin=thin


END

