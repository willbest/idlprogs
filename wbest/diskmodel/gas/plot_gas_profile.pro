@readradmc.pro

PRO PLOT_GAS_PROFILE, FILENAME=filename, FINE=fine, PDF=pdf, PS=ps, CHEM=chem, $
                      DILUTE_DUST_FACTOR=dilute_dust_factor

;+
;  Reads in the gas density and temperature files from radmc3d modeling.
;  Plots the gas density profile with isotherm contours on linear axes.
;  
;  HISTORY
;  Written 8/31/2012 (jpw)
;  03/13/13 (jpw): Modified for gas, to include pd and freeze-out boundaries.
;  03/24/13 (WB): Modified to run in loops
;
;  KEYWORDS
;      FILENAME - file path+name for postcript output
;      FINE - Use finer grid
;      PDF - If set, create pdf output
;      PS - If set, create postscript output
;      CHEM - Structure containing CO chemical parameters:
;             npd:N_photodissoc, tf:T_freeze
;             Defaults: {1d21, 20 K}
;      DILUTE_DUST_FACTOR - Dilute "dust" density, which is actually gas
;                           density, so that RADMC-3D does not contaminate the
;                           line intensities with dust emission.
;                           Default:  1d6
;-

@natconst.pro
if not keyword_set(dilute_dust_factor) then dilute_dust_factor = 1d6

;;; number of pixels along x-axis
;
npix = 500
if keyword_set(fine) then npix=1000


;;; use canned radmc3d procedure to read in the
;;; dust_density.inp and dust_temperature.dat files
;;; --> these are actually gas density and temperature
;;; --> and I multiply back the big dilution factor
;;; --> that I put in make_gas_profile
;
struct = read_data(/ddens,/dtemp)

;;; convert from dust to gas density
;;; via a simple scaling by the gas/dust ratio
struct.rho = struct.rho * dilute_dust_factor


;;; spherical coordinate axes
;
rgrid = struct.grid.r/au
nrgrid = struct.grid.nr
tgrid = struct.grid.theta
ntgrid = struct.grid.ntheta


;;; vertically integrate for dissociation contours
;
mu = 2.37d
columnh2 = dblarr(nrgrid,ntgrid)
for ir=0,nrgrid-1 do begin
  z = reverse(rgrid[ir]/tan(tgrid))
  rho_r = reverse(reform(struct.rho[ir,*]))
  col_r = dblarr(ntgrid)
  for iz=0,ntgrid-2 do $
      col_r[iz] = int_tabulated(z[iz:ntgrid-1],rho_r[iz:ntgrid-1],/double)
  col_r[ntgrid-1] = col_r[ntgrid-2]
  columnh2[ir,*] = reverse(col_r) * au / (mu*mp)
endfor

;;; linear axes
;
r_lin_max = 300.0
nr_lin = npix
r_lin = (dindgen(nr_lin)+1d)*r_lin_max/nr_lin
dr_lin = r_lin_max/(nr_lin-1)

z_lin_max = 150.0
aspect_ratio = z_lin_max/r_lin_max
nz_lin = nint(nr_lin*aspect_ratio)
z_lin = (dindgen(nz_lin)+1d)*z_lin_max/nz_lin
dz_lin = z_lin_max/(nz_lin-1)


;;; clumsy interpolation to 2d linear grid
;;; done first in z, then in r
;;; there should be a better 2d way...
;
log_nh2_grid = dblarr(nrgrid,nz_lin)
T_grid = dblarr(nrgrid,nz_lin)
log_columnh2_grid = dblarr(nrgrid,nz_lin)

log_nh2_lin = dblarr(nr_lin,nz_lin)
T_lin = dblarr(nr_lin,nz_lin)
log_columnh2_lin = dblarr(nr_lin,nz_lin)

for ir = 0,nrgrid-1 do begin
  zgrid = rgrid[ir]/tan(tgrid)
  log_nh2_grid[ir,*] = interpol(reform(alog10(struct.rho[ir,*]/(mu*mp))),zgrid,z_lin)
  T_grid[ir,*] = interpol(reform(struct.temp[ir,*]),zgrid,z_lin)
  log_columnh2_grid[ir,*] = interpol(alog10(reform(columnh2[ir,*]>1.0)),zgrid,z_lin)
endfor


for iz = 0,nz_lin-1 do begin
  log_nh2_lin[*,iz] = interpol(log_nh2_grid[*,iz],rgrid,r_lin)
  T_lin[*,iz] = interpol(T_grid[*,iz],rgrid,r_lin)
  log_columnh2_lin[*,iz] = interpol(log_columnh2_grid[*,iz],rgrid,r_lin)
endfor


;;; mask out low densities to avoid overstretching the plot
;
min_nh2 = 1.0d
ind = where(log_nh2_lin lt alog10(min_nh2),n)
if (n gt 0) then log_nh2_lin(ind) = alog10(min_nh2)
if (n gt 0) then T_lin(ind) = !values.d_NaN


;;; freeze-out and dissociate the CO
;
if keyword_set(chem) then begin
    T_freeze = chem.tf
    log_columnh2_pd = alog10(chem.npd) + 21.
endif else begin
    T_freeze = 20.0d
    log_columnh2_pd = 21.1d
endelse
molecules = dblarr(nr_lin,nz_lin)
good = where(T_lin gt T_freeze and log_columnh2_lin gt log_columnh2_pd,nmol)
if (nmol gt 0) then molecules[good] = 1


;;; set up plot page
;
if keyword_set(ps) or keyword_set(pdf) then begin
;if keyword_set(ps) then begin
    if n_elements(filename) eq 0 then filename='diskprofile.ps'
    set_plot,'ps',/interpolate
    device,filename=filename,bits_per_pixel=8 $
           ,xsize=11.0,ysize=11.0*aspect_ratio,xoff=0.0,yoff=0.0 $
           ,/inches,/encapsulated,/color
    cs=1.2
    ct=3
endif else begin
    device,decomposed=0
    xsize=1000
    window,0,xsize=xsize,ysize=xsize*aspect_ratio
    cs=1.7
    ct=1
endelse
pos = [0.10,0.15,0.85,0.90]
tic=0.04

ncol=250
loadct,0,ncolors=ncol
tvlct,fsc_color('Red',/triple),251
tvlct,fsc_color('Grey',/triple),252
tvlct,fsc_color('Green',/triple),253
tvlct,fsc_color('Royal Blue',/triple),254
tvlct,fsc_color('White',/triple),255
red=251
grey=252
green=253
blue=254
white=255
black=0

;;; convert to bytes for displaying
;
blog_nh2 = bytscl(log_nh2_lin,top=ncol)
bar=replicate(1,1b)#bindgen(256)
bbar=bytscl(bar,top=ncol-2)

xmin = 0.0
xmax = r_lin_max
ymin = 0.0
ymax = z_lin_max

loadct,39,ncolors=ncol
tvimage,blog_nh2,position=pos,/half,/keep_aspect,/erase
contour,T_lin,r_lin,z_lin,position=pos $
       ,levels=[10,20,30,40,50], c_labels=1+intarr(5), c_thick=ct+7 $
       ,xra=[xmin,xmax],xsty=1 $
       ,yra=[ymin,ymax],ysty=1 $
       ,xtickname=replicate(' ',10) $
       ,ytickname=replicate(' ',10) $
       ,xthick=ct,ythick=ct,color=white $
       ,charsize=cs,charthick=ct, c_charsize=cs+.4,c_charthick=ct+1 $
       ,/noerase
;contour,log_columnh2_lin,r_lin,z_lin,position=pos $
;       ,levels=[21],c_thick=ct+1,c_color=grey $
;       ,c_annotation='PD' $
;       ,xra=[xmin,xmax],xsty=5 $
;       ,yra=[ymin,ymax],ysty=5 $
;       ,charsize=cs,charthick=ct $
;       ,/noerase
contour,molecules,r_lin,z_lin,position=pos $
       ,levels=[0.99],c_thick=ct+4,c_color=black $
       ,c_annotation='CO' $
       ,xra=[xmin,xmax],xsty=1 $
       ,yra=[ymin,ymax],ysty=1 $
       ,xtitle='R (AU)' $
       ,ytitle='Z (AU)' $
       ,ticklen=0.001 $
       ,charsize=cs,charthick=ct, c_charsize=cs+.2,c_charthick=ct+1 $
       ,/noerase
;contour,log_nh2_lin,r_lin,z_lin,position=pos $
;       ,levels=[2,4,6],c_thick=ct+1,c_color=red $
;       ,xra=[xmin,xmax],xsty=5 $
;       ,yra=[ymin,ymax],ysty=5 $
;       ,charsize=cs,charthick=ct $
;       ,/noerase
plots,xmin,ymin
plots,[xmin,xmin,xmax,xmax,xmin],[ymin,ymax,ymax,ymin,ymin],thick=ct+1,/continue

;;; plot a color wedge showing density scale
;
barpos = [pos[2]+0.03,pos[1],pos[2]+0.05,pos[3]]
ymin=min(log_nh2_lin)
ymax=max(log_nh2_lin)
tvimage,bbar,position=barpos
plot,r_lin,z_lin $
    ,position=barpos $
    ,xra=[0,1],xsty=1 $
    ,yra=[ymin,ymax],ysty=1 $
    ,xticklen=0.00001,xtickname=replicate(' ',10) $
    ,yticklen=0.2,ytickname=replicate(' ',10) $
    ,charsize=cs,charthick=ct,color=white $
    ,xthick=ct,ythick=ct $
    ,/nodata,/noerase
axis,yaxis=1,ysty=1,ticklen=0.001,charsize=cs,charthick=ct
plots,0,ymin
plots,[0,0,1,1,0],[ymin,ymax,ymax,ymin,ymin],/continue,thick=ct
xyouts,2.7,(ymin+ymax)/2,align=0.5,orientation=90 $
      ;,'!6log!D10!N !8n!6(H!D2!N)',charsize=cs,charthick=ct
      ,'!6log !8n!6(H!D2!N) (cm!U-3!N)',charsize=cs,charthick=ct


if keyword_set(ps) or keyword_set(pdf) then begin
  device,/close
  set_plot,'x'
  !p.font=-1
  print,"Created color postscript file: "+filename
endif

if keyword_set(pdf) then begin
    if keyword_set(ps) then cgps2pdf, filename, /showcmd else cgps2pdf, filename, /showcmd, /delete_ps
endif


END
