PRO SCATREV_GAS_FLUX, P, NPAR, CHOSEN, NISO, ISO, LABEL, VALS, TRANS, THIN, $
                      ISLAB1, THLAB, TR, TNAME, HSPLIT, LOGFLAG=logflag, PS=ps, OUTPATH=outpath

;+
;  Called by scatter_gas.pro
;
;  Makes scatter plots of the CO fluxes from the make_gas simulations.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-04-28
;  2013-08-26 (WB): Finally enabled plotting for data partitioned by 
;                   isotopologue (PARAM=13) and transition (PARAM=14).
;                   Moved the scom stuff from scatter_gas to here.
;                   Minor efficiency tweaks
;
;  KEYWORDS
;      FIT - Fit a line to the data.
;      LOGFLAG - Make the x-axis (parameter) logarithmic.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/gasscatter.eps
;      PS - send output to postscript
;-

; Break the subset up by values of the chosen parameter (if one has been set as PARAM)
nvals = n_elements(vals)

case p of
    npar-1 : begin           ; If PARAM is set to the CO transitions
        ntrans = 1
        sdata = replicate({f:fltarr(n_elements(chosen)*niso)}, nvals)
        for i=0, nvals-1 do begin
            h2 = chosen.(iso[0]+13).(trans[i]-1)[thin-1]
            for k=1, niso-1 do h2 = [h2, chosen.(iso[k]+13).(trans[i]-1)[thin-1]]
            sdata[i].(0) = h2
        endfor
        islab = strjoin(islab1[iso-1])
        islab = strmid(islab, 0, strlen(islab)-3)+'   '
        ileg = islab + replicate(strmid(thlab[thin-1], 0, 1#strlen(thlab)-3), nvals)
    end
    npar-2 : begin              ; If PARAM is set to the CO isotopologues
        ntrans = n_elements(trans)
        ; Build the command that will be used to create the structure
        scomt = 'f'+trim(trans)+':fltarr(n_elements(chosen))-1, ' ; start with flux=-1 for each disk
        scomt = strjoin(scomt)
        scomt = strmid(scomt, 0, strlen(scomt)-2) ; chop off the final (extraneous) ', '
        dummy = execute('sdata = replicate({'+scomt+'}, '+trim(nvals)+')')        ; create the structure for the histogram values
        for i=0, nvals-1 do begin
            for j=0, ntrans-1 do begin                     ; Loop over chosen transitions
                sdata[i].(j) = chosen.(iso[i]+13).(trans[j]-1)[thin-1]  
            endfor
        endfor
        ileg = thlab[thin-1] + label[vals-1]
    end
    else : begin             ; If PARAM is set to anything else
        ntrans = n_elements(trans)
        ; Build the command that will be used to create the structure
        scomt = 'f'+trim(trans)+':fltarr(n_elements(chosen)*niso)-1, ' ; start with flux=-1 for each disk
        scomt = strjoin(scomt)
        scomt = strmid(scomt, 0, strlen(scomt)-2) ; chop off the final (extraneous) ', '
        dummy = execute('sdata = replicate({'+scomt+'}, '+trim(nvals)+')')        ; create the structure for the histogram values
        xarr = chosen.(p)
        smed = fltarr(nvals,ntrans)
        for j=0, ntrans-1 do begin      ; Loop over chosen transitions
            h2 = chosen.(iso[0]+13).(trans[j]-1)[thin-1]
            for k=1, niso-1 do h2 = [h2, chosen.(iso[k]+13).(trans[j]-1)[thin-1]]
            sdata.(j) = h2
            for i=0, nvals-1 do begin
                smed[i,j] = median(h2[where(xarr eq vals[i])])
            endfor
        endfor
        islab = strjoin(islab1[iso-1])
        islab = strmid(islab, 0, strlen(islab)-3)+'   '
        ileg = islab + thlab[thin-1]
    end
endcase

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/gas/gasscatter'
    ps_open, outpath, /color, /en, thick=4
    lchar = 1.5
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
    lchar = 1.6
endelse
xmarg = [8,3]
;; yarr = [6, 3.8, 1.8, 1.2, 1, 0.8]
;; ystretch = yarr[nvals-1]
;; charsarr = [1.4, 1.4, 2.2, 2.2, 2.2]
chars = 1.5
col = [3, 4, 2]
symbol = [16, 17, 15]

; offset so multiple transitions can be seen
; Is the paramater logarithmic?
if not keyword_set(logflag) then begin
    logflag = 0
    if n_elements(vals) gt 1 then adjust = (vals[1]-vals[0])/10. $
      else adjust = vals/10.
    xspan = [min(vals)-(5*adjust), max(vals)+(5*adjust)]
endif else begin
    if n_elements(vals) gt 1 then adjust = (float(vals[1])/vals[0])^(.25) $
      else adjust = vals^(.25)
    xspan = [min(vals)/(1.2*adjust), max(vals)*(1.2*adjust)]
endelse
if n_elements(vals) eq 1 then adjust = adjust / 2.

; Max flux for scatter plots
ymina = [0.]
ymaxa = [0.]
for j=0, ntrans-1 do begin
    ymaxa = [ymaxa, max(sdata.(j))]
endfor
remove, 0, ymaxa
yspan = [min(ymina), max(ymaxa)]

; Set up horizontal axis labels
;string(vals, format='(e6.0)')
if nvals ge 2 then begin
    tickv = vals
    ticks = nvals - 1
    tickn = trim(vals)
endif else begin
    tickv = [xspan[0], vals, xspan[1]]
    ticks = 2
    tickn = [' ', trim(vals), ' ']
endelse
minor = 2
if p eq 1 then tickn = string(vals, format='(e6.0)')   ; exponential format for disk gas masses

plot, xarr, sdata.(0), /nodata, charsize=chars, backg=1, color=0, $
      xrange=xspan, xstyle=1, xtitle=label, xchars=1.1, xticks=ticks, xtickv=tickv, xtickn=tickn, $
      xmargin=xmarg, xminor=minor, xlog=logflag, yrange=yspan, ytitle='Flux (Jy km s!U-1!N)'
          ;ymargin=ymarg, xtickname=replicate(' ',8)

;;    hmed = median(sdata[i].(0))
;;    vline, hmed, lines=2, color=col[tr-1]        ; Vertical line at median value
legend, ileg, box=0, textcolor=0, charsize=lchar   ; label for the parameter
tleg = [' ', tname[trans[0]-1]]
legend, tleg, box=0, textcolor=col[trans[0]-1], charsize=lchar ; label for the transition

for j=0, ntrans-1 do begin
    if keyword_set(logflag) then begin
        oplot, xarr*adjust^((j-(ntrans-1)/2.+randomu(seed, n_elements(xarr))-.5)*(4-ntrans)/1.25), sdata.(j), $
               psym=cgsymcat(symbol[trans[j]-1]), color=col[trans[j]-1], symsize=.5
        plots, vals*adjust^((j-(ntrans-1)/2.)*(4-ntrans)/1.25), smed[*,j], color=0, symsize=2, $
               psym=cgsymcat(symbol[trans[j]-1])
    endif else begin
        oplot, xarr+(j-(ntrans-1)/2.+randomu(seed, n_elements(xarr))-.5)*2*(4-ntrans)*adjust, sdata.(j), $
               psym=cgsymcat(symbol[trans[j]-1]), color=col[trans[j]-1], symsize=.5
        plots, vals+(j-(ntrans-1)/2.)*2*(4-ntrans)*adjust, smed[*,j], color=0, symsize=2, $
               psym=cgsymcat(symbol[trans[j]-1])
    endelse

;;        hmed = median(sdata[i].(j))
;;        vline, hmed, lines=2, color=col[trans[j]-1]             ; Vertical line at median value
    tleg = [replicate(' ', j+1), tname[trans[j]-1]]
    legend, tleg, box=0, textcolor=col[trans[j]-1], charsize=lchar ; label for the median and scatter
endfor

if keyword_set(ps) then ps_close

END

