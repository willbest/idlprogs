PRO SCATTER_GAS, PARAM, GAS=gas, FILEPATH=filepath, REVERSE=reverse, _EXTRA=extrakey, $
                 MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, TA1=ta1, QA=qa, $
                 NPD=npd, TFREEZE=tfreeze, INCL=incl, ISO=iso, TRANS=trans, THIN=thin, $
                 FLUX=flux, FRACTHIN=fracthin, FRACFROZEN=fracfrozen, FRACDIS=FRACDIS, FRACFRODIS=fracfrodis

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.
;  Makes scatter plots of the disk parameter PARAM vs. the fluxes
;  or any of the disk fractions.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the scatter plots will incorporate fluxes only from disks with those
;  parameter values.
;
;  If no value is specified for a given parameter, then scatter plots are made
;  for disks of all values of that parameter (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-04-28
;  2013-09-12 (WB):  Broke off several parts of the code into separate programs/functions.
;
;  INPUTS
;      PARAM - Make plots for different values of this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;                13 - Isotopologues of CO
;                14 - CO transitions
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/gas
;      REVERSE - Reverse the axes, i.e., plot the disk parameter indicated by
;                PARAM vs. the flux or one of the disk fractions.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the scatter plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the scatter plots.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;
;    The following keywords can be used to specify which disk values to plot --
;    line fluxes, optically thick and thin fractions, or frozen or dissociated
;    CO gas fractions.
;    If none of these keywords are set, scatter plots of line fluxes will be
;    plotted (same as setting the FLUX keyword).
;      FLUX - Scatter plots of CO line fluxes.
;      FRACDIS - Scatter plots of dissociated CO fractions.
;      FRACFRO - Scatter plots of frozen CO fractions.
;      FRACFRODIS - Scatter plots of dissociated + frozen CO fractions.
;      FRACTHIN - Scatter plots of optically thin CO line fractions.
;-

;;; IDENITFY THE PARAMETER TO PLOT VS. FLUX
if n_elements(param) eq 0 then param = 2    ; Default scatter plot with disk masses
p = param-1

; Check that the parameter is valid
npar = 14
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif


;;; GET THE DATA
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/gas/'
if not keyword_set(gas) then gas='gas.sav'
restore, filepath+gas


;;; DETERMINE WHICH DISKS TO USE IN CREATING THE PLOTS
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
; in a vector called 'select'.
select = gas_select_disks(gas, keys, npar)


;;; CHOOSE THE DISKS WITH THE REQUESTED PARAMETER VALUES
chosen = gas[select]

; Identify the values for PARAM in the chosen disks.
;     Allow for different numbers of disks for each PARAM value 
case p of
    npar-1 : vals = trans            ; Transitions
    npar-2 : vals = iso              ; Isotopologues
    else : gas_param_different_n, chosen, p, vals, hsplit
endcase


;;; GET LABELS TO BE USED IN LEGENDS FOR THE SCATTER PLOTS
; Label for the axis with the parameter to plot
gas_labels_no_values, p, label, isname, trname, thlab

; Label(s) for specified values of any other parameters
;gas_labels_specified_values, p, valset, keys, parlabel
parlabel = gas_labels_specified_values(p, valset, keys)


;;; What should the parameter be plotted against?  If nothing specified, then
;;;    plot vs. flux.
if n_elements(flux)+n_elements(fracthin)+n_elements(fracfrozen)+n_elements(fracdis) $
  +n_elements(fracfrodis)+n_elements(reverse) eq 0 then flux=1

if keyword_set(flux) then scatter_gas_flux, p, npar, chosen, vals, iso, trans, thin, label, parlabel, $
                            isname, thlab, trname, hsplit, _extra=extrakey

if keyword_set(reverse) then scatrev_gas_flux, p, npar, chosen, niso, iso, label, vals, trans, thin, $
                            islab1, thlab, tr, tname, hsplit, logflag=logflag, ps=ps, $
                            outpath=outpath, _extra=extrakey

END

