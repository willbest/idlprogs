PRO SCATTER_GAS_FLUX, P, NPAR, CHOSEN, VALS, ISO, TRANS, THIN, LABEL, PARLABEL, $
                      ISNAME, THLAB, TRNAME, HSPLIT, _EXTRA=extrakey, XRANGE=xrange, YRANGE=yrange, $
                      COLORS=colors, FIT=fit, LOGAXIS=logaxis, MEDMARK=medmark, $
                      OUTPATH=outpath, PS=ps, SPREAD=spread, SYMBOL=symbol, VERT=vert, EXPFIT=expfit

;+
;  Called by scatter_gas.pro
;
;  Makes scatter plots of the CO fluxes from the make_gas simulations.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-04-28
;  2013-08-26 (WB): Finally enabled plotting for data partitioned by 
;                   isotopologue (PARAM=13) and transition (PARAM=14).
;                   Moved the scom stuff from scatter_gas to here.
;                   Minor efficiency tweaks
;
;  INPUTS
;      P - index indicating the chosen parameter
;
;  KEYWORDS
;      COLORS - Vector containing colors from lincolr_wb.
;      FIT - Fit a line(?) to the data.  NEEDS DEVELOPMENT.
;      LOGAXIS - Make the y-axis (parameter) logarithmic.
;      VERT - Value for a vertical dashed line on the plot
;      MEDMARK - Place a marker on the median flux for each line and bin
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/gasscatter.eps
;      PS - send output to postscript
;      SPREAD - When plotting fluxes for more than one line, offset fluxes from
;               different lines having the same parameter value so they can be
;               seen more easily.
;      SYMBOL - Vector containing symbol indices for cgsymcat.
;
;  TO DO
;      General cleaning up of the code
;      Ability to spread out the rows of data
;-

nvals = n_elements(vals)        ; Number of values for the chosen parameter
niso = n_elements(iso)          ; Number of values for the chosen isotoplogues
ntrans = n_elements(trans)      ; Number of values for the chosen transitions
ndisks = n_elements(chosen)     ; Number of disks in the chosen sample

; Legend will show whether we are plotting (optically) thick, thin, or total emission,
;     and any parameters that have specified values.
ileg = [thlab[thin-1] , parlabel]


;;; PARTITION THE CHOSEN DISKS INTO BINS BY VALUES OF THE CHOSEN PARAMETER (if one has been set as PARAM)
case p of
    npar-1 : gas_partition_fluxes_trans, chosen, vals, iso, $    ; If PARAM is set to the CO transitions
                  trans, thin, nvals, niso, ntrans, ndisks, sdata, smed, nlines, yarr
    npar-2 : gas_partition_fluxes_iso, chosen, vals, iso, $      ; If PARAM is set to the CO isotopologues
                  trans, thin, nvals, niso, ntrans, ndisks, sdata, smed, nlines, yarr
    else : gas_partition_fluxes_param, p, chosen, vals, iso, $   ; If PARAM is set to anything else
                  trans, thin, nvals, niso, ntrans, ndisks, sdata, smed, nlines, yarr
endcase


;;; SET UP FOR PLOTTING
; Basic window stuff
device, decomposed=0
lincolr_wb, /silent     ; Load color table
if keyword_set(ps) then begin                       ; Set up postscript output
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/gas/gasscatter'
    ps_open, outpath, /color, /en, thick=4;, /ps     ; Color, postscript fonts, encapsulated, thicker lines
    lchar = 1.5                                     ; Size for legend characters
endif else begin                                    ; Set up screen output
    window, 0, retain=2, xsize=800, ysize=600
    lchar = 1.6                                     ; Size for legend characters
endelse
chars = 1.5                                         ; Generic character size for plots

; Colors and symbols
if not keyword_set(colors) then colors = [4, 3, 2, 0, 12, 13, 11, 6, 15]
if not keyword_set(symbol) then symbol = [16, 17, 15]

; Set the x-axis range, if not supplied by keyword
if not keyword_set(xrange) then xrange = [0., max(sdata)]

; Determine 'adjust' factor, which sets the y-axis range (if not supplied as a
; keyword) and the plotting width of each of the partitions.
if not keyword_set(logaxis) then logaxis = 0
adjust = gas_plotscale_adjust(vals, logaxis, span=yspan)     ; Get the 'adjust' quantity
if not keyword_set(yrange) then yrange = yspan               ; Set the y-axis range, if not supplied

; Determine 'width' = plotting width of each of the partitions
if keyword_set(logaxis) then width = adjust^((randomu(seed, n_elements(yarr))-.5)*(3-nlines)) $
  else width = adjust*(randomu(seed, n_elements(yarr))-.5)*3*(3-nlines)

; Determine 'spread' = amount to vertically separate fluxes from different lines
; within the bins.
if keyword_set(spread) then begin
    if keyword_set(logaxis) then spread = adjust $
      else spread = 3.3*adjust
endif else begin
    if keyword_set(logaxis) then spread = 1. else spread = 0.
endelse

; Set up vertical axis labels
gas_param_axis_labels, vals, nvals, yrange, tickn, ticks, tickv, minor
if p eq 1 then tickn = string(vals, format='(e6.0)')   ; exponential format for disk gas masses


;;; PLOT THE DATA
; Blank plot, just to create the window
plot, [0], /nodata, charsize=chars, backg=1, color=0, xrange=xrange, xtitle='Flux (Jy km s!U-1!N)', $
      xchars=1.1, yrange=yrange, ytitle=label, ychars=1.1, yticks=ticks, ytickv=tickv, ytickn=tickn, $
      ymargin=ymargin, yminor=minor, /ystyle, ylog=logaxis

; Legend showing type of emission and specified parameter values
legend, ileg, box=0, textcolor=0, charsize=lchar, /right

; Plot the data itself, widened for visibility (width) and separated by line (spread)
for j=0, ntrans-1 do begin                  ; Loop over chosen transitions
    for k=0, niso-1 do begin                ; Loop over chosen isotopologues
        linen = j*niso+k                    ; Which line are we plotting?
        if keyword_set(logaxis) then begin
            oplot, sdata[*,linen], yarr*width/spread^(linen-(nlines-1)/2.), $      ; plot the data
                   psym=cgsymcat(symbol[trans[j]-1]), color=colors[linen], symsize=.5;.7
            if keyword_set(medmark) then plots, smed[*,linen], vals/spread^(linen-(nlines-1)/2.), $  ; median symbols
                   color=0, symsize=2, psym=cgsymcat(symbol[trans[j]-1])
        endif else begin
            oplot, sdata[*,linen], yarr+width-spread*(linen-(nlines-1)/2.), $      ; plot the data
                   psym=cgsymcat(symbol[trans[j]-1]), color=colors[linen], symsize=1;.5;.7
            if keyword_set(medmark) then plots, smed[*,linen], vals-spread*(linen-(nlines-1)/2.), $  ; median symbols
                   color=0, symsize=2, psym=cgsymcat(symbol[trans[j]-1])
        endelse

        ; Legend showing the isotopologue and transition for each line
        tleg = [isname[iso[k]-1]+'  '+trname[trans[j]-1], replicate(' ', nlines-(linen))]
        ;; tleg = tleg[0:n_elements(tleg)-2]    ; remove extra blank line at the bottom
        legend, tleg, box=0, textcolor=colors[linen], charsize=lchar-.1, /right, /bottom
    endfor
endfor


;;; FIT FUNCTIONS TO THE DATA
if keyword_set(fit) and (ntrans eq 1) then begin    ; only fit one emission line (so far)
    ; Create vector of 101 evenly spaced flux values, to be used to plot the best-fit function
    fitxarr = findgen(101)/100*(xrange[1]-xrange[0]) + xrange[0]
    xarr = sdata[*,0]           ; abscissa for the linear fit is the flux data (all of it)
    sortxind = sort(xarr)       ; get the vector indices that sort the flux data into ascending order
    xarr = xarr[sortxind]       ; sort the flux data into ascending order
    yarr = yarr[sortxind]       ; sort the ordinate (chosen parameter values) to match the sorted flux data

    if keyword_set(expfit) then begin
        ; EXPONENTAL FIT
        ; Fitpar[0] * exp(fitpar[1]*z) + fitpar[2]
        fitpar = [5d-3, 1., -.001]
        ;fitpar = [-.01, -7., .01]
        fitc = curvefit(xarr, yarr, weights, fitpar, sigma, yerror=yerr, function_name='expfunc', /double, itmax=50)
        expfunc, fitxarr, fitpar, fityarr
        print, fitpar
        resid = yarr-fitc
    endif else begin
        ; LINEAR FIT:  y = m*x + b
        ;         i.e. param = fitc[1]*flux + fitc[0]
        fitc = linfit(xarr, yarr, sigma=sigma, yfit=yfit)    ; get the linear best-fit coefficients for all the fluxes
        print
        print, 'Linear fit coefficients:'
        print, 'Slope m = '+trim(fitc[1])+' +/- '+trim(sigma[1])
        print, '      b = '+trim(fitc[0])+' +/- '+trim(sigma[0])
        print
        fityarr = fitc[1]*fitxarr + fitc[0]                     ; get y-values for plotting the best-fit line
        resid = yarr - yfit           ; residuals = differences between actual param values and best-fit line
        yerr = sqrt(total(resid^2)/n_elements(resid))           ; rms error
    endelse

    ; Determine the uncertainty in param values:
    ;    Bin the disks and find the rms uncertainty in each bin.
    bin = 10L                        ; bin size
    nrows = n_elements(xarr)         ; number of disks to fit
    nbins = nrows/bin                ; number of bins.  If nrows/bin is not an integer,
                                     ;    the last bin will have more object than the others. 
    yunc = fltarr(nbins)             ; vector for rms param uncertainty in each bin
    xmeds = yunc                     ; vector for median flux value in each bin
    for i=0, nbins-1 do begin
;;         if (nrows - (i+1)*bin) lt 0 then break     ; if beyond the last bin, exit the loop
        if (nrows - (i+1)*bin) lt bin then begin      ; if at the last bin, bin goes to end of data
            yunc[i] = sqrt(total(resid[(i*bin):*]^2)/n_elements(resid[(i*bin):*]))  ; rms uncertainty for last bin
            xmeds[i] = median(xarr[(i*bin):*])                                      ; median flux value for last bin
        endif else begin                              ; if at any earlier bin, use regular bin size
            yunc[i] = sqrt(total(resid[(i*bin):((i+1)*bin)-1]^2)/bin)      ; rms uncertainty for a bin
            xmeds[i] = median(xarr[(i*bin):((i+1)*bin)-1])                 ; median flux value for a bin
        endelse
    endfor

    if keyword_set(expfit) then begin
        ; EXPONENTIAL FIT TO BINNED ERRORS
        efitpar = [-.005, -10, .002]
        ;efitpar = [.005, 4, -.002]
        errfit = curvefit(xmeds, yunc, weights, efitpar, yerror=eyerr, function_name='expfunc', /double, itmax=50)
        expfunc, fitxarr, efitpar, errband
    endif else begin
        ; LINEAR FIT TO BINNED ERRORS
        errfit = linfit(xmeds[*,0], yunc)           ; fit a line to the binned uncertainties
        errband = errfit[1]*fitxarr + errfit[0]     ; get y-values for plotting the binned uncertainties
    endelse

    oploterror, fitxarr, fityarr, errband, errcolor=15, errthick=6, /nohat    ; plot with error bars
    ;oplot, fitxarr, fityarr, color=0, lines=0, thick=6                        ; plot without error bars

    ;; fitleg = [textoidl('\gamma')+' = 0.5', 'i = 60'+textoidl('^\circ'), 'R!Dc!N = 100 AU', ' ', ' ', 'rms error = '+trim(yerr, '(e8.2)')]
    ;; fitleg = ['i = 60'+textoidl('^\circ'), 'R!Dc!N = 100 AU', ' ', ' ', 'rms error = '+trim(yerr, '(e8.2)')]
    ;; fitleg = ['R!Dc!N = 100 AU', ' ', ' ', 'rms error = '+trim(yerr, '(e8.2)')]
    fitleg = 'rms error = '+trim(yerr, '(e8.2)')
    legend, fitleg, chars=lchar, box=0, textc=0, /right, /bottom
    ;print, errfit
    ;print, yunc
    ;print, efitpar
endif

if keyword_set(vert) then vline, vert, lines=2, color=0, ylog=logaxis

if keyword_set(ps) then ps_close


END

