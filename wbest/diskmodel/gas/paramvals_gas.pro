FUNCTION PARAMVALS_GAS, NPAR, CHOSEN, DIMS=dims, COPY=copy

;+
;  Returns a structure containing the unique paramater values for each parameter
;  of the model disks.
;  Each tag of the structure contains a float array with corresponding parameter
;  values.  The arrays therefore have different lengths.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-16
;
;  INPUTS
;      NPAR - Number of parameters.
;      CHOSEN - Array of structures containing the disk data.
;
;  OUTPUT KEYWORDS
;      DIMS - Variable that will contain an integer array with the number of
;             unique values for each parameter, followed by two elements of 10
;             for the number of frozen-out and dissociated fraction bins.
;      COPY - Variable that will contain a copy of the empty parameter values
;             structure, i.e. before the parameter values have been stored.
;-

; Dimensions of the disk parameter space
dims = intarr(npar)
for i=0, npar-1 do dims[i] = n_elements(chosen[uniq(chosen.(i), sort(chosen.(i)))].(i))
dims = [dims, 10, 10]

; Store the unique values for each parameter
pvals = {Ms:fltarr(dims[0]), Mg:fltarr(dims[1]), g:fltarr(dims[2]), Rin:fltarr(dims[3]), $
        Rc:fltarr(dims[4]), Tm1:fltarr(dims[5]), qm:fltarr(dims[6]), Ta1:fltarr(dims[7]), $
        qa:fltarr(dims[8]), Npd:fltarr(dims[9]), Tf:fltarr(dims[10]), in:fltarr(dims[11]), $
        freeze:findgen(10)*.1 + .05, dissoc:findgen(10)*.1 + .05}
copy = pvals
for i=0, npar-1 do pvals.(i) = chosen[uniq(chosen.(i), sort(chosen.(i)))].(i)

return, pvals

END
