PRO WHISK_GAS_FLUX, P, NPAR, CHOSEN, NISO, ISO, LABEL, VALS, TRANS, THIN, $
                      ISNAME, THLAB, TR, TNAME, HSPLIT, PS=ps, OUTPATH=outpath

;+
;  Called by boxw_gas.pro
;
;  Makes scatter plots of the CO fluxes from the make_gas simulations.
;
;  HISTORY
;  Written by Will Best (IfA), 04/29/2013
;
;  INPUTS
;
;  KEYWORDS
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/boxw_gas.eps
;-

; Number of isotopologue-line pairs
ntrans = n_elements(trans)
nvals = n_elements(vals)
nyvals = niso*ntrans
yvals = indgen(nyvals)+1

; Break the subset up by values of the chosen parameter (if one has been set as PARAM)
;; dummy = execute(scom)
zarr = chosen.(p)
smed = fltarr(nvals,nyvals)
supp = smed
slow = smed
ylab = ' '
for k=0, niso-1 do begin      ; Loop over chosen transitions
    for j=0, ntrans-1 do begin
        ylab = [ylab, isname[iso[k]-1]+tname[trans[j]-1]]
        h2 = chosen.(iso[k]+13).(trans[j]-1)[thin-1]
        for i=0, nvals-1 do begin
            list = h2[where(zarr eq vals[i])]
            smed[i,j+k*ntrans] = median(list)
            list = list[sort(list)]
            supp[i,j+k*ntrans] = list[3*n_elements(list)/4 - 1]
            slow[i,j+k*ntrans] = list[n_elements(list)/4 - 1]
        endfor
    endfor
endfor
ileg = thlab[thin-1]

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/gas/whisk_gas'
    ps_open, outpath, /color, /en, thick=4
    lchar = 1.5
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
    lchar = 1.6
endelse
xmarg = [10,3]
chars = 1.5
col = [12, 3, 4, 2, 0, 11]
symbol = [22, 16, 17, 15, 7, 18]

; Set up horizontal axis
xspan = [0, max(supp)*1.05]

; Set up vertical axis
yspan = [0, nyvals+1]
tickv = reverse(indgen(nyvals+2))
ticks = nyvals + 1
tickn = [ylab, ' ']

plot, [0], /nodata, charsize=chars, backg=1, color=0, xmargin=xmarg, xtitle='Flux (Jy km s!U-1!N)', $
      xrange=xspan, /xstyle, yrange=yspan, /ystyle, ytitle='Line', ychars=1.1, ymargin=ymarg, $
      yticks=ticks, ytickv=tickv, ytickn=tickn

;legend, ileg, box=0, textcolor=0, charsize=lchar, /right
;legend, [ileg, replicate(' ', nvals)], box=0, textcolor=0, charsize=lchar, /right, /bottom

for i=0, nvals-1 do begin
;for i=nvals-1, 0, -1 do begin
    oploterror, smed[i,*], reverse(yvals)+(i-(nvals-1)/2.)*.1, smed[i,*]-slow[i,*], fltarr(nyvals), $
                color=col[i], errcolor=col[i], symsize=2, psym=cgsymcat(symbol[i]), $
                errthick=10, /lobar, hatl=400
    oploterror, smed[i,*], reverse(yvals)+(i-(nvals-1)/2.)*.1, supp[i,*]-smed[i,*], fltarr(nyvals), $
                color=col[i], errcolor=col[i], symsize=2, psym=cgsymcat(symbol[i]), $
                errthick=10, /hibar, hatl=400
endfor

;legend, [' ', label+' '], box=0, psym=[3, symbol], color=[1, col], textcolor=[1, col], charsize=lchar, /right
legend, label+' ', box=0, psym=symbol, color=col, textcolor=col, charsize=lchar, /right, /bottom

if keyword_set(ps) then ps_close

END

