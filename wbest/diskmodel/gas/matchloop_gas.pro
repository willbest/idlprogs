PRO MATCHLOOP_GAS, ISO, TRANS, FLUX, UNCERT, PARAM, MEANGASARR, MEANGASUARR, NMATCHARR, $
                   GAS=gas, FILEPATH=filepath, LIST=list, OUTPATH=outpath, PS=ps, SUBLIM=sublim, $
                   MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, $
                   TA1=ta1, QA=qa, NPD=npd, TFREEZE=tfreeze, INCL=incl, _EXTRA=extrakey

;+
;  Gets the means and standard deviations for the parameter indicated by PARAM 
;  over all of the disks matching vectors of fluxes and uncertainties for a
;  single specified emission line.
;  Makes a plot indicating the mean and uncertainty of the matched disk
;  parameter values for each flux.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the means and stddevs will incorporate only disks with those parameter
;  values.
;
;  If no value is specified for a given parameter, then the means and stddevs
;  will include disks with all values of that parameter (that have the values
;  given for other parameters).
;  
;  This program calls matchcore_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-17
;
;  INPUTS
;      ISO - Scalar indicating the CO isotopologue of the emission line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Scalar indicating the CO rotational transition of the
;              emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Structure containing a single vector (can be one-element) of the
;             fluxes, in Jy km/s, of the line to be matched to the disk models.  
;             Format: { line_1:[fluxes] }
;      UNCERT - Structure containing a single vector (can be one-element) of the
;               uncertainties in flux of the line to be matched to the disk
;               models.  Entries correspond to those in FLUX, using the same 
;               format as FLUX. 
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      MEANGASARR - Vector of mean PARAM values for the matched disks.
;      MEANGASUARR - Vector of standard deviations of PARAM for the matched disks.
;    To return mean and log(PARAM value) for the matched disks, set the /LOG flag.
;      NMATCHARR - Vector of number of matched disks for each line flux.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      LIST - Print all the flux-nmatch-mean-stddev combinations to the screen.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/matchloop.eps
;      PS - send output to postscript
;      SUBLIM - only plot points for which the uncertainty is less than the
;               limiting case of sigma = 0.25.
;      LOG - Compute and return mean and standard deviation in log space.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the plot.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

;;; DETERMINE THE PARAMETER FOR WHICH TO FIND MEAN AND STANDARD DEVIATION
if n_elements(param) eq 0 then param = 2    ; Default PARAM is gas mass

; Check input parameter
npar = 12
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif


;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO and TRANS must be scalars.
;                          FLUX and UNCERT must be structures with one tag,
;                          containing vectors of matching lengths.
errflag = 0
niso = n_elements(iso)
if (niso ne 1) or (n_elements(trans) ne niso) then errflag = 1

t_flux = n_tags(flux)
t_uncert = n_tags(uncert)
if (t_flux ne 1) or (t_flux ne t_uncert) then errflag = 1

if n_elements(flux.(0)) ne n_elements(uncert.(0)) then errflag = 1

if errflag then begin
    print
    print, 'Use:  matchloop_gas, ISO, TRANS, FLUX, UNCERT, PARAM [, MEANGASARR, MEANGASUARR, NMATCHARR, gas=gas,'
    print, '           filepath=filepath, list=list, outpath=outpath, ps=ps, log=log, sublim=sublim, '
    print, '           mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa,'
    print, '           npd=npd, tfreeze=tfreeze, incl=incl, thin=thin]'
    print, '    ISO and TRANS must be scalars.'
    print, '    FLUX and UNCERT must be structures with one tag, containing vectors of matching lengths.'
    print
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif


;;; GET THE DATA
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/gas/'
if not keyword_set(gas) then gas='gas.sav'
restore, filepath+gas


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
; in a vector called 'select'.
select = gas_select_disks(gas, keys, npar+2)

; Choose the disks with the requested parameter values
chosen = gas[select]


;;; GET MEAN AND STANDARD DEVIATION FOR THE CHOSEN PARAMETER, FOR THE DISKS THAT 
;;; MATCH THE FLUXES WITHIN THE UNCERTAINTIES
nmean = n_elements(flux.(0))
meangasarr = fltarr(nmean)
meangasuarr = meangasarr
nmatcharr = lonarr(nmean)

for i=0, nmean-1 do begin
    matchcore_gas, chosen, iso, trans, flux.(0)[i], uncert.(0)[i], meangas, meangasu, nmatch, param, $
                   _extra=extrakey
    meangasarr[i] = meangas
    meangasuarr[i] = meangasu
    nmatcharr[i] = nmatch
endfor


;;; PLOT
; Set up for plotting
device, decomposed=0
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = ' ~/Astro/699-2/results/biggas/matchloop'
    ps_open, outpath, /color, /en, thick=4
    lchar = 1.2
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
    lchar = 1.6
endelse

; Color scheme and symbol size
siglimit = .25
red_purple_blue_colorbar, white, black, nlev
colind = (nlev-1)/2. / siglimit
size = 125./nmean

; Get the label details for the specific disk parameter.
gas_labels_histo, param-1, pname
trlab = ['J=1-0', 'J=2-1', 'J=3-2']
islab = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
thlab = ['Optically thick emission', 'Optically thin emission', 'Total emission']

label = 'Flux ('+islab[iso-1]+' '+trlab[trans-1]+')'

; Make the plot
plot, flux.(0), nmatcharr, charsize=1.6, backg=white, color=black, /nodata, xtit=label, ytit='Number Of Matches'
for i=0, nmean-1 do begin
    if not (keyword_set(sublim) and (meangasuarr[i] ge siglimit)) then $
      plots, flux.(0)[i], nmatcharr[i], color=(colind*meangasuarr[i] < (nlev-1)), psym=cgsymcat(14), symsize=size
    ; Print the flux-nmatch-mean-stddev combinations to screen
    if keyword_set(list) then begin
        if i eq 0 then print, '       Flux        N_match      Mean        StdDev'
        print, flux.(0)[i], nmatcharr[i], meangasarr[i], meangasuarr[i]
    endif
endfor

; Legend
leg = [pname, thlab[thin-1], textoidl('\sigma > '+trim(siglimit))+' dex', textoidl('\sigma < '+trim(siglimit))+' dex']
legend, leg, outline_color=black, textcolor=[black, black, 180, 20], charsize=lchar, _extra=extrakey

if keyword_set(ps) then ps_close


END
