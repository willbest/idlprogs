PRO HIST_GAS_FRODIS, P, NPAR, CHOSEN, NISO, ISO, LABEL, VALS, HSPLIT, $
                     BINSIZE=binsize, PS=ps, OUTPATH=outpath

;+
;  Called by hist_gas.pro
;
;  Makes histograms of the frozen-out and dissociated CO fractions from the make_gas simulations.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-05-30
;  2013-08-14 (WB): Allow and plot histograms with unequal numbers of 
;                   elements. Finally enabled plotting for data partitioned by
;                   isotopologue (PARAM=13) and transition (PARAM=14), which 
;                   actually just plots a single histogram for all disks, since
;                   the dissociated fraction doesn't have anything to do
;                   with the isotopologues or transitions.
;  2013-08-20 (WB): Minor efficiency tweaks
;
;  INPUTS
;
;  KEYWORDS
;      BINSIZE - size of bins for histograms.
;                Default: 20 bins spanning (max - min)
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/gashist.eps
;      PS - send output to postscript
;-

; Break the subset up by values of the chosen parameter (if one has been set as PARAM)
case p of
    npar-1 : begin           ; If PARAM is set to the CO transitions
        nvals = 1
        ddata = {f:chosen.dissoc.f}
        fdata = {f:chosen.freeze.f}
        label = 'All disks'
    end
    npar-2 : begin           ; If PARAM is set to the CO isotopologues
        nvals = 1
        ddata = {f:chosen.dissoc.f}
        fdata = {f:chosen.freeze.f}
        label = 'All disks'
    end
    else : begin             ; If PARAM is set to anything else
        nvals = n_elements(vals)
        ddata = replicate({f:fltarr(hsplit)-1}, nvals)   ; start with fraction=-1 for each disk
        fdata = ddata
        for i=0, nvals-1 do begin
            index = where(chosen.(p) eq vals[i])
            ddata[i].f = chosen[index].dissoc.f
            fdata[i].f = chosen[index].freeze.f
        endfor
    end
endcase

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/gas/gas_fro1'
    ps_open, outpath, /color, /en, thick=4
    lchar = 0.9
endif else begin
    window, 0, retain=2, xsize=800, ysize=800
    lchar = 1.6
endelse
!p.multi = [0, 1, nvals, 0, 0]
xmarg = [9,3]
yarr = [6, 3.8, 1.8, 1.2, 1, 0.8]
ystretch = yarr[nvals-1]
charsarr = [1.6, 1.4, 2.2, 2.2, 2.2, 2.2]
chars = charsarr[nvals-1]
col = [3, 4, 2]

; Min and max for histograms
xspan = [0,1]
if n_elements(binsize) eq 0 then binsize = (xspan[1] - xspan[0])/20.

; Make the plots
for i=0, nvals-2 do begin
    fix1 = where(ddata[i].f ge 1.)
    if fix1[0] ge 0 then ddata[i].f[fix1] = .9999
    fix2 = where(fdata[i].f ge 1.)
    if fix2[0] ge 0 then fdata[i].f[fix2] = .9999

    ymarg = [ystretch*i, ystretch*(1-i)]
    yspan = [0., max(histogram(ddata[i].f, binsize=binsize, min=xspan[0], max=xspan[1])) > $
             max(histogram(fdata[i].f, binsize=binsize, min=xspan[0], max=xspan[1]))]

    plothist, ddata[i].f, charsize=chars, backg=1, bin=binsize, color=col[0], $
              xrange=xspan, xtickname=replicate(' ',8), ytickname=' ', yminor=2, xmargin=xmarg, ymargin=ymarg
    plothist, fdata[i].f, /over, bin=binsize, color=col[1]
    ;dmed = median(ddata[i].f)
    dmedarr = ddata[i].f
    dmed = median(dmedarr[where(dmedarr ge 0)])
    vline, dmed, lines=2, color=col[0]        ; Vertical line at median value
    ;fmed = median(fdata[i].f)
    fmedarr = fdata[i].f
    fmed = median(fmedarr[where(fmedarr ge 0)])
    vline, fmed, lines=2, color=col[1]        ; Vertical line at median value
    legend, label[i], box=0, textcolor=0, charsize=lchar, /right     ; label for the parameter
    mleg = [' ', 'Dissociated CO Fraction:  '+textoidl('\mu')+' = '+string(dmed, format='(f4.2)'), $
            'Frozen CO Fraction:  '+textoidl('\mu')+' = '+string(fmed, format='(f4.2)')]
    legend, mleg, box=0, textcolor=[0,col[0],col[1]], charsize=lchar, /right     ; label for the median
endfor

if nvals eq 1 then ymarg = [4,2] else ymarg = [ystretch*(nvals-1),ystretch*(2-nvals)]
i = nvals-1
fix1 = where(ddata[i].f ge 1.)
if fix1[0] ge 0 then ddata[i].f[fix1] = .9999
fix2 = where(fdata[i].f ge 1.)
if fix2[0] ge 0 then fdata[i].f[fix2] = .9999

yspan = [0., max(histogram(ddata[i].f, binsize=binsize, min=xspan[0], max=xspan[1])) > $
         max(histogram(fdata[i].f, binsize=binsize, min=xspan[0], max=xspan[1]))]

plothist, ddata[i].f, xtitle='Fraction', backg=1, color=col[0], $
          bin=binsize, charsize=chars, xchars=1.1, xrange=xspan, yminor=2, xmargin=xmarg, ymargin=ymarg
plothist, fdata[i].f, /over, bin=binsize, color=col[1]
;dmed = median(ddata[i].f)
dmedarr = ddata[i].f
dmed = median(dmedarr[where(dmedarr ge 0)])
vline, dmed, lines=2, color=col[0]    ; Vertical line at median value
;fmed = median(fdata[i].f)
fmedarr = fdata[i].f
fmed = median(fmedarr[where(fmedarr ge 0)])
vline, fmed, lines=2, color=col[1]    ; Vertical line at median value
legend, label[i], box=0, textcolor=0, charsize=lchar, /right     ; label for the parameter
mleg = [' ', 'Dissociated CO Fraction:  '+textoidl('\mu')+' = '+string(dmed, format='(f4.2)'), $
        'Frozen CO Fraction:  '+textoidl('\mu')+' = '+string(fmed, format='(f4.2)')]
legend, mleg, box=0, textcolor=[0,col[0],col[1]], charsize=lchar, /right ; label for the median

xyouts, .04, .52, 'Number of Disks', color=0, chars=2, align=0.5, orient=90, /normal

if keyword_set(ps) then ps_close
!p.multi = [0, 0, 1, 0, 0]

END

