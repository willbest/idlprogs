PRO HIST_GAS, PARAM, GAS=gas, BINSIZE=binsize, FILEPATH=filepath, OUTPATH=outpath, PS=ps, _EXTRA=extrakey, $
              MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, TA1=ta1, QA=qa, $
              NPD=npd, TFREEZE=tfreeze, INCL=incl, ISO=iso, TRANS=trans, THIN=thin, $
              FLUX=flux, FRACTHIN=fracthin, FRACFROZEN=fracfrozen, FRACDIS=FRACDIS, FRACFRODIS=fracfrodis

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.  Makes
;  histograms of the fluxes and any of the fractions from disks, partitioned by
;  different values of the parameter indicated by PARAM.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate fluxes only from disks with those
;  parameter values.
;
;  If no value is specified for a given parameter, then histograms are made
;  for disks of all values of that parameter (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-04-08
;  2013-08-06 (WB): Tweaked the hcom command to allow for PARAM values that
;                   partition the data into histograms with unequal numbers of
;                   elements.
;  2013-08-07 (WB): Finally wrote correct hcom commands for partitioning the
;                   data by isotopologue (PARAM=13) and transition (PARAM=14).
;  2013-08-20 (WB): Moved the hcom stuff to the daughter applications
;                      (hist_gas_flux, etc.)
;                   Minor efficiency tweaks
;
;  INPUTS
;      PARAM - Make bar plots for different values of this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;                13 - Isotopologues of CO
;                14 - CO transitions
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      BINSIZE - size of bins for histograms.
;                Default: 20 bins spanning (max - min)
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/gas
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/gashist.eps
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;
;    The following keywords can be used to specify which histograms to plot --
;    line fluxes, optically thick and thin fractions, or frozen or dissociated
;    CO gas fractions.
;    If none of these keywords are set, histograms of line fluxes will be
;    plotted (same as setting the FLUX keyword).
;      FLUX - Plot histograms of CO line fluxes.
;      FRACDIS - Plot histograms of dissociated CO fractions.
;      FRACFRO - Plot histograms of frozen CO fractions.
;      FRACFRODIS - Plot histograms of dissociated + frozen CO fractions.
;      FRACTHIN - Plot histograms of optically thin CO line fractions.
;-

;;; DETERMINE THE PARAMETER VALUES TO PARTITION THE HISTOGRAMS
if n_elements(param) eq 0 then param = 2    ; Default histogram partition is gas mass
p = param-1

; Check input parameter
npar = 14
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif


;;; GET THE DATA
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/gas/'
if not keyword_set(gas) then gas='gas.sav'
restore, filepath+gas


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Make a structure containing the parameters values from the keywords
if not keyword_set(mstar) then mstar = -1
if not keyword_set(mgas) then mgas = -1
if n_elements(gamma) eq 0 then gamma = -1
if not keyword_set(rin) then rin = -1
if not keyword_set(rc) then rc = -1
if not keyword_set(tm1) then tm1 = -1
if not keyword_set(qm) then qm = -1
if not keyword_set(ta1) then ta1 = -1
if not keyword_set(qa) then qa = -1
if not keyword_set(npd) then npd = -1
if not keyword_set(tfreeze) then tfreeze = -1
if n_elements(incl) eq 0 then incl = -1
if not keyword_set(iso) then iso = [1, 2, 3]
if not keyword_set(trans) then trans = [1, 2, 3]
if not keyword_set(thin) then thin = 3
niso = n_elements(iso)

keys = {mstar:mstar, mgas:mgas, gamma:gamma, rin:rin, rc:rc, tm1:tm1, qm:qm, ta1:ta1, qa:qa, $
        npd:npd, tfreeze:tfreeze, incl:incl};, iso:iso, trans:trans}

;;; Build a large string containing the command to select the disks with the
;;;     desired parameters, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters to select disks with the chosen parameters
for i=0, (npar-3) do begin
    if (keys.(i)[0] ge 0) then begin    ; if the keyword has value(s), go on, else go to next keyword
        match, gas[uniq(gas.(i), sort(gas.(i)))].(i), keys.(i), xx, ind  ; find where parameter matches keyword value
        nind = n_elements(ind)
        if ind[0] ge 0 then begin       ; if parameter matches keyword value at least once, go on
            if nind gt 1 then fstr = fstr + $  ; add the keyword value to the selecting command
                   '(' + strjoin('(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
            fstr = fstr + '(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
            if nind gt 1 then fstr = fstr + ')'
            fstr = fstr + ' and '
        endif
    endif
endfor


;;; GET THE GAS DISK VALUES
; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'    ; replace the last ' and ' with ')'
    dummy = execute(fstr)     ; use EXECUTE to run the selecting command
    chosen = gas[select]
endif else chosen = gas

; Allow for different amounts of data in each histogram
case p of
    npar-1 : vals = trans            ; Transitions
    npar-2 : vals = iso              ; Isotopologues
    else : begin
        vals = chosen[uniq(chosen.(p), sort(chosen.(p)))].(p)
        nvals = n_elements(vals)
        ; Find the modal parameter value among the model disks 
        hsplits = lonarr(nvals)
        for i=0, nvals-1 do hsplits[i] = n_elements(where(chosen.(p) eq vals[i]))
        hsplit = max(hsplits)
    end
endcase

; Make keys for the histograms
tname = ['J=1-0', 'J=2-1', 'J=3-2']
tr = trans[0]

islab1 = [textoidl('^{12}CO')+' + ', textoidl('^{13}CO')+' + ', textoidl('C^{18}O')+' + ']
thlab = ['Optically thick emission   ', 'Optically thin emission   ', 'Total emission   ']

case p of
    0 : label = ['M!Dstar!N = '+trim(vals)+' M!D!9n!X!N']
    1 : label = ['M!Dgas!N = '+string(vals, format='(e6.0)')+' M!D!9n!X!N']
    2 : label = [textoidl('\gamma')+' = '+trim(vals)]
    3 : label = ['R!Din!N = '+trim(vals)+' AU']
    4 : label = ['R!Dc!N = '+trim(vals)+' AU']
    5 : label = ['T!Dmid1!N = '+trim(vals)+' K']
    6 : label = ['q!Dmid!N = '+trim(vals)]
    7 : label = ['T!Datm1!N = '+trim(vals)+' K']
    8 : label = ['q!Datm!N = '+trim(vals)]
    9 : label = ['N!Dpd!N = '+trim(vals)+'!9X!X10!U21!N cm!U-2!N']
    10 : label = ['T!Dfreeze!N = '+trim(vals)+' K']
    11 : label = ['i ='+trim(vals)+textoidl('^\circ')]
    12 : label = ['Isotopologue: '+strmid(islab1, 0, 1#strlen(islab1)-3)]
    else : label = ['Transition: '+tname]
endcase

if n_elements(flux)+n_elements(fracthin)+n_elements(fracfrozen)+n_elements(fracdis) $
  +n_elements(fracfrodis) eq 0 then flux=1

if keyword_set(flux) then hist_gas_flux, p, npar, chosen, niso, iso, label, vals, trans, thin, $
                            islab1, thlab, tr, tname, hsplit, binsize=binsize, ps=ps, outpath=outpath

if keyword_set(fracthin) then hist_gas_thin, p, npar, chosen, niso, iso, label, vals, trans, thin, $
                                islab1, thlab, tr, tname, hsplit, ps=ps, binsize=binsize, outpath=outpath

if keyword_set(fracfrozen) then hist_gas_fro, p, npar, chosen, niso, iso, label, vals, hsplit, $
                               binsize=binsize, ps=ps, outpath=outpath

if keyword_set(fracdis) then hist_gas_dis, p, npar, chosen, iso, label, vals, hsplit, $
                               binsize=binsize, ps=ps, outpath=outpath

if keyword_set(fracfrodis) then hist_gas_frodis, p, npar, chosen, niso, iso, label, vals, hsplit, $
                               binsize=binsize, ps=ps, outpath=outpath


END

