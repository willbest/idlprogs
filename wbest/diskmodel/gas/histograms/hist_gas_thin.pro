PRO HIST_GAS_THIN, P, NPAR, CHOSEN, NISO, ISO, LABEL, VALS, TRANS, THIN, $
                   ISLAB1, THLAB, TR, TNAME, HSPLIT, BINSIZE=binsize, PS=ps, OUTPATH=outpath

;+
;  Called by hist_gas.pro
;
;  Makes histograms of the optically thin CO fraction from the make_gas simulations.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-04-09
;  2013-08-14 (WB): Allow and plot histograms with unequal numbers of 
;                   elements. Finally enabled plotting for data partitioned by
;                   isotopologue (PARAM=13) and transition (PARAM=14).
;  2013-08-20 (WB): Minor efficiency tweaks
;
;  INPUTS
;
;  KEYWORDS
;      BINSIZE - size of bins for histograms.
;                Default: 20 bins spanning (max - min)
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/gashist.eps
;      PS - send output to postscript
;-

; Break the subset up by values of the chosen parameter (if one has been set as PARAM)
nvals = n_elements(vals)

case p of
    npar-1 : begin           ; If PARAM is set to the CO transitions
        ntrans = 1
        hdata = replicate({f:fltarr(n_elements(chosen)*niso)}, nvals)
        for i=0, nvals-1 do begin
            h2 = chosen.(iso[0]+13).(trans[i]-1)[1] / chosen.(iso[0]+13).(trans[i]-1)[2]
            for k=1, niso-1 do h2 = [h2, chosen.(iso[k]+13).(trans[i]-1)[1] / chosen.(iso[k]+13).(trans[i]-1)[2]]
            hdata[i].(0) = h2
        endfor
        islab = strjoin(islab1[iso-1])
        islab = strmid(islab, 0, strlen(islab)-3)+'   '
        label = replicate(islab, nvals)
    end
    npar-2 : begin              ; If PARAM is set to the CO isotopologues
        ntrans = n_elements(trans)
        ; Build the command that will be used to create the structure
        hcomt = 'f'+trim(trans)+':fltarr(n_elements(chosen))-1, ' ; start with flux=-1 for each disk
        hcomt = strjoin(hcomt)
        hcomt = strmid(hcomt, 0, strlen(hcomt)-2) ; chop off the final (extraneous) ', '
        dummy = execute('hdata = replicate({'+hcomt+'}, '+trim(nvals)+')')        ; create the hdata structure for the histogram values
        for i=0, nvals-1 do begin
            for j=0, ntrans-1 do begin                     ; Loop over chosen transitions
                hdata[i].(j) = chosen.(iso[i]+13).(trans[j]-1)[1] / chosen.(iso[i]+13).(trans[j]-1)[2]
            endfor
        endfor
        label = label[vals-1]
    end
    else : begin             ; If PARAM is set to anything else
        ntrans = n_elements(trans)
        ; Build the command that will be used to create the structure
        hcomt = 'f'+trim(trans)+':fltarr('+trim(hsplit*niso)+')-1, ' ; start with flux=-1 for each disk
        hcomt = strjoin(hcomt)
        hcomt = strmid(hcomt, 0, strlen(hcomt)-2) ; chop off the final (extraneous) ', '
        dummy = execute('hdata = replicate({'+hcomt+'}, '+trim(nvals)+')')        ; create the hdata structure for the histogram values
        for i=0, nvals-1 do begin
            index = where(chosen.(p) eq vals[i])
            for j=0, ntrans-1 do begin                     ; Loop over chosen transitions
                h2 = chosen[index].(iso[0]+13).(trans[j]-1)[1] / chosen[index].(iso[0]+13).(trans[j]-1)[2]
                for k=1, niso-1 do h2 = [h2, chosen[index].(iso[k]+13).(trans[j]-1)[1] / $
                                         chosen[index].(iso[k]+13).(trans[j]-1)[2]]
                hdata[i].(j) = h2
            endfor
        endfor
        islab = strjoin(islab1[iso-1])
        islab = strmid(islab, 0, strlen(islab)-3)+'   '
        label = islab + label
    end
endcase

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/gas/gas_thin1'
    ps_open, outpath, /color, /en, thick=4
    lchar = 0.9
endif else begin
    window, 0, retain=2, xsize=800, ysize=800
    lchar = 1.6
endelse
!p.multi = [0, 1, nvals, 0, 0]
xmarg = [9,3]
yarr = [6, 3.8, 1.8, 1.2, 1, 0.8]
ystretch = yarr[nvals-1]
charsarr = [1.4, 1.4, 2.2, 2.2, 2.2, 2.2]
chars = charsarr[nvals-1]
col = [3, 4, 2]

; Min and max for histograms
xspan = [0,1]
if n_elements(binsize) eq 0 then binsize = (xspan[1] - xspan[0])/20.

; Make the plots
for i=0, nvals-2 do begin
    if p eq npar-1 then tr = trans[i]
    for j=0, ntrans-1 do begin
        fix1 = where(hdata[i].(j) ge 1.)
        if fix1[0] ge 0 then hdata[i].(j)[fix1] = .9999
    endfor

    ymarg = [ystretch*i, ystretch*(1-i)]
    yspan = [0., 0.]
    for j=0, ntrans-1 do begin
        yspan[1] = max(histogram(hdata[i].(j), binsize=binsize, min=xspan[0], max=xspan[1])) > yspan[1]
    endfor
    if i eq 0 then rr=1 else rr=0
    
    ; check for case where all data has the same value, which makes plothist crash
    hdmin = min(hdata[i].(0), max=hdmax)
    if hdmin eq hdmax then hdata[i].(0)[0] = hdata[i].(0)[0] - 1e-5

    plothist, hdata[i].(0), charsize=chars, backg=1, bin=binsize, color=col[tr-1], /nan, $;fcolor=col[0], /fill, $  
              xrange=xspan, yrange=yspan, xtickname=replicate(' ',8), ytickname=' ', yminor=2, $
              xmargin=xmarg, ymargin=ymarg
    ;hmed = median(hdata[i].(0))
    hmedarr = hdata[i].(0)
    hmed = median(hmedarr[where(hmedarr ge 0)])
    vline, hmed, lines=2, color=col[tr-1]        ; Vertical line at median value
    legend, label[i], box=0, textcolor=0, charsize=lchar, right=rr     ; label for the parameter
    mleg = [' ', tname[tr-1]+':  '+textoidl('\mu')+' = '+string(hmed, format='(f6.3)')]
    legend, mleg, box=0, textcolor=col[tr-1], charsize=lchar, right=rr     ; label for the median and scatter

    for j=1, ntrans-1 do begin
        plothist, hdata[i].(j), /over, bin=binsize, color=col[trans[j]-1], /nan;, fcolor=col[j], /fill
        ;hmed = median(hdata[i].(j))
        hmedarr = hdata[i].(j)
        hmed = median(hmedarr[where(hmedarr ge 0)])
        vline, hmed, lines=2, color=col[trans[j]-1]             ; Vertical line at median value
        mleg = [replicate(' ', j+1), tname[trans[j]-1]+':  '+textoidl('\mu')+' = '+string(hmed, format='(f6.3)')]
        legend, mleg, box=0, textcolor=col[trans[j]-1], charsize=lchar, right=rr  ; label for the median and scatter
    endfor

endfor

if nvals eq 1 then ymarg = [4,2] else ymarg = [ystretch*(nvals-1),ystretch*(2-nvals)]
i = nvals-1
if p eq npar-1 then tr = trans[i]
for j=0, ntrans-1 do begin
    fix1 = where(hdata[i].(j) ge 1.)
    if fix1[0] ge 0 then hdata[i].(j)[fix1] = .9999
endfor

yspan = [0., 0.]
for j=0, ntrans-1 do begin
    yspan[1] = max(histogram(hdata[i].(j), binsize=binsize, min=xspan[0], max=xspan[1])) > yspan[1]
endfor

plothist, hdata[i].(0), xtitle='Optically Thin Flux Fraction', backg=1, color=col[tr-1], /nan, $;fcolor=col[0], /fill, $
          bin=binsize, charsize=chars, xchars=1.1, xrange=xspan, yrange=yspan, yminor=2, $
          xmargin=xmarg, ymargin=ymarg
;hmed = median(hdata[i].(0))
hmedarr = hdata[i].(0)
hmed = median(hmedarr[where(hmedarr ge 0)])
vline, hmed, lines=2, color=col[tr-1]                     ; Vertical line at median value
legend, label[i], box=0, textcolor=0, charsize=lchar     ; label for the parameter
mleg = [' ', tname[tr-1]+':  '+textoidl('\mu')+' = '+string(hmed, format='(f6.3)')]
legend, mleg, box=0, textcolor=col[tr-1], charsize=lchar  ; label for the median and scatter

for j=1, ntrans-1 do begin
    plothist, hdata[i].(j), /over, bin=binsize, color=col[trans[j]-1], /nan;, fcolor=col[j], /fill
    ;hmed = median(hdata[i].(j))
    hmedarr = hdata[i].(j)
    hmed = median(hmedarr[where(hmedarr ge 0)])
    vline, hmed, lines=2, color=col[trans[j]-1]                 ; Vertical line at median value
    mleg = [replicate(' ', j+1), tname[trans[j]-1]+':  '+textoidl('\mu')+' = '+string(hmed, format='(f6.3)')]
    legend, mleg, box=0, textcolor=col[trans[j]-1], charsize=lchar  ; label for the median and scatter
endfor

xyouts, .04, .52, 'Number of Disks', color=0, chars=2, align=0.5, orient=90, /normal

if keyword_set(ps) then ps_close
!p.multi = [0, 0, 1, 0, 0]

END

