PRO HISTMATCH_GAS, ISO, TRANS, FLUX, UNCERT, $
                   GAS=gas, DATAFILE=datafile, FILEPATH=filepath, OUTPATH=outpath, PS=ps, $
                   MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, TA1=ta1, QA=qa, $
                   NPD=npd, TFREEZE=tfreeze, INCL=incl, THIN=thin, COLORS=colors, $
                   MEANBAR=meanbar, SILENT=silent, TITLE=title, _EXTRA=extrakey

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.  Makes
;  histograms of the various parameter values for all of the disks matching 
;  given fluxes and uncertainties for specified emission lines.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate only disks with those parameter values.
;
;  If no value is specified for a given parameter, then the histograms will
;  include disks with all values of that parameter (that have the values given
;  for other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-08
;
;  INPUTS
;      ISO - Vector (or scalar) indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Vector (or scalar) indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Vector (or scalar) containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - Vector (or scalar) containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load DATAFILE.
;      DATAFILE - name of the IDL save file containing the "gas".  If none is
;                supplied, the program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/histmatch.eps
;      PS - send output to postscript
;      COLORS - Vector of colors for histograms (using lincolr_wb)
;      MEANBAR - Plot the mean and uncertainty bar for the posterior M_gas
;                distribution.
;      SILENT - Suppress screen output.
;      TITLE - Optional string containing a title to be added to the plot.
;;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

; Number of disk parameters
npar = 12


;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO, TRANS, FLUX, and UNCERT must be equal length
niso = n_elements(iso)
if (niso lt 1) or (n_elements(trans) ne niso) or (n_elements(flux) ne niso) $
  or (n_elements(uncert) ne niso) then begin
    print, 'Use:  histmatch_gas, ISO, TRANS, FLUX, UNCERT, [, gas=gas, filepath=filepath, outpath=outpath,'
    print, '           mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa,'
    print, '           npd=npd, tfreeze=tfreeze, incl=incl, thin=thin, ps=ps]'
    print, '    ISO, TRANS, FLUX, and UNCERT must be scalars or vectors of equal length.'
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif


;;; GET THE DATA
gas = getdata_gas(filepath=filepath, gasname=datafile)


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
; in a vector called 'select'.
select = gas_select_disks(gas, keys, npar+2)

; Choose the disks with the requested parameter values
chosen = gas[select]


;;; DETERMINE THE PARAMETER SPACE
; Dimensions of the disk parameter space
pvals = paramvals_gas(npar, chosen, dims=dims, copy=nmatch)


;;; FIND THE DISKS THAT MATCH THE FLUX(ES) WITHIN THE UNCERTAINTY
for i=0, niso-1 do begin                         ; Loop over the input line fluxes
    lineflux = chosen.(iso[i]+13).(trans[i]-1)[thin-1]
    matched = where((lineflux ge flux[i]-uncert[i]) and (lineflux le flux[i]+uncert[i]))
; intersect matched with previous matched vectors.
    if i eq 0 then begin
        inters = matched
    endif else begin
        inters = setintersection(inters, matched)
    endelse
endfor

matched = inters

; Check that at least one disk matched
if matched[0] eq -1 then begin
    print, 'No disks matched the input fluxes.'
    return
endif


;;; FIND THE TOTAL MATCHES FOR EACH PARAMETER VALUE
for i=0, npar-1 do begin
    for j=0, dims[i]-1 do begin
        nmatch.(i)[j] = n_elements(where(chosen[matched].(i) eq pvals.(i)[j]))
    endfor
endfor

; Total matches for the dissociated and frozen-out fractions.
freeze_1 = where(chosen.freeze.f eq 1)
if freeze_1[0] ge 0 then chosen[freeze_1].freeze.f = .9999
dissoc_1 = where(chosen.dissoc.f eq 1)
if dissoc_1[0] ge 0 then chosen[dissoc_1].dissoc.f = .9999

for i=0, 9 do begin
    matchind = where((chosen[matched].freeze.f ge i/10.) and (chosen[matched].freeze.f lt (i+1)/10.))
    if matchind[0] ge 0 then nmatch.freeze[i] = n_elements(matchind) else nmatch.freeze[i] = 0.
    matchind = where((chosen[matched].dissoc.f ge i/10.) and (chosen[matched].dissoc.f lt (i+1)/10.))
    if matchind[0] ge 0 then nmatch.dissoc[i] = n_elements(matchind) else nmatch.dissoc[i] = 0.
endfor


;;; PLOT HISTOGRAMS
; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = ' ~/Astro/699-2/results/histmatch'
    ps_open, outpath, /color, /en, thick=4
    charsarr = replicate(1.5, 12)
    lchar = 1.2
endif else begin
    window, 2, retain=2, xsize=1200, ysize=700
    charsarr = replicate(2, 12)
    lchar = 1.6
endelse

; Set up for multiple plots in the same window
nohist = n_elements(where(dims eq 1))
!p.multi = [0, floor((npar-nohist)+6)/2, 2, 0, 1]
if n_elements(colors) eq 0 then colors = [4, 3, 2, 0, 12, 13, 11, 6, 15]

; Loop over all parameters, plotting histogram for each one
for i=0, npar+1 do begin
    ; Default values, used for most histograms
    chars = charsarr[npar-1]
    xmarg = [6,-0.5]
    ymarg = [4,2]

    ; Twist and turn to put the histograms in the right places
    if (dims[i] eq 1) or (i eq 1) then continue             ; Don't plot histograms for disk parameters with single values.
    ;; if (i eq 3) or (i eq 9) or (i eq 10) then continue      ; Don't plot histograms for disk parameters with single values.
    if (i eq 2) and (dims[0] ne 1) then $                   ; Skip the Mgas space.
      !p.multi = [(((npar-nohist)+6)/2)*2-2, floor((npar-nohist)+6)/2, 2, 0, 1]
    if i eq 7 then $                                        ; Make sure T_a1 and q_a plots are stacked.
      !p.multi = [8, floor((npar-nohist)+6)/2, 2, 0, 1]
    if i eq 11 then $                                       ; Plot inclination in the original Mgas space.
      !p.multi = [(((npar-nohist)+6)/2)*2-1, floor((npar-nohist)+6)/2, 2, 0, 1]
    if i eq 12 then !p.multi = [6, floor((npar-nohist)+6)/2, 2, 0, 1]

    ; Get the histogram details for the specific disk paramter.
    gas_labels_histo, i, label, xspan, intervals, binsize, logflag
    hdata = nmatch.(i) / total(nmatch.(i))

    ; Get the disk paramter values for the histogram.
    ;; if i le 11 then begin
    ;;     if logflag then begin
    ;;         hdata = alog10(chosen[matched].(i))
    ;;     endif else hdata = chosen[matched].(i)
    ;; endif else hdata = chosen[matched].(i).f

    ;; ; Check for a single-valued histogram, which will crash plothist.
    ;; hdatamin = min(hdata, max=hdatamax)
    ;; if hdatamin eq hdatamax then hdata = [hdata, -1]  ; Add an extraneous -1 to histogram.

    ;; ; Run plothist without plotting in order to get the peak value
    ;; plothist, hdata, xhist, yhist, bin=binsize, xrange=xspan, /noplot
    ;; peak = max(yhist)/float(n_elements(matched))                        ; scale ordinate to fraction of matched disks

    ;; ; Plot the histogram
    ;; plothist, hdata, peak=peak, charsize=chars, backg=1, color=colors[0], fcolor=colors[0], /fill, $
    ;;           bin=binsize, xrange=xspan, xtit=label, xmarg=xmarg, ymarg=ymarg, xtickint=intervals
    gas_plot_hist, pvals.(i), hdata, binsize, colors[0], psym=10, charsize=chars, backg=1, /fill, $
          color=0, xrange=xspan, xstyle=1, xtit=label, xmarg=xmarg, ymarg=ymarg, xtickint=intervals

endfor

;;; Special placement and size for Mgas histogram.
!p.multi = [4, floor((npar-nohist)+6)/2, 2, 0, 1]
if keyword_set(ps) then begin
    chars = 2
    xmarg = [6,-12]
    ymarg = [-39,15]/chars
    tsize = 1.10 - 0.05*niso
endif else begin
    chars = 3
    xmarg = [6,-16]
    ymarg = [-58,23]/chars
    tsize = 2.3 - 0.3*niso
endelse
pvals.(1) = alog10(pvals.(1))   ; Convert masses to log space

; Get the histogram details for the specific disk paramter.
gas_labels_histo, 1, label, xspan, intervals, binsize, logflag
hdata = nmatch.(1) / total(nmatch.(1))
peak = max(hdata, peakind)

; Plot the histogram
gas_plot_hist, pvals.(1), hdata, binsize*6./dims[1], colors[0], psym=10, charsize=chars, backg=1, /fill, $
    color=0, xrange=xspan, xstyle=1, xtit=label, xmarg=xmarg, ymarg=ymarg, xtickint=intervals

;;; Print most likely mass and fraction of matched disks to the screen
if not keyword_set(silent) then begin
    peak = max(hdata, peakind)
    print
    print, ' Most likely Mgas = '+trim(10^(4+pvals.(1)[peakind]))+' x 10^-4 Msun'
    print, 'Fraction of disks = '+trim(100*peak, '(f4.1)')+'%'
    print
    print, 'Number of matched disks:  '+trim(n_elements(matched))
    print
endif

;;; Plot the mean and error bar
if keyword_set(meanbar) then begin
    meangas = mean(alog10(chosen[matched].mg))
    meangasu = stddev(alog10(chosen[matched].mg))
    ; print mean and standard deviation to screen
    if not keyword_set(silent) then begin
        print, 'log(Mgas) = '+trim(meangas, '(f5.2)')+' +/- '+trim(meangasu, '(f4.2)')+' Msun'
        print, '     Mgas = '+trim(10^(4+meangas), '(g3.2)')+' x 10^-4 Msun'
        print
    endif
    if keyword_set(ps) then begin
        esize = 1.2
    endif else begin
        esize = 2
    endelse
    ; plot mean and error bar
    oploterror, meangas, 0.95*peak, meangasu, 0, errthick=4, psym=16, symsize=esize
endif

;;; Print the title and fluxes used to generate the plots.
; Print a title for the slide
titxpos = mean(!x.crange)
titypos = 1.086
titps = !y.crange[1]
if keyword_set(title) then xyouts, titxpos, titps*(titypos+.02*niso), title, color=0, chars=1.6, align=0.5, /data
; Print the line fluxes used to make the histograms
islab = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
trlab = ['1-0', '2-1', '3-2']
label = 'f['+islab[iso-1]+' '+trlab[trans-1]+']'
;; for j=0, niso-1 do xyouts, 0.872, 0.9-0.03*(j-(niso-1)/2.), $
;;         label[j]+' = '+trim(flux[j])+' Jy km/s', color=3, chars=tsize, align=0.5, /normal
for j=0, niso-1 do xyouts, titxpos, titps*(titypos-0.04*(j-(niso-2)/2.)), $
        label[j]+' = '+trim(flux[j])+' Jy km/s', color=colors[0], chars=tsize, align=0.5, /data


; Reset to single plot in a window
!p.multi = [0, 0, 1, 0, 0]

if keyword_set(ps) then ps_close


END
