PRO HIST_GAS_DIS, P, NPAR, CHOSEN, ISO, LABEL, VALS, HSPLIT, $
                  BINSIZE=binsize, PS=ps, OUTPATH=outpath

;+
;  Called by hist_gas.pro
;
;  Makes histograms of the dissociated CO fraction from the make_gas simulations.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-04-09
;  2013-08-14 (WB): Allow and plot histograms with unequal numbers of 
;                   elements. Finally enabled plotting for data partitioned by
;                   isotopologue (PARAM=13) and transition (PARAM=14), which 
;                   actually just plots a single histogram for all disks, since
;                   the dissociated fraction doesn't have anything to do
;                   with the isotopologues or transitions.
;  2013-08-20 (WB): Minor efficiency tweaks
;
;  INPUTS
;
;  KEYWORDS
;      BINSIZE - size of bins for histograms.
;                Default: 20 bins spanning (max - min)
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/gashist.eps
;      PS - send output to postscript
;-

; Break the subset up by values of the chosen parameter (if one has been set as PARAM)
case p of
    npar-1 : begin           ; If PARAM is set to the CO transitions
        nvals = 1
        hdata = {f:chosen.dissoc.f}
        label = 'All disks'
    end
    npar-2 : begin           ; If PARAM is set to the CO isotopologues
        nvals = 1
        hdata = {f:chosen.dissoc.f}
        label = 'All disks'
    end
    else : begin             ; If PARAM is set to anything else
        nvals = n_elements(vals)
        hdata = replicate({f:fltarr(hsplit)-1}, nvals)   ; start with fraction=-1 for each disk
        for i=0, nvals-1 do begin
            index = where(chosen.(p) eq vals[i])
            hdata[i].f = chosen[index].dissoc.f
        endfor
    end
endcase

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/gas/gas_dis1'
    ps_open, outpath, /color, /en, thick=4
    lchar = 0.9
endif else begin
    window, 0, retain=2, xsize=800, ysize=800
    lchar = 1.6
endelse
!p.multi = [0, 1, nvals, 0, 0]
xmarg = [9,3]
yarr = [6, 3.8, 1.8, 1.2, 1, 0.8]
ystretch = yarr[nvals-1]
charsarr = [1.4, 1.4, 2.2, 2.2, 2.2, 2.2]
chars = charsarr[nvals-1]
col = [3, 13, 4]

; Min and max for histograms
xspan = [0,1]
if n_elements(binsize) eq 0 then binsize = (xspan[1] - xspan[0])/20.

; Make the plots
for i=0, nvals-2 do begin
    fix1 = where(hdata[i].f ge 1.)
    if fix1[0] ge 0 then hdata[i].f[fix1] = .9999

    ymarg = [ystretch*i, ystretch*(1-i)]
    yspan = [0., max(histogram(hdata[i].f, binsize=binsize, min=xspan[0], max=xspan[1]))]

    plothist, hdata[i].f, charsize=chars, backg=1, bin=binsize, color=col[0], fcolor=col[0], /fill, $
              xrange=xspan, xtickname=replicate(' ',8), ytickname=' ', yminor=2, xmargin=xmarg, ymargin=ymarg
    ;hmed = median(hdata[i].f)
    hmedarr = hdata[i].f
    hmed = median(hmedarr[where(hmedarr ge 0)])
    vline, hmed, lines=2, color=col[0]        ; Vertical line at median value
    legend, label[i], box=0, textcolor=0, charsize=lchar, /right     ; label for the parameter
    mleg = [' ', 'Dissociated CO Fraction:  '+textoidl('\mu')+' = '+string(hmed, format='(f5.2)')]
    legend, mleg, box=0, textcolor=col[0], charsize=lchar, /right     ; label for the median and scatter
endfor

if nvals eq 1 then ymarg = [4,2] else ymarg = [ystretch*(nvals-1),ystretch*(2-nvals)]
i = nvals-1
fix1 = where(hdata[i].f ge 1.)
if fix1[0] ge 0 then hdata[i].f[fix1] = .9999

yspan = [0., max(histogram(hdata[i].f, binsize=binsize, min=xspan[0], max=xspan[1]))]

plothist, hdata[i].f, xtitle='Dissociated CO Fraction', backg=1, color=col[0], fcolor=col[0], /fill, $
          bin=binsize, charsize=chars, xchars=1.1, xrange=xspan, yminor=2, xmargin=xmarg, ymargin=ymarg
;hmed = median(hdata[i].f)
hmedarr = hdata[i].f
hmed = median(hmedarr[where(hmedarr ge 0)])
vline, hmed, lines=2, color=col[0]                          ; Vertical line at median value
legend, label[i], box=0, textcolor=0, charsize=lchar, /right     ; label for the parameter
mleg = [' ', 'Dissociated CO Fraction:  '+textoidl('\mu')+' = '+string(hmed, format='(f5.2)')]
legend, mleg, box=0, textcolor=col[0], charsize=lchar, /right         ; label for the median and scatter

xyouts, .04, .52, 'Number of Disks', color=0, chars=2, align=0.5, orient=90, /normal

if keyword_set(ps) then ps_close
!p.multi = [0, 0, 1, 0, 0]

END

