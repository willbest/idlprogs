@readradmc.pro 
PRO CALC_THIN_FRACTION, STAR=star, DISK=disk, TEMP=temp, CHEM=chem, $
                        SPECIES=species, J_HIGH=j_high, INCL=incl, $
                        NORADMC=noradmc, FILENAME=filename, PDF=pdf, PS=ps

;+
; Create images of spectral lines, plot integrated spectra,
; and calculate what fraction of the emission is optically thin
;
;  HISTORY
;  Written 03/20/13 (jpw)
;  03/24/13 (WB): Modified to run in loops
;  06/17/13 (WB): Changed the postcript plotting -- thicker lines
;                 Added code to save the spectra to a text file
;
;  KEYWORDS
;      STAR - Mass of the central star
;      DISK - Structure containing disk parameters:
;             m:mass, gamma:gamma, rin:R_in, rc:R_c
;      TEMP - Structure containing disk temperature parameters
;             tm1:T_m1, qm:q_m, ta1:T_a1, qa:q_a
;      CHEM - Structure containing CO chemical parameters:
;             npd:N_photodissoc, tf:T_freeze
;      SPECIES - Isotopologue of CO to use for line calculations
;                Default: 'c18o'
;      J_HIGH - Upper level of transition of CO to use for line calculations
;               Default: 3  (i.e., J=3-2 transition)
;      INCL - Disk inclination to use for line calculations, in degrees
;             Default: 45.0
;      NORADMC - Do not call radmc routines -- just plot.
;      FILENAME - file path+name for postcript output
;      PDF - If set, create pdf output
;      PS - If set, create postscript output
;-

@natconst.pro

if not keyword_set(species) then species = 'c18o'
if not keyword_set(J_high) then J_high = 3
if n_elements(incl) eq 0 then incl = 45.0
if keyword_set(star) then begin
    vrange = 7. + 4.*star.m
endif else vrange  = 10.0
dv      = 0.1;0.25

; --------------------------------------------------------
if species eq 'co' then begin
  is = 1
  case J_high of
    1: lambda0 = 2600.7363d
    2: lambda0 = 1300.4036d
    3: lambda0 =  866.95627d
  endcase
endif
if species eq '13co' then begin
  is = 2
  case J_high of
    1: lambda0 = 2720.3840d
    2: lambda0 = 1360.2280d
    3: lambda0 =  906.83886d
  endcase
endif
if species eq 'c18o' then begin
  is = 3
  case J_high of
    1: lambda0 = 2730.7712d
    2: lambda0 = 1365.4216d
    3: lambda0 =  910.30121d
  endcase
endif
nv = nint(vrange/dv)


;;; create line intensity image
;
cmd_image = string(format='("radmc3d image imolspec ",i1," iline ",i1," widthkms ",f4.1," linenlam ",i0," incl ",f5.1," posang 0")',is,J_high,vrange,nv,incl)
imfile = string(format='("image_",a0,i0,i0,"_i",i0,".out")',species,J_high,J_high-1,incl)
if not keyword_set(noradmc) then begin
  print
  print,cmd_image
  spawn,cmd_image
  file_move,'image.out',imfile,/overwrite
endif
line_intensity = readimage(file=imfile)

;;; create tau=1 image
;
cmd_image = string(format='("radmc3d tausurf 1 imolspec ",i1," iline ",i1," widthkms ",f4.1," linenlam ",i0," incl ",f5.1," posang 0")',is,J_high,vrange,nv,incl)
taufile = string(format='("tau_",a0,i0,i0,"_i",i0,".out")',species,J_high,J_high-1,incl)
if not keyword_set(noradmc) then begin
  print
  print,cmd_image
  spawn,cmd_image
  file_move,'image.out',taufile,/overwrite
endif
tau = readimage(file=taufile)

;;; calculate spectra
;;; Jy as observed at 140pc versus velocity in km/s
vel = cc*(line_intensity.lambda-lambda0)/lambda0
vel = vel / 1d5
dpc = 140.0d
;dpc = 122d; Rosenfeld
omega = line_intensity.sizepix_x*line_intensity.sizepix_y/pc^2
scale = 1d23*omega/dpc^2

flux_tot = total(total(line_intensity.image,1,/double),1,/double)

;;; crudely remove dust continuum
;;; (assuming that velocity scale extends far enough from line)
continuum = flux_tot[0]
;flux_tot = flux_tot - continuum

ind_thin = where(tau.image eq -1d91,nthin,complement=ind_thick,ncomplement=nthick)
if (nthin gt 0) then begin
  line_intensity_thick = line_intensity.image
  line_intensity_thick[ind_thin] = 0.0d
  flux_thick = total(total(line_intensity_thick,1,/double),1,/double)
endif else begin
  print,'>>> Emission is completely optically thick'
  flux_thick = flux_tot
endelse
if (nthick gt 0) then begin
  line_intensity_thin = line_intensity.image
  line_intensity_thin[ind_thick] = 0.0d
  flux_thin = total(total(line_intensity_thin,1,/double),1,/double)
endif else begin
  print,'>>> Emission is completely optically thin'
  flux_thin = flux_tot
endelse


;;; scale to Jy at dpc
;
flux_tot = scale * flux_tot
flux_thick = scale * flux_thick
flux_thin = scale * flux_thin


;;; set up plot page
;
asp=8.5/11.0
if keyword_set(ps) or keyword_set(pdf) then begin
    if n_elements(filename) eq 0 then filename='lineprofile.ps'
    set_plot,'ps',/interpolate
    device,filename=filename,bits_per_pixel=8 $
           ,xsize=11.0,ysize=11.0*asp,xoff=0.0,yoff=11.0 $
           ,/inches,/land,/encapsulated,/color
    cs=1.7
    ct=4
endif else begin
    device,decomposed=0
    xsize=1000
    window,0,xsize=xsize,ysize=xsize*asp
    cs=1.8
    ct=1
endelse
pos = [0.17,0.15,0.9,0.92]
tic = 0.04

;;; plot line profile
;
ncol=250
loadct,0,ncolors=ncol
tvlct,fsc_color('Red',/triple),251
tvlct,fsc_color('Dark Grey',/triple),252
tvlct,fsc_color('Green',/triple),253
tvlct,fsc_color('Royal Blue',/triple),254
tvlct,fsc_color('White',/triple),255
red=251
grey=252
green=253
blue=254
white=255
textcol = [white,0,0]
textflag = (keyword_set(ps)+keyword_set(pdf)) < 1

xmin = min(vel)
xmax = max(vel)
dx = 0.05*(xmax-xmin)
xmin = xmin-dx
xmax = xmax+dx
ymin = min(flux_tot)
ymax = max(flux_tot)
dy = 0.1*(ymax-ymin)
ymax = nint(10*(ymax+dy))/10.0 > 0.05
ymin = (nint(20*(ymin-dy))/20.0) < (-0.05)

plot,vel,vel $
    ,position=pos $
    ,xra=[xmin,xmax],xsty=1 $
    ,yra=[ymin,ymax],ysty=1 $
    ,xtitle='v (km/s)' $
    ,ytitle='Flux (Jy)' $
    ,xthick=2+3*textflag,ythick=2+3*textflag,thick=3+3*textflag $
    ,charthick=2+3*textflag,charsize=1.8 $
    ,/nodata
oplot,vel,flux_tot,psym=10,thick=3+3*textflag,color=textcol[textflag];white
oplot,vel,flux_thick,psym=10,thick=2+3*textflag,color=red
oplot,vel,flux_thin,psym=10,thick=2+3*textflag,color=blue
xyouts,0.92*xmin+0.08*xmax,0.08*ymin+0.92*ymax,string(format='(a0,x,i1,"-",i1)',strupcase(species),J_high,J_high-1),charsize=2.0,charthick=2+3*textflag,align=0
xyouts,0.92*xmin+0.08*xmax,0.12*ymin+0.88*ymax,string(format='("i = ",i0,"!9%!X")',incl),charsize=2.0,charthick=2+3*textflag,align=0

if keyword_set(ps) or keyword_set(pdf) then begin
  device,/close
  set_plot,'x'
  !p.font=-1
  print,"Created color postscript file: "+filename
endif

if keyword_set(pdf) then begin
    if keyword_set(ps) then cgps2pdf, filename, /showcmd else cgps2pdf, filename, /showcmd, /delete_ps
endif


intflux_tot   = int_tabulated(vel,flux_tot)
intflux_thick = int_tabulated(vel,flux_thick)
intflux_thin  = int_tabulated(vel,flux_thin)
percent_thick = 100*intflux_thick/intflux_tot

; print integrated line intensity results to screen
print
print,'--------------------------------'
print,format='(a0,x,i1,"-",i1," at ",i0," deg")',strupcase(species),J_high,J_high-1,incl
print,'--------------------------------'
print,'Integrated intensities (Jy km/s)'
print,format='("Thick =", f7.3," (",i0,"%)")',intflux_thick,percent_thick
print,format='("Thin  =", f7.3," (",i0,"%)")',intflux_thin,100-percent_thick
print,format='("Total =", f7.3)',intflux_tot
print,'--------------------------------'
print

; output integrated line intensity results to file
get_lun, lun
resfile = string(format='("line_results_",a0,i0,i0,"_i",i0,".txt")',species,J_high,J_high-1,incl)
openw, lun, resfile
if n_elements(star) ne 0 then $
  printf, lun, '% Star:  '+string(format='("M = ",f5.2," Msun")', star.m)
if n_elements(disk) ne 0 then $
  printf, lun, '% Disk:  '+string(format='("Mgas = ",e8.2," Msun,  gamma = ",f4.2,",  ")', disk.m, disk.gamma)+$
          string(format='("R_in = ",f5.2," AU,  R_c = ",f5.1," AU")', disk.rin, disk.rc)
if n_elements(temp) ne 0 then $
  printf, lun, '% Temperature:  '+string(format='("T_m1 = ",f6.1," K,  q_m = ",f4.2,",  ")', temp.tm1, temp.qm)+$
          string(format='("T_a1 = ",f6.1," K,  q_a = ",f4.2)', temp.ta1, temp.qa)
if n_elements(chem) ne 0 then $
  printf, lun, '% Chemistry:  '+$
          string(format='("N_pd = ",e8.2," cm^-2,  T_fr = ",f4.1," K")', chem.npd*1d21, chem.tf)
printf, lun, '%--------------------------------'
printf, lun, format='("% Transition:  ",a0,x,i1,"-",i1,"  at  ",i0," deg")',strupcase(species),J_high,J_high-1,incl
printf, lun, '%--------------------------------'
printf, lun, '%'
printf, lun, '%   Thick     Thin       Total'
printf, lun, '%--------------------------------'
printf, lun, format='(3(3x,f7.3))', intflux_thick, intflux_thin, intflux_tot
printf, lun, '% -------------------------------'
free_lun, lun

; output spectra to file
specfile = string(format='("line_spectra_",a0,i0,i0,"_i",i0,".txt")',species,J_high,J_high-1,incl)

speccomment = ['%--------------------------------', $
  string(format='("% Transition:  ",a0,x,i1,"-",i1,"  at  ",i0," deg")',strupcase(species),J_high,J_high-1,incl), $
  '%--------------------------------', $
  '%', $
  '%            Velocity (km/s)                    Flux_tot (Jy)                       Flux_thick (Jy)                    Flux_thin (Jy)']
if n_elements(chem) ne 0 then speccomment = ['% Chemistry:  '+$
  string(format='("N_pd = ",e8.2," cm^-2,  T_fr = ",f4.1," K")', chem.npd*1d21, chem.tf), speccomment]
if n_elements(temp) ne 0 then speccomment = ['% Temperature:  '+$
  string(format='("T_m1 = ",f6.1," K,  q_m = ",f4.2,",  ")', temp.tm1, temp.qm)+$
  string(format='("T_a1 = ",f6.1," K,  q_a = ",f4.2)', temp.ta1, temp.qa), speccomment]
if n_elements(disk) ne 0 then speccomment = ['% Disk:  '+$
  string(format='("Mgas = ",e8.2," Msun,  gamma = ",f4.2,",  ")', disk.m, disk.gamma)+$
  string(format='("R_in = ",f5.2," AU,  R_c = ",f5.1," AU")', disk.rin, disk.rc), speccomment]
if n_elements(star) ne 0 then speccomment = ['% Star:  '+$
  string(format='("M = ",f5.2," Msun")', star.m), speccomment]

forprint, vel, flux_tot, flux_thick, flux_thin, textout=specfile, format='f,f,f,f', $
          comment=speccomment

END
