FUNCTION GAS_SELECT_DISKS, GAS, KEYS, NPAR

;+
;  Returns a vector of indices for disks with the chosen parameter values.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-09-11
;
;  INPUTS
;      GAS - structure containing the model data.
;      KEYS - structure containing the parameter keywords.
;      NPAR - Total number of disk parameters.
;-

;;; Build a large string containing the command to select the disks with the
;;;     desired parameters, to call with the EXECUTE function

; Beginning of the selecting string
fstr = 'select = where('

; Loop over the parameters in the 'keys' structure to select disks with the chosen parameters
for i=0, (npar-3) do begin
    if (keys.(i)[0] ge 0) then begin    ; if the keyword has value(s), go on, else go to next keyword

        ; find which parameter values in the model data match the keyword values
        match, gas[uniq(gas.(i), sort(gas.(i)))].(i), keys.(i), xx, ind
        nind = n_elements(ind)          ; number of disks with parameter matching the keyword value

        ; if parameters matches keyword value at least once, identify those disks, else go to next keyword
        if ind[0] ge 0 then begin
            ; if there's more than one matching keyword value, add all of them to the selecting string
            if nind gt 1 then fstr = fstr + $  
                   '(' + strjoin('(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
            ; add the last matching keyword value to the selecting command
            fstr = fstr + '(gas.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
            if nind gt 1 then fstr = fstr + ')'

            fstr = fstr + ' and '       ; prepare for the next keyword
        endif
    endif
endfor


; If at least one keyword has been set to specific values, run the EXECUTE
;     command on the selecting string to identify to chosen disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'    ; replace the last ' and ' with ')'
    dummy = execute(fstr)     ; use EXECUTE to run the selecting command
; If no keywords were set to specific values, select all disks.
endif else select = lindgen(n_elements(gas))

return, select

END

