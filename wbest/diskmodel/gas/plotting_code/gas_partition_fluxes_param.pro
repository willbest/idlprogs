PRO GAS_PARTITION_FLUXES_PARAM, P, CHOSEN, VALS, ISO, TRANS, THIN, NVALS, NISO, NTRANS, NDISKS, $
                                DATA, MED, NLINES, YARR

;+
;  Called by scatter_gas_flux.pro
;
;  Partitions a data structure according to PARAM.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-09-12
;
;  INPUTS
;
;
;  OUTPUTS
;      DATA - Array of flux data to be plotted.
;      MED - Array containing medians for each line and partition of the data.
;      NLINES - Number of lines to be plotted.
;      YARR - Array of parameter data to plotted vs. DATA.
;-

yarr = chosen.(p)                                         ; Vector with values of PARAM
nlines = niso*ntrans                                      ; Number of emission lines being plotted
data = fltarr(ndisks, nlines)                             ; Create array for the flux(es) from each disk
med = fltarr(nvals, nlines)                               ; Create array for median flux(es) from each bin of disks
for j=0, ntrans-1 do begin                                ; Loop over chosen transitions
    for k=0, niso-1 do begin                              ; Loop over chosen isotopologues
        flux = chosen.(iso[k]+13).(trans[j]-1)[thin-1]    ; Get flux values for a specific emission line
        data[*,j*niso+k] = flux                           ; Store these fluxes in the data array
        for i=0, nvals-1 do begin                         ; Loop over chosen parameter values
            med[i,j*niso+k] = median(flux[where(yarr eq vals[i])]) ; Get median flux for a bin of disks
        endfor
    endfor
endfor

END

