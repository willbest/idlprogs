FUNCTION GAS_LABELS_SPECIFIED_VALUES, P, VALSET, KEYS, PARLABEL

;+
;  Returns disk parameter label(s) for plot legends, with specified values for 
;  specified parameters.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-09-12
;
;  INPUT
;      P - The index of the tag containing the parameter to be plotted.
;      VALSET - Vector with indices of the tags containing the other parameters
;               for which values should be included in labels.
;      KEYS - Structure containing the specified values for parameters.
;
;  OUTPUTS
;      PARLABEL - Vector containing the disk parameter labels.
;-

;;; Is the parameter to be plotted (p) in valset?  If so, remove it.
pind = where(valset eq p)
if pind[0] ne -1 then begin
    ; If p is the only value in valset, then set valset to -1 (same as if no parameters had specified values).
    if n_elements(valset) eq 1 then valset = [-1] $
    ; Otherwise, remove the index for p from valset
    else remove, pind, valset
endif

;;; Get a label for each parameter with specified values.
nvs = n_elements(valset)
parlabel = strarr(nvs)      ; create vector that will contain the labels
for i=0, nvs-1 do begin

    ; Get the label values from the 'keys' structure
    case valset[i] of
        0 : begin
            templabel = 'M!Dstar!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+' M!D!9n!X!N'
        end
        1 : begin
            templabel = 'M!Dgas!N = ' + strjoin(string(keys.(valset[i]), format='(e6.0,", ")'))
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+' M!D!9n!X!N'
        end
        2 : begin
            templabel = textoidl('\gamma')+' = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)
        end
        3 : begin
            templabel = 'R!Din!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+' AU'
        end
        4 : begin
            templabel = 'R!Dc!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+' AU'
        end
        5 : begin
            templabel = 'T!Dmid1!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+' K'
        end
        6 : begin
            templabel = 'q!Dmid!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)
        end
        7 : begin
            templabel = 'T!Datm1!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+' K'
        end
        8 : begin
            templabel = 'q!Datm!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)
        end
        9 : begin
            templabel = 'N!Dpd!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+'!9X!X10!U21!N cm!U-2!N'
        end
        10 : begin
            templabel = 'T!Dfreeze!N = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2)+' K'
        end
        11 : begin
            templabel = 'i = ' + strjoin(trim(keys.(valset[i]))+', ')
            parlabel[i] = strmid(templabel, 0, strlen(templabel)-2) + ' deg'
        end
        else : parlabel = ' '       ; If no parameters have specified values, return a blank label
    endcase

endfor

return, parlabel

END

