FUNCTION GAS_PLOTSCALE_ADJUST, VALS, LOGAXIS, SPAN=span

;+
;  Called by scatter_gas_flux.pro
;
;  Determines an 'adjust' factor, which sets the axis range and plotting
;  width of each of the partitions of a set of data. 
;
;  HISTORY
;  Written by Will Best (IfA), 2013-09-13
;
;  INPUTS
;      LOGAXIS - Make the y-axis (parameter) logarithmic.
;      VALS - Parameter values
;
;  KEYWORD OUTPUTS
;      SPAN - Axis range
;-

if logaxis eq 0 then begin                ; If the paramater axis is not logarithmic
    if n_elements(vals) gt 1 then adjust = (vals[1]-vals[0])/10. $
      else adjust = vals/10.
    span = [min(vals)-(5*adjust), max(vals)+(5*adjust)]
endif else begin                          ; If the paramater axis is  logarithmic
    if n_elements(vals) gt 1 then adjust = (float(vals[1])/vals[0])^(.25) $
      else adjust = vals^(.25)
    span = [min(vals)/(1.2*adjust), max(vals)*(1.2*adjust)]
endelse
if n_elements(vals) eq 1 then adjust = adjust / 2.

return, adjust

END

