PRO GAS_LABELS_SPECIFIED_VALUES, P, VALSET, KEYS, PARLABEL

;+
;  Returns disk parameter label(s) for plot legends, with specified values for 
;  specified parameters.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-09-12
;
;  INPUT
;      P - The index of the tag containing the parameter to be plotted.
;      VALSET - Vector with indices of the tags containing the other parameters
;               for which values should be included in labels.
;      KEYS - Structure containing the specified values for parameters.
;
;  OUTPUTS
;      PARLABEL - Vector containing the disk parameter labels.
;-

;;; Is the parameter to be plotted (p) in valset?  If so, remove it.
pind = where(valset eq p)
if pind[0] ne -1 then begin
    ; If p is the only value in valset, then set valset to -1 (same as if no parameters had specified values).
    if n_elements(valset) eq 1 then valset = [-1] $
    ; Otherwise, remove the index for p from valset
    else remove, pind, valset
endif

;;; Get a label for each parameter with specified values.
nvs = n_elements(valset)
parlabel = strarr(nvs)      ; create vector that will contain the labels
for i=0, nvs-1 do begin

    ; Get the label values from the 'keys' structure
    case valset[i] of
        0 : parlabel[i] = ['M!Dstar!N = '+trim(keys.(valset[i]))+' M!D!9n!X!N']
        1 : parlabel[i] = ['M!Dgas!N = '+string(keys.(valset[i]), format='(e6.0)')+' M!D!9n!X!N']
        2 : parlabel[i] = [textoidl('\gamma')+' = '+trim(keys.(valset[i]))]
        3 : parlabel[i] = ['R!Din!N = '+trim(keys.(valset[i]))+' AU']
        4 : parlabel[i] = ['R!Dc!N = '+trim(keys.(valset[i]))+' AU']
        5 : parlabel[i] = ['T!Dmid1!N = '+trim(keys.(valset[i]))+' K']
        6 : parlabel[i] = ['q!Dmid!N = '+trim(keys.(valset[i]))]
        7 : parlabel[i] = ['T!Datm1!N = '+trim(keys.(valset[i]))+' K']
        8 : parlabel[i] = ['q!Datm!N = '+trim(keys.(valset[i]))]
        9 : parlabel[i] = ['N!Dpd!N = '+trim(keys.(valset[i]))+'!9X!X10!U21!N cm!U-2!N']
        10 : parlabel[i] = ['T!Dfreeze!N = '+trim(keys.(valset[i]))+' K']
        11 : parlabel[i] = ['i = '+trim(keys.(valset[i]))+textoidl('^\circ'))
        else : parlabel = ' '       ; If no parameters have specified values, return a blank label
    endcase

endfor

END

