PRO GAS_PLOT_DIFF_SCAT, DIFFARR, FLUX, MODELVALS, LABEL=label, LCHAR=lchar, $
                        OUTPATH=outpath, PS=ps, ZOOM=zoom, _EXTRA=extrakey

;+
;  Called by bayesselfplots_gas.pro.
;
;  Makes scatter plots of abs(log Mgas – <log Mgas>) vs. flux. 
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-30
;
;  INPUTS
;      DIFFARR - Array of differences between the model and expected (from
;                Bayesian inference) parameter values.
;      FLUX - One-tag structure containing the array of fluxes.
;      MODELVALS - Model parameter values.
;
;  KEYWORDS
;      LABEL - Title for plot -- which emission line has been fit.
;      LCHAR - Character size for the legend.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/bayesself.eps
;      PS - send output to postscript
;      ZOOM - Make a second scatter plot, with xrange=[0, zoom].
;      Passes all plotting other keywords via _EXTRA.
;-

;;; SCATTER PLOT #1
; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/biggas/bayesself'
    ps_open, outpath+'.scat', /color, /en, thick=4
    if n_elements(lchar) eq 0 then lchar = 1.2
    chars = 1.5
endif else begin
    window, 1, retain=2, xsize=800, ysize=600
    if n_elements(lchar) eq 0 then lchar = 1.6
    chars = 1.8
endelse

; Set up plotting variables
xspan = [0, max(flux.(0))]
yspan = [0, max(diffarr)]
nflux = n_elements(diffarr)

; Plot the blank axes
plot, [0], /nodata, charsize=chars, xmarg=[8,8], $
      xrange=xspan, yrange=yspan, _extra=extrakey

; Overplot the differences between model and expected mass, colored by model mass
cubehelix, rots=-1.2
for i=0L, nflux-1 do plots, flux.(0)[i], diffarr[i], psym=cgsymcat(17), symsize=0.4, color=80*modelvals[i]+320

; Plot the colorbar
cgcolorbar, ncolors=200, /vert, /right, tickn=trim([-4, -3.5, -3, -2.5, -2, -1.5]), $    ; make the colorbar on the right
            chars=1, pos=[0.93, 0.045, 0.95, 0.915], tickint=40
xyouts, 0.94, 0.94, 'log M!Dgas!N', color=0, align=0.5, charsize=0.8*chars, /normal

if keyword_set(ps) then ps_close


;;; SCATTER PLOT #2
if keyword_set(zoom) then begin

    ; Set up for plotting
    device, decomposed=0
    lincolr_wb, /silent
    if keyword_set(ps) then begin
        outpath = strmid(outpath, 0, strlen(outpath)-5) + '.zoom'
        ps_open, outpath+'.scat', /color, /en, thick=4
        if n_elements(lchar) eq 0 then lchar = 1.2
        chars = 1.5
    endif else begin
        window, 2, retain=2, xsize=800, ysize=600
        if n_elements(lchar) eq 0 then lchar = 1.6
        chars = 1.8
    endelse

    ; Set up plotting variables
    zoomind = where(flux.(0) le zoom)
    zflux = flux.(0)[zoomind]
    zdiff = diffarr[zoomind]
    zmodel = modelvals[zoomind]

    xspan = [0, zoom]
    yspan = [0, max(zdiff)]
    nzoom = n_elements(zdiff)

    ; Plot the blank axes
    plot, [0], /nodata, charsize=chars, xmarg=[8,8], $
          xrange=xspan, yrange=yspan, _extra=extrakey

    ; Overplot the differences between model and expected mass, colored by model mass
    cubehelix, rots=-1.2
    for i=0L, nzoom-1 do plots, zflux[i], zdiff[i], $
               psym=cgsymcat(17), symsize=0.4, color=80*zmodel[i]+320

    ; Plot the colorbar
    cgcolorbar, ncolors=200, /vert, /right, tickn=trim([-4, -3.5, -3, -2.5, -2, -1.5]), $ ; make the colorbar on the right
                chars=1, pos=[0.93, 0.045, 0.95, 0.915], tickint=40
    xyouts, 0.94, 0.94, 'log M!Dgas!N', color=0, align=0.5, charsize=0.8*chars, /normal

    if keyword_set(ps) then ps_close

endif


END

