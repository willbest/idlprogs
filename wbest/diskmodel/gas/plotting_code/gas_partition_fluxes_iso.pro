PRO GAS_PARTITION_FLUXES_PARAM, CHOSEN, VALS, ISO, TRANS, THIN, NVALS, NISO, NTRANS, NDISKS, $
                                DATA, MED, NLINES, YARR

;+
;  Called by scatter_gas_flux.pro
;
;  Partitions a data structure according to isotoplogues.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-09-12
;
;  INPUTS
;
;
;  OUTPUTS
;      DATA - Array of flux data to be plotted.
;      MED - Array containing medians for each line and partition of the data.
;      NLINES - Number of lines to be plotted.
;      YARR - Array of isotopologue indices to plotted vs. DATA.
;-

yarr = reform(transpose(cmreplicate(iso, ndisks)), niso*ndisks)   ; yarr contains consecutive vectors,
                  ; each the length of chosen, with numbers corresponding to the chosen isotopologues
nlines = ntrans                                           ; Number of transitions being plotted
data = fltarr(ndisks*niso, nlines)                        ; Create array for the flux(es) from each disk
med = fltarr(nvals, nlines)                               ; Create array for median flux(es) from each bin of disks
for j=0, ntrans-1 do begin                                ; Loop over chosen transitions
    flux = chosen.(iso[0]+13).(trans[j]-1)[thin-1]        ; Get flux values for first chosen isotopologue
    for k=1, niso-1 do flux = [flux, chosen.(iso[k]+13).(trans[j]-1)[thin-1]]             
                                                          ; Get flux values for other chosen isotopologue(s)
    data[*,j] = flux                                      ; Store these fluxes in the sdata array
    for i=0, nvals-1 do begin                             ; Loop over chosen isotopologues
        med[i,j] = median(flux[where(yarr eq vals[i])])   ; Get median flux for a bin of disks
    endfor
endfor

END

