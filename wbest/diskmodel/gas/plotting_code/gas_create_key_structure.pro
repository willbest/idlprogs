FUNCTION GAS_CREATE_KEY_STRUCTURE, MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, $
             TM1=tm1, QM=qm, TA1=ta1, QA=qa, NPD=npd, TFREEZE=tfreeze, INCL=incl, ISO=iso, $
             TRANS=trans, THIN=thin, VALSET=valset

;+
;  Returns a structure of parameter keywords called 'keys' used by the programs
;  that analyze the gas disk simulations.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-09-11
;
;  KEYWORDS
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the scatter plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the scatter plots.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      ISO - Isotopologues of CO
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - CO transitions
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;
;    VALSET - vector containing the indices of the first twelve tags (i.e. disk
;             parameters) that have been set to specific values.
;-

; For all keywords without supplied values, set the default.
; -1 indicates that all possible values for the models will be used.
if not keyword_set(mstar) then mstar = -1
if not keyword_set(mgas) then mgas = -1
if n_elements(gamma) eq 0 then gamma = -1
if not keyword_set(rin) then rin = -1
if not keyword_set(rc) then rc = -1
if not keyword_set(tm1) then tm1 = -1
if not keyword_set(qm) then qm = -1
if not keyword_set(ta1) then ta1 = -1
if not keyword_set(qa) then qa = -1
if not keyword_set(npd) then npd = -1
if not keyword_set(tfreeze) then tfreeze = -1
if n_elements(incl) eq 0 then incl = -1
if not keyword_set(iso) then iso = [1, 2, 3]
if not keyword_set(trans) then trans = [1, 2, 3]
if not keyword_set(thin) then thin = 3

valset = where([mstar[0], mgas[0], gamma[0], rin[0], rc[0], tm1[0], qm[0], ta1[0], qa[0], $
                npd[0], tfreeze[0], incl[0] ] ne -1)

keys = {mstar:mstar, mgas:mgas, gamma:gamma, rin:rin, rc:rc, tm1:tm1, qm:qm, ta1:ta1, qa:qa, $
        npd:npd, tfreeze:tfreeze, incl:incl}

return, keys

END

