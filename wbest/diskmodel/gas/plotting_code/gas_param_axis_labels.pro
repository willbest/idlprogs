PRO GAS_PARAM_AXIS_LABELS, VALS, NVALS, RANGE, TICKN, TICKS, TICKV, MINOR

;+
;  Determine labels, ticks, and intervals for the parameter axis.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-09-13
;
;  INPUTS
;      RANGE - Two-element vector for the axis range.
;      NVALS - Number of parameter values
;      VALS - Parameter values
;
;  OUTPUTS
;      MINOR - Number of minor tick intervals
;      TICKN - Names (labels) for the major tick marks
;      TICKS - Number of major tick intervals
;      TICKV - Values for the major ticks
;-

minor = 2

if nvals ge 2 then begin                           ; for at least two bins
    tickv = vals                                       ; just use the bin values as the tick values
    ticks = nvals - 1                                  ; nvals-1 major tick *intervals*
    tickn = trim(vals)                                 ; and use bin values as labels
endif else begin                                   ; for only one bin
    tickv = [range[0], vals, range[1]]                 ; pad with an extra tick above and below
    ticks = 2                                          ; two major tick *intervals*
    tickn = [' ', trim(vals), ' ']                     ; no labels for the extra ticks
endelse

END

