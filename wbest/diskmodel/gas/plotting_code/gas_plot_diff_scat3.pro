PRO GAS_PLOT_DIFF_SCAT3, DIFFARR, FLUX, MODELVALS, LABEL=label, LCHAR=lchar, $
                         OUTPATH=outpath, PS=ps, ZOOM=zoom, _EXTRA=extrakey

;+
;  Called by bayesselfplots3_gas.pro.
;
;  Makes scatter plots of abs(log Mgas – <log Mgas>), plotted on flux2 vs. flux1
;  vs. flux0 axes.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-30
;
;  INPUTS
;      DIFFARR - Array of differences between the model and expected (from
;                Bayesian inference) parameter values.
;      FLUX - Two-tag structure containing the vectors of fluxes.
;      MODELVALS - Model parameter values.
;
;  KEYWORDS
;      LABEL - Title for plot -- which emission lines have been fit.
;      LCHAR - Character size for the legend.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/bayesself.eps
;      PS - send output to postscript
;      ZOOM - Make a second scatter plot, with xrange=[0, zoom[0]] 
;             yrange=[0, zoom[1]], and zrange=[0, zoom[2]].
;      Passes all plotting other keywords via _EXTRA.
;-

;;; SCATTER PLOT #1
; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/biggas/bayesself3'
    ps_open, outpath+'.scat', /color, /en, thick=4
    if n_elements(lchar) eq 0 then lchar = 1.2
    chars = 1.5
endif else begin
    window, 1, retain=2, xsize=800, ysize=600
    if n_elements(lchar) eq 0 then lchar = 1.6
    chars = 1.8
endelse

; Set up plotting variables
xspan = [0, max(flux.(0))]
yspan = [0, max(flux.(1))]
zspan = [0, max(flux.(2))]
nflux = n_elements(diffarr)

; Plot the blank axes
plot_3dbox, flux.(0)[0:1], flux.(1)[0:1], flux.(2)[0:1], /nodata, charsize=chars, /save, $
      psym=3, xrange=xspan, yrange=yspan, zrange=zspan, _extra=extrakey

; Overplot the differences between model and expected mass, colored by model mass
size = (1. - diffarr) > 0.2
cubehelix, rots=-1.2
colors = 80*modelvals + 320
for i=0L, nflux-1 do plots, flux.(0)[i], flux.(1)[i], flux.(2)[i], /t3d, $
                            psym=cgsymcat(14), symsize=size[i], color=colors[i]

if keyword_set(ps) then ps_close


;;; SCATTER PLOT #2
if keyword_set(zoom) then begin

    ; Set up for plotting
    device, decomposed=0
    lincolr_wb, /silent
    if keyword_set(ps) then begin
        outpath = strmid(outpath, 0, strlen(outpath)-5) + '.zoom'
        ps_open, outpath+'.scat', /color, /en, thick=4
        if n_elements(lchar) eq 0 then lchar = 1.2
        chars = 1.5
    endif else begin
        window, 2, retain=2, xsize=800, ysize=600
        if n_elements(lchar) eq 0 then lchar = 1.6
        chars = 1.8
    endelse

    ; Set up plotting variables
    zoomind = where((flux.(0) le zoom[0]) and (flux.(1) le zoom[1]) and (flux.(2) le zoom[2]))
    zflux0 = flux.(0)[zoomind]
    zflux1 = flux.(1)[zoomind]
    zflux2 = flux.(2)[zoomind]
    zdiff = diffarr[zoomind]
    zmodel = modelvals[zoomind]

    xspan = [0, zoom[0]]
    yspan = [0, zoom[1]]
    zspan = [0, zoom[2]]
    nzoom = n_elements(zdiff)

    ; Plot the blank axes
    plot_3dbox, zflux0[0:1], zflux1[0:1], zflux2[0:1], /nodata, charsize=chars, /save, $
          psym=3, xrange=xspan, yrange=yspan, zrange=zspan, _extra=extrakey

    ; Overplot the differences between model and expected mass, colored by model mass
    size = (1. - zdiff) > 0.2
    cubehelix, rots=-1.2
    colors = 80*zmodel + 320
    for i=0L, nzoom-1 do plots, zflux0[i], zflux1[i], zflux2[i], /t3d, $
               psym=cgsymcat(14), symsize=size[i], color=colors[i]

    if keyword_set(ps) then ps_close

endif


END

