PRO GAS_PARAM_DIFFERENT_N, CHOSEN, P, VALS, SPLIT

;+
;  Identifies the values for a specific tag (p) contained in an array of
;  structures.  Allows for different numbers of structures for each value of
;  (p).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-09-11
;
;  INPUTS
;      CHOSEN - array of structures that includes tag (p).
;      P - The number of the tag (a non-negative integer).
;
;  OUTPUTS
;      VALS - Vector containing the unique values of the tag (p).
;      SPLIT - The modal value of (p), i.e. the greatest number of structures
;              having one of the values of (p).
;-

; Identify the unique values of the tag (p).
vals = chosen[uniq(chosen.(p), sort(chosen.(p)))].(p)
nvals = n_elements(vals)

; Find the number of structures having each value of (p). 
splits = lonarr(nvals)
for i=0, nvals-1 do splits[i] = n_elements(where(chosen.(p) eq vals[i]))

; Find the modal value of (p) 
split = max(splits)

END

