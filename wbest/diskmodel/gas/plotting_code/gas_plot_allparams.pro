PRO GAS_PLOT_ALLPARAMS, ISO, TRANS, FLUX, NPAR, DIMS, PVALS, BARVALS, MGASVALS, TITLE=title, FILL=fill, $
                        COLORS=colors, CONLINE=conline, MEANBAR=meanbar, OUTPATH=outpath, PS=ps

;+
;  Called by histmatch_gas.pro and histbayes_gas.pro.
;
;  Makes the big spread of bar plots showing the parameter distributions. 
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-20
;
;  INPUTS
;      ISO - Vector (or scalar) indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Vector (or scalar) indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Vector (or scalar) containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      NPAR - Number of parameters in the disk models.
;      DIMS - Vector containing the number of values for each parameter.
;      PVALS - Values for x-axis (model parameter values).
;      BARVALS - Values for y-axis (marginalized posterior or fraction of
;                matched disks).
;      MGASVALS - Model gas mass values, in log space.
;
;  KEYWORDS
;      COLORS - Vector of color values for plotting the bars.
;      CONLINE - Plot horizontal dashed lines at P=0.05 and 0.159, indicating
;                the bottoms of the 68% and 90% confidence intervals.
;      FILL - Fill the histogram boxes.
;      MEANBAR - If set, plot the mean/expected Mgas and uncertainty bar.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/bayesmatch.eps
;      PS - send output to postscript
;-

niso = n_elements(iso)

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/bayesmatch'
    ps_open, outpath, /color, /en, thick=4
    charsarr = replicate(1.5, 12)
    lchar = 1.2
endif else begin
    window, 0, retain=2, xsize=1200, ysize=700
    charsarr = replicate(2, 12)
    lchar = 1.6
endelse

; Set up for multiple plots in the same window
nohist = n_elements(where(dims eq 1))
!p.multi = [0, floor((npar-nohist)+6)/2, 2, 0, 1]
if n_elements(colors) eq 0 then colors = [4, 3, 2, 0, 12, 13, 11, 6, 15]

; Loop over all parameters, plotting histogram for each one
for i=0, npar+1 do begin
    ; Default values, used for most histograms
    chars = charsarr[npar-1]
    xmarg = [6,-0.5]
    ymarg = [4,2]

    ; Twist and turn to put the histograms in the right places
    if (dims[i] eq 1) or (i eq 1) then continue             ; Don't plot histograms for disk parameters with single values.
    if (i eq 2) and (dims[0] ne 1) then $                   ; Skip the Mgas space.
      !p.multi = [(((npar-nohist)+6)/2)*2-2, floor((npar-nohist)+6)/2, 2, 0, 1]
    if i eq 7 then $                                        ; Make sure T_a1 and q_a plots are stacked.
      !p.multi = [8, floor((npar-nohist)+6)/2, 2, 0, 1]
    if i eq 11 then $                                       ; Plot inclination in the original Mgas space.
      !p.multi = [(((npar-nohist)+6)/2)*2-1, floor((npar-nohist)+6)/2, 2, 0, 1]
    if i eq 12 then !p.multi = [6, floor((npar-nohist)+6)/2, 2, 0, 1]

    ; Get the histogram details for the specific disk parameter.
    gas_labels_histo, i, label, xspan, intervals, binsize, logflag

    ; Plot the histogram
    gas_plot_hist, pvals.(i), barvals.(i), binsize, colors[0], psym=10, charsize=chars, backg=1, fill=fill, $
          color=0, xrange=xspan, xstyle=1, xtit=label, xmarg=xmarg, ymarg=ymarg, xtickint=intervals
endfor

;;; Special placement and size for Mgas histogram.
!p.multi = [4, floor((npar-nohist)+6)/2, 2, 0, 1]
if keyword_set(ps) then begin
    chars = 2
    xmarg = [6,-12]
    ymarg = [-39,15]/chars
    tsize = 1.10 - 0.05*niso
endif else begin
    chars = 3
    xmarg = [6,-16]
    ymarg = [-58,23]/chars
    tsize = 2.3 - 0.3*niso
endelse

; Get the histogram details for the gas mass parameter.
gas_labels_histo, 1, label, xspan, intervals, binsize, logflag

; Plot the gas mass histogram
;gas_plot_hist, mgasvals, barvals.(1), binsize*6./dims[1], colors[0], psym=10, charsize=chars, backg=1, fill=fill, $
gas_plot_hist, mgasvals, barvals.(1), binsize*6./dims[1], colors[0], charsize=chars, backg=1, fill=fill, $
    color=0, xrange=xspan, xstyle=1, xtit=label, xmarg=xmarg, ymarg=ymarg, xtickint=intervals

;;; Plot the confidence interval lines, if they fit on the plot.
if keyword_set(conline) then begin
    if !y.crange[1] gt 0.9 then begin
        hline, 0.9, lines=2, color=4, thick=6
        xyouts, -4.82, 0.892, '90%', color=4, chars=1.2, /data
    endif
    if !y.crange[1] gt 0.68 then begin
        hline, 0.68, lines=2, color=3, thick=6
        xyouts, -4.82, 0.672, '68%', color=3, chars=1.2, /data
    endif
endif

;;; Plot the mean and error bar
if keyword_set(meanbar) then begin
    meangas = total(mgasvals * barvals.(1))
    meangasu = sqrt(total((mgasvals-meangas)^2 * barvals.(1))) ; rms uncertainty
    if keyword_set(ps) then begin
        esize = 1.2
    endif else begin
        esize = 2
    endelse
    ; plot mean and error bar
    oploterror, meangas, 0.95*max(barvals.(1)), meangasu, 0, errthick=4, psym=16, symsize=esize
endif

;;; Print the title and fluxes used to generate the plots.
; Print a title for the slide
;; if keyword_set(title) then xyouts, 0.872, .915+.015*niso, title, color=3, chars=1.6, align=0.5, /normal
titxpos = mean(!x.crange)
titypos = 1.086
titps = !y.crange[1]
if keyword_set(title) then xyouts, titxpos, titps*(titypos+.02*niso), title, color=0, chars=1.6, align=0.5, /data
; Print the line fluxes used to make the histograms
islab = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
trlab = ['1-0', '2-1', '3-2']
label = 'f['+islab[iso-1]+' '+trlab[trans-1]+']'
;; for j=0, niso-1 do xyouts, 0.872, 0.9-0.03*(j-(niso-1)/2.), $
;;         label[j]+' = '+trim(flux[j])+' Jy km/s', color=3, chars=tsize, align=0.5, /normal
for j=0, niso-1 do xyouts, titxpos, titps*(titypos-0.04*(j-(niso-2)/2.)), $
        label[j]+' = '+trim(flux[j])+' Jy km/s', color=colors[0], chars=tsize, align=0.5, /data

; Reset to single plot in a window
!p.multi = [0, 0, 1, 0, 0]

if keyword_set(ps) then ps_close


END

