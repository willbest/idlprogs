PRO GAS_LABELS_HISTO, P, LABEL, XSPAN, INTERVALS, BINSIZE, LOGFLAG

;+
;  Returns a disk parameter label, abscissa range, and bin size for histograms.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-08
;  2013-10-09 (WB):  Added logflag.
;  2013-11-11 (WB):  Added 
;
;  INPUT
;      P - The number of the tag (a non-negative integer).
;
;  OUTPUTS
;      LABEL - Contains name of disk parameter identified by P.
;      XSPAN - Range of abscissa for histogram.
;      INTERVALS - Size of abscissa intervals between labels.
;      BINSIZE - Bin size for histogram.
;      LOGFLAG - Use logarithmic scale for the abscissa.
;-

logflag = 0

case p of
    0 : begin
        label = 'M!Dstar!N (M!D!9n!X!N)'
        xspan = [0.0, 2.3]
        intervals = 1.
        binsize = 0.3
    end
    1 : begin
        label = 'log M!Dgas!N (M!D!9n!X!N)'
        xspan = [-4.2, -1.3]
        intervals = 1
        binsize = 0.27
        logflag = 1
    end
    2 : begin
        label = textoidl('\gamma')
        xspan = [-0.3, 1.8]
        intervals = 1.
        binsize = 0.3
    end
    3 : begin
        label = 'R!Din!N (AU)'
        xspan = [0.0, 0.1]
        intervals = .05
        binsize = 0.02
    end
    4 : begin
        label = 'R!Dc!N (AU)'
        xspan = [0, 330]
        intervals = 100
        binsize = 25
    end
    5 : begin
        label = 'T!Dm1!N (K)'
        xspan = [10, 540]
        intervals = 200
        binsize = 32
    end
    6 : begin
        label = 'q!Dm!N'
        xspan = [0.4, 0.7]
        intervals = 0.1
        binsize = 0.04
    end
    7 : begin
        label = 'T!Da1!N (K)'
        xspan = [200, 1600]
        intervals = 500
        binsize = 80
    end
    8 : begin
        label = 'q!Da!N'
        xspan = [0.4, 0.7]
        intervals = 0.1
        binsize = 0.04
    end
    9 : begin
        label = 'N!Dpd!N (!9X!X10!U21!N cm!U-2!N)'
        xspan = [0.3, 2.4]
        intervals = 1.
        binsize = 0.2
    end
    10 : begin
        label = 'T!Dfreeze!N (K)'
        xspan = [0, 50]
        intervals = 20
        binsize = 5
    end
    11 : begin
        label = 'incl (deg)'
        xspan = [-15, 105]
        intervals = 30
        binsize = 16
    end
    12 : begin
        label = ['f_freeze']
        xspan = [0, 1]
        intervals = 0.5
        binsize = 0.1
    end
    else : begin
        label = ['f_dissoc']
        xspan = [0, 1]
        intervals = 0.5
        binsize = 0.1
    end
endcase

END

