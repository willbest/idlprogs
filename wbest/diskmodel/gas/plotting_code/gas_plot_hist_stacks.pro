PRO GAS_PLOT_HIST_STACKS, DIFFARR, MODELVALS, BINSIZE, FILL=fill, LABEL=label, $
                          LCHAR=lchar, OUTPATH=outpath, PS=ps, _EXTRA=extrakey

;+
;  Called by histmatch_gas.pro and histbayes_gas.pro.
;
;  Makes a bar plot of maginalized posterior vs. parameter value. 
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-29
;
;  INPUTS
;      DIFFARR - Array of differences between the model and expected (from
;                Bayesian inference) parameter values.
;      MODELVALS - Model parameter values.
;      BINSIZE - Width of the bars
;
;  KEYWORDS
;      FILL - Fill the histogram boxes.
;      LABEL - Title for plot -- which emission line has been fit.
;      LCHAR - Character size for the legend.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/bayesself.eps
;      PS - send output to postscript
;      Passes all plotting other keywords via _EXTRA.
;-

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/biggas/bayesself'
    ps_open, outpath+'.hist', /color, /en, thick=4
    if n_elements(lchar) eq 0 then lchar = 1.2
    chars = 1.5
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
    if n_elements(lchar) eq 0 then lchar = 1.6
    chars = 1.8
endelse

; Get the list of unique parameter vales
pvals = modelvals[uniq(modelvals, sort(modelvals))]
nvals = n_elements(pvals)

; Set up plotting variables
xspan = [0, max(diffarr)]

fullhist = histogram(diffarr, binsize=binsize, min=0, locations=minx)
yspan = [0, max(fullhist)]
nbins = n_elements(minx)


;;; PLOT THE HISTOGRAMS
plothist, diffarr, /nodata, color=0, backg=1, bin=binsize, xrange=xspan, yrange=yspan, chars=chars, _extra=extrakey
miny = lonarr(nbins)
maxy = miny


; Set up the colors
cubehelix, rots=-1.2
boxcolor = 80*pvals+320

for i=0, nvals-1 do begin

    ; Get the histogram array
    histvals = histogram(diffarr[where(modelvals eq pvals[i])], binsize=binsize, min=0, locations=minx)
    nhvals = n_elements(histvals)
    maxy[0:nhvals-1] = miny[0:nhvals-1] + histvals

    ; Plot each bar
    for j=0, nhvals-1 do begin
        xvec = [minx[j], minx[j]+binsize, minx[j]+binsize, minx[j]]   ; x-coords of box to fill with color
        yvec = [miny[j], miny[j], maxy[j], maxy[j]]                   ; y-coords of box to fill with color
        if keyword_set(fill) then begin
            polyfill, xvec, yvec, /data, color=boxcolor[i]            ; plot the bar
        endif else begin
            if not keyword_set(thick) then thick=4
            plots, [minx[j], minx[j]], [miny[j], maxy[j]], color=boxcolor[i], thick=thick
            if maxy[j] gt 0 then plots, [minx[j], minx[j]+binsize], [maxy[j], maxy[j]], color=boxcolor[i], thick=thick
            plots, [minx[j]+binsize, minx[j]+binsize], [0, maxy[j]], color=boxcolor[i], thick=thick
        endelse
    endfor
    miny = maxy

endfor


;;; PLOT THE LEGEND
; Add the title above the legend
legend, label, /right, chars=lchar, textcolors=0, box=0
leg = [' ', trim(10^reverse(pvals), '(e7.1)')+' Msun']
legend, leg, psym=[3, intarr(nvals)+15], colors=[255, reverse(boxcolor)], $
        textcolors=intarr(nvals+1), chars=lchar, /right, box=0

if keyword_set(ps) then ps_close


END

