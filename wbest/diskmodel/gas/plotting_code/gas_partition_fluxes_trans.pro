PRO GAS_PARTITION_FLUXES_PARAM, CHOSEN, VALS, ISO, TRANS, THIN, NVALS, NISO, NTRANS, NDISKS, $
                                DATA, MED, NLINES, YARR

;+
;  Called by scatter_gas_flux.pro
;
;  Partitions a data structure according to transitions.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-09-12
;
;  INPUTS
;
;
;  OUTPUTS
;      DATA - Array of flux data to be plotted.
;      MED - Array containing medians for each line and partition of the data.
;      NLINES - Number of lines to be plotted.
;      YARR - Array of transitions indices to plotted vs. DATA.
;-

yarr = reform(transpose(cmreplicate(trans, ndisks)), ntrans*ndisks)   ; yarr contains consecutive vectors,
                        ; each the length of chosen, with numbers corresponding to the chosen transitions
nlines = niso                                             ; Number of isotopologues being plotted
data = fltarr(ndisks*ntrans,nlines)                       ; Create array for the flux(es) from each disk
med = fltarr(nvals,nlines)                                ; Create array for median flux(es) from each bin of disks
for k=0, niso-1 do begin                                  ; Loop over chosen transitions
    flux = chosen.(iso[k]+13).(trans[0]-1)[thin-1]        ; Get flux values for first chosen transition
    for j=1, ntrans-1 do flux = [flux, chosen.(iso[k]+13).(trans[j]-1)[thin-1]]             
                                                          ; Get flux values for other chosen transition(s)
    data[*,k] = flux                                      ; Store these fluxes in the sdata array
    for i=0, nvals-1 do begin                             ; Loop over chosen transitions
        med[i,k] = median(flux[where(yarr eq vals[i])])   ; Get median flux for a bin of disks
    endfor
endfor

END

