;ps=1
;PRO MAKE_TAURUS_PLOT

;+
;  Plots the expected gas masses and uncertainties for coarse and fine gas
;  grids, for both the disk-matching and Bayesian analyses.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-11-19
;  
;-

; Names of the sources
source = ['AA Tau', 'BP Tau', 'CI Tau', 'CY Tau', 'DL Tau', 'CO Tau', 'DQ Tau', 'H6-13', 'IQ Tau', 'I04385']

; Data for the disk-matching analysis, coarse (big) grid
mc_mgas = [-2.93, -3.72, -2.52, -3.00, -3.14, -2.78, -3.72, -2.52, -3.12, 0]
mc_uncert = [.17, .40, .00, .02, .27, .24, .40, .00, .27, 0] > .25

; Data for the disk-matching analysis, coarse (big) grid
mf_mgas = [-2.85, -3.33, -2.61, -3.05, -3.19, -2.77, -3.33, -2.47, -3.17, 0]
mf_uncert = [.15, .29, .12, .15, .21, .14, .29, .14, .21, 0] ;> .09

; Data for the Bayesian analysis, coarse (big) grid
bc_mgas = [-2.94, -3.73, -2.59, -3.03, -3.14, -2.84, -3.73, -2.52, -3.13, -3.08]
bc_uncert = [.17, .40, .17, .12, .34, .23, .40, .06, .31, .26]

; Data for the Bayesian analysis, fine grid
bf_mgas = [-2.88, -3.33, -2.67, -3.09, -3.17, -2.81, -3.33, -2.49, -3.16, -3.03]
bf_uncert = [.17, .33, .14, .17, .28, .17, .29, .15, .25, .21]

; Put the data in a structure
data = [{m:mc_mgas, u:mc_uncert}, {m:mf_mgas, u:mf_uncert}, {m:bc_mgas, u:bc_uncert}, {m:bf_mgas, u:bf_uncert}]
ndata = n_elements(data)

; Which points have error bars, which are upper limits
bar = [0, 2, 3, 4, 5, 7, 8, 9]
arrow = [1, 6]

; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = ' ~/Astro/699-2/results/Taurusplot'
    ps_open, outpath, /color, /en, /ps, thick=4
    chars = 1
    lchar = 1
    syms = 1.8
    et = 4
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
    chars = 1.4
    lchar = 1.6
    syms = 2
    et = 2
endelse
colors = [0, 15, 4, 6]

; Plot
plot, [0], /nodata, color=0, backg=1, chars=chars, $
      xrange=[-1,10], xsty=1, xtickint=1, xminor=1, xtickv=indgen(12), xtickn=[' ', source, ' '], $
      yrange=[-4,-2], ysty=1, ytit='<log M!Dgas!N>'

for i=0, ndata-1 do begin
    oploterror, bar+0.08*(i-(ndata-1)/2.), data[i].m[bar], data[i].u[bar], $
                psym=cgsymcat(14), symsize=syms, errthick=et, color=colors[i]

    oplot, arrow+0.09*(i-(ndata-1)/2.), data[i].m[arrow], psym=cgsymcat(14), symsize=syms, color=colors[i]
    plotsym, 1, 40.*.25, thick=et, color=colors[i]
    oplot, arrow+0.09*(i-(ndata-1)/2.), data[i].m[arrow], psym=8
endfor

; Legend
leg = ['Matching, coarse grid', 'Matching, fine grid', 'Bayesian, coarse grid', 'Bayesian, fine grid']

legend, leg, textc=colors, box=0, colors=colors, $
            psym=cgsymcat(14), symsize=replicate(syms-.3, ndata), chars=chars

if keyword_set(ps) then ps_close

END
