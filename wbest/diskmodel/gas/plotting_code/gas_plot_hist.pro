PRO GAS_PLOT_HIST, PVALS, MARG, BINSIZE, BOXCOLOR, FILL=fill, THICK=thick, _EXTRA=extrakey

;+
;  Called by histmatch_gas.pro and histbayes_gas.pro.
;
;  Makes a bar plot of maginalized posterior vs. parameter value. 
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-18
;
;  INPUTS
;      PVALS - Values for x-axis (parameter values).
;      MARG - Values for y-axis (marginalized posterior).
;      BINSIZE - Width of the bars
;      BOXCOLOR - Color of the bars
;
;  KEYWORDS
;      FILL - Filled the histogram boxes
;      THICK - Thickness of unfilled histogram lines.
;      Passes all plotting other keywords via _EXTRA.
;-

plot, pvals, marg, _extra=extrakey, /nodata
for j=0, n_elements(pvals)-1 do begin
    minx = pvals[j]-binsize/2.
    maxy = marg[j]
    xvec = [minx, minx+binsize, minx+binsize, minx]  ; x-coords of box to fill with color
    yvec = [0, 0, maxy, maxy]                        ; y-coords of box to fill with color
    if keyword_set(fill) then begin
        polyfill, xvec, yvec, /data, color=boxcolor  ; plot the bar
    endif else begin
        if not keyword_set(thick) then thick=4
        plots, [minx, minx], [0, maxy], color=boxcolor, thick=thick
        if maxy gt 0 then plots, [minx, minx+binsize], [maxy, maxy], color=boxcolor, thick=thick
        plots, [minx+binsize, minx+binsize], [0, maxy], color=boxcolor, thick=thick
    endelse
    ;tvbox, [binsize, maxy], pvals[j], maxy/2., boxcolor, /data, fill=fill, thick=thick
endfor

END

