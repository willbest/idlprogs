PRO GAS_LABELS_NO_VALUES, P, LABEL, ISNAME, TRNAME, THLAB

;+
;  Returns a disk parameter label for plot legends, without values for the
;  parameter, and names of isotopologues and transitions.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-09-11
;
;  INPUT
;      P - The number of the tag (a non-negative integer).
;
;  OUTPUTS
;      LABEL - Contains name of disk parameter identified by P.
;      ISNAME - Vector containing the names of the isotopologues.
;      TRNAME - Vector containing the names of the J transitions.
;      ISNAME - Vector containing the labels for optically thick/thin/total.
;      SPLIT - The modal value of (p), i.e. the greatest number of structures
;-

case p of
    0 : label = ['M!Dstar!N (M!D!9n!X!N)']
    1 : label = ['M!Dgas!N (M!D!9n!X!N)']
    2 : label = [textoidl('\gamma')]
    3 : label = ['R!Din!N (AU)']
    4 : label = ['R!Dc!N (AU)']
    5 : label = ['T!Dm1!N (K)']
    6 : label = ['q!Dm!N']
    7 : label = ['T!Da1!N (K)']
    8 : label = ['q!Da!N']
    9 : label = ['N!Dpd!N (!9X!X10!U21!N cm!U-2!N)']
    10 : label = ['T!Dfreeze!N (K)']
    11 : label = ['inclination (degrees)']
    12 : label = ['Isotopologue']
    else : label = ['Transition']
endcase

isname = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
trname = ['J=1-0', 'J=2-1', 'J=3-2']
thlab = ['Optically thick emission', 'Optically thin emission', 'Total emission']

END

