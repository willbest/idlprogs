FUNCTION LIKELIHOOD_GAS, FLUX, UNCERT, MODEL, THRESH

;+
; Returns the unnormalized likelihood(s) for model disk fluxes matching the
; measured flux.  Designed to be used with the gas disk simulations.
; Likelihood is computed IN NATURAL LOG SPACE.
;
; HISTORY
; Written by Will Best (IfA), 2013-11-13
;
; INPUTS
;     FLUX - Measured flux.
;     UNCERT - Uncertainty in the measured flux.
;     MODEL - Flux predicted by a model.  May be a vector.
;     THRESH - Threshold for non-detections.
;-

; Check inputs
if(n_params()) lt 3 then begin
    print
    print, 'Use:  result = likelihood_gas(flux, uncert, model [, thresh])'
    print
    return, 0
endif

if n_elements(thresh) eq 0 then thresh = 0.
if uncert eq 0 then uncert = 1e-4

; Likelihood calculation
if flux ge thresh then begin    ; If the line is detectable
    ; Likelihood is chi-sq of model flux matching measured flux,
    ;     with sigma equal to the measurement uncertainty.
    ;; likelihood = -(model - flux)^2 / (2.*uncert^2)
    likelihood = chisq_prob_ln(model, [flux, uncert])
endif else begin                ; For non-detections
    ; Assume the flux is the threshold value.
    ;     For models above this, likelihood is the same chi-sq.
    ;     For models below, uniform likelihood (=1, in ln space).
    below = where(model lt thresh, comp=above)
    likelihood = dblarr(n_elements(model))
    ;; if above[0] ge 0 then likelihood[above] = -(model[above] - (thresh - 3.*uncert))^2 / (2.*uncert^2)
    if above[0] ge 0 then likelihood[above] = chisq_prob_ln(model[above], [(thresh - 3.*uncert), uncert])
    if below[0] ge 0 then likelihood[below] = 0d
endelse

return, likelihood

END
