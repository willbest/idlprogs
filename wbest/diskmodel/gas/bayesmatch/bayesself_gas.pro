PRO BAYESSELF_GAS, ISO, TRANS, UNCERT, THRESH, PARAM, OUTVALS, $
                   GAS=gas, DATAFILE=datafile, FILEPATH=filepath, OUTPATH=outpath, $
                   MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, $
                   TA1=ta1, QA=qa, NPD=npd, TFREEZE=tfreeze, INCL=incl, THIN=thin, $
                   FLUXMAX=fluxmax, NOPLOT=noplot, PLIM=plim, PS=ps, ZOOM=zoom, _EXTRA=extrakey

;+
;  Runs the Bayesian fitting routine for the parameter indicated by PARAM 
;  over all model fluxes and uncertainties for a single specified emission
;  line. Saves the summary data to an IDL save file named by the OUTPATH
;  keyword.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the fitting will incorporate only disks with those parameter
;  values.
;
;  If no value is specified for a given parameter, then the fitting will include
;  disks with all values of that parameter (that have the values given for other
;  parameters).
;  
;  This program calls bayesselfplots_gas.pro.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-30
;
;  INPUTS
;      ISO - Scalar indicating the CO isotopologue of the emission line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Scalar indicating the CO rotational transition of the
;              emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      UNCERT - Structure containing a single vector (can be one-element) of the
;               uncertainties in flux of the line to be matched to the disk
;               models.  Entries correspond to those in ISO and TRANS.
;      THRESH - Scalar equal to detection threshold for line fluxes.  Fluxes
;               less than this value will be considered non-detections.
;               Default: 0.
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      OUTVALS - Array of structures containing values summarizing the results
;                of the Bayesian fitting:
;              { maxbin:most likely Mgas bin, maxp:max. posterior, 
;                nmatch:no. of disks with unnormalized posterior > plim, 
;                expec:expected Mgas, unc:uncertainty in expected Mgas }
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load DATAFILE.
;      DATAFILE - name of the IDL save file containing the "gas".  If none is
;                supplied, the program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      FLUXMAX - Only fit disks whose flux does not exceed this value.
;      NOPLOT - Don't make the plot.  Just save the data.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/bayesself.eps
;      PLIM - Count the number of disks with unnormalized posterior above this
;             value.
;      PS - send output to postscript
;      ZOOM - Make a second scatter plot, with xrange=[0, zoom].
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the plot.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

;;; DETERMINE THE PARAMETER FOR WHICH TO FIND MEAN AND STANDARD DEVIATION
if n_elements(param) eq 0 then param = 2    ; Default PARAM is gas mass

; Check input parameter
npar = 12
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif


;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO and TRANS must be scalars.
;                          FLUX and UNCERT must be structures with one tag,
;                          containing vectors of matching lengths.
errflag = 0
niso = n_elements(iso)
if (niso ne 1) or (n_elements(trans) ne niso) or (n_elements(uncert) ne niso) then errflag = 1

if errflag then begin
    print
    print, 'Use:  bayesself_gas, ISO, TRANS, UNCERT [, THRESH, PARAM, OUTVALS, gas=gas,'
    print, '           datafile=datafile, filepath=filepath, noplot=noplot, outpath=outpath, plim=plim, ps=ps,'
    print, '           mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa,'
    print, '           npd=npd, tfreeze=tfreeze, incl=incl, thin=thin, fluxmax=fluxmax, zoom=zoom]'
    print, '    ISO, TRANS, UNCERT, and THRESH must be scalars.'
    print
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif

; Check for a THRESH input; if none, set THRESH = 0.
if n_elements(thresh) eq 0 then thresh = 0.
if n_elements(thresh) eq 1 then thresh = replicate(thresh, niso)
negth = where(thresh lt 0)
if negth[0] ge 0 then thresh[negth] = 0.

; Check for an output path
if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/bayesself'


;;; GET THE DATA
if n_elements(gas) eq 0 then gas = getdata_gas(filepath=filepath, gasname=datafile)


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
;    in a vector called 'select'.
select = gas_select_disks(gas, keys, npar+2)

; Choose the disks with the requested parameter values
chosen = gas[select]
if keyword_set(fluxmax) then chosen = chosen[where(chosen.(iso[0]+13).(trans[0]-1)[thin-1] le fluxmax)]
nchosen = n_elements(chosen)

; Set up structures for the fluxes
flux = {line0:chosen.(iso[0]+13).(trans[0]-1)[thin-1]}
unc = {unc0:sqrt((flux.(0)*uncert)^2 + (thresh[0]/3.)^2)}


;;; RUN THE BAYESIAN INFERENCE
nflux = n_elements(flux.(0))
for j=0L, nflux-1 do begin
    if j/100 eq j/100. then print, trim(j)+' out of '+trim(nflux)+' done.'
    histbayes_gas, iso, trans, flux.(0)[j], unc.(0)[j], thresh, sumvals, chosen=chosen, $
                   thin=thin, plim=plim, /noplot, /silent
    if j eq 0 then outvals = sumvals else outvals = [outvals, sumvals]
endfor


;;; OUTPUT THE RESULTS TO A SAVE FILE
modelvals = chosen.(param-1)
save, iso, trans, flux, modelvals, outvals, filename=outpath+'.sav'


;;; PLOT THE RESULTS TO SCREEN OR POSTSCRIPT
if not keyword_set(noplot) then bayesselfplots_gas, outpath, param, outpath=outpath, ps=ps, zoom=zoom


END
