PRO BAYESSELFPLOTS9_GAS, PLOTDATA, PARAM, OUTPATH=outpath, PS=ps

;+
;  Called by bayesself9_gas.pro.
;
;  Makes a histogram of abs(log Mgas – <log Mgas>), stacked by model gas mass
;  (colors), indicating the accuracy of the Bayesian fitting to model-generated
;  fluxes of all nine lines. 
;
;  HISTORY
;  Written by Will Best (IfA), 2013-12-01
;
;  INPUTS
;      PLOTDATA - path to the IDL save file containing the data to plot.
;                 Default: ~/Astro/699-2/results/bayesself2.sav
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/bayesself9.eps
;      PS - send output to postscript
;-

; Paramter to plot (default is Mgas)
if n_elements(param) eq 0 then param = 2

;;; LOAD THE STRUCTURE CONTAINING THE DATA TO PLOT
if n_elements(plotdata) eq 0 then plotdata = '~/Astro/699-2/results/bayesself9'
restore, plotdata+'.sav'
if param eq 2 then modelvals = alog10(modelvals)

; Number of data points to plot
nflux = n_elements(outvals)


;;; PLOT THE STACKED HISTOGRAM
; Get the label details for the specific disk parameter.
label = 'All nine lines'
;lchar = 

; Determine the axes
diffarr = abs(modelvals - outvals.expec)
binsize = 0.05
xlab1 = '!7D!X<log M!Dgas!N>'

ylab1 = 'Number of model disks'

; Plot the histogram
gas_plot_hist_stacks, diffarr, modelvals, binsize, /fill, lchar=lchar, label=label, outpath=outpath, $
      ps=ps, xtitle=xlab1, ytitle=ylab1, charsize=chars, backg=1, color=0


END
