;PRO MAKEBAYESACCUR_GAS

; Wrapper for bayesaccur_gas.pro

param = 2
limit = 0.25

title='bayes.self.fine.all9.full'
path = '~/Astro/699-2/results/finegas/bayesplots/ALMA/'+title

;silent=1

bayesaccur_gas, path, limit, param, frac1=frac1, frac2=frac2, fracu=fracu
print, '   '+title+':   '+trim(frac1*100, '(f4.1)')+'%'+'      '+$
       trim(frac2*100, '(f4.1)')+'%'+'      '+trim(fracu*100, '(f4.1)')+'%'
print

;+
;  Calculates the differences between the expected gas masses and model gas
;  masses, i.e., abs(log Mgas – <log Mgas>), for the model fluxes fit by the
;  bayesself#_gas routines.
;
;  Reports the fraction of differences that fall below the value indicated by
;  the LIMIT parameter.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-12-01
;
;  INPUTS
;      SUMMDATA - path to the IDL save file containing the data to plot.
;                 Default: ~/Astro/699-2/results/bayesself.sav
;      LIMIT - Program reports the fraction of models whose 
;              abs(log Mgas – <log Mgas>) is below this value.
;              Default:  0.25
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      FRACTION - Variable that will contain the fraction of model disks with
;                 abs(log Mgas – <log Mgas>) below LIMIT.
;      SILENT - Suppress screen output
;-

END

