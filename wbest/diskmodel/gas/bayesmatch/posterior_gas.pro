FUNCTION POSTERIOR_GAS, LIKELIHOOD, PRIOR, LINEAR=linear, LOG10=log10, NUM=num, PLIM=plim

;+
;  Returns the normalized posterior distribution for model disk fluxes matching
;  the measured flux(es).  Designed to be used with the gas disk simulations.
;
;  The posterior is return in linear space.  By default, it is calculated in
;  natural log space.  Keywords can be set to do the calculation in linear or
;  log base 10 space.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-16
;
;  INPUTS
;      LIKELIHOOD - Array containing the likelihoods for the model disks.
;      PRIOR - Array containing the priors for the model disks.
;
;  KEYWORDS
;      LINEAR - Compute and normalize the posterior in linear space.
;      LOG10 - Compute and normalize the posterior in log base 10 space.
;
;  OUTPUT KEYWORDS
;      NUM - The number of model disks with unnormalized posterior above the
;            value given by PLIM.
;      PLIM - Lower limit for unnormalized posterior for a disk to be included
;             in the tally returned as NUM.
;             Default: -3 (log space, equivalent to P = 0.001)
;-

; Apply Bayes' Theorem (in log space) to find the posterior PDF.
if keyword_set(linear) then posterior = likelihood * prior $
  else posterior = likelihood + prior

; Count number of disks with posterior above the PLIM value.
if n_elements(plim) eq 0 then plim = -3.
num = n_elements(where(posterior gt plim))

; Find the sum of the posteriors over the entire grid.
if keyword_set(linear) then pos_total = total(posterior) $
  else if keyword_set(log10) then pos_total = alog10(total(10^posterior)) $
  else pos_total = alog(total(exp(posterior)))

; Use this sum to normalize the grid and get the final PDF.
if keyword_set(linear) then posterior = posterior / pos_total $
  else posterior = posterior - pos_total

; Convert to linear space, if needed
if keyword_set(log10) then posterior = 10^posterior $
  else posterior = exp(posterior)


return, posterior

END
