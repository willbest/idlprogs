PRO BAYESGRID2_GAS, ISO, TRANS, FLUX, UNCERT, PARAM, THRESH, PMAXARR, P2ARR, $
                    GAS=gas, FILEPATH=filepath, LIST=list, OUTPATH=outpath, PS=ps, SUBLIM=sublim, $
                    MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, $
                    TA1=ta1, QA=qa, NPD=npd, TFREEZE=tfreeze, INCL=incl, _EXTRA=extrakey
;+
;  Gets the highest and second-highest posteriors for the parameter indicated by PARAM 
;  over a set of fluxes and uncertainties for two specified emission lines.
;  Makes a plot indicating the highest and second-highest posterior of the disk
;  parameter values for each pair of fluxes.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the means and stddevs will incorporate only disks with those parameter
;  values.
;
;  If no value is specified for a given parameter, then the means and stddevs
;  will include disks with all values of that parameter (that have the values
;  given for other parameters).
;  
;  This program calls bayescore2_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-11-11
;
;  INPUTS
;      ISO - 2-element vector indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - 2-element vector indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Structure of two vectors (can be one-element) containing the fluxes,
;             in Jy km/s, of each line to be matched to the disk models.
;             Format: { line_1:[fluxes], line_2:[fluxes] }
;             Vectors correspond to entries in ISO and TRANS.  
;      UNCERT - Structure of two vectors (can be one-element) containing the
;               uncertainties in flux of each line to be matched to the disk
;               models.  Entries correspond to those in FLUX, using the same 
;               format as FLUX. 
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;      THRESH - Scalar equal to detection threshold for line fluxes.  Fluxes
;               less than this value will be considered non-detections.
;               Default: 0.
;
;  OUTPUTS
;      PMAXARR - Vector of values of the highest posterior for each line flux.
;      P2ARR - Vector of values of the second-highest posterior for each line flux.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      LIST - Print all the flux0-flux1-nmatch-mean-stddev combinations to the screen.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/bayesgrid2.eps
;      PS - send output to postscript
;      SUBLIM - only plot points for which the uncertainty is less than the
;               limiting case of sigma = 0.25.
;      LOG - Compute and return mean and standard deviation in log space.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the plots.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

;;; DETERMINE THE PARAMETER FOR WHICH TO FIND MEAN AND STANDARD DEVIATION
if n_elements(param) eq 0 then param = 2    ; Default PARAM is gas mass

; Check input parameter
npar = 12
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif


;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO and TRANS must be two-element vectors.
;                          FLUX and UNCERT must be structures with two tags,
;                          which are vectors of matching lengths.
errflag = 0
niso = n_elements(iso)
if (niso ne 2) or (n_elements(trans) ne niso) then errflag = 1

t_flux = n_tags(flux)
t_uncert = n_tags(uncert)
if (t_flux ne 2) or (t_flux ne t_uncert) then errflag = 1

for i=0, ((t_flux-1) < (t_uncert-1)) do begin
    if n_elements(flux.(i)) ne n_elements(uncert.(i)) then errflag = 1
endfor

if errflag then begin
    print
    print, 'Use:  matchgrid2_gas, ISO, TRANS, FLUX, UNCERT, PARAM [, THRESH, PMAXARR, P2ARR,'
    print, '           gas=gas, filepath=filepath, list=list, outpath=outpath, ps=ps, sublim=sublim, '
    print, '           mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa,'
    print, '           npd=npd, tfreeze=tfreeze, incl=incl, thin=thin]'
    print, '    ISO and TRANS must be two-element vectors.'
    print, '    FLUX and UNCERT must be structures with two tags, which are vectors of matching lengths.'
    print
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif

; Check for a THRESH input; if none, set THRESH = 0.
if n_elements(thresh) eq 0 then thresh = 0.
if thresh lt 0 then thresh = 0.


;;; GET THE DATA
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/gas/'
if not keyword_set(gas) then gas='gas.sav'
restore, filepath+gas


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
; in a vector called 'select'.
select = gas_select_disks(gas, keys, npar+2)

; Choose the disks with the requested parameter values
chosen = gas[select]
nchosen = n_elements(chosen)


;;; GET THE MAXIMUM AND SECOND-HIGHEST POSTERIOR FOR EACH FLUX
nflux0 = n_elements(flux.(0))
nflux1 = n_elements(flux.(1))
pmaxarr = fltarr(nflux0, nflux1)
p2arr = pmaxarr
priors = dblarr(nchosen)

for i=0, nflux0-1 do begin
    for j=0, nflux1-1 do begin
        bayescore2_gas, chosen, iso, trans, [flux.(0)[i], flux.(1)[j]], [uncert.(0)[i], uncert.(1)[j]], $
                        thresh, priors, pmax, p2, param, _extra=extrakey
        pmaxarr[i,j] = pmax
        p2arr[i,j] = p2
    endfor
    print, 'Completed '+trim(i+1)+' cycles out of '+trim(nflux0)+'.'
endfor


;;; PLOT
; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = ' ~/Astro/699-2/results/biggas/bayesgrid2'
    ps_open, outpath, /color, /en, thick=4
    lchar = 1.2
endif else begin
    window, 0, retain=2, xsize=800, ysize=700
    lchar = 1.6
endelse

; Color scheme and symbol size
siglimits = [0.05, 0.159]
size = 125./(nflux0 > nflux1)

; Scale -- needed in case of flux vectors of different length
xr = [0, max(flux.(0))]
yr = [0, max(flux.(1))]

; Get the label details for the specific disk parameter.
gas_labels_histo, param-1, pname
trlab = ['J=1-0', 'J=2-1', 'J=3-2']
islab = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
thlab = ['Optically thick emission', 'Optically thin emission', 'Total emission']

label0 = 'Flux ('+islab[iso[0]-1]+' '+trlab[trans[0]-1]+')'
label1 = 'Flux ('+islab[iso[1]-1]+' '+trlab[trans[1]-1]+')'

; Make the plot
plot, flux.(0), flux.(1), charsize=1.6, backg=1, color=0, /nodata, $
      xtit=label0, ytit=label1, xrange=xr, yrange=yr, xstyle=1, ystyle=1
for i=0, nflux0-1 do begin
    for j=0, nflux1-1 do begin
        if p2arr[i,j] lt siglimits[0] then col = 4 $
          else if p2arr[i,j] lt siglimits[1] then col = 3 $
          else col = 0
        if not (keyword_set(sublim) and (p2arr[i,j] ge siglimits[1])) then $
          plots, flux.(0)[i], flux.(1)[j], color=col, $
                 psym=cgsymcat(14), symsize=(pmaxarr[i,j]-.15)*size
        ; Print the flux0-flux1-pmax-p2 combinations to screen
        if keyword_set(list) then begin
            if ((i eq 0) and (j eq 0)) then print, '       Flux0        Flux1       P_max        P_2'
            print, flux.(0)[i], flux.(1)[j], pmaxarr[i,j], p2arr[i,j]
        endif
    endfor
endfor

; Legend
;; leg = [pname, thlab[thin-1], '68% within factor of 3', '90% within factor of 3', $
;;        'P = 0.3', 'P = 1']
;; leg_tcol = [0, 0, 3, 4, 0, 0]
;; leg_syms = [0, 0, 0.7*size, 0.7*size, 0.15*size, 0.85*size]
;; leg_symc = [1, 1, 3, 4, 0, 0]
;; legend, leg, outline_color=black, textcolor=leg_tcol, psym=14, symsize=leg_syms, $
;;         colors=leg_symc, charsize=lchar, _extra=extrakey

if keyword_set(ps) then ps_close


END
