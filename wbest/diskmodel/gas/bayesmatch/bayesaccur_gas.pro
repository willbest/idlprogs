PRO BAYESACCUR_GAS, SUMMDATA, LIMIT, PARAM, FRAC1=fraction, FRAC2=fraction2, $
                    FRACU=fractionu, SILENT=silent

;+
;  Calculates the differences between the expected gas masses and model gas
;  masses, i.e., abs(log Mgas – <log Mgas>), for the model fluxes fit by the
;  bayesself#_gas routines.
;
;  Reports
;  1) The fraction of differences that fall below the value indicated by the
;     LIMIT parameter.
;  2) The fraction of differences that fall below 2 x LIMIT.
;  3) The fraction of models whose gas mass is correctly calculated to within
;     the Bayesian uncertainty.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-12-01
;  2013-12-05 (WB):  Added the 2xLIMIT and within-uncertainty outputs.
;
;  INPUTS
;      SUMMDATA - path to the IDL save file containing the data to plot.
;                 Default: ~/Astro/699-2/results/bayesself.sav
;      LIMIT - Program reports the fraction of models whose 
;              abs(log Mgas – <log Mgas>) is below this value.
;              Default:  0.25
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      FRAC - Variable that will contain the fraction of model disks with
;             abs(log Mgas – <log Mgas>) below LIMIT.
;      FRAC2 - Variable that will contain the fraction of model disks with
;              abs(log Mgas – <log Mgas>) below 2xLIMIT.
;      FRACU - Variable that will contain the fraction of model disks with
;              the correct calcualted mass, within the uncertainty.
;      SILENT - Suppress screen output
;-

; Paramter to evaluate (default is Mgas)
if n_elements(param) eq 0 then param = 2

; Limit to evaluate (default is 0.25)
if n_elements(limit) eq 0 then limit = 0.25


;;; LOAD THE STRUCTURE CONTAINING THE DATA
if n_elements(summdata) eq 0 then summdata = '~/Astro/699-2/results/bayesself'
restore, summdata+'.sav'
if param eq 2 then modelvals = alog10(modelvals)

; Number of data points
nmodels = n_elements(outvals)


;;; COUNT THE FRACTION OF MODELS
diffarr = abs(modelvals - outvals.expec)
ngood = n_elements(where(diffarr lt limit))
ngood2 = n_elements(where(diffarr lt (2*limit)))
ngoodu = n_elements(where( (modelvals ge (outvals.expec-outvals.unc)) $
                           and (modelvals le (outvals.expec+outvals.unc)) ))

fraction = float(ngood)/nmodels
fraction2 = float(ngood2)/nmodels
fractionu = float(ngoodu)/nmodels

; Print output to screen
if not keyword_set(silent) then begin
    print
    print, '   '+trim(fraction*100, '(f4.1)')+'% of models match with delta < '+trim(limit)
    print, '   '+trim(fraction2*100, '(f4.1)')+'% of models match with delta < '+trim(2*limit)
    print
    print, '   '+trim(fractionu*100, '(f4.1)')+'% of models have correct calculated mass within the uncertainty.'
    print
endif


END
