PRO HISTBAYES_GAS, ISO, TRANS, FLUX, UNCERT, THRESH, SUMVALS, $
                   CHOSEN=chosen, GAS=gas, DATAFILE=datafile, FILEPATH=filepath, OUTPATH=outpath, PS=ps, $
                   MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, TA1=ta1, QA=qa, $
                   NPD=npd, TFREEZE=tfreeze, INCL=incl, THIN=thin, COLORS=colors, CONLINE=conline, $
                   MEANBAR=meanbar, NOPLOT=noplot, PLIM=plim, SILENT=silent, TITLE=title, _EXTRA=extrakey

;+
;  Reads the output line strengths, optically thick and thin fractions, and
;  frozen and dissociated CO gas fractions from the make_gas simulations.  
;  Performs Bayesian inference to determine the values of each disk parameter  
;  given fluxes and uncertainties for specified emission lines.
;
;  If specific values for various other parameters are supplied (via keyword),
;  then the histograms will incorporate only disks with those parameter values.
;
;  If no value is specified for a given parameter, then the histograms will
;  include disks with all values of that parameter (that have the values given
;  for other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-11-10
;  
;
;  INPUTS
;      ISO - Vector (or scalar) indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - Vector (or scalar) indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Vector (or scalar) containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - Vector (or scalar) containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;      THRESH - Scalar or vector equal to detection threshold for line fluxes.
;               Fluxes less than this value will be considered non-detections.
;               Default: 0.
;
;  OUTPUTS
;      SUMVALS - Structure containing values summarizing the results of the
;                Bayesian fitting:
;              { maxbin:most likely Mgas bin, maxp:max. posterior, 
;                nmatch:no. of disks with unnormalized posterior > plim, 
;                expec:expected Mgas, unc:uncertainty in expected Mgas }
;
;  KEYWORDS
;      CHOSEN - Structure containing the data for the specific model disks that
;               will be plotted.  If none is supplied, the program will load the
;               structure indicated by GAS.
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load DATAFILE.
;      DATAFILE - name of the IDL save file containing the "gas".  If none is
;                supplied, the program will load 'gas.sav' from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/histbayes.eps
;      PLIM - Count the number of disks with unnormalized posterior above this
;             value.
;      PS - send output to postscript
;      COLORS - Vector of colors for histograms (using lincolr_wb)
;      CONLINE - Plot horizontal dashed lines at P=0.05 and 0.159, indicating
;                the bottoms of the 68% and 90% confidence intervals.
;      MEANBAR - Plot the mean and uncertainty bar for the posterior M_gas
;                distribution.
;      NOPLOT - Suppress histogram plotting.
;      SILENT - Suppress screen output.
;      TITLE - Optional string containing a title to be added to the plot.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the histogram.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the histograms.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

; Number of disk parameters
npar = 12


;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO, TRANS, FLUX, and UNCERT must be equal length
niso = n_elements(iso)
if (niso lt 1) or (n_elements(trans) ne niso) or (n_elements(flux) ne niso) $
  or (n_elements(uncert) ne niso) then begin
    print, 'Use:  histbayes_gas, ISO, TRANS, FLUX, UNCERT [, THRESH, SUMVALS, chosen=chosen, gas=gas,'
    print, '           datafile=datafile, filepath=filepath, outpath=outpath, plim=plim, ps=ps,'
    print, '           mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm,'
    print, '           ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, thin=thin, colors=colors,'
    print, '           conline=conline, meanbar=meanbar, noplot=noplot, silent=silent, title=title ]'
    print, '    ISO, TRANS, FLUX, and UNCERT must be scalars or vectors of equal length.'
    return
endif

if n_elements(chosen) eq 0 then begin
; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
    if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
        print, 'ISO can only contain the values 1, 2 and 3.'
        return
    endif
    if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
        print, 'TRANS can only contain the values 1, 2 and 3.'
        return
    endif

; Check for a THRESH input; if none, set THRESH = 0.
    if n_elements(thresh) eq 0 then thresh = 0.
    if n_elements(thresh) eq 1 then thresh = replicate(thresh, niso)
    negth = where(thresh lt 0)
    if negth[0] ge 0 then thresh[negth] = 0.


;;; GET THE DATA
    if n_elements(gas) eq 0 then gas = getdata_gas(filepath=filepath, gasname=datafile)


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
    keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
; in a vector called 'select'.
    select = gas_select_disks(gas, keys, npar+2)

; Choose the disks with the requested parameter values
    chosen = gas[select]
endif

nchosen = n_elements(chosen)


;;; DETERMINE THE PARAMETER SPACE
pvals = paramvals_gas(npar, chosen, dims=dims, copy=marg)


;;; PRIORS
; First assumption: uniform priors for all parameters, in NATURAL LOG space
    ; Ms: use an IMF?  Not much Ms dependancy for fluxes.
    ; Mg: any distribution we know of?
    ; Some Gaussian peaked at 1?  pri2 = -(chosen.g - 1d)^2 / .5
pri = {Ms:0d, Mg:0d, g:0d, Rin:0d, Rc:0d, Tm1:0d, qm:0d, Ta1:0d, qa:0d, Npd:0d, Tf:0d, in:0d}

priors = dblarr(nchosen)
for i=0, npar-1 do if n_elements(pri.(i)) eq nchosen then priors = priors + pri.(i)


;;; LIKELIHOODS
likelihoods = dblarr(nchosen)
for i=0, niso-1 do begin                         ; Loop over the input line fluxes
    modelflux = chosen.(iso[i]+13).(trans[i]-1)[thin-1]
    likes = likelihood_gas(flux[i], uncert[i], modelflux, thresh[i])
    ; Combine the likelihoods from different lines.
    likelihoods = likelihoods + likes
endfor


;;; POSTERIORS
posteriors = posterior_gas(likelihoods, priors, num=num, plim=plim)


;;; MARGINALIZING
; Marginalize to find the posterior PDF for each parameter.
for i=0, npar-1 do begin
    for j=0, dims[i]-1 do begin
        marg.(i)[j] = total(posteriors[where(chosen.(i) eq pvals.(i)[j])])
    endfor
endfor

; Marginalize the dissociated and frozen-out fractions.
freeze_1 = where(chosen.freeze.f eq 1)
if freeze_1[0] ge 0 then chosen[freeze_1].freeze.f = .9999
dissoc_1 = where(chosen.dissoc.f eq 1)
if dissoc_1[0] ge 0 then chosen[dissoc_1].dissoc.f = .9999

for i=0, 9 do begin
    marg.freeze[i] = total(posteriors[where((chosen.freeze.f ge i/10.) and (chosen.freeze.f lt (i+1)/10.))])
    marg.dissoc[i] = total(posteriors[where((chosen.dissoc.f ge i/10.) and (chosen.dissoc.f lt (i+1)/10.))])
endfor


;;; COMPUTE SUMMARY QUANTITIES
mgasvals = alog10(pvals.(1))   ; Convert masses to log space
maxp = max(marg.(1), maxp_ind)      ; Maximum posterior value
meangas = total(mgasvals * marg.(1))
meangasu = sqrt(total((mgasvals-meangas)^2 * marg.(1))) ; rms uncertainty
; Summary: {most likely Mgas bin, max. posterior, Nmatch, meangas, meangasu}
sumvals = {maxbin:mgasvals[maxp_ind], maxp:maxp, nmatch:num, expec:meangas, unc:meangasu}


;;; PLOT HISTOGRAMS
if not keyword_set(noplot) then $
  gas_plot_allparams, iso, trans, flux, npar, dims, pvals, marg, mgasvals, title=title, $
                   colors=colors, conline=conline, meanbar=meanbar, outpath=outpath, ps=ps, /fill


;;; Print summary Mgas information to the screen
if not keyword_set(silent) then begin
    print
    ;print, 'Most likely log(Mgas) = '+trim(mgasvals[maxp_ind], '(f4.1)')+' Msun'
    ;print, '    Maximum posterior = '+trim(100*maxp, '(f4.1)')+'%'
    print, ' Most likely Mgas bin = '+trim(10^(4+mgasvals[maxp_ind]))+' x 10^-4 Msun'
    print, '    Maximum posterior = '+trim(100*maxp, '(f4.1)')+'%'
    print
    print, 'Number of matches with Posterior > '+trim(10^plim)+':  '+trim(num)
    print
    print, '   Expected log(Mgas) = '+trim(meangas, '(f5.2)')+' +/- '+trim(meangasu, '(f4.2)')+' Msun'
    print, '        Expected Mgas = '+trim(10^(4+meangas), '(g3.2)')+' x 10^-4 Msun'
    print
endif


END
