PRO BAYESSELFPLOTS2_GAS, PLOTDATA, PARAM, OUTPATH=outpath, PS=ps, ZOOM=zoom

;+
;  Called by bayesself2_gas.pro.
;
;  Makes plots indicating the accuracy of the Bayesian fitting to
;  model-generated fluxes:
;  1) A histogram of abs(log Mgas – <log Mgas>), stacked by model gas mass
;     (colors).
;  2) A scatter plot of abs(log Mgas – <log Mgas>), plotted on flux1 vs. flux0
;     axes. 
;  3) If the ZOOM keyword is set, a second scatter plot of 
;     abs(log Mgas – <logMgas>) with xrange=[0, zoom[0]] and yrange=[0, zoom[1]].
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-30
;
;  INPUTS
;      PLOTDATA - path to the IDL save file containing the data to plot.
;                 Default: ~/Astro/699-2/results/bayesself2.sav
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/bayesself2.eps
;      PS - send output to postscript
;      ZOOM - Make a second scatter plot, with xrange=[0, zoom[0]] and
;             yrange=[0, zoom[1]].
;-

; Paramter to plot (default is Mgas)
if n_elements(param) eq 0 then param = 2

;;; LOAD THE STRUCTURE CONTAINING THE DATA TO PLOT
if n_elements(plotdata) eq 0 then plotdata = '~/Astro/699-2/results/bayesself2'
restore, plotdata+'.sav'
if param eq 2 then modelvals = alog10(modelvals)

; Number of data points to plot
nflux = n_elements(outvals)


;;; PLOT THE STACKED HISTOGRAM
; Get the label details for the specific disk parameter.
gas_labels_histo, param-1, pname
trlab = ['J=1-0', 'J=2-1', 'J=3-2']
islab = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
thlab = ['Optically thick emission', 'Optically thin emission', 'Total emission']

lab0 = islab[iso[0]-1]+' '+trlab[trans[0]-1]
lab1 = islab[iso[1]-1]+' '+trlab[trans[1]-1]
label = '('+lab0+') & ('+lab1+')'
;lchar = 

; Determine the axes
diffarr = abs(modelvals - outvals.expec)
binsize = 0.05
xlab1 = '!7D!X<log M!Dgas!N>'

ylab1 = 'Number of model disks'

; Plot the histogram
gas_plot_hist_stacks, diffarr, modelvals, binsize, /fill, lchar=lchar, label=label, outpath=outpath, $
      ps=ps, xtitle=xlab1, ytitle=ylab1, charsize=chars, backg=1, color=0


;;; PLOT THE SCATTER PLOT(S)
; Set up the axes
xlab2 = lab0+'  Jy km s!U-1!N'
ylab2 = lab1+'  Jy km s!U-1!N'

; Plot the scatter plot(s)
gas_plot_diff_scat2, diffarr, flux, modelvals, label=label, lchar=lchar, outpath=outpath, zoom=zoom, $
      ps=ps, xtitle=xlab2, ytitle=ylab2, backg=1, color=0


END
