PRO BAYESCORE2_GAS, CHOSEN, ISO, TRANS, FLUX, UNCERT, THRESH, PRIORS, PMAX, P2, PARAM, $
                    THIN=thin

;+
;  Returns the mean and standard deviation for the parameter indicated by PARAM 
;  over all of the disks matching the given fluxes and uncertainties for two
;  specified emission lines.
;
;  This program is designed to be called by bayesloop2_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-11-11
;
;  INPUTS
;      CHOSEN - Structure containing disk data to match to line flux(es).
;      ISO - 2-element vector indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - 2-element vector indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - 2-element vector containing the flux, in Jy km s-1, of each line to
;             be matched to the disk models.  Entries correspond to those in ISO
;             and TRANS.  
;      UNCERT - 2-element vector containing the uncertainty in flux of each
;               line to be matched to the disk models.  Entries correspond to
;               those in ISO, TRANS, and FLUX.
;      THRESH - Scalar equal to detection threshold for line fluxes.  Fluxes
;               less than this value will be considered non-detections.
;               Default: 0.
;      PRIORS - Vector of the same length as chosen, containing the gas mass priors.
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      PMAX - Value of highest posterior.
;      P2 - Value of highest posterior.
;
;  KEYWORDS
;      THIN - Match the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO, TRANS, FLUX, and UNCERT must be equal length
niso = n_elements(iso)
if (niso ne 2) or (n_elements(trans) ne niso) or (n_elements(flux) ne niso) $
  or (n_elements(uncert) ne niso) then begin
    print
    print, 'Use:  bayescore2_gas, CHOSEN, ISO, TRANS, FLUX, UNCERT, THRESH, PRIORS, PMAX, P2, PARAM'
    print, '                     [, thin=thin]'
    print
    print, '    ISO, TRANS, FLUX, and UNCERT must be two-element vectors.'
    print
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif

if not keyword_set(thin) then thin = 3


;;; LIKELIHOODS
likelihoods = dblarr(nchosen)
for i=0, niso-1 do begin                         ; Loop over the input line fluxes
    modelflux = chosen.(iso[i]+13).(trans[i]-1)[thin-1]
    likes = likelihood_gas(flux[i], uncert[i], modelflux, thresh)
    ; Combine the likelihoods from different lines.
    likelihoods = likelihoods + likes
endfor


;;; POSTERIORS
; Apply Bayes' Theorem (in log space) to find the posterior PDF.
posteriors = likelihoods + priors

; Find the sum of the posteriors over the entire grid.
pos_total = alog(total(exp(posteriors)))

; Use this sum to normalize the grid and get the final PDF.
posteriors = posteriors - pos_total

; Convert back to normal space
posteriors = exp(posteriors)


;;; MARGINALIZING
p = param - 1
pvals = chosen[uniq(chosen.(p), sort(chosen.(p)))].(p)
dim = n_elements(pvals)
marg = fltarr(dim)

; Marginalize to find the posterior PDF for the gas mass.
for j=0, dim-1 do begin
    marg[j] = total(posteriors[where(chosen.(p) eq pvals[j])])
endfor


;;; FIND THE MAXIMUM AND SECOND-HIGHEST POSTERIOR
margsort = marg[sort(marg)]
pmax = margsort[dim-1]
p2 = margsort[dim-2]


END
