PRO BAYESSCAT_GAS, PLOTDATA, PARAM, OUTPATH=outpath, PS=ps

;+
;  Called by bayesgrid_gas.pro.
;
;  Makes a plot indicating the number of matches with posterior above the value
;  PLIM, and the expected Mgas and uncertainty for each flux.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-25
;  2013-11-27 (WB): Added PLOTDATA parameter, separate from outpath.
;
;  INPUTS
;      PLOTDATA - path to the IDL save file containing the data to plot.
;                 Default: ~/Astro/699-2/results/bayesgrid.sav
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  KEYWORDS
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/bayesgrid.eps
;      PS - send output to postscript
;-

; Paramter to plot (default is Mgas)
if n_elements(param) eq 0 then param = 2


;;; LOAD THE STRUCTURE CONTAINING THE DATA TO PLOT
if n_elements(plotdata) eq 0 then plotdata = '~/Astro/699-2/results/bayesgrid.sav'
restore, plotdata

; Number of data points to plot
nflux = n_elements(outvals)


;;; PLOT
; Set up for plotting
device, decomposed=0
lincolr_wb, /silent
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = '~/Astro/699-2/results/bayesgrid'
    ps_open, outpath, /color, /en, thick=4
    lchar = 1.2
    chars = 1.5
endif else begin
    window, 1, retain=2, xsize=800, ysize=600
    lchar = 1.6
    chars = 2
endelse

; Color scheme and symbol size
size = 92./nflux

; Get the label details for the specific disk parameter.
gas_labels_histo, param-1, pname
trlab = ['J=1-0', 'J=2-1', 'J=3-2']
islab = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
thlab = ['Optically thick emission', 'Optically thin emission', 'Total emission']

label = 'Flux ('+islab[iso-1]+' '+trlab[trans-1]+')'

;outvals = {maxbin:mgasvals[maxp_ind], maxp:maxp, nmatch:num, expec:meangas, unc:meangasu}

; Determine the x-axis
binsize = (max(flux.(0))-min(flux.(0))) / nflux
xspan = [min(flux.(0))-binsize/2., max(flux.(0))+binsize/2.]

; Label for the left-hand y-axis
ylab1 = 'Number of matches with P>'+trim(10^plim)

; Plot the histogram-style Nmatches
;gas_plot_hist, flux.(0), outvals.nmatch, binsize, 0, charsize=chars, backg=1, thick=3, $
;    color=0, xrange=xspan, /xstyle, xtit=label
plot, flux.(0), outvals.nmatch, psym=10, color=0, backg=1, thick=4, $
      charsize=chars, xtit=label, ytit=ylab1, xrange=xspan, /xstyle, xmarg=[10,12], ystyle=8

; Set up the right-hand y-axis
yspan = [0, max(outvals.unc)]
ylab2 = '!7r!X!D<log M!Igas!D>!N'
axis, yaxis=1, ytit=ylab2, color=0, charsize=chars, yrange=yspan, /save

; Overplot the mean masses and uncertainties
cubehelix, rots=-1.2
for i=0, nflux-1 do plots, flux.(0)[i], outvals[i].unc, psym=cgsymcat(14), symsize=size, $
                           color=80*outvals[i].expec+320

; Plot the colorbar
cgcolorbar, ncolors=200, /vert, /right, tickn=trim([-4, -3.5, -3, -2.5, -2, -1.5]), $    ; make the colorbar on the right
            chars=1, pos=[0.93, 0.045, 0.95, 0.915], tickint=40
xyouts, 0.94, 0.94, 'log M!Dgas!N', color=0, align=0.5, charsize=0.8*chars, /normal

if keyword_set(ps) then ps_close


END
