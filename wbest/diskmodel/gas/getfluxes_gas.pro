PRO GETFLUXES_GAS, MSTAR, MGAS, GAMMA, RIN, RC, TM1, QM, TA1, QA, NPD, TFREEZE, INCL, CHOSEN, $
                   GAS=gas, FILEPATH=filepath, THIN=thin, SILENT=silent

;+
;  Script to find and print the fluxes for a specified model disk.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-14
;
;  INPUTS
;    The following inputs are used to specify values for each disk paramter.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;
;  OUTPUTS
;      CHOSEN - The structure containing the data for the chosen model disk.
;
;  KEYWORDS
;      GAS - Can be either the "gas" structure containing the data to plot, or
;            the name of an IDL save file containing the data structure.  If the
;            keyword is not supplied, the program will load 'gas.sav' from
;            FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas/
;      THIN - Get the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;      SILENT - Suppress screen output.
;-

npar = 12

;;; GET THE DATA
if size(gas, /type) ne 8 then begin
    if not keyword_set(filepath) then filepath='~/Astro/699-2/results/biggas/'
    if not keyword_set(gas) then gas='gas.sav'
    restore, filepath+gas
endif


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin)

; Identify the disk with the requested parameter values, and save its index
; in a variable called 'select'.
select = gas_select_disks(gas, keys, npar+2)

; Choose the disk with the requested parameter values
chosen = gas[select]

; Vectors containing information for printing to screen
islab = ['12CO', '13CO', 'C18O']
trlab = ['1-0', '2-1', '3-2']

; Print to screen
if not keyword_set(silent) then begin
    print
    for i=0, 2 do begin
        for j=0, 2 do begin
            label = 'f('+islab[i]+' '+trlab[j]+')'
            print, label+' = '+trim(chosen.(i+14).(j)[thin-1])+' Jy km/s'
        endfor
        print
    endfor
endif

END

