PRO MATCHLOOP2_GAS, ISO, TRANS, FLUX, UNCERT, PARAM, MEANGASARR, MEANGASUARR, NMATCHARR, GAS=gas, $
                    BASENUM=basenum, FILEPATH=filepath, LIST=list, OUTPATH=outpath, PS=ps, SUBLIM=sublim, $
                    MSTAR=mstar, MGAS=mgas, GAMMA=gamma, RIN=rin, RC=rc, TM1=tm1, QM=qm, $
                    TA1=ta1, QA=qa, NPD=npd, TFREEZE=tfreeze, INCL=incl, _EXTRA=extrakey
;+
;  Gets the means and standard deviations for the parameter indicated by PARAM 
;  over all of the disks matching arrays of fluxes and uncertainties for two
;  specified emission lines.
;  Makes a plot indicating the mean and uncertainty of the matched disk
;  parameter values for each pair of fluxes.
;
;  If specific values for various parameters are supplied (via keyword),
;  then the means and stddevs will incorporate only disks with those parameter
;  values.
;
;  If no value is specified for a given parameter, then the means and stddevs
;  will include disks with all values of that parameter (that have the values
;  given for other parameters).
;  
;  This program calls matchcore_gas.pro.
;  
;  HISTORY
;  Written by Will Best (IfA), 2013-10-17
;
;  INPUTS
;      ISO - 2-element vector indicating the CO isotopologue of each emission
;            line.
;            1 : 12CO
;            2 : 13CO
;            3 : C18O
;      TRANS - 2-element vector indicating the CO rotational transition of
;              each emission line.  
;              1 : (1-0)
;              2 : (2-1)
;              3 : (3-2)
;      FLUX - Structure of two vectors (can be one-element) containing the fluxes,
;             in Jy km/s, of each line to be matched to the disk models.
;             Format: { line_1:[fluxes], line_2:[fluxes] }
;             Vectors correspond to entries in ISO and TRANS.  
;      UNCERT - Structure of two vectors (can be one-element) containing the
;               uncertainties in flux of each line to be matched to the disk
;               models.  Entries correspond to those in FLUX, using the same 
;               format as FLUX. 
;      PARAM - Find the mean and standard deviation for this parameter.
;                 1 - M_star
;                 2 - M_gas
;                 3 - gamma
;                 4 - R_in
;                 5 - R_c
;                 6 - T_m1
;                 7 - q_m
;                 8 - T_a1
;                 9 - q_a
;                10 - N_pd
;                11 - T_freeze
;                12 - inclination
;              DEFAULT: 2 - M_gas
;
;  OUTPUTS
;      MEANGASARR - Array of mean PARAM values for the matched disks.
;      MEANGASUARR - Array of standard deviations of PARAM for the matched disks.
;    To return mean and log(PARAM value) for the matched disks, set the /LOG flag.
;      NMATCHARR - Array of number of matched disks for each line flux.
;
;  KEYWORDS
;      GAS - "gas" structure containing the data to plot.  If none is supplied, the
;             program will load 'gas.sav' from FILEPATH.
;      BASENUM - Base value for size of plotted points.  Default: 400.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas
;      LIST - Print all the flux0-flux1-nmatch-mean-stddev combinations to the screen.
;      OUTPATH - path for output postscript file.
;                Default: ~/Astro/699-2/results/biggas/matchloop.eps
;      PS - send output to postscript
;      SUBLIM - only plot points for which the uncertainty is less than the
;               limiting case of sigma = 0.25.
;      LOG - Compute and return mean and standard deviation in log space.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used to make the plot.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used to make the plots.
;      MSTAR - M_star
;      MGAS - M_gas
;      GAMMA - gamma
;      RIN - R_in
;      RC - R_c
;      TM1 - T_m1
;      QM - q_m
;      TA1 - T_a1
;      QA - q_a
;      NPD - N_pd
;      TFREEZE - T_freeze
;      INCL - inclination
;      THIN - Plot the optically thick or thin flux?
;              1 : Thick
;              2 : Thin
;              3 : Total = Thick + Thin
;              Default = 3 (Total)
;-

;;; DETERMINE THE PARAMETER FOR WHICH TO FIND MEAN AND STANDARD DEVIATION
if n_elements(param) eq 0 then param = 2    ; Default PARAM is gas mass

; Check input parameter
npar = 12
if param lt 1 or param gt npar then begin
    print, 'Invalid parameter chosen.'
    return
endif


;;; CHECK THE EMISSION LINES AND FLUXES TO MATCH TO DISKS
; Check input parameters:  ISO and TRANS must be two-element vectors.
;                          FLUX and UNCERT must be structures with two tags,
;                          which are vectors of matching lengths.
errflag = 0
niso = n_elements(iso)
if (niso ne 2) or (n_elements(trans) ne niso) then errflag = 1

t_flux = n_tags(flux)
t_uncert = n_tags(uncert)
if (t_flux ne 2) or (t_flux ne t_uncert) then errflag = 1

for i=0, ((t_flux-1) < (t_uncert-1)) do begin
    if n_elements(flux.(i)) ne n_elements(uncert.(i)) then errflag = 1
endfor

if errflag then begin
    print
    print, 'Use:  matchloop2_gas, ISO, TRANS, FLUX, UNCERT, MEANGASARR, MEANGASUARR, PARAM [, gas=gas,'
    print, '           basenum=basenum, filepath=filepath, list=list, outpath=outpath, ps=ps, log=log, sublim=sublim, '
    print, '           mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, tm1=tm1, qm=qm, ta1=ta1, qa=qa,'
    print, '           npd=npd, tfreeze=tfreeze, incl=incl, thin=thin]'
    print, '    ISO and TRANS must be two-element vectors.'
    print, '    FLUX and UNCERT must be structures with two tags, which are vectors of matching lengths.'
    print
    return
endif

; Check input parameters:  ISO and TRANS must be integers 1, 2, 3
if (max(iso) gt 3) or (min(iso) lt 1) or (size(iso, /type) ne 2) then begin
    print, 'ISO can only contain the values 1, 2 and 3.'
    return
endif
if (max(trans) gt 3) or (min(trans) lt 1) or (size(trans, /type) ne 2) then begin
    print, 'TRANS can only contain the values 1, 2 and 3.'
    return
endif


;;; GET THE DATA
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/gas/'
if not keyword_set(gas) then gas='gas.sav'
restore, filepath+gas


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Create a structure containing the various parameters values from the keywords
keys = gas_create_key_structure(mstar=mstar, mgas=mgas, gamma=gamma, rin=rin, rc=rc, $
             tm1=tm1, qm=qm, ta1=ta1, qa=qa, npd=npd, tfreeze=tfreeze, incl=incl, iso=iso, $
             trans=trans, thin=thin, valset=valset)

; Identify the disks with the requested parameter values, and save their indices
; in a vector called 'select'.
select = gas_select_disks(gas, keys, npar+2)

; Choose the disks with the requested parameter values
chosen = gas[select]


;;; GET MEAN AND STANDARD DEVIATION FOR THE CHOSEN PARAMETER, FOR THE DISKS THAT 
;;; MATCH THE FLUXES WITHIN THE UNCERTAINTIES 
nmean0 = n_elements(flux.(0))
nmean1 = n_elements(flux.(1))
meangasarr = fltarr(nmean0, nmean1)
meangasuarr = meangasarr
nmatcharr = lonarr(nmean0, nmean1)

for i=0, nmean0-1 do begin
    ;print, trim(i+1)+' out of '+trim(nmean0)
    for j=0, nmean1-1 do begin
        matchcore2_gas, chosen, iso, trans, [flux.(0)[i], flux.(1)[j]], [uncert.(0)[i], uncert.(1)[j]], $
                   meangas, meangasu, nmatch, param, _extra=extrakey
        meangasarr[i,j] = meangas
        meangasuarr[i,j] = meangasu
        nmatcharr[i,j] = nmatch
    endfor
endfor


;;; PLOT
; Set up for plotting
device, decomposed=0
if keyword_set(ps) then begin
    if n_elements(outpath) eq 0 then outpath = ' ~/Astro/699-2/results/biggas/matchloop2'
    ps_open, outpath, /color, /en, thick=4
    lchar = 1.2
endif else begin
    window, 0, retain=2, xsize=800, ysize=700
    lchar = 1.6
endelse

; Color scheme and symbol size
siglimit = .25
red_purple_blue_colorbar, white, black, nlev
colind = (nlev-1)/2. / siglimit
if n_elements(basenum) eq 0 then basenum = 400
basenum = float(basenum)
sizeind = 1 / basenum

; Scale -- needed in case of flux vectors of different length
xr = [0, max(flux.(0))]
yr = [0, max(flux.(1))]

; Get the label details for the specific disk parameter.
gas_labels_histo, param-1, pname
trlab = ['J=1-0', 'J=2-1', 'J=3-2']
islab = [textoidl('^{12}CO'), textoidl('^{13}CO'), textoidl('C^{18}O')]
thlab = ['Optically thick emission', 'Optically thin emission', 'Total emission']

label0 = 'Flux ('+islab[iso[0]-1]+' '+trlab[trans[0]-1]+')'
label1 = 'Flux ('+islab[iso[1]-1]+' '+trlab[trans[1]-1]+')'

; Make the plot
plot, flux.(0), flux.(1), charsize=1.6, backg=white, color=black, /nodata, $
      xtit=label0, ytit=label1, xrange=xr, yrange=yr, xstyle=1, ystyle=1
for i=0, nmean0-1 do begin
    for j=0, nmean1-1 do begin
        if not (keyword_set(sublim) and (meangasuarr[i,j] ge siglimit)) then $
          plots, flux.(0)[i], flux.(1)[j], color=(colind*meangasuarr[i,j] < (nlev-1)), $
                 psym=cgsymcat(14), symsize=(sizeind*nmatcharr[i,j] > 0.05)
        ; Print the flux0-flux1-nmatch-mean-stddev combinations to screen
        if keyword_set(list) then begin
            if ((i eq 0) and (j eq 0)) then print, '       Flux0        Flux1       N_match        Mean        StdDev'
            print, flux.(0)[i], flux.(1)[j], nmatcharr[i,j], meangasarr[i,j], meangasuarr[i,j]
        endif
    endfor
endfor

; Legend
leg = [pname, thlab[thin-1], textoidl('\sigma > '+trim(siglimit))+' dex', textoidl('\sigma < '+trim(siglimit))+' dex', $
       trim(basenum)+' matches', trim(2.5*basenum)+' matches']
leg_tcol = [black, black, 180, 20, black, black]
leg_syms = [0, 0, sizeind*1.8*basenum, sizeind*1.8*basenum, sizeind*basenum, sizeind*2.5*basenum]
leg_symc = [white, white, 180, 20, black, black]
legend, leg, outline_color=black, textcolor=leg_tcol, psym=14, symsize=leg_syms, $
        colors=leg_symc, charsize=lchar, _extra=extrakey

if keyword_set(ps) then ps_close


END
