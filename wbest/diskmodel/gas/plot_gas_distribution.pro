@readradmc.pro

pro plot_gas_distribution,ps=ps
;
; read in the density and temperature files
; from radmc3d modeling
; and plot the cumulative distribution
; of mass fraction with density and temperature
;
;  3/27/13  jpw
;
; --------------------------------------------------------
@natural_constants.pro
dilute_dust_factor = 1d6

;;; use canned radmc3d procedure to read in the
;;; dust_density.inp and dust_temperature.dat files
;;; --> these are actually gas density and temperature
;;; --> and I multiply back the big dilution factor
;;; --> that I put in make_gas_profile
;
struct = read_data(/ddens,/dtemp)

;;; convert from nominal dust to gas density
;;; by rescaling by the factor input in make_gas_profile.pro
struct.rho = struct.rho * dilute_dust_factor


;;; spherical coordinate axes
;
r  = struct.grid.r
nr = struct.grid.nr
t  = struct.grid.theta
nt = struct.grid.ntheta

;;; calculate total mass
;
Sigma = dblarr(nr)
for ir = 0,nr-1 do begin
  z = reverse(r[ir]/tan(t))
  rho_r = reverse(reform(struct.rho[ir,*]))
  Sigma[ir] = int_tabulated(z,rho_r,/double)
endfor
m_total = 2*int_tabulated(r,2*!pi*r*Sigma)

;;; calculate density distribution
;
nh2_min = 1d2
nh2_max = 1d14
n_nh2 = 25
nh2 = nh2_min*(nh2_max/nh2_min)^(dindgen(n_nh2)/float(n_nh2-1))
rho_g = nh2 * 2.364 * mp
frac_nh2 = dblarr(n_nh2)
for n = 0,n_nh2-1 do begin
  rho = struct.rho
  pix = where(rho gt rho_g[n], npix)
  if (npix gt 0) then rho[pix] = 0.0d
  Sigma = dblarr(nr)
  for ir = 0,nr-1 do begin
    z = reverse(r[ir]/tan(t))
    rho_r = reverse(reform(rho[ir,*]))
    Sigma[ir] = int_tabulated(z,rho_r,/double)
  endfor
  frac_nh2[n] = 2*int_tabulated(r,2*!pi*r*Sigma)/m_total
endfor

;;; find median
;
ind = where(frac_nh2 lt 0.5)
i = ind[n_elements(ind)-1]
xmin = nh2[i]
xmax = nh2[i+1]
x = xmin*(xmax/xmin)^(dindgen(10)/9.)
for n = 0,9 do begin
  f = interpol(frac_nh2,nh2,x)
endfor
dummy = min((f-0.5)^2,imin)
nh2_median = x[imin]
frac_nh2_median = interpol(frac_nh2,nh2,nh2_median)
print,format='("Median density = ",e8.2," cm-3")',nh2_median


;;; calculate temperature distribution
;
temp_min = 5.0
temp_max = 500.0
n_temp = 11
temp = temp_min * (temp_max/temp_min)^(findgen(n_temp)/float(n_temp-1))
frac_temp = dblarr(n_temp)
for n = 0,n_temp-1 do begin
  rho = struct.rho
  pix = where(struct.temp gt temp[n], npix)
  if (npix gt 0) then rho[pix] = 0.0d
  Sigma = dblarr(nr)
  for ir = 0,nr-1 do begin
    z = reverse(r[ir]/tan(t))
    rho_r = reverse(reform(rho[ir,*]))
    Sigma[ir] = int_tabulated(z,rho_r,/double)
  endfor
  frac_temp[n] = 2*int_tabulated(r,2*!pi*r*Sigma)/m_total
endfor

;;; find median
;
ind = where(frac_temp lt 0.5)
i = ind[n_elements(ind)-1]
xmin = temp[i]
xmax = temp[i+1]
x = xmin*(xmax/xmin)^(dindgen(10)/9.)
for n = 0,9 do begin
  f = interpol(frac_temp,temp,x)
endfor
dummy = min((f-0.5)^2,imin)
temp_median = x[imin]
frac_temp_median = interpol(frac_temp,temp,temp_median)
print,format='("Median temperature = ",i0," K")',temp_median


;;; set up plot page
;
aspect_ratio=0.5
if keyword_set(ps) then begin
  set_plot,'ps',/interpolate
  device,filename='idl.ps',bits_per_pixel=8 $
        ,xsize=11.0,ysize=11.0*aspect_ratio,xoff=0.0,yoff=0.0 $
        ,/inches,/encapsulated,/color
  cs=1.2
  ct=3
endif else begin
  device,decomposed=0
  xsize=1000
  window,0,xsize=xsize,ysize=xsize*aspect_ratio
  cs=1.7
  ct=1
endelse
pos1 = [0.10,0.2,0.5,0.85]
pos2 = [0.55,0.2,0.95,0.85]
tic=0.04

ncol=250
loadct,0,ncolors=ncol
tvlct,fsc_color('Red',/triple),251
tvlct,fsc_color('Grey',/triple),252
tvlct,fsc_color('Green',/triple),253
tvlct,fsc_color('Royal Blue',/triple),254
tvlct,fsc_color('White',/triple),255
red=251
grey=252
green=253
blue=254
white=255
black=0

plot,nh2,frac_nh2 $
    ,position=pos1 $
    ,xra=[1d4,1d13],xsty=1,/xlog $
    ,yra=[0,1],ysty=1 $
    ,xtitle='!8n!6(H!D2!N) [cm!U-3!N]' $
    ,ytitle='Cumulative mass fraction' $
    ,thick=ct+2 $
    ,xthick=ct,ythick=ct $
    ,charthick=ct,charsize=cs
oplot,[nh2_median,nh2_median],[0,frac_nh2_median],linestyle=1
oplot,[1d4,nh2_median],[frac_nh2_median,frac_nh2_median],linestyle=1

plot,temp,frac_temp $
    ,position=pos2 $
    ,xra=[5,500],xsty=1,/xlog $
    ,yra=[0,1],ysty=1 $
    ,ytickname=replicate(' ',10) $
    ,xtitle='!6T!Dgas!N [K]' $
    ,thick=ct+2 $
    ,xthick=ct,ythick=ct $
    ,charthick=ct,charsize=cs $
    ,/noerase
oplot,[temp_median,temp_median],[0,frac_temp_median],linestyle=1
oplot,[1,temp_median],[frac_temp_median,frac_temp_median],linestyle=1

if keyword_set(ps) then begin
  device,/close
  set_plot,'x'
  !p.font=-1
  print,"Created color postscript file: idl.ps"
endif



end
; --------------------------------------------------------
