FUNCTION GETDATA_GAS, FILEPATH=filepath, GASNAME=gasname

;+
; Returns the "gas" structure containing the data from the model disks.
;
; HISTORY
; Written by Will Best (IfA), 2013-11-16
;
; KEYWORDS
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/biggas/
;      GASNAME - name of the IDL save file containing the "gas".  If none is
;                supplied, the program will load 'gas.sav' from FILEPATH.
;-

; Check inputs
if(n_params()) gt 0 then begin
    print
    print, 'Use:  result = getdata_gas([filepath=filepath, gasname=gasname])'
    print
    return, 0
endif

; Get the array of structures containing the data
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/biggas/'
if not keyword_set(gasname) then gasname = 'gas.sav'
restore, filepath+gasname

return, gas

END
