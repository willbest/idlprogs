pro make_dust_profile,star=star,disk=disk,fine=fine
;
; grid up an exponentially tapered disk profile
; for input to radmc3d to fit the SED and SMA visibilities
; borrowed much of the code from radmc-3d/examples/run_ppdisk_gui
;
; see ARAA article for details but allow an inner radius
; 6 parameters: Mdust, Rin, Rc, gamma, H100, h
; (but I fix Rin = Rsublime)
;
; Sigma(R) = C * (R/Rc)^-gamma / exp[(R/Rc)^(2-gamma)]
; C = (2-gamma) * Md * exp[(Rin/Rc)^(2-gamma)] / (2*pi*Rc^2)
; rho(R,Z) = Sigma(R) * exp(-Z^2/2H^2) / (sqrt(2*pi)*H)
; H(R) = H100*(R/100 AU)^h
;
; -----------------------------------------------------------
;@natural_constants.pro
@natconst.pro

;
; Monte Carlo parameters
;
nphot    = 100000
;nphot    = 50000
;
; Spherical grid parameters
;
nr       = 100L
nt       = 60L       ; One-sided only. So the "real" value is twice this.
nphi     = 1L

if keyword_set(fine) then begin
  nr = 200L
  nt = 120L
  nphi = 1L
endif

;
; Star parameters (input in star structure)
;
;;;mstar    = MS
;;;rstar    = RS
;;;tstar    = TS
pstar    = [0.,0.,0.]

; stellar radius for blackbody star
rstar = sqrt(star.l*LS/(4d*!dpi*ss))/star.t^2

;
; Write the wavelength_micron.inp file
;
lambda1 = 0.1d0
lambda2 = 2.0d0
lambda3 = 25.d0
lambda4 = 1.0d4
n12     = 10
n23     = 10
n34     = 20
;
lam12   = lambda1 * (lambda2/lambda1)^(dindgen(n12)/(1.d0*n12))
lam23   = lambda2 * (lambda3/lambda2)^(dindgen(n23)/(1.d0*n23))
lam34   = lambda3 * (lambda4/lambda3)^(dindgen(n34)/(1.d0*(n34-1.d0)))
lambda  = [lam12,lam23,lam34]
nlam    = n_elements(lambda)
;
; Write the wavelength file
;
openw,1,'wavelength_micron.inp'
printf,1,nlam
for ilam=0,nlam-1 do printf,1,lambda[ilam]
close,1

;
; Dust opacity control file
;
openw,1,'dustopac.inp'
printf,1,'2               Format number of this file'
printf,1,'1               Nr of dust species'
printf,1,'============================================================================'
printf,1,'1               Way in which this dust species is read'
printf,1,'0               0=Thermal grain'
printf,1,'andrews         Extension of name of dustkappa_***.inp file'
printf,1,'----------------------------------------------------------------------------'
close,1

;
; Write the radmc3d.inp control file
;
openw,1,'radmc3d.inp'
printf,1,'nphot = ',nphot
printf,1,'scattering_mode_max = 0'
printf,1,'istar_sphere = 1'
printf,1,'tgas_eq_tdust = 1'
printf,1,'lines_mode = 1'
close,1


; -----------------------------------------------------------
;
; Disk parameters - most are user input on command line
;
;;;mdust    = 1d-4*MS
mdust    = disk.m*MS

;;;rin      = 0.05*AU
; set inner radius to dust sublimation radius
tsublime = 1500.0d
rin      = 0.5*rstar*(star.t/tsublime)^2

;;;rc       = 100*AU
rc       = disk.rc*AU

; fix gamma based on Andrews et al. results
;;;gamma    = 1.0
gamma    = disk.gamma

;;;H100     = 10*AU
H100     = disk.H100*AU

;;;h        = 1.1
h        = disk.h
; -----------------------------------------------------------
 
;
; Create the coordinate system
;
; some hard-wired constraints on the grid
rout     = 500*AU
tmax     = 1.0d0
;
; grid coordinates: logarithmic in radius, linear in theta
r        = rin * (rout/rin)^(dindgen(nr+1)/(nr-1))
t        = rotate((!dpi/2.0 - !dpi/2.*tmax*dindgen(nt+1)/(nt*1.0)),2)
phi      = [0.,0.]
rcen     = 0.5d0 * (r[0:nr-1] + r[1:nr])
tcen     = 0.5d0 * (t[0:nt-1] + t[1:nt])

;
; Make the dust density model
;
rho = dblarr(nr,nt)
g2=2.d0-gamma
const = g2 * mdust * exp((rin/rc)^g2) / (2*!dpi*rc^2)
sigma = const * (rc/r)^gamma / exp((r/rc)^g2)
hr    = H100*(r/(100*AU))^h
for ir=0, nr-1 do begin
  if rcen[ir] gt rin and rcen[ir] le rout then begin
    sigR = sigma[ir] / (hr[ir] * sqrt(2d0*!dpi))
    expZ = exp(-0.5*(tan(!dpi/2. - tcen)*rcen[ir]/hr[ir])^2)
    rho[ir,*] = (sigR * expZ) > (1d-23)
  endif
endfor

;
; Write the grid file
;
openw,1,'amr_grid.inp'
printf,1,1                      ; iformat
printf,1,0                      ; AMR grid style  (0=regular grid, no AMR)
printf,1,100                    ; Coordinate system
printf,1,0                      ; gridinfo
printf,1,1,1,0                  ; Include x,y,z coordinate
printf,1,nr,nt,nphi             ; Size of grid
for i=0,nr do printf,1,r[i]     ; X coordinates (cell walls)
for i=0,nt do printf,1,t[i]     ; Y coordinates (cell walls)
for i=0,nphi do printf,1,phi[i] ; Z coordinates (cell walls)
close,1

;
; Write the density file
;
openw,1,'dust_density.inp'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
printf,1,1                      ; Nr of dust species
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,rho[i,j]
      endfor
   endfor
endfor
close,1

;
; Write the gas velocity field
;
vr = dblarr(nr,nt,nphi)
vtheta = dblarr(nr,nt,nphi)
vphi = dblarr(nr,nt,nphi)
for ir=0,nr-1 do vphi[ir,*,*] = sqrt(GG*MS*star.m/r[ir])
openw,1,'gas_velocity.inp'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,vr[i,j,k],vtheta[i,j,k],vphi[i,j,k]
      endfor
   endfor
endfor
close,1

;
; Write the microturbulence file
;
; small turbulent motion of 10 m/s
vturb0 = 1d3
vturb = dblarr(nr,nt,nphi) + vturb0
openw,1,'microturbulence.inp'
printf,1,1                      ; Format number
printf,1,nr*nt*nphi             ; Nr of cells
for k=0,nphi-1 do begin
   for j=0,nt-1 do begin
      for i=0,nr-1 do begin
         printf,1,vturb[i,j,k]
      endfor
   endfor
endfor
close,1

;
; Write the lines.inp control file
;
openw,1,'lines.inp'
printf,1,'2'
printf,1,'3'
printf,1,'co    leiden  0  0  0'
printf,1,'13co  leiden  0  0  0'
printf,1,'c18o  leiden  0  0  0'
close,1


return
end
