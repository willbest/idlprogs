PRO PLOT_TEMP, SIMS=sims, FILEPATH=filepath, PS=ps, DALESSIO=dalessio, _EXTRA=extrakey, $
                 LSTAR=lstar, TSTAR=tstar, MDISK=mdisk, GAMMA=gamma, RC=rc, H100=h100, $
                 HSCALE=hscale

;+
;  Reads the output temperature profiles from the make_cont_lines simulations,
;      which are saved as ~/Astro/699-2/results/temp/temp.sav
;  Plots them as a function of radius (distance from the central star) for the
;  disk with the properties specified by the keywords.
;  If multiple values are supplied for a parameter(s), the program will draw
;  plots for disks with all values of that parameter(s) on the same axes.
;  
;  HISTORY
;  Written by Will Best (IfA), 02/12/2013
;  06/13/2013 (WB): Adjusted for varying amr_grid files.
;                   Fixed bad plotting when radmc3d returned T=0 for a grid point.
;
;  KEYWORDS
;      SIMS - "sims" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then all its values are used to make plots on the same axes.
;      LSTAR - L_star    Default: 0.44 (Lsun)
;      TSTAR - T_star    Default: 3705 (K)
;      MDISK - M_disk    Default: 1e-4 (Msun)
;      GAMMA - gamma     Default: 0.5
;      RC - R_c          Default: 100 (AU)
;      H100 - H_100      Default: 10 (AU)
;      HSCALE - h        Default: 1.0
;-

; Potential variables
;gridfile = '~/Astro/699-2/results/amr_grid.inp'
Tfreeze = 20                   ; CO freeze-out temperature
col = [0, 4, 2, 11, 12, 3]     ; plotting colors

; get the data
if n_elements(sims) eq 0 then begin
    if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/temp/'
    restore, filepath+'temp.sav'
endif
tgrid = n_elements(temp[0].td)

; Check input values
print
print, 'Checking input values'
npar = 7
if not keyword_set(lstar) then lstar = 0.44
if not keyword_set(tstar) then tstar = 3705
if not keyword_set(mdisk) then mdisk = 1e-4
if n_elements(gamma) eq 0 then gamma = 0.5
if not keyword_set(rc) then rc = 100
if not keyword_set(h100) then h100 = 10
if not keyword_set(hscale) then hscale = 1.


;;; DETERMINE WHICH DISK(S) TO GET THE TEMPERATURES FROM.
print, 'Determining which model disk(s) to get temperatures from'

; Make a structure containing the values from the keywords
keys = {lstar:float(lstar), tstar:fix(tstar), mdisk:float(mdisk), gamma:float(gamma), $
        rc:fix(rc), h100:fix(h100), hscale:float(hscale)}

;;; Build a large string, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters
for i=0, npar-1 do begin
    match, temp[uniq(temp.(i), sort(temp.(i)))].(i), keys.(i), xx, ind ; where parameter matches keyword value(s)
    nind = n_elements(ind)
    if ind[0] ge 0 then begin   ; if parameter matches keyword value(s) at least once, go on
        if nind gt 1 then fstr = fstr + $
               '(' + strjoin('(temp.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
        fstr = fstr + '(temp.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
        if nind gt 1 then fstr = fstr + ')'
        fstr = fstr + ' and '
    endif
endfor


;;; GET THE DUST TEMPERATURE VALUES
print, 'Getting dust temperature values'

; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'  ; replace the last ' and ' with a )
    dummy = execute(fstr)
    chosen = temp[select]
    nplot = n_elements(chosen)
endif else begin
    print, 'The parameter values you entered do not match any disks.'
    return
endelse


;;; GET THE RADIUS VALUES
;; readcol, gridfile, radgrid, skipline=6, format='f', /silent
;; r = fltarr(tgrid)
;; for i=0, tgrid-1 do r[i] = mean(radgrid[i:i+1])
;; r = r/1.49598e13     ; convert to AU


;;; MAKE THE PLOT
print, 'Plotting the model disk temperatures'

; Load a color table
device, decomposed=0
lincolr_wb, /silent

if keyword_set(ps) then begin
    fpath = '~/Astro/699-2/results/tempplot'
    ps_open, fpath, /color, /en, thick=4
endif else begin
    window, 0, retain=2, xsize=800, ysize=600
endelse

good = where(chosen[0].Td gt 0)
plot, chosen[0].Rd[good], chosen[0].Td[good], color=col[0], background=1, charsize=1.8, _extra=extrakey, $
      xtit='Distance (AU)', ytit='Dust Temperature (K)' ;, psym=-4
hline, Tfreeze, lines=1, color=col[0], _extra=extrakey

xyouts, 0.768-0.18*(nplot-1), 0.87, 'L!Dstar!N = '+trim(chosen[0].L)+' L!D!9n!X!N', /normal, color=col[0], chars=1.1
xyouts, 0.768-0.18*(nplot-1), 0.84, 'T!Dstar!N = '+trim(chosen[0].T)+' K', /normal, color=col[0], chars=1.1
xyouts, 0.762-0.18*(nplot-1), 0.81, 'M!Ddust!N = '+trim(chosen[0].Md)+' M!D!9n!X!N', /normal, color=col[0], chars=1.1
xyouts, 0.788-0.18*(nplot-1), 0.78, textoidl('\gamma')+' = '+trim(chosen[0].g), /normal, color=col[0], chars=1.1
xyouts, 0.781-0.18*(nplot-1), 0.75, 'R!Dc!N = '+trim(chosen[0].Rc)+' AU', /normal, color=col[0], chars=1.1
xyouts, 0.766-0.18*(nplot-1), 0.72, 'H!D100!N = '+trim(chosen[0].H100)+' AU', /normal, color=col[0], chars=1.1
xyouts, 0.788-0.18*(nplot-1), 0.69, 'h = '+trim(chosen[0].h), /normal, color=col[0], chars=1.1

for i=1, nplot-1 do begin
    good = where(chosen[i].Td gt 0)
    oplot, chosen[i].Rd[good], chosen[i].Td[good], color=col[i], _extra=extrakey

    xyouts, 0.768-0.18*(nplot-1-i), 0.87, 'L!Dstar!N = '+trim(chosen[i].L)+' L!D!9n!X!N', /normal, color=col[i], chars=1.1
    xyouts, 0.768-0.18*(nplot-1-i), 0.84, 'T!Dstar!N = '+trim(chosen[i].T)+' K', /normal, color=col[i], chars=1.1
    xyouts, 0.762-0.18*(nplot-1-i), 0.81, 'M!Ddust!N = '+trim(chosen[i].Md)+' M!D!9n!X!N', /normal, color=col[i], chars=1.1
    xyouts, 0.788-0.18*(nplot-1-i), 0.78, textoidl('\gamma')+' = '+trim(chosen[i].g), /normal, color=col[i], chars=1.1
    xyouts, 0.781-0.18*(nplot-1-i), 0.75, 'R!Dc!N = '+trim(chosen[i].Rc)+' AU', /normal, color=col[i], chars=1.1
    xyouts, 0.766-0.18*(nplot-1-i), 0.72, 'H!D100!N = '+trim(chosen[i].H100)+' AU', /normal, color=col[i], chars=1.1
    xyouts, 0.788-0.18*(nplot-1-i), 0.69, 'h = '+trim(chosen[i].h), /normal, color=col[i], chars=1.1
endfor

;;; OVERPLOT A D'ALESSIO MODEL
if keyword_set(dalessio) then begin
    print, "Overplotting the D'Alessio model"

    readcol, '~/Astro/699-2/DAlessio/'+dalessio, dal_logR, dal_logTc, format='f,x,f', comment='#', /silent
    oplot, 10^dal_logR, 10^dal_logTc, color=3, thick=4, lines=2

end

if keyword_set(ps) then ps_close
print

END

