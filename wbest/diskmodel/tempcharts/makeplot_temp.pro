;PRO MAKEPLOT_TEMP

; Wrapper for plot_temp.pro
; Values for the parameters must be supplied explicitly.  If values for
; parameters are not supplied, defaults will be assumed.

lstar = 0.31;[.5, 1, 2, 5, 10]
;tstar = [4000]
;mdisk = 1d-4;[1d-6, 3d-6, 1d-5, 3d-5, 1d-4]
gamma = [0.];,1.]
rc = [150];, 50]
h100 = [10, 20]
hscale = [1.1, 1.0]

dalessio='dal_model1.ascii'
;ps=1

plot_temp, filepath='~/Astro/699-2/results/temp/', dalessio=dalessio, lstar=lstar, tstar=tstar, $
  mdisk=mdisk, gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, /ylog, /xlog

;  Reads the output temperature profiles from the make_cont_lines simulations.
;  Plots them as a function of radius (distance from the central star) for the
;  disk with the properties specified by the keywords.
;  If multiple values are suppliedfor for a parameter, the program will draw plots for disks with all values
;  of that parameter on the same axes.
;  
;  HISTORY
;  Written by Will Best (IfA), 02/12/2013
;
;  KEYWORDS
;      SIMS - "sims" structure containing the data to plot.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      PS - send output to postscript
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then all its values are used to make plots on the same axes.
;      LSTAR - L_star    Default: 0.44 (Lsun)
;      TSTAR - T_star    Default: 3705 (K)
;      MDISK - M_disk    Default: 1e-4 (Msun)
;      GAMMA - gamma     Default: 0.5
;      RC - R_c          Default: 100 (AU)
;      H100 - H_100      Default: 10 (AU)
;      HSCALE - h        Default: 1.0

END

