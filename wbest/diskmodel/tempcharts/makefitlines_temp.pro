;PRO MAKEFITLINES_TEMP

; Wrapper for fitlines_temp.pro

;lstar = 0.6
;tstar = 3705
;mdisk = [3e-5]
;gamma = [0.5, 1]
;rc = [100]
;h100 = [10]
;hscale = [1, 1.1]

fitlines_temp, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, $
  mdisk=mdisk, gamma=gamma, rc=rc, h100=h100, hscale=hscale;, /graph;, /ps

;  Reads the output temperature profiles from the make_cont_lines simulations,
;      ~/Astro/699-2/results/which are saved as /temp.sav
;  Fits a line to the log-log plots of temperature as a function of radius
;  (distance from the central star) for the disk(s) with the properties
;  specified by the keywords.
;       log(T) = a + b*log(R)       -- find a and b for each disk
;  If multiple values are supplied via keyword for a parameter, the program will
;  make fits for disks with all supplied values of that parameter.
;  If no value is supplied via keyword for a parameter, the program will
;  make fits for disks with all available values of that parameter.
;  
;  HISTORY
;  Written by Will Best (IfA), 02/23/2013
;
;  OUTPUTS (OPTIONAL)
;      OUTFIT - File to save the fits to.  Default is FILEPATH/tempfits.sav
;
;  KEYWORDS
;      SIMS - "sims" structure containing the data to fit.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      GRAPH - Make plot of temperature vs. radius for chosen disks.
;      PS - send plot to postscript.  If GRAPH is not set, PS is quietly ignored.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then all its values are used to make plots on the same axes.
;      LSTAR - L_star    Default: 0.44 (Lsun)
;      TSTAR - T_star    Default: 3705 (K)
;      MDISK - M_disk    Default: 1e-4 (Msun)
;      GAMMA - gamma     Default: 0.5
;      RC - R_c          Default: 100 (AU)
;      H100 - H_100      Default: 10 (AU)
;      HSCALE - h        Default: 1.0

END

