nsets=4
setnames='dal'
;start=3
;filepath='~/Astro/699-2/results/temp/dal1_output/'
;PRO READ_TEMP_DATA, FILEPATH=filepath, NSETS=nsets, SETNAMES=setnames

;+
;  Reads the output midplane dust temperature profiles from the make_temp
;  models.  Can read and combine the data from multiple sets of models.
;  Stores the results in an array of structures, named "temp", with the
;  following flags:
;      L = stellar luminosity (units of Lsun)
;      T = stellar temperature (K)
;      Md = dust mass (Msun)
;      g = gamma -- gradient parameter for disk surface density profile
;      Rc = characteristic radius of the disk (AU)
;      H100 = scale height of the disk (AU) at R = 100 AU
;      h = gradient parameter for disk scale height
;      Rd = vector of midplane radial grid points (AU)
;      Td = vector midplane dust temperature at each radial grid point (K)
;  Saves "temp" in two formats:
;      Text:           temp.txt
;      IDL save file:  temp.sav
;  
;  HISTORY
;  Begun by Will Best (IfA), 2/12/2013
;  05/28/2013 (WB): Added START keyword
;  05/29/2013 (WB): Adjusted for varying amr_grid files.
;                   Added Rd to the structure -- radial grid values for the midplane.
;  06/13/2013 (WB): Changed counter and nsim to long integer format
;                   Adjusted for amr_grid files of varying size.
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/temp/
;      NSETS - If multiple sets of models were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      SETNAMES - If multiple sets of models were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not given, SETNAMES is ignored.
;-

; Set up paths
if not keyword_set(start) then start = 1
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/temp/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1

; Get the number of disks in each set of models to be read, and number of radial
; grid points for each set.
nsim = intarr(nsets)
ngrid = nsim
if nsets gt 1 then begin
    for i=0, nsets-1 do begin
        readpath = filepath+setnames+trim(start+i)+'_output/'
        cd, readpath
        nsim[i] = file_lines('report.txt') - 1    ; get number of disks in the set
        readcol, 'amr_grid.inp', gridsize, skipline=5, numline=1, format='i'    ; get number of grid points in the set
        ngrid[i] = gridsize
    endfor
endif else begin
    readpath = filepath
    cd, readpath
    nsim[0] = file_lines('report.txt') - 1    ; get number of disks in the set
    readcol, 'amr_grid.inp', gridsize, skipline=5, numline=1, format='i'    ; get number of grid points in the set
    ngrid[0] = gridsize
endelse


;;; Create the structure containing the parameters and temperature profiles
;; ; get the number of radial midplane grid points from the local amr_grid.inp file
;; readcol, 'amr_grid.inp', tgrid, skipline=5, numline=1, format='i'
;; tgrid = tgrid[0]    ; convert from array to scalar
ntot = total(nsim, /integer)
tgrid = max(ngrid)
temp = replicate({L:0., T:0, Md:0., g:0., Rc:0, H100:0, h:0., Rd:fltarr(tgrid), Td:fltarr(tgrid)}, ntot)
rval = fltarr(tgrid)
tval = rval

;;; Loop over the sets of models and extract the data
counter = 0     ; running total of the number of models extracted
for i=0, nsets-1 do begin
    print, 'Loop #'+trim(start+i)
    if nsets gt 1 then readpath = filepath+setnames+trim(start+i)+'_output/'
    cd, readpath
    readcol, 'report.txt', run, star, disk, delim=' ', comment='#', format='i,a,a'

    ; Read the model parameter values from the report lines, and make vectors of the
    ; values for each parameter
    num = nsim[i]
    L = rebin(float(strmid(star, 1, 1#strpos(star, '_T')-1)), num, /sample)
    T = rebin(fix(strmid(star, 1#strpos(star, '_T')+2, 1#strpos(star, '/')-1#strpos(star, '_T')-2)), num, /sample)
    Md = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), num, /sample)
    g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rc')-1#strpos(disk, '_g')-2)), num, /sample)
    Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '_H')-1#strpos(disk, '_Rc')-3)), num, /sample)
    H100 = rebin(fix(strmid(disk, 1#strpos(disk, '_H')+2, 1#strpos(disk, '_h')-1#strpos(disk, '_H')-2)), num, /sample)
    h = rebin(float(strmid(disk, 1#strpos(disk, '_h')+2, 1#strpos(disk, '/')-1#strpos(disk, '_h')-2)), num, /sample)

    ; Store the parameter values in the "temp" structure
    temp[counter:counter+num-1].L = L
    temp[counter:counter+num-1].T = T
    temp[counter:counter+num-1].Md = Md
    temp[counter:counter+num-1].g = g
    temp[counter:counter+num-1].Rc = Rc
    temp[counter:counter+num-1].H100 = H100
    temp[counter:counter+num-1].h = h

    ; get the radial midplane grid values from the local amr_grid.inp file
    readcol, 'amr_grid.inp', radgrid, skipline=6, format='f', /silent
    ; this set of models may use a smaller grid than the maximum size used in other models
    for ii=0, ngrid[i]-1 do rval[ii] = mean(radgrid[ii:ii+1])
    rval = rval/1.49598e13            ; convert to AU
    if tgrid gt ngrid[i] then rval[ngrid[i]:*] = !VALUES.F_NAN
    temp[counter:counter+num-1].Rd = rval

    for j=0, num-1 do begin
        readcol, star[j]+disk[j]+'dust_temperature'+trim(run[j])+'.dat', $
                 dtemp, comment='%', format='f', skipline=3, /silent
        ntemp = n_elements(dtemp)
;        temp[j+counter].Td = dtemp[(ntemp-tgrid):ntemp-1]
        tval[0:ngrid[i]-1] = dtemp[(ntemp-ngrid[i]):ntemp-1]
        if tgrid gt ngrid[i] then tval[ngrid[i]:*] = !VALUES.F_NAN
        temp[j+counter].Td = tval
    endfor

    counter = counter + num
endfor

cd, filepath
forprint, temp, textout='temp.txt', width=135, /nocomment, /silent
save, temp, filename='temp.sav'

print
print, 'Done!!'
print

END

