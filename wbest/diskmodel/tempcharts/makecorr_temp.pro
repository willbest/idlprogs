;PRO MAKECORR_TEMP

; Wrapper for corr_temp.pro

;lstar = 0.6
;tstar = 3800
mdisk = [1e-6, 3e-6, 1e-5, 3e-5, 1e-4]
;gamma = [0., 0.5, 1.0]
;rc = 100
;h100 = 10
;hscale = 1
colmin=8
rowmax=7

;logcorr=1
ps=1

corr_temp, filepath='~/Astro/699-2/results/temp/', lstar=lstar, tstar=tstar, mdisk=mdisk, $
          gamma=gamma, rc=rc, h100=h100, hscale=hscale, ps=ps, logcorr=logcorr, $
          colmin=colmin, colmax=colmax, rowmin=rowmin, rowmax=rowmax

END
