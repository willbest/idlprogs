PRO FITLINES_TEMP, OUTFIT, SIMS=sims, FILEPATH=filepath, GRAPH=graph, PS=ps, _EXTRA=extrakey, $
                     LSTAR=lstar, TSTAR=tstar, MDISK=mdisk, GAMMA=gamma, RC=rc, H100=h100, $
                     HSCALE=hscale

;+
;  Reads the output midplane temperature profiles from the make_temp simulations,
;      which are saved as ~/Astro/699-2/results/temp/temp.sav
;  Fits a line to the log-log plots of temperature as a function of radius
;  (distance from the central star) for the disk(s) with the properties
;  specified by the keywords.
;       log(T) = a + b*log(R)       -- find a and b for each disk
;  If multiple values are supplied via keyword for a parameter, the program will
;  make fits for disks with all supplied values of that parameter.
;  If no value is supplied via keyword for a parameter, the program will
;  make fits for disks with all available values of that parameter.
;  
;  HISTORY
;  Written by Will Best (IfA), 02/23/2013
;  05/29/2013 (WB): Adjusted for varying amr_grid files.
;  06/13/2013 (WB): Changed loop variable long integer format
;
;  OUTPUTS (OPTIONAL)
;      OUTFIT - File to save the fits to.  Default is FILEPATH/tempfits_new.sav
;
;  KEYWORDS
;      SIMS - "sims" structure containing the data to fit.  If none is supplied, the
;             program will load it from FILEPATH.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/
;      GRAPH - Make plot of temperature vs. radius for chosen disks.
;      PS - send plot to postscript.  If GRAPH is not set, PS is quietly ignored.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then all its values are used to make plots on the same axes.
;      LSTAR - L_star    Default: 0.44 (Lsun)
;      TSTAR - T_star    Default: 3705 (K)
;      MDISK - M_disk    Default: 1e-4 (Msun)
;      GAMMA - gamma     Default: 0.5
;      RC - R_c          Default: 100 (AU)
;      H100 - H_100      Default: 10 (AU)
;      HSCALE - h        Default: 1.0
;-

; Potential variables
;gridfile = '~/Astro/699-2/results/amr_grid.inp'
if n_elements(outfit) eq 0 then outfit = filepath+'tempfits_new.sav'
Tfreeze = 20                   ; CO freeze-out temperature
col = [0, 3, 4, 2, 11, 12]     ; plotting colors

logTf = alog10(Tfreeze)

; get the data
if n_elements(sims) eq 0 then begin
    if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/temp/'
    restore, filepath+'temp.sav'
endif
tgrid = n_elements(temp[0].td)
;ntot = n_elements(temp)

; Check input values
npar = 7
if not keyword_set(lstar) then lstar = temp[uniq(temp.L, sort(temp.L))].L
if not keyword_set(tstar) then tstar = temp[uniq(temp.T, sort(temp.T))].T
if not keyword_set(mdisk) then mdisk = temp[uniq(temp.Md, sort(temp.Md))].Md
if not keyword_set(gamma) then gamma = temp[uniq(temp.g, sort(temp.g))].g
if not keyword_set(rc) then rc = temp[uniq(temp.Rc, sort(temp.Rc))].Rc
if not keyword_set(h100) then h100 = temp[uniq(temp.H100, sort(temp.H100))].H100
if not keyword_set(hscale) then hscale = temp[uniq(temp.h, sort(temp.h))].h


;;; DETERMINE WHICH DISK(S) TO GET THE TEMPERATURES FROM.
; Make a structure containing the values from the keywords
keys = {lstar:float(lstar), tstar:fix(tstar), mdisk:float(mdisk), gamma:float(gamma), $
        rc:fix(rc), h100:fix(h100), hscale:float(hscale)}

;;; Build a large string, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters
for i=0, npar-1 do begin
    match, temp[uniq(temp.(i), sort(temp.(i)))].(i), keys.(i), xx, ind ; where parameter matches keyword value(s)
    nind = n_elements(ind)
    if ind[0] ge 0 then begin   ; if parameter matches keyword value(s) at least once, go on
        if nind gt 1 then fstr = fstr + $
               '(' + strjoin('(temp.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
        fstr = fstr + '(temp.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
        if nind gt 1 then fstr = fstr + ')'
        fstr = fstr + ' and '
    endif
endfor


;;; GET THE DUST TEMPERATURE VALUES
; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'  ; replace the last ' and ' with a )
    dummy = execute(fstr)
    chosen = temp[select]
    ndisks = n_elements(chosen)
endif else begin
    print, 'The parameter values you entered do not match any disks.'
    return
endelse


;;; GET THE RADIUS VALUES
;; readcol, gridfile, radgrid, skipline=6, format='f', /silent
;; r = fltarr(tgrid)
;; for i=0, tgrid-1 do r[i] = mean(radgrid[i:i+1])
;; r = r/1.49598e13     ; convert to AU


;;; FIT log(T) = a + b*log(R) FOR EACH DISK
tempfits = replicate({L:0., T:0, Md:0., g:0., Rc:0, H100:0, h:0., Rd:fltarr(tgrid), Td:fltarr(tgrid), $
                      a:0., b:0., siga:0., sigb:0., chi2:0., Rf:0.}, ndisks)
copy_struct, chosen, tempfits    ; import the values from chosen into tempfits

yfit = fltarr(tgrid, ndisks)
for i=0L, ndisks-1 do begin
    good = where(chosen[i].Td gt 0)
    p = linfit(alog10(chosen[i].Rd[good]), alog10(chosen[i].Td[good]), chisq=tempfits[i].chi2, sigma=sigma);, yfit=yvals)
    tempfits[i].a = p[0]
    tempfits[i].b = p[1]
    tempfits[i].siga = sigma[0]
    tempfits[i].sigb = sigma[1]
    ;yfit[*,i] = yvals
; solve for R_freeze, the R value where T = T_freeze
; log T = a + b log R, so R = 10^[(log T – a)/b]
    tempfits[i].Rf = 10^((logTf - tempfits[i].a) / tempfits[i].b)
endfor

save, tempfits, filename=outfit


;;; SET UP FOR PLOTTING
if keyword_set(graph) then begin
; Load a color table
    device, decomposed=0
    lincolr_wb, /silent

    if keyword_set(ps) then begin
        fpath = '~/Astro/699-2/results/tempplot'
        ps_open, fpath, /color, /en, thick=4
    endif else begin
        window, 0, retain=2, xsize=800, ysize=600
    endelse

;;; MAKE THE PLOT(S)
    plot, chosen[0].Rd, chosen[0].Td, color=col[0], background=1, charsize=1.8, /xlog, /ylog, _extra=extrakey, $
          xtit='Distance (AU)', ytit='Dust Temperature (K)' ;, psym=-4
;    oplot, chosen[0].Rd, 10^yfit[*,0], lines=2, color=col[0]
    oplot, chosen[0].Rd, 10^(tempfits[0].a + tempfits[0].b*alog10(chosen[0].Rd)), lines=2, color=col[0]
    hline, Tfreeze, lines=1, color=col[0], /xlog, _extra=extrakey
    
    xyouts, 0.768-0.18*(nplot-1), 0.87, 'L!Dstar!N = '+trim(chosen[0].L)+' L!D!9n!X!N', /normal, color=col[0], chars=1.1
    xyouts, 0.768-0.18*(nplot-1), 0.84, 'T!Dstar!N = '+trim(chosen[0].T)+' K', /normal, color=col[0], chars=1.1
    xyouts, 0.762-0.18*(nplot-1), 0.81, 'M!Ddust!N = '+trim(chosen[0].Md)+' M!D!9n!X!N', /normal, color=col[0], chars=1.1
    xyouts, 0.788-0.18*(nplot-1), 0.78, textoidl('\gamma')+' = '+trim(chosen[0].g), /normal, color=col[0], chars=1.1
    xyouts, 0.781-0.18*(nplot-1), 0.75, 'R!Dc!N = '+trim(chosen[0].Rc)+' AU', /normal, color=col[0], chars=1.1
    xyouts, 0.766-0.18*(nplot-1), 0.72, 'H!D100!N = '+trim(chosen[0].H100)+' AU', /normal, color=col[0], chars=1.1
    xyouts, 0.788-0.18*(nplot-1), 0.69, 'h = '+trim(chosen[0].h), /normal, color=col[0], chars=1.1
    
    for i=1, ndisks-1 do begin
        oplot, chosen[0].Rd, chosen[i].Td, color=col[i], _extra=extrakey
;        oplot, chosen[0].Rd, 10^yfit[*,i], lines=2, color=col[i]
        oplot, chosen[0].Rd, 10^(tempfits[i].a + tempfits[i].b*alog10(chosen[0].Rd)), lines=2, color=col[i]

        xyouts, 0.768-0.18*(nplot-1-i), 0.87, 'L!Dstar!N = '+trim(chosen[i].L)+' L!D!9n!X!N', /normal, color=col[i], chars=1.1
        xyouts, 0.768-0.18*(nplot-1-i), 0.84, 'T!Dstar!N = '+trim(chosen[i].T)+' K', /normal, color=col[i], chars=1.1
        xyouts, 0.762-0.18*(nplot-1-i), 0.81, 'M!Ddust!N = '+trim(chosen[i].Md)+' M!D!9n!X!N', /normal, color=col[i], chars=1.1
        xyouts, 0.788-0.18*(nplot-1-i), 0.78, textoidl('\gamma')+' = '+trim(chosen[i].g), /normal, color=col[i], chars=1.1
        xyouts, 0.781-0.18*(nplot-1-i), 0.75, 'R!Dc!N = '+trim(chosen[i].Rc)+' AU', /normal, color=col[i], chars=1.1
        xyouts, 0.766-0.18*(nplot-1-i), 0.72, 'H!D100!N = '+trim(chosen[i].H100)+' AU', /normal, color=col[i], chars=1.1
        xyouts, 0.788-0.18*(nplot-1-i), 0.69, 'h = '+trim(chosen[i].h), /normal, color=col[i], chars=1.1
    endfor

    if keyword_set(ps) then ps_close
endif

END

