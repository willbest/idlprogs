PRO CORR_TEMP, TEMP=temp, FILEPATH=filepath, OUTGRID=outgrid, OUTTEXT=outtext, _EXTRA=extrakey, $
               LSTAR=lstar, TSTAR=tstar, MDISK=mdisk, GAMMA=gamma, RC=rc, H100=h100, HSCALE=hscale, $
               LOGCORR=logcorr

;+
;  Reads the output plane temperature linear fits from the make_temp
;  simulations.
;      log(T) = a + b*log(R)       -- a and b for each disk
;  The values read are a, b, and R_freeze = R(T=20 K)
;  Calculates the correlation matrix to determine the most significant
;  parameters for a, b, and R_freeze.
;
;  What is the correlation matrix, you say?
;  First, for any m x n matrix A, the covariance matrix is the n x n matrix
;     trans(A) # A.  Each element of this symmetric matrix gives you the covariance
;     of the two columns of A corresponding to the location of the element.
;     The diagonal of the covariance matrix gives the variance for each column
;     of A.
;  The correlation matrix for A is the covariance matrix, with each element
;     divided by the standard deviations of the two columns of A corresponding to
;     the location of the element.  This normalizes the matrix so that each element
;     is between -1 and 1, and therefore...
;        >>> each element gives the correlation coefficient of the two columns of A
;        >>> corresponding to the location of the element.
;     The diagonal of the correlation matrix is all 1's, because each column
;     of A correlates with itself.
;
;  If specific values for various disk parameters are supplied (via keyword),
;  then the correlation matrix will incorporate fluxes only from disks with those
;  parameter values.
;
;  If no value is specified for a given parameter, then the correlation matrix
;  includes disks of all values of that parameter (that have the values given for
;  other parameters).
;  
;  HISTORY
;  Written by Will Best (IfA), 05/23/2013
;
;  OPTIONAL KEYWORDS
;      ABS - Print the formatted matrix with colors representing the absolute
;            values of the correlation coefficients; in other words, only use red.
;      COLMIN, COLMAX - Integers for the first and last columns to be printed in
;               the formatted correlation matrix.
;               Defaults:  colmin = 1, colmax = n   (i.e. all columns)
;               To confirm the integers you want to use, first print the entire
;               correlation matrix without supplying labels.
;      CORRMAT - Set this equal to a variable that will receive the n x n
;                correlation matrix.
;      FILEPATH - path for the directory where the data structure is stored.
;                 Default: ~/Astro/699-2/results/temp/
;      LOGCORR - Find correlation coefficients for the logarithms of the values
;      NONUM - Don't print the numbers in the formatted matrix -- just colors.
;      OUTGRID - Set this equal to the path/filename for postscript output.
;                Default: ~/Astro/699-2/results/temp/corr_temp_grid.eps
;      OUTTEXT - To print the correlation matrix values to a text file, set
;                OUTTEXT equal to the path/filename.
;      PS - Print the formatted correlation matrix to a postscript file.
;      ROWMIN, ROWMAX - Integers for the first and last rows to be printed in
;               the formatted correlation matrix.
;               Defaults:  rowmin = 1, rowmax = n   (i.e. all rows)
;               To confirm the integers you want to use, first print the entire
;               correlation matrix without supplying labels.
;      SILENT - Suppress text output to the screen.
;      TEMP - Structure containing the data to plot.  If none is supplied, the
;             program will load "tempfits.sav" from FILEPATH.
;
;    The following keywords can be used to specify values for various disk
;    paramters.
;    Each keyword can be a scalar or a vector of values.  If a vector is
;    supplied, then the values from disks with all supplied values
;    are used in the correlation analysis.
;    If a parameter value is supplied for which there are no disks, that value
;    will be ignored (needs to be coded in).
;    If a keyword is not set, disks with all values for that parameter will be
;    used in the correlation analysis.
;      LSTAR - L_star
;      TSTAR - T_star
;      MDISK - M_disk
;      GAMMA - gamma
;      RC - R_c
;      H100 - H_100
;      HSCALE - h
;
;  CALLS
;      corrmatrix
;      trim
;-

;;; SET UP PARAMETERS AND PATHS
if not keyword_set(filepath) then filepath='~/Astro/699-2/results/temp/'
if not keyword_set(outgrid) then outgrid='~/Astro/699-2/results/temp/corr_temp_grid'
if not keyword_set(outtext) then outtext='~/Astro/699-2/results/temp/corr_temp_result.txt'

; Labels for the parameters and emission lines
params = ['L_star', 'T_star', 'M_disk', 'gamma', 'R_c', 'H_100', 'h']
labarr = textoidl(['L_{star}', 'T_{star}', 'M_{disk}', '\gamma', 'R_c', 'H_{100}', 'h'])
npar = n_elements(params)
fitvals = ['a', 'b', 'R_freeze']
fitlabarr = textoidl(['a', 'b', 'R_{freeze}'])
nfit = n_elements(fitvals)


;;; GET THE DATA
if not keyword_set(temp) then restore, filepath+'tempfits.sav'


;;; DETERMINE WHICH DISKS TO GET THE FLUXES FROM.
; Make a structure containing the values from the keywords
if not keyword_set(lstar) then lstar = -1
if not keyword_set(tstar) then tstar = -1
if not keyword_set(mdisk) then mdisk = -1
if n_elements(gamma) eq 0 then gamma = -1
if not keyword_set(rc) then rc = -1
if not keyword_set(h100) then h100 = -1
if not keyword_set(hscale) then hscale = -1

keys = {lstar:float(lstar), tstar:fix(tstar), mdisk:float(mdisk), gamma:float(gamma), $
        rc:fix(rc), h100:fix(h100), hscale:float(hscale)}

;;; Build a large string, to call with the EXECUTE function
fstr = 'select = where('

; Loop over the parameters to select disks with the chosen parameters
for i=0, (npar-1) do begin
    if (keys.(i)[0] ge 0) then begin    ; if the keyword has value(s) and is not the PARAM, go on
        match, tempfits[uniq(tempfits.(i), sort(tempfits.(i)))].(i), keys.(i), xx, ind  ; where parameter matches keyword value(s)
        nind = n_elements(ind)
        if ind[0] ge 0 then begin                    ; if parameter matches keyword value(s) at least once, go on
            if nind gt 1 then fstr = fstr + $
                   '(' + strjoin('(tempfits.('+trim(i)+') eq '+trim(keys.(i)[ind[0:nind-2]])+') or ')
            fstr = fstr + '(tempfits.('+trim(i)+') eq '+trim(keys.(i)[ind[nind-1]])+')'
            if nind gt 1 then fstr = fstr + ')'
            fstr = fstr + ' and '
        endif
    endif
endfor


;;; GET THE A, B, and R_FREEZE VALUES FROM THE TEMPERATURE FITS
; Get the chosen subset of disks
if strlen(fstr) gt 15 then begin
    fstr = strmid(fstr, 0, strlen(fstr)-5) + ')'  ; replace the last ' and ' with ')'
    dummy = execute(fstr)
    chosen = tempfits[select]
endif else chosen = tempfits


;;; CREATE THE DATA MATRIX FOR WHICH TO CALCULATE THE CORRELATION MATRIX
; Choose the parameters with more than one value, and build the data matrix
data = fltarr(n_elements(chosen))
palab = 'dummy'
labels = 'dummy'
for i=0, npar-1 do begin
    if n_elements(chosen[uniq(chosen.(i), sort(chosen.(i)))].(i)) gt 1 then begin
        data = [[data], [chosen.(i)]]    ; add column to the data matrix
        palab = [palab, params[i]]       ; add parameter name to the labels vector for text tables
        labels = [labels, labarr[i]]     ; add parameter name to the formatted matrix labels vector
    endif
endfor
data = data[*,1:*]
palab = palab[1:*]
labels = labels[1:*]

; Add a, b, and R_freeze to the matrix
data = [[data], [chosen.a], [chosen.b], [chosen.rf]]    ; add columns to the data matrix
labels = [labels, fitlabarr]                            ; add labels to the formatted matrix labels vector


;;; GET AND PRINT THE CORRELATION MATRIX
; Numbers of parameters and total columns
data = transpose(data)                   ; correct orientation for calculating the correlation matrix
ntot = n_elements(data[*,0])             ; number of columns in the data matrix
ncol = ntot - nfit                       ; number of varying parameters
if keyword_set(logcorr) then begin
    data[3,where(data[3,*] eq 0)] = 1d-20
    data[0:ncol-1, *] = alog10(data[0:ncol-1, *])
endif
; Call for the correlation matrix
corrmatrix, data, labels, corrmat=corrmat, outgrid=outgrid, outtext=outtext, /silent, _extra=extrakey


;;; PRINT CORRELATON COEFFICIENTS IN TABLES
; Set up the parameter tables
palabo = strarr(ncol,nfit)               ; array for ordered parameter labels
corro = fltarr(ncol,nfit)                ; array for ordered correlation coefficients
for k=0, nfit-1 do begin                 ; Loop over temperature fit values
    row = k + ncol                       ; choose a row corresponding to a line flux
    corr = corrmat[0:ncol-1, row]        ; get the correlation coeffs with parameters for that fit value
    order = reverse(sort(abs(corr)))     ; sort the coeffs into descending order
    corro[*,k] = corr[order]
    palabo[*,k] = palab[order]
endfor

; Print to screen
print
print
print, fitvals, format='(t11,a,t43,a,t75,a)'
print
print, replicate(' Parameter    Correlation', nfit), format='(a,t33,a,t65,a)'
print, replicate(' ---------    -----------', nfit), format='(a,t33,a,t65,a)'
for i=0, ncol-1 do print, reform([palabo[i,*], trim(corro[i,*])], 2*nfit), $
                          format='(t4,a,t18,f5.2,t36,a,t50,f5.2,t68,a,t82,f5.2)'
print

; Output to file
if keyword_set(outtext) then begin
    get_lun, lun
    openw, lun, outtext, /append
    printf, lun
    printf, lun
    printf, lun, fitvals, format='(t11,a,t43,a,t75,a)'
    printf, lun
    printf, lun, replicate(' Parameter    Correlation', nfit), format='(a,t33,a,t65,a)'
    printf, lun, replicate(' ---------    -----------', nfit), format='(a,t33,a,t65,a)'
    for i=0, ncol-1 do printf, lun, reform([palabo[i,*], trim(corro[i,*])], 2*nfit), $
                               format='(t4,a,t18,f5.2,t36,a,t50,f5.2,t68,a,t82,f5.2)'
    printf, lun
    free_lun, lun
endif

END

