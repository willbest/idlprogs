@radmc3dfits.pro

PRO MAKE_TEMP_CORE, LLOOP, TLOOP, MLOOP, GLOOP, RCLOOP, H100LOOP, HLOOP, SIMNUM, $
                    DATAROOT=dataroot, FINE=fine, OUTPATH=outpath, WORKING=working

;+
;  Generates the dust temperature profile for a model protoplanetary disk.
;  1. Creates a dust profile
;  2. Uses radmc-3d to calculate dust temperature structure
;     - output model SED, profiles and image
;  3. plot results
;
;  Parameters for the star and disk should be entered in make_cont_parameters_loop.pro
;  
;  HISTORY
;  Adapted by Will Best (IfA), 02/09/13  from make_cont_lines_core.pro
;  05/24/2013 (WB): Include LLOOP and TLOOP.
;
;  INPUTS
;      LLOOP, TLOOP, MLOOP, GLOOP, RCLOOP, H100LOOP, HLOOP - Parameters passed by
;             make_cont_loop.pro, used by make_cont_parameters_loop.pro
;      SIMNUM - simulation number.  Used to name output files.
;
;  KEYWORDS
;      DATAROOT - top level of the data output folder structure.
;                 report.txt goes here.
;      FINE - Use a finer grid for the dust continuum model
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;      WORKING - generic working directory for RADMC
;
;  CALLS
;      make_cont_parameters_loop
;      make_header
;      trim
;-

goto, debug
debug:

; mark the start time
starttime = systime()

; Star and disk parameters
@make_cont_parameters_loop.pro

; Generic working directory
if n_elements(working) eq 0 then working = '~/Astro/699-2/radmc_output/'

; Data output paths
if n_elements(dataroot) eq 0 then dataroot = '~/Astro/699-2/test_output/'
level1 = 'L'+trim(star.l)+'_T'+trim(star.t)+'/'
level2 = 'Md'+trim(disk.m)+'_g'+trim(disk.gamma)+'_Rc'+trim(disk.rc)+'_H'+trim(disk.H100)+'_h'+trim(disk.h)+'/'
ips_out = dataroot+level1+level2
spawn, 'mkdir -p '+ips_out

if keyword_set(outpath) then filename=outpath else filename=ips_out

; Close any lingering open write files
close, /all

; Point to generic working directory
cd, working

; Start the report file if necessary
report = dataroot+'report.txt'
if file_test(report) eq 0 then begin
    openw, 33, report, width=100
    printf, 33, '#Run', 'Star', 'Disk', $
            format='(a,t9,a,t24,a)'
    close, 33
endif

; Check for iteration number
if n_elements(simnum) eq 0 then simnum = 0

runnum = trim(simnum)
numlabel = runnum

;;;;;;;;;;
; create dust profile
if keyword_set(fine) then $
  make_dust_profile,star=star,disk=disk,/fine $
else make_dust_profile,star=star,disk=disk

;;;;;;;;;;
; create star spectrum
make_star_spec,star=star,/silent

;;;;;;;;;;
; calculate dust temperature
spawn,'radmc3d mctherm'

;;;;;;;;;;
; output model continuum image at SMA observing wavelength
;; npix = 256
;; lambda_microns = 1333.3
;; cmd=string(format='("radmc3d image lambda ",f6.1," npix ",i0," incl ",f5.1," posang ",f6.1, " nostar noline")',lambda_microns,npix,INCL,PA)
;; print,cmd
;; spawn,cmd

; make a fits image
;; file_delete,'image.fits',/allow_nonexistent
;; radmcimage_to_fits,'image.out','image.fits',140.
;; file_delete,src+'_model.fits',/allow_nonexistent
;; make_header,src,RA,DEC


;;;;;;;;;;
; output SED
; first create camera wavelength file
;; readcol,'sed.txt',lambda,flux,format='f,f',skipline=10
;; sed_lambda = [lambda[0:n_elements(lambda)-2],[200., 350., 450., 850., 1333.3]]
;; get_lun,lun
;; sedfile='camera_wavelength_micron.inp'
;; file_delete,sedfile,/allow_nonexistent
;; openw,lun,sedfile
;; nlambda=n_elements(sed_lambda)
;; printf,lun,format='(3x,i0)',nlambda
;; for n=0,nlambda-1 do printf,lun,format='(3x,f9.4)',sed_lambda[n]
;; close,lun

; then calculate SED
;; file_delete,'sed.out',/allow_nonexistent
;; cmd=string(format='("radmc3d spectrum loadlambda incl ",f5.1," posang ",f5.1, " noline")',INCL,PA)
;; print,cmd
;; spawn,cmd

; copy files to data directory
;; spawn, 'cp image.fits '+filename+'image'+numlabel+'.fits'
;; spawn, 'cp image.out '+filename+'image'+numlabel+'.out'
;; spawn, 'mv spectrum.out sed.out'
;; spawn, 'cp sed.out '+filename+'sed'+numlabel+'.out'
spawn, 'cp dust_temperature.dat '+filename+'dust_temperature'+numlabel+'.dat'


;;;;;;;;;;
; calculate visibilities and simulated image
;cmd=string(format='("casapy -c simobs.py -n",a0)',src)
;print,cmd
;spawn,cmd


;;;;;;;;;;
;; plot:
;; ; plot results
;; plot_sed, src=src, disk=disk, filename=filename+'sed'+numlabel+'.eps', incl=incl, pdf=pdf, ps=ps
;; plot_profile, filename=filename+'profile'+numlabel+'.eps', pdf=pdf, ps=ps
;; plot_image, filename=filename+'image'+numlabel+'.eps', pdf=pdf, ps=ps

; Write to the report file
print
print, 'Writing report line for run '+runnum
openw, 33, report, width=100, /append
printf, 33, runnum, level1, level2, $
        format='(a,t9,a,t24,a)'
close, 33

if not keyword_set(plot) then begin
    print
    print, 'Start time for this simulation: '+starttime
    print, 'Finish time for this simulation: '+systime()
    print
endif

return
end
