PRO MAKE_FROZEN_LOOP, OUTPATH=outpath

;+
;  Runs make_frozen_core multiple times, varying the input disk parameters
;  
;  HISTORY
;  Written by Will Best (IfA), 03/09/2013
;
;  KEYWORDS
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;
;  CALLS
;      make_frozen_core
;-

; mark the start time
startloop = systime()

; model disk parameters
; m    = dust mass in units of Msun
; gamma = index for surface density profile
; rc   = characteristic radius in units of AU
; H100 = scale height at r=100AU in AU
; h    = power law exponent of H(r)

marr = [1d-5, 1d-4, 1d-3, 1d-2]
garr = [0.3, 0.6, 1.0, 1.2]
rcarr = [50., 100., 150.]
aarr = [1.95, 2.00, 2.05]
barr = [-0.60, -0.55, -0.50]

;; marr = [1d-3]
;; garr = [1.0]
;; rcarr = [100.]
;; aarr = [1.95, 2.05]
;; barr = [-0.6, -0.5]

simnum = 1

; set up output path
;dataroot = '~/Astro/699-2/test_output/'
dataroot = '~/Astro/699-2/results/frozen/'
report = dataroot+'report.txt'

; set up to check for previous incomplete run
file_flag = 0
if file_test(report) eq 1 then begin
    readcol, report, runnum, format='f,x,x,x', comment='#'
    if n_elements(runnum) eq 0 then runnum = 0   ; case where no report lines have yet been written
    file_flag = 1
endif

; Vary the m parameter
for i=0, n_elements(marr)-1 do begin
    mloop = marr[i]
    ; Vary the gamma parameter
    for i2=0, n_elements(garr)-1 do begin
        gloop = garr[i2]
        ; Vary the rc parameter
        for i3=0, n_elements(rcarr)-1 do begin
            rcloop = rcarr[i3]
            ; Vary the rc parameter
            for i4=0, n_elements(aarr)-1 do begin
                aloop = aarr[i4]
                ; Vary the rc parameter
                for i5=0, n_elements(barr)-1 do begin
                    bloop = barr[i5]
                    print
                    print, 'Run number '+trim(simnum)
                    if file_flag eq 1 then begin
                        prev = where(simnum eq round(runnum))
                        if n_elements(prev) eq 1 then begin
                            print, '    This run is already complete.'
                            simnum++
                            continue
                        endif
                        file_flag = 0
                    endif
                    print
                    make_frozen_core, mloop, gloop, rcloop, aloop, bloop, simnum, $
                                      dataroot=dataroot, outpath=outpath, /pdf, /ps
                    simnum++
                endfor
            endfor
        endfor
    endfor
endfor

print
print, 'Done!!'
print
print, 'Start time for the whole run: '+startloop
print, 'Finish time for the whole run: '+systime()
print

END

