PRO CALCFREEZE, WORKING=working, _EXTRA=extrakey

;+
; Create H2 number density file and calculate freeze-out.
;
;  HISTORY
;  10/29/12 (jpw): hacked from calclines.pro
;  11/27/12 (WB):  added INCL as a parameter
;                  added GD and WORKING keywords
;
;  KEYWORDS
;      WORKING - working directory to point to
;                Default: '~/Astro/699-2/radmc_output'
;
;      _EXTRA can be used to pass star, disk, temp, and t_freeze through 
;      calcfreeze.pro to plot_profile.pro and make_frozen_profile.pro.
;-

@natconst

; set number of spectral channels over line width
;nchan = 20

if not keyword_set(working) then working = '~/Astro/699-2/radmc_output'
cd, working

;plot_profile, _extra=extrakey

make_frozen_profile, _extra=extrakey

print,'---> *** calcfreeze complete! ***' 

return
end
