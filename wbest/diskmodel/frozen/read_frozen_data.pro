;nsets=2
;filepath='~/Astro/699-2/results/test9/'
;PRO READ_FROZEN_DATA, FILEPATH=filepath, NSETS=nsets, SETNAMES=setnames;

;+
;  Reads the output line strengths from the make_cont_lines simulations.
;  
;  HISTORY
;  Begun by Will Best (IfA), 3/15/2013
;
;  KEYWORDS
;      FILEPATH - path for the directory tree where the output files are stored.
;                 Default: ~/Astro/699-2/results/
;      NSETS - If multiple sets of simulations were run, set this keyword equal
;              to the number of sets.
;              Default = 1
;      SETNAMES - If multiple sets of simulations were run, set this keyword
;                 equal to the root of the names for those sets.
;                 e.g. for loop1, loop2, etc., setnames='loop'
;                 Default = 'loop'
;                 If NSETS is not set, SETNAMES is ignored.
;-

; set up generic paths
if not keyword_set(filepath) then filepath = '~/Astro/699-2/results/frozen/'
if keyword_set(nsets) then begin
    if not keyword_set(setnames) then setnames='loop'
endif else nsets=1
readpath = filepath

; Oh, the structure
nsim = intarr(nsets)
for i=0, nsets-1 do begin
    if nsets gt 1 then readpath = filepath+setnames+trim(i+1)+'_output/'
    cd, readpath
    nsim[i] = file_lines('report.txt') - 1
endfor
ntot = total(nsim)
frozen = replicate({L:0., T:0, Md:0., g:0., Rc:0, a:0., b:0., Tf:0., frac:0.}, ntot)

; start the loops
counter = 0
for i=0, nsets-1 do begin
    if nsets gt 1 then begin
        readpath = filepath+setnames+trim(i+1)+'_output/'
        print, 'Loop #'+trim(i+1)
    endif
    cd, readpath
    readcol, 'report.txt', run, star, disk, temp, chem, delim=' ', comment='#', format='f,a,a,a,a'

    num = nsim[i]
    L = rebin(float(strmid(star, 1, 1#strpos(star, '_T')-1)), num, /sample)
    T = rebin(fix(strmid(star, 1#strpos(star, '_T')+2, 1#strpos(star, '/')-1#strpos(star, '_T')-2)), num, /sample)
    Md = rebin(float(strmid(disk, 2, 1#strpos(disk, '_g')-2)), num, /sample)
    g = rebin(float(strmid(disk, 1#strpos(disk, '_g')+2, 1#strpos(disk, '_Rc')-1#strpos(disk, '_g')-2)), num, /sample)
    Rc = rebin(fix(strmid(disk, 1#strpos(disk, '_Rc')+3, 1#strpos(disk, '/')-1#strpos(disk, '_Rc')-3)), num, /sample)
    a = rebin(float(strmid(temp, 1, 1#strpos(temp, '_b')-1)), num, /sample)
    b = rebin(float(strmid(temp, 1#strpos(temp, '_b')+2, 1#strpos(temp, '/')-1#strpos(disk, '_b')-2)), num, /sample)
    Tf = rebin(fix(strmid(chem, 2, 1#strpos(disk, '/')-2)), num, /sample)

    frozen[counter:counter+num-1].L = L
    frozen[counter:counter+num-1].T = T
    frozen[counter:counter+num-1].Md = Md
    frozen[counter:counter+num-1].g = g
    frozen[counter:counter+num-1].Rc = Rc
    frozen[counter:counter+num-1].a = a
    frozen[counter:counter+num-1].b = b
    frozen[counter:counter+num-1].Tf = Tf

    for j=0, num-1 do begin
        readcol, star[j]+disk[j]+temp[j]+chem[j]+'frozen_co.out', frac, comment='%', format='f', /silent
        frozen[j+counter].frac = frac
    endfor
    counter = counter + num

endfor

cd, filepath
forprint, frozen, textout='frozen.txt', width=135, /nocomment, /silent
save, frozen, filename='frozen.sav'

print
print, 'Done!!'
print

END

