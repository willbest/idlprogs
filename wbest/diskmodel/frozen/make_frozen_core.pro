@radmc3dfits.pro

PRO MAKE_FROZEN_CORE, MLOOP, GLOOP, RCLOOP, ALOOP, BLOOP, SIMNUM, $
                      DATAROOT=dataroot, FINE=fine, OUTPATH=outpath, $
                      PDF=pdf, PS=ps

;+
;  Generates gas density and thermal profiles and determines the CO freeze-out
;  radius and fraction in a model protoplanetary disk.
;
;  Parameters for the star and disk should be entered in make_frozen_parameters_loop.pro
;  
;  HISTORY
;  Begun by Will Best (IfA), 03/10/2013
;
;  INPUTS
;      MLOOP, GLOOP, RCLOOP, ALOOP, BLOOP  - Parameters passed by
;             make_frozen_loop.pro, used by make_frozen_parameters_loop.pro
;      SIMNUM - simulation number.  Used to name output files.
;
;  KEYWORDS
;      DATAROOT - top level of the data output folder structure.
;                 report.txt goes here.
;      FINE - Use a finer grid for the dust continuum model
;      OUTPATH - file path+prefix for postcript output.  Overrides the default
;                 data output folder structure.
;      PDF - If set, create pdf output
;      PS - If set, create postscript output
;
;  CALLS
;;      calclines
;      make_frozen_parameters_loop
;;      make_header
;;      make_line_results
;;      plot_image
;;      plot_profile
;;      plot_sed
;;      radmcimage_to_fits
;      trim
;-

goto, debug
debug:

; mark the start time
starttime = systime()

; Star and disk parameters
@make_frozen_parameters_loop.pro

; Generic working directory
working = '~/Astro/699-2/radmc_output'
;working = '.'

; Data output paths
if n_elements(dataroot) eq 0 then dataroot = '~/Astro/699-2/results/frozen/'
spawn, 'mkdir -p '+dataroot
level1 = 'L'+trim(star.l)+'_T'+trim(star.t)+'/'
level2 = 'Md'+trim(disk.m)+'_g'+trim(disk.gamma)+'_Rc'+trim(disk.rc)+'/'
level3 = 'a'+trim(temp.a)+'_b'+trim(temp.b)+'/'

; Close any lingering open write files
close, /all

; Point to generic working directory
cd, working

; Start the report file if necessary
report = dataroot+'report.txt'
if file_test(report) eq 0 then begin
    openw, 33, report, width=200
    printf, 33, '#Run', 'Star', 'Disk', 'Temperature', 'Chemistry', $
            format='(a,t9,a,t24,a,t47,a,t63,a)'
    close, 33
endif

; Check for iteration number
if n_elements(simnum) eq 0 then simnum = 0

; Vary the t_freeze parameter
for it=0, n_elements(tfarr)-1 do begin
    t_freeze = tfarr[it]
    level4 = 'Tf'+trim(t_freeze)+'/'

; make the output folders
    ;runnum = trim(simnum)+'.'+trim(n_elements(tfarr)*iav+it+1)
    runnum = trim(simnum)+'.'+trim(it+1)
    ;numlabel = runnum+'.i'+trim(incl)   DON'T NEED ANYMORE
    fr_out = dataroot+level1+level2+level3+level4
    ;line_out = ips_out+'spectra'     DON'T NEED ANYMORE
    spawn, 'mkdir -p '+fr_out
    if keyword_set(outpath) then filename=outpath else filename=fr_out

; generate the emission spectra and profile plot
    calcfreeze, working=working, star=star, disk=disk, temp=temp, t_freeze=t_freeze ;, $
                ;filename=filename+'profile'+numlabel+'.eps', pdf=pdf, ps=ps

; move the frozen_co file over to the data directory
    spawn, 'mv frozen_co.out '+fr_out

; Write to the report file
    print
    print, 'Writing report line for run '+runnum
    openw, 33, report, width=200, /append
    printf, 33, runnum, level1, level2, level3, level4, format='(a,t9,a,t24,a,t47,a,t63,a)'
    close, 33

endfor

finish:
; display the start and finish times
print
print, 'Start time for this simulation: '+starttime
print, 'Finish time for this simulation: '+systime()
print

return
END
