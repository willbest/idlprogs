PRO SYNTHPHO, SPECINDEX, FILTER, ATMOSPHERE, $
              NOAIR=noair, NOFILTER=nofilter, SHOWFLUX=showflux, $
              _EXTRA=extra_keywords

;  Procedure to create an output spectrum using an input spectrum and
;  a filter, for a list of sprectra.
;
;  By default, this program will determine the total flux (normalized)
;  for stars in the Spex Prism library, using a designated filter.
;
;  The SpeX Prism Spectral Libraries are maintained by Adam Burgasser
;  at http://www.browndwarfs.org/spexprism .
;
;  USES
;        loadirvega
;        synthflux
;
;  HISTORY
;  Written by Will Best (IfA), 6/22/2010
;  06/23/10 (WB): added Vega options, using SpexTools Vega model
;  07/01/10 (WB): added _EXTRA keyword, default is to not norm Vega's
;                 flux
;  07/02/10 (WB): shifted the photometry and flux calulations into the
;                 SYNTHFLUX function
;
;  INPUTS
;       SPECINDEX - File with list of input spectra filenames.
;       FILTER - Filter file.
;       ATMOSPHERE - Atmosphere file.
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       NOFILTER - Don't use any filter
;       SHOWFLUX - Print calculated flux for each star processed

;  Establish spectrum index, filter, and atmosphere files
if n_elements(specindex) eq 0 then specindex='default'
case specindex of
    'vega' : begin
        loadirvega, lambda, flux, _extra=extra_keywords
        list='vega'
    end
    'default' : begin
        specindex = '~/spectra/spl_may2010_list.txt'
        readcol, specindex, spclass, list, comment='#', format='a,a', /silent
    end
    else : begin
        readcol, specindex, spclass, list, comment='#', format='a,a', /silent
    end
endcase

if not keyword_set(nofilter) then begin
    filter = '~/filters/mkoj.txt'
;    filter = '~/filters/willjfilter.txt'
    readcol, filter, flambda, fpass, comment='#', format='f,f', /silent
endif

if not keyword_set(noair) then begin
    atmosphere = '~/airmass/ukirt_spexprism.txt'
    readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent
endif

;Set up to read in and plot one object's spectrum at a time
i=0
quit=' '
mainpath='~/spectra/'
device, decomposed=1
window, xsize=800, ysize=600

while (quit ne 'q') and (i lt n_elements(list)) do begin
    case specindex of
        'vega' : name='Vega'
        else : begin
            path=mainpath+strlowcase(spclass[i])+'dwarf/'
            readcol, path+list[i], lambda, flux, format='f,f', /silent
            name=strsplit(list[i],'_',/extract)    ;Nice pretty title for graph
            name=name[1]
        end
    endcase

;Plot a spectrum in white
    pos=getpos(0.7)
    plot, lambda, flux, title='Spectrum for '+name, $
      ;xrange=[1.0,1.5], $
      xtitle='Wavelength (microns)', ytitle='Normalized flux', position=pos, $
      subtitle='Original spectrum is white!CFilter pass is red!CAtmosphere pass is blue!CResultant flux is green'

    totalflux=synthflux(lambda,flux,flambda,fpass,alambda,apass,$
                    NOAIR=noair,NOFILTER=nofilter)
    if keyword_set(showflux) then $
      print, 'Total flux for '+name+' is'+string(totalflux)

;Output to a file
;    path=mainpath+'output/'
;    forprint, lambda, flux, textout=path+list[i], $
;      comment='Filtered spectrum for '+name, /silent

;Continue or quit
    i=i+1
    print, 'Press any key for next spectrum, or q to quit.'
    quit=get_kbrd()
endwhile

END
