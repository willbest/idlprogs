PRO MAKEFILTS

;  Create square filters for testing S/N ratios.
;
;  HISTORY
;  Written by Will Best (IfA), 08/10/2010
;  08/16/2010:  Added band codes

device, decomposed=1
window, 0, xsize=600, ysize=450
pos=getpos(0.7)

jname = ['', 'j', 'jtrim']
jlow = [0, 1.17, 1.26]
jhigh = [0, 1.33, 1.33]

hname = ['', 'h', 'htrim']
hlow = [0, 1.49, 1.53]
hhigh = [0, 1.78, 1.61]

kname = ['', 'k', 'ktrim']
klow = [0, 2.03, 2.03]
khigh = [0, 2.37, 2.18]

lambda = (findgen(127) + 114) / 100

for j=0, 2 do begin
    for h=0, 2 do begin
        for k=0, 2 do begin
            if (j eq 0) and (h eq 0) and (k eq 0) then continue

            file = jname[j] + hname[h] + kname[k]
            path='~/filters/irtest/'+file+'.txt'
            flux = fltarr(127)
            name = ''
            c = 0

            if j ne 0 then begin
                name = jname[j]
                strput, name, strupcase(strmid(name,0,1))
                low = where(lambda eq jlow[j])
                high = where(lambda eq jhigh[j])
                ;tqe = tqe + tqearr[0]
                ;c = c+1.
                c = c+1
                flux[low-3] = 0.
                flux[low-2] = 0.
                flux[low-1] = 0.1
                flux[low] = 0.5
                flux[low+1] = 0.8
                for i=low[0]+2, high[0]-2 do flux[i] = 0.9
                flux[high-1] = 0.8
                flux[high] = 0.5
                flux[high+1] = 0.1
                flux[high+2] = 0.
                flux[high+3] = 0.
            endif

            if h ne 0 then begin
                if name ne '' then name = name+'+'
                addname = hname[h]
                strput, addname, strupcase(strmid(addname,0,1))
                name = name + addname
                low = where(lambda eq hlow[h])
                high = where(lambda eq hhigh[h])
                ;tqe = tqe + tqearr[1]
                ;c = c+1.
                c = c+2
                flux[low-3] = 0.
                flux[low-2] = 0.
                flux[low-1] = 0.1
                flux[low] = 0.5
                flux[low+1] = 0.8
                for i=low[0]+2, high[0]-2 do flux[i] = 0.9
                flux[high-1] = 0.8
                flux[high] = 0.5
                flux[high+1] = 0.1
                flux[high+2] = 0.
                flux[high+3] = 0.
            endif

            if k ne 0 then begin
                if name ne '' then name = name+'+'
                addname = kname[k]
                strput, addname, strupcase(strmid(addname,0,1))
                name = name + addname
                low = where(lambda eq klow[k])
                high = where(lambda eq khigh[k])
                ;tqe = tqe + tqearr[2]
                ;c = c+1.
                c = c+4
                flux[low-3] = 0.
                flux[low-2] = 0.
                flux[low-1] = 0.1
                flux[low] = 0.5
                flux[low+1] = 0.8
                for i=low[0]+2, high[0]-2 do flux[i] = 0.9
                flux[high-1] = 0.8
                flux[high] = 0.5
                flux[high+1] = 0.1
                flux[high+2] = 0.
                flux[high+3] = 0.
            endif

            Plot, lambda, flux, title='Filter: '+name,  position=pos, $
              xtitle='Wavelength (microns)', ytitle='Transmission', $
              xrange=[1.0,2.5], xstyle=1

            title = 'TRIAL '+name+' - TOTAL RESPONSE'
            ;c = tqe / x
            header = [['#'+title],$
                      ['#name: '+name],$
                      ['#Band code: '+strtrim(c,1)],$
                      ['#'],$
                      ['#Wavelength   Transmission'],$
                      ['#=========================']]

            forprint, lambda, flux, textout=path, /silent, $
              format='(3x,f6.4,7x,f6.4)', comment=header

            ;wait, 1
        endfor
    endfor
endfor

END
