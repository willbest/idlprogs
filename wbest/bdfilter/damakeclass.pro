FUNCTION DAMAKECLASS, NUMCLASS

;  Converts the numerical spectral class from DwarfArchives into
;  L and T classifications.  0-9.5 are L, 10-19.5 are T.

on_error, 2
if n_elements(numclass) eq 0 then return, -999.0

if (numclass ge 0) and (numclass le 9.5) then type='L'
if (numclass ge 10) then begin
    type='T'
    numclass=numclass-10.0
endif

if round(numclass) eq numclass then len=1 else len=3
num=strtrim(numclass,1)
num=strmid(num,0,len)
class=type+num

return, class

END
