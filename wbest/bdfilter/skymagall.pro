PRO SKYMAGALL, TWOMASS=twomass, NOAIR=noair, SHOWFLUX=showflux, $
               _EXTRA=extra_keywords

;  Converts the emission spectrum of the sky in infrared into a
;  magnitude, calibrated by setting mag(Vega) = 0.0.  Uses six
;  sky backround files by Lord (1992) from Gemini Observatory.  Writes
;  outputs to a file.
;  http://www.gemini.edu/sciops/telescopes-and-sites/observing-condition-constraints/ir-background-spectra
;
;  Default is MKO filters.
;
;  HISTORY
;  Written by Will Best (IfA), 7/13/2010
;  07/14/2010:  Added Vega error calculation, TWOMASS keyword, normalizing
;
;  USES
;       synthflux
;       vegaflux
;
;  KEYWORDS
;       TWOMASS - Use 2MASS filter set instead of MKO.
;       NOAIR - Ignore atmospheric absorption
;       SHOWFLUX - Display calculated flux for each background processed

lambdaarr=fltarr(80001,6,/nozero)
fluxarr=fltarr(80001,6,/nozero)
filtarr=strarr(18)
magarr=fltarr(18,/nozero)
vegafluxarr=fltarr(18,/nozero)
totalfluxarr=fltarr(18,/nozero)
wvc=fltarr(6,/nozero)
am=fltarr(6,/nozero)
wvcarr=fltarr(18,/nozero)
amarr=fltarr(18,/nozero)

;Establish filters
if not keyword_set(twomass) then fset='mko' else fset='2mass'
case fset of
    '2mass' : begin
        fparr = ['2massj', '2massh', '2massks']
        fnarr = ['2MASS J', '2MASS H', '2MASS Ks']
        vegarr = [3.129e-9, 1.133e-9, 4.283e-10]  ;bandwidth normalized
;        vegarr = [5.06898e-10, 2.84383e-10, 1.12215e-10]
    end
    else : begin
        fparr = ['mkoj', 'mkoh', 'mkok']
        fnarr = ['MKO J', 'MKO H', 'MKO K']
        vegarr = [3.01e-9, 1.18e-9, 4.00e-10]  ;bandwidth normalized
;        vegarr = [4.816e-10, 3.422e-10, 1.360e-10]
    end
endcase

;  Establish atmosphere absorption profile
if not keyword_set(noair) then begin
    atmosphere = '~/airmass/ukirt_spexprism.txt'
    readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent
endif

;  Begin loop of filters
for f=0,2 do begin
    filter = '~/filters/'+fparr[f]+'.txt'
    filt = fnarr[f]
    vegaexpec = vegarr[f]
    readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

;Determine Vega flux, and magnitude constant for mag(Vega) = 0.0
    vgflux = vegaflux(flambda,fpass,alambda,apass,/nograph,$
                      noair=noair,showflux=showflux)
    magc = 2.5*alog10(vgflux)
    if keyword_set(showflux) then begin
        pcterror, vgflux, vegaexpec, errpct, direc
        print, '    error is'+string(errpct)+'% '+direc
        print, 'mag constant is', magc
    endif

;Begin loop of sky backgrounds
    for s=0,5 do begin
        if f eq 0 then begin
            case s of
                0 : begin
                    spectrum = '~/skybright/mk_skybg_zm_10_10_ph.txt'
                    wvc[s] = 1.0
                    am[s] = 1.0
                end
                1 : begin
                    spectrum = '~/skybright/mk_skybg_zm_10_15_ph.txt'
                    wvc[s] = 1.0
                    am[s] = 1.5
                end
                2 : begin
                    spectrum = '~/skybright/mk_skybg_zm_10_20_ph.txt'
                    wvc[s] = 1.0
                    am[s] = 2.0
                end
                3 : begin
                    spectrum = '~/skybright/mk_skybg_zm_30_10_ph.txt'
                    wvc[s] = 3.0
                    am[s] = 1.0
                end
                4 : begin
                    spectrum = '~/skybright/mk_skybg_zm_30_15_ph.txt'
                    wvc[s] = 3.0
                    am[s] = 1.5
                end
                else : begin
                    spectrum = '~/skybright/mk_skybg_zm_30_20_ph.txt'
                    wvc[s] = 3.0
                    am[s] = 2.0
                end
            endcase
            readcol, spectrum, lambda, photflux, numline=80001, $
              comment='#', format='f,f', /silent
            lambda=lambda/1000.0
            lambdaarr[*,s]=lambda
            fluxarr[*,s]=photflux
        endif

;Sky flux
        if f gt 0 then begin
            lambda=lambdaarr[*,s]
            photflux=fluxarr[*,s]
        endif
        flux = photflux * 1.e9 * 6.63e-34 * 2.998e8 / lambda
        totalflux=synthflux(lambda,flux,flambda,fpass,alambda,apass,$
                            /noair,/nograph)
        if keyword_set(showflux) then print, 'Total flux is', totalflux
        mag = magc - 2.5*alog10(totalflux)
        print, 'Magnitude is', mag
        
;Store data
        i = (f*6)+s
        filtarr[i]=filt
        vegafluxarr[i]=vgflux
        wvcarr[i]=wvc[s]
        amarr[i]=am[s]
        totalfluxarr[i]=totalflux
        magarr[i]=mag

    endfor
endfor

;Print data to file
textout='~/skybright/skymags.txt'
forprint, filtarr, vegafluxarr, wvcarr, amarr, totalfluxarr, magarr, $
  textout=textout, $
  format='(a8,3x,e11.5,3x,f3.1,3x,f3.1,3x,e10.4,3x,f7.4)', $
  comment='#Filter    Vega flux     WVC   AM    Flux         Mag', /silent

END
