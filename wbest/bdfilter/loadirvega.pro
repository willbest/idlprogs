PRO LOADIRVEGA, VLAMBDA, VFLUX, NORM=norm

;  Loads the model Vega from the Spextool data reduction package.
;  Selects only wavelengths 9,000 - 26,000 Angstroms into vlambda.
;  Converts wavelengths to 0.9 - 2.6 microns.
;  Converts flux units to W/m^2/micron 
;  Corresponding flux into vflux
;
;  Spextool: Cushing, Vacca, Rayner (2004, PASP 116, 362).
;
;  HISTORY
;  Written by Will Best (IfA), 6/23/2010
;  07/01/10 (WB):  Added NORM keyword, default is not to norm the flux.
;
;  KEYWORDS
;       NORM - When set, sets the maximum flux value for this data set
;              (which occurs at 0.9 microns) to 1.

if n_params() lt 2 then begin
   print, 'yIntagh! Syntax = loadirvega, wavelength_vector, flux_vector'
   return
endif

restore, '~/spectra/lvega99.sav'
vlambda=wvin[2722:8465]   ;select wavelengths
vlambda=vlambda*0.0001    ;convert units to micron
vflux=fvin[2722:8465]     ;select flux for selected wavelengths
vflux=vflux*10.           ;convert units to W/m^2/micron
if keyword_set(norm) then begin
    maxf=max(vflux)
    vflux=vflux/maxf
endif

END
