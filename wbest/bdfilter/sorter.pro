;PRO SORTER

;  Sorts the original list of SpeX Prism Library brown dwarfs with
;  near-infrared spectral types by that nir spectral type, and saves the
;  new list.
;
;  The SpeX Prism Spectral Libraries are maintained by Adam Burgasser
;  at http://www.browndwarfs.org/spexprism .
;
;  HISTORY
;  Written by Will Best (IfA), 08/16/2010
;

specindex = '~/spectra/spl_may2010_nirlist_old.txt'
readcol, specindex, spclass, list, cal, nirst, comment='#', $
  format='a,a,f,i', /silent
leng = n_elements(list)
si = sort(nirst)
spclass = spclass[si]
list = list[si]
cal = cal[si]
nirst = nirst[si]

newindex = '~/spectra/spl_may2010_nirlist.txt'
forprint, spclass, list, cal, nirst, textout=newindex, $
  format='(a1,2x,a58,2x,e13,2x,i2)', $
  comment='#SC Name                                                       Calib Const   NIR ST'

end
