FUNCTION ISNUMBER, X

;  Determines whether an array (or scalar) is a numeric type.
;  Returns 1 if numeric, 0 if non-numeric.
;
;  HISTORY
;  Ganked from RosettaCode by Will Best (IfA), 07/15/2010
;  http://rosettacode.org/wiki/Determine_if_a_string_is_numeric#IDL
;        as called for in Mike Liu's SPTYPENUM function.

on_ioerror, false
x=float(x)
return, 1
false : return, 0

END
