FUNCTION AIRPHO, LAMBDA, FLUX, FILLAMBDA, FILPASS

;   Establish the region in which to work
lower = max([min(lambda),min(fillambda)])
upper = min([max(lambda),max(fillambda)])

;   Map the filter's lambda variable onto the spectrum's lambda
fitind=where((lambda ge lower) and (lambda le upper))
;fitind=[min(fitind)-1,fitind,max(fitind)+1]
if min(fitind) gt 0 then fitind=[min(fitind)-1,fitind]
if max(fitind) lt (n_elements(lambda) - 1) then fitind=[fitind,max(fitind)+1]
fitlambda=lambda[fitind]
fitflux=flux[fitind]

fitfilind=where((fillambda ge lower) and (fillambda le upper))
fitfillambda=fillambda[fitfilind]
fitfilpass=filpass[fitfilind]

mapind=fitfillambda-min(fitlambda)
;maxmapind=max(fitlambda)-min(fitlambda)
;mapind=mapind/maxmapind
mapind=mapind/(upper-lower)
lnum=n_elements(fitlambda)
mapind=mapind*(lnum-1)

;   Determine the spectral flux through the filter
newflux=interpol(fitflux, fitlambda, fitfillambda)
;newflux=interpolate(fitflux, mapind)
filterflux=newflux*fitfilpass
newspec=[[fitfillambda],[filterflux]]

return, newspec

END
