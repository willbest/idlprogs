showflux=1
;PRO SPEXSCALE, SPECINDEX, NOGRAPH=nograph, SHOWFLUX=showflux, $
;                          SILENT=silent

;  Determines the calibrating constant for a set of spectra and a
;  given filter, using mag(Vega) = 0.0 as the standard.
;
;  Current default is to use the SpeX Prism library.
;
;  HISTORY
;  Written by Will Best (IfA), 08/13/2010
;
;  KEYWORDS
;       NOGRAPH - Suppress the pretty graphs
;       SHOWFLUX - Print calculated flux for each star processed
;       SILENT - suppress other outputs
;
;  USES
;      calibconst
;      synthflux

;  Establish spectrum index
if n_elements(specindex) eq 0 then specindex='default'
case specindex of
    'default' : begin
        specindex = '~/spectra/spl_may2010_list.txt'
        readcol, specindex, spclass, list, comment='#', format='a,a', /silent
    end
    else : begin
        readcol, specindex, spclass, list, comment='#', format='a,a', /silent
    end
endcase

;  Establish filters, begin loop
fparr = ['mkoj', 'mkoh', 'mkok', '2massj', '2massh', '2massks']
fnarr = ['MKO J', 'MKO H', 'MKO K', '2MASS J', '2MASS H', '2MASS Ks']
fbarr = ['J', 'H', 'Ks', 'J', 'H', 'Ks']

magarr=fltarr(n_elements(list), 6, /nozero)
calarr=fltarr(n_elements(list), 6, /nozero)
mainpath='~/spectra/'

if not keyword_set(nograph) then begin
    device, decomposed=1
    window, 0, xsize=800, ysize=600
    pos=getpos(0.7)
endif

for f=0,2 do begin
    filter = '~/filters/'+fparr[f]+'.txt'
    fn = fnarr[f]
    fb = fbarr[f]
    if not keyword_set(silent) then print, 'Using '+fn+' filter'
    readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

;Set up to read in one object's spectrum at a time
    i=0
    quit=' '
    while (quit ne 'q') and (i lt n_elements(list)) do begin

;Read in Spex Prism header and get magnitude
        path=mainpath+strlowcase(spclass[i])+'dwarf/'
        readcol, path+list[i], str, /silent, numline=13, $
          format='a', delim='@', comment='%'
        mag=-999
        for j=9,12 do begin
            tmp=strsplit(str[j],' ',/extract)
            if n_elements(tmp) ge 5 then $
              if (tmp[1] eq fb and tmp[2] eq 'magnitude') then mag=float(tmp[4])
        endfor    
        magarr[i,f] = mag

;Read in the spectrum file
        readcol, path+list[i], lambda, flux, format='f,f', /silent
        if not keyword_set(nograph) then begin
            origflux=flux
            origlambda=lambda
        endif
        name=strsplit(list[i],'_',/extract)   ;Nice pretty title
        name=name[1]

;Plot a spectrum in white
        if not keyword_set(nograph) then begin
            plot, lambda, flux, title='Spectrum for '+name, $
              xtitle='Wavelength (microns)', ytitle='Normalized flux', position=pos, $
              subtitle='Original spectrum is white!CFilter pass is red!CAtmosphere pass is blue!CResultant flux is green'
        endif

;Get the total flux and calibrate it
        totalflux = synthflux(lambda,flux,flambda,fpass, $ ;alambda,apass, $
                              /noair,nograph=nograph)
        cal = calibconst(totalflux,fnarr[f],mag)
        calarr[i,f] = cal
        if keyword_set(showflux) then $
          print, 'Total flux for '+name+' is'+string(cal*totalflux)+' W/m^2'

;Continue or quit
        i=i+1
;        print, 'Press any key for next spectrum, or q to quit.'
;        quit=get_kbrd()
    endwhile

endfor

delcolj=where(magarr[*,0] eq -999)    ;ignore Spex data with no given J mag
delcolh=where(magarr[*,1] eq -999)    ;ignore Spex data with no given H mag
delcolk=where(magarr[*,2] eq -999)    ;ignore Spex data with no given Ks mag
;delcolj=where(magarr[*,3] eq -999)    ;ignore Spex data with no given J mag
;delcolh=where(magarr[*,4] eq -999)    ;ignore Spex data with no given H mag
;delcolk=where(magarr[*,5] eq -999)    ;ignore Spex data with no given Ks mag
delindex=replicate(1,i)
delindex[delcolj]=0                   ;index of the data with no mag
delindex[delcolh]=0                   ;index of the data with no mag
delindex[delcolk]=0                   ;index of the data with no mag
keepcol=where(delindex eq 1)          ;index of good data

keepmag=magarr[keepcol,*]             ;mags given in SpeX data
keepcal=calarr[keepcol,*]             ;calibration const for stars with given mags
keepname=list[keepcol]
keepclass=spclass[keepcol]

;device, decomposed=1
;window, 1, xsize=800, ysize=600
;plot, keepmag, keepcalcmag, xtitle='Published J-band magnitude', $
;      ytitle='Calculated J-band magnitude', psym=5;, yrange=[30,40]
;plot, keepmag, keepflux, xtitle='Published J-band magnitude', $
;      ytitle='Calculated J-band flux', psym=5;, yrange=[30,40]

;best fit line
;bestfit=linfit(keepmag, keepcalcmag, yfit=bfit, prob=prob)
;bestfit=linfit(keepmag, keepflux, yfit=bfit, prob=prob)
;oplot, keepmag, bfit

;  Print data to file
textout=mainpath+'output/spexscales.txt'
;forprint, keepname, keepmag[*,0], keepcal[*,0], keepmag[*,1], keepcal[*,1], $
;  keepmag[*,2], keepcal[*,2], keepmag[*,3], keepcal[*,3], $
;  keepmag[*,4], keepcal[*,4], keepmag[*,5], keepcal[*,5], $
;  textout=textout ;, format='(a30,6x,f6.4,6x,f12.4)', $
;  comment='#Name                              Flux   Mag', /silent
forprint, keepclass, keepname, keepcal[*,0], keepcal[*,1], keepcal[*,2], $
  textout=textout, format='(a1,2x,a58,2x,e13,2x,e13,2x,e13)', $
  comment='#SC Name                                                       Jconst         Hconst         Ksconst'
;forprint, keepclass, keepname, keepcal[*,3], keepcal[*,4], keepcal[*,5], $
;  textout=textout, format='(a1,2x,a58,2x,e13,2x,e13,2x,e13)', $
;  comment='#SC Name                                                       Jconst         Hconst         Ksconst'

END
