showflux=1
nograph=1
;PRO SPEXSNCNT, FILTER, NOAIR=noair, NOGRAPH=nograph, SHOWFLUX=showflux, $
;               SILENT=silent, TIME=time

;  Given atmosphere, telescope efficiency and size, filter,
;  integration time, gain, read noise, and pixel scale,
;  calculates the signal-to-noise ratio for the SpeX Prism library
;;;  stars for which there are published J, H, and Ks magnitudes, and
;;;  known calibration constants for each spectrum.
;
;  HISTORY
;  Written by Will Best (IfA), 07/19/2010
;  08/13/2010:  Changed to use predicted abs mag for each spectral type
;
;  USES
;      dwarfabsmag
;      obsparms
;      signoicnt
;      sptypenum
;      synthflux
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       NOGRAPH - Suppress the pretty processing graphs
;       SHOWFLUX - Print calculated flux for each star processed
;       SILENT - suppress other outputs
;       TIME - integration time, default is 20 sec

;  Establish spectrum index
if n_elements(specindex) eq 0 then $
  specindex = '~/spectra/spl_may2010_maglist.txt'
readcol, specindex, spclass, list, jconst, hconst, kconst, $
         comment='#', format='a,a', /silent

;  Establish filter
fparr = ['mkoj', 'mkoh', 'mkok', '2massj', '2massh', '2massks']
fnarr = ['MKO J', 'MKO H', 'MKO K', '2MASS J', '2MASS H', '2MASS Ks']
fbarr = ['J', 'H', 'K', 'J', 'H', 'Ks']
tqearr = [0.75*0.70*0.92, 0.75*0.75*0.94, 0.75*0.69*0.96]
conarr = [[jconst], [hconst], [kconst]]

if n_elements(filter) eq 0 then fn = 'MKO J'
find = where (fnarr eq fn)
filter = '~/filters/'+fparr[find]+'.txt'
fband = fbarr[find]
calcon = conarr[*,find mod 3]
if not keyword_set(silent) then print, 'Using '+fn+' filter'

readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

;  Establish atmosphere
if not keyword_set(noair) then begin
    atmosphere = '~/airmass/ukirt_spexprism.txt'
    readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent
endif

;CFHT telescope parameters
tqe = (tqearr[find mod 3])[0]  ;need the [0], or tqe*[array] will always return a single number
area = 8.4   ;m^2
obsparms, gain,rn,pixscl,/wircj,silent=silent

if not keyword_set(time) then time = 5
time = float(time)

;Calculate sky emission flux
sky = '~/skybright/mk_skybg_zm_10_10_ph.txt'   ;airmass=1.0, water=1.0mm
readcol, sky, slambda, sflux, numline=80001, $
  comment='#', format='f,f', /silent
slambda = slambda/1000.         ;convert units to photons/s/micron/arcsec^2/m^2
sflux = 1000.*sflux
skyflux=tqe*area*time*sflux
skycounts=synthflux(slambda,skyflux,flambda,fpass,/noair,nograph=nograph)
if keyword_set(showflux) then $
  print, 'Total sky background flux is'+string(skycounts)+' photons/arcsec^2'
stop
;  Set up to read in one object's spectrum at a time
i=0
quit=' '
mainpath='~/spectra/'

nirstarr=fltarr(n_elements(list),/nozero)
optstarr=fltarr(n_elements(list),/nozero)
magarr=fltarr(n_elements(list),/nozero)
namearr=strarr(n_elements(list))
snarr=fltarr(n_elements(list),/nozero)

while (quit ne 'q') and (i lt n_elements(list)) do begin

;Read in Spex Prism header
    path=mainpath+strlowcase(spclass[i])+'dwarf/'
    readcol, path+list[i], str, /silent, numline=13, $
      format='a', delim='@', comment='%'

;Get spectral types and magnitude from SpeX Prism file
    nirst=99
    optst=99
    mag=-999
    for j=7,12 do begin
        tmp=strsplit(str[j],' ',/extract)
        if n_elements(tmp) ge 5 then begin
            if tmp[1] eq 'Near' and tmp[2] eq 'infrared' then begin
                nirststr=strsplit(tmp[5],'+:',/extract)
                nirst=sptypenum(nirststr[0],/silent)
            endif
            if tmp[1] eq 'Optical' and tmp[2] eq 'spectral' then begin
                optststr=strsplit(tmp[4],'+:',/extract)
                optst=sptypenum(optststr[0],/silent)
            endif
            if tmp[1] eq fband and tmp[2] eq 'magnitude' then mag=float(tmp[4])
        endif
    endfor    
    nirstarr[i]=nirst
    optstarr[i]=optst
    magarr[i]=mag

;Read in the spectrum file, and calibrate it
    readcol, path+list[i], lambda, flux, format='f,f', /silent
    if not keyword_set(nograph) then begin
        origflux=flux
        origlambda=lambda
    endif
    flux = flux * calcon[i]
    name=strsplit(list[i],'_',/extract) ;Nice pretty title
    name=name[1]
    namearr[i]=name

;Plot a spectrum in white
    if not keyword_set(nograph) then begin
        device, decomposed=1
        window, 0, xsize=800, ysize=600
        pos=getpos(0.7)
        plot, origlambda, origflux, title='Spectrum for '+name, $
          xtitle='Wavelength (microns)', ytitle='Normalized flux', position=pos, $
          subtitle='Original spectrum is white!CFilter pass is red!CAtmosphere pass is blue!CResultant flux is green'
    endif

;Determine photon flux through filter and atmosphere
    objflux=(1.e-6/(6.63e-34*2.998e8)*tqe*area*time*flux)*lambda
    objcounts=synthflux(lambda,objflux,flambda,fpass,alambda,apass, $
                    noair=noair,/nograph)
    if keyword_set(showflux) then $
      print, 'Total flux for '+name+' is'+string(objcounts)+' photons'

;Calculate signal to noise!
    sn = signoicnt(objcounts,skycounts,gain,rn,pixscl)
    if keyword_set(showflux) then $
      print, 'S/N for '+name+' is'+string(sn)
    snarr[i] = sn

;Pretty graphs!
    if not keyword_set(nograph) then begin
        graphflux=synthflux(origlambda,origflux,flambda,fpass,$
                            alambda,apass,noair=noair)
    endif

;Continue or quit
    i=i+1
;    print, 'Press any key for next spectrum, or q to quit.'
;    quit=get_kbrd()
endwhile

;delcol=where(nirstarr eq 99)       ;ignore Spex data with no given NIR ST
delcol=where(optstarr eq 99)        ;ignore Spex data with no given Opt ST
delindex=replicate(1,i)
delindex[delcol]=0                  ;index of the data with no ST
keepcol=where(delindex eq 1)        ;index of good data

;keepnirst=nirstarr[keepcol]
keepoptst=optstarr[keepcol]
keepmag=magarr[keepcol]
keepname=namearr[keepcol]
keepsn=snarr[keepcol]

device, decomposed=1
window, 1, xsize=800, ysize=600
;plot, keepmag, alog10(keepsn), xtitle='Published J-band Magnitude', $
;      ytitle='log(Signal-to-Noise Ratio)', psym=5, /ynozero
;plot, keepnirst, keepsn, xtitle='Published J-band Near-IR Spectral Type', $
;      ytitle='Signal-to-Noise Ratio', psym=5, xrange=[8,31], xstyle=1
plot, keepoptst, keepsn, xtitle='Published J-band Optical Spectral Type', $
      ytitle='Signal-to-Noise Ratio', psym=5, xrange=[8,31], xstyle=1, /ylog

;Best fit line
;bestfit=linfit(keepmag, alog10(keepsn), yfit=bfit, prob=prob)
;oplot, keepmag, bfit

;Print data to file
textout=mainpath+'output/sncnt.txt'
;forprint, keepname, keepmag, keepnirst, keepsn, textout=textout, $
forprint, keepname, keepmag, keepoptst, keepsn, textout=textout, $
;  format='(a30,3x,f7.4,3x,f3.0,3x,f7.0)', $
  format='(a30,3x,f7.4,3x,i2,3x,i6)', $
  comment='#Name                            Mag       ST     S/N'

END
