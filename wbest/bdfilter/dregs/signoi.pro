FUNCTION SIGNOI;, SPECTRUM, FILTER, ATMOSPHERE, $
                ; NOAIR=noair, NOFILTER=nofilter, SHOWFLUX=showflux, $
                 ;_EXTRA=extra_keywords

;  Given inputs of spectrum, calibration constant
;  caluclates the signal-to-noise ratio for the SpeX Prism library
;  stars for which there are published J, H, and Ks magnitudes.
;
;  HISTORY
;  Written by Will Best (IfA), 7/8/2010 in progress
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       NOFILTER - Don't use any filter
;       SHOWFLUX - Print calculated flux for each star processed
;
;  USES
;       synthflux

mag
tint
sigmatot

noisemag = -2.5*alog10(sigmatot/tint)
sn = 10.^((noisemag-mag)/2.5)

return, sn

END
