FUNCTION SKYMAG, SPECTRUM, FILTER, ATMOSPHERE, $
                 SHOWFLUX=showflux, _EXTRA=extra_keywords

;  Converts the emission spectrum of the sky in infrared into a
;  magnitude, calibrated by setting mag(Vega) = 0.0.
;  Uses six sky backround files by Lord (1992) from Gemini Observatory.
;  http://www.gemini.edu/sciops/telescopes-and-sites/observing-condition-constraints/ir-background-spectra
;
;  HISTORY
;  Written by Will Best (IfA), 7/14/2010
;
;  USES
;       synthflux
;       vegaflux
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       SHOWFLUX - Display calculated flux for each star processed

;  Establish spectrum, filter, and atmosphere files
if n_elements(spectrum) eq 0 then spectrum='~/skybright/mk_skybg_zm_10_10_ph.txt'
readcol, spectrum, lambda, photflux, numline=80001, $
         comment='#', format='f,f', /silent
lambda=lambda/1000.0

;if n_elements(filter) eq 0 then filter = '~/filters/mkok.txt'
if n_elements(filter) eq 0 then filter = '~/filters/irtest/k.txt'
readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

atmosphere = '~/airmass/ukirt_spexprism.txt'
readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent

;Determine Vega flux, and magnitude constant for mag(Vega) = 0.0
vgflux = vegaflux(flambda,fpass,alambda,apass,showflux=showflux,$
                  /nograph,_extra=extra_keywords)
;vgflux = vegaflux(filter,/photons,/nograph,showflux=showflux,$
;                  _extra=extra_keywords)

;Sky flux
flux = photflux * 1.e3 * 1.e6 * 6.63e-34 * 2.998e8 / lambda
		; 1.e3 because changed units from /nm to /micron
		; 1.e6 because dividing by lambda whose unit is microns
;flux = 1000. * photflux
totalflux=synthflux(lambda,flux,flambda,fpass,/noair,/nograph)
if keyword_set(showflux) then print, 'Total flux is', totalflux
mag = 2.5 * alog10(vgflux / totalflux)
print, 'Sky magnitude is', mag

return, mag

END
