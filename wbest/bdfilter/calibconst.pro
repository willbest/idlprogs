FUNCTION CALIBCONST, FLUX, FILTER, MAG

;  Returns the constant needed to calibrate the flux for a spectrum
;  (FLUX) of a stated magnitude (MAG) through a filter (FILTER),
;  using the standard mag(Vega)=0.0 for that filter.
;
;  HISTORY
;      Written by Will Best (IfA), 07/15/2010
;
;  INPUTS
;      FLUX - Calculated total flux of spectrum through filter
;      FILTER - filter used
;      MAG - stated magnitude
;
;  OUTPUTS
;      SCALE - scaling coefficient. Multiply the original
;              spectrum's flux/wl or flux/f to obtain the
;              correctly calibrated spectrum.
;
;  USES
;      vegaflux (if needed)
;

case filter of
    'MKO J' : vgflux = 2.97822e-09
    'MKO H' : vgflux = 1.17594e-09
    'MKO K' : vgflux = 3.96906e-10
    '2MASS J' : vgflux = 3.08559e-09
    '2MASS H' : vgflux = 1.11774e-09
    '2MASS Ks' : vgflux = 4.19751e-10
    else : begin
        vgflux = vegaflux(filter,/noair,/nograph)
        print, 'Vega''s flux through filter '+filter+' is'+string(vgflux)+' W/m^2'
    end
endcase

expflux = vgflux * 100^(-mag/5)
scale = expflux / flux

return, scale

END
