;PRO MAKEFILT

;  Creates a square filter (with rounded corners).
;
;  HISTORY
;  Written by Will Best (IfA), 08/09/2010
;  08/16/2010:  Added band codes:
;               J = 1
;               H = 2
;               K = 4
;               Y = 8
;               These are summative, e.g. 5 means J and K bands

device, decomposed=1
window, 0, xsize=600, ysize=450
pos=getpos(0.7)

name=''
read, name, prompt='Enter filter name: '

while name ne 'q' do begin
    path='~/filters/irtest/'+name+'.txt'

    code=0
    read, code, prompt='Enter band code (J=1, H=2, K=4, Y=8): '

    read, low, prompt='Low end: '
    low = float(low)
    l = low * 100.
    if l ne fix(l) then low = fix(l) / 100.

    read, high, prompt='High end: '
    high = float(high)
    h = high * 100.
    if h ne fix(h) then high = fix(h) / 100.

    size = (high - low) * 100 + 7
    lambda = findgen(size) * .01 + (low - .03)
    flux = fltarr(size,/nozero)
    
    flux[0] = 0.
    flux[1] = 0.
    flux[2] = 0.1
    flux[3] = 0.5
    flux[4] = 0.8
    for i=5, size-6 do flux[i] = 0.9
    flux[size-5] = 0.8
    flux[size-4] = 0.5
    flux[size-3] = 0.1
    flux[size-2] = 0.
    flux[size-1] = 0.

    Plot, lambda, flux, title=name+' filter',  position=pos, $
      xtitle='Wavelength (microns)', ytitle='Transmission'

    title = name
    strput, title, strupcase(strmid(name,0,1))
    header = [['#TRIAL '+title+' - TOTAL RESPONSE'],$
              ['#name: '+title],$
              ['#Band code: '+strtrim(code,1)],$
              ['#'],$
              ['#Wavelength   Transmission'],$
              ['#=========================']]

    forprint, lambda, flux, textout=path, /silent, $
      format='(3x,f6.4,7x,f6.4)', $
      comment=header

    read, name, prompt='Enter next filter name, or q to quit: '

endwhile

END
