FUNCTION CALFLUX, LAMBDA, FLUX, FILTER, MAG

;  Returns the calibrated flux for a spectrum (FLUX) of a
;  stated magnitude (MAG) through a filter (FILTER), using
;  the standard mag(Vega)=0.0 for that filter.
;
;  INPUTS
;      LAMBDA - wavelength of spectrum
;      FLUX - flux/wv of spectrum
;      FILTER - filter used
;      MAG - stated magnitude
;
;  OUTPUTS
;      NEWFLUX - cailbrated flux/wv of spectrum
;
;  USES
;      synthflux
;      vegaflux (if needed)
;
;  HISTORY
;      Written by Will Best (IfA), 07/15/2010
;

case filter of
    '~/filters/mkoj.txt' : vgflux = 2.97822e-09
    '~/filters/mkoh.txt' : vgflux = 1.17594e-09
    '~/filters/mkok.txt' : vgflux = 3.96906e-10
    '~/filters/2massj.txt' : vgflux = 3.08559e-09
    '~/filters/2massh.txt' : vgflux = 1.11774e-09
    '~/filters/2massks.txt' : vgflux = 4.19751e-10
    else : begin
        vgflux=vegaflux(filter,/noair,/nograph)
        print, 'Vega''s flux through filter '+filter+' is'+string(vgflux)+' W/m^2'
    end
endcase

expflux = vgflux * 100^(-mag/5.)

readcol, filter, flambda, fpass, comment='#', format='f,f', /silent
totalflux = synthflux(lambda,flux,flambda,fpass, $ ;alambda,apass, $
                              /noair,/nograph)

newflux = flux * (expflux / totalflux)

return, newflux

END
