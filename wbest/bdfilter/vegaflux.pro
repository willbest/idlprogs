FUNCTION VEGAFLUX, FLAMBDA, FPASS, ALAMBDA, APASS, $
                   NOAIR=noair, NOFILTER=nofilter, PHOTONS=photons, $
                   SHOWFLUX=showflux, _EXTRA=extra_keywords

;  Returns the total flux for Vega, optionally using a filter and/or
;  atmosphere.  Uses the model Vega spectrum in the Spextool data
;  reduction package.
;
;  Spextool: Cushing, Vacca, Rayner (2004, PASP 116, 362).
;
;  HISTORY
;  Written by Will Best (IfA), 7/14/2010
;  07/18/2010:  Added PHOTONS keyword and routine
;
;  USES
;       loadirvega
;       synthflux
;
;  INPUTS
;       FLAMBDA - Wavelength array for filter.
;       FPASS - Pass coefficient array for filter.  Each value
;                 should lie between 0 and 1 inclusive.
;       ALAMBDA - Wavelength array for atmosphere.
;       APASS - Pass coefficient array for atmosphere.  Each value
;                 should lie between 0 and 1 inclusive.
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       NOFILTER - Don't use any filter
;       PHOTONS - return flux in photons/sec/m^2
;       SHOWFLUX - Print calculated flux for each star processed

;  Establish filter and atmosphere files
if not keyword_set(nofilter) then begin
    if n_elements(flambda) eq 0 then begin
        print, 'No filter given!'
        return, -999
    endif
endif

if not keyword_set(noair) then begin
    if n_elements(alambda) eq 0 then begin
        print, 'No atmosphere given!'
        return, -999
    endif
endif

;Determine Vega flux, and magnitude constant for mag(Vega) = 0.0
loadirvega, vlambda, vflux, _extra=extra_keywords
units=' W/m^2'
if keyword_set(photons) then begin
    vphoflux=(1.e-6/(6.63e-34*2.998e8))*vflux*vlambda
    units=' photons/sec/m^2'
endif else vphoflux=vflux
vgflux=synthflux(vlambda,vphoflux,flambda,fpass,alambda,apass,$
                    NOAIR=noair,NOFILTER=nofilter,_extra=extra_keywords)
if keyword_set(showflux) then $
   print, 'Total flux for Vega is'+string(vgflux)+units

return, vgflux

END
