FUNCTION ZEROPOINT, VGFLUX, TIME

;  Determines the zeropoint (magnitude of a 1 ADU/sec object), given
;  integration time and Vega's flux for the instrument being used.
;
;  HISTORY
;  Written by Will Best (IfA), 7/18/2010
;
;  INPUTS
;       VGFLUX - Vega's flux, in photons
;       TIME - Integration time
;

z = 2.5*alog10(vgflux/time)

return, z

END
