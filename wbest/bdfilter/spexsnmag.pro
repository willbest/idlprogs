;PRO SPEXSNMAG, FILTER, NOAIR=noair, NOGRAPH=nograph, SHOWFLUX=showflux, $
;               SILENT=silent, TIME=time

;  Given atmosphere, telescope efficiency and size, filter,
;  integration time, gain read noise, and pixel scale,
;  calculates the signal-to-noise ratio for the SpeX Prism library
;  stars for which there are published near-infrared spectral types.
;
;  HISTORY
;  Written by Will Best (IfA), 07/19/2010
;  08/13/2010:  Changed to use predicted abs mag for each spectral type
;
;  USES
;      dwarfabsmag
;      obsparms
;      signoimag
;      sptypenum
;      synthflux
;      vegaflux
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       NOGRAPH - Suppress the pretty processing graphs
;       SHOWFLUX - Print calculated flux for each star processed
;       SILENT - suppress other outputs
;       TIME - integration time, default is 20 sec

;  Establish spectrum index
specindex = '~/spectra/spl_may2010_nirlist.txt'
readcol, specindex, spclass, list, cal, nirst, comment='#', format='a,a', /silent

;  Establish filter
fparr = ['mkoj', 'mkoh', 'mkok']
fnarr = ['MKO J', 'MKO H', 'MKO K']
fbarr = ['J', 'H', 'K']
tqearr = [0.75*0.70*0.92, 0.75*0.75*0.94, 0.75*0.69*0.96]

if n_elements(filter) eq 0 then fn = 'MKO K'
find = where (fnarr eq fn)
filter = '~/filters/'+(fparr[find])[0]+'.txt'
fband = fbarr[find]
if not keyword_set(silent) then print, 'Using '+fn+' filter'

readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

;  Establish atmosphere
if not keyword_set(noair) then begin
    atmosphere = '~/airmass/ukirt_spexprism.txt'
    readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent
endif

;CFHT telescope parameters
tqe = (tqearr[find])[0]  ;need the [0], or tqe*[array] will always return a single number
area = 8.4   ;units are m^2
obsparms, gain,rn,pixscl,/wircj,/silent
seeing = 0.6

if not keyword_set(time) then time = 20
time = float(time)

;Determine Vega flux, in Watts and photons, and zeropoint
vgflux = vegaflux(flambda, fpass, alambda, apass, noair=noair)
vphoflux = tqe * area * time * vegaflux(flambda,fpass,alambda,apass,$
                                        noair=noair,/photons)
zpt = zeropoint(vphoflux,time)

;Calculate sky background magnitude
sky = '~/skybright/mk_skybg_zm_10_10_ph.txt'   ;airmass=1.0, water=1.0mm
smag = skymag(sky,filter)

;  Set up to read in one object's spectrum at a time
i=0
quit=' '
mainpath='~/spectra/'

magarr=fltarr(n_elements(list),/nozero)
snarr=fltarr(n_elements(list),/nozero)

if not keyword_set(nograph) then begin
    device, decomposed=1
    window, 0, xsize=800, ysize=600
    pos=getpos(0.7)
endif

while (quit ne 'q') and (i lt n_elements(list)) do begin

;Read in the spectrum file, and calibrate it
    path=mainpath+strlowcase(spclass[i])+'dwarf/'
    readcol, path+list[i], lambda, flux, format='f,f', /silent
    if not keyword_set(nograph) then begin
        origflux=flux
        origlambda=lambda
    endif
    flux = flux * cal[i]

    name=strsplit(list[i],'_',/extract) ;Nice pretty title
    name=name[1]

;Plot a spectrum in white
    if not keyword_set(nograph) then begin
        plot, origlambda, origflux, title='Spectrum for '+name, $
          xtitle='Wavelength (microns)', ytitle='Normalized flux', position=pos, $
          subtitle='Original spectrum is white!CFilter pass is red!CAtmosphere pass is blue!CResultant flux is green'
    endif

;Determine magnitude of object through filter and atmosphere
    objflux=synthflux(lambda,flux,flambda,fpass,alambda,apass, $
                      noair=noair,/nograph)
    mag = 2.5 * alog10(vgflux / objflux)
    if keyword_set(showflux) then $
      print, 'Magnitude for '+name+' is'+string(mag)
    magarr[i] = mag

;Calculate signal to noise!
    sn = signoimag(mag,smag,zpt,time,gain,rn,pixscl,seeing)
    if keyword_set(showflux) then $
      print, 'S/N for '+name+' is'+string(sn)
    snarr[i] = sn

;Pretty graphs!
    if not keyword_set(nograph) then begin
        graphflux=synthflux(origlambda,origflux,flambda,fpass,alambda,apass,noair=noair)
    endif

;Continue or quit
    i=i+1
;    print, 'Press any key for next spectrum, or q to quit.'
;    quit=get_kbrd()
endwhile

;delcol=where((nirstarr lt 26) or (nirstarr gt 30))
delindex=replicate(1,i)
;delindex[delcol]=0                  ;index of the data with no ST
keepcol=where(delindex eq 1)        ;index of good data

keepnirst=nirst[keepcol]
keepmag=magarr[keepcol]
keepname=list[keepcol]
keepsn=snarr[keepcol]

device, decomposed=1
window, 1, xsize=800, ysize=600
;plot, keepmag, alog10(keepsn), xtitle='Published J-band Magnitude', $
;      ytitle='log(Signal-to-Noise Ratio)', psym=5, /ynozero
plot, keepnirst, keepsn, xtitle='Published J-band Near-IR Spectral Type', $
      ytitle='Signal-to-Noise Ratio', psym=5;, xrange=[24,30], xstyle=1

;Best fit line
;bestfit=linfit(keepmag, alog10(keepsn), yfit=bfit, prob=prob)
;oplot, keepmag, bfit

;Print data to file
textout=mainpath+'output/sn.txt'
forprint, keepname, keepmag, keepnirst, keepsn, textout=textout, $
;forprint, keepname, keepmag, keepoptst, keepsn, textout=textout, $
;  format='(a30,3x,f7.4,3x,f3.0,3x,f7.0)', $
  format='(a30,3x,f7.4,3x,i2,3x,i6)', $
  comment='#Name                            Mag       ST     S/N'

END
