FUNCTION FINDCICONST, BLUEFILTER, REDFILTER, ATMOSPHERE, $
                      NOAIR=noair, SILENT=silent

;  Procedure to determine the color indexing constant (see below) for
;  two filters, using Vega as the calibrating star and assuming
;  mag(Vega)=0.0 for both filters.
;
;  For filters A and B on the same source,
;  magA - magB = constant - 2.5*alog10(fluxA/fluxB)
;  That constant is the color indexing constant, and it is equal to
;     2.5*alog10(fluxA(Vega)/fluxB(Vega))
;
;  HISTORY
;  Written by Will Best (IfA), 06/29/2010
;
;  USES
;       filterpho
;       loadirvega
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       SILENT - Suppress the output for each star

;  Establish filter and atmosphere files
if n_elements(bluefilter) eq 0 then bluefilter='default'
case bluefilter of
    'willj' : begin
        bluefilter = '~/filters/willjfilter.txt'
        if not keyword_set(silent) then print, $
          'Using Will''s J filter for blue filter'
    end
    'default' : begin
        bluefilter = '~/filters/2massj.txt'
        if not keyword_set(silent) then print, $
          'Using 2MASS J filter for blue filter'
    end
    else : bluefilter=bluefilter
endcase
blue = 'J'
readcol, bluefilter, blambda, bpass, comment='#', format='f,f', /silent

if n_elements(redfilter) eq 0 then redfilter='default'
case redfilter of
    'willk' : begin
        redfilter = '~/filters/willkfilter.txt'
        if not keyword_set(silent) then print, $
          'Using Will''s K filter for red filter'
    end
    'default' : begin
        redfilter = '~/filters/2massks.txt'
        if not keyword_set(silent) then print, $
          'Using 2MASS Ks filter for red filter'
    end
    else : redfilter=redfilter
endcase
red = 'K'
readcol, redfilter, rlambda, rpass, comment='#', format='f,f', /silent

if not keyword_set(noair) then begin
    atmosphere = '~/airmass/ukirt_spexprism.txt'
    readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent
endif

;Load Vega's spectrum
loadirvega, lambda, flux

;Plot the spectrum and filters
device, decomposed=1
window, xsize=500, ysize=375
plot, lambda, flux, title='Spectrum for Vega', $
  xtitle='Wavelength (microns)', ytitle='Normalized flux'
oplot, blambda, bpass, color='FF0000'XL
oplot, rlambda, rpass, color='0000FF'XL

;Determine photometry for the blue filter.
filterpho, lambda,flux,blambda,bpass, newblambda,newbflux
if not keyword_set(noair) then begin
    oplot, alambda, apass, color='FFFF00'XL ;graph
    filterpho, newblambda,newbflux,alambda,apass, newblambda,newbflux
endif
oplot, newblambda, newbflux, color='00FF00'XL ;graph

;Determine photometry for the red filter.
filterpho, lambda,flux,rlambda,rpass, newrlambda,newrflux
if not keyword_set(noair) then begin
    filterpho, newrlambda,newrflux,alambda,apass, newrlambda,newrflux
endif
oplot, newrlambda, newrflux, color='00FF00'XL ;graph

;Calculate total resultant fluxes
bflux = tsum(newblambda,newbflux)
if not keyword_set(silent) then $
  print, 'Total '+blue+' flux is'+string(bflux)

rflux = tsum(newrlambda,newrflux)
if not keyword_set(silent) then $
  print, 'Total '+red+' flux is'+string(rflux)

;Calculate color index const
ciconst = 2.5*alog10(bflux/rflux)
if not keyword_set(silent) then $
  print, blue+'-'+red+' constant is'+string(ciconst)

return, ciconst

END
