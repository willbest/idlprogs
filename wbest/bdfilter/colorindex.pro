PRO COLORINDEX, SPECINDEX, BLUEFILTER, REDFILTER, ATMOSPHERE, $
                CICONST=ciconst, NOAIR=noair, SILENT=silent

;  Determines the color index of a star or set of stars,
;  given two filters.  Outputs to a file.
;
;  HISTORY
;  Written by Will Best (IfA), 06/29/2010
;
;  USES
;       filterpho
;       findciconst
;
;  KEYWORDS
;       CICONST - Sets the constant used to calculate the color index.
;                 If not set, a constant is determined using the model
;                 spectrum for Vega in SpexTools.
;       NOAIR - Ignore atmospheric absorption
;       SILENT - Suppress the output for each star

;  Establish spectrum index, filter, and atmosphere files
if n_elements(specindex) eq 0 then specindex='default'
case specindex of
    'vega' : begin
        loadirvega, lambda, flux
        list='vega'
    end
    'default' : begin
        specindex = '~/spectra/spl_may2010_list.txt'
        readcol, specindex, spclass, list, comment='#', format='a,a', /silent
    end
    else : begin
        readcol, specindex, spclass, list, comment='#', format='a,a', /silent
    end
endcase

;  Establish filter and atmosphere files
if n_elements(bluefilter) eq 0 then bluefilter='default'
case bluefilter of
    'willj' : begin
        bluefilter = '~/filters/willjfilter.txt'
        if not keyword_set(silent) then print, $
          'Using Will''s J filter for blue filter'
    end
    'default' : begin
        bluefilter = '~/filters/2massj.txt'
        if not keyword_set(silent) then print, $
          'Using 2MASS J filter for blue filter'
    end
    else : bluefilter=bluefilter
endcase
blue = 'J'
readcol, bluefilter, blambda, bpass, comment='#', format='f,f', /silent

if n_elements(redfilter) eq 0 then redfilter='default'
case redfilter of
    'willk' : begin
        redfilter = '~/filters/willkfilter.txt'
        if not keyword_set(silent) then print, $
          'Using Will''s K filter for red filter'
    end
    'default' : begin
        redfilter = '~/filters/2massks.txt'
        if not keyword_set(silent) then print, $
          'Using 2MASS Ks filter for red filter'
    end
    else : redfilter=redfilter
endcase
red = 'K'
readcol, redfilter, rlambda, rpass, comment='#', format='f,f', /silent

if not keyword_set(noair) then begin
    atmosphere = '~/airmass/ukirt_spexprism.txt'
    readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent
endif

;if not keyword_set(ciconst) then ciconst=1.43588
if not keyword_set(ciconst) then $
  ciconst=findciconst(bluefilter,redfilter,atmosphere,$
                      NOAIR=keyword_set(noair),/SILENT)

;  Set up to read in one object's spectrum at a time
i=0
quit=' '
mainpath='~/spectra/'
namearr=' '
optstarr=' '
nirstarr=' '
ciarr=0

device, decomposed=1
window, xsize=500, ysize=375

while (quit ne 'q') and (i lt n_elements(list)) do begin
    case specindex of
        'vega' : name='Vega'
        else : begin
            path=mainpath+strlowcase(spclass[i])+'dwarf/'
            readcol, path+list[i], lambda, flux, format='f,f', /silent
            name=strsplit(list[i],'_',/extract)
            name=name[1]
        end
    endcase

;Plot a spectrum in white
    plot, lambda, flux, title='Spectrum for '+name, $
      ;xrange=[1.9,2.5], yrange=[0.0,1.2], ystyle=1, $
      xtitle='Wavelength (microns)', ytitle='Normalized flux'
    oplot, blambda, bpass, color='FF0000'XL
    oplot, rlambda, rpass, color='0000FF'XL

;Determine photometry for the blue filter.
    filterpho, lambda,flux,blambda,bpass, newblambda,newbflux
    if not keyword_set(noair) then begin
        oplot, alambda, apass, color='FFFF00'XL ;graph
        filterpho, newblambda,newbflux,alambda,apass, newblambda,newbflux
    endif
    oplot, newblambda, newbflux, color='00FF00'XL ;graph

;Determine photometry for the red filter.
    filterpho, lambda,flux,rlambda,rpass, newrlambda,newrflux
    if not keyword_set(noair) then begin
        filterpho, newrlambda,newrflux,alambda,apass, newrlambda,newrflux
    endif
    oplot, newrlambda, newrflux, color='00FF00'XL ;graph

;Calculate total resultant fluxes
    bflux = tsum(newblambda,newbflux)
    if not keyword_set(silent) then $
      print, 'Total '+blue+' flux is'+string(bflux)

    rflux = tsum(newrlambda,newrflux)
    if not keyword_set(silent) then $
      print, 'Total '+red+' flux is'+string(rflux)

;Calculate color index
    ci = ciconst - 2.5*alog10(bflux/rflux)
    if not keyword_set(silent) then $
      print, blue+'-'+red+' for '+name+' is'+string(ci)
    ciarr=[ciarr,ci]
    namearr=[namearr,name]

;Store data
    nirst='?'
    optst='?'
    readcol, path+list[i], str, /silent, numline=15, $
      format='a', delim='@', comment='%'
    for j=7, 11 do begin
        tmp=strsplit(str[j],/extract)
        if n_elements(tmp) ge 5 then begin
            if tmp[1] eq 'Near' and tmp[2] eq 'infrared' then begin
                nirst=strsplit(tmp[5],'+:',/extract)
                nirst=nirst[0]
            endif
            if tmp[1] eq 'Optical' and tmp[2] eq 'spectral' then begin
                optst=strsplit(tmp[4],'+:',/extract)
                optst=optst[0]
            endif
        endif
    endfor    
    optstarr=[optstarr,optst]
    nirstarr=[nirstarr,nirst]

;Continue or quit
    i=i+1
;    print, 'Press any key for next spectrum, or q to quit.'
;    quit=get_kbrd()
endwhile

;Print data to file
textout=mainpath+'output/cindices.txt'
forprint, namearr, optstarr, nirstarr, ciarr, textout=textout, $
  format='(a30,3x,a4,3x,a4,3x,f6.2)', $
  comment='#Name                              Opt    NIR    J-K', /silent

END
