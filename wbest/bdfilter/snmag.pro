;nograph=1
;showflux=1
PRO SNMAG, FILTER, NOAIR=noair, NOFILTER=nofilter, NOGRAPH=nograph, $
           SHOWFLUX=showflux, SILENT=silent, TIME=time

;  Given atmosphere, telescope efficiency and size, filter,
;  integration time, gain, read noise, and pixel scale,
;  calculates the signal-to-noise ratio for the SpeX Prism library
;  brown dwarfs for which there are published near-infrared spectral
;  types and known calibration constants.
;  The spectra are grouped into six categories: early/mid/late L and
;  early/mid/late T.
;  Method:  zeropoint, object magnitude, sky magnitude
;
;  The SpeX Prism Spectral Libraries are maintained by Adam Burgasser
;  at http://www.browndwarfs.org/spexprism .
;
;  HISTORY
;  Written by Will Best (IfA), 08/10/2010
;  08/18/2010:  Added legends to plots and jpeg exporting
;
;  USES
;      loadcolors
;      obsparms
;      signoimag
;      sptypenum
;      synthflux
;      vegaflux
;      zeropoint
;
;  INPUTS
;       FILTER - Filter to be used.
;
;  KEYWORDS
;       NOAIR - Ignore atmospheric absorption
;       NOFILTER - Don't use any filter
;       NOGRAPH - Suppress the pretty processing graphs
;       SHOWFLUX - Print calculated flux for each star processed
;       SILENT - suppress other outputs
;       TIME - integration time, default is 10 sec


;  Establish atmosphere and sky emission
if not keyword_set(noair) then begin
    atmosphere = '~/airmass/ukirt_spexprism.txt'
    readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent
endif

sky = '~/skybright/mk_skybg_zm_10_10_ph.txt'   ;airmass=1.0, water=1.0mm
readcol, sky, slambda, sflux, numline=80001, $
         comment='#', format='f,f', /silent
slambda=slambda/1000.0
skyflux = sflux * 1.e3 * 1.e6 * 6.63e-34 * 2.998e8 / slambda
		; 1.e3 because changed units from /nm to /micron
 		; 1.e6 because dividing by lambda whose unit is microns

;  CFHT telescope parameters
area = 8.4   ;units are m^2 
obsparms, gain,rn,pixscl,/wircj,/silent
seeing = 0.6
tqearr = [0.75*0.70*0.92, 0.75*0.75*0.94, 0.75*0.69*0.96, 0.50*0.80*0.91]
;        [J, H, K, Y]

fp = ''
read, fp, prompt='Enter first filter: '
f = 0

repeat begin

;  Establish filter
filter = '~/filters/irtest/'+fp+'.txt'
readcol, filter, fstr, /silent, numline=3, format='a', $
  delim='@', comment='%'
fn = strmid(fstr[1],7)
if n_elements(fnarr) ne 0 then fnarr=[fnarr,fn] else fnarr=fn
if not keyword_set(silent) then print, 'Using '+fn+' filter'

;Telescope throughput
x = 0.
code = strmid(fstr[2],12)
if (code and 1) ne 0 then x = [x, tqearr[0]]
if (code and 2) ne 0 then x = [x, tqearr[1]]
if (code and 4) ne 0 then x = [x, tqearr[2]]
if (code and 8) ne 0 then x = [x, tqearr[3]]
tqe = mean(x[1:*])
;tqe = float(strmid(fstr[2],6))

readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

if not keyword_set(time) then time = 20
time = float(time)

;Determine Vega flux, in Watts and photons, and zeropoint
vgflux = vegaflux(flambda,fpass,alambda,apass,showflux=showflux,$
                  nofilter=nofilter,/nograph)
vphoflux = vegaflux(flambda,fpass,alambda,apass,nofilter=nofilter,$
                    /nograph,/nonorm,/photons)*tqe*area*time
zpt = zeropoint(vphoflux,time)
if not keyword_set(silent) then print, 'zeropoint is', zpt

;Calculate sky emission magnitude
;skyflux = skyflux*tqe*area*time
totalflux=synthflux(slambda,skyflux,flambda,fpass,/noair,/nograph,$
                    nofilter=nofilter)
if keyword_set(showflux) then print, 'Total sky flux is', totalflux
smag = 2.5 * alog10(vgflux / totalflux)
print, 'Sky magnitude is', smag

;  Set up to read in one object's spectrum at a time
g=0
quit=' '
mainpath='~/spectra/'
msnarr=fltarr(6,/nozero)
devarr=fltarr(6,/nozero)

while (quit ne 'q') and (g lt 6) do begin

    if not keyword_set(nograph) then begin
        device, decomposed=1
        window, 0, xsize=600, ysize=450
        pos=getpos(0.7)
    endif

;Establish spectrum index
    if g eq 0 then begin
        specindex = '~/spectra/spl_may2010_earlyLlist.txt'
    endif else if g eq 1 then begin
        specindex = '~/spectra/spl_may2010_midLlist.txt'
    endif else if g eq 2 then begin
        specindex = '~/spectra/spl_may2010_lateLlist.txt'
    endif else if g eq 3 then begin
        specindex = '~/spectra/spl_may2010_earlyTlist.txt'
    endif else if g eq 4 then begin
        specindex = '~/spectra/spl_may2010_midTlist.txt'
    endif else specindex = '~/spectra/spl_may2010_lateTlist.txt'

    readcol, specindex, spclass, list, cal, nirst, comment='#', $
      format='a,a,f,i', /silent
    leng = n_elements(list)
    snarr=fltarr(leng,/nozero)

    for i=0, (leng-1) do begin

;Read in the spectrum file, and calibrate it
        path=mainpath+strlowcase(spclass[i])+'dwarf/'
        readcol, path+list[i], lambda, flux, format='f,f', /silent
        if not keyword_set(nograph) then begin
            origflux=flux
            origlambda=lambda
        endif
        name=strsplit(list[i],'_',/extract) ;Nice pretty title
        name=name[1]
        flux = flux * cal[i]

;Plot a spectrum in white
        if not keyword_set(nograph) then begin
            Plot, origlambda, origflux, title='Spectrum for '+name, $
              xtitle='Wavelength (microns)', ytitle='Normalized flux', $
              position=pos, $
              subtitle='Original spectrum is white!CFilter pass is red!CAtmosphere pass is blue!CResultant flux is green'
        endif

;Determine magnitude of object through filter and atmosphere
        objflux=synthflux(lambda,flux,flambda,fpass,alambda,apass, $
                          noair=noair,nofilter=nofilter,/nograph)
        mag = 2.5 * alog10(vgflux / objflux)
        if keyword_set(showflux) then $
          print, 'Magnitude for '+name+' is'+string(mag)

;Calculate signal to noise!
        sn = signoimag(mag,smag,zpt,time,gain,rn,pixscl,seeing)
        if keyword_set(showflux) then $
          print, 'S/N for '+name+' is'+string(sn)
        snarr[i] = sn

;Pretty graphs!
        if not keyword_set(nograph) then begin
            graphflux=synthflux(origlambda,origflux,flambda,fpass,$
                                alambda,apass,noair=noair,nofilter=nofilter)
        endif

    endfor

    msnarr[g] = mean(snarr)
    print, 'Mean S/N is', msnarr[g]
    devarr[g] = stddev(snarr)
    print, 'Standard Deviation is', devarr[g]

;Continue or quit
    g=g+1
;    print, 'Press any key for next set of objects, or q to quit.'
;    quit=get_kbrd()
endwhile

if f ne 0 then begin
    msnl = [[msnl],[msnarr]]
    devl = [[devl],[devarr]]
endif else begin
    msnl = msnarr
    devl = devarr
endelse

read, fp, prompt='Enter next filter, or q to quit: '
f=f+1

endrep until fp eq 'q'

;Print data to file
textout='~/filters/output/sn.txt'
forprint, fnarr, msnl[0,*], devl[0,*], msnl[1,*], devl[1,*], $
  msnl[2,*], devl[2,*], msnl[3,*], devl[3,*], msnl[4,*], devl[4,*], $
  msnl[5,*], devl[5,*], textout=textout, $
  format='(a17,4x,f7.1,2x,f6.1,4x,f6.1,2x,f5.1,4x,f6.1,2x,f5.1,4x,f6.1,2x,f5.1,4x,f6.1,2x,f5.1,4x,f6.1,2x,f5.1)', comment='#          Filter     S/N     StdDev    using magnitudes'

;Plot the data
device, decomposed=0
window, 1, xsize=800, ysize=600
loadcolors
a = findgen(17) * (!pi*2/16.)
usersym, cos(a), sin(a), /fill
plot, msnl[*,0], xtitle='Brown Dwarf groups', xstyle=2, yrange=[0,max(msnl)], $
;plot, msnl[*,0], xtitle='Brown Dwarf groups', xstyle=2, yrange=[0,2000], $
  ytitle='Signal-to-Noise Ratio', /nodata, background=7, color=0, $
  xtickname=['Early L','Mid L','Late L','Early T','Mid T','Late T']

if f ge 7 then begin
    for u=0,5 do oploterror, msnl[*,u], devl[*,u]/2, $
      psym=-8, color=u+1, errcolor=u+1 
    for u=6,f-1 do oploterror, msnl[*,u], devl[*,u]/2, $
      psym=-8, color=u+2, errcolor=u+2 
    cset = [indgen(6)+1,indgen(f-6)+8]
endif else begin
    for u=0,f-1 do oploterror, msnl[*,u], devl[*,u]/2, $
      psym=-8, color=u+1, errcolor=u+1
    cset = indgen(f)+1
endelse
legend, fnarr,linestyle=0,outline=0,charsize=1.5,/right,$
  colors=cset,textcolors=cset

image = screenread()
write_jpeg, '~/filters/graphs/sn.jpg', image, true=1

END
