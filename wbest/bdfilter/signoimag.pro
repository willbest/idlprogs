FUNCTION SIGNOIMAG, OBJMAG, SKYMAG, ZPT, TIME, GAIN, RN, PIXSCL, SEEING, SAT

;  Calculates the signal-to-noise ratio for an object, using object 
;  and sky fluxes in terms of magnitudes.
;
;  USE
;      sn = signoimag(objmag,skymag,zpt,time,gain,rn,pixscl,seeing,sat)
;
;  HISTORY
;  Written by Will Best (IfA), 07/18/2010
;  07/20/2010 (WB): Added SEEING
;  08/13/2010 (WB): Changed default to circular aperature with radius seeing/2.
;
;  INPUTS
;      OBJMAG - magnitude of object
;      SKYMAG - magnitude of sky emission
;      ZPT - zeropoint (magnitude of 1 ADU/sec object)
;      TIME - integration time
;      GAIN - intrument gain
;      RN - instrument readnoise
;      PXSCL - instrument pixel scale
;      SEEING - size of box in which object lies
;      SAT - saturation point in ADU
;

objcounts = time * 10.^((zpt-objmag)/2.5)
objcounts = objcounts / 2.                     ;half of flux inside FWHM...
objmaga = objmag + .7528                       ;...so add .7528 to the magnitude

skycounts = time * 10.^((zpt-skymag)/2.5)
skycounts = skycounts * !pi * (seeing/2.)^2.    ;circular aperature of FWHM

skynoise2 = (skycounts+objcounts) / gain
;readnoise2 = (rn/gain)^2.*(pixscl)^2.
readnoise2 = (rn/gain)^2.*(seeing / pixscl)^2.
sigma = sqrt(skynoise2 + readnoise2)

;sn = objcounts / sigma

bigsigma = zpt + 2.5 * alog10(time / sigma)
sn = 10. ^ ((bigsigma - objmaga) / 2.5)

pixelcounts = (objcounts + skycounts) / (seeing/pixscl)^2 + (rn/gain)
if pixelcounts gt (.8 * sat * gain) then print, $
  'WARNING: Pixel wells over 80% full'

return, sn

END
