function sptypenum, specstr, reverse = reverse, silent=silent

;+
; convert a spectral type (string) into a number
; assumes "M0" = 0, "M1" = 1, "L0" = 10, etc.
; objects w/o a subclass (e.g. "M") are assigned subclass 0
;
; /reverse  given a list of numbers, convert to strings
;
; 03/16/01 M. Liu
; 11/17/01 MCL: returns 99 for bad spectral types
; 05/30/02 MCL: minor bug fix when encounters an invalid SpT
; 11/21/05 MCL: for /reverse, returns '-99' for value outside range
; 12/15/08 MCL: bug fix for /reverse - wasn't handling subclass=0 case ("M10" instead of "M0")
; 02/01/09 MCL: make sure to return a scalar value if only 1 element,
; not an array (IDL's annoying WHERE behavior)
; 07/15/10 WB: updated type-to-number portion
;
;  USES
;       isnumber
;-


;on_error, 2

classlist = ['O', 'B', 'A', 'F', 'G', 'K', 'M', 'L', 'T']
mpos = 6

n = n_elements(specstr)

if not(keyword_set(reverse)) then begin

    out = fltarr(n)
    for i = 0, n-1 do begin
        
        spt = strupcase(strmid(specstr[i], 0, 1))
        class = strmid(specstr[i], 1, strlen(specstr[i])-1)
        if (class eq '') then class = 0  ; if no subclass, set to 0
        w = where(classlist eq spt) 
        
        if (size(spt, /type) ne 7) or (isnumber(class) eq 0) then begin
            if not(keyword_set(silent)) then $
              message, 'bad format, string = "'+strc(specstr[i])+'"!', /info
            out(i) = 99
        endif else if (w[0] eq -1) then begin
            if not(keyword_set(silent)) then $
              message, 'invalid spectral type = "'+spt+'"', /info
            ;message, 'invalid spectral type = "'+commalist(w)+'"', /info
            out[i] = 99
        endif else $
          out[i] = (w-mpos)*10 + float(class)

    endfor

endif else begin

    out = strarr(n)
    for i = 0, n-1 do begin

        spt = floor(specstr(i)/10.)
        ;spt = floor(specstr(i)/10.)      ; removed 11/21/05
        ;class = abs(specstr(i) mod 10)   ; removed 11/05/05

        if (specstr(i) gt 0) then $
           class = specstr(i) mod 10 $
        else if (specstr(i) le 0) then $
           class = (10-(abs(specstr(i)) mod 10)) mod 10

        if (spt+mpos ge 0) and (spt+mpos lt n_elements(classlist)) then begin
            if class eq fix(class) then  $
              out(i) = classlist(spt+mpos) + strc(string(class, '(I4)')) $
              else $
              out(i) = classlist(spt+mpos) + strc(string(class, '(F4.1)'))
            ;if classlist(spt+mpos) eq 'T' then out(i) = '>L9'
        endif else $
          out(i) = '-99'   ; numerical SpT value outside plausible range

    endfor
    if (n eq 1) then out = out(0)

endelse


if n_elements(out) eq 1 then out = out(0)
return, out
end

