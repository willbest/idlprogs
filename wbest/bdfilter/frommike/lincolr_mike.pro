pro lincolr, silent = silent

; set color table for 6 colored lines
; XX/XX  Jeff Valenti(?)
;
; 08/08/06 M.C. Liu: added more colors
; 05/27/07 MCL: tweaked "olive" from {0.4,0.7,0.3} to {0.2,0.6,0.2} to look more green on page
; 03/02/08 MCL: added "med.blue" color
;               previous changes were made in a new program called LINCOLR2.PRO
;               now just update all the changed into the LINCOLR.PRO routine directly


;device,pseudo_color=8
if not(keyword_set(silent)) then $
  print, $
  ' 0: black, 1:white, 2:bright green, 3:red, 4:aqua, 5:yellow, 6:purple, 7:blue, 8:white, '+ $
            '9:dark green, 10:orange, 11:grey, 12:med.blue, 13:lt.grey, 14:dark grey, 15:med grey'
  ;' 0: black, 1:white, 2:green, 3:red, 4:aqua, 5:yellow, 6:purple, 7:blue, 8:white'


;; original
;red=  [0,1,0,1,0,1,1,0,1,.5,1]*255
;green=[0,1,1,0,1,1,0,0,1,.7,1]*255
;blue= [0,1,0,0,1,0,1,1,1,.3,1]*255
 

red=  [0, 1, 0, 1, 0, 1, 1, 0, 1, .2, 0.8, 0.3, 0,    0.7, 0.2, 0.5]*255
green=[0, 1, 1, 0, 1, 1, 0, 0, 1, .6, 0.4, 0.3, 0.65, 0.7, 0.2, 0.5]*255
blue= [0, 1, 0, 0, 1, 0, 1, 1, 1, .2, 0.0, 0.3, 1,    0.7, 0.2, 0.5]*255
 

tvlct,red,green,blue

return
end

