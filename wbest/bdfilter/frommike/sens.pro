pro Sens, mobj=mobj, Mabs=Mabs, dist=dist, Msbf=Msbf, $ ; object parms
          time=time, msky=msky, seeing=seeing, $        ; observing parms 
          zpt=zpt, pixscl=pixscl, gain=gain, rn=rn, $   ; instrument parms
          sat=sat, dc=dc, $
          ao=ao, ctio60=ctio60, ctio4m = ctio4m, $  ; instrument choice
          hst110m=hst110m, hsth=hsth, hst110w=hst110w, $
          hstk=hstk, hst207m = hst207m, hst237m = hst237m, $
          keckk=keckk, keckj=keckj, keckh=keckh, keckr=keckr, $
          lickj=lickj, lickh=lickh, lickk=lickk, $
          irtfj=irtfj, itrfh=irtfh, irtfk=irtfk, irtfl=irtfl, irtfm=irtfm,$
          vltj=vltj, vlth=vlth, vltk=vltk, $
          wircj=wircj, wirch=wirch, wircks=wircks, $
          help=help

;+
; Calculate S/N ratios for object detection,
; assuming that the sky frame used for subtraction is noise-free
;
; Default is that source fills a 1" box, adjust 'seeing' keyword to
; change this
;
; OPTIONAL INPUTS
; <Object related>
;	mobj:	apparent magnitude of object
;	Mabs:	absolute magnitude of object
;	dist:	distance to object in Mpc
;       Msbf:   absolute mag of SBFs
; <Observing related>
;	time:	integration time (seconds; 100 s default)
;       seeing: size of box for noise calculation (1" default)
;	msky:	sky brightness in mag/sq."
; <Instrument related>
;	zpt:	zeropoint (counts/sec from a mag=0 object)
;	pixscl: pixel scale ("/pixel)
;	gain:	instrument gain (e-/ADU)
;	rn:     instrument readnoise (e-)
;       sat:    pixel saturation (in ADU)
;       dc:     dark current (e-/sec/pixel)
; <Observatory+instrument choice>
;       ao      Lick 3-meter with Berkcam & LLNL AO Cam at K'
;	ctio60  CTIO 1.5-meter with CIRIM at Kshort
;	ctio4m  CTIO 4-meter with CIRIM at Kshort
;       hstj    NICMOS Camera 1 with F110W filter
;       hst110m NICMOS Camera 1 with F110M filter
;       hsth    NICMOS Camera 2 with F160W filter
;       hstk    NICMOS Camera 2 with F222M filter
;       keckr   Keck 10-m with LRIS at R
;	keckj	Keck 10-m with NIRC at J
;	keckh	Keck 10-m with NIRC at H
;	keckk	Keck 10-m with NIRC at K (default)
;	lickj	Lick 3-meter with LIRC2 at J
;	lickh	Lick 3-meter with LIRC2 at H
;	lickk	Lick 3-meter with LIRC2 at K'
;       irtf    IRTF w/NSFCAM at JHKLM
;       vlt     VLT w/ISSAC (short-wl array) at JHKs
;
; OUTPUTS
;	S/N of detection
;	1 sigma noise in a box of SEEING x SEEING size
;	integration time to where Poisson noise = read noise
;
; USES
;       obsparms
;
; HISTORY
; Written by MCL (UCB): 2/24/95 
; 03/05/97 (MCL): corrected error in calculating read & Poisson noise equal
; 03/07/97 (MCL): added dark current to calculation and saturation check
; 07/14/10 (WB): corrected error in calculating mobj from Mabs (line 87)
; 07/22/10 (WB): added calls to obsparms for WIRCam on CFHT
;-

if keyword_set(help) then begin
    print, 'pro ' + $
      'sens, [mobj=], [time(sec) = ], [Mabs = ], [dist(Mpc) = ], [msky ' + $
      '= ], [seeing = ]'  
    print, '    [zpt=],[pixscl=],[gain=],[readnoise=],
    retall
endif


; default parameters
if not(keyword_set(mobj)) then mobj = 16.
if not(keyword_set(time)) then time = 1000.
if not(keyword_set(seeing)) then seeing = 1.0
if not(keyword_set(Msbf)) then Msbf = -5.6
if keyword_set(Mabs) then begin
  if keyword_set(dist) then begin
	mobj = Mabs + 25. + 5.*alog10(dist)
  endif else begin
	message,'* need distance if give absolute Mag *'
  endelse
endif


; get observing parameters
if keyword_set(ao) then obsparms, gain, rn, pixscl, zpt, ms, ss, dd, /ao $
else if keyword_set(ctio60) then obsparms, ga, rr, pix, zz, ms, ss, dd, /ctio60 $
else if keyword_set(ctio4m) then obsparms, ga, rr, pix, zz, ms, ss, dd, /ctio4m $
;else if keyword_set(hstj) then obsparms, ga, rr, pix, zz, ms, ss, dd, /hstj $
else if keyword_set(hst110m) then obsparms, ga, rr, pix, zz, ms, ss, dd, /hst110m $
else if keyword_set(hsth) then obsparms, ga, rr, pix, zz, ms, ss, dd, /hsth $
else if keyword_set(hst110w) then obsparms, ga, rr, pix, zz, ms, ss, dd, /hst110w $
else if keyword_set(hstk) then obsparms, ga, rr, pix, zz, ms, ss, dd, /hstk $
else if keyword_set(hst207m) then obsparms, ga, rr, pix, zz, ms, ss, dd, /hst207m $
else if keyword_set(hst237m) then obsparms, ga, rr, pix, zz, ms, ss, dd, /hst237m $
else if keyword_set(keckj) then obsparms, ga, rr, pix, zz, ms, ss, dd, /keckj $
else if keyword_set(keckh) then obsparms, ga, rr, pix, zz, ms, ss, dd, /keckh $
else if keyword_set(keckr) then obsparms, ga, rr, pix, zz, ms, ss, dd, /keckr $
else if keyword_set(lickj) then obsparms, ga, rr, pix, zz, ms, ss, dd, /lickj $
else if keyword_set(lickh) then obsparms, ga, rr, pix, zz, ms, ss, dd, /lickh $
else if keyword_set(lickk) then obsparms, ga, rr, pix, zz, ms, ss, dd, /lickk $
else if keyword_set(irtfj) then obsparms, ga, rr, pix, zz, ms, ss, dd, /irtfj $
else if keyword_set(irtfh) then obsparms, ga, rr, pix, zz, ms, ss, dd, /irtfh $
else if keyword_set(irtfk) then obsparms, ga, rr, pix, zz, ms, ss, dd, /irtfk $
else if keyword_set(irtfl) then obsparms, ga, rr, pix, zz, ms, ss, dd, /irtfl $
else if keyword_set(irtfm) then obsparms, ga, rr, pix, zz, ms, ss, dd, /irtfm $
else if keyword_set(vltj) then obsparms, ga, rr, pix, zz, ms, ss, dd, /vltj $
else if keyword_set(vlth) then obsparms, ga, rr, pix, zz, ms, ss, dd, /vlth $
else if keyword_set(vltk) then obsparms, ga, rr, pix, zz, ms, ss, dd, /vltk $
else if keyword_set(wircj) then obsparms, ga, rr, pix, zz, ms, ss, dd, /wircj $
else if keyword_set(wirch) then obsparms, ga, rr, pix, zz, ms, ss, dd, /wirch $
else if keyword_set(wircks) then obsparms, ga, rr, pix, zz, ms, ss, dd, /wircks $
else  obsparms, ga, rr, pix, zz, ms, ss, dd, /keckk


; establish observing parms, allowing user to change some via keywords
if not(keyword_set(gain)) then gain = ga
if not(keyword_set(rn)) then rn = rr
if not(keyword_set(msky)) then msky = ms
if not(keyword_set(zpt)) then zpt = zz
if not(keyword_set(pixscl)) then pixscl = pix
if not(keyword_set(sat)) then sat = ss
if not(keyword_set(dc)) then dc = dd


; counts from sky, obj, and dark current in the specified box size
sky = 10.^((zpt-msky)/2.5) * time * seeing^(2.0)
obj = 10.^((zpt-mobj)/2.5) * time
darkc = dc/gain * time * (seeing/pixscl)^2.0


; noise in a 1" box measured in DN = 
;   quadrature sum of readnoise + Poisson noise (from obj and sky)
rnoise2 = 1. * (seeing/pixscl)^(2.0) * (rn/gain)^2.0
poiss2 = 1. * (sky+obj+darkc) / gain 
sigma = sqrt(rnoise2 + poiss2)
sigmamag = zpt + 2.5 * alog10(time/sigma) 

	
; signal to noise
sn = obj / sigma


; when does Poisson noise = read noise?
tbkgnd = rnoise2 / ((sky+darkc+obj)/gain/time)


; print results
print,'  --------- RESULTS -----------'
print, format = '(A,F7.2,A)', $
  '  S/N in ('+strc(seeing)+'")^2 box = ', (sn)
print, format = '(A,F7.2,A)', $
  '  noise in ('+strc(seeing)+'")^2 box = ', (sigmamag), ' mag'
print, format = '(A,G7.3,A)', $
  '  Poisson & read noise equal in ', (tbkgnd), ' seconds'
print, '  obj in ('+strc(seeing)+'")^2 box = ', strc(obj), ' ADU'
print, '  sky in ('+strc(seeing)+'")^2 box = ', strc(sky), ' ADU'
print, '  dark current in ('+strc(seeing)+'")^2 box = ', strc(darkc), ' ADU'
print, '  noise in ('+strc(seeing)+'")^2 box = ', strc(sigma), ' ADU'

; print parameters used for calculation
print,'  -------- PARAMETERS ---------'
print, format = '(A,F7.2,A,F6.2)', $
  '  time = ', strc(time), ' sec    m(obj) =', strc(mobj)
print, format = '(A,F5.2,A18,F5.2,A)', $
  '  msky = ', strc(msky), ' mag     seeing =', strc(seeing), ' arcsec'
print, format = '(A,F5.3,A,A,A)', $
  '  pixscl("/pix) = ', strc(pixscl), '   zpt = ', strc(zpt) 
print, format = '(A,F5.2,A15,F6.2,A)', $
  '  gain = ', strc(gain), ' e-/ADU    rn =', strc(rn), ' e-'
print, '  saturation = ', strc(sat), ' DN/pix'
if ((obj+sky+darkc)*pixscl^2.0 gt sat) then $
  print, '  ** pixels are saturated! **'
print


; IR SBF S/N calculation (Tonry & Schneider 1988)
if not(keyword_set(dist)) then dist= 0.77
sbf = gain * obj * time / (obj+sky+darkc+gain*rnoise2) * dist^(-2.0) 
;sbf = gain * obj * time / (obj+sky+darkc) * dist^(-2.0) 
sbf = sbf * 10.0^(0.4*(zpt-Msbf-25))
snsbf =  sbf
;snsbf =  sqrt(sbf)
print, 'sbf P0/P1 = ', strc(snsbf), ' for a distance of ', $
  strc(dist), ' Mpc'
print, 'percent error in SBF mag = ', strc(0.3/snsbf*100.), ' %'
print, 'assumes M(SBF) = ', strc(Msbf)

print
message, '** may have a bug?  safer to set gain=1.0 for now **', /info


end
