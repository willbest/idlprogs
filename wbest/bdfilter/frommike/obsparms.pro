pro Obsparms, gain, rn, pixscl, zpt, msky, sat, darkc, tstr, $
              ao=ao,  $   ; Lick/AO
              ctio60=ctio60, ctio4m = ctio4m, $   ; CTIO
              hst110m=hst110m, $              ; NIC1
              hsth=hsth, hst110w=hst110w, $   ; NIC2
              hstk=hstk, hst207m=hst207m, hst237m=hst237m, $   ; NIC3
              keckr=keckr, keckk=keckk, keckj=keckj, keckh=keckh, $ ; Keck
              lickj=lickj, lickh=lickh, lickk=lickk, $ ; Lick 3-m
              irtfj=irtfj, itrfh=irtfh, irtfk=irtfk, $ ; IRTF short-wl
              irtfl=irtfl, irtfm=irtfm, $              ; IRTF long-wl
              vltk=vltk, $     ; VLT ISSAC
              wircj=wircj, wirch=wirch, wircks=wircks, $   ; CFHT WIRCam
              silent=silent 

;+
; retrieve observing parameters for a particular instrument (gain,
; readnoise, pixel scale, saturation, dark current) and observing site
; (zeropoint, sky brightness).  To call the program, use:
;
;   obsparms,gain,rn,pixscl,zpt,msky,sat,darkc,tstr,/observatory
;
; and the variables will be set.
;
; INPUTS 
;       none
;
; OUTPUTS
;       gain    gain in e-/ADU
;       rn      read noise in e-
;       pixscl  image scale in "/pixel
;       zpt     zeropoint in mag (mag of an object the gives 1 ADU/sec)
;       msky    sky brightness in magnitudes/sq. arcsec
;       sat     saturation/non-linearity level (ADU/pixel)
;               (not available for all)
;       darkc   dark current in e-/pix/s (not available for most)
;       tstr    string with telescope, instrument & filter names
;
; KEYWORD PARAMETERS
;       ao      Lick 3-meter with Berkcam & LLNL AO Cam at K'
;       ctio60    CTIO 1.5-meter with CIRIM at Kshort
;       ctio4m  CTIO 4-meter with CIRIM at Kshort
;       hst110m NICMOS Camera 1 with F110M filter
;       hsth    NICMOS Camera 2 with F160W filter
;       hst110w NICMOS Camera 2 with F110W filter
;       hstk    NICMOS Camera 2 with F222M filter
;       hst207m NICMOS Camera 2 with F207M filter
;       keckr   Keck 10-meter with LRIS at R
;       keckj   Keck 10-meter with NIRC at J
;       keckh   Keck 10-meter with NIRC at H
;       keckk   Keck 10-meter with NIRC at K
;       lickj   Lick 10-meter with LIRC2 at J
;       lickh   Lick 10-meter with LIRC2 at H
;       lickk   Lick 10-meter with LIRC2 at K'
;       irtfj   IRTF 3-m with NSFCAM at J
;       irtfh   IRTF 3-m with NSFCAM at H
;       irtfk   IRTF 3-m with NSFCAM at K
;       irtfl   IRTF 3-m with NSFCAM at L
;       irtfm   IRTF 3-m with NSFCAM at M
;       vltk    VLT 8-m with ISSAC at Ks
;       wircj   CFHT 3.4-m with WIRCam at J
;       wirch   CFHT 3.4-m with WIRCam at H
;       wircks  CFHT 3.4-m with WIRCam at Ks
;
; NOTES
; at Keck, the zeropoint in JHK is about 2.e4 DN/sec/mJy
;    meaning, J0 = 26.2, H0 = 25.8, K0 = 25.2
;    the sky is Jsky = 15.2 mag/sq.arcsec, H = 12.9, K=13.2
;
; HISTORY
; Written by M. C. Liu (UCB): 9/27/95
; 08/29/96 (MCL): added HST NICMOS numbers from the manual,
;                 checked against the NICMOS S/N calculator web site
; 10/03/96 (MCL): added Keck LRIS numbers 
; 03/07/97 (MCL): added saturation & dark current
; 03/20/97 (MCL): added IRTF NSFCAM info
; 08/10/97 (MCL): updated HST NICMOS info (backgrounds lowered)
; 09/26/98 (MCL): added VLT ISSAC info
; 07/14/10 (WB): added CFHT WIRCam info
;-

if n_params() eq 0 then begin
    print, 'pro obsparms,gain,rn,pixscl,zpt,msky,sat,darkc,tstr,/observatory'
    retall
endif


;------------------------------
; Lick 3-meter with LIRC2
; (J & H data are based on only one measurement)
;------------------------------
if keyword_set(lickj) then begin
    tstr =  'Lick 3-meter with LIRC2 at J-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 9.	; my measurements
    rn = 200.
    msky = 15.60
    zpt = 21.27
    pixscl = 0.38
    sat = 16000.
    darkc = 0

endif else if keyword_set(lickh) then begin
    tstr =  'Lick 3-meter with LIRC2 at H-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 9.	; my measurements
    rn = 200.
    msky = 14.1
    zpt = 21.4
    pixscl = 0.38
    sat = 16000.
    darkc = 0

endif else if keyword_set(lickk) then begin
    tstr = 'Lick 3-meter with LIRC2 at Kprime'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 9.
    rn = 200.
    msky = 12.1
    zpt = 20.94
    pixscl = 0.38
    sat = 16000.
    darkc = 0
    
endif else if keyword_set(hst110m) then begin
    tstr = 'HST NICMOS Camera 1 with F110M filter (only 1 read)'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 10.3
    rn = 30
    msky = 27.77 - 6.83
    zpt = 20.24
;    zpt = 20.35 + 2.5*alog10(0.160)
    pixscl = 0.043
    sat = 173000./gain
    darkc = 0.1
    
endif else if keyword_set(hsth) then begin
    tstr = 'HST NICMOS Camera 2 with F160W filter (only 1 read)'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 10.2
    rn = 28
    msky = 25.86 - 5.62
    zpt = 20.70
;    zpt = 20.67 + 2.5*alog10(0.190)
    pixscl = 0.075
    sat = 205000./gain
    darkc = 0.1
    
endif else if keyword_set(hst110w) then begin
    tstr = 'HST NICMOS Camera 2 with F110W filter (only 1 read)'
    if not(keyword_set(silent)) then message,tstr, /info
    gain = 10.2
    rn = 28
    msky = 26.48 - 5.62
    zpt =  21.46
;    zpt = 21.36 + 2.5*alog10(0.340)
    pixscl = 0.075
    sat = 205000./gain
    darkc = 0.1
    
endif else if keyword_set(hstk) then begin
    tstr = 'HST NICMOS Camera 2 with F222M filter (only 1 read)'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 10.2
    rn = 28
    msky = 19.75 - 5.62
;    msky = 17.28 - 5.62
    zpt = 19.45
;    zpt = 19.45 + 2.5*alog10(0.140)
    pixscl = 0.075
    darkc = 0.1
    
endif else if keyword_set(hst207m) then begin
    tstr = 'HST NICMOS Camera 2 with F207M filter (only 1 read)'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 10.2
    rn = 28
    msky = 21.12 - 5.62   ; approx guess as to Jy/mag for 2.07 um
;    msky = 18.85 - 5.62   ; approx guess
    zpt = 19.38
;    zpt = 19.45 + 2.5*alog10(0.140)
    pixscl = 0.075
    darkc = 0.1
    
endif else if keyword_set(hst237m) then begin
    tstr = 'HST NICMOS Camera 2 with F237M filter (only 1 read)'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 10.2
    rn = 28
    msky = 18.41- 5.62   ; approx guess as to Jy/mag for 2.37 um
    zpt = 19.61
;    zpt = 19.45 + 2.5*alog10(0.140)
    pixscl = 0.075
    darkc = 0.1
    
;------------------------------
; Keck with NIRC at JHK
;------------------------------
endif else if keyword_set(keckk) then begin
    tstr = 'Keck 10-meter with NIRC at K'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 4.8		; my measurements
    rn = 113
;    gain = 5.4         ; James' measurements
;    rn = 5.4 * 20.9
    msky = 13.23
    zpt = 25.18
    pixscl = 0.15
    sat = 35000.
    darkc = 0
    
endif else if keyword_set(keckj) then begin
    tstr = 'Keck 10-meter with NIRC at J'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 4.8		
    rn = 113
    msky = 15.2
    zpt = 26.19
    pixscl = 0.15
    sat = 35000.
    darkc = 0
    
endif else if keyword_set(keckh) then begin
    tstr = 'Keck 10-meter with NIRC at H'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 4.8		
    rn = 113
    msky = 12.9
    zpt = 25.80
    pixscl = 0.15
    sat = 35000.
    darkc = 0

;----------------------------------------
; Keck with LRIS at R (data from Arjun)
;----------------------------------------
endif else if keyword_set(keckr) then begin
    tstr = 'Keck 10-meter with LRIS at R'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.9		
    rn = 5.5
    msky = 21.3
;    msky =  19.6    ; I-band
    zpt = 26.43
    pixscl = 0.215
    darkc = 0

;-------------------------------------------
; CTIO 1.5m with CIRIM at Kshort/Kregular
; (from NOAO 3/95 newsletter + CIRIM manual)
;-------------------------------------------
endif else if keyword_set(ctio60) then begin
    tstr = 'CTIO 1.5-meter with CIRIM at Kshort with f/8 optics'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 9.
    rn = 37.
    msky = 12.8       
    zpt = 19.93       ; f/13.5 - Kshort
    pixscl = 1.16     ; f/8 optics
;    pixscl = 0.65     ; f/13.5 optics
;    msky = 12.6   ; f/30 - Kshort, T=15C
;    zpt = 19.63   ; f/30 - Kshort
;    msky = 11.8   ; f/30 - K regular, T=15C
;    zpt = 19.70   ; f/30 - K regular
;    pixscl = 0.30 ; f/30 optics
    darkc = 0
    
;-------------------------------------------
; CTIO 4-m with CIRIM at Kshort/Kregular
; (from NOAO 3/95 newsletter + CIRIM manual)
;-------------------------------------------
endif else if keyword_set(ctio4m) then begin
    tstr = 'CTIO 4-meter with CIRIM at Kregular with f/7.5 optics'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 9.
    rn = 37.
    msky = 12.4       
    zpt = 21.91       ; f/13.5 - Kshort
    pixscl = 0.4     ; f/8 optics
    darkc = 0
    
;----------------------------------------
; Lick 3-m with LLNL AO and Berkcam at K'
;----------------------------------------
endif else if keyword_set(ao) then begin
    tstr = 'Lick 3-m with Berkcam & LLNL AO at Kprime'
    if not(keyword_set(silent)) then message, tstr, /info 
    gain = 9.
    rn = 40./sqrt(10.)    ; multiple reads to reduce readnoise
;    msky =  11.0          ; T = 293K
    msky = 11.6           ; T = 283K
    zpt = 20.94
    pixscl = 0.076
    darkc = 0

;-------------------------------------------------
; IRTF with NSFCAM at JHKLM'
; choice of plate scales: 0.31, 0.153, 0.056 "/pix
; info from March 1994 IRTF newsletter
;-------------------------------------------------
endif else if keyword_set(irtfj) then begin
    tstr = 'IRTF with NSFCAM at -band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 50.0
    msky = 15.9
    zpt = 25.50
    pixscl = 0.31
    darkc = 0

endif else if keyword_set(irtfh) then begin
    tstr = 'IRTF with NSFCAM at H-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 50.0
    msky = 13.4
    zpt = 24.56
    pixscl = 0.31
    darkc = 0

endif else if keyword_set(irtfk) then begin
    tstr = 'IRTF with NSFCAM at K-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 50.0
    msky = 13.7
    zpt = 24.28
    pixscl = 0.31
    darkc = 0

endif else if keyword_set(irtfl) then begin
    tstr = 'IRTF with NSFCAM at L-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 50.0
    msky = 4.9
;    zpt = 23.04   ; this is what the newsletter says
    zpt = 22.04   ; this is approx what John Rayner says
    pixscl = 0.31
    darkc = 0

endif else if keyword_set(irtfm) then begin
    tstr = 'IRTF with NSFCAM at M''-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 50.0
    msky = 0.3
    zpt = 20.7
    pixscl = 0.15   ; usually use 0.06"/pix scale
    darkc = 0

;-------------------------------------------------
; VLT with ISSAC, short wl Rockwell 1024^2 array
; from ESO ISSAC exposure time calculator
;   version 2.2 (7/8/98)
;   http://www.hq.eso.org/observing/etc/
;-------------------------------------------------
endif else if keyword_set(vltj) then begin
    tstr = 'VLT 8-m with ISSAC-SW at J-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 10.0
    msky = 16.2
    zpt = 27.17
    pixscl = 0.147
    darkc = 0.3
    sat = 60000./gain

endif else if keyword_set(vlth) then begin
    tstr = 'VLT 8-m with ISSAC-SW at H-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 10.0
    msky = 14.3
    zpt = 26.45
    pixscl = 0.147
    darkc = 0.3
    sat = 60000./gain

endif else if keyword_set(vltk) then begin
    tstr = 'VLT 8-m with ISSAC-SW at Ks-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 1.0   ; unavailable, so work in electrons
    rn = 10.0
    msky = 12.8
    zpt = 26.04
    pixscl = 0.147
    darkc = 0.3
    sat = 60000./gain

;-------------------------------------------------
; CHFT with WIRCam at JHKs
; info from www.cfht.hawaii.edu/instruments/imaging/WIRCam/quickinformation.html
;-------------------------------------------------
endif else if keyword_set(wircj) then begin
    tstr = 'CHFT with WIRCam at J-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 3.8
    rn = 30.0
    msky = 15.0   ;not yet calculated
    zpt = 25.0    ;not yet calculated
    pixscl = 0.306
    darkc = 0.05
    sat = 35000.

endif else if keyword_set(wirch) then begin
    tstr = 'CHFT with WIRCam at H-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 3.8
    rn = 30.0
    msky = 15.0   ;not yet calculated
    zpt = 25.0    ;not yet calculated
    pixscl = 0.306
    darkc = 0.05
    sat = 35000.

endif else if keyword_set(wircks) then begin
    tstr = 'CHFT with WIRCam at Ks-band'
    if not(keyword_set(silent)) then message, tstr, /info
    gain = 3.8
    rn = 30.0
    msky = 15.0   ;not yet calculated
    zpt = 25.0    ;not yet calculated
    pixscl = 0.306
    darkc = 0.05
    sat = 35000.

endif


if n_elements(sat) eq 0 then sat = 1e10

END

