function dwarfabsmag, spt, espt0, $
                      errmags = errmags, $    ; output: errors in abs mags
                      rmsfit = rmsfit, $      ; output: RMS in polynomial fit of abs mag vs SpT
                      j = j, h = h, k = k, $
                      ;jhk = jhk, hk = hk, jk = jk, $
                      burg_k = burg_k, $
                      excludeall = excludeall, $  ; i.e. use the "faint" relation
                      faint = faint, $
                      average = average, $
                      lateM = lateM, $
                      extend = extend

;+
; compute absolute magnitude to L and T dwarfs
; using polynomial fits given in Liu et al (2006, SDSS 1534 paper)
;   which are based on MKO magnitudes and near-IR spectral type
;   where 'spt' is defined as 11=L1, 12=L2, 20=T0, etc.
;
; 03/01/07 M. Liu (IfA/Hawaii)
; 06/17/07 MCL: bug fix - output array was incorrect when passed more than one filter
; 07/06/07 MCL: eliminated /jh, /hk, /jhk, /jk input flags, as these were not unique names
;               changed SpT convention from 1=L1 to 11=L1, 
;                 to agree with my routines for Golimowski et al Teff and BC(K) 
;               added 'espt0' and 'errmags' to allow for error propagation
; 01/27/07 MCL: added 'burg_k' for MKO K-band relation from Burgasser (2007)
; 01/02/09 MCL: bug fix - given multiple SpTs and only 1 value for 'espt0', calculation was incorrect!
;                         I don't think this feature was ever used before, esp. since IF/THEN code was wrong
;               added 'rmsfit' as keyword output - gives RMS about the fit in mags
; 08/04/10 MCL: added /lateM - polynomial fit for M6-L0.5
;               added /extend - extrapolates the L1-T8 fit beyond these SpTs
;               added /faint as a synonym for /excludeall (which is less intuitive
;               small bug fix - allowable SpT range is in fact L1-T8, not L0-T8 as assumed previously
;-



if n_params() lt 1 then begin
   print, 'function dwarfabsmag(spt, (espt),              note: 11=L1, 12=L2, 20=T0'
   print, '                     [errmags=], [rmsfit=],'
   print, '                     [j], [h], [k],'
   print, '                     [excludeall], [faint], [average], [lateM], [extend])'
   return, ''
endif
if keyword_set(faint) then $
   excludeall = 1


; coefficients for L1-T8 from Liu et al (2006)
; in the SDSS 1534 paper, the coefficients assume SpT=1 is L1, SpT=10 is T0
; however, this IDL routine assumes SpT=10 is L0 for input variable, 
;   for consistency with my Golimowski et al Teff routine.
; thus when using these, ** remember to subtract 10 from the input SpT in the coding **
;
; (1) Excluding known binaries, a.k.a. "bright" relation (the default choice)
if not(keyword_set(excludeall)) and not(keyword_set(average)) then begin
   message, 'using "bright" relation', /info
   cc = [[11.746, -2.259e-1, 3.229e-1, -5.155e-2, 2.966e-3, -5.648e-5], $  ; J-band
         [11.263, -4.164e-1, 3.565e-1, -5.610e-2, 3.349e-3, -6.720e-5], $  ; H-band
         [10.731, -3.964e-1, 3.150e-1, -4.912e-2, 2.994e-3, -6.179e-5]]    ; K-band  
   rr = [0.39, 0.35, 0.39]
; (2) Excluding known and possible binaries, a.k.a. "faint" relation
endif else if not(keyword_set(average)) then begin
   message, 'using "faint" relation', /info
   cc = [[11.359, 3.174e-1, 1.102e-1, -1.877e-2, 9.169e-4, -1.233e-5], $  ; J-band
         [10.767, 2.744e-1, 8.955e-2, -1.552e-2, 8.319e-4, -1.315e-5], $  ; H-band
         [10.182, 3.688e-1, 2.028e-2, -4.488e-3, 2.301e-4, -2.466e-6]]    ; K-band
   rr = [0.37, 0.30, 0.34]
; (3) Average of published "bright" and "faint" relations
;     RMS values is simply an average of the 2 above, didn't calculate from scratch
endif else begin
   message, 'using *average* relation', /info
   cc = [ [([11.746, -2.259e-1, 3.229e-1, -5.155e-2, 2.966e-3, -5.648e-5] + [11.359, 3.174e-1, 1.102e-1, -1.877e-2, 9.169e-4, -1.233e-5])/2.], $
          [([11.263, -4.164e-1, 3.565e-1, -5.610e-2, 3.349e-3, -6.720e-5] + [10.767, 2.744e-1, 8.955e-2, -1.552e-2, 8.319e-4, -1.315e-5])/2.], $
          [([10.731, -3.964e-1, 3.150e-1, -4.912e-2, 2.994e-3, -6.179e-5] + [10.182, 3.688e-1, 2.028e-2, -4.488e-3, 2.301e-4, -2.466e-6])/2.] ]
   rr = [0.38, 0.32, 0.36]
endelse
norder = n_elements(cc(*, 0))-1


; coefficient for M6-L0.5, based on same Leggett data as used for Liu et al (2006)
;   RMS about these fits for M6-L0.5 are {0.32, 0.26, 0.21} mags for JHK
; again, need to assume SpT=0 is L0 when using these coeffs
; these are implemented as a piecewise attachment, to the existing L1-T8 relations
;   this is ok, as the joining at L1 is mostly continuous 
;   (jumps of 0.10, 0.06, 0.04 for JHK)
cc_lateM = [[ 11.664, 0.17145, 0.053071, 0.0098609, -0.0030177, 0.00020719, -4.3496E-06], $   ; J-band
            [ 11.012, 0.14087, 0.046372, 0.0092927, -0.0027656, 0.00019448, -4.2134E-06], $   ; H-band
            [ 10.481, 0.12294, 0.035077, 0.0085039, -0.0022364, 0.00015317, -3.2803E-06]]     ; K-band


; Ks-band relation, from Burgasser (2007)
; 07/19/07 email: full (necessary) number of significant digits, not just the published result
cc_burg_k =  [10.4458, 0.232154, 0.0512942,  $
              -0.0402365, 0.0141398, -0.00227108,  $
              0.000180674, -6.98501e-06 , 1.05119e-07]
rr_burg = 0.26


;if keyword_set(hk) then begin
;   h = 1  &  k = 1
;endif
;if keyword_set(jk) then begin
;   j = 1  &  k = 1
;endif
;if keyword_set(jhk) then begin
;   j = 1  &  h = 1  &  k = 1
;endif


; initialize outputs
nfilt = keyword_set(j) + keyword_set(h) + (keyword_set(k) or keyword_set(burg_k))
nspt = n_elements(spt)
out = fltarr(nspt, nfilt)


; set up error calculations
errflag = 0
if n_params() eq 2 then begin
   errmags = fltarr(nspt, nfilt)
   ; check if a single value is passed for the error in SpT
   if n_elements(espt0) eq 1 and nspt ne 1 then $
      espt = espt0+intarr(nspt) $   
   ;if n_elements(espt) eq 1 and nspt ne 1 then $       ; removed 01/02/09 
      ;espt = espt0+indgen(nspt) $                      ; removed 01/02/09 
   else $
      espt = espt0
   if (total(espt lt 0) ge 1) then $
      message, 'errors in SpT need to be positive!'
   errflag = 1
endif


; do calcs
; remember to subtract 10 from the input SpT for proper use of coefficients
ii = nfilt-1  ; loop over filters
ss = float(spt)-10.
; --- K-band (MKO or 2MASS) ---;
if (keyword_set(k) eq 1) or keyword_set(burg_k) then begin

   if not(keyword_set(burg_k)) then begin
      out(*, ii) =  poly(float(ss), cc(*, 2)) 
      rmsfit = rr(2)
   endif else begin
      message, 'using Burgasser relation for 2MASS K-band', /info
      out(*, ii) =  poly(float(ss), cc_burg_k)
      rmsfit = rr_burg
   endelse
   
   if (errflag) then begin
      if not(keyword_set(burg_k)) then $
         errmags(*, ii) = abs(poly(ss, cc(1:*, 2)*(findgen(norder)+1))) * espt $
      else $
         errmags(*, ii) = abs(poly(ss, cc_burg_k(1:*)*(findgen(n_elements(cc_burg_k)+1)))) * espt 
   endif
   
   if keyword_set(lateM) then begin
      w = where(ss lt 1)
      out(w, ii) = poly(float(ss(w)), cc_lateM(*, 2))
      if (errflag) then $
         errmags(w, ii) = abs(poly(ss(w), cc_lateM(1:*, 2)*(findgen(norder)+1))) * espt 
   endif

   ii = ii-1
   message, 'k', /info

endif
; --- H-band ---;
if keyword_set(h) eq 1 then begin
   out(*, ii) = poly(float(ss), cc(*, 1))
   rmsfit = rr(1)
   if (errflag) then $
      errmags(*, ii) = abs(poly(ss, cc(1:*, 1)*(findgen(norder)+1))) * espt

   if keyword_set(lateM) then begin
      w = where(ss lt 1)
      out(w, ii) = poly(float(ss(w)), cc_lateM(*, 1))
      if (errflag) then $
         errmags(w, ii) = abs(poly(ss(w), cc_lateM(1:*, 1)*(findgen(norder)+1))) * espt 
   endif
   
   ii = ii-1
   message, 'h', /info
     endif
; --- J-band ---;
if keyword_set(j) eq 1 then begin
   out(*, ii) = poly(float(ss), cc(*, 0))
   rmsfit = rr(0)

   if keyword_set(lateM) then begin
      w = where(ss lt 1)
      out(w, ii) = poly(float(ss(w)), cc_lateM(*, 0))
      if (errflag) then $
         errmags(w, ii) = abs(poly(ss(w), cc_lateM(1:*, 0)*(findgen(norder)+1))) * espt 
   endif
   
   if (errflag) then $
      errmags(*, ii) = abs(poly(ss, cc(1:*, 0)*(findgen(norder)+1))) * espt
   message, 'j', /info
endif


; default is to exclude objects outside the fitting range
; can also apply coefficients for late-M's if desired
if not(keyword_set(extend)) then begin
   if not(keyword_set(lateM)) then $
      w = where(spt lt 11 or spt gt 28, nw) $
   else $
      w = where(spt lt 6 or spt gt 28, nw) 
   ;w = where(spt lt 10 or spt gt 28, nw)
   if (nw gt 0) then $
      out(w, *) = !values.f_nan
endif else $
   message, '/extend set - using published (2006) relations outside of L1-T8 range.  USE WITH CAUTION!', /info


return, reform(out)
end  
