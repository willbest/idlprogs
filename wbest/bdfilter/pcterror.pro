PRO PCTERROR, MEAS, EXP, ERRPCT, DIREC

;  Calculates the percent error for a measured value given an expected
;  value.  Outputs an error percentage and a direction (above or below).
;
;  HISTORY
;  Written by Will Best (IfA), 07/14/2010
;
;  INPUTS
;       MEAS - measured value
;       EXP - expected value
;
;  OUTPUTS
;       ERRPCT - percent error
;       DIREC - above or below the expected value


error = (meas-exp)/exp
errpct = abs(error*100.)

case 1 of
    error gt 0 : direc='above the expected value'
    error lt 0 : direc='below the expected value'
    else : direc=''
endcase

END
