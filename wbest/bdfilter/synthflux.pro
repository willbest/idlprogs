FUNCTION SYNTHFLUX, INLAMBDA, INFLUX, FLAMBDA, FPASS, ALAMBDA, APASS, INFLUXERR=influxerr, $
                    NOAIR=noair, NOFILTER=nofilter, NOGRAPH=nograph, $
                    NONORM=nonorm, _EXTRA=extra_keywords

;+
;  Returns the total flux for an input spectrum, optionally using a filter
;  and/or atmosphere.
;
;  HISTORY
;  Written by Will Best (IfA), 07/02/2010
;  07/05/10 (WB):  Removed SHOWFLUX keyword and associated line of code.
;                     Leaving this to the calling program
;  07/13/10 (WB):  Changed integration function from TSUM to INT_TABULATED.
;                     Seems to make ~10% difference.
;  07/14/10 (WB):  Added NONORM keyword and default bandwidth normalizing.
;  07/22/10 (WB):  Changed incoming lambda and flux variables to INLAMBDA
;                     and INFLUX, so these variables would not change in the
;                     calling program after running filterpho.
;  06/24/13 (WB):  Added INFLUXERR keyword and error calculation.
;                  If INFLUXERR is set, the function returns a vector: [totalflux, totalerr]
;
;  USES
;       filterpho
;
;  INPUTS
;       INLAMBDA - Wavelength array for input spectrum.
;       INFLUX - Flux array for input spectrum.
;       FLAMBDA - Wavelength array for filter.
;       FPASS - Pass coefficient array for filter.  Each value
;                 should lie between 0 and 1 inclusive.
;       ALAMBDA - Wavelength array for atmosphere.
;       APASS - Pass coefficient array for atmosphere.  Each value
;                 should lie between 0 and 1 inclusive.
;
;  INPUT KEYWORDS
;       INFLUXERR - Flux error array for input spectrum.
;       NOAIR - Ignore atmospheric absorption
;       NOFILTER - Don't use any filter
;       NOGRAPH - Supresses the pretty plots
;       NONORM - Don't divide out the filter transmission (i.e. don't
;                bandwidth normalize the spectrum)
;
;  OUTPUT KEYWORDS
;       TOTALERR - Variable to receive the calculated error in the total
;                     integrated flux.
;-

; Establish spectrum index, filter, and atmosphere files
if n_elements(inlambda) eq 0 then begin
    print, 'No spectrum entered'
    return, -999
endif

lambda = inlambda
flux = influx

; If a filter is present, plot it in red and determine photometry
if not keyword_set(nofilter) then begin
    if not keyword_set(nograph) then oplot, flambda, fpass, color='0000FF'XL
    filterpho, lambda,flux,flambda,fpass, lambda,flux
endif

; If an atmosphere is present, plot it in blue and determine photometry
if not keyword_set(noair) then begin
    if not keyword_set(nograph) then oplot, alambda, apass, color='FFA300'XL
    filterpho, lambda,flux,alambda,apass, lambda,flux
endif

; Plot the flux convolved with the filter and/or atmosphere
if not (keyword_set(nofilter) and keyword_set(noair)) then $
  if not keyword_set(nograph) then oplot, lambda, flux, color='00FF00'XL

; Calculate total resultant flux
;  totalflux = tsum(lambda,flux)
totalflux = int_tabulated(lambda,flux)

; Calculate the error in flux
if n_elements(influxerr) gt 0 then begin
    filterpho, inlambda,influxerr,flambda,fpass, lambdaerr,fluxerr
    n = n_elements(fluxerr)
    totalerr = sqrt(total(fluxerr^2)/n) * (lambdaerr[n-1] - lambdaerr[0])
endif

; Normalize the total flux and error to the bandpass 
if not (keyword_set(nofilter) or keyword_set(nonorm)) then begin
;    filternorm = tsum(flambda,fpass)
    filternorm = int_tabulated(flambda,fpass)
    totalflux = totalflux / filternorm
    if n_elements(influxerr) gt 0 then totalerr = totalerr / filternorm
endif

if n_elements(influxerr) gt 0 then totalflux = [totalflux, totalerr]
return, totalflux

END
