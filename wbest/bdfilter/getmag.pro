showflux=1
;FUNCTION GETMAG, SPECTRUM, FILTER, ATMOSPHERE, $
;                 SHOWFLUX=showflux, _EXTRA=extra_keywords

;  Determines the magnitude of a star from its spectrum file,
;  calibrated by setting mag(Vega) = 0.0.
;
;  HISTORY
;  Written by Will Best (IfA), 7/21/2010
;
;  USES
;       synthflux
;       vegaflux
;
;  KEYWORDS
;       SHOWFLUX - Print calculated flux for each star processed

;  Establish spectrum, filter, and atmosphere files
if n_elements(spectrum) eq 0 then spectrum='~/spectra/L7.5_2MASSJ0825+2115.txt'
readcol, spectrum, lambda, flux, comment='#', format='f,f', /silent
badind = where(flux eq -999)
flux[badind] = 0

if n_elements(filter) eq 0 then filter = '~/filters/2massj.txt'
readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

atmosphere = '~/airmass/ukirt_spexprism.txt'
readcol, atmosphere, alambda, apass, comment='#', format='f,f', /silent

;Determine Vega flux
vgflux = vegaflux(flambda,fpass,alambda,apass,/nograph,$
                  showflux=showflux,_extra=extra_keywords)

;Object flux
totalflux=synthflux(lambda,flux,flambda,fpass,alambda,apass,/nograph,$
                   _extra=extra_keywords)
if keyword_set(showflux) then print, 'Total flux is', totalflux
mag = 2.5 * alog10(vgflux / totalflux)
print, 'Magnitude is', mag

;return, mag

END
