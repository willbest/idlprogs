FUNCTION SIGNOICNT, OBJCOUNTS, SKYCOUNTS, GAIN, RN, PIXSCL, SEEING

;  Calculates the signal-to-noise ratio for an object, using object
;  and sky fluxes in terms of total photons detected.
;
;  USE
;      sn = signoicnt(objcounts,skycounts,gain,rn,pixscl,seeing)
;
;  HISTORY
;  Written by Will Best (IfA), 07/16/2010
;  07/22/2010 (WB): Added SEEING
;  08/13/2010 (WB): Changed default to circular aperature with radius seeing/2.
;
;  INPUTS
;      OBJCOUNTS - total photons detected from object
;      SKYCOUNTS - total photons detected from sky emission
;      GAIN - intrument gain
;      RN - instrument readnoise
;      PXSCL - instrument pixel scale
;      SEEING - size of box in which object lies
;

objcounts = objcounts * 0.5     ;half of flux inside FWHM

;skycounts = skycounts * (seeing)^2.
skycounts = skycounts * !pi * (seeing/2.)^2.    ;circular aperature of FWHM
skynoise2 = (skycounts + objcounts) / gain
readnoise2 = (rn/gain)^2. * (seeing/pixscl)^2.

sigma = sqrt(skynoise2 + readnoise2)

sn = objcounts / sigma

pixelcounts = (objcounts + skycounts) / (seeing/pixscl)^2 + (rn/gain)
if pixelcounts gt (.8 * sat * gain) then print, $
  'WARNING: Pixel wells over 80% full'

return, sn

END
