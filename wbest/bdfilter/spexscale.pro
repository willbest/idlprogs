PRO SPEXSCALE, SPECINDEX, NOGRAPH=nograph, SHOWFLUX=showflux, $
                          SILENT=silent

;  Determines the calibrating constant for a set of spectra and a
;  given filter, using mag(Vega) = 0.0 as the standard.
;
;  Current default is to use the SpeX Prism library.
;
;  HISTORY
;  Written by Will Best (IfA), 08/13/2010
;
;  USES
;      calibconst
;      synthflux
;
;  KEYWORDS
;       NOGRAPH - Suppress the pretty graphs
;       SHOWFLUX - Print calculated flux for each star processed
;       SILENT - suppress other outputs

;  Establish spectrum index
if n_elements(specindex) eq 0 then specindex='~/spectra/spl_may2010_list.txt'
readcol, specindex, spclass, list, comment='#', format='a,a', /silent

;  Establish filter
filter = '~/filters/mkoj.txt'
fn = 'MKO J'
fb = 'J'
if not keyword_set(silent) then print, 'Using '+fn+' filter'
readcol, filter, flambda, fpass, comment='#', format='f,f', /silent

magarr=fltarr(n_elements(list), /nozero)
calarr=fltarr(n_elements(list))
nirstarr=fltarr(n_elements(list), /nozero)
optstarr=fltarr(n_elements(list), /nozero)

mainpath='~/spectra/'

vgflux = vegaflux(flambda,fpass,alambda,apass,showflux=showflux,$
                  /noair,/nograph,_extra=extra_keywords)

spt = indgen(20) + 10
absmag = dwarfabsmag(spt, /j, /average)
expflux = vgflux * 100^(-absmag/5.)

if not keyword_set(nograph) then begin
    device, decomposed=1
    window, 0, xsize=800, ysize=600
    pos=getpos(0.7)
endif

;Set up to read in one object's spectrum at a time
i=0
quit=' '
while (quit ne 'q') and (i lt n_elements(list)) do begin

;Read in Spex Prism header and get magnitude
    path=mainpath+strlowcase(spclass[i])+'dwarf/'
    readcol, path+list[i], str, /silent, numline=13, $
      format='a', delim='@', comment='%'

;Get spectral types and magnitude from SpeX Prism file
    nirst=99
    optst=99
    mag=-999
    for j=7,10 do begin
        tmp=strsplit(str[j],' ',/extract)
        if n_elements(tmp) ge 5 then begin
            if tmp[1] eq 'Near' and tmp[2] eq 'infrared' then begin
                nirststr=strsplit(tmp[5],'+:',/extract)
                nirst=sptypenum(nirststr[0],/silent)
            endif
            if tmp[1] eq 'Optical' and tmp[2] eq 'spectral' then begin
                optststr=strsplit(tmp[4],'+:',/extract)
                optst=sptypenum(optststr[0],/silent)
            endif
        endif
    endfor    
    nirstarr[i]=nirst
    optstarr[i]=optst

;Read in the spectrum file
    readcol, path+list[i], lambda, flux, format='f,f', /silent
    if not keyword_set(nograph) then begin
        origflux=flux
        origlambda=lambda
    endif
    name=strsplit(list[i],'_',/extract) ;Nice pretty title
    name=name[1]

;Plot a spectrum in white
    if not keyword_set(nograph) then begin
        plot, origlambda, origflux, title='Spectrum for '+name, $
          xtitle='Wavelength (microns)', ytitle='Normalized flux', position=pos, $
          subtitle='Original spectrum is white!CFilter pass is red!CAtmosphere pass is blue!CResultant flux is green'
    endif

;Get the total flux and calibrate it
    totalflux = synthflux(lambda,flux,flambda,fpass,alambda,apass, $
                          /noair)
    if (nirst le 28 and nirst ge 11) then begin
        cal = expflux[nirst-10] / totalflux
        calarr[i] = cal
        magarr[i] = absmag[nirst-10]
    endif
    if keyword_set(showflux) then $
      print, 'Total flux for '+name+' is'+string(cal*totalflux)+' W/m^2'

;Continue or quit
        i=i+1
;        print, 'Press any key for next spectrum, or q to quit.'
;        quit=get_kbrd()
    endwhile

delcol=where(nirstarr gt 28 or nirstarr lt 11)
;delcol=where(optstarr eq 99)          ;ignore Spex data with no given Opt ST
delindex=replicate(1,i)
delindex[delcol]=0                    ;index of the data with no mag
keepcol=where(delindex eq 1)          ;index of good data

keepnirst=nirstarr[keepcol]
keepoptst=optstarr[keepcol]
keepmag=magarr[keepcol]
keepcal=calarr[keepcol]               ;calibration const for stars with given ST
keepname=list[keepcol] 
keepclass=spclass[keepcol]

device, decomposed=1
window, 1, xsize=800, ysize=600
plot, keepnirst, keepmag, xtitle='Published Near-infrared spectral type', $
      ytitle='Calculated J-band absolute magnitude', psym=5;, yrange=[30,40]
;plot, keepmag, keepflux, xtitle='Published J-band magnitude', $
;      ytitle='Calculated J-band flux', psym=5;, yrange=[30,40]

;best fit line
;bestfit=linfit(keepmag, keepcalcmag, yfit=bfit, prob=prob)
;bestfit=linfit(keepmag, keepflux, yfit=bfit, prob=prob)
;oplot, keepmag, bfit

;  Print data to file
textout=mainpath+'output/spexscales.txt'
forprint, keepclass, keepname, keepcal, keepnirst, $
  textout=textout, format='(a1,2x,a58,2x,e13,2x,i2)', $
  comment='#SC Name                                                       Calib Const   NIR ST'

END
