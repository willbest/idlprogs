PRO COLORCOMPARE, MINE, THEIRS, SILENT=silent

;  Compares the color indices for two different lists of stars
;  containing common stars.  Outputs to a file.
;
;  Currently set up to compare output from synthpho.pro, which includes
;  J-K colors, to a list from DwarfArchives.org, which contains only
;  J, H, and K magnitudes.
;
;  HISTORY
;  Written by Will Best (IfA), 6/28/2010
;
;  KEYWORDS
;       SILENT - Suppress the output for each star

if n_elements(mine) eq 0 then mine='~/spectra/output/2MASSjks.txt'
if n_elements(theirs) eq 0 then $
  theirs='~/spectra/DwarfArchives-phot.jhk.txt'

readcol, mine, mname, mopt, mnir, mjk, comment='#', format='a,a,a,f', /silent
mysize=n_elements(mname)
tspt=replicate('?',mysize)
tjk=replicate(-999.0,mysize)
delta=replicate(-999.0,mysize)

readcol, theirs, tname, tjmag, tjmage, thmag, thmage, tkmag, tkmage, topt, $
  comment='#', format='a,f,f,f,f,f,f,f', /silent
theirsize=n_elements(tname)
for j=0, theirsize-1 do tname[j]=strjoin(strsplit(tname[j],'_',/extract))
tname=[tname,'end']

for i=0, mysize-1 do begin
    j=0
    repeat j=j+1 until (mname[i] eq tname[j]) or (j eq theirsize)
    if (j lt theirsize) then begin
        tspt[i]=damakeclass(topt[j])
        tjk[i] = tjmag[j] - tkmag[j]
        delta[i] = mjk[i] - tjk[i]
        if not keyword_set(silent) then $
          print, 'delta(J-K) for '+mname[i]+' is '+string(delta[i])
    endif
endfor
tname=tname[0:theirsize-2]

delrow=where(delta eq -999)
delindex=replicate(1,mysize)
delindex[delrow]=0
keeprow=where(delindex eq 1)
keepdelta=delta[keeprow]
print, 'Mean value of delta(J-K) is', mean(keepdelta)
print, 'Standard deviation of delta(J-K) is', stddev(keepdelta)

textout='~/spectra/output/comparison.txt'
forprint, mname, mopt, mnir, tspt, mjk, tjk, delta, textout=textout, $
  format='(a28,3x,a4,3x,a4,3x,a4,3x,f6.2,5x,f6.2,5x,f6.2)', $
  comment='#Name                            MyOpt  MyNIR  Spt   My J-K    Std J-K    delta(J-K)', /silent

END
