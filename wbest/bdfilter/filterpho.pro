PRO FILTERPHO, LAMBDA, FLUX, FILLAMBDA, FILPASS, FILTERLAMBDA, FILTERFLUX

;  Convolves the synthetic photometry of input spectral data with
;  a filter of any type.  Wavelengths for the spectrum and filter
;  do not need to match or correlate, but must be in the same units.
;
;  HISTORY
;  Written by Will Best (IfA), 06/28/2010
;  08/12/2010: Amended to use the union of the LAMBDA and FILLAMBDA
;              for interpolation.
;
;  USES
;       interpol
;
;  INPUTS
;       LAMBDA - Wavelength array for input spectrum.
;       FLUX - Flux array for input spectrum.
;       FILLAMBDA - Wavelength array for filter.
;       FILPASS - Pass coefficient array for filter.  Each value
;                 should lie between 0 and 1 inclusive.
;
;  OUTPUTS
;       FILTERLAMBDA - Wavelength array for filtered spectrum
;       FILTERFLUX - Flux array for filtered spectrum

;   Establish the region in which to work
lower = max([min(lambda),min(fillambda)])
upper = min([max(lambda),max(fillambda)])

;   Extract the spectrum values in the working region
fitind = where((lambda ge lower) and (lambda le upper))
if min(fitind) gt 0 then fitind=[min(fitind)-1,fitind]
if max(fitind) lt (n_elements(lambda) - 1) then fitind=[fitind,max(fitind)+1]
fitlambda = lambda[fitind]   ;only use wvln where fliter and spectrum overlap
fitflux = flux[fitind]       ;only use flux where fliter and spectrum overlap

;   Extract the filter values in the working region
fitfilind = where((fillambda ge lower) and (fillambda le upper))
fitfillambda = fillambda[fitfilind]
fitfilpass = filpass[fitfilind]

;Combine the wavelength vectors
newlambda = [fitlambda, fitfillambda]
newlambda = newlambda[sort(newlambda)]
newlambda = newlambda[uniq(newlambda)]

;   Map the filter's lambda variable onto the combined lambda
newflux = interpol(fitflux, fitlambda, newlambda)
newpass = interpol(fitfilpass, fitfillambda, newlambda)

;   Determine the spectral flux through the filter
filterflux = newflux*newpass
filterlambda = newlambda

END
