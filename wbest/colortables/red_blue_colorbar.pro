PRO RED_BLUE_COLORBAR, WHITE, BLACK, NLEV

;+
;  Loads a color table with the following values:
;     0-99:  Blue --> White
;      100:  White
;  101-200:  White --> Red
;      201:  Black
;
;  HISTORY
;  Written by Will Best (IfA), 2013-04-26
;  2013-10-17 (WB): Split this part off to generalize it.
;
;  OUTPUTS
;      WHITE - Value for the color white (100)
;      BLACK - Value for the color black (201)
;-

; {green,red,blue}: {0,0,0}=black, {255,255,255}=white, {0,255,0}=green, etc.
; 0 --> 100 = blue --> white
; 100 --> 200 = white --> red
; 201 = black
if n_elements(nlev) eq 0 then nlev = 200
green = fltarr(nlev+2)   ; array with elements 0 to 201
green[nlev/2] = 1.       ; element 100 is max value for green
red = green              ; same for red
blue = green             ; same for blue
for i=0, (nlev/2)-1 do begin
    green[i] = i / (nlev/2.)      ; green 0 --> 1  for first 100 elements of color table
    red[i] = i / (nlev/2.)        ; red 0 --> 1  for first 100 elements of color table
    blue[i] = 1.                  ; blue always 1  for first 100 elements of color table
    green[nlev-i] = green[i]      ; green 1 --> 0  for second 100 elements of color table
    red[nlev-i] = 1.              ; red always 1  for second 100 elements of color table
    blue[nlev-i] = green[i]       ; blue 1 --> 0  for second 100 elements of color table
endfor

; Load the new color table
tvlct, fix(sqrt(red)*255), fix(sqrt(green)*255), fix(sqrt(blue)*255) ; sqrt makes better visual transition
white = nlev/2           ; element 100 is white
black = nlev + 1         ; element 201 is black


END

