PRO RED_GREEN_BLUE_COLORBAR, WHITE, BLACK, NLEV

;+
;  Loads a color table with the following values:
;    0-100:  Blue --> Green
;  100-200:  Green --> Red
;      201:  Black
;      202:  White
;
;  HISTORY
;  Written by Will Best (IfA), 2013-10-17
;
;  OUTPUTS
;      WHITE - Value for the color white (202)
;      BLACK - Value for the color black (201)
;-

; {green,red,blue}: {0,0,0}=black, {255,255,255}=white, {0,255,0}=green, etc.
; 0 --> 100 = blue --> green
; 100 --> 200 = green --> red
; 201 = black
; 202 = white
if n_elements(nlev) eq 0 then nlev = 200
green = fltarr(nlev+3)   ; array with elements 0 to 202
green[nlev+2] = 1.       ; Last element is max color (will be white)
red = green              ; same for red
blue = green             ; same for blue
green[nlev/2] = 1.       ; element 100 is max value for green
for i=0, (nlev/2)-1 do begin
    green[i] = i / (nlev/2.)      ; green 0 --> 1  for first 100 elements of color table
    red[i] = 0.                   ; red always 0  for first 100 elements of color table
    blue[i] = 1. - i/(nlev/2.)    ; blue 1 --> 0  for first 100 elements of color table
    green[nlev-i] = green[i]      ; green 1 --> 0  for second 100 elements of color table
    red[nlev-i] = blue[i]         ; red 0 --> 1  for second 100 elements of color table
    blue[nlev-i] = 0.             ; blue always 0  for second 100 elements of color table
endfor

; Load the new color table
tvlct, fix(sqrt(red)*255), fix(sqrt(green)*255), fix(sqrt(blue)*255) ; sqrt makes better visual transition
white = nlev + 2         ; element 100 is white
black = nlev + 1         ; element 201 is black


END

