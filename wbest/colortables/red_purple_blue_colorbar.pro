PRO RED_PURPLE_BLUE_COLORBAR, WHITE, BLACK, NLEV

;+
;  Loads a color table with the following values:
;    0-100:  Blue --> Purple
;  100-200:  Purple --> Red
;      201:  Black
;      202:  White
;
;  HISTORY
;  Written by Will Best (IfA), 2013-10-17
;
;  OUTPUTS
;      WHITE - Value for the color white (202)
;      BLACK - Value for the color black (201)
;-

; {green,red,blue}: {0,0,0}=black, {255,255,255}=white, {0,255,0}=green, etc.
; 0 --> 100 = blue --> purple
; 100 --> 200 = purple --> red
; 201 = black
; 202 = white
if n_elements(nlev) eq 0 then nlev = 200
green = fltarr(nlev+3)   ; array with elements 0 to 202, green always 0.
green[nlev+2] = 1.       ; Last element is max color (will be white)
red = green              ; same for red
blue = green             ; same for blue

for i=0, (nlev*3/4)-1 do red[i] = i / (nlev*3/4.) ; red 0 --> 1  for first 3/4 elements of color table
for i=(nlev*3/4), nlev do red[i] = 1.             ; red always 1  for remaining elements of color table
blue[0:nlev] = reverse(red[0:nlev])

; Load the new color table
tvlct, fix(red*255), fix(green*255), fix(blue*255) ; sqrt makes better visual transition
white = nlev + 2         ; element 100 is white
black = nlev + 1         ; element 201 is black


END

