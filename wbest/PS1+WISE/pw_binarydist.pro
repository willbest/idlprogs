FUNCTION PW_BINARYDIST, SPT1, SPT2, W2, W2ERR

; Calculate the photometric distance to a blended binary brown dwarf using its
; W2 magnitude.
;
; HISTORY
;     Written by Will Best (IfA), 07/10/2013
;     07/11/2013 (WB): Adder error calculation
;
; INPUTS
;     SPT1 - spectral type of one component of the binary
;     SPT2 - spectral type of the other component
;           0 = M0, 10 = L0, 20 = T0
;     W2 - apparent W2 magnitude of the blend
;     W2ERR - W2 magnitude uncertainty
;
; USES
;     combinemag.pro
;     tjd_sptabsmag.pro
;

; Get absolute W2 mags for the spectral types
abs1 = tjd_sptabsmag(spt1, 'SpT', 'W2')
abs2 = tjd_sptabsmag(spt2, 'SpT', 'W2')
abs1err = 0.18
abs2err = 0.18

; Combine to get a blended absolute magnitude
abs = combinemag(abs1, abs2, abs1err, abs2err)

; Calculate the distance
d = 10.^((w2-abs[0]+5)/5.)

; Calculate the error, if W2ERR is provided
if n_elements(w2err) gt 0 then begin
    derr = 0.46042 * d * sqrt(w2err^2 + abs[1]^2)
    d = [d, derr]
endif

return, d

END
