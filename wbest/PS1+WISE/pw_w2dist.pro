FUNCTION PW_W2DIST, SPT, W2, W2ERR

; Calculate the photometric distance to a brown dwarf using its W2 magnitude.
; Calls tjd_sptabsmag.pro, which means Trent's polynomial.
;
; HISTORY
;     Written by Will Best (IfA), 09/28/2012
;     07/11/2013 (WB): Adder error calculation
;
; INPUTS
;     SPT - spectral type of object
;           0 = M0, 10 = L0, 20 = T0
;     W2 - W2 magnitude
;     W2ERR - W2 magnitude uncertainty
;

; Get absolute W2 mag for the spectral type
abs = tjd_sptabsmag(spt, 'SpT', 'W2')
abserr = 0.18

; Calculate the distance
d = 10.^((w2-abs+5)/5.)

; Calculate the error, if W2ERR is provided
if n_elements(w2err) gt 0 then begin
    derr = 0.46042 * d * sqrt(w2err^2 + abserr^2)
    d = [d, derr]
endif

return, d

END
