FUNCTION FITSGAIN, HEAD

; Extracts the gain from a FITS header.
g = head[where(strmid(head, 0, 4) eq 'GAIN')]
eqpos = strpos(g, '=')
slpos = strpos(g, '/')
g1 = strmid(g[0], eqpos+1, slpos-eqpos-1)
g2 = strtrim(g1, 2)
gain = double(g2)
return, gain

END
