FUNCTION QV_LIST, CATALOG, TARGLIST, DIS, MERGEDATA=mergedata, _EXTRA=ex

;  Queries any catalog in the Vizier database for a list of targets.
;
;  QV_list calls the QueryVizier function in the IDL Astro library, and is
;  designed to work exactly the same way.  However, QV_list allows the user to
;  input a list of targets, instead of just a single target.
;  
;  HISTORY
;  Written by Will Best (IfA), 10/07/2011
;
;  CALLING SEQUENCE: 
;     info = QV_list(catalog, targlist, [ dis, /ALLCOLUMNS, 
;                       /CANADA, CONSTRAINT= , MERGEDATA= , /SILENT, /VERBOSE ])
;
;  INPUTS: 
;      CATALOG - Scalar string giving the name of the VIZIER catalog to be
;            searched.    The complete list of catalog names is available at
;            http://vizier.u-strasbg.fr/vizier/cats/U.htx . 
;
;            Popular VIZIER catalogs include  
;            'II/294' - Sloan SDSS photometric catalog Release 7 (2009)
;            '2MASS-PSC' - 2MASS point source catalog (2003)
;            'GSC2.3' - Version 2.3.2 of the HST Guide Star Catalog (2006)
;            'USNO-B1' - Verson B1 of the US Naval Observatory catalog (2003)
;            'NVSS'  - NRAO VLA Sky Survey (1998)
;            'B/DENIS/DENIS' - 2nd Deep Near Infrared Survey of southern Sky
;            'I/259/TYC2' - Tycho-2 main catalog (2000)
;            'I/311/HIP2' - Hipparcos main catalog, new reduction (2007)
;
;          Note that some names will prompt a search of multiple catalogs
;          and QUERYVIZIER will only return the result of the first search.
;          Thus, setting catalog to "HIP2" will search all catalogs 
;          associated with the Hipparcos mission, and return results for the
;          first catalog found.    To specifically search the Hipparcos or
;          Tycho main catalogs use the VIZIER catalog names listed above
;                             
;      TARGLIST - An array, either of scalar strings giving target names (with
;          J2000 coordinates determined by SIMBAD), or a 2-row numeric
;          vector giving the J2000 right ascension and declination for each
;          target, both in *degrees*.
;
;          Example: targlist = [ [23.4, 158.5, 358.0], [78.2, -22.1, 12.7] ]
;             will query for objects around these coordinates:
;
;                   R.A.    DEC   (both in degrees)
;                   ----    ---
;                   23.4    78.2
;                  158.5   -22.1
;                  358.0    12.7
;
;  OPTIONAL INPUT:
;      DIS - scalar or 2-element vector, or commensurate array.
;
;          If one value is supplied, then this is the search radius for every
;          target.  If an array of scalars, equal in length to the number of
;          targets, is supplied, then each scalar is the search radius for 
;          each corresponding target.
;
;          If two values are supplied, then this is the width (i.e., in
;          longitude direction) and height of the search box.  If a two-row
;          array is supplied, then each row defines the search box for each
;          corresponding target.
;
;          DIS must be given in *ARCMINUTES*.
;
;          Default is a search with radius of 5 arcminutes.
;
;  OUTPUTS: 
;      info - Anonymous IDL structure array containing information on the
;          catalog sources within the specified distance of the specified 
;          centers.  Either the first structure tag is the target name, or the
;          first two structure tags are the RA and DEC of the search center.
;          The remaining structure tag names are identical with the VIZIER 
;          catalog column names, with the exception of an occasional underscore
;          addition, if necessary to convert the column name to a valid 
;          structure tag.  The VIZIER Web page should consulted for the 
;          column names and their meaning for each particular catalog.
;           
;          If the tagname is numeric and the catalog field is blank then either
;          NaN  (if floating) or -1 (if integer) is placed in the tag.
;
;          If no sources are found within the specified radius, or an
;          error occurs in the query, then no entry is made in info for that
;          search target.
;
; OPTIONAL KEYWORDS:
;          /ALLCOLUMNS - if set, then all columns for the catalog are returned.
;                 The default is to return a smaller VIZIER default set. 
;
;          /CANADA - By default, the query is sent to the main VIZIER site in
;            Strasbourg, France.   If /CANADA is set then the VIZIER site
;            at the Canadian Astronomical Data Center (CADC) is used instead.
;            Note that not all Vizier sites have the option to return
;            tab-separated values (TSV) which is required by this program.
;   
;          CONSTRAINT - string giving additional nonpositional numeric 
;            constraints on the entries to be selected.     For example, when 
;            in the GSC2.3  catalog, to only select sources with Rmag < 16 set 
;            Constraint = 'Rmag<16'.    Multiple constraints can be 
;            separated by commas.    Use '!=' for "not equal", '<=' for smaller
;            or equal, ">=" for greater than or equal.  See the complete list
;            of operators at  
;                 http://vizier.u-strasbg.fr/doc/asu.html#AnnexQual
;            For this keyword only, **THE COLUMN NAME IS CASE SENSITIVE** and 
;            must be written exactly as displayed on the VIZIER Web page.  
;            Thus for the GSC2.3 catalog one must use 'Rmag' and not 'rmag' or
;            'RMAG'.    In addition, *DO NOT INCLUDE ANY BLANK SPACE* unless it 
;            is a necessary part of the query.
;
;          MERGEDATA - a structure array, with the same length as the target 
;            list.  If set, qv_list will return a named structure array
;            (name="merge"), combining this MERGEDATA structure, the data
;            structure obtained from the Vizier database, and the target list 
;            (either names or coordinates).
;            This is useful for automatically combining your list of search
;            targets with the results of the search into a single structure.
;         
;          /SILENT - If set, then no message will be displayed if no sources
;                are found.    Error messages are still displayed.
;          /VERBOSE - If set then the query sent to the VIZIER site is
;               displayed, along with the returned title(s) of found
;               catalog(s)
;
; EXAMPLE: Search for SDSS sources within 30" of all targets on a list named
;          'objects.txt'.
;          IDL> targets = '[path]/objects.txt'
;          IDL> readcol, targets, ra, dec
;          IDL>  info = qv_list('II/294', [ra, dec], 0.5)
;

on_error, 2

; Check for existence for catalog name and target list
if n_params() lt 2 then begin
    print,'Syntax - info = qv_list(catalog, targlist, [ dis, /ALLCOLUMNS,'
    print,'         /CANADA, CONSTRAINT= , MERGEDATA= , /SILENT, /VERBOSE ])'
    print,'                       '
    print,'  Coordinates (if supplied) should be J2000 RA (degrees) and Dec'
    message,'  dis -- search radius or box in arcminutes'
endif

; Check on format of target list
st = size(targlist)
case st[0] of
    1 : begin
        if st[2] ne 7 then message, 'ERROR: Target names must be strings.'
        names = 1
    end
    2 : begin
        if (st[3] gt 5) and (st[3] lt 12) then $
            message, 'ERROR: Target coordinates must be real numbers.'
        if st[1] eq 2 then targlist = transpose(targlist)
        names = 0
        st = size(targlist)
    end
    else : message, 'ERROR: targlist must be an array of names or coordinates.'
endcase
lent = st[1]

; Check on the mergedata keyword
if keyword_set(mergedata) then begin
    sm = size(mergedata)
    if (sm[2] ne 8) or (sm[3] ne lent) then message, $
      'MERGEDATA must be a structure array of length equal to the target list.'
    mtags = n_tags(mergedata)
endif

; Check the format of dis, creating a new dis array as needed.
sd = size(dis)
lendis = n_elements(dis)
case lendis of
    0 : dis = replicate(5., lent)
    1 : dis = replicate(dis, lent)
    2 : dis = [[replicate(dis[0], lent)], [replicate(dis[1], lent)]]
    else: begin
        errmsg='dis must have the same number of entries as the target list.'
        case sd[0] of
            1 : if lendis ne lent then message, errmsg
            2 : begin
                case sd[1] of
                    1 : if lendis ne lent then message, errmsg else $
                        dis = transpose(dis)
                    2 : if sd[2] ne lent then message, errmsg else $
                        dis = transpose(dis)
                    else : if (sd[2] ne 2) or (sd[1] ne lent) then message, errmsg
                endcase
            end
            else : message, 'ERROR: dis array has too many dimensions.'
        endcase
    end
endcase

; Query the Vizier database
hit = 0
for i=0, lent-1 do begin     ;must query Vizier one object at a time
    result = QueryVizier(catalog, targlist[i,*], dis[i,*], _EXTRA=ex)
; If you get a match for target i...
    if size(result,/type) eq 8 then begin
        hit = hit + 1
        lenr = n_elements(result.(0))
; If it's the first match, create the structure for match data
        if hit eq 1 then begin
            rtags = n_tags(result)
            if keyword_set(mergedata) then begin
                if names then rawlist = create_struct(mergedata[i], result[0], $
                      'targname', targlist[i], name='merge') $
                else rawlist = create_struct(mergedata[i], result[0], $
                      'racenter', targlist[i,0], 'decenter', targlist[i,1], $
                      name='merge')
                if lenr gt 1 then begin
                    item = replicate({merge}, lenr-1)
                    for tag=0, mtags-1 do item[0:lenr-2].(tag) = mergedata[i].(tag)
                    for tag=0, rtags-1 do item.(tag+mtags) = result[1:lenr-1].(tag)
                    if names then item.targname = targlist[i] else begin
                        item.racenter = targlist[i,0]
                        item.decenter = targlist[i,1]
                    endelse
                    rawlist = [rawlist, item]
                endif
            endif else begin
                templist = create_struct(result[0], name='temp')
                if lenr gt 1 then begin
                    item = replicate({temp}, lenr-1)
                    for tag=0, rtags-1 do item.(tag) = result[1:lenr-1].(tag)
                    templist = [templist, item]
                endif
            endelse
        endif else begin
; Merge the new search result data into the existing match data
            if keyword_set(mergedata) then begin
                item = replicate({merge},lenr)
                for tag=0, mtags-1 do item[0:lenr-1].(tag) = mergedata[i].(tag)
                for tag=0, rtags-1 do item.(tag+mtags) = result.(tag)
                if names then item.targname = targlist[i] else begin
                    item.racenter = targlist[i,0]
                    item.decenter = targlist[i,1]
                endelse
                rawlist = [rawlist, item]
            endif else begin
                item = replicate({temp}, lenr)
                for tag=0, rtags-1 do item.(tag) = result.(tag)
                templist = [templist, item]
            endelse
        endelse
    endif
endfor

; If MERGEDATA is not set, create an anonymous structure array to return
temp = n_elements(templist)
if (temp gt 0) then begin
    rawlist = replicate(templist[0], temp)
    for tag=0, rtags-1 do rawlist.(tag) = templist.(tag)
endif

match = n_elements(rawlist)

;summarize results
if not keyword_set(silent) then print, string(match, hit, lent, $
   format='("Found a total of ",i4," matches for ",i4," out of ",i4," targets.")')

return, rawlist

END
