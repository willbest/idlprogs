PRO WDELETEALL

;  Deletes all the open IDL graphics windows.
;  
;  HISTORY
;  Written by Mark Piper, 11/21/2011
;  http://idldatapoint.com/2011/11/21/a-small-helpful-routine/
;

;; compile_opt idl2
   
;; w = getwindows()
;; if n_elements(w) gt 0 then foreach i, w do i.close

while !d.window ne -1 do wdelete, !d.window

END
