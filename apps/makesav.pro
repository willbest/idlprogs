;PRO MAKESAV

;  Framework for making a .sav file from a .csv of objects.
;
;  HISTORY
;  Written by Will Best (IfA), 06/21/2012
;

; Load the data table into the structure "wise_mlt". 
tagsl = 'name,jmag,e_jmag,hmag,e_hmag,kmag,e_kmag,pmra,e_pmra,pmde,e_pmde,ospt,nirspt,spt,spectraltype,dis,e_dis,f_dis,vtan,e_vtan,note,epoch,ra2010,dec2010,mm,mj,y_2,j,h,k,yerr_2,jerr,herr,jherr,kerr,ra_1,dec_1,g,gerr,gnphot,gabs,r,rerr,rnphot,rabs,i,ierr,inphot,iabs,z,zerr,znphot,zabs,y,yerr,ynphot,yabs,groupid,groupsize,separation,gr,grerr,gy,gyerr,ri,rierr,ry,ryerr,iz,izerr,iy,iyerr,zy,zyerr,yj_2mass,yj_2masserr,zj_2mass,zj_2masserr,jh_2mass,jh_2masserr,hk_2mass,hk_2masserr,yj_mko,yj_mkoerr,zj_mko,zj_mkoerr,jh_mko,jh_mkoerr,hk_mko,hk_mkoerr,y_mkoj_mko,y_mkoj_mkoerr,yy,yyerr,name_da,disc_paper,name_2mass,ra_hours,dec_deg,j_da,j_da_err,h_da,h_da_err,k_da,k_da_err,plx,plx_err,plx_paper,pm_tot,pm_tot_err,pm_pa,pm_pa_err,pm_pap,spt_o,spt_o_paper,spt_ir,spt_ir_paper,note_da,ra_wise,dec_wise,dist_wise,w1,w1err,w1abs,w1snr_1,w2,w2err,w2abs,w2snr_1,w3,w3err,w3abs,w3snr_1,w4,w4err,w4abs,w4snr_1,cc_flags_1,ph_qual_1,ext_flag,deblend_flag,note_wise,note_binary,objflags,prob_mem,cluster_name,mbol,e_mbol,prev_id,m,ra_2mass,dec_2mass,ra_hyades,dec_hyades,z_uk,z_uk_err,z_uk_bits,y_uk_bits,j_uk_bits,h_uk_bits,k_uk_bits,dist_mod,ra_1a,dec_1a'
infile = '~/Astro/PS1BD/mlt_2uwp_duplicate_3pi_new.csv'
form = 'a,f,f,f,f,f,f,f,f,f,f,a,a,f,f,d,f,a,d,f,a,f,d,d,f,f,f,f,f,f,d,d,d,d,d,d,d,f,d,i,f,f,d,i,f,f,d,i,f,f,d,i,f,f,d,i,f,i,i,d,d,d,f,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,d,f,d,d,d,d,d,d,a,a,a,d,d,f,f,f,f,f,f,f,f,a,f,f,f,f,a,a,a,a,a,a,d,d,d,f,f,d,f,f,f,d,f,f,f,d,f,f,f,d,f,a,a,i,i,a,a,i,d,a,f,d,a,a,f,f,d,d,d,d,i,i,i,i,i,d,d,d'
outfile = '~/Astro/PS1BD/mlt_2uwp_duplicate_3pi_new.sav'
wise_mlt = read_struct(infile, tagsl, tagformat=form, skipline=1, delimiter=',', /nan, /verbose)

save, wise_mlt, filename=outfile


END
