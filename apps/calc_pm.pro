PRO CALC_PM, ra1, dec1, t1, ra2, dec2, t2, utot, ura, udec, $
             HOURS=hours, SILENT=silent, $
             dra1=dra1, ddec1=ddec1, dra2=dra2, ddec2=ddec2, $
             dutot=dutot, dura=dura, dudec=dudec

;+
;  Calculates the proper motion of an object, given two position and epochs.
;  Default unit is square degrees.
;  Assumes that position 1 is earlier than position 2, so motion is calculated
;  using [position 2] - [position 1].
;
;  HISTORY
;  Written by Will Best (IfA), 12/05/2012
;
;  INPUTS
;      RA1, RA2 - Right ascension values for the two object positions.  Assumes
;                 the first position is earlier.  Assumes the values are given
;                 in degrees unless the HOURS keyword is set.
;      DEC1, DEC2 - Declination values for the two object positions.   Assumes
;                   the first position is earlier.
;      T1, T2 - Epoch values for the two object positions.   Assumes
;                   the first position is earlier.  Units are DAYS.
;
;  OUTPUTS (OPTIONAL)
;      UTOT - Total proper motion, in ARCSECONDS/YEAR.
;      URA - Right ascension proper motion = u_ra * cos(dec), in ARCSECONDS/YEAR.
;      UDEC - Declination proper motion, in ARCSECONDS/YEAR.
;
;  KEYWORD INPUTS (OPTIONAL)
;      HOURS - Set if right ascension values are given as decimal hours.
;          The program will not accept hours, minutes, seconds.
;      SILENT - Suppress output to screen
;      DRA1, DRA2 - Right ascension errors for the two object positions.  Assumes
;                 the values are given in ARCSECONDS.
;      DDEC1, DDEC2 - Declination errors for the two object positions.   Assumes
;                   the values are given in ARCSECONDS.
;
;  KEYWORD OUTPUTS (OPTIONAL)
;      DUTOT - Total proper motion error, in ARCSECONDS/YEAR.
;      DURA - Right ascension proper motion error, in ARCSECONDS/YEAR.
;      DUDEC - Declination proper motion error, in ARCSECONDS/YEAR.
;-

if n_params() lt 6 then begin
    print
    print, 'SYNTAX: calc_pm, ra1, dec1, t1, ra2, dec2, t2 [, utot, ura, udec, $'
    print, '                 HOURS=hours, SILENT=silent, $'
    print, '                 dra1=dra1, ddec1=ddec1, dra2=dra2, ddec2=ddec2, $'
    print, '                 dutot=dutot, dura=dura, dudec=dudec ]'
    print
    return
endif

if keyword_set(hours) then begin
    x1 = double(ra1) * 15d
    x2 = double(ra2) * 15d
endif else begin
    x1 = double(ra1)
    x2 = double(ra2)
endelse

y1 = double(dec1)
y2 = double(dec2)
my = (y1+y2)/2d

; Calculate time interval
dt = double(t2 - t1)
dt = dt / 365.26d                  ; convert to years

; Calculate u_ra
dx = x2 - x1
dx = dx * 3600d                    ; convert to arcsec
ura = dx * cos(my*!dpi/180d) / dt  ; u_ra * cos(dec)

; Calculate u_dec
dy = y2 - y1
dy = dy * 3600d                    ; convert to arcsec
udec = dy / dt                     ; u_dec

; Total
utot = sqrt(ura^2 + udec^2)

; Calculate errors
if (n_elements(dra1)+n_elements(dra2)) gt 0 then begin
    if n_elements(dra1) eq 0 then dra1 = 0
    if n_elements(dra2) eq 0 then dra2 = 0
    dra1 = double(dra1)
    dra2 = double(dra2)
    dratot = sqrt(dra1^2 + dra2^2)       ; assume already in arcsec
    dura = dratot * cos(my*!dpi/180d) / dt
endif

if (n_elements(ddec1)+n_elements(ddec2)) gt 0 then begin
    if n_elements(ddec1) eq 0 then ddec1 = 0d
    if n_elements(ddec2) eq 0 then ddec2 = 0d
    ddec1 = double(ddec1)
    ddec2 = double(ddec2)
    ddectot = sqrt(ddec1^2 + ddec2^2)    ; assume already in arcsec
    dudec = ddectot / dt
    if n_elements(dura) gt 0 then dutot = sqrt(dura^2 + dudec^2)
endif

; Print to screen
if not keyword_set(silent) then begin
    print
    if n_elements(dura) gt 0 then $
      print, 'u_ra = '+trim(ura)+' +/- '+trim(dura) else $
        print, 'u_ra = '+trim(ura)
    if n_elements(dudec) gt 0 then $
      print, 'u_dec = '+trim(udec)+' +/- '+trim(dudec) else $
        print, 'u_dec = '+trim(udec)
    if n_elements(dutot) gt 0 then $
      print, 'u_tot = '+trim(utot)+' +/- '+trim(dutot) else $
        print, 'u_tot = '+trim(utot)
    print
endif

END
