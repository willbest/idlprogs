FUNCTION APP2ABS, APPMAG, PAR, APPMAGERR=appmagerr, PARERR=parerr, ABSMAGERR=absmagerr

;+
;  Returns the absolute magnitude of an object, given apparent magnitude and parallax.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-12-30
;
;  INPUTS
;      APPMAG - Apparent magnitude of the object.
;      PAR - parallax for the object (mas).
;
;  KEYWORD INPUTS
;      APPMAGERR - Error in apparent magnitue.
;      PARERR - Error in parallax.
;
;  KEYWORD OUTPUT
;      ABSMAGERR - Uncertainty in the absolute magnitude
;         Requires non-zero values supplied to APPMAGERR and PARERR
;-

; Calculate the absolute magnitude
absmag = appmag + 5*alog10(par) - 10

; If requested, calculate the uncertainty
if n_elements(appmagerr) gt 0 and n_elements(parerr) gt 0 then begin
    absmagerr = sqrt(appmagerr^2 + (5*0.43429*parerr/par)^2)
endif

return, absmag          

END
