; Read in equatorial coordinates
readcol, '~/Desktop/radec.ascii', ra, dec, format='d,d'

; Convert the coordinates
glactc, ra, dec, 2000, glon, glat, 1, /degree

; Write the galactic coordinates
forprint, glon, glat, ra, textout='~/Desktop/galac.csv', /nocomment, $
          format="(f11.7,',',f12.7,',',f11.7)";format="(f11.7,T13,f12.7,T26,f11.7)"
          

END
