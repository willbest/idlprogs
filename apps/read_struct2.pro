function getyn, qstr, def,  $
                noquery = noquery, help=help
;+
; NAME:
;   getyn
;
; PURPOSE:
;   get a reply to a yes/no question,
;   allowing the user to hit <CR> to select the default respone
;
; INPUT:
;   qstr  string to be printed (the question)
;
; OPTIONAL INPUT:
;   def   default answer (0=no, not 0=yes; yes is default)
;
; KEYWORD PARM:
;   noquery  if this is set, then returns the default answer
;            w/o asking any question (seems useless, but
;            actually useful for some programs)
;
; RETURNS:
;   0 = no
;   1 = yes
;
; EXAMPLE:
;  if getyn('print something?') then print,'something'
;
; HISTORY: Written by M. Liu (UCB) 09/04/96 
; 06/27/00: added /noquery
;-


on_error, 2

if n_elements(def) eq 0 then def = 1
if (def lt 0) then def = 0
if n_params() eq 0 or keyword_set(help) then begin
    print, 'function getyn(qstr, def, noquery=noquery)'
    retall
endif

; check for /noquery
if keyword_set(noquery) then  $
  return, def


cr = string("12b)		; carriage return

repeat begin

    print, format = '($,A)', qstr

    if (def ne 0) then print, format = '($,"(<y>,n) ")' $
    else print, format = '($,"(y,<n>) ")' 

    case get_kbrd(1) of
        
        'y': begin
            ans = 1
        end
        
        'n': begin
            ans = 0
        end

        cr: begin
            ans =  def
        end

        Else: begin
            ans = -1
        end

    endcase
    print

endrep until (ans ge 0)

return, ans

end

;
function read_struct2, filename, taginput, $
                      struct_name=struct_name, comment=comment, $
                      delimiter=delimiter, skipline=skipline, $
                      numline=numline, nan=nan, silent=silent, $
                      verbose=verbose
;+
;
; NAME:
;
;   read_struct
;
; PURPOSE:
;
;   Reads a free-format ASCII file with columns of data (using
;   READCOL) and creates an array of structures from the output
;   arrays using user-supplied structure tagnames.
;
; CATEGORY:
;
;
; CALLING SEQUENCE:
;
;   newstruct = read_struct2(filename, taginput,
;                           [struct_name=struct_name, comment=comment,]
;                            delimiter=delimiter, skipline=skipline, 
;                            numline=numline, /nan]
;
; INPUTS:
;
;   FILENAME - The filename to be read.  The format must be a string.
;
;   TAGINPUT - The tagnames and tagformats for the structure as a 
;              scalar string separated by commas
;
;              e.g. 
;                 taginput = 'name1:format1,name2:format2'
;
;              IDEA TO ADD: (Dec 1, 2011 not added yet)
;              If there are no formats e.g.
;                 tagnames = 'name1,name2,name3'
;              then can set for all the format to be a single type
;              such as double, float, string etc.
;
;              TAGNAME RULES: 
;
;              See RESTRICTIONS for names which are not allowed
;              because of conflicts within function.
;
;              TAGFORMAT RULES:
;
;               The IDL format types for the structure tags and each
;               column of data to be read.  These should be a scalar
;               string format, separated by commas (the allowed values
;               that the READCOL procedure uses).  A few examples are:
;                       A = String
;                       D = Double Precision
;                       F = Float
;                       I = Integer
;                       L = Long Integer
;                      LL = Long64 Integer (allowed format for
;                                           create_struct 2009+)
;
; OPTIONAL KEYWORDS:
;  
;    STRUCT_NAME - A string for the unique structure name to identify
;                  the output structure.  Default is ' ', meaning an
;                  annonymous structure.  (See create_struct documentation)
;    
;    /VERBOSE    - Provides information on what the program is doing
;
; READCOL KEYWORDS: 
;   (adapted from READCOL documentation)
;
;    COMMENT - single character specifying comment character. Any line 
;              beginning with this character will be skipped. Default is
;              comment='#'.
;
;    DELIMITER - Character(s) specifying delimiter used to separate 
;                columns. Usually a single character but, e.g. delimiter=':,'
;                specifies that either a colon or comma as a delimiter. 
;                Set DELIM = string(9b) to read tab separated data
;                The default delimiter is either a comma or a blank.
;
;    SKIPLINE - Scalar specifying number of lines to skip at the top of file
;                before reading.   Default is to start at the first line.
;
;    NUMLINE - Scalar specifying number of lines in the file to read.  
;              Default is to read the entire file
;
;    /NAN - if set, then an empty field will be read into a floating or 
;           double numeric variable as NaN; by default an empty field is 
;           converted to 0.0.
;    
;    /SILENT - Normally, READCOL will display each line that it skips over.
;               If SILENT is set and non-zero then these messages will be 
;               suppressed.
;
; OUTPUTS:
;
;   An array of structures with user-chosen tagnames corresponding to
;   the data in the input file.  See the example for more details.
;
;  PROGRAMS CALLED:
;
;    CREATE_STRUCT - From the idlastro library, a procedure
;                    (not the default function)
;    READCOL       - idlastro library procedure
;    EXECUTE       - idl library function
;    GETYN         - To answer y/n questions (included)
;
; RESTRICTIONS:
;
;   TAGNAMES: The following tagnames are not allowed because
;             the variables are used within this program:
;
;             -filename, locs, nelem, ntags, str_execute, struct
;              tag, taglen, taglen_rm, stagformat, tagfmt, tagfmt_str,
;              tagn, tagn_new, tagn_new_i, tagnames, tagnames_old, var
;
;   STRUCT_NAME: This value must be set for create_struct to work.  If 
;                a name is not set, then the struct_name will be ' ' 
;                which is an annonymous structure
;
;   The total number of columns is currently limited by READCOL, which
;   has a limit of 40 columns of data that can be read.
;              
; NOTES:
;
;   If you want to get information on the required syntax for the
;   function, call the function without any arguments as follows:
;
;   IDL> test = read_struct()
;   Syntax - mstruct(filename, taginput,
;                   [struct_name=struct_name, comment=comment,]
;                   delimiter=delimiter, skipline=skipline, 
;                   numline=numline, /nan]
;
; EXAMPLE:
;
;   Let's say that you have a file, 'test.dat', which has three
;   columns of data like so:
;             a    b    c
;            1.2  2.2  3.4
;            3.4  5.4  2.1
;
;   You can create a structure from the data in the file as follows:
; 
;   IDL> output = mstruct('test.dat', 'a:f,b:f,c:f')
;
;   This produces an output which is an array of structures with the 
;   following format:  
;          output = [{a:1.2, b:2.2, c:3.4}, {a:3.4, b:5.4, c:2.1}]
;
;   The output.a, output.b, output.c arrays will be floats, as
;   specified.  The values in a, b, and c correspond to values in each
;   column of data which was read.  
;
;   IDL> help, output
;      OUTPUT          STRUCT    = -> Array[2]
;    
;   IDL> help, output, /st
;   ** Structure <5a1578>, 3 tags, length=12, data length=12, refs=1:
;      A               FLOAT           1.20000
;      B               FLOAT           2.20000
;      C               FLOAT           3.40000
;
;
; MODIFICATION HISTORY:
;  
;   February 22, 2011 - Created by - Kimberly Mei Aller
;   February 23, 2011 - Added in comment, delimiter, skipline, numline
;                      /nan, from READCOL
;   February 23, 2011 - Works for create_struct version for 2009
;   February 24, 2011 - Set default comment='#'
;   March     3, 2011 - Checks if file exists, 
;                     - Checks if tagnames and tagformat are the same
;                       length
;                     - Checks if tagnames have a space instead of a
;                       comma and fixes it if user-allows
;                     - Added /verbose
;   May 31, 2011      - readcolk b/c that can go up to 80 lines
;                       NOTE - only on KMA computer
;   Dec  1/2, 2011    - tagname:tagformat linked better in inputs
;                       borrowed idea from Mike Cushing's mc_filltable
;   June 24, 2012     - made /silent work right (M. Liu)
;-

;;; --------------------------------------
;;;           ERROR CHECKS
;;; --------------------------------------

on_error, 2

if n_params() lt 1 then begin

    print, 'Syntax - read_struct2(filename, taginput,' 
    print, '                    [struct_name=struct_name, comment=comment,' 
    print, '                     delimiter=delimiter, skipline=skipline, '
    print, '                     numline=numline, /nan, /silent, /verbose]'
    
    return, ' '

endif

tagnames = taginput

;;; Check file existance

temp=file_search(filename, count=count)

if count eq 0 then begin
    
    print, 'Filename: ', filename, ' does not exist'
    return, ' '

endif

;;; Check if there is a missing comma in tagnames
;;; If there are, fix it up by splitting them by comma

tagn = strsplit(tagnames, ',', /extract)
ntags = n_elements(tagn)

for tag=0, ntags-1 do begin

   ;;; length of the tagname
    taglen = strlen(strtrim(tagn[tag], 2))
   ;;; length of the tagname without blanks in the middle
    taglen_rm = strlen(strcompress(tagn[tag], /remove_all))

    if (taglen_rm lt taglen) then begin
        tagn_new_i = strsplit(tagn[tag], ''' ''', /extract)        
    endif else begin
        tagn_new_i = tagn[tag]
    endelse
   ;;; Export the new tagnames
    if tag eq 0 then begin
        tagn_new = tagn_new_i
    endif else begin
        tagn_new = [tagn_new, tagn_new_i]
    endelse

endfor

;;; Changing your tagnames

ntags_new = n_elements(tagn_new)

if ntags_new ne ntags then begin

    tagnames_old = tagnames
    print, 'Fixing your tagnames . . .'

    for tag=0, ntags_new-1 do begin    
        if tag eq 0 then begin
            tagnames = tagn_new[tag]
        endif else begin
            tagnames = tagnames + ',' + tagn_new[tag]
        endelse
    endfor

    if keyword_set(verbose) then begin
        print, 'tagnames: ', tagnames_old
        print, 'new tagnames: ', tagnames
    endif

;;; Asking permission
    str_execute ='your input tagnames were changed to ' + tagnames + $
                 ' , is that ok?'

    if not(getyn(str_execute)) then begin
        return, ' '
    endif
endif

;;; ----------------------------------------
;;;       Parsing the tagname:tagformat
;;; 
;;;   Implemented: Dec 1, 2011
;;; ----------------------------------------

;;; Are there : in the string?

if strmatch(tagnames, '*:*') then begin
    
    tagn_tagf_set = 1

    tagnames_split = strsplit(tagnames, ',:', /extract)

    tn_loc = 2.*(findgen(n_elements(tagnames_split)/2.))
    tf_loc = tn_loc + 1
    tagnames = tagnames_split[tn_loc]
    tagformat = tagnames_split[tf_loc]

endif

;;; -----------------------------------
;;;   Add back in the commas so it works
;;;   similar to how it did before

ntags = n_elements(tagnames)
tagnames0 = tagnames
tagformat0 = tagformat

for tag=0, ntags-1 do begin
    if tag eq 0 then begin
        tagnames = tagnames0[tag]
        tagformat = tagformat0[tag]
    endif else begin
        tagnames = tagnames + ',' + tagnames0[tag]
        tagformat = tagformat + ',' + tagformat0[tag]
    endelse    
endfor

tagnames = strcompress(tagnames, /remove_all)
tagformat = strcompress(tagformat, /remove_all)
    
;;;   END Dec 2, 2011 edit (MAIN)
;;; ------------------------------------

;;; Check if tagnames and tagformats are the same length

if (tagn_tagf_set lt 1) then begin

    ntags = n_elements(strsplit(tagnames, ',', /extract))
    ntagfmt = n_elements(strsplit(tagformat, ',', /extract))

    if (ntags ne ntagfmt) then begin
        
        print, 'Number of tagnames and number of tagformats are not the same'
        print, 'number of tagnames is: ' + strcompress(ntags, /remove_all)
        print, 'number of tagformats is: ' + strcompress(ntagfmt, /remove_all)
        return, ' '

    endif

endif

;;; --------------------------------------
;;;           KEYWORD SETUP
;;; --------------------------------------

;;; STRUCT_NAME
if (1-keyword_set(struct_name)) then begin
    struct_name = ''' '''
endif else begin
    struct_name = '''' + struct_name + ''''
endelse

;;; COMMENT
if (1-keyword_set(comment)) then begin 
    comment=' '
endif else begin
;;; default = '#' instead of ' '  
;    comment = ',comment=''' + comment + ''''
    comment = ',comment=''#'''
endelse

;;; DELIMITER
if (1-keyword_set(delimiter)) then begin
    delimiter=' '
endif else begin
    delimiter = ',delimiter=''' + delimiter + ''''
endelse

;;; SKIPLINE
if (1-keyword_set(skipline)) then begin
    skipline=' '
endif else begin
    skipline = ',skipline=' + strcompress(skipline)
endelse

;;; NUMLINE
if (1-keyword_set(numline)) then begin
    numline=' '
endif else begin
    numline=',numline=' + strcompress(numline)
endelse    

;;; NAN
if (1-keyword_set(nan)) then begin
    nan=' '
endif else begin
    nan=',/NAN'
endelse

;;; SILENT
if (1-keyword_set(silent)) then begin
    silent= ' '
endif else begin
    silent=', /silent'
endelse

;;; TAGFORMAT assumed to be all floats if not set

if (1-keyword_set(tagformat)) then begin
    ntags = n_elements(strsplit(tagnames, ',', /extract))
    for tag=0, ntags-1 do begin
        if tag eq 0 then begin
            tagformat = 'f'
        endif else begin
            tagformat = tagformat + ',f'
        endelse
    endfor
endif

;;; --------------------------------------
;;;              READCOL
;;; --------------------------------------

if keyword_set(verbose) then begin
    print, 'Setting up READCOL . . . '
endif

tagnames = strcompress(tagnames)
tagformat = strcompress(tagformat)
filename0 = strcompress('''' + filename + '''')

;;; READCOLK - May 31, 2011 -only on KMA computer
str_execute = 'readcolk, ' + filename0 + ', ' + tagnames + ',' + $
              'f=''' + tagformat + '''' + comment + delimiter + skipline + $
              numline + nan + silent

if keyword_set(verbose) then begin
    print, 'Running: ' + str_execute
endif

var = execute(str_execute)

;;; --------------------------------------
;;;              TAGNAMES
;;; --------------------------------------

tagn = strsplit(tagnames, ',', /extract)
ntags = n_elements(tagn)

;;; --------------------------------------
;;;              TAGFORMAT
;;; --------------------------------------

;;; create_struct has different format codes
;;; than readcol so must be converted

tagfmt = tagformat
tagfmt_spl = strsplit(tagfmt, ',', /extract)

for tag=0, ntags-1 do begin
    if tagfmt_spl[tag] eq 'L' then begin
        tagfmt_spl[tag] = 'J'
    endif 
    if tagfmt_spl[tag] eq 'LL' then begin
        tagfmt_spl[tag] = 'K'
    endif
endfor

tagfmt=''
for tag=0, ntags-1 do begin
    if tag gt 0 then begin
        tagfmt = tagfmt + ',' + tagfmt_spl[tag]
    endif else begin
        tagfmt = tagfmt_spl[tag]
    endelse
endfor      

;;; --------------------------------------
;;;            CREATE_STRUCT
;;; --------------------------------------

if keyword_set(verbose) then begin
    print, 'Creating structure . . '
endif

;;; find the number of elements
str_execute = 'nelem = n_elements( ' + tagn[0] + ')'
var = execute(str_execute)

;;; make the structure
str_execute = 'create_struct, struct, ' + $
              struct_name + ', ''' + $
              tagnames + ''',''' + tagfmt + ''''

str_execute = strcompress(str_execute, /remove_all)

if keyword_set(verbose) then begin
    print, 'Running: ', str_execute
endif

var = execute(str_execute)

;;; now to replicate the structure

struct = replicate(struct, nelem)

;;; --------------------------------------
;;;      FILLING STRUCTURE WITH DATA
;;; --------------------------------------

if keyword_set(verbose) then begin
    print, 'Filling your structure with the arrays of data . . .'
endif

;;; now to fill the structure
locs = findgen(nelem)

for tag=0, ntags-1 do begin

    ;;; this is the tag we are filling with the data
    tag_t = tagn[tag]
    str_execute = 'struct[locs].' + tag_t + ' = ' + tag_t
    var = execute(str_execute)

endfor

return, struct

end

    
    
