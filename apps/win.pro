pro win, num, szsz, title=title, $
         xsize=xs, ysize=ys, sz=sz,  $
         xpos = xp, ypos = yp, $
         ;small = small, $
         land = land, $
         checkopen = checkopen, matchsize = matchsize, wshow = wshow

;+
; windows displayed in rows in a nice tiled fashion.
; default size is 256 x 256
;
; program is optimized for Will's laptop!
; 
; INPUTS
;  num    window number to open
; 
; OPTIONAL KEYWORD INPUTS
;  title  title string for the window
;  xsize  xsize
;  ysize  ysize
;  sz     edge size for a square window
;  small  adjusted for 17" monitor (20" is default)
; 
;  /check  if window is already open, just use it, do not open again
;  /match  same as /check, but require that already open window is desired size
;  /wshow  for either /check or /match, if re-using existing window, bring it to the front
;
; USES
;   strc
; 
; Written by M. Liu (UCB): 1995?
; 12/22/99 MCL: added /land (nice size for plots)
; 04/16/00 MCL: can now pass 'sz' as parameter also
; 11/15/01 MCL: changed default size from 350 to 335 (better for laptop)
; 05/20/04 MCL: force window number to be an integer
; 04/23/07 MCL: tweaked for new Mac 15" notebook, added /checkopen
; 09/03/08 MCL: display size calcs now done with GET_SCREEN_SIZE(), which is *much* cleaner
;                 consequently, removed the /small keyword option since obsolete
; 09/13/08 MCL: added /wshow
; 09/07/09 MCL: added /matchsize
; 07/22/12 WB: configured for my laptop
;
; Please send comments/questions to <mliu@ifa.hawaii.edu>
;-

if n_params() lt 1 then begin
    print, 'pro win,num,[title=],[xs=', strc(xs), '],[ys=', strc(ys), '],[sz=],[xp=],[yp=]'
    retall
endif

num = round(num)


if not(keyword_set(xs)) then xs = 335
if not(keyword_set(ys)) then ys = 335
if keyword_set(land) then begin
    xs = 600
    ys = 500
endif
if keyword_set(title) eq 0 then title='IDL '+strc(num)
if n_params() eq 2 then  $
  sz = szsz
if keyword_set(sz) then begin
    xs = sz
    ys = sz
endif


;; check if using laptop
;spawn, 'echo $HOST', host
;hflag = 0
;if (host(0) eq 'Hancock.local') then hflag = 1



;; default starting positions --> removed 09/04/08
;if (hflag) then begin
;    x0 = 20    ; 17" monitor, or 14" notebook 
;    ;y0 = 585 & nn = floor(1500/xs)   ; removed 04/23/07
;    y0 = 875. & nn = floor(1400/xs)  ; for new Mac 15" notebook
;endif else if keyword_set(small) then begin
;    x0 = 30    ; 17" monitor
;    y0 = 640
;    nn =  floor(1100/xs)
;endif else begin 
;    x0 = 40     
;    ;y0 = 750  &  nn = floor(1220/xs)    ; 20" monitor   
;    y0 = 1175  &  nn =  floor(1600/xs)   ; 24" monitor
;endelse

; Get number of monitors
oInfo = OBJ_NEW('IDLsysMonitorInfo')
numMons = oinfo->GetNumberOfMonitors()
;names = oinfo->GetMonitorNames()
OBJ_DESTROY, oInfo

; establish starting positions and max number of windows in horizontal direction
scrsize = get_screen_size()
nn = floor(scrsize[0] / xs)
x0 = 20
if (numMons gt 1) then offset = 96 else offset = 0 
y0 = scrsize[1] - offset

if not(keyword_set(xp)) then $
  xpos=x0+((xs+3)*(num mod nn)) $
else $
  xpos = xp


if not(keyword_set(yp)) then $
  ;ypos=y0-(ys-256)-(num/nn)*(ys+150) $  ; changed 03/27/07
  ;ypos=y0-(ys-256)-(num/nn)*(ys+30) $   ; changed 05/01/07
  ypos=y0-((num/nn)+1)*(ys+25) $
else $
  ypos = yp


;print, !d.x_size, xs, !d.y_size, ys


; if desired, check if window is already open and if it's the desired size
done = 0
if keyword_set(checkopen) or keyword_set(matchsize) then begin
    device, window_state=winstate

    if winstate(num) eq 1 then begin
       wset, num

       if keyword_set(matchsize) and ((!d.x_size ne xs) or (!d.y_size ne ys)) then $
             done = 0 $
       else $
          done = 1

       ;if keyword_set(matchsize) then begin
       ;   if (!d.x_size eq xs) and (!d.y_size eq ys) then begin
       ;      done = 1
       ;      print, 'bbbb'
       ;   endif else print, 'aaaaa'
       ;endif else begin
       ;   done = 1
       ;endelse

       if keyword_set(wshow) then $
          wshow, num

    endif

endif


if (done eq 0) then $
   window, fix(num), xs=xs, ys=ys, $
           title=title, $
           xpos = xpos, ypos = ypos
;print, num, xs, ys, xpos, ypos


end
