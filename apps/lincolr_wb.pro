PRO LINCOLR_WB, BOTTOM=BOTTOM, NAMES=NAMES, SILENT=SILENT

; set color table for 6 colored lines
; XX/XX  Jeff Valenti(?)
;
; 08/08/06 M.C. Liu: added more colors
; 05/27/07 MCL: tweaked "olive" from {0.4,0.7,0.3} to {0.2,0.6,0.2} to look more green on page
; 03/02/08 MCL: added "med.blue" color
;               previous changes were made in a new program called LINCOLR2.PRO
;               now just update all the changed into the LINCOLR.PRO routine directly
; 08/20/2010 (WB):  Revised to my own colors

if (n_elements(bottom) eq 0) then bottom = 0

;  Set color names
names = [ 'Black', 'White', 'Green', 'Red', 'Blue', 'Magenta', 'Cyan', 'Brown', $
          'Orange', 'Pink', 'Aquamarine', 'Orchid', 'Gray', 'Dark Green', 'Gold', 'Navy' ]

if not(keyword_set(silent)) then $
  print, $
  ' 0:black, 1:white, 2:bright green, 3:red, 4:blue, 5:magenta, 6:cyan, 7:brown, 8:orange, '+ $
            '9:pink, 10:aquamarine, 11:orchid, 12:gray, 13:dark green, 14:gold, 15:light gray'

;navy:  0, 0, 175

;- Load graphics colors
;        0   1    2    3    4    5    6    7    8    9    10   11   12   13   14   15
red = [  0, 255,   0, 255,   0, 255,   0, 204, 255, 255, 112, 219, 120,  51, 255, 200]
grn = [  0, 255, 255,   0,   0,   0, 201, 102, 187, 159, 219, 112, 120, 188, 171, 200]
blu = [  0, 255,   0,   0, 255, 255, 255,   0,   0, 159, 147, 219, 120,  51, 127, 200]
tvlct, red, grn, blu, bottom

return
end

