FUNCTION LOOKUP, VALUE, TABVALUE, TABRESULT, RESULT, EXACT=exact, LESS=less

;  This function compares the input parameter VALUE to those in a lookup table
;  (TABVALUE), and returns the index of the largest element in the lookup table
;  that is less than or equal to VALUE.
;  If TABRESULT is passed, the function will find the element in TABRESULT with
;  the same index, and return that value as RESULT.  Therefore, TABRESULT must
;  have the same dimensions as TABVALUE.
;
;  If VALUE is a vector or array, this function will return a vector or array of
;  indices, with the same dimensions as VALUE.
;
;  TABVALUE and TABRESULT may be vectors or arrays, but must match in
;  dimensions.  They will be sorted into ascending order of TABVALUE, which
;  cannot have any duplicate values.
;
;  This is an imitation of the LOOKUP function in MS Excel.
;
;  HISTORY
;  Written by Will Best (IfA), 08/27/2012
;
;  CALLING SEQUENCE: 
;     index = lookup(values, tablookup [, tabresult, result, exact=exact, less=less ])
;
;  INPUTS: 
;      VALUE - Item(s) to be compared to a lookup table.
;      TABVALUE - Lookup table.
;      TABRESULT - Table of elements corresponding to TABVALUE.
;
;  OUTPUTS:
;      INDEX - The index of the element in ARRAY whose value is closest to VALUE.
;      RESULT - Element in TABRESULT with the same index as the element in
;               TABVALUE that is matched to VALUE.
;
;  OPTIONAL KEYWORDS:
;      EXACT - Only return the index of the element in TABRESULT that is equal
;              to VALUE.  If no such element exists in TABRESULT, return -1.  If
;              VALUE is a vector or array with only some elements that appear in
;              TABRESULT, then INDEX will contain indices for those elements and
;              -1 for the others.
;      LESS - Return the index of the smallest element in the lookup table that
;             is greater than or equal to VALUE.
;

;on_error, 2
if n_params() lt 2 then begin
    message, 'Syntax:  index = lookup(values, tablookup [, tabresult, result, exact=exact, less=less ])'
    return, -1
endif

; Sort the TABVALUE array into ascending order
tabsort = sort(tabvalue)
tabvalue = tabvalue[tabsort]

; Check for duplicates 
if n_elements(tabvalue) ne n_elements(uniq(tabvalue)) then begin
    message, 'Error:  TABVALUE cannot have any duplicate elements'
    return, -1
endif

; Look stuff up
if keyword_set(exact) then begin
; Exact matches only
    match2, value, tabvalue, index
endif else begin
    n = n_elements(value)
    index = indgen(n)
    if keyword_set(less) then begin
    ; Use the table elements as ceilings for matches
        for i=0L, n-1 do index[i] = min(where(value[i] le tabvalue))
    endif else begin
    ; Use the table elements as floors for matches
        for i=0L, n-1 do index[i] = max(where(value[i] ge tabvalue))
    endelse
endelse

; If TABRESULT is supplied, return looked up values in RESULT
if n_elements(tabresult) gt 0 then begin
    if size(tabvalue, /dim) ne size(tabresult, /dim) then begin
        message, 'Error: TABVALUE and TABRESULT must have the same dimensions.'
        return, -1
    endif
    tabresult = tabresult[tabsort]
    result = tabresult[index]
endif

return, index

END
