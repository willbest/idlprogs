FUNCTION MOTION_ANGLE, PM_RA, PM_DEC, PM_RA_ERR=pm_ra_err, PM_DEC_ERR=pm_dec_err, ANGLE_ERR=angle_err

;+
;  Returns the direction of motion of an object, given motion in RA and Dec.
;
;  HISTORY
;  Written by Will Best (IfA), 2014-01-24
;
;  INPUTS
;      PM_RA - RA proper motion * cosine(Dec).
;      PAR - Dec proper motion.
;
;  KEYWORD INPUTS
;      PM_RA_ERR - Error in RA proper motion.
;      PM_DEC_ERR - Error in Dec proper motion.
;
;  KEYWORD OUTPUT
;      ANGLE_ERR - Uncertainty in the direction of motion.
;         Requires non-zero values supplied to PM_RA_ERR and PM_DEC_ERR
;-

; Calculate the absolute magnitude
angle = atan(pm_ra / pm_dec)
angle = angle * 180./!pi              ; convert from radians to degrees
if pm_dec lt 0 then begin
    angle = angle + 180.              ; Handle angles for negative Dec motion
endif else begin
    if pm_ra lt 0 then angle = angle + 360.    ; Handle angles for negative RA motion
endelse

; If requested, calculate the uncertainty
if n_elements(pm_ra_err) gt 0 and n_elements(pm_dec_err) gt 0 then begin
    angle_err = sqrt((pm_dec*pm_ra_err)^2 + (pm_ra*pm_dec_err)^2) / (pm_ra^2 + pm_dec^2)
    angle_err = angle_err * 180./!pi              ; convert from radians to degrees
endif

return, angle          

END
