function bb_gconvol, w, f, kernel, error=error

; Gaussian smooth a spectrum and propagate errors.
; Manual convolution- does not reverse kernel, but ok for Gaussian.
; kernel=fwhm of gaussian kernel
; automatically normalizes the spectrum to original area
;
; output: a structure with tags of {w, convf} or {w, convf, converr}
;
; History: Created by Brendan Bowler (IfA) 12-9-2010 
;          Added /NAN keywords: BPB: 2-12-2011

; make kernel
npixel = kernel * 10.

psf = psf_gaussian(npixel=npixel, fwhm=kernel, ndimension=1)


if not keyword_set(error) then begin

  convf = convol(f,psf,/edge_truncate, /normalize, /nan, /center)

  outstruc = {w:w, convf:convf}

endif else begin

  orig_area = total(f, /nan)

  convf = fltarr(n_elements(f)+n_elements(psf))
  converr = fltarr(n_elements(f)+n_elements(psf))

  ; index for spectrum
  xarr = findgen(n_elements(psf))

  tmpf = [fltarr(n_elements(psf))+!values.f_nan, f, fltarr(n_elements(psf))+!values.f_nan]
  fxarr = findgen(n_elements(tmpf))
  tmpe = [fltarr(n_elements(psf))+!values.f_nan, error, fltarr(n_elements(psf))+!values.f_nan]

  for i=0,n_elements(f)+n_elements(psf)-1 do begin

    convf[i] = total(tmpf[xarr] * psf, /double, /nan)
    converr[i] = sqrt( total( tmpe[xarr]^2 * psf^2 , /double, /nan) )

    fracgood = total(finite(tmpf[xarr])*psf)/total(psf)
    if fracgood lt 0.99 then convf[i] = !values.f_nan
    ;print,fracgood,total(finite(tmpf[xarr])),float(n_elements(psf))

    ; shift index
    xarr=xarr+1

  endfor

  convf = convf[n_elements(psf)/2.:n_elements(psf)/2.+n_elements(f)-1]
  converr = converr[n_elements(psf)/2.:n_elements(psf)/2.+n_elements(error)-1]

  converr = converr/total(convf,/nan) * orig_area
  convf = convf/total(convf,/nan) * orig_area

  outstruc = {w:w, convf:convf, converr:converr}

endelse



return, outstruc


end
