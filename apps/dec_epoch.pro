FUNCTION DEC_EPOCH, VDATE

;+
;  Converts date information into a decimal year.  Date must be entered as a
;  vector in the specified format (below), or as a Julian date.
;  If a single value is entered
;  All calculations are done in double precision.
;
;  HISTORY
;  Written by Will Best (IfA), 01/17/2013
;
;  CALLING SEQUENCE: 
;     dec = dec_epoch(vdate)
;
;  INPUTS: 
;      VDATE - Vector of date info to be converted, or Julian date.
;            If a scalar is entered, the program assumes it is a Julian date,
;            and calls daycnv.pro to convert it to year-month-day-hour.
;            Otherwise, the vector format is:
;              vdate[0] = year
;              vdate[1] = month
;              vdate[2] = date
;              vdate[3] = hour
;              vdate[4] = minute
;              vdate[5] = second
;
;  OUTPUT:
;      DEC - Date in decimal year form.
;-

vdate = double(vdate)

if n_elements(vdate) eq 1 then begin                       ; if single number input, assume it's a Julian date
    daycnv, vdate, yr, mn, dt, hr                          ; convert Julian date to year format
    vdate = double([yr, mn, dt, hr, 0, 0])                 ; create the vdate vector
endif

year = vdate[0]                                            ; Extract the year
dlist = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
if (year mod 4 eq 0) and (year ne 0) then dlist[1] = 29    ; Leap years!

date = total(dlist[0:vdate[1]-1]) - dlist[vdate[1]-1]      ; total days in preceding months
date = date + vdate[2] - 1                                 ; preceding days in this month
hour = vdate[3] + vdate[4]/60d + vdate[5]/3600d            ; convert hms into decimal hours
date = date + hour/24d
dec = year + date/365.26d

return, dec

END
