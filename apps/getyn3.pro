function getyn3, qstr, def,  $
                noquery = noquery, help=help

;+
; get a reply to a yes/no/other question,
; allowing the user to hit <CR> to select the default respone
;
; INPUT
;   qstr  string to be printed (the question)
;
; OPTIONAL INPUT
;   def   default answer (0=no, 1=yes, other=other; yes is default)
;
; KEYWORD PARM
;   noquery  if this is set, then returns the default answer
;            w/o asking any question (seems useless, but
;            actually useful for some programs)
;
; RETURNS
;   0 = no
;   1 = yes
;  -1 = other
;
; EXAMPLE
;  if getyn('print something?') then print,'something'
;
; HISTORY: Written by M. Liu (UCB) 09/04/96 
; 06/27/00: added /noquery
; 07/27/12 (WB): added option for a third response
; 
;-


on_error, 2

if n_elements(def) eq 0 then def = 1
if ((def ne 0) and (def ne 1)) then def = -1
if n_params() eq 0 or keyword_set(help) then begin
    print, 'function getyn(qstr, def, noquery=noquery)'
    retall
endif

; check for /noquery
if keyword_set(noquery) then  $
  return, def


cr = string("12b)		; carriage return

repeat begin

    print, format = '($,A)', qstr

    case def of
        1 : print, format = '($,"(<y>,n,o) ")'
        0 : print, format = '($,"(y,<n>,o) ")'
        else : print, format = '($,"(y,n,<o>) ")' 
    endcase

    case get_kbrd(1) of
        
        'y': begin
            ans = 1
        end
        
        'n': begin
            ans = 0
        end

        'o': begin
            ans = -1
        end

        cr: begin
            ans =  def
        end

        Else: begin
            ans = -2
        end

    endcase
    print

endrep until (ans ge -1)

return, ans

end

        
