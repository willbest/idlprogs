;+
;
; NAME:
;
;   ADD_STRUCT
; 
; PURPOSE:
; 
;   Combine two structures with DIFFERENT tagnames and
;   the same length.  
;
;   Good for if you have objects separated into two surveys
;   and you want to merge the structures without typing out
;   the details for each program...?
;
; INPUT:
;
;   STRUCT1 - Any structure. 
;   STRUCT2 - Any structure with the same length as struct1
;   
; KEYWORDS:
;
;   DUP_TAG - Set this keyword if there are duplicate tagnames
;             between STRUCT1 and STRUCT2
;
;   TAG_S1  - Set this keyword if you prefer the values for 
;             the duplicate tags to come from STRUCT1 (This is 
;             also the default)
;   
;   TAG_S2  - Set this keyword if you prefer the values for 
;             the duplicate tags to come from STRUCT2
;
; NOTES:
;
;   If your tags overlap in the structures then the program
;   will warn you.
; 
;   If your structure lengths are different then the program
;   will warn you.
;
; EXAMPLE:
;
;   IDL> s1 = {a:10, b:'hello', c:5}
;   IDL> s1 = repliate(s1, 5)
;   IDL> s2 = {d:20, e:'bye', c:25}
;   IDL> s2 = replicate(s2, 5)
;   IDL> s3 = add_struct(s1, s2)
;   IDL> help, s3, /st
;   A               INT             10
;   B               STRING    'hello'
;   C               INT              5
;   D               INT             20
;   E               STRING    'bye'
;   F               INT             25
;   IDL> help, s3
;   S3              STRUCT    = -> <Anonymous> Array[5]
;
;
; TESTS DONE:
;
;   1. combine two structures totally different
;   2. combine two structures with overlapping tagnames
;
; RESTRICTIONS: 
;
;     
;
; TO-DO LIST: 
;
;
;
; MODIFICATION HISTORY:
;   
;   January 24, 2012 - Created - Kiki (checked for simple tests)
;-
function add_struct, s1, s2, dup_tag=dup_tag, $
                     tag_s1=tag_s1, tag_s2=tag_s2, $
                     verbose=verbose

;;; ERRORS for too few arguments
on_error, 2

if n_params() lt 1 then begin
    print, 'SYNTAX - ADD_STRUCT(s1, s2, '
    print, '         [dup_tag=dup_tag, /verbose])'
    return, 0
endif

s1_tagn = tag_names(s1)
s2_tagn = tag_names(s2)

;;; ------------------------------------
;;; Check if the TAGNAMES overlap!!


match, s1_tagn, s2_tagn, sub1, sub2, count=count
if count ne 0 then begin
    print, 'There are overlapping tagnames'

    if keyword_set(dup_tag) then begin

        ;;; This means that the tagnames in struct 2 are
        ;;; the ones we keep 
        if keyword_set(tag_s2) then begin

            ;;; This is where the s1_tagn = s2_tagn
            n1 = n_elements(s1_tagn)
            locs_t1 = findgen(n1)
            match, locs_t1, sub1, sub_t1, sub11, count=count

            ;;; this is where the s1_tagn = s2_tagn
            locs_t1[sub_t1] = -1
            
            ;;; w_ns1_tags = where to keep
            w_s1_tags = where(locs_t1 ge 0)

            ;;; Remove the s1_tagn where there are already s2
            s1_tagn = s1_tagn[w_s1_tags]
            
        ;;; Else we use the tagnames from struct 1
        endif else begin
            
            ;;; This is where the s1_tagn = s2_tagn
            n2 = n_elements(s2_tagn)
            locs_t2 = findgen(n2)
            ;;; sub2 = locations in s2_tagn where s1=s2
            match, locs_t2, sub2, sub_t2, sub22, count=count

            ;;; this is where the s1_tagn = s2_tagn
            locs_t2[sub_t2] = -1
            
            ;;; w_ns2_tags = where to keep
            w_s2_tags = where(locs_t2 ge 0)

            ;;; Remove the s1_tagn where there are already s2
            s2_tagn = s2_tagn[w_s2_tags]
            
        endelse

    endif else begin
        ;;; This means that we didn't know that there were duplicate tags
        print, 'The following tags are the same: '
        print, s1_tagn[sub1]
        print, 'If you wish to use the tagnames from struct 1 or 2'
        print, 'then recall the program and use /DUP_TAG keyword'
        return, 0
    endelse
        
endif


;;; Number of tags in total
n_tags1 = n_elements(s1_tagn)
n_tags2 = n_elements(s2_tagn)
n_tags = n_tags1 + n_tags2

;;; Structure Length
num = n_elements(s1)
num2 = n_elements(s2)

;;; Check the STRUCTURE length for consistency
if num ne num2 then begin
    print, 'Your structures are NOT the same length'
    
    return, 0
endif    

;;; ----------------------------------
;;; ----------------------------------
;;; 
;;;          CREATE NEW
;;; 
;;; ----------------------------------
;;; ----------------------------------


;;; ----------------------
;;; TAGNAMES

for i=0, n_tags1-1 do begin
    if i eq 0 then tagn1 = s1_tagn[i] + ',' 
    if i gt 0 then tagn1 = tagn1 + s1_tagn[i] + ','
endfor    
    
for i=0, n_tags2-1 do begin
    if i eq 0 then tagn2 = s2_tagn[i] + ',' 
    if i gt 0 then tagn2 = tagn2 + s2_tagn[i] + ','
endfor    

;;; Final comma-separated tagnames for create_struct
tagn = tagn1 + tagn2

;;; ----------------------
;;; TAGFORMAT

tagf1 = fltarr(n_tags1)
tagf2 = fltarr(n_tags2)

for i=0, n_tags1-1 do begin

    ;;; Get the TYPE
    str = 'fill_type = size(s1.' + s1_tagn[i] + ',/type)'

    var = execute(str)
    tagf1[i] = fill_type

    if keyword_set(verbose) then print, fill_type
endfor

for i=0, n_tags2-1 do begin

    ;;; Get the TYPE
    str = 'fill_type = size(s2.' + s2_tagn[i] + ',/type)'
    var = execute(str)
    tagf2[i] = fill_type

    if keyword_set(verbose) then print, fill_type
endfor

tagf = [tagf1, tagf2]

;;; Select the types?

w1 = where(tagf eq 1, n1)
w2 = where(tagf eq 2, n2)
w3 = where(tagf eq 3, n3)
w4 = where(tagf eq 4, n4)
w5 = where(tagf eq 5, n5)
w6 = where(tagf eq 6, n6)
w7 = where(tagf eq 7, n7)
w8 = where(tagf eq 8, n8)
w9 = where(tagf eq 9, n9)
w10 = where(tagf eq 10, n10)
w11 = where(tagf eq 11, n11)
w12 = where(tagf eq 12, n12)
w13 = where(tagf eq 13, n13)
w14 = where(tagf eq 14, n14)
w15 = where(tagf eq 15, n15)

;;; STRING for tagformat
tagfstr = strarr(n_tags)

if n1 gt 0 then tagfstr[w1] = 'b' ;;; byte
if n2 gt 0 then tagfstr[w2] = 'i' ;;; integer
if n3 gt 0 then tagfstr[w3] = 'j' ;;; longword integer
if n4 gt 0 then tagfstr[w4] = 'f' ;;; float
if n5 gt 0 then tagfstr[w5] = 'd' ;;; double
if n6 gt 0 then tagfstr[w6] = 'c' ;;; complex
if n7 gt 0 then tagfstr[w7] = 'a' ;;; string
;if n8 gt 0 then tagfstr[w8] = 'structure'
if n9 gt 0 then tagfstr[w9] = 'm' ;;; double complex
;if n10 gt 0 then tagfstr[w10] = 'pointer'
;if n11 gt 0 then tagfstr[w11] = 'object'
if n12 gt 0 then tagfstr[w12] = 'i' ;;; unsigned integer?
if n13 gt 0 then tagfstr[w13] = 'b'
if n14 gt 0 then tagfstr[w14] = 'k' ;; 64bit integer
if n15 gt 0 then tagfstr[w15] = 'k' ;; unsigned 64bit integer

;;; Make the string now
for i=0, n_tags-1 do begin
    if i eq 0 then tagfs = tagfstr[i]
    if i ne 0 then tagfs = tagfs + ',' + tagfstr[i]
endfor

create_struct, s0, '', tagn, tagfs

s0 = replicate(s0, num)

tagn_ar = strsplit(tagn, ',', /extract)

ind = dindgen(num)

for i=0, n_tags-1 do begin

    if i lt n_tags1 then begin
        str = 's0[ind].' + tagn_ar[i] + ' = s1[ind].' + tagn_ar[i]
    endif else begin
        str = 's0[ind].' + tagn_ar[i] + ' = s2[ind].' + tagn_ar[i]
    endelse
    
    var = execute(str)
    
endfor

return, s0

end
