FUNCTION COMBINEMAG, M1, M2, M1ERR, M2ERR

;  Returns the combined magnitude of two objects (e.g. a binary star).
;       (Why isn't this in astrolib?)
;
;  HISTORY
;  Written by Will Best (IfA), 08/18/2012
;  07/10/2013 (WB): Fixed bug -- had m1 in both 10^ terms.
;
;  INPUTS:
;      M1, M2 - input magnitudes
;      M1ERR, M2ERR - input magnitude errors

; Calculate combined magnitude 
f = 10^(m1/(-2.5)) + 10^(m2/(-2.5))
m = -2.5 * alog10(f)

; If error(s) are input, calculate the error of m.
if n_elements(m1err) gt 0 then begin
    if n_elements(m2err) eq 0 then m2err = 0.
    ;merr = sqrt( (m1err*10^(m1/(-2.5)))^2 + (m2err*10^(m2/(-2.5)))^2 ) / f
    merr = sqrt( m1err^2 + m2err^2 )
    m = [m, merr]
endif

return, m

END
