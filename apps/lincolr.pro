PRO LINCOLR, SILENT=SILENT

; set color table for 6 colored lines
; XX/XX  Jeff Valenti(?)
;
; 08/08/06 M.C. Liu: added more colors
; 05/27/07 MCL: tweaked "olive" from {0.4,0.7,0.3} to {0.2,0.6,0.2} to look more green on page
; 03/02/08 MCL: added "med.blue" color
;               previous changes were made in a new program called LINCOLR2.PRO
;               now just update all the changed into the LINCOLR.PRO routine directly

if not(keyword_set(silent)) then $
  print, $
  ' 0: black, 1:white, 2:bright green, 3:red, 4:aqua, 5:yellow, 6:purple, 7:blue, 8:white, '+ $
            '9:dark green, 10:orange, 11:grey, 12:med.blue, 13:pink, 14: sky blue'
  ;' 0: black, 1:white, 2:green, 3:red, 4:aqua, 5:yellow, 6:purple, 7:blue, 8:white'

;; original
;red=  [0,1,0,1,0,1,1,0,1,.5,1]*255
;green=[0,1,1,0,1,1,0,0,1,.7,1]*255
;blue= [0,1,0,0,1,0,1,1,1,.3,1]*255
 
;      0  1  2  3  4  5  6  7  8   9   10   11   12    13    14
red=  [0, 1, 0, 1, 0, 1, 1, 0, 1, .2, 0.8, 0.3, 0.00, 1.00, 0.69]*255
green=[0, 1, 1, 0, 1, 1, 0, 0, 1, .6, 0.4, 0.3, 0.65, 0.75, 0.88]*255
blue= [0, 1, 0, 0, 1, 0, 1, 1, 1, .2, 0.0, 0.3, 1.00, 0.80, 0.90]*255
 

tvlct,red,green,blue

return
end

