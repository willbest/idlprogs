;clrset='lin'
;clrset='cube'
;ps=1
;=1
;PRO SIMPLEPLOT, XARR, YARR, CLRSET=clrset, FILE=file, PS=ps$
;           _EXTRA=ex

;  Routine to handle the nuts and bolts of plotting, so it can be called
;  efficiently from other programs.
;  Plots black and colors on a white background.
;  
;  HISTORY
;  Written by Will Best (IfA), 2/20/2012
;
;  INPUTS
;       XARR - Values for the horizontal axis
;       YARR - Values for the vertical axis
;
;  KEYWORDS
;       CLRSET - Choose a color scheme.
;          If not set, the currently loaded color table is used
;          'lin' calls the lincolr.pro color scheme.
;                0:black, 1:white, 2:bright green, 3:red, 4:blue, 5:magenta, 6:cyan, 7:brown,
;                8:orange, 9:pink, 10:aquamarine, 11:orchid, 12:gray, 13:dark green, 14:gold, 15:navy
;          'cube' calls the cubehelix.pro color scheme (0:black, 255:white).

; Define special characters
angstrom = '!sA!r!u!9 %!X!n'

; Load color table
if not keyword_set(clrset) then clrset=''
case clrset of
    'lin' : begin
        device, decomposed=0
        lincolr, /silent, names=names
        black = 0
        white = 255
        red = 200
        blue = 100
    end
    'cube' : begin
        device, decomposed=0
        cubehelix
        black = 0
        white = 1
        red = 3
        blue = 4
    end
    else : print, 'No change to color table.'
endcase

; Set up for postscript output
if keyword_set(ps) then begin
    if size(file, /type) ne 7 then message, 'ERROR: Path/name needed for postcript output.'
    
endif else begin
    wn = !d.window + 1
    window, wn, xsize=700, ysize=525
endelse

END
