FUNCTION CLOSEST, ARRAY, VALUE, ACTUAL=actual, DELTA=delta

;  Returns the index of a vector element whose value is closest to a set value,
;  supplied by the user.
;
;  HISTORY
;  Written by Will Best (IfA), 02/15/2012
;     (not like I'm the first to do this...)
;
;  CALLING SEQUENCE: 
;     result = closest(array, array [, actual=actual, delta=delta ])
;
;  INPUTS: 
;      ARRAY - Vector to be searched.
;      VALUE - The target scalar value.
;
;  OUTPUT:
;      RESULT - The index of the element in ARRAY whose value is closest to VALUE.
;
;  OPTIONAL KEYWORD OUTPUTS:
;      ACTUAL - The actual value of the element in ARRAY whose value is closest to VALUE.
;      DELTA - The difference between the closest value in ARRAY and the target value.

on_error, 2
if n_params() lt 2 then $
  message, 'Syntax:  index = closest(array, value [, actual=actual, delta=delta ])'

delta = min(abs(array - value), index)
actual = array[index]

return, index

END
