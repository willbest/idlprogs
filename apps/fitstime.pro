FUNCTION FITSTIME, HEAD

; Extracts the exposure time from a FITS header.
t = head[where(strmid(head, 0, 7) eq 'EXPTIME')]
eqpos = strpos(t, '=')
slpos = strpos(t, '/')
t1 = strmid(t[0], eqpos+1, slpos-eqpos-1)
t2 = strtrim(t1, 2)
time = double(t2)
return, time

END
