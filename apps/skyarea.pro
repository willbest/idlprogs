FUNCTION SKYAREA, ra1, ra2, dec1, dec2, HOURS=hours, SILENT=silent, STERADIANS=steradians, $
                  TRIANGLE=triangle

;  Calculates the area of a (spherical) rectangular or triangular region of sky.
;  Default unit is square degrees. 
;  The triangular case assumes a right spherical triangle with a horizontal base 
;  (i.e. two points of equal declination).
;
;  HISTORY
;  Written by Will Best (IfA), 05/22/2012
;
;  INPUTS
;      RA1, RA2 - Right ascension values for the boundaries of the region.  Can
;                 be entered in ascending or descending order.  Assumes the
;                 values are given in degrees unless the HOURS keyword is set.
;      DEC1, DEC2 - Declination values for the boundaries of the region.  Can be
;                   entered in ascending or descending order.  If the TRIANGLE
;                   keyword is set, the program will calcualte the area of a
;                   right spherical triangle with base (ra1, dec1), (ra2, dec1)
;                   and apex (ra1, dec2).
;           CAUTION:  If DEC2 is less than DEC1, a spherical triangle may have
;           a different area than if DEC1 is less than DEC2 !!!
;
;  OPTIONAL KEYWORDS
;      HOURS - Set if right ascension values are given as decimal hours.
;          The program will not accept hours, minutes, seconds.
;      SILENT - Suppress output to screen
;      STERADIANS - Output area is in steradians.
;      TRIANGLE - Calculate the area of a triangle with base (ra1, dec1), (ra2, dec1), 
;                 and apex (ra1, dec2).

if n_params() ne 4 then begin
    print, 'SYNTAX: a = skyarea(ra1, ra2, dec1, dec2, HOURS=hours, SILENT=silent,'
    print, '                    STERADIANS=steradians, TRIANGLE=triangle'
    return, 0
endif

if ra1 gt ra2 then swap, ra1, ra2

if keyword_set(hours) then begin
    x1 = ra1 * 15.
    x2 = ra2 * 15.
endif else begin
    x1 = ra1
    x2 = ra2
endelse

y1 = dec1 * !pi/180.
y2 = dec2 * !pi/180.

if keyword_set(steradians) then begin
    unit = 'steradians'
    c = !pi/180.
endif else begin
    unit = 'square degrees'
    c = 180./!pi
endelse

if keyword_set(triangle) then begin
    if y2 ge y1 then begin
        des = 'right spherical triangle, with northward apex'
    endif else begin
        des = 'right spherical triangle, with southward apex'
    endelse
    b = (cos(y1)-cos(y2)) / (y2-y1)
endif else begin
    des = 'spherical rectangle'
    b = sin(y2)
endelse

area = abs(c * (x2-x1) * (b-sin(y1)))

if not keyword_set(silent) then begin
    print, 'Area of a ', des
    print, area, unit, format='(f9.3," ",a)'
endif

return, area

END
