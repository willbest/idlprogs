FUNCTION TEXTSIGN, X, POS=pos, ZERO=zero

;+
;  This function returns a string, either '+' or '-', the sign of the input
;  number x.  x can be a scalar or a vector.
;  By default, '+' is returned for a number equal to 0.  Set the ZERO keyword to
;  return '0' for a number equal to 0.
;
;  HISTORY
;  Written by Will Best (IfA), 02/05/2013
;
;  CALLING SEQUENCE: 
;      sign = textsign(x [, zero=zero ])
;
;  INPUTS 
;      X - Number whose sign you want returned.
;
;  KEYWORDS
;      POS - return '+' for a positive number, and '' for negative.
;-

;on_error, 2
if n_params() lt 1 then begin
    message, 'Syntax:  sign = textsign(x [, zero=zero ])'
    return, 'error'
endif

n = n_elements(x)
sign = strarr(n)

for i=0, n-1 do begin
    if x[i] ge 0 then sign[i]='+' else sign[i]='-'
    if keyword_set(zero) and x[i] eq 0 then sign[i]='0'
    if keyword_set(pos) and x[i] lt 0 then sign[i]=''
endfor

return, sign

END
