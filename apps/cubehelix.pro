pro cubehelix,start=start,rots=rots,hue=hue,gamma=gamma,get=get,plot=plot
;+
; NAME: CUBEHELIX
;
; PURPOSE: Calculate a "cube helix" color table
;          Based on the FORTRAN 77 code provided in 
;          D.A. Green, 2011, BASI, 39, 289
;
;          http://adsabs.harvard.edu/abs/2011arXiv1108.5083G
;
; CALLING SEQUENCE:  CUBEHELIX,[start=start,rots=rots,hue=hue,gamma=gamma,
;                               get=get,plot=plot,w0=w0]
;
; OPTIONAL INPUTS:  [this wording is taken from the paper]
;          START: color (1=red, 2=green, 3=blue)
;                 e.g.  0.5=purple
;                 DEFAULT = 0.5
;
;          ROTS:  rotations in colour (typically -1.5 to 1.5)
;                 DEFAULT = -1.5
;
;          HUE:   hue intensity scaling (in the range 0 (B+W) to 1
;                 to be strictly correct, larger values may be OK with
;                 particular star/end colours)
;                 DEFAULT = 1.2
;
;          GAMMA: set the gamma correction for intensity
;                 DEFAULT = 1.0
;
; KEYWORD PARAMETERS:
;          PLOT:  Have the program plot a color-bar to the screen,
;                 with colors 0->255 going left->right 
;
; OPTIONAL OUTPUTS:
;          GET:   Set this to a named vector which will have
;                 dimensions [256,3] to hold the RGB color vectors 
;
; EXAMPLE:
;
;          IDL> cubehelix,/plot,gamma=1,/w0
;
; MODIFICATION HISTORY:
;          August 2011, Created by J.R.A. Davenport
;I would appreciate a simple acknowledgement for published works using my code:
;   "This publication has made use of code written by James R. A. Davenport."
;
;-

; set options for the compiler
  compile_opt defint32, strictarr, strictarrsubs
; suppress some outputs
  compile_opt HIDDEN

  nlo = 0
  nhi = 0

; will always assume 256 colors for IDL
  nlev = 256 

; use defaults from the preprint if not otherwise set
  if not keyword_set(start) then start = 0.5 ; purple
  if not keyword_set(rots) then rots = -1.5
  if not keyword_set(gamma) then gamma = 1.0
  if not keyword_set(hue) then hue = 1.2

; the blank arrays to put the color indicies
  red = fltarr(nlev) 
  grn = fltarr(nlev)
  blu = fltarr(nlev)
  
for i=0,255 do begin
   fract = float(i)/float(nlev - 1)
   angle = 2.*!dpi*(start/3.0+1.0+rots*fract)
   fract = fract^gamma
   amp = hue*fract*(1-fract)/2.0
   red[i]=fract+amp*(-0.14861*cos(angle)+1.78277*sin(angle))
   grn[i]=fract+amp*(-0.29227*cos(angle)-0.90649*sin(angle))
   blu[i]=fract+amp*(1.97294*cos(angle))

   if red[i] lt 0 then begin
      red[i] = 0.
      nlo = nlo+1
   endif
   if grn[i] lt 0 then begin
      grn[i] = 0.
      nlo = nlo+1
   endif
   if blu[i] lt 0 then begin
      blu[i] = 0.
      nlo = nlo+1
   endif

   if red[i] gt 1 then begin
      red[i] = 1.
      nhi = nhi+1
   endif
   if grn[i] gt 1 then begin
      grn[i] = 1.
      nhi = nhi+1
   endif
   if blu[i] gt 1 then begin
      blu[i] = 1.
      nhi = nhi+1
   endif
endfor

  if total(nhi) gt 0 then print,'Warning: color-clipping on high-end'
  if total(nlo) gt 0 then print,'Warning: color-clipping on low-end'

  tvlct,fix(red*255.),fix(grn*255.),fix(blu*255.) ; load the new color table

; output the color vectors if requested
  get=[[red],[grn],[blu]]

; show on screen if requested
  if keyword_set(plot) then begin
     plot,[-5,260],[1,1],/nodata,/xstyle,/ystyle,yrange=[-1,1]
     for i=0,255 do polyfill,[i-.5,i+.5,i+.5,i-.5],[-1,-1,1,1],/data,color=i
  endif

return
end

