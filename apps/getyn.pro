function getyn, qstr, def,  $
                noquery = noquery, help=help

;+
; get a reply to a yes/no question,
; allowing the user to hit <CR> to select the default respone
;
; INPUT
;   qstr  string to be printed (the question)
;
; OPTIONAL INPUT
;   def   default answer (0=no, not 0=yes; yes is default)
;
; KEYWORD PARM
;   noquery  if this is set, then returns the default answer
;            w/o asking any question (seems useless, but
;            actually useful for some programs)
;
; RETURNS
;   0 = no
;   1 = yes
;
; EXAMPLE
;  if getyn('print something?') then print,'something'
;
; HISTORY: Written by M. Liu (UCB) 09/04/96 
; 06/27/00: added /noquery
; 
; Please send comments/questions to <mliu@astro.berkeley.edu>
;-


on_error, 2

if n_elements(def) eq 0 then def = 1
if (def lt 0) then def = 0
if n_params() eq 0 or keyword_set(help) then begin
    print, 'function getyn(qstr, def, noquery=noquery)'
    retall
endif

; check for /noquery
if keyword_set(noquery) then  $
  return, def


cr = string("12b)		; carriage return

repeat begin

    print, format = '($,A)', qstr

    if (def ne 0) then print, format = '($,"(<y>,n) ")' $
    else print, format = '($,"(y,<n>) ")' 

    case get_kbrd(1) of
        
        'y': begin
            ans = 1
        end
        
        'n': begin
            ans = 0
        end

        cr: begin
            ans =  def
        end

        Else: begin
            ans = -1
        end

    endcase
    print

endrep until (ans ge 0)

return, ans

end

        
