FUNCTION APP2ABS_DIST, APPMAG, DIST, APPMAGERR=appmagerr, DISTERR=disterr, ABSMAGERR=absmagerr

;+
;  Returns the absolute magnitude of an object, given apparent magnitude and distance.
;
;  HISTORY
;  Written by Will Best (IfA), 2013-11-04
;  2013-12-30 (WB):  Added error propagation
;
;  INPUTS
;      APPMAG - Apparent magnitude of the object.
;      DIST - distance to the object (parsecs).
;
;  KEYWORD INPUTS
;      APPMAGERR - Error in apparent magnitue.
;      DISTERR - Error in distance.
;
;  KEYWORD OUTPUT
;      ABSMAGERR - Uncertainty in the absolute magnitude
;         Requires non-zero values supplied to APPMAGERR and DISTERR
;-

; Calculate the absolute magnitude
absmag = appmag + 5 - 5*alog10(dist)

; If requested, calculate the uncertainty
if keyword_set(appmagerr) and keyword_set(disterr) then begin
    absmagerr = sqrt(appmagerr^2 + (5*0.43429*disterr/dist)^2)
endif

return, absmag          

END
