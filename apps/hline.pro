;+
; NAME:
;      HLINE
;     
; PURPOSE:
;      Draw a horizontal line on a pre-existing plot window.
;
; CALLING SEQUENCE:
;      HLINE, VAL
;
; INPUTS:
;
;      VAL: The y-value where the horizontal line should be drawn
;
; KEYWORD PARAMETERS:
;
;      XLOG: Set this if the existing plot has a log-scale abscissa/
;      All other keyword parameters are passed to OPLOT.
;
; SIDE EFFECTS:
;
;      Causes a horizontal line to appear on your screen.
;
; RESTRICTIONS:
;
;      This program won't do anything else. Sorry, them's the 
;      restrictions.
;
; EXAMPLE:
;
;      Draw a horizontal line at x = 4
;      IDL> plot, findgen(10)
;      IDL> hline, 4
;
; MODIFICATION HISTORY:
; Written sometime in 2003 by JohnJohn
; 02/23/2013 (WB): Added XLOG keyword
;-

pro hline, val, XLOG=xlog, _extra=extra

nv = n_elements(val)
if keyword_set(xlog) then x=10^!x.crange else x=!x.crange
for i = 0, nv-1 do oplot, x, fltarr(2)+val[i], _extra=extra 

end
